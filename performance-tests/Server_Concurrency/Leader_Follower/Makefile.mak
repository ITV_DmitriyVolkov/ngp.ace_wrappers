#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake -value_template platforms=Win64 -make_coexistence -recurse -exclude TAO/CIAO -features qos=1,mfc=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: Svr_Conc_Leader_Follower Svr_Conc_Leader_Follower_RT_CORBA

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.Svr_Conc_Leader_Follower.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Svr_Conc_Leader_Follower.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Svr_Conc_Leader_Follower_RT_CORBA.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Svr_Conc_Leader_Follower_RT_CORBA.mak CFG="$(CFG)" $(@)

Svr_Conc_Leader_Follower:
	@echo Project: Makefile.Svr_Conc_Leader_Follower.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Svr_Conc_Leader_Follower.mak CFG="$(CFG)" all

Svr_Conc_Leader_Follower_RT_CORBA:
	@echo Project: Makefile.Svr_Conc_Leader_Follower_RT_CORBA.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Svr_Conc_Leader_Follower_RT_CORBA.mak CFG="$(CFG)" all

project_name_list:
	@echo Svr_Conc_Leader_Follower
	@echo Svr_Conc_Leader_Follower_RT_CORBA
