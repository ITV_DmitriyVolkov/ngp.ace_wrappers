# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.Synch_Benchmarks_Perf_Test.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INTDIR=Debug\Synch_Benchmarks_Perf_Test\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) ".\Perf_Testd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -f "Makefile.Synch_Benchmarks_Perf_Test.dep" "Benchmark_Performance.cpp" "memory_test.cpp" "Performance_Test.cpp" "adaptive_sema_test.cpp" "pipe_thr_test.cpp" "adaptive_mutex_test.cpp" "rwrd_test.cpp" "pipe_proc_test.cpp" "sysvsema_test.cpp" "context_test.cpp" "condb_test.cpp" "conds_test.cpp" "adaptive_recursive_lock_test.cpp" "mutex_test.cpp" "sema_test.cpp" "rwwr_test.cpp" "guard_test.cpp" "Adaptive_Lock_Performance_Test_Base.cpp" "Performance_Test_Options.cpp" "token_test.cpp" "recursive_lock_test.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\Perf_Testd.pdb"
	-@del /f/q ".\Perf_Testd.dll"
	-@del /f/q "$(OUTDIR)\Perf_Testd.lib"
	-@del /f/q "$(OUTDIR)\Perf_Testd.exp"
	-@del /f/q "$(OUTDIR)\Perf_Testd.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\Synch_Benchmarks_Perf_Test\$(NULL)" mkdir "Debug\Synch_Benchmarks_Perf_Test"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4355 /Fd"$(INTDIR)/" /I "..\..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib Synch_Libd.lib /libpath:"." /libpath:"..\..\..\lib" /libpath:"..\Synch_Lib" /nologo /subsystem:windows /dll /debug /pdb:".\Perf_Testd.pdb" /machine:IA64 /out:".\Perf_Testd.dll" /implib:"$(OUTDIR)\Perf_Testd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Benchmark_Performance.obj" \
	"$(INTDIR)\memory_test.obj" \
	"$(INTDIR)\Performance_Test.obj" \
	"$(INTDIR)\adaptive_sema_test.obj" \
	"$(INTDIR)\pipe_thr_test.obj" \
	"$(INTDIR)\adaptive_mutex_test.obj" \
	"$(INTDIR)\rwrd_test.obj" \
	"$(INTDIR)\pipe_proc_test.obj" \
	"$(INTDIR)\sysvsema_test.obj" \
	"$(INTDIR)\context_test.obj" \
	"$(INTDIR)\condb_test.obj" \
	"$(INTDIR)\conds_test.obj" \
	"$(INTDIR)\adaptive_recursive_lock_test.obj" \
	"$(INTDIR)\mutex_test.obj" \
	"$(INTDIR)\sema_test.obj" \
	"$(INTDIR)\rwwr_test.obj" \
	"$(INTDIR)\guard_test.obj" \
	"$(INTDIR)\Adaptive_Lock_Performance_Test_Base.obj" \
	"$(INTDIR)\Performance_Test_Options.obj" \
	"$(INTDIR)\token_test.obj" \
	"$(INTDIR)\recursive_lock_test.obj"

".\Perf_Testd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist ".\Perf_Testd.dll.manifest" mt.exe -manifest ".\Perf_Testd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=.
INTDIR=Release\Synch_Benchmarks_Perf_Test\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) ".\Perf_Test.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -f "Makefile.Synch_Benchmarks_Perf_Test.dep" "Benchmark_Performance.cpp" "memory_test.cpp" "Performance_Test.cpp" "adaptive_sema_test.cpp" "pipe_thr_test.cpp" "adaptive_mutex_test.cpp" "rwrd_test.cpp" "pipe_proc_test.cpp" "sysvsema_test.cpp" "context_test.cpp" "condb_test.cpp" "conds_test.cpp" "adaptive_recursive_lock_test.cpp" "mutex_test.cpp" "sema_test.cpp" "rwwr_test.cpp" "guard_test.cpp" "Adaptive_Lock_Performance_Test_Base.cpp" "Performance_Test_Options.cpp" "token_test.cpp" "recursive_lock_test.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q ".\Perf_Test.dll"
	-@del /f/q "$(OUTDIR)\Perf_Test.lib"
	-@del /f/q "$(OUTDIR)\Perf_Test.exp"
	-@del /f/q "$(OUTDIR)\Perf_Test.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\Synch_Benchmarks_Perf_Test\$(NULL)" mkdir "Release\Synch_Benchmarks_Perf_Test"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib Synch_Lib.lib /libpath:"." /libpath:"..\..\..\lib" /libpath:"..\Synch_Lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:".\Perf_Test.dll" /implib:"$(OUTDIR)\Perf_Test.lib"
LINK32_OBJS= \
	"$(INTDIR)\Benchmark_Performance.obj" \
	"$(INTDIR)\memory_test.obj" \
	"$(INTDIR)\Performance_Test.obj" \
	"$(INTDIR)\adaptive_sema_test.obj" \
	"$(INTDIR)\pipe_thr_test.obj" \
	"$(INTDIR)\adaptive_mutex_test.obj" \
	"$(INTDIR)\rwrd_test.obj" \
	"$(INTDIR)\pipe_proc_test.obj" \
	"$(INTDIR)\sysvsema_test.obj" \
	"$(INTDIR)\context_test.obj" \
	"$(INTDIR)\condb_test.obj" \
	"$(INTDIR)\conds_test.obj" \
	"$(INTDIR)\adaptive_recursive_lock_test.obj" \
	"$(INTDIR)\mutex_test.obj" \
	"$(INTDIR)\sema_test.obj" \
	"$(INTDIR)\rwwr_test.obj" \
	"$(INTDIR)\guard_test.obj" \
	"$(INTDIR)\Adaptive_Lock_Performance_Test_Base.obj" \
	"$(INTDIR)\Performance_Test_Options.obj" \
	"$(INTDIR)\token_test.obj" \
	"$(INTDIR)\recursive_lock_test.obj"

".\Perf_Test.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist ".\Perf_Test.dll.manifest" mt.exe -manifest ".\Perf_Test.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=.
INTDIR=Static_Debug\Synch_Benchmarks_Perf_Test\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\Perf_Testsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.Synch_Benchmarks_Perf_Test.dep" "Benchmark_Performance.cpp" "memory_test.cpp" "Performance_Test.cpp" "adaptive_sema_test.cpp" "pipe_thr_test.cpp" "adaptive_mutex_test.cpp" "rwrd_test.cpp" "pipe_proc_test.cpp" "sysvsema_test.cpp" "context_test.cpp" "condb_test.cpp" "conds_test.cpp" "adaptive_recursive_lock_test.cpp" "mutex_test.cpp" "sema_test.cpp" "rwwr_test.cpp" "guard_test.cpp" "Adaptive_Lock_Performance_Test_Base.cpp" "Performance_Test_Options.cpp" "token_test.cpp" "recursive_lock_test.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\Perf_Testsd.lib"
	-@del /f/q "$(OUTDIR)\Perf_Testsd.exp"
	-@del /f/q "$(OUTDIR)\Perf_Testsd.ilk"
	-@del /f/q ".\Perf_Testsd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\Synch_Benchmarks_Perf_Test\$(NULL)" mkdir "Static_Debug\Synch_Benchmarks_Perf_Test"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4355 /Fd".\Perf_Testsd.pdb" /I "..\..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:".\Perf_Testsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Benchmark_Performance.obj" \
	"$(INTDIR)\memory_test.obj" \
	"$(INTDIR)\Performance_Test.obj" \
	"$(INTDIR)\adaptive_sema_test.obj" \
	"$(INTDIR)\pipe_thr_test.obj" \
	"$(INTDIR)\adaptive_mutex_test.obj" \
	"$(INTDIR)\rwrd_test.obj" \
	"$(INTDIR)\pipe_proc_test.obj" \
	"$(INTDIR)\sysvsema_test.obj" \
	"$(INTDIR)\context_test.obj" \
	"$(INTDIR)\condb_test.obj" \
	"$(INTDIR)\conds_test.obj" \
	"$(INTDIR)\adaptive_recursive_lock_test.obj" \
	"$(INTDIR)\mutex_test.obj" \
	"$(INTDIR)\sema_test.obj" \
	"$(INTDIR)\rwwr_test.obj" \
	"$(INTDIR)\guard_test.obj" \
	"$(INTDIR)\Adaptive_Lock_Performance_Test_Base.obj" \
	"$(INTDIR)\Performance_Test_Options.obj" \
	"$(INTDIR)\token_test.obj" \
	"$(INTDIR)\recursive_lock_test.obj"

"$(OUTDIR)\Perf_Testsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\Perf_Testsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\Perf_Testsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=.
INTDIR=Static_Release\Synch_Benchmarks_Perf_Test\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\Perf_Tests.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.Synch_Benchmarks_Perf_Test.dep" "Benchmark_Performance.cpp" "memory_test.cpp" "Performance_Test.cpp" "adaptive_sema_test.cpp" "pipe_thr_test.cpp" "adaptive_mutex_test.cpp" "rwrd_test.cpp" "pipe_proc_test.cpp" "sysvsema_test.cpp" "context_test.cpp" "condb_test.cpp" "conds_test.cpp" "adaptive_recursive_lock_test.cpp" "mutex_test.cpp" "sema_test.cpp" "rwwr_test.cpp" "guard_test.cpp" "Adaptive_Lock_Performance_Test_Base.cpp" "Performance_Test_Options.cpp" "token_test.cpp" "recursive_lock_test.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\Perf_Tests.lib"
	-@del /f/q "$(OUTDIR)\Perf_Tests.exp"
	-@del /f/q "$(OUTDIR)\Perf_Tests.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\Synch_Benchmarks_Perf_Test\$(NULL)" mkdir "Static_Release\Synch_Benchmarks_Perf_Test"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:".\Perf_Tests.lib"
LINK32_OBJS= \
	"$(INTDIR)\Benchmark_Performance.obj" \
	"$(INTDIR)\memory_test.obj" \
	"$(INTDIR)\Performance_Test.obj" \
	"$(INTDIR)\adaptive_sema_test.obj" \
	"$(INTDIR)\pipe_thr_test.obj" \
	"$(INTDIR)\adaptive_mutex_test.obj" \
	"$(INTDIR)\rwrd_test.obj" \
	"$(INTDIR)\pipe_proc_test.obj" \
	"$(INTDIR)\sysvsema_test.obj" \
	"$(INTDIR)\context_test.obj" \
	"$(INTDIR)\condb_test.obj" \
	"$(INTDIR)\conds_test.obj" \
	"$(INTDIR)\adaptive_recursive_lock_test.obj" \
	"$(INTDIR)\mutex_test.obj" \
	"$(INTDIR)\sema_test.obj" \
	"$(INTDIR)\rwwr_test.obj" \
	"$(INTDIR)\guard_test.obj" \
	"$(INTDIR)\Adaptive_Lock_Performance_Test_Base.obj" \
	"$(INTDIR)\Performance_Test_Options.obj" \
	"$(INTDIR)\token_test.obj" \
	"$(INTDIR)\recursive_lock_test.obj"

"$(OUTDIR)\Perf_Tests.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\Perf_Tests.lib.manifest" mt.exe -manifest "$(OUTDIR)\Perf_Tests.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Synch_Benchmarks_Perf_Test.dep")
!INCLUDE "Makefile.Synch_Benchmarks_Perf_Test.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Benchmark_Performance.cpp"

"$(INTDIR)\Benchmark_Performance.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Benchmark_Performance.obj" $(SOURCE)

SOURCE="memory_test.cpp"

"$(INTDIR)\memory_test.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\memory_test.obj" $(SOURCE)

SOURCE="Performance_Test.cpp"

"$(INTDIR)\Performance_Test.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Performance_Test.obj" $(SOURCE)

SOURCE="adaptive_sema_test.cpp"

"$(INTDIR)\adaptive_sema_test.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\adaptive_sema_test.obj" $(SOURCE)

SOURCE="pipe_thr_test.cpp"

"$(INTDIR)\pipe_thr_test.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\pipe_thr_test.obj" $(SOURCE)

SOURCE="adaptive_mutex_test.cpp"

"$(INTDIR)\adaptive_mutex_test.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\adaptive_mutex_test.obj" $(SOURCE)

SOURCE="rwrd_test.cpp"

"$(INTDIR)\rwrd_test.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\rwrd_test.obj" $(SOURCE)

SOURCE="pipe_proc_test.cpp"

"$(INTDIR)\pipe_proc_test.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\pipe_proc_test.obj" $(SOURCE)

SOURCE="sysvsema_test.cpp"

"$(INTDIR)\sysvsema_test.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\sysvsema_test.obj" $(SOURCE)

SOURCE="context_test.cpp"

"$(INTDIR)\context_test.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\context_test.obj" $(SOURCE)

SOURCE="condb_test.cpp"

"$(INTDIR)\condb_test.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\condb_test.obj" $(SOURCE)

SOURCE="conds_test.cpp"

"$(INTDIR)\conds_test.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\conds_test.obj" $(SOURCE)

SOURCE="adaptive_recursive_lock_test.cpp"

"$(INTDIR)\adaptive_recursive_lock_test.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\adaptive_recursive_lock_test.obj" $(SOURCE)

SOURCE="mutex_test.cpp"

"$(INTDIR)\mutex_test.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\mutex_test.obj" $(SOURCE)

SOURCE="sema_test.cpp"

"$(INTDIR)\sema_test.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\sema_test.obj" $(SOURCE)

SOURCE="rwwr_test.cpp"

"$(INTDIR)\rwwr_test.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\rwwr_test.obj" $(SOURCE)

SOURCE="guard_test.cpp"

"$(INTDIR)\guard_test.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\guard_test.obj" $(SOURCE)

SOURCE="Adaptive_Lock_Performance_Test_Base.cpp"

"$(INTDIR)\Adaptive_Lock_Performance_Test_Base.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Adaptive_Lock_Performance_Test_Base.obj" $(SOURCE)

SOURCE="Performance_Test_Options.cpp"

"$(INTDIR)\Performance_Test_Options.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Performance_Test_Options.obj" $(SOURCE)

SOURCE="token_test.cpp"

"$(INTDIR)\token_test.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\token_test.obj" $(SOURCE)

SOURCE="recursive_lock_test.cpp"

"$(INTDIR)\recursive_lock_test.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\recursive_lock_test.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Synch_Benchmarks_Perf_Test.dep")
	@echo Using "Makefile.Synch_Benchmarks_Perf_Test.dep"
!ELSE
	@echo Warning: cannot find "Makefile.Synch_Benchmarks_Perf_Test.dep"
!ENDIF
!ENDIF

