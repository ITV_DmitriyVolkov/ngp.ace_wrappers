// $Id: Timeprobe.cpp 14 2007-02-01 15:49:12Z mitza $

#include "ace/config-all.h"

ACE_RCSID(ace, Timeprobe, "$Id: Timeprobe.cpp 14 2007-02-01 15:49:12Z mitza $")

#if defined (ACE_COMPILE_TIMEPROBES)

#include "ace/Timeprobe.h"

#if !defined (__ACE_INLINE__)
#include "ace/Timeprobe.inl"
#endif /* __ACE_INLINE__ */

#endif /* ACE_COMPILE_TIMEPROBES */
