// -*- C++ -*-
// $Id: OS_NS_ctype.cpp 14 2007-02-01 15:49:12Z mitza $

#include "ace/OS_NS_ctype.h"

ACE_RCSID(ace, OS_NS_ctype, "$Id: OS_NS_ctype.cpp 14 2007-02-01 15:49:12Z mitza $")

#if !defined (ACE_HAS_INLINED_OSCALLS)
# include "ace/OS_NS_ctype.inl"
#endif /* ACE_HAS_INLINED_OSCALLS */

