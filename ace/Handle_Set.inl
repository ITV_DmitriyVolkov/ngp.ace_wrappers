// -*- C++ -*-
//
// $Id: Handle_Set.inl 935 2008-12-10 21:47:27Z mitza $

#include "ace/Log_Msg.h"

// todo: This should be cleaned up a bit.
// memset for FD_ZERO on OpenBSD and Solaris w/ gcc 2.95.3
#include "ace/os_include/os_string.h"

// FreeBSD 4.8-RC? for bzero() used by FD_ZERO
#include "ace/os_include/os_strings.h"

// IRIX5 defines bzero() in this odd file... used by FD_ZERO
#if defined (ACE_HAS_BSTRING)
#  include /**/ <bstring.h>
#endif /* ACE_HAS_BSTRING */

// AIX defines bzero() in this odd file... used by FD_ZERO
#if defined (ACE_HAS_STRINGS)
#  include "ace/os_include/os_strings.h"
#endif /* ACE_HAS_STRINGS */

#if defined (ACE_HAS_POLL)
# include <algorithm>
# include <assert.h>
#endif

ACE_BEGIN_VERSIONED_NAMESPACE_DECL

// Initialize the bitmask to all 0s and reset the associated fields.

ACE_INLINE void
ACE_Handle_Set::reset (void)
{
  ACE_TRACE ("ACE_Handle_Set::reset");
  this->max_handle_ =
    ACE_INVALID_HANDLE;

  this->size_ = 0;
#ifdef ACE_HAS_POLL
  //this->handles_.fill(ACE_INVALID_HANDLE);
#else /* ! ACE_HAS_POLL */
#   if defined (ACE_HAS_BIG_FD_SET)
  this->min_handle_ =
    NUM_WORDS * WORDSIZE;
#   endif /* ACE_HAS_BIG_FD_SET */
  // #if !defined (ACE_HAS_BIG_FD_SET)      Why is this here?  -Steve Huston
  FD_ZERO (&this->mask_);
  // #endif /* ACE_HAS_BIG_FD_SET */
#endif /* ! ACE_HAS_POLL */
}

#if !defined(ACE_HAS_POLL) && defined (ACE_HAS_BIG_FD_SET)
ACE_INLINE ACE_Handle_Set &
ACE_Handle_Set::operator = (const ACE_Handle_Set &rhs)
{
  ACE_TRACE ("ACE_Handle_Set::operator =");

  if (rhs.size_ > 0)
    {
      this->size_ =
        rhs.size_;
      this->max_handle_ =
        rhs.max_handle_;
      this->min_handle_ =
        rhs.min_handle_;
      this->mask_ =
        rhs.mask_;
    }
  else
    this->reset ();

  return *this;
}
#endif /* !ACE_HAS_POLL && ACE_HAS_BIG_FD_SET */

// Returns the number of the large bit.

ACE_INLINE ACE_HANDLE
ACE_Handle_Set::max_set (void) const
{
  ACE_TRACE ("ACE_Handle_Set::max_set");
  return this->max_handle_;
}

// Checks whether handle is enabled.

ACE_INLINE int
ACE_Handle_Set::is_set (ACE_HANDLE handle) const
{
  ACE_TRACE ("ACE_Handle_Set::is_set");
#if defined (ACE_HAS_POLL)
  return std::binary_search(this->handles_.begin(), this->handles_.begin() + this->size_, handle);
#elif defined (ACE_HAS_BIG_FD_SET)
  return FD_ISSET (handle,
                   &this->mask_)
    && this->size_ > 0;
#elif defined (ACE_HAS_NONCONST_FD_ISSET)
  return FD_ISSET (handle,
                   const_cast<fd_set*> (&this->mask_));
#else
  return FD_ISSET (handle,
                   &this->mask_);
#endif /* ACE_HAS_BIG_FD_SET */
}

// Enables the handle.

ACE_INLINE void
ACE_Handle_Set::set_bit (ACE_HANDLE handle)
{
  ACE_TRACE ("ACE_Handle_Set::set_bit");
  if ((handle != ACE_INVALID_HANDLE)
#if !defined (ACE_HAS_POLL)
      && (!this->is_set (handle))
#endif 
      )
    {
#if defined (ACE_WIN32) && !defined (ACE_HAS_POLL)
      FD_SET ((SOCKET) handle,
              &this->mask_);
      ++this->size_;
#else /* ACE_WIN32 && !ACE_HAS_POLL */
# if defined (ACE_HAS_POLL)
      auto const end = this->handles_.begin() + this->size_;
      auto equal_range = std::equal_range(this->handles_.begin(), end, handle);
      bool const already_in = equal_range.first != equal_range.second;
      if (already_in)
          return;
      const bool ACE_Handle_Set_is_full = MAXSIZE <= this->size_;
      if (ACE_Handle_Set_is_full)
      {
          assert(!ACE_Handle_Set_is_full);
          abort();
      }
      memmove( &*(equal_range.second + 1), &*equal_range.second, sizeof(ACE_HANDLE) * (end - equal_range.second) );
      *equal_range.second = handle;

# else /* !ACE_HAS_POLL */
#   if defined (ACE_HAS_BIG_FD_SET)
      if (this->size_ == 0)
        FD_ZERO (&this->mask_);

      if (handle < this->min_handle_)
        this->min_handle_ = handle;
#   endif /* ACE_HAS_BIG_FD_SET */

      FD_SET (handle,
              &this->mask_);
# endif /* !ACE_HAS_POLL */
      ++this->size_;

      if (handle > this->max_handle_)
        this->max_handle_ = handle;
#endif /* ACE_WIN32 */
    }
}

// Disables the handle.

ACE_INLINE void
ACE_Handle_Set::clr_bit (ACE_HANDLE handle)
{
  ACE_TRACE ("ACE_Handle_Set::clr_bit");

#if defined (ACE_HAS_POLL)
  if (ACE_INVALID_HANDLE == handle || 0 == this->size_)
      return;
  auto const end = this->handles_.begin() + this->size_;
  auto equal_range = std::equal_range(this->handles_.begin(), end, handle);
  bool const exists = equal_range.first != equal_range.second;
  if (!exists)
      return;
  memmove( &*equal_range.first, &*equal_range.second, sizeof(ACE_HANDLE) * (end - equal_range.second) );
  this->size_ -= equal_range.second - equal_range.first;

#else /* !ACE_HAS_POLL */
  if ((handle != ACE_INVALID_HANDLE) && 
       (this->is_set (handle)))
    {
      FD_CLR ((ACE_SOCKET) handle,
              &this->mask_);
      --this->size_;

#if !defined (ACE_WIN32)
      if (handle == this->max_handle_)
        this->set_max (this->max_handle_);
#endif /* !ACE_WIN32 */
    }
#endif /* ! ACE_HAS_POLL */
}

// Returns a count of the number of enabled bits.

ACE_INLINE int
ACE_Handle_Set::num_set (void) const
{
  ACE_TRACE ("ACE_Handle_Set::num_set");
#if defined (ACE_WIN32) && !defined (ACE_HAS_POLL)
  return this->mask_.fd_count;
#else /* !ACE_WIN32 || ACE_HAS_POLL */
  return this->size_;
#endif /* ACE_WIN32 && !ACE_HAS_POLL */
}

#if !defined (ACE_HAS_POLL)
// Returns a pointer to the underlying fd_set.

ACE_INLINE
ACE_Handle_Set::operator fd_set *()
{
  ACE_TRACE ("ACE_Handle_Set::operator fd_set *");

  if (this->size_ > 0)
    return (fd_set *) &this->mask_;
  else
    return (fd_set *) 0;
}

// Returns a pointer to the underlying fd_set.

ACE_INLINE fd_set *
ACE_Handle_Set::fdset (void)
{
  ACE_TRACE ("ACE_Handle_Set::fdset");

  if (this->size_ > 0)
    return (fd_set *) &this->mask_;
  else
    return (fd_set *) 0;
}

#endif /* !ACE_HAS_POLL */

ACE_INLINE
ACE_Handle_Set_Iterator::~ACE_Handle_Set_Iterator (void)
{
}

ACE_END_VERSIONED_NAMESPACE_DECL
