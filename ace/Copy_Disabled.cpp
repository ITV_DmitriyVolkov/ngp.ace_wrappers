/**
 * @file Copy_Disabled.cpp
 *
 * $Id: Copy_Disabled.cpp 14 2007-02-01 15:49:12Z mitza $
 *
 * @author Carlos O'Ryan <coryan@uci.edu>
 */

#include "ace/Copy_Disabled.h"


ACE_RCSID (ace,
           Copy_Disabled,
           "$Id: Copy_Disabled.cpp 14 2007-02-01 15:49:12Z mitza $")


ACE_BEGIN_VERSIONED_NAMESPACE_DECL

ACE_Copy_Disabled::ACE_Copy_Disabled (void)
{
}

ACE_END_VERSIONED_NAMESPACE_DECL
