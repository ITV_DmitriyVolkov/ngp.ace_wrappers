// $Id: OS_NS_sys_resource.cpp 14 2007-02-01 15:49:12Z mitza $

#include "ace/OS_NS_sys_resource.h"

ACE_RCSID(ace, OS_NS_sys_resource, "$Id: OS_NS_sys_resource.cpp 14 2007-02-01 15:49:12Z mitza $")

#if !defined (ACE_HAS_INLINED_OSCALLS)
# include "ace/OS_NS_sys_resource.inl"
#endif /* ACE_HAS_INLINED_OSCALLS */

