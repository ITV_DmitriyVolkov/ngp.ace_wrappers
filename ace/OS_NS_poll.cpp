// $Id: OS_NS_poll.cpp 14 2007-02-01 15:49:12Z mitza $

#include "ace/OS_NS_poll.h"

ACE_RCSID(ace, OS_NS_poll, "$Id: OS_NS_poll.cpp 14 2007-02-01 15:49:12Z mitza $")

#if !defined (ACE_HAS_INLINED_OSCALLS)
# include "ace/OS_NS_poll.inl"
#endif /* ACE_HAS_INLINED_OSCALLS */

