// Date_Time.cpp
// $Id: Date_Time.cpp 14 2007-02-01 15:49:12Z mitza $

#include "ace/Date_Time.h"

#if !defined (__ACE_INLINE__)
#include "ace/Date_Time.inl"
#endif /* __ACE_INLINE__ */

ACE_RCSID(ace, Date_Time, "$Id: Date_Time.cpp 14 2007-02-01 15:49:12Z mitza $")
