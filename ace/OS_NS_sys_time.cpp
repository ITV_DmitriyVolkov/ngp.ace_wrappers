// $Id: OS_NS_sys_time.cpp 14 2007-02-01 15:49:12Z mitza $

#include "ace/OS_NS_sys_time.h"

ACE_RCSID(ace, OS_NS_sys_time, "$Id: OS_NS_sys_time.cpp 14 2007-02-01 15:49:12Z mitza $")

#if !defined (ACE_HAS_INLINED_OSCALLS)
# include "ace/OS_NS_sys_time.inl"
#endif /* ACE_HAS_INLINED_OSCALLS */

