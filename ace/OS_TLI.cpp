// $Id: OS_TLI.cpp 14 2007-02-01 15:49:12Z mitza $

#include "ace/OS_TLI.h"

ACE_RCSID(ace, OS_TLI, "$Id: OS_TLI.cpp 14 2007-02-01 15:49:12Z mitza $")

#if !defined (ACE_HAS_INLINED_OSCALLS)
# include "ace/OS_TLI.inl"
#endif /* !ACE_HAS_INLINED_OSCALLS */
