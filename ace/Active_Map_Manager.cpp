// $Id: Active_Map_Manager.cpp 14 2007-02-01 15:49:12Z mitza $

#include "ace/Active_Map_Manager.h"

ACE_RCSID(ace, Active_Map_Manager, "$Id: Active_Map_Manager.cpp 14 2007-02-01 15:49:12Z mitza $")

#if !defined (__ACE_INLINE__)
#include "ace/Active_Map_Manager.inl"
#endif /* __ACE_INLINE__ */
