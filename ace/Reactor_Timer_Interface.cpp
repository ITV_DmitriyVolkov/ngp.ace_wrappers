// $Id: Reactor_Timer_Interface.cpp 14 2007-02-01 15:49:12Z mitza $

#include "ace/Reactor_Timer_Interface.h"

ACE_RCSID (ace,
           Reactor_Timer_Interface,
           "$Id: Reactor_Timer_Interface.cpp 14 2007-02-01 15:49:12Z mitza $")


ACE_BEGIN_VERSIONED_NAMESPACE_DECL

ACE_Reactor_Timer_Interface::~ACE_Reactor_Timer_Interface()
{
}

ACE_END_VERSIONED_NAMESPACE_DECL
