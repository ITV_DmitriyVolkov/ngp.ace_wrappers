// $Id: OS_NS_sys_msg.cpp 14 2007-02-01 15:49:12Z mitza $

#include "ace/OS_NS_sys_msg.h"

ACE_RCSID(ace, OS_NS_sys_msg, "$Id: OS_NS_sys_msg.cpp 14 2007-02-01 15:49:12Z mitza $")

#if !defined (ACE_HAS_INLINED_OSCALLS)
# include "ace/OS_NS_sys_msg.inl"
#endif /* ACE_HAS_INLINED_OSCALLS */

