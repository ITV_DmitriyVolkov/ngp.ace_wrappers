// -*- C++ -*-
//
// $Id: OS_NS_sys_select.inl 935 2008-12-10 21:47:27Z mitza $

#include "ace/OS_NS_errno.h"
#include "ace/OS_NS_macros.h"
#include "ace/Time_Value.h"

#if defined(ACE_HAS_POLL)
# include "ace/OS_NS_poll.h"
# include <algorithm>
# include <memory>
/// @see http://man7.org/linux/man-pages/man2/select.2.html
#define POLLIN_SET (POLLRDNORM | POLLRDBAND | POLLIN | POLLHUP | POLLERR) /* Ready for reading */
#define POLLOUT_SET (POLLWRBAND | POLLWRNORM | POLLOUT | POLLERR) /* Ready for writing */
#define POLLEX_SET (POLLPRI) /* Exceptional condition */
#endif

ACE_BEGIN_VERSIONED_NAMESPACE_DECL

#if defined (ACE_HAS_POLL)
ACE_INLINE int
ACE_OS::select (int ignored,
                ACE_Handle_Set& rfds)
{
    ACE_Handle_Set wfds;
    return ACE_OS::select(ignored, rfds, wfds);
}

ACE_INLINE int
ACE_OS::select (int ignored,
                ACE_Handle_Set& rfds,
                ACE_Handle_Set& wfds)
{
    ACE_Handle_Set efds;
    return ACE_OS::select(ignored, rfds, wfds, efds);
}

ACE_INLINE int
ACE_OS::select (int ignored,
                ACE_Handle_Set& rfds,
                ACE_Handle_Set& wfds,
                ACE_Handle_Set& efds,
                const ACE_Time_Value &tv)
{
    return ACE_OS::select(ignored, rfds, wfds, efds, &tv);
}

#else /* !ACE_HAS_POLL */
// It would be really cool to add another version of select that would
// function like the one we're defending against below!
ACE_INLINE int
ACE_OS::select (int width,
                fd_set *rfds, fd_set *wfds, fd_set *efds,
                const ACE_Time_Value *timeout)
{
  ACE_OS_TRACE ("ACE_OS::select");
#if defined (ACE_HAS_NONCONST_SELECT_TIMEVAL)
  // We must defend against non-conformity!
  timeval copy;
  timeval *timep = 0;

  if (timeout != 0)
    {
      copy = *timeout;
      timep = &copy;
    }
  else
    timep = 0;
#else
  const timeval *timep = (timeout == 0 ? (const timeval *)0 : *timeout);
#endif /* ACE_HAS_NONCONST_SELECT_TIMEVAL */
#if defined (ACE_LACKS_SELECT)
  ACE_UNUSED_ARG (width);
  ACE_UNUSED_ARG (rfds);
  ACE_UNUSED_ARG (wfds);
  ACE_UNUSED_ARG (efds);
  ACE_UNUSED_ARG (timeout);
  ACE_NOTSUP_RETURN (-1);
#elif defined(ACE_TANDEM_T1248_PTHREADS)
  ACE_SOCKCALL_RETURN (::spt_select (width, rfds, wfds, efds, timep),
                       int, -1);
#else
  ACE_SOCKCALL_RETURN (::select (width, rfds, wfds, efds, timep),
                       int, -1);
#endif
}

ACE_INLINE int
ACE_OS::select (int width,
                fd_set *rfds, fd_set *wfds, fd_set *efds,
                const ACE_Time_Value &timeout)
{
  ACE_OS_TRACE ("ACE_OS::select");
#if defined (ACE_HAS_NONCONST_SELECT_TIMEVAL)
# define ___ACE_TIMEOUT &copy
  timeval copy = timeout;
#else
# define ___ACE_TIMEOUT timep
  const timeval *timep = timeout;
#endif /* ACE_HAS_NONCONST_SELECT_TIMEVAL */
#if defined (ACE_LACKS_SELECT)
  ACE_UNUSED_ARG (width);
  ACE_UNUSED_ARG (rfds);
  ACE_UNUSED_ARG (wfds);
  ACE_UNUSED_ARG (efds);
  ACE_UNUSED_ARG (timeout);
  ACE_NOTSUP_RETURN (-1);
#elif defined(ACE_TANDEM_T1248_PTHREADS)
  ACE_SOCKCALL_RETURN (::spt_select (width, rfds, wfds, efds, ___ACE_TIMEOUT),
                       int, -1);
#else
  ACE_SOCKCALL_RETURN (::select (width, rfds, wfds, efds, ___ACE_TIMEOUT),
                       int, -1);
#endif
#undef ___ACE_TIMEOUT
}

#endif /* !ACE_HAS_POLL */
ACE_END_VERSIONED_NAMESPACE_DECL
