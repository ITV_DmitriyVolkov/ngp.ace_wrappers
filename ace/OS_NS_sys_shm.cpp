// $Id: OS_NS_sys_shm.cpp 14 2007-02-01 15:49:12Z mitza $

#include "ace/OS_NS_sys_shm.h"

ACE_RCSID(ace, OS_NS_sys_shm, "$Id: OS_NS_sys_shm.cpp 14 2007-02-01 15:49:12Z mitza $")

#if !defined (ACE_HAS_INLINED_OSCALLS)
# include "ace/OS_NS_sys_shm.inl"
#endif /* ACE_HAS_INLINED_OSCALLS */

