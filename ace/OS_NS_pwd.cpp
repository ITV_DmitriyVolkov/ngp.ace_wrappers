// $Id: OS_NS_pwd.cpp 14 2007-02-01 15:49:12Z mitza $

#include "ace/OS_NS_pwd.h"

ACE_RCSID(ace, OS_NS_pwd, "$Id: OS_NS_pwd.cpp 14 2007-02-01 15:49:12Z mitza $")

#if !defined (ACE_HAS_INLINED_OSCALLS)
# include "ace/OS_NS_pwd.inl"
#endif /* ACE_HAS_INLINED_OSCALLS */

