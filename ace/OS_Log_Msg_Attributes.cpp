// $Id: OS_Log_Msg_Attributes.cpp 14 2007-02-01 15:49:12Z mitza $

#include "ace/OS_Log_Msg_Attributes.h"

#if !defined (ACE_HAS_INLINED_OSCALLS)
# include "ace/OS_Log_Msg_Attributes.inl"
#endif /* ACE_HAS_INLINED_OSCALLS */

ACE_RCSID(ace, OS_Log_Msg_Attributes, "$Id: OS_Log_Msg_Attributes.cpp 14 2007-02-01 15:49:12Z mitza $")
