// $Id: OS_NS_sys_select.cpp 14 2007-02-01 15:49:12Z mitza $

#include "ace/OS_NS_sys_select.h"

ACE_RCSID(ace, OS_NS_sys_select, "$Id: OS_NS_sys_select.cpp 14 2007-02-01 15:49:12Z mitza $")

#if !defined (ACE_HAS_INLINED_OSCALLS)
# include "ace/OS_NS_sys_select.inl"
#endif /* ACE_HAS_INLINED_OSCALLS */

#if defined (ACE_HAS_POLL)
#include <limits>
#include <type_traits>

ACE_BEGIN_VERSIONED_NAMESPACE_DECL

namespace {
using events_mask_t = decltype(pollfd::events);
struct HandleSetContext
{
    ACE_Handle_Set_Iterator iter;
    ACE_HANDLE handle;
    events_mask_t events;

    HandleSetContext(ACE_Handle_Set& fds, events_mask_t mask)
        : iter( fds )
        , handle( iter() )
        , events( mask )
    {}

    bool operator < (HandleSetContext const& rhs) const
    {
        using unsigned_handle = std::make_unsigned<ACE_HANDLE>::type;
        static_assert(unsigned_handle(ACE_INVALID_HANDLE) == std::numeric_limits<unsigned_handle>::max(), "cannot treat ACE_HANDLE as unsigned for comparison");
        return unsigned_handle(handle) < unsigned_handle(rhs.handle);
    }

    bool valid() const
    {
        return ACE_INVALID_HANDLE != handle;
    }

    void next()
    {
        handle = iter();
    }
};

} // anonymous namespace

/*ACE_INLINE*/  int
ACE_OS::select (int ,
                ACE_Handle_Set& rfds,
                ACE_Handle_Set& wfds,
                ACE_Handle_Set& efds,
                const ACE_Time_Value *tv)
{
    ACE_OS_TRACE ("ACE_OS::select[poll]");
    pollfd fdbuf[ ACE_Handle_Set::MAXSIZE * 3 ];
    size_t fdbuf_count = 0u;
    HandleSetContext ctx[3] = { HandleSetContext(rfds, POLLIN_SET), HandleSetContext(wfds, POLLOUT_SET), HandleSetContext(efds, POLLEX_SET) };
    for ( HandleSetContext* minHandleIt = nullptr; (minHandleIt = std::min_element(ctx, ctx + 3))->valid(); minHandleIt->next() )
    {
        if (fdbuf_count > 0 && fdbuf[fdbuf_count - 1].fd == minHandleIt->handle)
        {
            fdbuf[fdbuf_count - 1].events |= minHandleIt->events;
        }
        else
        {
            if (fdbuf_count >= sizeof(fdbuf)/sizeof(fdbuf[0]))
            {
                assert(false);
                abort();
            }
            fdbuf[fdbuf_count].fd = minHandleIt->handle;
            fdbuf[fdbuf_count].events = minHandleIt->events;
            fdbuf[fdbuf_count].revents = 0;
            ++fdbuf_count;
        }
    }
    auto status = ACE_OS::poll(fdbuf, fdbuf_count, tv);
    if (-1 != status)
    {
        if (auto nrEvents = status)
        {
            ACE_Handle_Set out_rfds, out_wfds, out_efds;

            for ( size_t i = 0; i < fdbuf_count; ++i )
            {
                if (0 == fdbuf[i].revents)
                    continue;
                if (fdbuf[i].revents & POLLNVAL)
                {
                    status = -1;
                    errno = EBADF;
                    break;
                }
                if (fdbuf[i].revents & POLLERR)
                {
                    if (fdbuf[i].events & POLLIN)
                        out_rfds.set_bit(fdbuf[i].fd);
                    if (fdbuf[i].events & POLLOUT)
                        out_wfds.set_bit(fdbuf[i].fd);
                }
                else
                {
                    if (fdbuf[i].revents & POLLIN_SET)
                        out_rfds.set_bit(fdbuf[i].fd);
                    if (fdbuf[i].revents & POLLOUT_SET)
                        out_wfds.set_bit(fdbuf[i].fd);
                    if (fdbuf[i].revents & POLLEX_SET)
                        out_efds.set_bit(fdbuf[i].fd);
                }
                if (0 == --nrEvents)
                    break;
            }
            if (-1 != status)
            {
                rfds = out_rfds;
                wfds = out_wfds;
                efds = out_efds;
            }
        }
        else
        {
            rfds.reset();
            wfds.reset();
            efds.reset();
        }
    }
    return status;
}


ACE_END_VERSIONED_NAMESPACE_DECL
#endif /* ACE_HAS_POLL */

