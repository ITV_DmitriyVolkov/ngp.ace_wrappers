// $Id: xlc_dummy.cpp 935 2008-12-10 21:47:27Z mitza $
//
// This file exists only to help with template instantiation when building
// shared libraries on AIX using C Set++.  See rules.lib.GNU for usage.

/* FUZZ: disable check_for_improper_main_declaration */

main() {}
