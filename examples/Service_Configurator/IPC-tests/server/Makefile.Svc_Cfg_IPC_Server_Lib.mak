# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.Svc_Cfg_IPC_Server_Lib.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INTDIR=Debug\Svc_Cfg_IPC_Server_Lib\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) ".\IPC_Tests_Serverd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_BUILD_SVC_DLL -f "Makefile.Svc_Cfg_IPC_Server_Lib.dep" "Handle_Broadcast.cpp" "Handle_L_CODgram.cpp" "Handle_L_Dgram.cpp" "Handle_L_FIFO.cpp" "Handle_L_Pipe.cpp" "Handle_L_SPIPE.cpp" "Handle_L_Stream.cpp" "Handle_R_Stream.cpp" "Handle_Thr_Stream.cpp" "Handle_Timeout.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\IPC_Tests_Serverd.pdb"
	-@del /f/q ".\IPC_Tests_Serverd.dll"
	-@del /f/q "$(OUTDIR)\IPC_Tests_Serverd.lib"
	-@del /f/q "$(OUTDIR)\IPC_Tests_Serverd.exp"
	-@del /f/q "$(OUTDIR)\IPC_Tests_Serverd.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\Svc_Cfg_IPC_Server_Lib\$(NULL)" mkdir "Debug\Svc_Cfg_IPC_Server_Lib"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4355 /Fd"$(INTDIR)/" /I "..\..\..\.." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_BUILD_SVC_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:".\IPC_Tests_Serverd.pdb" /machine:IA64 /out:".\IPC_Tests_Serverd.dll" /implib:"$(OUTDIR)\IPC_Tests_Serverd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Handle_Broadcast.obj" \
	"$(INTDIR)\Handle_L_CODgram.obj" \
	"$(INTDIR)\Handle_L_Dgram.obj" \
	"$(INTDIR)\Handle_L_FIFO.obj" \
	"$(INTDIR)\Handle_L_Pipe.obj" \
	"$(INTDIR)\Handle_L_SPIPE.obj" \
	"$(INTDIR)\Handle_L_Stream.obj" \
	"$(INTDIR)\Handle_R_Stream.obj" \
	"$(INTDIR)\Handle_Thr_Stream.obj" \
	"$(INTDIR)\Handle_Timeout.obj"

".\IPC_Tests_Serverd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist ".\IPC_Tests_Serverd.dll.manifest" mt.exe -manifest ".\IPC_Tests_Serverd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=.
INTDIR=Release\Svc_Cfg_IPC_Server_Lib\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) ".\IPC_Tests_Server.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_BUILD_SVC_DLL -f "Makefile.Svc_Cfg_IPC_Server_Lib.dep" "Handle_Broadcast.cpp" "Handle_L_CODgram.cpp" "Handle_L_Dgram.cpp" "Handle_L_FIFO.cpp" "Handle_L_Pipe.cpp" "Handle_L_SPIPE.cpp" "Handle_L_Stream.cpp" "Handle_R_Stream.cpp" "Handle_Thr_Stream.cpp" "Handle_Timeout.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q ".\IPC_Tests_Server.dll"
	-@del /f/q "$(OUTDIR)\IPC_Tests_Server.lib"
	-@del /f/q "$(OUTDIR)\IPC_Tests_Server.exp"
	-@del /f/q "$(OUTDIR)\IPC_Tests_Server.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\Svc_Cfg_IPC_Server_Lib\$(NULL)" mkdir "Release\Svc_Cfg_IPC_Server_Lib"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\..\..\.." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_BUILD_SVC_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:".\IPC_Tests_Server.dll" /implib:"$(OUTDIR)\IPC_Tests_Server.lib"
LINK32_OBJS= \
	"$(INTDIR)\Handle_Broadcast.obj" \
	"$(INTDIR)\Handle_L_CODgram.obj" \
	"$(INTDIR)\Handle_L_Dgram.obj" \
	"$(INTDIR)\Handle_L_FIFO.obj" \
	"$(INTDIR)\Handle_L_Pipe.obj" \
	"$(INTDIR)\Handle_L_SPIPE.obj" \
	"$(INTDIR)\Handle_L_Stream.obj" \
	"$(INTDIR)\Handle_R_Stream.obj" \
	"$(INTDIR)\Handle_Thr_Stream.obj" \
	"$(INTDIR)\Handle_Timeout.obj"

".\IPC_Tests_Server.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist ".\IPC_Tests_Server.dll.manifest" mt.exe -manifest ".\IPC_Tests_Server.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=.
INTDIR=Static_Debug\Svc_Cfg_IPC_Server_Lib\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\IPC_Tests_Serversd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.Svc_Cfg_IPC_Server_Lib.dep" "Handle_Broadcast.cpp" "Handle_L_CODgram.cpp" "Handle_L_Dgram.cpp" "Handle_L_FIFO.cpp" "Handle_L_Pipe.cpp" "Handle_L_SPIPE.cpp" "Handle_L_Stream.cpp" "Handle_R_Stream.cpp" "Handle_Thr_Stream.cpp" "Handle_Timeout.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\IPC_Tests_Serversd.lib"
	-@del /f/q "$(OUTDIR)\IPC_Tests_Serversd.exp"
	-@del /f/q "$(OUTDIR)\IPC_Tests_Serversd.ilk"
	-@del /f/q ".\IPC_Tests_Serversd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\Svc_Cfg_IPC_Server_Lib\$(NULL)" mkdir "Static_Debug\Svc_Cfg_IPC_Server_Lib"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4355 /Fd".\IPC_Tests_Serversd.pdb" /I "..\..\..\.." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:".\IPC_Tests_Serversd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Handle_Broadcast.obj" \
	"$(INTDIR)\Handle_L_CODgram.obj" \
	"$(INTDIR)\Handle_L_Dgram.obj" \
	"$(INTDIR)\Handle_L_FIFO.obj" \
	"$(INTDIR)\Handle_L_Pipe.obj" \
	"$(INTDIR)\Handle_L_SPIPE.obj" \
	"$(INTDIR)\Handle_L_Stream.obj" \
	"$(INTDIR)\Handle_R_Stream.obj" \
	"$(INTDIR)\Handle_Thr_Stream.obj" \
	"$(INTDIR)\Handle_Timeout.obj"

"$(OUTDIR)\IPC_Tests_Serversd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\IPC_Tests_Serversd.lib.manifest" mt.exe -manifest "$(OUTDIR)\IPC_Tests_Serversd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=.
INTDIR=Static_Release\Svc_Cfg_IPC_Server_Lib\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\IPC_Tests_Servers.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.Svc_Cfg_IPC_Server_Lib.dep" "Handle_Broadcast.cpp" "Handle_L_CODgram.cpp" "Handle_L_Dgram.cpp" "Handle_L_FIFO.cpp" "Handle_L_Pipe.cpp" "Handle_L_SPIPE.cpp" "Handle_L_Stream.cpp" "Handle_R_Stream.cpp" "Handle_Thr_Stream.cpp" "Handle_Timeout.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\IPC_Tests_Servers.lib"
	-@del /f/q "$(OUTDIR)\IPC_Tests_Servers.exp"
	-@del /f/q "$(OUTDIR)\IPC_Tests_Servers.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\Svc_Cfg_IPC_Server_Lib\$(NULL)" mkdir "Static_Release\Svc_Cfg_IPC_Server_Lib"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\..\..\.." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:".\IPC_Tests_Servers.lib"
LINK32_OBJS= \
	"$(INTDIR)\Handle_Broadcast.obj" \
	"$(INTDIR)\Handle_L_CODgram.obj" \
	"$(INTDIR)\Handle_L_Dgram.obj" \
	"$(INTDIR)\Handle_L_FIFO.obj" \
	"$(INTDIR)\Handle_L_Pipe.obj" \
	"$(INTDIR)\Handle_L_SPIPE.obj" \
	"$(INTDIR)\Handle_L_Stream.obj" \
	"$(INTDIR)\Handle_R_Stream.obj" \
	"$(INTDIR)\Handle_Thr_Stream.obj" \
	"$(INTDIR)\Handle_Timeout.obj"

"$(OUTDIR)\IPC_Tests_Servers.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\IPC_Tests_Servers.lib.manifest" mt.exe -manifest "$(OUTDIR)\IPC_Tests_Servers.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Svc_Cfg_IPC_Server_Lib.dep")
!INCLUDE "Makefile.Svc_Cfg_IPC_Server_Lib.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Handle_Broadcast.cpp"

"$(INTDIR)\Handle_Broadcast.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Handle_Broadcast.obj" $(SOURCE)

SOURCE="Handle_L_CODgram.cpp"

"$(INTDIR)\Handle_L_CODgram.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Handle_L_CODgram.obj" $(SOURCE)

SOURCE="Handle_L_Dgram.cpp"

"$(INTDIR)\Handle_L_Dgram.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Handle_L_Dgram.obj" $(SOURCE)

SOURCE="Handle_L_FIFO.cpp"

"$(INTDIR)\Handle_L_FIFO.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Handle_L_FIFO.obj" $(SOURCE)

SOURCE="Handle_L_Pipe.cpp"

"$(INTDIR)\Handle_L_Pipe.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Handle_L_Pipe.obj" $(SOURCE)

SOURCE="Handle_L_SPIPE.cpp"

"$(INTDIR)\Handle_L_SPIPE.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Handle_L_SPIPE.obj" $(SOURCE)

SOURCE="Handle_L_Stream.cpp"

"$(INTDIR)\Handle_L_Stream.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Handle_L_Stream.obj" $(SOURCE)

SOURCE="Handle_R_Stream.cpp"

"$(INTDIR)\Handle_R_Stream.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Handle_R_Stream.obj" $(SOURCE)

SOURCE="Handle_Thr_Stream.cpp"

"$(INTDIR)\Handle_Thr_Stream.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Handle_Thr_Stream.obj" $(SOURCE)

SOURCE="Handle_Timeout.cpp"

"$(INTDIR)\Handle_Timeout.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Handle_Timeout.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Svc_Cfg_IPC_Server_Lib.dep")
	@echo Using "Makefile.Svc_Cfg_IPC_Server_Lib.dep"
!ELSE
	@echo Warning: cannot find "Makefile.Svc_Cfg_IPC_Server_Lib.dep"
!ENDIF
!ENDIF

