# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.Log_Msg_MFC.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Console Application")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INSTALLDIR=.
INTDIR=Debug\Log_Msg_MFC\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\Log_Msg_MFC.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -D_AFXDLL -DACE_HAS_MFC=1 -f "Makefile.Log_Msg_MFC.dep" "Log_Msg_MFC.cpp" "Log_Msg_MFCDlg.cpp" "MFC_Log.cpp" "StdAfx.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\Log_Msg_MFC.pdb"
	-@del /f/q "$(INSTALLDIR)\Log_Msg_MFC.exe"
	-@del /f/q "$(INSTALLDIR)\Log_Msg_MFC.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\Log_Msg_MFC\$(NULL)" mkdir "Debug\Log_Msg_MFC"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4355 /Fd"$(INTDIR)/" /I "..\..\.." /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D _AFXDLL /D ACE_HAS_MFC=1 /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /debug /pdb:"$(INSTALLDIR)\Log_Msg_MFC.pdb" /machine:IA64 /out:"$(INSTALLDIR)\Log_Msg_MFC.exe"
LINK32_OBJS= \
	"$(INTDIR)\Log_Msg_MFC.res" \
	"$(INTDIR)\Log_Msg_MFC.obj" \
	"$(INTDIR)\Log_Msg_MFCDlg.obj" \
	"$(INTDIR)\MFC_Log.obj" \
	"$(INTDIR)\StdAfx.obj"

"$(INSTALLDIR)\Log_Msg_MFC.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\Log_Msg_MFC.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\Log_Msg_MFC.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=Release
INSTALLDIR=Release
INTDIR=Release\Log_Msg_MFC\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\Log_Msg_MFC.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -D_AFXDLL -DACE_HAS_MFC=1 -f "Makefile.Log_Msg_MFC.dep" "Log_Msg_MFC.cpp" "Log_Msg_MFCDlg.cpp" "MFC_Log.cpp" "StdAfx.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\Log_Msg_MFC.exe"
	-@del /f/q "$(INSTALLDIR)\Log_Msg_MFC.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\Log_Msg_MFC\$(NULL)" mkdir "Release\Log_Msg_MFC"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\..\.." /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D _AFXDLL /D ACE_HAS_MFC=1  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows  /machine:IA64 /out:"$(INSTALLDIR)\Log_Msg_MFC.exe"
LINK32_OBJS= \
	"$(INTDIR)\Log_Msg_MFC.res" \
	"$(INTDIR)\Log_Msg_MFC.obj" \
	"$(INTDIR)\Log_Msg_MFCDlg.obj" \
	"$(INTDIR)\MFC_Log.obj" \
	"$(INTDIR)\StdAfx.obj"

"$(INSTALLDIR)\Log_Msg_MFC.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\Log_Msg_MFC.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\Log_Msg_MFC.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=Static_Debug
INSTALLDIR=Static_Debug
INTDIR=Static_Debug\Log_Msg_MFC\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\Log_Msg_MFC.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -D_AFXDLL -DACE_HAS_MFC=1 -DACE_AS_STATIC_LIBS -f "Makefile.Log_Msg_MFC.dep" "Log_Msg_MFC.cpp" "Log_Msg_MFCDlg.cpp" "MFC_Log.cpp" "StdAfx.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\Log_Msg_MFC.pdb"
	-@del /f/q "$(INSTALLDIR)\Log_Msg_MFC.exe"
	-@del /f/q "$(INSTALLDIR)\Log_Msg_MFC.ilk"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\Log_Msg_MFC\$(NULL)" mkdir "Static_Debug\Log_Msg_MFC"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4355 /Fd"$(INTDIR)/" /I "..\..\.." /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D _AFXDLL /D ACE_HAS_MFC=1 /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEsd.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /debug /pdb:"$(INSTALLDIR)\Log_Msg_MFC.pdb" /machine:IA64 /out:"$(INSTALLDIR)\Log_Msg_MFC.exe"
LINK32_OBJS= \
	"$(INTDIR)\Log_Msg_MFC.res" \
	"$(INTDIR)\Log_Msg_MFC.obj" \
	"$(INTDIR)\Log_Msg_MFCDlg.obj" \
	"$(INTDIR)\MFC_Log.obj" \
	"$(INTDIR)\StdAfx.obj"

"$(INSTALLDIR)\Log_Msg_MFC.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\Log_Msg_MFC.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\Log_Msg_MFC.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=Static_Release
INSTALLDIR=Static_Release
INTDIR=Static_Release\Log_Msg_MFC\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\Log_Msg_MFC.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -D_AFXDLL -DACE_HAS_MFC=1 -DACE_AS_STATIC_LIBS -f "Makefile.Log_Msg_MFC.dep" "Log_Msg_MFC.cpp" "Log_Msg_MFCDlg.cpp" "MFC_Log.cpp" "StdAfx.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\Log_Msg_MFC.exe"
	-@del /f/q "$(INSTALLDIR)\Log_Msg_MFC.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\Log_Msg_MFC\$(NULL)" mkdir "Static_Release\Log_Msg_MFC"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\..\.." /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D _AFXDLL /D ACE_HAS_MFC=1 /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEs.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows  /machine:IA64 /out:"$(INSTALLDIR)\Log_Msg_MFC.exe"
LINK32_OBJS= \
	"$(INTDIR)\Log_Msg_MFC.res" \
	"$(INTDIR)\Log_Msg_MFC.obj" \
	"$(INTDIR)\Log_Msg_MFCDlg.obj" \
	"$(INTDIR)\MFC_Log.obj" \
	"$(INTDIR)\StdAfx.obj"

"$(INSTALLDIR)\Log_Msg_MFC.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\Log_Msg_MFC.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\Log_Msg_MFC.exe.manifest" -outputresource:$@;1

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Log_Msg_MFC.dep")
!INCLUDE "Makefile.Log_Msg_MFC.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Log_Msg_MFC.cpp"

"$(INTDIR)\Log_Msg_MFC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Log_Msg_MFC.obj" $(SOURCE)

SOURCE="Log_Msg_MFCDlg.cpp"

"$(INTDIR)\Log_Msg_MFCDlg.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Log_Msg_MFCDlg.obj" $(SOURCE)

SOURCE="MFC_Log.cpp"

"$(INTDIR)\MFC_Log.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\MFC_Log.obj" $(SOURCE)

SOURCE="StdAfx.cpp"

"$(INTDIR)\StdAfx.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\StdAfx.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF

SOURCE="Log_Msg_MFC.rc"

"$(INTDIR)\Log_Msg_MFC.res" : $(SOURCE)
	$(RSC) /l 0x409 /fo"$(INTDIR)\Log_Msg_MFC.res" /d NDEBUG /d WIN64 /d _CRT_SECURE_NO_WARNINGS /d _CRT_SECURE_NO_DEPRECATE /d _CRT_NONSTDC_NO_DEPRECATE /d _AFXDLL /d ACE_HAS_MFC=1 /i "..\..\.." $(SOURCE)



!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Log_Msg_MFC.dep")
	@echo Using "Makefile.Log_Msg_MFC.dep"
!ELSE
	@echo Warning: cannot find "Makefile.Log_Msg_MFC.dep"
!ENDIF
!ENDIF

