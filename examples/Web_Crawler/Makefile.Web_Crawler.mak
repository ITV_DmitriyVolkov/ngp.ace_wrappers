# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.Web_Crawler.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Console Application")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INSTALLDIR=.
INTDIR=Debug\Web_Crawler\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\main.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -f "Makefile.Web_Crawler.dep" "URL.cpp" "Command_Processor.cpp" "URL_Visitor.cpp" "Options.cpp" "Web_Crawler.cpp" "URL_Status.cpp" "Iterators.cpp" "main.cpp" "HTTP_URL.cpp" "URL_Visitor_Factory.cpp" "Mem_Map_Stream.cpp" "URL_Addr.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\main.pdb"
	-@del /f/q "$(INSTALLDIR)\main.exe"
	-@del /f/q "$(INSTALLDIR)\main.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\Web_Crawler\$(NULL)" mkdir "Debug\Web_Crawler"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4355 /Fd"$(INTDIR)/" /I "..\.." /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\main.pdb" /machine:IA64 /out:"$(INSTALLDIR)\main.exe"
LINK32_OBJS= \
	"$(INTDIR)\URL.obj" \
	"$(INTDIR)\Command_Processor.obj" \
	"$(INTDIR)\URL_Visitor.obj" \
	"$(INTDIR)\Options.obj" \
	"$(INTDIR)\Web_Crawler.obj" \
	"$(INTDIR)\URL_Status.obj" \
	"$(INTDIR)\Iterators.obj" \
	"$(INTDIR)\main.obj" \
	"$(INTDIR)\HTTP_URL.obj" \
	"$(INTDIR)\URL_Visitor_Factory.obj" \
	"$(INTDIR)\Mem_Map_Stream.obj" \
	"$(INTDIR)\URL_Addr.obj"

"$(INSTALLDIR)\main.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\main.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\main.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=Release
INSTALLDIR=Release
INTDIR=Release\Web_Crawler\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\main.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -f "Makefile.Web_Crawler.dep" "URL.cpp" "Command_Processor.cpp" "URL_Visitor.cpp" "Options.cpp" "Web_Crawler.cpp" "URL_Status.cpp" "Iterators.cpp" "main.cpp" "HTTP_URL.cpp" "URL_Visitor_Factory.cpp" "Mem_Map_Stream.cpp" "URL_Addr.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\main.exe"
	-@del /f/q "$(INSTALLDIR)\main.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\Web_Crawler\$(NULL)" mkdir "Release\Web_Crawler"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\.." /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\main.exe"
LINK32_OBJS= \
	"$(INTDIR)\URL.obj" \
	"$(INTDIR)\Command_Processor.obj" \
	"$(INTDIR)\URL_Visitor.obj" \
	"$(INTDIR)\Options.obj" \
	"$(INTDIR)\Web_Crawler.obj" \
	"$(INTDIR)\URL_Status.obj" \
	"$(INTDIR)\Iterators.obj" \
	"$(INTDIR)\main.obj" \
	"$(INTDIR)\HTTP_URL.obj" \
	"$(INTDIR)\URL_Visitor_Factory.obj" \
	"$(INTDIR)\Mem_Map_Stream.obj" \
	"$(INTDIR)\URL_Addr.obj"

"$(INSTALLDIR)\main.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\main.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\main.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=Static_Debug
INSTALLDIR=Static_Debug
INTDIR=Static_Debug\Web_Crawler\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\main.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.Web_Crawler.dep" "URL.cpp" "Command_Processor.cpp" "URL_Visitor.cpp" "Options.cpp" "Web_Crawler.cpp" "URL_Status.cpp" "Iterators.cpp" "main.cpp" "HTTP_URL.cpp" "URL_Visitor_Factory.cpp" "Mem_Map_Stream.cpp" "URL_Addr.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\main.pdb"
	-@del /f/q "$(INSTALLDIR)\main.exe"
	-@del /f/q "$(INSTALLDIR)\main.ilk"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\Web_Crawler\$(NULL)" mkdir "Static_Debug\Web_Crawler"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4355 /Fd"$(INTDIR)/" /I "..\.." /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEsd.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\main.pdb" /machine:IA64 /out:"$(INSTALLDIR)\main.exe"
LINK32_OBJS= \
	"$(INTDIR)\URL.obj" \
	"$(INTDIR)\Command_Processor.obj" \
	"$(INTDIR)\URL_Visitor.obj" \
	"$(INTDIR)\Options.obj" \
	"$(INTDIR)\Web_Crawler.obj" \
	"$(INTDIR)\URL_Status.obj" \
	"$(INTDIR)\Iterators.obj" \
	"$(INTDIR)\main.obj" \
	"$(INTDIR)\HTTP_URL.obj" \
	"$(INTDIR)\URL_Visitor_Factory.obj" \
	"$(INTDIR)\Mem_Map_Stream.obj" \
	"$(INTDIR)\URL_Addr.obj"

"$(INSTALLDIR)\main.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\main.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\main.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=Static_Release
INSTALLDIR=Static_Release
INTDIR=Static_Release\Web_Crawler\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\main.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.Web_Crawler.dep" "URL.cpp" "Command_Processor.cpp" "URL_Visitor.cpp" "Options.cpp" "Web_Crawler.cpp" "URL_Status.cpp" "Iterators.cpp" "main.cpp" "HTTP_URL.cpp" "URL_Visitor_Factory.cpp" "Mem_Map_Stream.cpp" "URL_Addr.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\main.exe"
	-@del /f/q "$(INSTALLDIR)\main.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\Web_Crawler\$(NULL)" mkdir "Static_Release\Web_Crawler"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\.." /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEs.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\main.exe"
LINK32_OBJS= \
	"$(INTDIR)\URL.obj" \
	"$(INTDIR)\Command_Processor.obj" \
	"$(INTDIR)\URL_Visitor.obj" \
	"$(INTDIR)\Options.obj" \
	"$(INTDIR)\Web_Crawler.obj" \
	"$(INTDIR)\URL_Status.obj" \
	"$(INTDIR)\Iterators.obj" \
	"$(INTDIR)\main.obj" \
	"$(INTDIR)\HTTP_URL.obj" \
	"$(INTDIR)\URL_Visitor_Factory.obj" \
	"$(INTDIR)\Mem_Map_Stream.obj" \
	"$(INTDIR)\URL_Addr.obj"

"$(INSTALLDIR)\main.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\main.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\main.exe.manifest" -outputresource:$@;1

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Web_Crawler.dep")
!INCLUDE "Makefile.Web_Crawler.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="URL.cpp"

"$(INTDIR)\URL.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\URL.obj" $(SOURCE)

SOURCE="Command_Processor.cpp"

"$(INTDIR)\Command_Processor.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Command_Processor.obj" $(SOURCE)

SOURCE="URL_Visitor.cpp"

"$(INTDIR)\URL_Visitor.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\URL_Visitor.obj" $(SOURCE)

SOURCE="Options.cpp"

"$(INTDIR)\Options.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Options.obj" $(SOURCE)

SOURCE="Web_Crawler.cpp"

"$(INTDIR)\Web_Crawler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Web_Crawler.obj" $(SOURCE)

SOURCE="URL_Status.cpp"

"$(INTDIR)\URL_Status.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\URL_Status.obj" $(SOURCE)

SOURCE="Iterators.cpp"

"$(INTDIR)\Iterators.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Iterators.obj" $(SOURCE)

SOURCE="main.cpp"

"$(INTDIR)\main.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\main.obj" $(SOURCE)

SOURCE="HTTP_URL.cpp"

"$(INTDIR)\HTTP_URL.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\HTTP_URL.obj" $(SOURCE)

SOURCE="URL_Visitor_Factory.cpp"

"$(INTDIR)\URL_Visitor_Factory.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\URL_Visitor_Factory.obj" $(SOURCE)

SOURCE="Mem_Map_Stream.cpp"

"$(INTDIR)\Mem_Map_Stream.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Mem_Map_Stream.obj" $(SOURCE)

SOURCE="URL_Addr.cpp"

"$(INTDIR)\URL_Addr.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\URL_Addr.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Web_Crawler.dep")
	@echo Using "Makefile.Web_Crawler.dep"
!ELSE
	@echo Warning: cannot find "Makefile.Web_Crawler.dep"
!ENDIF
!ENDIF

