/**
 * $Id: SecurityContext.h 14 2007-02-01 15:49:12Z mitza $
 *
 * Sample code from The ACE Programmer's Guide,
 * copyright 2003 Addison-Wesley. All Rights Reserved.
 */

#ifndef __SECURITYCONTEXT_H_
#define __SECURITYCONTEXT_H_

struct SecurityContext
  {
    const char * user;
  };

#endif /* __SECURITYCONTEXT_H_ */
