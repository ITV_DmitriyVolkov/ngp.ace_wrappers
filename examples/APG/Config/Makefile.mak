#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake -value_template platforms=Win64 -make_coexistence -recurse -exclude TAO/CIAO -features qos=1,mfc=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: ARGV_Example Config_HA_Status Get_Opt Get_Opt_Long

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.ARGV_Example.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.ARGV_Example.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Config_HA_Status.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Config_HA_Status.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Get_Opt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Get_Opt.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Get_Opt_Long.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Get_Opt_Long.mak CFG="$(CFG)" $(@)

ARGV_Example:
	@echo Project: Makefile.ARGV_Example.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.ARGV_Example.mak CFG="$(CFG)" all

Config_HA_Status:
	@echo Project: Makefile.Config_HA_Status.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Config_HA_Status.mak CFG="$(CFG)" all

Get_Opt:
	@echo Project: Makefile.Get_Opt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Get_Opt.mak CFG="$(CFG)" all

Get_Opt_Long:
	@echo Project: Makefile.Get_Opt_Long.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Get_Opt_Long.mak CFG="$(CFG)" all

project_name_list:
	@echo ARGV_Example
	@echo Config_HA_Status
	@echo Get_Opt
	@echo Get_Opt_Long
