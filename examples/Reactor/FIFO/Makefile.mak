#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake -value_template platforms=Win64 -make_coexistence -recurse -exclude TAO/CIAO -features qos=1,mfc=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: Reactor_FIFO_Client Reactor_FIFO_Server

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.Reactor_FIFO_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Reactor_FIFO_Client.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Reactor_FIFO_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Reactor_FIFO_Server.mak CFG="$(CFG)" $(@)

Reactor_FIFO_Client:
	@echo Project: Makefile.Reactor_FIFO_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Reactor_FIFO_Client.mak CFG="$(CFG)" all

Reactor_FIFO_Server:
	@echo Project: Makefile.Reactor_FIFO_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Reactor_FIFO_Server.mak CFG="$(CFG)" all

project_name_list:
	@echo Reactor_FIFO_Client
	@echo Reactor_FIFO_Server
