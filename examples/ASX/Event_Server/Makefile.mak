#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake -value_template platforms=Win64 -make_coexistence -recurse -exclude TAO/CIAO -features qos=1,mfc=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: Event_Server-target Transceiver-target

clean depend generated realclean $(CUSTOM_TARGETS):
	@cd Event_Server
	@echo Directory: Event_Server
	@echo Project: Makefile.Event_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Server.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Transceiver
	@echo Directory: Transceiver
	@echo Project: Makefile.Transceiver.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Transceiver.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)

Event_Server-target:
	@cd Event_Server
	@echo Directory: Event_Server
	@echo Project: Makefile.Event_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Server.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Transceiver-target:
	@cd Transceiver
	@echo Directory: Transceiver
	@echo Project: Makefile.Transceiver.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Transceiver.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

project_name_list:
	@echo Event_Server-target
	@echo Transceiver-target
