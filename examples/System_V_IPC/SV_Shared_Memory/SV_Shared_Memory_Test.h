/* -*- C++ -*- */
// $Id: SV_Shared_Memory_Test.h 14 2007-02-01 15:49:12Z mitza $

#include "ace/config-all.h"

#if !defined (ACE_LACKS_PRAGMA_ONCE)
# pragma once
#endif /* ACE_LACKS_PRAGMA_ONCE */

#define SHMSZ 27
#define SEM_KEY 1234
#define SHM_KEY 5678
