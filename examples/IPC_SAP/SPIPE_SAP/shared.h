/* -*- C++ -*- */
// $Id: shared.h 14 2007-02-01 15:49:12Z mitza $

#include "ace/config-all.h"

#if !defined (ACE_LACKS_PRAGMA_ONCE)
# pragma once
#endif /* ACE_LACKS_PRAGMA_ONCE */

static const ACE_TCHAR *rendezvous = ACE_DEFAULT_RENDEZVOUS;
