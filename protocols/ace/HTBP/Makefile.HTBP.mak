# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.HTBP.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\..\lib
INTDIR=Debug\HTBP\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\lib\ACE_HTBPd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DHTBP_BUILD_DLL -f "Makefile.HTBP.dep" "HTBP_ID_Requestor.cpp" "HTBP_Environment.cpp" "HTBP_Session.cpp" "HTBP_Inside_Squid_Filter.cpp" "HTBP_Filter.cpp" "HTBP_Stream.cpp" "HTBP_Channel.cpp" "HTBP_Outside_Squid_Filter.cpp" "HTBP_Notifier.cpp" "HTBP_Filter_Factory.cpp" "HTBP_Addr.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\ACE_HTBPd.pdb"
	-@del /f/q "..\..\..\lib\ACE_HTBPd.dll"
	-@del /f/q "$(OUTDIR)\ACE_HTBPd.lib"
	-@del /f/q "$(OUTDIR)\ACE_HTBPd.exp"
	-@del /f/q "$(OUTDIR)\ACE_HTBPd.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\HTBP\$(NULL)" mkdir "Debug\HTBP"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4355 /Fd"$(INTDIR)/" /I "..\..\.." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D HTBP_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\..\lib\ACE_HTBPd.pdb" /machine:IA64 /out:"..\..\..\lib\ACE_HTBPd.dll" /implib:"$(OUTDIR)\ACE_HTBPd.lib"
LINK32_OBJS= \
	"$(INTDIR)\HTBP_ID_Requestor.obj" \
	"$(INTDIR)\HTBP_Environment.obj" \
	"$(INTDIR)\HTBP_Session.obj" \
	"$(INTDIR)\HTBP_Inside_Squid_Filter.obj" \
	"$(INTDIR)\HTBP_Filter.obj" \
	"$(INTDIR)\HTBP_Stream.obj" \
	"$(INTDIR)\HTBP_Channel.obj" \
	"$(INTDIR)\HTBP_Outside_Squid_Filter.obj" \
	"$(INTDIR)\HTBP_Notifier.obj" \
	"$(INTDIR)\HTBP_Filter_Factory.obj" \
	"$(INTDIR)\HTBP_Addr.obj"

"..\..\..\lib\ACE_HTBPd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\lib\ACE_HTBPd.dll.manifest" mt.exe -manifest "..\..\..\lib\ACE_HTBPd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\..\lib
INTDIR=Release\HTBP\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\lib\ACE_HTBP.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DHTBP_BUILD_DLL -f "Makefile.HTBP.dep" "HTBP_ID_Requestor.cpp" "HTBP_Environment.cpp" "HTBP_Session.cpp" "HTBP_Inside_Squid_Filter.cpp" "HTBP_Filter.cpp" "HTBP_Stream.cpp" "HTBP_Channel.cpp" "HTBP_Outside_Squid_Filter.cpp" "HTBP_Notifier.cpp" "HTBP_Filter_Factory.cpp" "HTBP_Addr.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\..\lib\ACE_HTBP.dll"
	-@del /f/q "$(OUTDIR)\ACE_HTBP.lib"
	-@del /f/q "$(OUTDIR)\ACE_HTBP.exp"
	-@del /f/q "$(OUTDIR)\ACE_HTBP.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\HTBP\$(NULL)" mkdir "Release\HTBP"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\..\.." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D HTBP_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\..\lib\ACE_HTBP.dll" /implib:"$(OUTDIR)\ACE_HTBP.lib"
LINK32_OBJS= \
	"$(INTDIR)\HTBP_ID_Requestor.obj" \
	"$(INTDIR)\HTBP_Environment.obj" \
	"$(INTDIR)\HTBP_Session.obj" \
	"$(INTDIR)\HTBP_Inside_Squid_Filter.obj" \
	"$(INTDIR)\HTBP_Filter.obj" \
	"$(INTDIR)\HTBP_Stream.obj" \
	"$(INTDIR)\HTBP_Channel.obj" \
	"$(INTDIR)\HTBP_Outside_Squid_Filter.obj" \
	"$(INTDIR)\HTBP_Notifier.obj" \
	"$(INTDIR)\HTBP_Filter_Factory.obj" \
	"$(INTDIR)\HTBP_Addr.obj"

"..\..\..\lib\ACE_HTBP.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\lib\ACE_HTBP.dll.manifest" mt.exe -manifest "..\..\..\lib\ACE_HTBP.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\..\lib
INTDIR=Static_Debug\HTBP\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\ACE_HTBPsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.HTBP.dep" "HTBP_ID_Requestor.cpp" "HTBP_Environment.cpp" "HTBP_Session.cpp" "HTBP_Inside_Squid_Filter.cpp" "HTBP_Filter.cpp" "HTBP_Stream.cpp" "HTBP_Channel.cpp" "HTBP_Outside_Squid_Filter.cpp" "HTBP_Notifier.cpp" "HTBP_Filter_Factory.cpp" "HTBP_Addr.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\ACE_HTBPsd.lib"
	-@del /f/q "$(OUTDIR)\ACE_HTBPsd.exp"
	-@del /f/q "$(OUTDIR)\ACE_HTBPsd.ilk"
	-@del /f/q "..\..\..\lib\ACE_HTBPsd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\HTBP\$(NULL)" mkdir "Static_Debug\HTBP"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4355 /Fd"..\..\..\lib\ACE_HTBPsd.pdb" /I "..\..\.." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\lib\ACE_HTBPsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\HTBP_ID_Requestor.obj" \
	"$(INTDIR)\HTBP_Environment.obj" \
	"$(INTDIR)\HTBP_Session.obj" \
	"$(INTDIR)\HTBP_Inside_Squid_Filter.obj" \
	"$(INTDIR)\HTBP_Filter.obj" \
	"$(INTDIR)\HTBP_Stream.obj" \
	"$(INTDIR)\HTBP_Channel.obj" \
	"$(INTDIR)\HTBP_Outside_Squid_Filter.obj" \
	"$(INTDIR)\HTBP_Notifier.obj" \
	"$(INTDIR)\HTBP_Filter_Factory.obj" \
	"$(INTDIR)\HTBP_Addr.obj"

"$(OUTDIR)\ACE_HTBPsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\ACE_HTBPsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\ACE_HTBPsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\..\lib
INTDIR=Static_Release\HTBP\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\ACE_HTBPs.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.HTBP.dep" "HTBP_ID_Requestor.cpp" "HTBP_Environment.cpp" "HTBP_Session.cpp" "HTBP_Inside_Squid_Filter.cpp" "HTBP_Filter.cpp" "HTBP_Stream.cpp" "HTBP_Channel.cpp" "HTBP_Outside_Squid_Filter.cpp" "HTBP_Notifier.cpp" "HTBP_Filter_Factory.cpp" "HTBP_Addr.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\ACE_HTBPs.lib"
	-@del /f/q "$(OUTDIR)\ACE_HTBPs.exp"
	-@del /f/q "$(OUTDIR)\ACE_HTBPs.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\HTBP\$(NULL)" mkdir "Static_Release\HTBP"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\..\.." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\lib\ACE_HTBPs.lib"
LINK32_OBJS= \
	"$(INTDIR)\HTBP_ID_Requestor.obj" \
	"$(INTDIR)\HTBP_Environment.obj" \
	"$(INTDIR)\HTBP_Session.obj" \
	"$(INTDIR)\HTBP_Inside_Squid_Filter.obj" \
	"$(INTDIR)\HTBP_Filter.obj" \
	"$(INTDIR)\HTBP_Stream.obj" \
	"$(INTDIR)\HTBP_Channel.obj" \
	"$(INTDIR)\HTBP_Outside_Squid_Filter.obj" \
	"$(INTDIR)\HTBP_Notifier.obj" \
	"$(INTDIR)\HTBP_Filter_Factory.obj" \
	"$(INTDIR)\HTBP_Addr.obj"

"$(OUTDIR)\ACE_HTBPs.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\ACE_HTBPs.lib.manifest" mt.exe -manifest "$(OUTDIR)\ACE_HTBPs.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.HTBP.dep")
!INCLUDE "Makefile.HTBP.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="HTBP_ID_Requestor.cpp"

"$(INTDIR)\HTBP_ID_Requestor.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\HTBP_ID_Requestor.obj" $(SOURCE)

SOURCE="HTBP_Environment.cpp"

"$(INTDIR)\HTBP_Environment.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\HTBP_Environment.obj" $(SOURCE)

SOURCE="HTBP_Session.cpp"

"$(INTDIR)\HTBP_Session.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\HTBP_Session.obj" $(SOURCE)

SOURCE="HTBP_Inside_Squid_Filter.cpp"

"$(INTDIR)\HTBP_Inside_Squid_Filter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\HTBP_Inside_Squid_Filter.obj" $(SOURCE)

SOURCE="HTBP_Filter.cpp"

"$(INTDIR)\HTBP_Filter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\HTBP_Filter.obj" $(SOURCE)

SOURCE="HTBP_Stream.cpp"

"$(INTDIR)\HTBP_Stream.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\HTBP_Stream.obj" $(SOURCE)

SOURCE="HTBP_Channel.cpp"

"$(INTDIR)\HTBP_Channel.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\HTBP_Channel.obj" $(SOURCE)

SOURCE="HTBP_Outside_Squid_Filter.cpp"

"$(INTDIR)\HTBP_Outside_Squid_Filter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\HTBP_Outside_Squid_Filter.obj" $(SOURCE)

SOURCE="HTBP_Notifier.cpp"

"$(INTDIR)\HTBP_Notifier.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\HTBP_Notifier.obj" $(SOURCE)

SOURCE="HTBP_Filter_Factory.cpp"

"$(INTDIR)\HTBP_Filter_Factory.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\HTBP_Filter_Factory.obj" $(SOURCE)

SOURCE="HTBP_Addr.cpp"

"$(INTDIR)\HTBP_Addr.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\HTBP_Addr.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.HTBP.dep")
	@echo Using "Makefile.HTBP.dep"
!ELSE
	@echo Warning: cannot find "Makefile.HTBP.dep"
!ENDIF
!ENDIF

