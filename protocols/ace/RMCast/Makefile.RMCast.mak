# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.RMCast.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\..\lib
INTDIR=Debug\RMCast\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\lib\ACE_RMCastd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_RMCAST_BUILD_DLL -f "Makefile.RMCast.dep" "Flow.cpp" "Acknowledge.cpp" "Fragment.cpp" "Link.cpp" "Reassemble.cpp" "Simulator.cpp" "Stack.cpp" "Retransmit.cpp" "Socket.cpp" "Protocol.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\ACE_RMCastd.pdb"
	-@del /f/q "..\..\..\lib\ACE_RMCastd.dll"
	-@del /f/q "$(OUTDIR)\ACE_RMCastd.lib"
	-@del /f/q "$(OUTDIR)\ACE_RMCastd.exp"
	-@del /f/q "$(OUTDIR)\ACE_RMCastd.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\RMCast\$(NULL)" mkdir "Debug\RMCast"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4355 /Fd"$(INTDIR)/" /I "..\..\.." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_RMCAST_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\..\lib\ACE_RMCastd.pdb" /machine:IA64 /out:"..\..\..\lib\ACE_RMCastd.dll" /implib:"$(OUTDIR)\ACE_RMCastd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Flow.obj" \
	"$(INTDIR)\Acknowledge.obj" \
	"$(INTDIR)\Fragment.obj" \
	"$(INTDIR)\Link.obj" \
	"$(INTDIR)\Reassemble.obj" \
	"$(INTDIR)\Simulator.obj" \
	"$(INTDIR)\Stack.obj" \
	"$(INTDIR)\Retransmit.obj" \
	"$(INTDIR)\Socket.obj" \
	"$(INTDIR)\Protocol.obj"

"..\..\..\lib\ACE_RMCastd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\lib\ACE_RMCastd.dll.manifest" mt.exe -manifest "..\..\..\lib\ACE_RMCastd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\..\lib
INTDIR=Release\RMCast\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\lib\ACE_RMCast.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_RMCAST_BUILD_DLL -f "Makefile.RMCast.dep" "Flow.cpp" "Acknowledge.cpp" "Fragment.cpp" "Link.cpp" "Reassemble.cpp" "Simulator.cpp" "Stack.cpp" "Retransmit.cpp" "Socket.cpp" "Protocol.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\..\lib\ACE_RMCast.dll"
	-@del /f/q "$(OUTDIR)\ACE_RMCast.lib"
	-@del /f/q "$(OUTDIR)\ACE_RMCast.exp"
	-@del /f/q "$(OUTDIR)\ACE_RMCast.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\RMCast\$(NULL)" mkdir "Release\RMCast"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\..\.." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_RMCAST_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\..\lib\ACE_RMCast.dll" /implib:"$(OUTDIR)\ACE_RMCast.lib"
LINK32_OBJS= \
	"$(INTDIR)\Flow.obj" \
	"$(INTDIR)\Acknowledge.obj" \
	"$(INTDIR)\Fragment.obj" \
	"$(INTDIR)\Link.obj" \
	"$(INTDIR)\Reassemble.obj" \
	"$(INTDIR)\Simulator.obj" \
	"$(INTDIR)\Stack.obj" \
	"$(INTDIR)\Retransmit.obj" \
	"$(INTDIR)\Socket.obj" \
	"$(INTDIR)\Protocol.obj"

"..\..\..\lib\ACE_RMCast.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\lib\ACE_RMCast.dll.manifest" mt.exe -manifest "..\..\..\lib\ACE_RMCast.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\..\lib
INTDIR=Static_Debug\RMCast\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\ACE_RMCastsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.RMCast.dep" "Flow.cpp" "Acknowledge.cpp" "Fragment.cpp" "Link.cpp" "Reassemble.cpp" "Simulator.cpp" "Stack.cpp" "Retransmit.cpp" "Socket.cpp" "Protocol.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\ACE_RMCastsd.lib"
	-@del /f/q "$(OUTDIR)\ACE_RMCastsd.exp"
	-@del /f/q "$(OUTDIR)\ACE_RMCastsd.ilk"
	-@del /f/q "..\..\..\lib\ACE_RMCastsd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\RMCast\$(NULL)" mkdir "Static_Debug\RMCast"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4355 /Fd"..\..\..\lib\ACE_RMCastsd.pdb" /I "..\..\.." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\lib\ACE_RMCastsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Flow.obj" \
	"$(INTDIR)\Acknowledge.obj" \
	"$(INTDIR)\Fragment.obj" \
	"$(INTDIR)\Link.obj" \
	"$(INTDIR)\Reassemble.obj" \
	"$(INTDIR)\Simulator.obj" \
	"$(INTDIR)\Stack.obj" \
	"$(INTDIR)\Retransmit.obj" \
	"$(INTDIR)\Socket.obj" \
	"$(INTDIR)\Protocol.obj"

"$(OUTDIR)\ACE_RMCastsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\ACE_RMCastsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\ACE_RMCastsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\..\lib
INTDIR=Static_Release\RMCast\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\ACE_RMCasts.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.RMCast.dep" "Flow.cpp" "Acknowledge.cpp" "Fragment.cpp" "Link.cpp" "Reassemble.cpp" "Simulator.cpp" "Stack.cpp" "Retransmit.cpp" "Socket.cpp" "Protocol.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\ACE_RMCasts.lib"
	-@del /f/q "$(OUTDIR)\ACE_RMCasts.exp"
	-@del /f/q "$(OUTDIR)\ACE_RMCasts.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\RMCast\$(NULL)" mkdir "Static_Release\RMCast"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\..\.." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\lib\ACE_RMCasts.lib"
LINK32_OBJS= \
	"$(INTDIR)\Flow.obj" \
	"$(INTDIR)\Acknowledge.obj" \
	"$(INTDIR)\Fragment.obj" \
	"$(INTDIR)\Link.obj" \
	"$(INTDIR)\Reassemble.obj" \
	"$(INTDIR)\Simulator.obj" \
	"$(INTDIR)\Stack.obj" \
	"$(INTDIR)\Retransmit.obj" \
	"$(INTDIR)\Socket.obj" \
	"$(INTDIR)\Protocol.obj"

"$(OUTDIR)\ACE_RMCasts.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\ACE_RMCasts.lib.manifest" mt.exe -manifest "$(OUTDIR)\ACE_RMCasts.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.RMCast.dep")
!INCLUDE "Makefile.RMCast.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Flow.cpp"

"$(INTDIR)\Flow.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Flow.obj" $(SOURCE)

SOURCE="Acknowledge.cpp"

"$(INTDIR)\Acknowledge.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Acknowledge.obj" $(SOURCE)

SOURCE="Fragment.cpp"

"$(INTDIR)\Fragment.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Fragment.obj" $(SOURCE)

SOURCE="Link.cpp"

"$(INTDIR)\Link.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Link.obj" $(SOURCE)

SOURCE="Reassemble.cpp"

"$(INTDIR)\Reassemble.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Reassemble.obj" $(SOURCE)

SOURCE="Simulator.cpp"

"$(INTDIR)\Simulator.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Simulator.obj" $(SOURCE)

SOURCE="Stack.cpp"

"$(INTDIR)\Stack.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Stack.obj" $(SOURCE)

SOURCE="Retransmit.cpp"

"$(INTDIR)\Retransmit.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Retransmit.obj" $(SOURCE)

SOURCE="Socket.cpp"

"$(INTDIR)\Socket.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Socket.obj" $(SOURCE)

SOURCE="Protocol.cpp"

"$(INTDIR)\Protocol.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Protocol.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.RMCast.dep")
	@echo Using "Makefile.RMCast.dep"
!ELSE
	@echo Warning: cannot find "Makefile.RMCast.dep"
!ENDIF
!ENDIF

