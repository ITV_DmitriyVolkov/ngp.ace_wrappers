// file      : ACE_TMCast/GroupFwd.hpp
// author    : Boris Kolpackov <boris@dre.vanderbilt.edu>
// cvs-id    : $Id: GroupFwd.hpp 935 2008-12-10 21:47:27Z mitza $

#ifndef TMCAST_GROUP_FWD_HPP
#define TMCAST_GROUP_FWD_HPP

#include "Export.hpp"

namespace ACE_TMCast
{
  class Group;
}

#endif // TMCAST_GROUP_FWD_HPP
