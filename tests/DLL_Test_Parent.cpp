// $Id: DLL_Test_Parent.cpp 14 2007-02-01 15:49:12Z mitza $

#include "DLL_Test_Parent.h"
#include "ace/Log_Msg.h"

ACE_RCSID (tests,
           DLL_Test_Parent,
           "$Id: DLL_Test_Parent.cpp 14 2007-02-01 15:49:12Z mitza $")


Parent::~Parent (void)
{
}

void
Parent::test (void)
{
  ACE_DEBUG ((LM_DEBUG, ACE_TEXT ("parent called\n")));
}
