eval '(exit $?0)' && eval 'exec perl -w -S $0 ${1+"$@"}'
    & eval 'exec perl -w -S $0 $argv:q'
    if 0;
# ********************************************************************
# $Id: ace_install_pkgconfig.pl 1761 2011-01-03 16:23:00Z mitza $
# ace_install_pkgconfig.pl - Creates *.pc files for pkg-config in the
#                            installed location, based on the *.pc.in
#                            files from the source tree, with @foo@
#                            variables replaced with their values.
#                            Called from the MPC-generated makefiles.
# ********************************************************************

use strict;
use Getopt::Long;
use File::Basename;

my ($prefix, $libdir, $libs, $version, %custom);
GetOptions('prefix=s' => \$prefix, 'libdir=s' => \$libdir, 'libs=s' => \$libs,
           'version=s' => \$version, 'custom=s' => \%custom);

my %subs = ('LIBS' => $libs, 'VERSION' => $version, 'exec_prefix' => $prefix,
            'prefix' => $prefix, 'includedir' => "$prefix/include",
            'libdir' => "$prefix/$libdir");

for my $k (keys %custom) {
  $subs{$k} = $custom{$k};
}

my $pcdir = "$prefix/$libdir/pkgconfig";
if (scalar @ARGV && ! -d $pcdir) {
  mkdir($pcdir, 0755);
}

for my $file (@ARGV) {
  open IN, $file;
  my $pcfile = basename($file);
  $pcfile =~ s/\.in$//;
  open OUT, ">$pcdir/$pcfile" or die "Can't write to $pcdir/$pcfile";
  while (<IN>) {
    s/@(\w+)@/exists $subs{$1} ? $subs{$1} : $&/ge;
    print OUT $_;
  }
  close OUT;
  close IN;
}

