# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.Activity.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Console Application")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "JobC.inl" "JobS.inl" "JobC.h" "JobS.h" "JobC.cpp" "JobS.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INSTALLDIR=.
INTDIR=Debug\Activity\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\activity.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\..\orbsvcs" -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACTIVITY_BUILD_DLL -f "Makefile.Activity.dep" "JobC.cpp" "JobS.cpp" "Activity.cpp" "Builder.cpp" "Job_i.cpp" "Periodic_Task.cpp" "POA_Holder.cpp" "Task_Stats.cpp" "Thread_Task.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\activity.pdb"
	-@del /f/q "$(INSTALLDIR)\activity.exe"
	-@del /f/q "$(INSTALLDIR)\activity.ilk"
	-@del /f/q "JobC.inl"
	-@del /f/q "JobS.inl"
	-@del /f/q "JobC.h"
	-@del /f/q "JobS.h"
	-@del /f/q "JobC.cpp"
	-@del /f/q "JobS.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\Activity\$(NULL)" mkdir "Debug\Activity"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /I "..\..\..\orbsvcs" /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACTIVITY_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_CosNamingd.lib TAO_CodecFactoryd.lib TAO_PId.lib TAO_PortableServerd.lib TAO_RTCORBAd.lib TAO_RTPortableServerd.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\activity.pdb" /machine:IA64 /out:"$(INSTALLDIR)\activity.exe"
LINK32_OBJS= \
	"$(INTDIR)\JobC.obj" \
	"$(INTDIR)\JobS.obj" \
	"$(INTDIR)\Activity.obj" \
	"$(INTDIR)\Builder.obj" \
	"$(INTDIR)\Job_i.obj" \
	"$(INTDIR)\Periodic_Task.obj" \
	"$(INTDIR)\POA_Holder.obj" \
	"$(INTDIR)\Task_Stats.obj" \
	"$(INTDIR)\Thread_Task.obj"

"$(INSTALLDIR)\activity.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\activity.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\activity.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=Release
INSTALLDIR=Release
INTDIR=Release\Activity\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\activity.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\..\orbsvcs" -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACTIVITY_BUILD_DLL -f "Makefile.Activity.dep" "JobC.cpp" "JobS.cpp" "Activity.cpp" "Builder.cpp" "Job_i.cpp" "Periodic_Task.cpp" "POA_Holder.cpp" "Task_Stats.cpp" "Thread_Task.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\activity.exe"
	-@del /f/q "$(INSTALLDIR)\activity.ilk"
	-@del /f/q "JobC.inl"
	-@del /f/q "JobS.inl"
	-@del /f/q "JobC.h"
	-@del /f/q "JobS.h"
	-@del /f/q "JobC.cpp"
	-@del /f/q "JobS.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\Activity\$(NULL)" mkdir "Release\Activity"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\.." /I "..\..\.." /I "..\..\..\orbsvcs" /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACTIVITY_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_CosNaming.lib TAO_CodecFactory.lib TAO_PI.lib TAO_PortableServer.lib TAO_RTCORBA.lib TAO_RTPortableServer.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\activity.exe"
LINK32_OBJS= \
	"$(INTDIR)\JobC.obj" \
	"$(INTDIR)\JobS.obj" \
	"$(INTDIR)\Activity.obj" \
	"$(INTDIR)\Builder.obj" \
	"$(INTDIR)\Job_i.obj" \
	"$(INTDIR)\Periodic_Task.obj" \
	"$(INTDIR)\POA_Holder.obj" \
	"$(INTDIR)\Task_Stats.obj" \
	"$(INTDIR)\Thread_Task.obj"

"$(INSTALLDIR)\activity.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\activity.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\activity.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=Static_Debug
INSTALLDIR=Static_Debug
INTDIR=Static_Debug\Activity\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\activity.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\..\orbsvcs" -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACTIVITY_BUILD_DLL -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Activity.dep" "JobC.cpp" "JobS.cpp" "Activity.cpp" "Builder.cpp" "Job_i.cpp" "Periodic_Task.cpp" "POA_Holder.cpp" "Task_Stats.cpp" "Thread_Task.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\activity.pdb"
	-@del /f/q "$(INSTALLDIR)\activity.exe"
	-@del /f/q "$(INSTALLDIR)\activity.ilk"
	-@del /f/q "JobC.inl"
	-@del /f/q "JobS.inl"
	-@del /f/q "JobC.h"
	-@del /f/q "JobS.h"
	-@del /f/q "JobC.cpp"
	-@del /f/q "JobS.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\Activity\$(NULL)" mkdir "Static_Debug\Activity"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /I "..\..\..\orbsvcs" /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACTIVITY_BUILD_DLL /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEsd.lib TAOsd.lib TAO_AnyTypeCodesd.lib TAO_CosNamingsd.lib TAO_CodecFactorysd.lib TAO_PIsd.lib TAO_PortableServersd.lib TAO_RTCORBAsd.lib TAO_RTPortableServersd.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\activity.pdb" /machine:IA64 /out:"$(INSTALLDIR)\activity.exe"
LINK32_OBJS= \
	"$(INTDIR)\JobC.obj" \
	"$(INTDIR)\JobS.obj" \
	"$(INTDIR)\Activity.obj" \
	"$(INTDIR)\Builder.obj" \
	"$(INTDIR)\Job_i.obj" \
	"$(INTDIR)\Periodic_Task.obj" \
	"$(INTDIR)\POA_Holder.obj" \
	"$(INTDIR)\Task_Stats.obj" \
	"$(INTDIR)\Thread_Task.obj"

"$(INSTALLDIR)\activity.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\activity.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\activity.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=Static_Release
INSTALLDIR=Static_Release
INTDIR=Static_Release\Activity\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\activity.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\..\orbsvcs" -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACTIVITY_BUILD_DLL -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Activity.dep" "JobC.cpp" "JobS.cpp" "Activity.cpp" "Builder.cpp" "Job_i.cpp" "Periodic_Task.cpp" "POA_Holder.cpp" "Task_Stats.cpp" "Thread_Task.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\activity.exe"
	-@del /f/q "$(INSTALLDIR)\activity.ilk"
	-@del /f/q "JobC.inl"
	-@del /f/q "JobS.inl"
	-@del /f/q "JobC.h"
	-@del /f/q "JobS.h"
	-@del /f/q "JobC.cpp"
	-@del /f/q "JobS.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\Activity\$(NULL)" mkdir "Static_Release\Activity"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\.." /I "..\..\.." /I "..\..\..\orbsvcs" /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACTIVITY_BUILD_DLL /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEs.lib TAOs.lib TAO_AnyTypeCodes.lib TAO_CosNamings.lib TAO_CodecFactorys.lib TAO_PIs.lib TAO_PortableServers.lib TAO_RTCORBAs.lib TAO_RTPortableServers.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\activity.exe"
LINK32_OBJS= \
	"$(INTDIR)\JobC.obj" \
	"$(INTDIR)\JobS.obj" \
	"$(INTDIR)\Activity.obj" \
	"$(INTDIR)\Builder.obj" \
	"$(INTDIR)\Job_i.obj" \
	"$(INTDIR)\Periodic_Task.obj" \
	"$(INTDIR)\POA_Holder.obj" \
	"$(INTDIR)\Task_Stats.obj" \
	"$(INTDIR)\Thread_Task.obj"

"$(INSTALLDIR)\activity.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\activity.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\activity.exe.manifest" -outputresource:$@;1

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Activity.dep")
!INCLUDE "Makefile.Activity.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="JobC.cpp"

"$(INTDIR)\JobC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\JobC.obj" $(SOURCE)

SOURCE="JobS.cpp"

"$(INTDIR)\JobS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\JobS.obj" $(SOURCE)

SOURCE="Activity.cpp"

"$(INTDIR)\Activity.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Activity.obj" $(SOURCE)

SOURCE="Builder.cpp"

"$(INTDIR)\Builder.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Builder.obj" $(SOURCE)

SOURCE="Job_i.cpp"

"$(INTDIR)\Job_i.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Job_i.obj" $(SOURCE)

SOURCE="Periodic_Task.cpp"

"$(INTDIR)\Periodic_Task.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Periodic_Task.obj" $(SOURCE)

SOURCE="POA_Holder.cpp"

"$(INTDIR)\POA_Holder.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\POA_Holder.obj" $(SOURCE)

SOURCE="Task_Stats.cpp"

"$(INTDIR)\Task_Stats.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Task_Stats.obj" $(SOURCE)

SOURCE="Thread_Task.cpp"

"$(INTDIR)\Thread_Task.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Thread_Task.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="Job.idl"

InputPath=Job.idl

"JobC.inl" "JobS.inl" "JobC.h" "JobS.h" "JobC.cpp" "JobS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Job_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="Job.idl"

InputPath=Job.idl

"JobC.inl" "JobS.inl" "JobC.h" "JobS.h" "JobC.cpp" "JobS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Job_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="Job.idl"

InputPath=Job.idl

"JobC.inl" "JobS.inl" "JobC.h" "JobS.h" "JobC.cpp" "JobS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Job_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="Job.idl"

InputPath=Job.idl

"JobC.inl" "JobS.inl" "JobC.h" "JobS.h" "JobC.cpp" "JobS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Job_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Activity.dep")
	@echo Using "Makefile.Activity.dep"
!ELSE
	@echo Warning: cannot find "Makefile.Activity.dep"
!ENDIF
!ENDIF

