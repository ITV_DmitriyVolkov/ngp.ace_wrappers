// $Id: Server.h 14 2007-02-01 15:49:12Z mitza $

// ===========================================================
//
//
// = LIBRARY
//    TAO/tests/Simple/chat
//
// = FILENAME
//    Server.h
//
// = DESCRIPTION
//    Definition of the Server class for the chat.
//
// = AUTHOR
//    Pradeep Gore <pradeep@cs.wustl.edu>
//
// ===========================================================
