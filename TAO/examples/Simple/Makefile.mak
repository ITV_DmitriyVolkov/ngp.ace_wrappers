#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake -value_template platforms=Win64 -make_coexistence -recurse -exclude TAO/CIAO -features qos=1,mfc=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: bank_IDL bank_client bank_server chat_IDL chat_client chat_server echo_IDL echo_client echo_server grid_IDL grid_client grid_server Simple_Time_Date_Lib Simple_Time_Date_Client Simple_Time_Date_Server time_IDL time_client time_server

clean depend generated realclean $(CUSTOM_TARGETS):
	@cd bank
	@echo Directory: bank
	@echo Project: Makefile.bank_IDL.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.bank_IDL.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd bank
	@echo Directory: bank
	@echo Project: Makefile.bank_client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.bank_client.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd bank
	@echo Directory: bank
	@echo Project: Makefile.bank_server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.bank_server.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd chat
	@echo Directory: chat
	@echo Project: Makefile.chat_IDL.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.chat_IDL.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd chat
	@echo Directory: chat
	@echo Project: Makefile.chat_client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.chat_client.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd chat
	@echo Directory: chat
	@echo Project: Makefile.chat_server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.chat_server.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd echo
	@echo Directory: echo
	@echo Project: Makefile.echo_IDL.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.echo_IDL.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd echo
	@echo Directory: echo
	@echo Project: Makefile.echo_client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.echo_client.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd echo
	@echo Directory: echo
	@echo Project: Makefile.echo_server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.echo_server.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd grid
	@echo Directory: grid
	@echo Project: Makefile.grid_IDL.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.grid_IDL.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd grid
	@echo Directory: grid
	@echo Project: Makefile.grid_client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.grid_client.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd grid
	@echo Directory: grid
	@echo Project: Makefile.grid_server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.grid_server.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd time-date
	@echo Directory: time-date
	@echo Project: Makefile.Simple_Time_Date_Lib.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Simple_Time_Date_Lib.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd time-date
	@echo Directory: time-date
	@echo Project: Makefile.Simple_Time_Date_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Simple_Time_Date_Client.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd time-date
	@echo Directory: time-date
	@echo Project: Makefile.Simple_Time_Date_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Simple_Time_Date_Server.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd time
	@echo Directory: time
	@echo Project: Makefile.time_IDL.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.time_IDL.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd time
	@echo Directory: time
	@echo Project: Makefile.time_client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.time_client.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd time
	@echo Directory: time
	@echo Project: Makefile.time_server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.time_server.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)

bank_IDL:
	@cd bank
	@echo Directory: bank
	@echo Project: Makefile.bank_IDL.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.bank_IDL.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

bank_client: bank_IDL
	@cd bank
	@echo Directory: bank
	@echo Project: Makefile.bank_client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.bank_client.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

bank_server: bank_IDL
	@cd bank
	@echo Directory: bank
	@echo Project: Makefile.bank_server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.bank_server.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

chat_IDL:
	@cd chat
	@echo Directory: chat
	@echo Project: Makefile.chat_IDL.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.chat_IDL.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

chat_client: chat_IDL
	@cd chat
	@echo Directory: chat
	@echo Project: Makefile.chat_client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.chat_client.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

chat_server: chat_IDL
	@cd chat
	@echo Directory: chat
	@echo Project: Makefile.chat_server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.chat_server.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

echo_IDL:
	@cd echo
	@echo Directory: echo
	@echo Project: Makefile.echo_IDL.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.echo_IDL.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

echo_client: echo_IDL
	@cd echo
	@echo Directory: echo
	@echo Project: Makefile.echo_client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.echo_client.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

echo_server: echo_IDL
	@cd echo
	@echo Directory: echo
	@echo Project: Makefile.echo_server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.echo_server.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

grid_IDL:
	@cd grid
	@echo Directory: grid
	@echo Project: Makefile.grid_IDL.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.grid_IDL.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

grid_client: grid_IDL
	@cd grid
	@echo Directory: grid
	@echo Project: Makefile.grid_client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.grid_client.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

grid_server: grid_IDL
	@cd grid
	@echo Directory: grid
	@echo Project: Makefile.grid_server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.grid_server.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Simple_Time_Date_Lib:
	@cd time-date
	@echo Directory: time-date
	@echo Project: Makefile.Simple_Time_Date_Lib.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Simple_Time_Date_Lib.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Simple_Time_Date_Client: Simple_Time_Date_Lib
	@cd time-date
	@echo Directory: time-date
	@echo Project: Makefile.Simple_Time_Date_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Simple_Time_Date_Client.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Simple_Time_Date_Server:
	@cd time-date
	@echo Directory: time-date
	@echo Project: Makefile.Simple_Time_Date_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Simple_Time_Date_Server.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

time_IDL:
	@cd time
	@echo Directory: time
	@echo Project: Makefile.time_IDL.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.time_IDL.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

time_client: time_IDL
	@cd time
	@echo Directory: time
	@echo Project: Makefile.time_client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.time_client.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

time_server: time_IDL
	@cd time
	@echo Directory: time
	@echo Project: Makefile.time_server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.time_server.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

project_name_list:
	@echo bank_IDL
	@echo bank_client
	@echo bank_server
	@echo chat_IDL
	@echo chat_client
	@echo chat_server
	@echo echo_IDL
	@echo echo_client
	@echo echo_server
	@echo grid_IDL
	@echo grid_client
	@echo grid_server
	@echo Simple_Time_Date_Client
	@echo Simple_Time_Date_Lib
	@echo Simple_Time_Date_Server
	@echo time_IDL
	@echo time_client
	@echo time_server
