# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.Kokyu_DSRT_Schedulers.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "MUF_SchedulingC.inl" "MUF_SchedulingS.inl" "MUF_SchedulingC.h" "MUF_SchedulingC.cpp" "MIF_SchedulingC.inl" "MIF_SchedulingS.inl" "MIF_SchedulingC.h" "MIF_SchedulingC.cpp" "FP_SchedulingC.inl" "FP_SchedulingS.inl" "FP_SchedulingC.h" "FP_SchedulingC.cpp" "Kokyu_qosC.inl" "Kokyu_qosS.inl" "Kokyu_qosC.h" "Kokyu_qosC.cpp" "EDF_SchedulingC.inl" "EDF_SchedulingS.inl" "EDF_SchedulingC.h" "EDF_SchedulingC.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INTDIR=Debug\Kokyu_DSRT_Schedulers\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) ".\Kokyu_DSRT_Schedulersd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -I"..\..\orbsvcs" -I"..\..\..\Kokyu" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DKOKYU_DSRT_SCHEDULERS_BUILD_DLL -f "Makefile.Kokyu_DSRT_Schedulers.dep" "FP_Scheduler.cpp" "MUF_Scheduler.cpp" "MIF_Scheduler.cpp" "Task_Stats.cpp" "utils.cpp" "Kokyu_qosC.cpp" "FP_SchedulingC.cpp" "MUF_SchedulingC.cpp" "MIF_SchedulingC.cpp" "EDF_SchedulingC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\Kokyu_DSRT_Schedulersd.pdb"
	-@del /f/q ".\Kokyu_DSRT_Schedulersd.dll"
	-@del /f/q "$(OUTDIR)\Kokyu_DSRT_Schedulersd.lib"
	-@del /f/q "$(OUTDIR)\Kokyu_DSRT_Schedulersd.exp"
	-@del /f/q "$(OUTDIR)\Kokyu_DSRT_Schedulersd.ilk"
	-@del /f/q "MUF_SchedulingC.inl"
	-@del /f/q "MUF_SchedulingS.inl"
	-@del /f/q "MUF_SchedulingC.h"
	-@del /f/q "MUF_SchedulingC.cpp"
	-@del /f/q "MIF_SchedulingC.inl"
	-@del /f/q "MIF_SchedulingS.inl"
	-@del /f/q "MIF_SchedulingC.h"
	-@del /f/q "MIF_SchedulingC.cpp"
	-@del /f/q "FP_SchedulingC.inl"
	-@del /f/q "FP_SchedulingS.inl"
	-@del /f/q "FP_SchedulingC.h"
	-@del /f/q "FP_SchedulingC.cpp"
	-@del /f/q "Kokyu_qosC.inl"
	-@del /f/q "Kokyu_qosS.inl"
	-@del /f/q "Kokyu_qosC.h"
	-@del /f/q "Kokyu_qosC.cpp"
	-@del /f/q "EDF_SchedulingC.inl"
	-@del /f/q "EDF_SchedulingS.inl"
	-@del /f/q "EDF_SchedulingC.h"
	-@del /f/q "EDF_SchedulingC.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\Kokyu_DSRT_Schedulers\$(NULL)" mkdir "Debug\Kokyu_DSRT_Schedulers"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\.." /I "..\.." /I "..\..\orbsvcs" /I "..\..\..\Kokyu" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D KOKYU_DSRT_SCHEDULERS_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib Kokyud.lib TAO_CodecFactoryd.lib TAO_PId.lib TAO_RTCORBAd.lib TAO_PortableServerd.lib TAO_PI_Serverd.lib TAO_RTSchedulerd.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:".\Kokyu_DSRT_Schedulersd.pdb" /machine:IA64 /out:".\Kokyu_DSRT_Schedulersd.dll" /implib:"$(OUTDIR)\Kokyu_DSRT_Schedulersd.lib"
LINK32_OBJS= \
	"$(INTDIR)\FP_Scheduler.obj" \
	"$(INTDIR)\MUF_Scheduler.obj" \
	"$(INTDIR)\MIF_Scheduler.obj" \
	"$(INTDIR)\Task_Stats.obj" \
	"$(INTDIR)\utils.obj" \
	"$(INTDIR)\Kokyu_qosC.obj" \
	"$(INTDIR)\FP_SchedulingC.obj" \
	"$(INTDIR)\MUF_SchedulingC.obj" \
	"$(INTDIR)\MIF_SchedulingC.obj" \
	"$(INTDIR)\EDF_SchedulingC.obj"

".\Kokyu_DSRT_Schedulersd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist ".\Kokyu_DSRT_Schedulersd.dll.manifest" mt.exe -manifest ".\Kokyu_DSRT_Schedulersd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=.
INTDIR=Release\Kokyu_DSRT_Schedulers\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) ".\Kokyu_DSRT_Schedulers.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -I"..\..\orbsvcs" -I"..\..\..\Kokyu" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DKOKYU_DSRT_SCHEDULERS_BUILD_DLL -f "Makefile.Kokyu_DSRT_Schedulers.dep" "FP_Scheduler.cpp" "MUF_Scheduler.cpp" "MIF_Scheduler.cpp" "Task_Stats.cpp" "utils.cpp" "Kokyu_qosC.cpp" "FP_SchedulingC.cpp" "MUF_SchedulingC.cpp" "MIF_SchedulingC.cpp" "EDF_SchedulingC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q ".\Kokyu_DSRT_Schedulers.dll"
	-@del /f/q "$(OUTDIR)\Kokyu_DSRT_Schedulers.lib"
	-@del /f/q "$(OUTDIR)\Kokyu_DSRT_Schedulers.exp"
	-@del /f/q "$(OUTDIR)\Kokyu_DSRT_Schedulers.ilk"
	-@del /f/q "MUF_SchedulingC.inl"
	-@del /f/q "MUF_SchedulingS.inl"
	-@del /f/q "MUF_SchedulingC.h"
	-@del /f/q "MUF_SchedulingC.cpp"
	-@del /f/q "MIF_SchedulingC.inl"
	-@del /f/q "MIF_SchedulingS.inl"
	-@del /f/q "MIF_SchedulingC.h"
	-@del /f/q "MIF_SchedulingC.cpp"
	-@del /f/q "FP_SchedulingC.inl"
	-@del /f/q "FP_SchedulingS.inl"
	-@del /f/q "FP_SchedulingC.h"
	-@del /f/q "FP_SchedulingC.cpp"
	-@del /f/q "Kokyu_qosC.inl"
	-@del /f/q "Kokyu_qosS.inl"
	-@del /f/q "Kokyu_qosC.h"
	-@del /f/q "Kokyu_qosC.cpp"
	-@del /f/q "EDF_SchedulingC.inl"
	-@del /f/q "EDF_SchedulingS.inl"
	-@del /f/q "EDF_SchedulingC.h"
	-@del /f/q "EDF_SchedulingC.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\Kokyu_DSRT_Schedulers\$(NULL)" mkdir "Release\Kokyu_DSRT_Schedulers"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\.." /I "..\.." /I "..\..\orbsvcs" /I "..\..\..\Kokyu" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D KOKYU_DSRT_SCHEDULERS_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib Kokyu.lib TAO_CodecFactory.lib TAO_PI.lib TAO_RTCORBA.lib TAO_PortableServer.lib TAO_PI_Server.lib TAO_RTScheduler.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:".\Kokyu_DSRT_Schedulers.dll" /implib:"$(OUTDIR)\Kokyu_DSRT_Schedulers.lib"
LINK32_OBJS= \
	"$(INTDIR)\FP_Scheduler.obj" \
	"$(INTDIR)\MUF_Scheduler.obj" \
	"$(INTDIR)\MIF_Scheduler.obj" \
	"$(INTDIR)\Task_Stats.obj" \
	"$(INTDIR)\utils.obj" \
	"$(INTDIR)\Kokyu_qosC.obj" \
	"$(INTDIR)\FP_SchedulingC.obj" \
	"$(INTDIR)\MUF_SchedulingC.obj" \
	"$(INTDIR)\MIF_SchedulingC.obj" \
	"$(INTDIR)\EDF_SchedulingC.obj"

".\Kokyu_DSRT_Schedulers.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist ".\Kokyu_DSRT_Schedulers.dll.manifest" mt.exe -manifest ".\Kokyu_DSRT_Schedulers.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=.
INTDIR=Static_Debug\Kokyu_DSRT_Schedulers\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\Kokyu_DSRT_Schedulerssd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -I"..\..\orbsvcs" -I"..\..\..\Kokyu" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Kokyu_DSRT_Schedulers.dep" "FP_Scheduler.cpp" "MUF_Scheduler.cpp" "MIF_Scheduler.cpp" "Task_Stats.cpp" "utils.cpp" "Kokyu_qosC.cpp" "FP_SchedulingC.cpp" "MUF_SchedulingC.cpp" "MIF_SchedulingC.cpp" "EDF_SchedulingC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\Kokyu_DSRT_Schedulerssd.lib"
	-@del /f/q "$(OUTDIR)\Kokyu_DSRT_Schedulerssd.exp"
	-@del /f/q "$(OUTDIR)\Kokyu_DSRT_Schedulerssd.ilk"
	-@del /f/q ".\Kokyu_DSRT_Schedulerssd.pdb"
	-@del /f/q "MUF_SchedulingC.inl"
	-@del /f/q "MUF_SchedulingS.inl"
	-@del /f/q "MUF_SchedulingC.h"
	-@del /f/q "MUF_SchedulingC.cpp"
	-@del /f/q "MIF_SchedulingC.inl"
	-@del /f/q "MIF_SchedulingS.inl"
	-@del /f/q "MIF_SchedulingC.h"
	-@del /f/q "MIF_SchedulingC.cpp"
	-@del /f/q "FP_SchedulingC.inl"
	-@del /f/q "FP_SchedulingS.inl"
	-@del /f/q "FP_SchedulingC.h"
	-@del /f/q "FP_SchedulingC.cpp"
	-@del /f/q "Kokyu_qosC.inl"
	-@del /f/q "Kokyu_qosS.inl"
	-@del /f/q "Kokyu_qosC.h"
	-@del /f/q "Kokyu_qosC.cpp"
	-@del /f/q "EDF_SchedulingC.inl"
	-@del /f/q "EDF_SchedulingS.inl"
	-@del /f/q "EDF_SchedulingC.h"
	-@del /f/q "EDF_SchedulingC.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\Kokyu_DSRT_Schedulers\$(NULL)" mkdir "Static_Debug\Kokyu_DSRT_Schedulers"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd".\Kokyu_DSRT_Schedulerssd.pdb" /I "..\..\.." /I "..\.." /I "..\..\orbsvcs" /I "..\..\..\Kokyu" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:".\Kokyu_DSRT_Schedulerssd.lib"
LINK32_OBJS= \
	"$(INTDIR)\FP_Scheduler.obj" \
	"$(INTDIR)\MUF_Scheduler.obj" \
	"$(INTDIR)\MIF_Scheduler.obj" \
	"$(INTDIR)\Task_Stats.obj" \
	"$(INTDIR)\utils.obj" \
	"$(INTDIR)\Kokyu_qosC.obj" \
	"$(INTDIR)\FP_SchedulingC.obj" \
	"$(INTDIR)\MUF_SchedulingC.obj" \
	"$(INTDIR)\MIF_SchedulingC.obj" \
	"$(INTDIR)\EDF_SchedulingC.obj"

"$(OUTDIR)\Kokyu_DSRT_Schedulerssd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\Kokyu_DSRT_Schedulerssd.lib.manifest" mt.exe -manifest "$(OUTDIR)\Kokyu_DSRT_Schedulerssd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=.
INTDIR=Static_Release\Kokyu_DSRT_Schedulers\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\Kokyu_DSRT_Schedulerss.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -I"..\..\orbsvcs" -I"..\..\..\Kokyu" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Kokyu_DSRT_Schedulers.dep" "FP_Scheduler.cpp" "MUF_Scheduler.cpp" "MIF_Scheduler.cpp" "Task_Stats.cpp" "utils.cpp" "Kokyu_qosC.cpp" "FP_SchedulingC.cpp" "MUF_SchedulingC.cpp" "MIF_SchedulingC.cpp" "EDF_SchedulingC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\Kokyu_DSRT_Schedulerss.lib"
	-@del /f/q "$(OUTDIR)\Kokyu_DSRT_Schedulerss.exp"
	-@del /f/q "$(OUTDIR)\Kokyu_DSRT_Schedulerss.ilk"
	-@del /f/q "MUF_SchedulingC.inl"
	-@del /f/q "MUF_SchedulingS.inl"
	-@del /f/q "MUF_SchedulingC.h"
	-@del /f/q "MUF_SchedulingC.cpp"
	-@del /f/q "MIF_SchedulingC.inl"
	-@del /f/q "MIF_SchedulingS.inl"
	-@del /f/q "MIF_SchedulingC.h"
	-@del /f/q "MIF_SchedulingC.cpp"
	-@del /f/q "FP_SchedulingC.inl"
	-@del /f/q "FP_SchedulingS.inl"
	-@del /f/q "FP_SchedulingC.h"
	-@del /f/q "FP_SchedulingC.cpp"
	-@del /f/q "Kokyu_qosC.inl"
	-@del /f/q "Kokyu_qosS.inl"
	-@del /f/q "Kokyu_qosC.h"
	-@del /f/q "Kokyu_qosC.cpp"
	-@del /f/q "EDF_SchedulingC.inl"
	-@del /f/q "EDF_SchedulingS.inl"
	-@del /f/q "EDF_SchedulingC.h"
	-@del /f/q "EDF_SchedulingC.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\Kokyu_DSRT_Schedulers\$(NULL)" mkdir "Static_Release\Kokyu_DSRT_Schedulers"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\.." /I "..\.." /I "..\..\orbsvcs" /I "..\..\..\Kokyu" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:".\Kokyu_DSRT_Schedulerss.lib"
LINK32_OBJS= \
	"$(INTDIR)\FP_Scheduler.obj" \
	"$(INTDIR)\MUF_Scheduler.obj" \
	"$(INTDIR)\MIF_Scheduler.obj" \
	"$(INTDIR)\Task_Stats.obj" \
	"$(INTDIR)\utils.obj" \
	"$(INTDIR)\Kokyu_qosC.obj" \
	"$(INTDIR)\FP_SchedulingC.obj" \
	"$(INTDIR)\MUF_SchedulingC.obj" \
	"$(INTDIR)\MIF_SchedulingC.obj" \
	"$(INTDIR)\EDF_SchedulingC.obj"

"$(OUTDIR)\Kokyu_DSRT_Schedulerss.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\Kokyu_DSRT_Schedulerss.lib.manifest" mt.exe -manifest "$(OUTDIR)\Kokyu_DSRT_Schedulerss.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Kokyu_DSRT_Schedulers.dep")
!INCLUDE "Makefile.Kokyu_DSRT_Schedulers.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="FP_Scheduler.cpp"

"$(INTDIR)\FP_Scheduler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\FP_Scheduler.obj" $(SOURCE)

SOURCE="MUF_Scheduler.cpp"

"$(INTDIR)\MUF_Scheduler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\MUF_Scheduler.obj" $(SOURCE)

SOURCE="MIF_Scheduler.cpp"

"$(INTDIR)\MIF_Scheduler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\MIF_Scheduler.obj" $(SOURCE)

SOURCE="Task_Stats.cpp"

"$(INTDIR)\Task_Stats.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Task_Stats.obj" $(SOURCE)

SOURCE="utils.cpp"

"$(INTDIR)\utils.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\utils.obj" $(SOURCE)

SOURCE="Kokyu_qosC.cpp"

"$(INTDIR)\Kokyu_qosC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Kokyu_qosC.obj" $(SOURCE)

SOURCE="FP_SchedulingC.cpp"

"$(INTDIR)\FP_SchedulingC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\FP_SchedulingC.obj" $(SOURCE)

SOURCE="MUF_SchedulingC.cpp"

"$(INTDIR)\MUF_SchedulingC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\MUF_SchedulingC.obj" $(SOURCE)

SOURCE="MIF_SchedulingC.cpp"

"$(INTDIR)\MIF_SchedulingC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\MIF_SchedulingC.obj" $(SOURCE)

SOURCE="EDF_SchedulingC.cpp"

"$(INTDIR)\EDF_SchedulingC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\EDF_SchedulingC.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="MUF_Scheduling.idl"

InputPath=MUF_Scheduling.idl

"MUF_SchedulingC.inl" "MUF_SchedulingS.inl" "MUF_SchedulingC.h" "MUF_SchedulingC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-MUF_Scheduling_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -Wb,export_macro=Kokyu_DSRT_Schedulers_Export -Wb,export_include=Kokyu_dsrt_schedulers_export.h "$(InputPath)"
<<

SOURCE="MIF_Scheduling.idl"

InputPath=MIF_Scheduling.idl

"MIF_SchedulingC.inl" "MIF_SchedulingS.inl" "MIF_SchedulingC.h" "MIF_SchedulingC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-MIF_Scheduling_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -Wb,export_macro=Kokyu_DSRT_Schedulers_Export -Wb,export_include=Kokyu_dsrt_schedulers_export.h "$(InputPath)"
<<

SOURCE="FP_Scheduling.idl"

InputPath=FP_Scheduling.idl

"FP_SchedulingC.inl" "FP_SchedulingS.inl" "FP_SchedulingC.h" "FP_SchedulingC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-FP_Scheduling_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -Wb,export_macro=Kokyu_DSRT_Schedulers_Export -Wb,export_include=Kokyu_dsrt_schedulers_export.h "$(InputPath)"
<<

SOURCE="Kokyu_qos.idl"

InputPath=Kokyu_qos.idl

"Kokyu_qosC.inl" "Kokyu_qosS.inl" "Kokyu_qosC.h" "Kokyu_qosC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Kokyu_qos_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -Wb,export_macro=Kokyu_DSRT_Schedulers_Export -Wb,export_include=Kokyu_dsrt_schedulers_export.h "$(InputPath)"
<<

SOURCE="EDF_Scheduling.idl"

InputPath=EDF_Scheduling.idl

"EDF_SchedulingC.inl" "EDF_SchedulingS.inl" "EDF_SchedulingC.h" "EDF_SchedulingC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-EDF_Scheduling_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -Wb,export_macro=Kokyu_DSRT_Schedulers_Export -Wb,export_include=Kokyu_dsrt_schedulers_export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="MUF_Scheduling.idl"

InputPath=MUF_Scheduling.idl

"MUF_SchedulingC.inl" "MUF_SchedulingS.inl" "MUF_SchedulingC.h" "MUF_SchedulingC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-MUF_Scheduling_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -Wb,export_macro=Kokyu_DSRT_Schedulers_Export -Wb,export_include=Kokyu_dsrt_schedulers_export.h "$(InputPath)"
<<

SOURCE="MIF_Scheduling.idl"

InputPath=MIF_Scheduling.idl

"MIF_SchedulingC.inl" "MIF_SchedulingS.inl" "MIF_SchedulingC.h" "MIF_SchedulingC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-MIF_Scheduling_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -Wb,export_macro=Kokyu_DSRT_Schedulers_Export -Wb,export_include=Kokyu_dsrt_schedulers_export.h "$(InputPath)"
<<

SOURCE="FP_Scheduling.idl"

InputPath=FP_Scheduling.idl

"FP_SchedulingC.inl" "FP_SchedulingS.inl" "FP_SchedulingC.h" "FP_SchedulingC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-FP_Scheduling_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -Wb,export_macro=Kokyu_DSRT_Schedulers_Export -Wb,export_include=Kokyu_dsrt_schedulers_export.h "$(InputPath)"
<<

SOURCE="Kokyu_qos.idl"

InputPath=Kokyu_qos.idl

"Kokyu_qosC.inl" "Kokyu_qosS.inl" "Kokyu_qosC.h" "Kokyu_qosC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Kokyu_qos_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -Wb,export_macro=Kokyu_DSRT_Schedulers_Export -Wb,export_include=Kokyu_dsrt_schedulers_export.h "$(InputPath)"
<<

SOURCE="EDF_Scheduling.idl"

InputPath=EDF_Scheduling.idl

"EDF_SchedulingC.inl" "EDF_SchedulingS.inl" "EDF_SchedulingC.h" "EDF_SchedulingC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-EDF_Scheduling_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -Wb,export_macro=Kokyu_DSRT_Schedulers_Export -Wb,export_include=Kokyu_dsrt_schedulers_export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="MUF_Scheduling.idl"

InputPath=MUF_Scheduling.idl

"MUF_SchedulingC.inl" "MUF_SchedulingS.inl" "MUF_SchedulingC.h" "MUF_SchedulingC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-MUF_Scheduling_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -Wb,export_macro=Kokyu_DSRT_Schedulers_Export -Wb,export_include=Kokyu_dsrt_schedulers_export.h "$(InputPath)"
<<

SOURCE="MIF_Scheduling.idl"

InputPath=MIF_Scheduling.idl

"MIF_SchedulingC.inl" "MIF_SchedulingS.inl" "MIF_SchedulingC.h" "MIF_SchedulingC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-MIF_Scheduling_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -Wb,export_macro=Kokyu_DSRT_Schedulers_Export -Wb,export_include=Kokyu_dsrt_schedulers_export.h "$(InputPath)"
<<

SOURCE="FP_Scheduling.idl"

InputPath=FP_Scheduling.idl

"FP_SchedulingC.inl" "FP_SchedulingS.inl" "FP_SchedulingC.h" "FP_SchedulingC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-FP_Scheduling_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -Wb,export_macro=Kokyu_DSRT_Schedulers_Export -Wb,export_include=Kokyu_dsrt_schedulers_export.h "$(InputPath)"
<<

SOURCE="Kokyu_qos.idl"

InputPath=Kokyu_qos.idl

"Kokyu_qosC.inl" "Kokyu_qosS.inl" "Kokyu_qosC.h" "Kokyu_qosC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Kokyu_qos_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -Wb,export_macro=Kokyu_DSRT_Schedulers_Export -Wb,export_include=Kokyu_dsrt_schedulers_export.h "$(InputPath)"
<<

SOURCE="EDF_Scheduling.idl"

InputPath=EDF_Scheduling.idl

"EDF_SchedulingC.inl" "EDF_SchedulingS.inl" "EDF_SchedulingC.h" "EDF_SchedulingC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-EDF_Scheduling_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -Wb,export_macro=Kokyu_DSRT_Schedulers_Export -Wb,export_include=Kokyu_dsrt_schedulers_export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="MUF_Scheduling.idl"

InputPath=MUF_Scheduling.idl

"MUF_SchedulingC.inl" "MUF_SchedulingS.inl" "MUF_SchedulingC.h" "MUF_SchedulingC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-MUF_Scheduling_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -Wb,export_macro=Kokyu_DSRT_Schedulers_Export -Wb,export_include=Kokyu_dsrt_schedulers_export.h "$(InputPath)"
<<

SOURCE="MIF_Scheduling.idl"

InputPath=MIF_Scheduling.idl

"MIF_SchedulingC.inl" "MIF_SchedulingS.inl" "MIF_SchedulingC.h" "MIF_SchedulingC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-MIF_Scheduling_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -Wb,export_macro=Kokyu_DSRT_Schedulers_Export -Wb,export_include=Kokyu_dsrt_schedulers_export.h "$(InputPath)"
<<

SOURCE="FP_Scheduling.idl"

InputPath=FP_Scheduling.idl

"FP_SchedulingC.inl" "FP_SchedulingS.inl" "FP_SchedulingC.h" "FP_SchedulingC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-FP_Scheduling_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -Wb,export_macro=Kokyu_DSRT_Schedulers_Export -Wb,export_include=Kokyu_dsrt_schedulers_export.h "$(InputPath)"
<<

SOURCE="Kokyu_qos.idl"

InputPath=Kokyu_qos.idl

"Kokyu_qosC.inl" "Kokyu_qosS.inl" "Kokyu_qosC.h" "Kokyu_qosC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Kokyu_qos_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -Wb,export_macro=Kokyu_DSRT_Schedulers_Export -Wb,export_include=Kokyu_dsrt_schedulers_export.h "$(InputPath)"
<<

SOURCE="EDF_Scheduling.idl"

InputPath=EDF_Scheduling.idl

"EDF_SchedulingC.inl" "EDF_SchedulingS.inl" "EDF_SchedulingC.h" "EDF_SchedulingC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-EDF_Scheduling_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -Wb,export_macro=Kokyu_DSRT_Schedulers_Export -Wb,export_include=Kokyu_dsrt_schedulers_export.h "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Kokyu_DSRT_Schedulers.dep")
	@echo Using "Makefile.Kokyu_DSRT_Schedulers.dep"
!ELSE
	@echo Warning: cannot find "Makefile.Kokyu_DSRT_Schedulers.dep"
!ENDIF
!ENDIF

