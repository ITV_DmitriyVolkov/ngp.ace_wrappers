// $Id: AnswerListener.java 14 2007-02-01 15:49:12Z mitza $
// 
// = FILENAME
//    AnswerListener.java
//
// = AUTHOR
//    Michael Kircher (mk1@cs.wustl.edu)
//
// = DESCRIPTION
//   EventListener definition for the Dialog for selecting Observables.
//
// ============================================================================


public class AnswerListener implements java.util.EventListener {
  public void ok (AnswerEvent e) {}
  public void cancel (AnswerEvent e) {}
}
