// $Id: DemoObservable.java 14 2007-02-01 15:49:12Z mitza $
// 
// = FILENAME
//    DemoObservable.java
//
// = AUTHOR
//    Michael Kircher (mk1@cs.wustl.edu)
//
// = DESCRIPTION
//   This class servers as the core class of the simulation demo
//
// ============================================================================



public abstract class DemoObservable extends java.util.Observable {

  public abstract int getProperty ();
}
