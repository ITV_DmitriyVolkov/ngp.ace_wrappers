# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.MIF_Scheduler_Test.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Console Application")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "..\MIF_SchedulingC.inl" "..\MIF_SchedulingC.h" "..\MIF_SchedulingC.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INSTALLDIR=.
INTDIR=Debug\MIF_Scheduler_Test\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\test.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\..\orbsvcs" -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -f "Makefile.MIF_Scheduler_Test.dep" "test.cpp" "MIF_DT_Creator.cpp" "MIF_Task.cpp" "MIF_Scheduler.cpp" "..\MIF_SchedulingC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\test.pdb"
	-@del /f/q "$(INSTALLDIR)\test.exe"
	-@del /f/q "$(INSTALLDIR)\test.ilk"
	-@del /f/q "..\MIF_SchedulingC.inl"
	-@del /f/q "..\MIF_SchedulingC.h"
	-@del /f/q "..\MIF_SchedulingC.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\MIF_Scheduler_Test\$(NULL)" mkdir "Debug\MIF_Scheduler_Test"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /I "..\..\..\orbsvcs" /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_CosNamingd.lib TAO_CodecFactoryd.lib TAO_PId.lib TAO_PortableServerd.lib TAO_RTCORBAd.lib TAO_RTPortableServerd.lib TAO_PI_Serverd.lib TAO_RTSchedulerd.lib RTSchedSynchd.lib Jobd.lib RTSchedTestLibd.lib /libpath:"." /libpath:"..\..\..\..\lib" /libpath:".." /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\test.pdb" /machine:IA64 /out:"$(INSTALLDIR)\test.exe"
LINK32_OBJS= \
	"$(INTDIR)\test.obj" \
	"$(INTDIR)\MIF_DT_Creator.obj" \
	"$(INTDIR)\MIF_Task.obj" \
	"$(INTDIR)\MIF_Scheduler.obj" \
	"$(INTDIR)\dotdot\MIF_SchedulingC.obj"

"$(INSTALLDIR)\test.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\test.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\test.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=Release
INSTALLDIR=Release
INTDIR=Release\MIF_Scheduler_Test\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\test.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\..\orbsvcs" -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -f "Makefile.MIF_Scheduler_Test.dep" "test.cpp" "MIF_DT_Creator.cpp" "MIF_Task.cpp" "MIF_Scheduler.cpp" "..\MIF_SchedulingC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\test.exe"
	-@del /f/q "$(INSTALLDIR)\test.ilk"
	-@del /f/q "..\MIF_SchedulingC.inl"
	-@del /f/q "..\MIF_SchedulingC.h"
	-@del /f/q "..\MIF_SchedulingC.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\MIF_Scheduler_Test\$(NULL)" mkdir "Release\MIF_Scheduler_Test"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\.." /I "..\..\.." /I "..\..\..\orbsvcs" /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_CosNaming.lib TAO_CodecFactory.lib TAO_PI.lib TAO_PortableServer.lib TAO_RTCORBA.lib TAO_RTPortableServer.lib TAO_PI_Server.lib TAO_RTScheduler.lib RTSchedSynch.lib Job.lib RTSchedTestLib.lib /libpath:"." /libpath:"..\..\..\..\lib" /libpath:".." /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\test.exe"
LINK32_OBJS= \
	"$(INTDIR)\test.obj" \
	"$(INTDIR)\MIF_DT_Creator.obj" \
	"$(INTDIR)\MIF_Task.obj" \
	"$(INTDIR)\MIF_Scheduler.obj" \
	"$(INTDIR)\dotdot\MIF_SchedulingC.obj"

"$(INSTALLDIR)\test.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\test.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\test.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=Static_Debug
INSTALLDIR=Static_Debug
INTDIR=Static_Debug\MIF_Scheduler_Test\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\test.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\..\orbsvcs" -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.MIF_Scheduler_Test.dep" "test.cpp" "MIF_DT_Creator.cpp" "MIF_Task.cpp" "MIF_Scheduler.cpp" "..\MIF_SchedulingC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\test.pdb"
	-@del /f/q "$(INSTALLDIR)\test.exe"
	-@del /f/q "$(INSTALLDIR)\test.ilk"
	-@del /f/q "..\MIF_SchedulingC.inl"
	-@del /f/q "..\MIF_SchedulingC.h"
	-@del /f/q "..\MIF_SchedulingC.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\MIF_Scheduler_Test\$(NULL)" mkdir "Static_Debug\MIF_Scheduler_Test"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /I "..\..\..\orbsvcs" /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEsd.lib TAOsd.lib TAO_AnyTypeCodesd.lib TAO_CosNamingsd.lib TAO_CodecFactorysd.lib TAO_PIsd.lib TAO_PortableServersd.lib TAO_RTCORBAsd.lib TAO_RTPortableServersd.lib TAO_PI_Serversd.lib TAO_RTSchedulersd.lib RTSchedSynchsd.lib Jobsd.lib RTSchedTestLibsd.lib /libpath:"." /libpath:"..\..\..\..\lib" /libpath:".." /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\test.pdb" /machine:IA64 /out:"$(INSTALLDIR)\test.exe"
LINK32_OBJS= \
	"$(INTDIR)\test.obj" \
	"$(INTDIR)\MIF_DT_Creator.obj" \
	"$(INTDIR)\MIF_Task.obj" \
	"$(INTDIR)\MIF_Scheduler.obj" \
	"$(INTDIR)\dotdot\MIF_SchedulingC.obj"

"$(INSTALLDIR)\test.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\test.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\test.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=Static_Release
INSTALLDIR=Static_Release
INTDIR=Static_Release\MIF_Scheduler_Test\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\test.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\..\orbsvcs" -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.MIF_Scheduler_Test.dep" "test.cpp" "MIF_DT_Creator.cpp" "MIF_Task.cpp" "MIF_Scheduler.cpp" "..\MIF_SchedulingC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\test.exe"
	-@del /f/q "$(INSTALLDIR)\test.ilk"
	-@del /f/q "..\MIF_SchedulingC.inl"
	-@del /f/q "..\MIF_SchedulingC.h"
	-@del /f/q "..\MIF_SchedulingC.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\MIF_Scheduler_Test\$(NULL)" mkdir "Static_Release\MIF_Scheduler_Test"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\.." /I "..\..\.." /I "..\..\..\orbsvcs" /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEs.lib TAOs.lib TAO_AnyTypeCodes.lib TAO_CosNamings.lib TAO_CodecFactorys.lib TAO_PIs.lib TAO_PortableServers.lib TAO_RTCORBAs.lib TAO_RTPortableServers.lib TAO_PI_Servers.lib TAO_RTSchedulers.lib RTSchedSynchs.lib Jobs.lib RTSchedTestLibs.lib /libpath:"." /libpath:"..\..\..\..\lib" /libpath:".." /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\test.exe"
LINK32_OBJS= \
	"$(INTDIR)\test.obj" \
	"$(INTDIR)\MIF_DT_Creator.obj" \
	"$(INTDIR)\MIF_Task.obj" \
	"$(INTDIR)\MIF_Scheduler.obj" \
	"$(INTDIR)\dotdot\MIF_SchedulingC.obj"

"$(INSTALLDIR)\test.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\test.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\test.exe.manifest" -outputresource:$@;1

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.MIF_Scheduler_Test.dep")
!INCLUDE "Makefile.MIF_Scheduler_Test.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="test.cpp"

"$(INTDIR)\test.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\test.obj" $(SOURCE)

SOURCE="MIF_DT_Creator.cpp"

"$(INTDIR)\MIF_DT_Creator.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\MIF_DT_Creator.obj" $(SOURCE)

SOURCE="MIF_Task.cpp"

"$(INTDIR)\MIF_Task.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\MIF_Task.obj" $(SOURCE)

SOURCE="MIF_Scheduler.cpp"

"$(INTDIR)\MIF_Scheduler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\MIF_Scheduler.obj" $(SOURCE)

SOURCE="..\MIF_SchedulingC.cpp"

"$(INTDIR)\dotdot\MIF_SchedulingC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\dotdot\$(NULL)" mkdir "$(INTDIR)\dotdot\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\dotdot\MIF_SchedulingC.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="..\MIF_Scheduling.idl"

InputPath=..\MIF_Scheduling.idl

"..\MIF_SchedulingC.inl" "..\MIF_SchedulingC.h" "..\MIF_SchedulingC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-___MIF_Scheduling_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -SS -St -o .. "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="..\MIF_Scheduling.idl"

InputPath=..\MIF_Scheduling.idl

"..\MIF_SchedulingC.inl" "..\MIF_SchedulingC.h" "..\MIF_SchedulingC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-___MIF_Scheduling_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -SS -St -o .. "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="..\MIF_Scheduling.idl"

InputPath=..\MIF_Scheduling.idl

"..\MIF_SchedulingC.inl" "..\MIF_SchedulingC.h" "..\MIF_SchedulingC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-___MIF_Scheduling_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -SS -St -o .. "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="..\MIF_Scheduling.idl"

InputPath=..\MIF_Scheduling.idl

"..\MIF_SchedulingC.inl" "..\MIF_SchedulingC.h" "..\MIF_SchedulingC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-___MIF_Scheduling_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -SS -St -o .. "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.MIF_Scheduler_Test.dep")
	@echo Using "Makefile.MIF_Scheduler_Test.dep"
!ELSE
	@echo Warning: cannot find "Makefile.MIF_Scheduler_Test.dep"
!ENDIF
!ENDIF

