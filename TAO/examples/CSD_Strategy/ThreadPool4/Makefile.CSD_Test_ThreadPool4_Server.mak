# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.CSD_Test_ThreadPool4_Server.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Console Application")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "CallbackC.inl" "CallbackS.inl" "CallbackC.h" "CallbackS.h" "CallbackC.cpp" "CallbackS.cpp" "FooC.inl" "FooS.inl" "FooC.h" "FooS.h" "FooC.cpp" "FooS.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INSTALLDIR=.
INTDIR=Debug\CSD_Test_ThreadPool4_Server\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\server_main.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -f "Makefile.CSD_Test_ThreadPool4_Server.dep" "CallbackC.cpp" "CallbackS.cpp" "FooC.cpp" "FooS.cpp" "Foo_i.cpp" "Callback_i.cpp" "ServerApp.cpp" "server_main.cpp" "ClientTask.cpp" "OrbShutdownTask.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\server_main.pdb"
	-@del /f/q "$(INSTALLDIR)\server_main.exe"
	-@del /f/q "$(INSTALLDIR)\server_main.ilk"
	-@del /f/q "CallbackC.inl"
	-@del /f/q "CallbackS.inl"
	-@del /f/q "CallbackC.h"
	-@del /f/q "CallbackS.h"
	-@del /f/q "CallbackC.cpp"
	-@del /f/q "CallbackS.cpp"
	-@del /f/q "FooC.inl"
	-@del /f/q "FooS.inl"
	-@del /f/q "FooC.h"
	-@del /f/q "FooS.h"
	-@del /f/q "FooC.cpp"
	-@del /f/q "FooS.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\CSD_Test_ThreadPool4_Server\$(NULL)" mkdir "Debug\CSD_Test_ThreadPool4_Server"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_PortableServerd.lib TAO_CodecFactoryd.lib TAO_PId.lib TAO_CSD_Frameworkd.lib TAO_CSD_ThreadPoold.lib TAO_Valuetyped.lib TAO_Messagingd.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\server_main.pdb" /machine:IA64 /out:"$(INSTALLDIR)\server_main.exe"
LINK32_OBJS= \
	"$(INTDIR)\CallbackC.obj" \
	"$(INTDIR)\CallbackS.obj" \
	"$(INTDIR)\FooC.obj" \
	"$(INTDIR)\FooS.obj" \
	"$(INTDIR)\Foo_i.obj" \
	"$(INTDIR)\Callback_i.obj" \
	"$(INTDIR)\ServerApp.obj" \
	"$(INTDIR)\server_main.obj" \
	"$(INTDIR)\ClientTask.obj" \
	"$(INTDIR)\OrbShutdownTask.obj"

"$(INSTALLDIR)\server_main.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\server_main.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\server_main.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=Release
INSTALLDIR=Release
INTDIR=Release\CSD_Test_ThreadPool4_Server\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\server_main.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -f "Makefile.CSD_Test_ThreadPool4_Server.dep" "CallbackC.cpp" "CallbackS.cpp" "FooC.cpp" "FooS.cpp" "Foo_i.cpp" "Callback_i.cpp" "ServerApp.cpp" "server_main.cpp" "ClientTask.cpp" "OrbShutdownTask.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\server_main.exe"
	-@del /f/q "$(INSTALLDIR)\server_main.ilk"
	-@del /f/q "CallbackC.inl"
	-@del /f/q "CallbackS.inl"
	-@del /f/q "CallbackC.h"
	-@del /f/q "CallbackS.h"
	-@del /f/q "CallbackC.cpp"
	-@del /f/q "CallbackS.cpp"
	-@del /f/q "FooC.inl"
	-@del /f/q "FooS.inl"
	-@del /f/q "FooC.h"
	-@del /f/q "FooS.h"
	-@del /f/q "FooC.cpp"
	-@del /f/q "FooS.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\CSD_Test_ThreadPool4_Server\$(NULL)" mkdir "Release\CSD_Test_ThreadPool4_Server"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\.." /I "..\..\.." /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_PortableServer.lib TAO_CodecFactory.lib TAO_PI.lib TAO_CSD_Framework.lib TAO_CSD_ThreadPool.lib TAO_Valuetype.lib TAO_Messaging.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\server_main.exe"
LINK32_OBJS= \
	"$(INTDIR)\CallbackC.obj" \
	"$(INTDIR)\CallbackS.obj" \
	"$(INTDIR)\FooC.obj" \
	"$(INTDIR)\FooS.obj" \
	"$(INTDIR)\Foo_i.obj" \
	"$(INTDIR)\Callback_i.obj" \
	"$(INTDIR)\ServerApp.obj" \
	"$(INTDIR)\server_main.obj" \
	"$(INTDIR)\ClientTask.obj" \
	"$(INTDIR)\OrbShutdownTask.obj"

"$(INSTALLDIR)\server_main.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\server_main.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\server_main.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=Static_Debug
INSTALLDIR=Static_Debug
INTDIR=Static_Debug\CSD_Test_ThreadPool4_Server\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\server_main.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CSD_Test_ThreadPool4_Server.dep" "CallbackC.cpp" "CallbackS.cpp" "FooC.cpp" "FooS.cpp" "Foo_i.cpp" "Callback_i.cpp" "ServerApp.cpp" "server_main.cpp" "ClientTask.cpp" "OrbShutdownTask.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\server_main.pdb"
	-@del /f/q "$(INSTALLDIR)\server_main.exe"
	-@del /f/q "$(INSTALLDIR)\server_main.ilk"
	-@del /f/q "CallbackC.inl"
	-@del /f/q "CallbackS.inl"
	-@del /f/q "CallbackC.h"
	-@del /f/q "CallbackS.h"
	-@del /f/q "CallbackC.cpp"
	-@del /f/q "CallbackS.cpp"
	-@del /f/q "FooC.inl"
	-@del /f/q "FooS.inl"
	-@del /f/q "FooC.h"
	-@del /f/q "FooS.h"
	-@del /f/q "FooC.cpp"
	-@del /f/q "FooS.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\CSD_Test_ThreadPool4_Server\$(NULL)" mkdir "Static_Debug\CSD_Test_ThreadPool4_Server"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEsd.lib TAOsd.lib TAO_AnyTypeCodesd.lib TAO_PortableServersd.lib TAO_CodecFactorysd.lib TAO_PIsd.lib TAO_CSD_Frameworksd.lib TAO_CSD_ThreadPoolsd.lib TAO_Valuetypesd.lib TAO_Messagingsd.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\server_main.pdb" /machine:IA64 /out:"$(INSTALLDIR)\server_main.exe"
LINK32_OBJS= \
	"$(INTDIR)\CallbackC.obj" \
	"$(INTDIR)\CallbackS.obj" \
	"$(INTDIR)\FooC.obj" \
	"$(INTDIR)\FooS.obj" \
	"$(INTDIR)\Foo_i.obj" \
	"$(INTDIR)\Callback_i.obj" \
	"$(INTDIR)\ServerApp.obj" \
	"$(INTDIR)\server_main.obj" \
	"$(INTDIR)\ClientTask.obj" \
	"$(INTDIR)\OrbShutdownTask.obj"

"$(INSTALLDIR)\server_main.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\server_main.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\server_main.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=Static_Release
INSTALLDIR=Static_Release
INTDIR=Static_Release\CSD_Test_ThreadPool4_Server\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\server_main.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CSD_Test_ThreadPool4_Server.dep" "CallbackC.cpp" "CallbackS.cpp" "FooC.cpp" "FooS.cpp" "Foo_i.cpp" "Callback_i.cpp" "ServerApp.cpp" "server_main.cpp" "ClientTask.cpp" "OrbShutdownTask.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\server_main.exe"
	-@del /f/q "$(INSTALLDIR)\server_main.ilk"
	-@del /f/q "CallbackC.inl"
	-@del /f/q "CallbackS.inl"
	-@del /f/q "CallbackC.h"
	-@del /f/q "CallbackS.h"
	-@del /f/q "CallbackC.cpp"
	-@del /f/q "CallbackS.cpp"
	-@del /f/q "FooC.inl"
	-@del /f/q "FooS.inl"
	-@del /f/q "FooC.h"
	-@del /f/q "FooS.h"
	-@del /f/q "FooC.cpp"
	-@del /f/q "FooS.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\CSD_Test_ThreadPool4_Server\$(NULL)" mkdir "Static_Release\CSD_Test_ThreadPool4_Server"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\.." /I "..\..\.." /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEs.lib TAOs.lib TAO_AnyTypeCodes.lib TAO_PortableServers.lib TAO_CodecFactorys.lib TAO_PIs.lib TAO_CSD_Frameworks.lib TAO_CSD_ThreadPools.lib TAO_Valuetypes.lib TAO_Messagings.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\server_main.exe"
LINK32_OBJS= \
	"$(INTDIR)\CallbackC.obj" \
	"$(INTDIR)\CallbackS.obj" \
	"$(INTDIR)\FooC.obj" \
	"$(INTDIR)\FooS.obj" \
	"$(INTDIR)\Foo_i.obj" \
	"$(INTDIR)\Callback_i.obj" \
	"$(INTDIR)\ServerApp.obj" \
	"$(INTDIR)\server_main.obj" \
	"$(INTDIR)\ClientTask.obj" \
	"$(INTDIR)\OrbShutdownTask.obj"

"$(INSTALLDIR)\server_main.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\server_main.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\server_main.exe.manifest" -outputresource:$@;1

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CSD_Test_ThreadPool4_Server.dep")
!INCLUDE "Makefile.CSD_Test_ThreadPool4_Server.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="CallbackC.cpp"

"$(INTDIR)\CallbackC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CallbackC.obj" $(SOURCE)

SOURCE="CallbackS.cpp"

"$(INTDIR)\CallbackS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CallbackS.obj" $(SOURCE)

SOURCE="FooC.cpp"

"$(INTDIR)\FooC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\FooC.obj" $(SOURCE)

SOURCE="FooS.cpp"

"$(INTDIR)\FooS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\FooS.obj" $(SOURCE)

SOURCE="Foo_i.cpp"

"$(INTDIR)\Foo_i.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Foo_i.obj" $(SOURCE)

SOURCE="Callback_i.cpp"

"$(INTDIR)\Callback_i.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Callback_i.obj" $(SOURCE)

SOURCE="ServerApp.cpp"

"$(INTDIR)\ServerApp.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ServerApp.obj" $(SOURCE)

SOURCE="server_main.cpp"

"$(INTDIR)\server_main.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\server_main.obj" $(SOURCE)

SOURCE="ClientTask.cpp"

"$(INTDIR)\ClientTask.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ClientTask.obj" $(SOURCE)

SOURCE="OrbShutdownTask.cpp"

"$(INTDIR)\OrbShutdownTask.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\OrbShutdownTask.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="Callback.idl"

InputPath=Callback.idl

"CallbackC.inl" "CallbackS.inl" "CallbackC.h" "CallbackS.h" "CallbackC.cpp" "CallbackS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Callback_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. "$(InputPath)"
<<

SOURCE="Foo.idl"

InputPath=Foo.idl

"FooC.inl" "FooS.inl" "FooC.h" "FooS.h" "FooC.cpp" "FooS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Foo_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="Callback.idl"

InputPath=Callback.idl

"CallbackC.inl" "CallbackS.inl" "CallbackC.h" "CallbackS.h" "CallbackC.cpp" "CallbackS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Callback_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. "$(InputPath)"
<<

SOURCE="Foo.idl"

InputPath=Foo.idl

"FooC.inl" "FooS.inl" "FooC.h" "FooS.h" "FooC.cpp" "FooS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Foo_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="Callback.idl"

InputPath=Callback.idl

"CallbackC.inl" "CallbackS.inl" "CallbackC.h" "CallbackS.h" "CallbackC.cpp" "CallbackS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Callback_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. "$(InputPath)"
<<

SOURCE="Foo.idl"

InputPath=Foo.idl

"FooC.inl" "FooS.inl" "FooC.h" "FooS.h" "FooC.cpp" "FooS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Foo_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="Callback.idl"

InputPath=Callback.idl

"CallbackC.inl" "CallbackS.inl" "CallbackC.h" "CallbackS.h" "CallbackC.cpp" "CallbackS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Callback_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. "$(InputPath)"
<<

SOURCE="Foo.idl"

InputPath=Foo.idl

"FooC.inl" "FooS.inl" "FooC.h" "FooS.h" "FooC.cpp" "FooS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Foo_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CSD_Test_ThreadPool4_Server.dep")
	@echo Using "Makefile.CSD_Test_ThreadPool4_Server.dep"
!ELSE
	@echo Warning: cannot find "Makefile.CSD_Test_ThreadPool4_Server.dep"
!ENDIF
!ENDIF

