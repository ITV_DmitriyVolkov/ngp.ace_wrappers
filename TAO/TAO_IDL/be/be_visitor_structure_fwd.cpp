//
// $Id: be_visitor_structure_fwd.cpp 14 2007-02-01 15:49:12Z mitza $
//

// ============================================================================
//
// = LIBRARY
//    TAO IDL
//
// = FILENAME
//    be_visitor_structure_fwd.cpp
//
// = DESCRIPTION
//    Visitors for generation of code for be_structure_fwd
//
// = AUTHOR
//    Jeff Parsons
//
// ============================================================================

#include "be_structure_fwd.h"
#include "be_structure.h"

#include "be_visitor_structure_fwd.h"
#include "be_visitor_context.h"
#include "be_helper.h"

#include "be_visitor_structure_fwd/structure_fwd_ch.cpp"

ACE_RCSID (be, 
           be_visitor_structure_fwd, 
           "$Id: be_visitor_structure_fwd.cpp 14 2007-02-01 15:49:12Z mitza $")
