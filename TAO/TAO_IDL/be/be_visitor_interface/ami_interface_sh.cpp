#include <sstream>

be_visitor_ami_default_handler_sh::be_visitor_ami_default_handler_sh(be_visitor_context *ctx)
  : be_visitor_interface_sh (ctx)
{ }

be_visitor_ami_default_handler_sh::~be_visitor_ami_default_handler_sh(void)
{ }


bool be_visitor_ami_default_handler_sh::is_ami_handler(ACE_CString const& name)
{
  // check interface name is AMI_.+Handler
  return
    name.length() > 11 && // "AMI_?Handler
    name.substr(0, 4) == "AMI_" &&
    name.substr(name.length() - 7, 7) == "Handler"
  ;
}

namespace
{
  ACE_CString generate_default_ami_handler_class_name(const char* ami_handler_name)
  {
    return ACE_CString(ami_handler_name) + "Default";
  }

  ACE_CString to_string(UTL_ScopedName* name)
  {
    ACE_CString result;

    for (UTL_ScopedNameActiveIterator it(name); !it.is_done(); it.next())
    {
      if (result.length() > 0)
        result += "::";

      result += it.item()->get_string();
    }

    return result;
  }
} // anonymous namespace

int be_visitor_ami_default_handler_sh::visit_interface(be_interface *node)
{
  TAO_OutStream& os = *this->ctx_->stream();

  os << be_nl << be_nl << "// TAO_IDL - Generated from" << be_nl
      << "// " << __FILE__ << ":" << __LINE__ << be_nl
      << "// AxxonSoft custom generated AMI handler" << be_nl << be_nl;

  ACE_CString iface_class_name;

  // We shall have a POA_ prefix only if we are at the topmost level.
  if (!node->is_nested ())
    {
      // We are outermost.
      iface_class_name += "POA_";
      iface_class_name += node->local_name ();
    }
  else
    {
      iface_class_name +=  node->local_name ();
    }

  ACE_CString class_name = generate_default_ami_handler_class_name(iface_class_name.c_str());

  os << "class " << be_global->skel_export_macro() << " " << class_name.c_str()
     << ":" << be_nl
     << "    public virtual " << iface_class_name.c_str()
  ;

  {
    auto n_parents = node->n_inherits();
    for (auto i = 0; i < n_parents; ++i)
    {
      AST_Interface* parent = node->inherits()[i];
      ACE_CString
        parent_full_name = to_string(parent->name()),
        parent_short_name = parent->name()->last_component()->get_string()
      ;

      if (is_ami_handler(parent_short_name))
      {
        os << "," << be_nl
           << "    public virtual ::POA_"
              << generate_default_ami_handler_class_name(parent_full_name.c_str()).c_str();
      }
    }

    os << be_nl;
  }

  os << "{" << be_nl
     << "public:" << be_nl
     ;

  if (this->visit_scope (node) == -1)
  {
    ACE_ERROR_RETURN ((LM_ERROR,
                       "(%N:%l) be_visitor_ami_default_handler_sh:"
                       "visit_interface - "
                       "codegen for scope failed\n"),
                      -1);
  }

  os << "};" << be_nl << be_nl;

  return 0;
}

int be_visitor_ami_default_handler_sh::visit_operation (be_operation *node)
{
  TAO_OutStream& os = *this->ctx_->stream();
  this->ctx_->node (node);

  os << be_nl << be_nl << "// TAO_IDL - Generated from" << be_nl
      << "// " << __FILE__ << ":" << __LINE__ << be_nl << be_nl;

  os << "inline virtual ";

  // STEP I: generate the return type.
  be_type *bt = be_type::narrow_from_decl (node->return_type ());

  if (!bt)
    {
      ACE_ERROR_RETURN ((LM_ERROR,
                         "(%N:%l) be_visitor_ami_default_handler_sh::"
                         "visit_operation - "
                         "Bad return type\n"),
                        -1);
    }

  be_visitor_context ctx (*this->ctx_);
  be_visitor_operation_rettype oro_visitor (&ctx);

  if (bt->accept (&oro_visitor) == -1)
    {
      ACE_ERROR_RETURN ((LM_ERROR,
                         "(%N:%l) be_visitor_ami_default_handler_sh::"
                         "visit_operation - "
                         "codegen for return type failed\n"),
                        -1);
    }

  // STEP 2: generate the operation name
  os << " " << node->local_name ();

  // STEP 3: generate the argument list with the appropriate mapping. For these
  // we grab a visitor that generates the parameter listing
  ctx = *this->ctx_;
  ctx.state (TAO_CodeGen::TAO_OPERATION_AMI_DEFAULT_HANDLER_ARGLIST_SH);
  be_visitor_operation_arglist oa_visitor (&ctx);

  if (node->accept (&oa_visitor) == -1)
    {
      ACE_ERROR_RETURN ((LM_ERROR,
                         "(%N:%l) be_visitor_ami_default_handler_sh::"
                         "visit_operation - "
                         "codegen for argument list failed\n"),
                        -1);
    }

  // function body
  os << be_nl
     << "{" << be_nl
     << "   abort();" << be_nl
     << "}" << be_nl
  ;

  os << be_nl;

  return 0;
}

int be_visitor_ami_default_handler_sh::visit_attribute (be_attribute * /* node */)
{
  return 0;
}
