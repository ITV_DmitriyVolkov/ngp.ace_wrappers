//
// $Id: proxy_brokers_ch.cpp 14 2007-02-01 15:49:12Z mitza $
//

ACE_RCSID (be_visitor_interface,
           proxy_brokers_ch,
           "$Id: proxy_brokers_ch.cpp 14 2007-02-01 15:49:12Z mitza $")

be_visitor_interface_proxy_brokers_ch::be_visitor_interface_proxy_brokers_ch (
    be_visitor_context *ctx
  )
  : be_visitor_interface (ctx)
{
  // No-Op.
}


be_visitor_interface_proxy_brokers_ch::~be_visitor_interface_proxy_brokers_ch (
    void
  )
{
  // No-Op.
}

int
be_visitor_interface_proxy_brokers_ch::visit_interface (be_interface *node)
{
  TAO_OutStream *os = this->ctx_->stream ();

  // Generate Guards.

  // Generate the class declaration.
  *os << "// The Proxy Brokers are used by each interface to get" << be_nl
      << "// the right proxy for performing a call. In the new " << be_nl
      << "// collocation scheme, the proxy to be used can vary on" << be_nl
      << "// a call by call basis." << be_nl;

  // Code Generation for the proxy brokers base class.
  be_visitor_context ctx (*this->ctx_);
  be_visitor_interface_base_proxy_broker_ch bpb_visitor (&ctx);

  if (node->accept (&bpb_visitor) == -1)
    {
      ACE_ERROR_RETURN ((LM_ERROR,
                         "be_visitor_interface_ch::"
                         "visit_interface - "
                         "codegen for Base Proxy Broker class failed\n"),
                        -1);
    }

  ctx = *this->ctx_;
  be_visitor_interface_remote_proxy_broker_ch rpb_visitor (&ctx);

  if (node->accept (&rpb_visitor) == -1)
    {
      ACE_ERROR_RETURN ((LM_ERROR,
                         "be_visitor_interface_ch::"
                         "visit_interface - "
                         "codegen for Remote Proxy Broker class failed\n"),
                        -1);
    }

  return 0;
}

int be_visitor_interface_proxy_brokers_ch::visit_component (
    be_component *node
  )
{
  return this->visit_interface (node);
}

