//
// $Id: be_visitor_exception.cpp 14 2007-02-01 15:49:12Z mitza $
//

// ============================================================================
//
// = LIBRARY
//    TAO IDL
//
// = FILENAME
//    be_visitor_exception.cpp
//
// = DESCRIPTION
//    Visitors for generation of code for Exception
//
// = AUTHOR
//    Aniruddha Gokhale
//
// ============================================================================

#include "be_field.h"
#include "be_exception.h"
#include "be_typedef.h"
#include "be_enum.h"
#include "be_interface.h"
#include "be_interface_fwd.h"
#include "be_valuebox.h"
#include "be_valuetype.h"
#include "be_valuetype_fwd.h"
#include "be_array.h"
#include "be_predefined_type.h"
#include "be_string.h"
#include "be_sequence.h"
#include "be_structure.h"
#include "be_union.h"
#include "be_helper.h"
#include "be_extern.h"

#include "be_visitor_exception.h"
#include "be_visitor_context.h"
#include "be_visitor_field.h"
#include "be_visitor_typecode.h"
#include "ace/Log_Msg.h"

#include "be_visitor_exception/exception.cpp"
#include "be_visitor_exception/exception_ch.cpp"
#include "be_visitor_exception/exception_ci.cpp"
#include "be_visitor_exception/exception_cs.cpp"
#include "be_visitor_exception/exception_ctor.cpp"
#include "be_visitor_exception/ctor_assign.cpp"
#include "be_visitor_exception/any_op_ch.cpp"
#include "be_visitor_exception/any_op_cs.cpp"
#include "be_visitor_exception/cdr_op_ch.cpp"
#include "be_visitor_exception/cdr_op_cs.cpp"

ACE_RCSID (be,
           be_visitor_exception,
           "$Id: be_visitor_exception.cpp 14 2007-02-01 15:49:12Z mitza $")
