/* -*- c++ -*- */
// $Id: be_module.h 935 2008-12-10 21:47:27Z mitza $

// ============================================================================
//
// = LIBRARY
//    TAO IDL
//
// = FILENAME
//    be_module.h
//
// = DESCRIPTION
//    Extension of class AST_Module that provides additional means for C++
//    mapping.
//
// = AUTHOR
//    Copyright 1994-1995 by Sun Microsystems, Inc.
//    and
//    Aniruddha Gokhale
//
// ============================================================================

#ifndef _BE_MODULE_H
#define _BE_MODULE_H

#include "be_scope.h"
#include "be_decl.h"
#include "ast_module.h"

class be_visitor;

class be_module : public virtual AST_Module,
                  public virtual be_scope,
                  public virtual be_decl
{
public:
  be_module (void);
  // Default constructor.

  be_module (UTL_ScopedName *n);
  // Constructor.

  virtual void destroy (void);
  // Cleanup method.

  // Visiting
  virtual int accept (be_visitor *visitor);

  // Narrowing.

  DEF_NARROW_FROM_DECL (be_module);
  DEF_NARROW_FROM_SCOPE (be_module);
};

#endif
