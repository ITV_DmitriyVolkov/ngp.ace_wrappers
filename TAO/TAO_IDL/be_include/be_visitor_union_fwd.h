/* -*- C++ -*- */
//
// $Id: be_visitor_union_fwd.h 14 2007-02-01 15:49:12Z mitza $
//

// ============================================================================
//
// = LIBRARY
//    TAO IDL
//
// = FILENAME
//    be_visitor_union_fwd.h
//
// = DESCRIPTION
//    Concrete visitor for the be_union_fwd class
//
// = AUTHOR
//    Jeff Parsons
//
// ============================================================================

#ifndef TAO_BE_VISITOR_UNION_FWD_H
#define TAO_BE_VISITOR_UNION_FWD_H

#include "be_visitor_decl.h"
#include "be_visitor_union_fwd/union_fwd_ch.h"

#endif /* TAO_BE_VISITOR_UNION_FWD_H */
