/* -*- c++ -*- */
//
// $Id: be_visitor_native.h 14 2007-02-01 15:49:12Z mitza $
//

// ============================================================================
//
// = LIBRARY
//    TAO IDL
//
// = FILENAME
//    be_visitor_native.h
//
// = DESCRIPTION
//    Concrete visitor for the native class
//
// = AUTHOR
//    Johnny Willemsen
//
// ============================================================================

#ifndef TAO_BE_VISITOR_NATIVE_H
#define TAO_BE_VISITOR_NATIVE_H

#include "be_visitor_scope.h"
#include "be_visitor_native/native_ch.h"

#endif /* TAO_BE_VISITOR_NATIVE_H */
