/* -*- C++ -*- */
//
// $Id: be_visitor_interface_fwd.h 14 2007-02-01 15:49:12Z mitza $
//

// ============================================================================
//
// = LIBRARY
//    TAO IDL
//
// = FILENAME
//    be_visitor_interface_fwd.h
//
// = DESCRIPTION
//    Concrete visitor for the interface_fwd class
//
// = AUTHOR
//    Aniruddha Gokhale
//
// ============================================================================

#ifndef TAO_BE_VISITOR_INTERFACE_FWD_H
#define TAO_BE_VISITOR_INTERFACE_FWD_H

#include "be_visitor_decl.h"
#include "be_visitor_interface_fwd/interface_fwd_ch.h"
#include "be_visitor_interface_fwd/cdr_op_ch.h"
#include "be_visitor_interface_fwd/any_op_ch.h"

#endif /* TAO_BE_VISITOR_INTERFACE_FWD_H */
