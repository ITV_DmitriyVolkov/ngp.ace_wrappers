//=============================================================================
/**
 *  @file   ami_interface_sh.h
 *
 *  Specialized interface visitor for generating default AMI handler
 *  implementation
 *
 *  @author Oleg Malashenko <oleg.malashenko@ru.axxonsoft.com>
 */
//=============================================================================
#pragma once

class be_visitor_ami_default_handler_sh: public be_visitor_interface_sh
{
public:
  be_visitor_ami_default_handler_sh (be_visitor_context *ctx);
  ~be_visitor_ami_default_handler_sh (void);

  int visit_interface (be_interface *node);
  int visit_operation (be_operation *node);
  int visit_attribute (be_attribute *node);

  static bool is_ami_handler(ACE_CString const& name);
};
