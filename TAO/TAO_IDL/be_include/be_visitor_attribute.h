/* -*- c++ -*- */
//
// $Id: be_visitor_attribute.h 14 2007-02-01 15:49:12Z mitza $
//

// ============================================================================
//
// = LIBRARY
//    TAO IDL
//
// = FILENAME
//    be_visitor_attribute.h
//
// = DESCRIPTION
//    Concrete visitor for the Attribute class
//
// = AUTHOR
//    Aniruddha Gokhale
//
// ============================================================================

#ifndef TAO_BE_VISITOR_ATTRIBUTE_H
#define TAO_BE_VISITOR_ATTRIBUTE_H

#include "be_visitor_decl.h"
#include "be_visitor_attribute/attribute.h"

#endif
