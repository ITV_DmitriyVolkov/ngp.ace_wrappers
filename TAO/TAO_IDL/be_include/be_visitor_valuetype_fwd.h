/* -*- C++ -*- */
//
// $Id: be_visitor_valuetype_fwd.h 14 2007-02-01 15:49:12Z mitza $
//

// ============================================================================
//
// = LIBRARY
//    TAO IDL
//
// = FILENAME
//    be_visitor_valuetype_fwd.h
//
// = DESCRIPTION
//    Concrete visitor for the valuetype_fwd class
//
// = AUTHOR
//    Torsten Kuepper  <kuepper2@lfa.uni-wuppertal.de>
//    based on code from Aniruddha Gokhale (be_visitor_interface_fwd.h)
//
// ============================================================================

#ifndef TAO_BE_VISITOR_VALUETYPE_FWD_H
#define TAO_BE_VISITOR_VALUETYPE_FWD_H

#include "be_visitor_decl.h"
#include "be_visitor_valuetype_fwd/valuetype_fwd_ch.h"
#include "be_visitor_valuetype_fwd/any_op_ch.h"
#include "be_visitor_valuetype_fwd/cdr_op_ch.h"

#endif /* TAO_BE_VISITOR_VALUETYPE_FWD_H */
