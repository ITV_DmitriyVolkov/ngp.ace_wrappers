/* -*- c++ -*- */
//
// $Id: be_visitor_component.h 14 2007-02-01 15:49:12Z mitza $
//

// ============================================================================
//
// = LIBRARY
//    TAO IDL
//
// = FILENAME
//    be_visitor_component.h
//
// = DESCRIPTION
//    Concrete visitor for the Component class
//
// = AUTHOR
//    Jeff Parsons
//
// ============================================================================

#ifndef TAO_BE_VISITOR_COMPONENT_H
#define TAO_BE_VISITOR_COMPONENT_H

#include "be_visitor_interface.h"

#include "be_visitor_component/component.h"
#include "be_visitor_component/component_ch.h"
#include "be_visitor_component/component_ci.h"
#include "be_visitor_component/component_cs.h"
#include "be_visitor_component/component_sh.h"
#include "be_visitor_component/component_si.h"
#include "be_visitor_component/component_ss.h"
#include "be_visitor_component/component_ih.h"
#include "be_visitor_component/component_is.h"
#include "be_visitor_component/any_op_ch.h"
#include "be_visitor_component/any_op_cs.h"
#include "be_visitor_component/cdr_op_ch.h"
#include "be_visitor_component/cdr_op_cs.h"

#endif /* TAO_BE_VISITOR_COMPONENT_H */
