/* -*- c++ -*- */
//
// $Id: be_visitor_typecode.h 14 2007-02-01 15:49:12Z mitza $
//

// ============================================================================
//
// = LIBRARY
//    TAO IDL
//
// = FILENAME
//    be_visitor_typecode.h
//
// = DESCRIPTION
//    Concrete visitor to generate code for TypeCodes
//
// = AUTHOR
//    Aniruddha Gokhale
//
// ============================================================================

#ifndef TAO_BE_VISITOR_TYPECODE_H
#define TAO_BE_VISITOR_TYPECODE_H

#include "idl_defines.h"

#include "be_visitor_decl.h"
#include "be_visitor_typecode/typecode_decl.h"
#include "be_visitor_typecode/typecode_defn.h"

#include "be_visitor_typecode/alias_typecode.h"
#include "be_visitor_typecode/enum_typecode.h"
#include "be_visitor_typecode/objref_typecode.h"
#include "be_visitor_typecode/struct_typecode.h"
#include "be_visitor_typecode/union_typecode.h"
#include "be_visitor_typecode/value_typecode.h"


#endif /* TAO_BE_VISITOR_TYPECODE_H */
