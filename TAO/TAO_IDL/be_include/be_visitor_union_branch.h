//
// $Id: be_visitor_union_branch.h 935 2008-12-10 21:47:27Z mitza $
//
/* -*- c++ -*- */
// ============================================================================
//
// = LIBRARY
//    TAO IDL
//
// = FILENAME
//    be_visitor_union_branch.h
//
// = DESCRIPTION
//    Concrete visitor for the base "BE_Union_Branch" node
//
// = AUTHOR
//    Aniruddha Gokhale
//
// ============================================================================

#ifndef TAO_BE_VISITOR_UNION_BRANCH_H
#define TAO_BE_VISITOR_UNION_BRANCH_H

#include "idl_defines.h"

#include "be_visitor_decl.h"
#include "be_visitor_union_branch/public_ch.h"
#include "be_visitor_union_branch/public_ci.h"
#include "be_visitor_union_branch/public_cs.h"
#include "be_visitor_union_branch/public_assign_cs.h"
#include "be_visitor_union_branch/public_constructor_cs.h"
#include "be_visitor_union_branch/public_reset_cs.h"
#include "be_visitor_union_branch/private_ch.h"
#include "be_visitor_union_branch/cdr_op_ch.h"
#include "be_visitor_union_branch/cdr_op_cs.h"
#include "be_visitor_union_branch/serializer_op_ch.h"
#include "be_visitor_union_branch/serializer_op_cs.h"

#endif // TAO_BE_VISITOR_UNION_BRANCH_H
