// -*- C++ -*-
//
// $Id: be_visitor_argument.h 14 2007-02-01 15:49:12Z mitza $

// ============================================================================
//
// = LIBRARY
//    TAO IDL
//
// = FILENAME
//    be_visitor_args.h
//
// = DESCRIPTION
//    Visitors for generation of code for Arguments. These are used for the
//    interpretive styled stubs and skeletons
//
// = AUTHOR
//    Aniruddha Gokhale and Carlos O'Ryan
//
// ============================================================================

#ifndef _BE_VISITOR_ARGUMENT_H
#define _BE_VISITOR_ARGUMENT_H

#include "idl_defines.h"

#include "be_visitor_scope.h"
#include "be_visitor_argument/argument.h"
#include "be_visitor_argument/arglist.h"
#include "be_visitor_argument/vardecl_ss.h"
#include "be_visitor_argument/upcall_ss.h"
#include "be_visitor_argument/post_upcall_ss.h"
#include "be_visitor_argument/marshal_ss.h"
#include "be_visitor_argument/invoke_cs.h"
#include "be_visitor_argument/paramlist.h"

#endif /* _BE_VISITOR_ARGUMENT_H */
