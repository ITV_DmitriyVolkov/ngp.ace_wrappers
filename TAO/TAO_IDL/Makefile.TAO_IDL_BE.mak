# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.TAO_IDL_BE.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\TAO_IDL_BE\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_IDL_BEd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I"include" -I"be_include" -I"fe" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_IDL_BE_BUILD_DLL -f "Makefile.TAO_IDL_BE.dep" "be\be_interface_fwd.cpp" "be\be_constant.cpp" "be\be_factory.cpp" "be\be_visitor_arg_traits.cpp" "be\be_visitor_union_branch.cpp" "be\be_generator.cpp" "be\be_home.cpp" "be\be_visitor_amh_pre_proc.cpp" "be\be_sunsoft.cpp" "be\be_visitor_root.cpp" "be\be_visitor_scope.cpp" "be\be_interface_strategy.cpp" "be\be_visitor_typedef.cpp" "be\be_component.cpp" "be\be_visitor_typecode.cpp" "be\be_visitor_native.cpp" "be\be_argument.cpp" "be\be_init.cpp" "be\be_decl.cpp" "be\be_expression.cpp" "be\be_predefined_type.cpp" "be\be_visitor_enum.cpp" "be\be_enum.cpp" "be\be_string.cpp" "be\be_visitor_sequence.cpp" "be\be_visitor_field.cpp" "be\be_scope.cpp" "be\be_interface.cpp" "be\be_visitor_exception.cpp" "be\be_structure.cpp" "be\be_visitor_valuetype_fwd.cpp" "be\be_visitor_template_export.cpp" "be\be_operation_strategy.cpp" "be\be_structure_fwd.cpp" "be\be_sequence.cpp" "be\be_union_fwd.cpp" "be\be_valuebox.cpp" "be\be_visitor_union_fwd.cpp" "be\be_visitor_component_fwd.cpp" "be\be_union_label.cpp" "be\be_visitor_interface.cpp" "be\be_enum_val.cpp" "be\be_union_branch.cpp" "be\be_visitor_context.cpp" "be\be_array.cpp" "be\be_visitor_constant.cpp" "be\be_produce.cpp" "be\be_visitor.cpp" "be\be_exception.cpp" "be\be_visitor_structure.cpp" "be\be_visitor_array.cpp" "be\be_visitor_ami_pre_proc.cpp" "be\be_visitor_attribute.cpp" "be\be_util.cpp" "be\be_valuetype_fwd.cpp" "be\be_visitor_traits.cpp" "be\be_stream_factory.cpp" "be\be_visitor_factory.cpp" "be\be_root.cpp" "be\be_global.cpp" "be\be_codegen.cpp" "be\be_visitor_union.cpp" "be\be_native.cpp" "be\be_tmplinst.cpp" "be\be_visitor_operation.cpp" "be\be_helper.cpp" "be\be_visitor_argument.cpp" "be\be_component_fwd.cpp" "be\be_visitor_interface_fwd.cpp" "be\be_type.cpp" "be\be_typedef.cpp" "be\be_visitor_decl.cpp" "be\be_visitor_structure_fwd.cpp" "be\be_module.cpp" "be\be_eventtype_fwd.cpp" "be\be_field.cpp" "be\be_visitor_home.cpp" "be\be_operation.cpp" "be\be_visitor_valuebox.cpp" "be\be_visitor_valuetype.cpp" "be\be_eventtype.cpp" "be\be_union.cpp" "be\be_visitor_module.cpp" "be\be_visitor_ccm_pre_proc.cpp" "be\be_visitor_component.cpp" "be\be_attribute.cpp" "be\be_valuetype.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_IDL_BEd.pdb"
	-@del /f/q "..\..\lib\TAO_IDL_BEd.dll"
	-@del /f/q "$(OUTDIR)\TAO_IDL_BEd.lib"
	-@del /f/q "$(OUTDIR)\TAO_IDL_BEd.exp"
	-@del /f/q "$(OUTDIR)\TAO_IDL_BEd.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\TAO_IDL_BE\$(NULL)" mkdir "Debug\TAO_IDL_BE"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\.." /I "include" /I "be_include" /I "fe" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_IDL_BE_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAO_IDL_FEd.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\TAO_IDL_BEd.pdb" /machine:IA64 /out:"..\..\lib\TAO_IDL_BEd.dll" /implib:"$(OUTDIR)\TAO_IDL_BEd.lib"
LINK32_OBJS= \
	"$(INTDIR)\be\be_interface_fwd.obj" \
	"$(INTDIR)\be\be_constant.obj" \
	"$(INTDIR)\be\be_factory.obj" \
	"$(INTDIR)\be\be_visitor_arg_traits.obj" \
	"$(INTDIR)\be\be_visitor_union_branch.obj" \
	"$(INTDIR)\be\be_generator.obj" \
	"$(INTDIR)\be\be_home.obj" \
	"$(INTDIR)\be\be_visitor_amh_pre_proc.obj" \
	"$(INTDIR)\be\be_sunsoft.obj" \
	"$(INTDIR)\be\be_visitor_root.obj" \
	"$(INTDIR)\be\be_visitor_scope.obj" \
	"$(INTDIR)\be\be_interface_strategy.obj" \
	"$(INTDIR)\be\be_visitor_typedef.obj" \
	"$(INTDIR)\be\be_component.obj" \
	"$(INTDIR)\be\be_visitor_typecode.obj" \
	"$(INTDIR)\be\be_visitor_native.obj" \
	"$(INTDIR)\be\be_argument.obj" \
	"$(INTDIR)\be\be_init.obj" \
	"$(INTDIR)\be\be_decl.obj" \
	"$(INTDIR)\be\be_expression.obj" \
	"$(INTDIR)\be\be_predefined_type.obj" \
	"$(INTDIR)\be\be_visitor_enum.obj" \
	"$(INTDIR)\be\be_enum.obj" \
	"$(INTDIR)\be\be_string.obj" \
	"$(INTDIR)\be\be_visitor_sequence.obj" \
	"$(INTDIR)\be\be_visitor_field.obj" \
	"$(INTDIR)\be\be_scope.obj" \
	"$(INTDIR)\be\be_interface.obj" \
	"$(INTDIR)\be\be_visitor_exception.obj" \
	"$(INTDIR)\be\be_structure.obj" \
	"$(INTDIR)\be\be_visitor_valuetype_fwd.obj" \
	"$(INTDIR)\be\be_visitor_template_export.obj" \
	"$(INTDIR)\be\be_operation_strategy.obj" \
	"$(INTDIR)\be\be_structure_fwd.obj" \
	"$(INTDIR)\be\be_sequence.obj" \
	"$(INTDIR)\be\be_union_fwd.obj" \
	"$(INTDIR)\be\be_valuebox.obj" \
	"$(INTDIR)\be\be_visitor_union_fwd.obj" \
	"$(INTDIR)\be\be_visitor_component_fwd.obj" \
	"$(INTDIR)\be\be_union_label.obj" \
	"$(INTDIR)\be\be_visitor_interface.obj" \
	"$(INTDIR)\be\be_enum_val.obj" \
	"$(INTDIR)\be\be_union_branch.obj" \
	"$(INTDIR)\be\be_visitor_context.obj" \
	"$(INTDIR)\be\be_array.obj" \
	"$(INTDIR)\be\be_visitor_constant.obj" \
	"$(INTDIR)\be\be_produce.obj" \
	"$(INTDIR)\be\be_visitor.obj" \
	"$(INTDIR)\be\be_exception.obj" \
	"$(INTDIR)\be\be_visitor_structure.obj" \
	"$(INTDIR)\be\be_visitor_array.obj" \
	"$(INTDIR)\be\be_visitor_ami_pre_proc.obj" \
	"$(INTDIR)\be\be_visitor_attribute.obj" \
	"$(INTDIR)\be\be_util.obj" \
	"$(INTDIR)\be\be_valuetype_fwd.obj" \
	"$(INTDIR)\be\be_visitor_traits.obj" \
	"$(INTDIR)\be\be_stream_factory.obj" \
	"$(INTDIR)\be\be_visitor_factory.obj" \
	"$(INTDIR)\be\be_root.obj" \
	"$(INTDIR)\be\be_global.obj" \
	"$(INTDIR)\be\be_codegen.obj" \
	"$(INTDIR)\be\be_visitor_union.obj" \
	"$(INTDIR)\be\be_native.obj" \
	"$(INTDIR)\be\be_tmplinst.obj" \
	"$(INTDIR)\be\be_visitor_operation.obj" \
	"$(INTDIR)\be\be_helper.obj" \
	"$(INTDIR)\be\be_visitor_argument.obj" \
	"$(INTDIR)\be\be_component_fwd.obj" \
	"$(INTDIR)\be\be_visitor_interface_fwd.obj" \
	"$(INTDIR)\be\be_type.obj" \
	"$(INTDIR)\be\be_typedef.obj" \
	"$(INTDIR)\be\be_visitor_decl.obj" \
	"$(INTDIR)\be\be_visitor_structure_fwd.obj" \
	"$(INTDIR)\be\be_module.obj" \
	"$(INTDIR)\be\be_eventtype_fwd.obj" \
	"$(INTDIR)\be\be_field.obj" \
	"$(INTDIR)\be\be_visitor_home.obj" \
	"$(INTDIR)\be\be_operation.obj" \
	"$(INTDIR)\be\be_visitor_valuebox.obj" \
	"$(INTDIR)\be\be_visitor_valuetype.obj" \
	"$(INTDIR)\be\be_eventtype.obj" \
	"$(INTDIR)\be\be_union.obj" \
	"$(INTDIR)\be\be_visitor_module.obj" \
	"$(INTDIR)\be\be_visitor_ccm_pre_proc.obj" \
	"$(INTDIR)\be\be_visitor_component.obj" \
	"$(INTDIR)\be\be_attribute.obj" \
	"$(INTDIR)\be\be_valuetype.obj"

"..\..\lib\TAO_IDL_BEd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_IDL_BEd.dll.manifest" mt.exe -manifest "..\..\lib\TAO_IDL_BEd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\TAO_IDL_BE\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_IDL_BE.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I"include" -I"be_include" -I"fe" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_IDL_BE_BUILD_DLL -f "Makefile.TAO_IDL_BE.dep" "be\be_interface_fwd.cpp" "be\be_constant.cpp" "be\be_factory.cpp" "be\be_visitor_arg_traits.cpp" "be\be_visitor_union_branch.cpp" "be\be_generator.cpp" "be\be_home.cpp" "be\be_visitor_amh_pre_proc.cpp" "be\be_sunsoft.cpp" "be\be_visitor_root.cpp" "be\be_visitor_scope.cpp" "be\be_interface_strategy.cpp" "be\be_visitor_typedef.cpp" "be\be_component.cpp" "be\be_visitor_typecode.cpp" "be\be_visitor_native.cpp" "be\be_argument.cpp" "be\be_init.cpp" "be\be_decl.cpp" "be\be_expression.cpp" "be\be_predefined_type.cpp" "be\be_visitor_enum.cpp" "be\be_enum.cpp" "be\be_string.cpp" "be\be_visitor_sequence.cpp" "be\be_visitor_field.cpp" "be\be_scope.cpp" "be\be_interface.cpp" "be\be_visitor_exception.cpp" "be\be_structure.cpp" "be\be_visitor_valuetype_fwd.cpp" "be\be_visitor_template_export.cpp" "be\be_operation_strategy.cpp" "be\be_structure_fwd.cpp" "be\be_sequence.cpp" "be\be_union_fwd.cpp" "be\be_valuebox.cpp" "be\be_visitor_union_fwd.cpp" "be\be_visitor_component_fwd.cpp" "be\be_union_label.cpp" "be\be_visitor_interface.cpp" "be\be_enum_val.cpp" "be\be_union_branch.cpp" "be\be_visitor_context.cpp" "be\be_array.cpp" "be\be_visitor_constant.cpp" "be\be_produce.cpp" "be\be_visitor.cpp" "be\be_exception.cpp" "be\be_visitor_structure.cpp" "be\be_visitor_array.cpp" "be\be_visitor_ami_pre_proc.cpp" "be\be_visitor_attribute.cpp" "be\be_util.cpp" "be\be_valuetype_fwd.cpp" "be\be_visitor_traits.cpp" "be\be_stream_factory.cpp" "be\be_visitor_factory.cpp" "be\be_root.cpp" "be\be_global.cpp" "be\be_codegen.cpp" "be\be_visitor_union.cpp" "be\be_native.cpp" "be\be_tmplinst.cpp" "be\be_visitor_operation.cpp" "be\be_helper.cpp" "be\be_visitor_argument.cpp" "be\be_component_fwd.cpp" "be\be_visitor_interface_fwd.cpp" "be\be_type.cpp" "be\be_typedef.cpp" "be\be_visitor_decl.cpp" "be\be_visitor_structure_fwd.cpp" "be\be_module.cpp" "be\be_eventtype_fwd.cpp" "be\be_field.cpp" "be\be_visitor_home.cpp" "be\be_operation.cpp" "be\be_visitor_valuebox.cpp" "be\be_visitor_valuetype.cpp" "be\be_eventtype.cpp" "be\be_union.cpp" "be\be_visitor_module.cpp" "be\be_visitor_ccm_pre_proc.cpp" "be\be_visitor_component.cpp" "be\be_attribute.cpp" "be\be_valuetype.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\TAO_IDL_BE.dll"
	-@del /f/q "$(OUTDIR)\TAO_IDL_BE.lib"
	-@del /f/q "$(OUTDIR)\TAO_IDL_BE.exp"
	-@del /f/q "$(OUTDIR)\TAO_IDL_BE.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\TAO_IDL_BE\$(NULL)" mkdir "Release\TAO_IDL_BE"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I "include" /I "be_include" /I "fe" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_IDL_BE_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO_IDL_FE.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\TAO_IDL_BE.dll" /implib:"$(OUTDIR)\TAO_IDL_BE.lib"
LINK32_OBJS= \
	"$(INTDIR)\be\be_interface_fwd.obj" \
	"$(INTDIR)\be\be_constant.obj" \
	"$(INTDIR)\be\be_factory.obj" \
	"$(INTDIR)\be\be_visitor_arg_traits.obj" \
	"$(INTDIR)\be\be_visitor_union_branch.obj" \
	"$(INTDIR)\be\be_generator.obj" \
	"$(INTDIR)\be\be_home.obj" \
	"$(INTDIR)\be\be_visitor_amh_pre_proc.obj" \
	"$(INTDIR)\be\be_sunsoft.obj" \
	"$(INTDIR)\be\be_visitor_root.obj" \
	"$(INTDIR)\be\be_visitor_scope.obj" \
	"$(INTDIR)\be\be_interface_strategy.obj" \
	"$(INTDIR)\be\be_visitor_typedef.obj" \
	"$(INTDIR)\be\be_component.obj" \
	"$(INTDIR)\be\be_visitor_typecode.obj" \
	"$(INTDIR)\be\be_visitor_native.obj" \
	"$(INTDIR)\be\be_argument.obj" \
	"$(INTDIR)\be\be_init.obj" \
	"$(INTDIR)\be\be_decl.obj" \
	"$(INTDIR)\be\be_expression.obj" \
	"$(INTDIR)\be\be_predefined_type.obj" \
	"$(INTDIR)\be\be_visitor_enum.obj" \
	"$(INTDIR)\be\be_enum.obj" \
	"$(INTDIR)\be\be_string.obj" \
	"$(INTDIR)\be\be_visitor_sequence.obj" \
	"$(INTDIR)\be\be_visitor_field.obj" \
	"$(INTDIR)\be\be_scope.obj" \
	"$(INTDIR)\be\be_interface.obj" \
	"$(INTDIR)\be\be_visitor_exception.obj" \
	"$(INTDIR)\be\be_structure.obj" \
	"$(INTDIR)\be\be_visitor_valuetype_fwd.obj" \
	"$(INTDIR)\be\be_visitor_template_export.obj" \
	"$(INTDIR)\be\be_operation_strategy.obj" \
	"$(INTDIR)\be\be_structure_fwd.obj" \
	"$(INTDIR)\be\be_sequence.obj" \
	"$(INTDIR)\be\be_union_fwd.obj" \
	"$(INTDIR)\be\be_valuebox.obj" \
	"$(INTDIR)\be\be_visitor_union_fwd.obj" \
	"$(INTDIR)\be\be_visitor_component_fwd.obj" \
	"$(INTDIR)\be\be_union_label.obj" \
	"$(INTDIR)\be\be_visitor_interface.obj" \
	"$(INTDIR)\be\be_enum_val.obj" \
	"$(INTDIR)\be\be_union_branch.obj" \
	"$(INTDIR)\be\be_visitor_context.obj" \
	"$(INTDIR)\be\be_array.obj" \
	"$(INTDIR)\be\be_visitor_constant.obj" \
	"$(INTDIR)\be\be_produce.obj" \
	"$(INTDIR)\be\be_visitor.obj" \
	"$(INTDIR)\be\be_exception.obj" \
	"$(INTDIR)\be\be_visitor_structure.obj" \
	"$(INTDIR)\be\be_visitor_array.obj" \
	"$(INTDIR)\be\be_visitor_ami_pre_proc.obj" \
	"$(INTDIR)\be\be_visitor_attribute.obj" \
	"$(INTDIR)\be\be_util.obj" \
	"$(INTDIR)\be\be_valuetype_fwd.obj" \
	"$(INTDIR)\be\be_visitor_traits.obj" \
	"$(INTDIR)\be\be_stream_factory.obj" \
	"$(INTDIR)\be\be_visitor_factory.obj" \
	"$(INTDIR)\be\be_root.obj" \
	"$(INTDIR)\be\be_global.obj" \
	"$(INTDIR)\be\be_codegen.obj" \
	"$(INTDIR)\be\be_visitor_union.obj" \
	"$(INTDIR)\be\be_native.obj" \
	"$(INTDIR)\be\be_tmplinst.obj" \
	"$(INTDIR)\be\be_visitor_operation.obj" \
	"$(INTDIR)\be\be_helper.obj" \
	"$(INTDIR)\be\be_visitor_argument.obj" \
	"$(INTDIR)\be\be_component_fwd.obj" \
	"$(INTDIR)\be\be_visitor_interface_fwd.obj" \
	"$(INTDIR)\be\be_type.obj" \
	"$(INTDIR)\be\be_typedef.obj" \
	"$(INTDIR)\be\be_visitor_decl.obj" \
	"$(INTDIR)\be\be_visitor_structure_fwd.obj" \
	"$(INTDIR)\be\be_module.obj" \
	"$(INTDIR)\be\be_eventtype_fwd.obj" \
	"$(INTDIR)\be\be_field.obj" \
	"$(INTDIR)\be\be_visitor_home.obj" \
	"$(INTDIR)\be\be_operation.obj" \
	"$(INTDIR)\be\be_visitor_valuebox.obj" \
	"$(INTDIR)\be\be_visitor_valuetype.obj" \
	"$(INTDIR)\be\be_eventtype.obj" \
	"$(INTDIR)\be\be_union.obj" \
	"$(INTDIR)\be\be_visitor_module.obj" \
	"$(INTDIR)\be\be_visitor_ccm_pre_proc.obj" \
	"$(INTDIR)\be\be_visitor_component.obj" \
	"$(INTDIR)\be\be_attribute.obj" \
	"$(INTDIR)\be\be_valuetype.obj"

"..\..\lib\TAO_IDL_BE.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_IDL_BE.dll.manifest" mt.exe -manifest "..\..\lib\TAO_IDL_BE.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\TAO_IDL_BE\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_IDL_BEsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I"include" -I"be_include" -I"fe" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.TAO_IDL_BE.dep" "be\be_interface_fwd.cpp" "be\be_constant.cpp" "be\be_factory.cpp" "be\be_visitor_arg_traits.cpp" "be\be_visitor_union_branch.cpp" "be\be_generator.cpp" "be\be_home.cpp" "be\be_visitor_amh_pre_proc.cpp" "be\be_sunsoft.cpp" "be\be_visitor_root.cpp" "be\be_visitor_scope.cpp" "be\be_interface_strategy.cpp" "be\be_visitor_typedef.cpp" "be\be_component.cpp" "be\be_visitor_typecode.cpp" "be\be_visitor_native.cpp" "be\be_argument.cpp" "be\be_init.cpp" "be\be_decl.cpp" "be\be_expression.cpp" "be\be_predefined_type.cpp" "be\be_visitor_enum.cpp" "be\be_enum.cpp" "be\be_string.cpp" "be\be_visitor_sequence.cpp" "be\be_visitor_field.cpp" "be\be_scope.cpp" "be\be_interface.cpp" "be\be_visitor_exception.cpp" "be\be_structure.cpp" "be\be_visitor_valuetype_fwd.cpp" "be\be_visitor_template_export.cpp" "be\be_operation_strategy.cpp" "be\be_structure_fwd.cpp" "be\be_sequence.cpp" "be\be_union_fwd.cpp" "be\be_valuebox.cpp" "be\be_visitor_union_fwd.cpp" "be\be_visitor_component_fwd.cpp" "be\be_union_label.cpp" "be\be_visitor_interface.cpp" "be\be_enum_val.cpp" "be\be_union_branch.cpp" "be\be_visitor_context.cpp" "be\be_array.cpp" "be\be_visitor_constant.cpp" "be\be_produce.cpp" "be\be_visitor.cpp" "be\be_exception.cpp" "be\be_visitor_structure.cpp" "be\be_visitor_array.cpp" "be\be_visitor_ami_pre_proc.cpp" "be\be_visitor_attribute.cpp" "be\be_util.cpp" "be\be_valuetype_fwd.cpp" "be\be_visitor_traits.cpp" "be\be_stream_factory.cpp" "be\be_visitor_factory.cpp" "be\be_root.cpp" "be\be_global.cpp" "be\be_codegen.cpp" "be\be_visitor_union.cpp" "be\be_native.cpp" "be\be_tmplinst.cpp" "be\be_visitor_operation.cpp" "be\be_helper.cpp" "be\be_visitor_argument.cpp" "be\be_component_fwd.cpp" "be\be_visitor_interface_fwd.cpp" "be\be_type.cpp" "be\be_typedef.cpp" "be\be_visitor_decl.cpp" "be\be_visitor_structure_fwd.cpp" "be\be_module.cpp" "be\be_eventtype_fwd.cpp" "be\be_field.cpp" "be\be_visitor_home.cpp" "be\be_operation.cpp" "be\be_visitor_valuebox.cpp" "be\be_visitor_valuetype.cpp" "be\be_eventtype.cpp" "be\be_union.cpp" "be\be_visitor_module.cpp" "be\be_visitor_ccm_pre_proc.cpp" "be\be_visitor_component.cpp" "be\be_attribute.cpp" "be\be_valuetype.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_IDL_BEsd.lib"
	-@del /f/q "$(OUTDIR)\TAO_IDL_BEsd.exp"
	-@del /f/q "$(OUTDIR)\TAO_IDL_BEsd.ilk"
	-@del /f/q "..\..\lib\TAO_IDL_BEsd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\TAO_IDL_BE\$(NULL)" mkdir "Static_Debug\TAO_IDL_BE"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"..\..\lib\TAO_IDL_BEsd.pdb" /I "..\.." /I "include" /I "be_include" /I "fe" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_IDL_BEsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\be\be_interface_fwd.obj" \
	"$(INTDIR)\be\be_constant.obj" \
	"$(INTDIR)\be\be_factory.obj" \
	"$(INTDIR)\be\be_visitor_arg_traits.obj" \
	"$(INTDIR)\be\be_visitor_union_branch.obj" \
	"$(INTDIR)\be\be_generator.obj" \
	"$(INTDIR)\be\be_home.obj" \
	"$(INTDIR)\be\be_visitor_amh_pre_proc.obj" \
	"$(INTDIR)\be\be_sunsoft.obj" \
	"$(INTDIR)\be\be_visitor_root.obj" \
	"$(INTDIR)\be\be_visitor_scope.obj" \
	"$(INTDIR)\be\be_interface_strategy.obj" \
	"$(INTDIR)\be\be_visitor_typedef.obj" \
	"$(INTDIR)\be\be_component.obj" \
	"$(INTDIR)\be\be_visitor_typecode.obj" \
	"$(INTDIR)\be\be_visitor_native.obj" \
	"$(INTDIR)\be\be_argument.obj" \
	"$(INTDIR)\be\be_init.obj" \
	"$(INTDIR)\be\be_decl.obj" \
	"$(INTDIR)\be\be_expression.obj" \
	"$(INTDIR)\be\be_predefined_type.obj" \
	"$(INTDIR)\be\be_visitor_enum.obj" \
	"$(INTDIR)\be\be_enum.obj" \
	"$(INTDIR)\be\be_string.obj" \
	"$(INTDIR)\be\be_visitor_sequence.obj" \
	"$(INTDIR)\be\be_visitor_field.obj" \
	"$(INTDIR)\be\be_scope.obj" \
	"$(INTDIR)\be\be_interface.obj" \
	"$(INTDIR)\be\be_visitor_exception.obj" \
	"$(INTDIR)\be\be_structure.obj" \
	"$(INTDIR)\be\be_visitor_valuetype_fwd.obj" \
	"$(INTDIR)\be\be_visitor_template_export.obj" \
	"$(INTDIR)\be\be_operation_strategy.obj" \
	"$(INTDIR)\be\be_structure_fwd.obj" \
	"$(INTDIR)\be\be_sequence.obj" \
	"$(INTDIR)\be\be_union_fwd.obj" \
	"$(INTDIR)\be\be_valuebox.obj" \
	"$(INTDIR)\be\be_visitor_union_fwd.obj" \
	"$(INTDIR)\be\be_visitor_component_fwd.obj" \
	"$(INTDIR)\be\be_union_label.obj" \
	"$(INTDIR)\be\be_visitor_interface.obj" \
	"$(INTDIR)\be\be_enum_val.obj" \
	"$(INTDIR)\be\be_union_branch.obj" \
	"$(INTDIR)\be\be_visitor_context.obj" \
	"$(INTDIR)\be\be_array.obj" \
	"$(INTDIR)\be\be_visitor_constant.obj" \
	"$(INTDIR)\be\be_produce.obj" \
	"$(INTDIR)\be\be_visitor.obj" \
	"$(INTDIR)\be\be_exception.obj" \
	"$(INTDIR)\be\be_visitor_structure.obj" \
	"$(INTDIR)\be\be_visitor_array.obj" \
	"$(INTDIR)\be\be_visitor_ami_pre_proc.obj" \
	"$(INTDIR)\be\be_visitor_attribute.obj" \
	"$(INTDIR)\be\be_util.obj" \
	"$(INTDIR)\be\be_valuetype_fwd.obj" \
	"$(INTDIR)\be\be_visitor_traits.obj" \
	"$(INTDIR)\be\be_stream_factory.obj" \
	"$(INTDIR)\be\be_visitor_factory.obj" \
	"$(INTDIR)\be\be_root.obj" \
	"$(INTDIR)\be\be_global.obj" \
	"$(INTDIR)\be\be_codegen.obj" \
	"$(INTDIR)\be\be_visitor_union.obj" \
	"$(INTDIR)\be\be_native.obj" \
	"$(INTDIR)\be\be_tmplinst.obj" \
	"$(INTDIR)\be\be_visitor_operation.obj" \
	"$(INTDIR)\be\be_helper.obj" \
	"$(INTDIR)\be\be_visitor_argument.obj" \
	"$(INTDIR)\be\be_component_fwd.obj" \
	"$(INTDIR)\be\be_visitor_interface_fwd.obj" \
	"$(INTDIR)\be\be_type.obj" \
	"$(INTDIR)\be\be_typedef.obj" \
	"$(INTDIR)\be\be_visitor_decl.obj" \
	"$(INTDIR)\be\be_visitor_structure_fwd.obj" \
	"$(INTDIR)\be\be_module.obj" \
	"$(INTDIR)\be\be_eventtype_fwd.obj" \
	"$(INTDIR)\be\be_field.obj" \
	"$(INTDIR)\be\be_visitor_home.obj" \
	"$(INTDIR)\be\be_operation.obj" \
	"$(INTDIR)\be\be_visitor_valuebox.obj" \
	"$(INTDIR)\be\be_visitor_valuetype.obj" \
	"$(INTDIR)\be\be_eventtype.obj" \
	"$(INTDIR)\be\be_union.obj" \
	"$(INTDIR)\be\be_visitor_module.obj" \
	"$(INTDIR)\be\be_visitor_ccm_pre_proc.obj" \
	"$(INTDIR)\be\be_visitor_component.obj" \
	"$(INTDIR)\be\be_attribute.obj" \
	"$(INTDIR)\be\be_valuetype.obj"

"$(OUTDIR)\TAO_IDL_BEsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_IDL_BEsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_IDL_BEsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\TAO_IDL_BE\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_IDL_BEs.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I"include" -I"be_include" -I"fe" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.TAO_IDL_BE.dep" "be\be_interface_fwd.cpp" "be\be_constant.cpp" "be\be_factory.cpp" "be\be_visitor_arg_traits.cpp" "be\be_visitor_union_branch.cpp" "be\be_generator.cpp" "be\be_home.cpp" "be\be_visitor_amh_pre_proc.cpp" "be\be_sunsoft.cpp" "be\be_visitor_root.cpp" "be\be_visitor_scope.cpp" "be\be_interface_strategy.cpp" "be\be_visitor_typedef.cpp" "be\be_component.cpp" "be\be_visitor_typecode.cpp" "be\be_visitor_native.cpp" "be\be_argument.cpp" "be\be_init.cpp" "be\be_decl.cpp" "be\be_expression.cpp" "be\be_predefined_type.cpp" "be\be_visitor_enum.cpp" "be\be_enum.cpp" "be\be_string.cpp" "be\be_visitor_sequence.cpp" "be\be_visitor_field.cpp" "be\be_scope.cpp" "be\be_interface.cpp" "be\be_visitor_exception.cpp" "be\be_structure.cpp" "be\be_visitor_valuetype_fwd.cpp" "be\be_visitor_template_export.cpp" "be\be_operation_strategy.cpp" "be\be_structure_fwd.cpp" "be\be_sequence.cpp" "be\be_union_fwd.cpp" "be\be_valuebox.cpp" "be\be_visitor_union_fwd.cpp" "be\be_visitor_component_fwd.cpp" "be\be_union_label.cpp" "be\be_visitor_interface.cpp" "be\be_enum_val.cpp" "be\be_union_branch.cpp" "be\be_visitor_context.cpp" "be\be_array.cpp" "be\be_visitor_constant.cpp" "be\be_produce.cpp" "be\be_visitor.cpp" "be\be_exception.cpp" "be\be_visitor_structure.cpp" "be\be_visitor_array.cpp" "be\be_visitor_ami_pre_proc.cpp" "be\be_visitor_attribute.cpp" "be\be_util.cpp" "be\be_valuetype_fwd.cpp" "be\be_visitor_traits.cpp" "be\be_stream_factory.cpp" "be\be_visitor_factory.cpp" "be\be_root.cpp" "be\be_global.cpp" "be\be_codegen.cpp" "be\be_visitor_union.cpp" "be\be_native.cpp" "be\be_tmplinst.cpp" "be\be_visitor_operation.cpp" "be\be_helper.cpp" "be\be_visitor_argument.cpp" "be\be_component_fwd.cpp" "be\be_visitor_interface_fwd.cpp" "be\be_type.cpp" "be\be_typedef.cpp" "be\be_visitor_decl.cpp" "be\be_visitor_structure_fwd.cpp" "be\be_module.cpp" "be\be_eventtype_fwd.cpp" "be\be_field.cpp" "be\be_visitor_home.cpp" "be\be_operation.cpp" "be\be_visitor_valuebox.cpp" "be\be_visitor_valuetype.cpp" "be\be_eventtype.cpp" "be\be_union.cpp" "be\be_visitor_module.cpp" "be\be_visitor_ccm_pre_proc.cpp" "be\be_visitor_component.cpp" "be\be_attribute.cpp" "be\be_valuetype.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_IDL_BEs.lib"
	-@del /f/q "$(OUTDIR)\TAO_IDL_BEs.exp"
	-@del /f/q "$(OUTDIR)\TAO_IDL_BEs.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\TAO_IDL_BE\$(NULL)" mkdir "Static_Release\TAO_IDL_BE"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I "include" /I "be_include" /I "fe" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_IDL_BEs.lib"
LINK32_OBJS= \
	"$(INTDIR)\be\be_interface_fwd.obj" \
	"$(INTDIR)\be\be_constant.obj" \
	"$(INTDIR)\be\be_factory.obj" \
	"$(INTDIR)\be\be_visitor_arg_traits.obj" \
	"$(INTDIR)\be\be_visitor_union_branch.obj" \
	"$(INTDIR)\be\be_generator.obj" \
	"$(INTDIR)\be\be_home.obj" \
	"$(INTDIR)\be\be_visitor_amh_pre_proc.obj" \
	"$(INTDIR)\be\be_sunsoft.obj" \
	"$(INTDIR)\be\be_visitor_root.obj" \
	"$(INTDIR)\be\be_visitor_scope.obj" \
	"$(INTDIR)\be\be_interface_strategy.obj" \
	"$(INTDIR)\be\be_visitor_typedef.obj" \
	"$(INTDIR)\be\be_component.obj" \
	"$(INTDIR)\be\be_visitor_typecode.obj" \
	"$(INTDIR)\be\be_visitor_native.obj" \
	"$(INTDIR)\be\be_argument.obj" \
	"$(INTDIR)\be\be_init.obj" \
	"$(INTDIR)\be\be_decl.obj" \
	"$(INTDIR)\be\be_expression.obj" \
	"$(INTDIR)\be\be_predefined_type.obj" \
	"$(INTDIR)\be\be_visitor_enum.obj" \
	"$(INTDIR)\be\be_enum.obj" \
	"$(INTDIR)\be\be_string.obj" \
	"$(INTDIR)\be\be_visitor_sequence.obj" \
	"$(INTDIR)\be\be_visitor_field.obj" \
	"$(INTDIR)\be\be_scope.obj" \
	"$(INTDIR)\be\be_interface.obj" \
	"$(INTDIR)\be\be_visitor_exception.obj" \
	"$(INTDIR)\be\be_structure.obj" \
	"$(INTDIR)\be\be_visitor_valuetype_fwd.obj" \
	"$(INTDIR)\be\be_visitor_template_export.obj" \
	"$(INTDIR)\be\be_operation_strategy.obj" \
	"$(INTDIR)\be\be_structure_fwd.obj" \
	"$(INTDIR)\be\be_sequence.obj" \
	"$(INTDIR)\be\be_union_fwd.obj" \
	"$(INTDIR)\be\be_valuebox.obj" \
	"$(INTDIR)\be\be_visitor_union_fwd.obj" \
	"$(INTDIR)\be\be_visitor_component_fwd.obj" \
	"$(INTDIR)\be\be_union_label.obj" \
	"$(INTDIR)\be\be_visitor_interface.obj" \
	"$(INTDIR)\be\be_enum_val.obj" \
	"$(INTDIR)\be\be_union_branch.obj" \
	"$(INTDIR)\be\be_visitor_context.obj" \
	"$(INTDIR)\be\be_array.obj" \
	"$(INTDIR)\be\be_visitor_constant.obj" \
	"$(INTDIR)\be\be_produce.obj" \
	"$(INTDIR)\be\be_visitor.obj" \
	"$(INTDIR)\be\be_exception.obj" \
	"$(INTDIR)\be\be_visitor_structure.obj" \
	"$(INTDIR)\be\be_visitor_array.obj" \
	"$(INTDIR)\be\be_visitor_ami_pre_proc.obj" \
	"$(INTDIR)\be\be_visitor_attribute.obj" \
	"$(INTDIR)\be\be_util.obj" \
	"$(INTDIR)\be\be_valuetype_fwd.obj" \
	"$(INTDIR)\be\be_visitor_traits.obj" \
	"$(INTDIR)\be\be_stream_factory.obj" \
	"$(INTDIR)\be\be_visitor_factory.obj" \
	"$(INTDIR)\be\be_root.obj" \
	"$(INTDIR)\be\be_global.obj" \
	"$(INTDIR)\be\be_codegen.obj" \
	"$(INTDIR)\be\be_visitor_union.obj" \
	"$(INTDIR)\be\be_native.obj" \
	"$(INTDIR)\be\be_tmplinst.obj" \
	"$(INTDIR)\be\be_visitor_operation.obj" \
	"$(INTDIR)\be\be_helper.obj" \
	"$(INTDIR)\be\be_visitor_argument.obj" \
	"$(INTDIR)\be\be_component_fwd.obj" \
	"$(INTDIR)\be\be_visitor_interface_fwd.obj" \
	"$(INTDIR)\be\be_type.obj" \
	"$(INTDIR)\be\be_typedef.obj" \
	"$(INTDIR)\be\be_visitor_decl.obj" \
	"$(INTDIR)\be\be_visitor_structure_fwd.obj" \
	"$(INTDIR)\be\be_module.obj" \
	"$(INTDIR)\be\be_eventtype_fwd.obj" \
	"$(INTDIR)\be\be_field.obj" \
	"$(INTDIR)\be\be_visitor_home.obj" \
	"$(INTDIR)\be\be_operation.obj" \
	"$(INTDIR)\be\be_visitor_valuebox.obj" \
	"$(INTDIR)\be\be_visitor_valuetype.obj" \
	"$(INTDIR)\be\be_eventtype.obj" \
	"$(INTDIR)\be\be_union.obj" \
	"$(INTDIR)\be\be_visitor_module.obj" \
	"$(INTDIR)\be\be_visitor_ccm_pre_proc.obj" \
	"$(INTDIR)\be\be_visitor_component.obj" \
	"$(INTDIR)\be\be_attribute.obj" \
	"$(INTDIR)\be\be_valuetype.obj"

"$(OUTDIR)\TAO_IDL_BEs.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_IDL_BEs.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_IDL_BEs.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.TAO_IDL_BE.dep")
!INCLUDE "Makefile.TAO_IDL_BE.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="be\be_interface_fwd.cpp"

"$(INTDIR)\be\be_interface_fwd.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_interface_fwd.obj" $(SOURCE)

SOURCE="be\be_constant.cpp"

"$(INTDIR)\be\be_constant.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_constant.obj" $(SOURCE)

SOURCE="be\be_factory.cpp"

"$(INTDIR)\be\be_factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_factory.obj" $(SOURCE)

SOURCE="be\be_visitor_arg_traits.cpp"

"$(INTDIR)\be\be_visitor_arg_traits.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_arg_traits.obj" $(SOURCE)

SOURCE="be\be_visitor_union_branch.cpp"

"$(INTDIR)\be\be_visitor_union_branch.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_union_branch.obj" $(SOURCE)

SOURCE="be\be_generator.cpp"

"$(INTDIR)\be\be_generator.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_generator.obj" $(SOURCE)

SOURCE="be\be_home.cpp"

"$(INTDIR)\be\be_home.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_home.obj" $(SOURCE)

SOURCE="be\be_visitor_amh_pre_proc.cpp"

"$(INTDIR)\be\be_visitor_amh_pre_proc.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_amh_pre_proc.obj" $(SOURCE)

SOURCE="be\be_sunsoft.cpp"

"$(INTDIR)\be\be_sunsoft.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_sunsoft.obj" $(SOURCE)

SOURCE="be\be_visitor_root.cpp"

"$(INTDIR)\be\be_visitor_root.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_root.obj" $(SOURCE)

SOURCE="be\be_visitor_scope.cpp"

"$(INTDIR)\be\be_visitor_scope.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_scope.obj" $(SOURCE)

SOURCE="be\be_interface_strategy.cpp"

"$(INTDIR)\be\be_interface_strategy.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_interface_strategy.obj" $(SOURCE)

SOURCE="be\be_visitor_typedef.cpp"

"$(INTDIR)\be\be_visitor_typedef.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_typedef.obj" $(SOURCE)

SOURCE="be\be_component.cpp"

"$(INTDIR)\be\be_component.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_component.obj" $(SOURCE)

SOURCE="be\be_visitor_typecode.cpp"

"$(INTDIR)\be\be_visitor_typecode.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_typecode.obj" $(SOURCE)

SOURCE="be\be_visitor_native.cpp"

"$(INTDIR)\be\be_visitor_native.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_native.obj" $(SOURCE)

SOURCE="be\be_argument.cpp"

"$(INTDIR)\be\be_argument.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_argument.obj" $(SOURCE)

SOURCE="be\be_init.cpp"

"$(INTDIR)\be\be_init.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_init.obj" $(SOURCE)

SOURCE="be\be_decl.cpp"

"$(INTDIR)\be\be_decl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_decl.obj" $(SOURCE)

SOURCE="be\be_expression.cpp"

"$(INTDIR)\be\be_expression.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_expression.obj" $(SOURCE)

SOURCE="be\be_predefined_type.cpp"

"$(INTDIR)\be\be_predefined_type.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_predefined_type.obj" $(SOURCE)

SOURCE="be\be_visitor_enum.cpp"

"$(INTDIR)\be\be_visitor_enum.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_enum.obj" $(SOURCE)

SOURCE="be\be_enum.cpp"

"$(INTDIR)\be\be_enum.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_enum.obj" $(SOURCE)

SOURCE="be\be_string.cpp"

"$(INTDIR)\be\be_string.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_string.obj" $(SOURCE)

SOURCE="be\be_visitor_sequence.cpp"

"$(INTDIR)\be\be_visitor_sequence.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_sequence.obj" $(SOURCE)

SOURCE="be\be_visitor_field.cpp"

"$(INTDIR)\be\be_visitor_field.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_field.obj" $(SOURCE)

SOURCE="be\be_scope.cpp"

"$(INTDIR)\be\be_scope.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_scope.obj" $(SOURCE)

SOURCE="be\be_interface.cpp"

"$(INTDIR)\be\be_interface.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_interface.obj" $(SOURCE)

SOURCE="be\be_visitor_exception.cpp"

"$(INTDIR)\be\be_visitor_exception.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_exception.obj" $(SOURCE)

SOURCE="be\be_structure.cpp"

"$(INTDIR)\be\be_structure.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_structure.obj" $(SOURCE)

SOURCE="be\be_visitor_valuetype_fwd.cpp"

"$(INTDIR)\be\be_visitor_valuetype_fwd.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_valuetype_fwd.obj" $(SOURCE)

SOURCE="be\be_visitor_template_export.cpp"

"$(INTDIR)\be\be_visitor_template_export.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_template_export.obj" $(SOURCE)

SOURCE="be\be_operation_strategy.cpp"

"$(INTDIR)\be\be_operation_strategy.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_operation_strategy.obj" $(SOURCE)

SOURCE="be\be_structure_fwd.cpp"

"$(INTDIR)\be\be_structure_fwd.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_structure_fwd.obj" $(SOURCE)

SOURCE="be\be_sequence.cpp"

"$(INTDIR)\be\be_sequence.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_sequence.obj" $(SOURCE)

SOURCE="be\be_union_fwd.cpp"

"$(INTDIR)\be\be_union_fwd.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_union_fwd.obj" $(SOURCE)

SOURCE="be\be_valuebox.cpp"

"$(INTDIR)\be\be_valuebox.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_valuebox.obj" $(SOURCE)

SOURCE="be\be_visitor_union_fwd.cpp"

"$(INTDIR)\be\be_visitor_union_fwd.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_union_fwd.obj" $(SOURCE)

SOURCE="be\be_visitor_component_fwd.cpp"

"$(INTDIR)\be\be_visitor_component_fwd.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_component_fwd.obj" $(SOURCE)

SOURCE="be\be_union_label.cpp"

"$(INTDIR)\be\be_union_label.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_union_label.obj" $(SOURCE)

SOURCE="be\be_visitor_interface.cpp"

"$(INTDIR)\be\be_visitor_interface.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_interface.obj" $(SOURCE)

SOURCE="be\be_enum_val.cpp"

"$(INTDIR)\be\be_enum_val.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_enum_val.obj" $(SOURCE)

SOURCE="be\be_union_branch.cpp"

"$(INTDIR)\be\be_union_branch.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_union_branch.obj" $(SOURCE)

SOURCE="be\be_visitor_context.cpp"

"$(INTDIR)\be\be_visitor_context.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_context.obj" $(SOURCE)

SOURCE="be\be_array.cpp"

"$(INTDIR)\be\be_array.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_array.obj" $(SOURCE)

SOURCE="be\be_visitor_constant.cpp"

"$(INTDIR)\be\be_visitor_constant.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_constant.obj" $(SOURCE)

SOURCE="be\be_produce.cpp"

"$(INTDIR)\be\be_produce.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_produce.obj" $(SOURCE)

SOURCE="be\be_visitor.cpp"

"$(INTDIR)\be\be_visitor.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor.obj" $(SOURCE)

SOURCE="be\be_exception.cpp"

"$(INTDIR)\be\be_exception.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_exception.obj" $(SOURCE)

SOURCE="be\be_visitor_structure.cpp"

"$(INTDIR)\be\be_visitor_structure.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_structure.obj" $(SOURCE)

SOURCE="be\be_visitor_array.cpp"

"$(INTDIR)\be\be_visitor_array.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_array.obj" $(SOURCE)

SOURCE="be\be_visitor_ami_pre_proc.cpp"

"$(INTDIR)\be\be_visitor_ami_pre_proc.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_ami_pre_proc.obj" $(SOURCE)

SOURCE="be\be_visitor_attribute.cpp"

"$(INTDIR)\be\be_visitor_attribute.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_attribute.obj" $(SOURCE)

SOURCE="be\be_util.cpp"

"$(INTDIR)\be\be_util.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_util.obj" $(SOURCE)

SOURCE="be\be_valuetype_fwd.cpp"

"$(INTDIR)\be\be_valuetype_fwd.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_valuetype_fwd.obj" $(SOURCE)

SOURCE="be\be_visitor_traits.cpp"

"$(INTDIR)\be\be_visitor_traits.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_traits.obj" $(SOURCE)

SOURCE="be\be_stream_factory.cpp"

"$(INTDIR)\be\be_stream_factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_stream_factory.obj" $(SOURCE)

SOURCE="be\be_visitor_factory.cpp"

"$(INTDIR)\be\be_visitor_factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_factory.obj" $(SOURCE)

SOURCE="be\be_root.cpp"

"$(INTDIR)\be\be_root.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_root.obj" $(SOURCE)

SOURCE="be\be_global.cpp"

"$(INTDIR)\be\be_global.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_global.obj" $(SOURCE)

SOURCE="be\be_codegen.cpp"

"$(INTDIR)\be\be_codegen.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_codegen.obj" $(SOURCE)

SOURCE="be\be_visitor_union.cpp"

"$(INTDIR)\be\be_visitor_union.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_union.obj" $(SOURCE)

SOURCE="be\be_native.cpp"

"$(INTDIR)\be\be_native.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_native.obj" $(SOURCE)

SOURCE="be\be_tmplinst.cpp"

"$(INTDIR)\be\be_tmplinst.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_tmplinst.obj" $(SOURCE)

SOURCE="be\be_visitor_operation.cpp"

"$(INTDIR)\be\be_visitor_operation.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_operation.obj" $(SOURCE)

SOURCE="be\be_helper.cpp"

"$(INTDIR)\be\be_helper.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_helper.obj" $(SOURCE)

SOURCE="be\be_visitor_argument.cpp"

"$(INTDIR)\be\be_visitor_argument.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_argument.obj" $(SOURCE)

SOURCE="be\be_component_fwd.cpp"

"$(INTDIR)\be\be_component_fwd.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_component_fwd.obj" $(SOURCE)

SOURCE="be\be_visitor_interface_fwd.cpp"

"$(INTDIR)\be\be_visitor_interface_fwd.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_interface_fwd.obj" $(SOURCE)

SOURCE="be\be_type.cpp"

"$(INTDIR)\be\be_type.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_type.obj" $(SOURCE)

SOURCE="be\be_typedef.cpp"

"$(INTDIR)\be\be_typedef.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_typedef.obj" $(SOURCE)

SOURCE="be\be_visitor_decl.cpp"

"$(INTDIR)\be\be_visitor_decl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_decl.obj" $(SOURCE)

SOURCE="be\be_visitor_structure_fwd.cpp"

"$(INTDIR)\be\be_visitor_structure_fwd.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_structure_fwd.obj" $(SOURCE)

SOURCE="be\be_module.cpp"

"$(INTDIR)\be\be_module.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_module.obj" $(SOURCE)

SOURCE="be\be_eventtype_fwd.cpp"

"$(INTDIR)\be\be_eventtype_fwd.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_eventtype_fwd.obj" $(SOURCE)

SOURCE="be\be_field.cpp"

"$(INTDIR)\be\be_field.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_field.obj" $(SOURCE)

SOURCE="be\be_visitor_home.cpp"

"$(INTDIR)\be\be_visitor_home.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_home.obj" $(SOURCE)

SOURCE="be\be_operation.cpp"

"$(INTDIR)\be\be_operation.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_operation.obj" $(SOURCE)

SOURCE="be\be_visitor_valuebox.cpp"

"$(INTDIR)\be\be_visitor_valuebox.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_valuebox.obj" $(SOURCE)

SOURCE="be\be_visitor_valuetype.cpp"

"$(INTDIR)\be\be_visitor_valuetype.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_valuetype.obj" $(SOURCE)

SOURCE="be\be_eventtype.cpp"

"$(INTDIR)\be\be_eventtype.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_eventtype.obj" $(SOURCE)

SOURCE="be\be_union.cpp"

"$(INTDIR)\be\be_union.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_union.obj" $(SOURCE)

SOURCE="be\be_visitor_module.cpp"

"$(INTDIR)\be\be_visitor_module.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_module.obj" $(SOURCE)

SOURCE="be\be_visitor_ccm_pre_proc.cpp"

"$(INTDIR)\be\be_visitor_ccm_pre_proc.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_ccm_pre_proc.obj" $(SOURCE)

SOURCE="be\be_visitor_component.cpp"

"$(INTDIR)\be\be_visitor_component.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_visitor_component.obj" $(SOURCE)

SOURCE="be\be_attribute.cpp"

"$(INTDIR)\be\be_attribute.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_attribute.obj" $(SOURCE)

SOURCE="be\be_valuetype.cpp"

"$(INTDIR)\be\be_valuetype.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\be\$(NULL)" mkdir "$(INTDIR)\be\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be\be_valuetype.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.TAO_IDL_BE.dep")
	@echo Using "Makefile.TAO_IDL_BE.dep"
!ELSE
	@echo Warning: cannot find "Makefile.TAO_IDL_BE.dep"
!ENDIF
!ENDIF

