/** $Id: mcpp_lib.cpp 979 2008-12-31 20:22:32Z mitza $
 * @file mcpp_lib.cpp
 * Adapter to ensure the exe and/or lib compilations don't try to use the 
 * same .o file for main.cpp
 */

#include "main.cpp"
