# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.TAO_IDL_FE.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\TAO_IDL_FE\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_IDL_FEd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I"include" -I"fe" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_IDL_FE_BUILD_DLL -f "Makefile.TAO_IDL_FE.dep" "fe\fe_init.cpp" "fe\fe_declarator.cpp" "fe\fe_interface_header.cpp" "fe\fe_lookup.cpp" "fe\fe_extern.cpp" "fe\fe_global.cpp" "fe\y.tab.cpp" "fe\fe_private.cpp" "fe\lex.yy.cpp" "ast\ast_union_fwd.cpp" "ast\ast_interface.cpp" "ast\ast_decl.cpp" "ast\ast_union_label.cpp" "ast\ast_structure_fwd.cpp" "ast\ast_operation.cpp" "ast\ast_attribute.cpp" "ast\ast_valuetype.cpp" "ast\ast_root.cpp" "ast\ast_check.cpp" "ast\ast_union.cpp" "ast\ast_enum.cpp" "ast\ast_module.cpp" "ast\ast_eventtype.cpp" "ast\ast_recursive.cpp" "ast\ast_redef.cpp" "ast\ast_visitor.cpp" "ast\ast_valuetype_fwd.cpp" "ast\ast_native.cpp" "ast\ast_field.cpp" "ast\ast_generator.cpp" "ast\ast_factory.cpp" "ast\ast_constant.cpp" "ast\ast_union_branch.cpp" "ast\ast_component.cpp" "ast\ast_type.cpp" "ast\ast_typedef.cpp" "ast\ast_string.cpp" "ast\ast_predefined_type.cpp" "ast\ast_component_fwd.cpp" "ast\ast_interface_fwd.cpp" "ast\ast_argument.cpp" "ast\ast_eventtype_fwd.cpp" "ast\ast_structure.cpp" "ast\ast_home.cpp" "ast\ast_array.cpp" "ast\ast_enum_val.cpp" "ast\ast_exception.cpp" "ast\ast_expression.cpp" "ast\ast_valuebox.cpp" "ast\ast_concrete_type.cpp" "ast\ast_sequence.cpp" "util\utl_global.cpp" "util\utl_decllist.cpp" "util\utl_labellist.cpp" "util\utl_strlist.cpp" "util\utl_exceptlist.cpp" "util\utl_identifier.cpp" "util\utl_scope.cpp" "util\utl_exprlist.cpp" "util\utl_list.cpp" "util\utl_err.cpp" "util\utl_string.cpp" "util\utl_stack.cpp" "util\utl_namelist.cpp" "util\utl_indenter.cpp" "util\utl_idlist.cpp" "narrow\narrow.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_IDL_FEd.pdb"
	-@del /f/q "..\..\lib\TAO_IDL_FEd.dll"
	-@del /f/q "$(OUTDIR)\TAO_IDL_FEd.lib"
	-@del /f/q "$(OUTDIR)\TAO_IDL_FEd.exp"
	-@del /f/q "$(OUTDIR)\TAO_IDL_FEd.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\TAO_IDL_FE\$(NULL)" mkdir "Debug\TAO_IDL_FE"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4355 /Fd"$(INTDIR)/" /I "..\.." /I "include" /I "fe" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_IDL_FE_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\TAO_IDL_FEd.pdb" /machine:IA64 /out:"..\..\lib\TAO_IDL_FEd.dll" /implib:"$(OUTDIR)\TAO_IDL_FEd.lib"
LINK32_OBJS= \
	"$(INTDIR)\fe\fe_init.obj" \
	"$(INTDIR)\fe\fe_declarator.obj" \
	"$(INTDIR)\fe\fe_interface_header.obj" \
	"$(INTDIR)\fe\fe_lookup.obj" \
	"$(INTDIR)\fe\fe_extern.obj" \
	"$(INTDIR)\fe\fe_global.obj" \
	"$(INTDIR)\fe\y.tab.obj" \
	"$(INTDIR)\fe\fe_private.obj" \
	"$(INTDIR)\fe\lex.yy.obj" \
	"$(INTDIR)\ast\ast_union_fwd.obj" \
	"$(INTDIR)\ast\ast_interface.obj" \
	"$(INTDIR)\ast\ast_decl.obj" \
	"$(INTDIR)\ast\ast_union_label.obj" \
	"$(INTDIR)\ast\ast_structure_fwd.obj" \
	"$(INTDIR)\ast\ast_operation.obj" \
	"$(INTDIR)\ast\ast_attribute.obj" \
	"$(INTDIR)\ast\ast_valuetype.obj" \
	"$(INTDIR)\ast\ast_root.obj" \
	"$(INTDIR)\ast\ast_check.obj" \
	"$(INTDIR)\ast\ast_union.obj" \
	"$(INTDIR)\ast\ast_enum.obj" \
	"$(INTDIR)\ast\ast_module.obj" \
	"$(INTDIR)\ast\ast_eventtype.obj" \
	"$(INTDIR)\ast\ast_recursive.obj" \
	"$(INTDIR)\ast\ast_redef.obj" \
	"$(INTDIR)\ast\ast_visitor.obj" \
	"$(INTDIR)\ast\ast_valuetype_fwd.obj" \
	"$(INTDIR)\ast\ast_native.obj" \
	"$(INTDIR)\ast\ast_field.obj" \
	"$(INTDIR)\ast\ast_generator.obj" \
	"$(INTDIR)\ast\ast_factory.obj" \
	"$(INTDIR)\ast\ast_constant.obj" \
	"$(INTDIR)\ast\ast_union_branch.obj" \
	"$(INTDIR)\ast\ast_component.obj" \
	"$(INTDIR)\ast\ast_type.obj" \
	"$(INTDIR)\ast\ast_typedef.obj" \
	"$(INTDIR)\ast\ast_string.obj" \
	"$(INTDIR)\ast\ast_predefined_type.obj" \
	"$(INTDIR)\ast\ast_component_fwd.obj" \
	"$(INTDIR)\ast\ast_interface_fwd.obj" \
	"$(INTDIR)\ast\ast_argument.obj" \
	"$(INTDIR)\ast\ast_eventtype_fwd.obj" \
	"$(INTDIR)\ast\ast_structure.obj" \
	"$(INTDIR)\ast\ast_home.obj" \
	"$(INTDIR)\ast\ast_array.obj" \
	"$(INTDIR)\ast\ast_enum_val.obj" \
	"$(INTDIR)\ast\ast_exception.obj" \
	"$(INTDIR)\ast\ast_expression.obj" \
	"$(INTDIR)\ast\ast_valuebox.obj" \
	"$(INTDIR)\ast\ast_concrete_type.obj" \
	"$(INTDIR)\ast\ast_sequence.obj" \
	"$(INTDIR)\util\utl_global.obj" \
	"$(INTDIR)\util\utl_decllist.obj" \
	"$(INTDIR)\util\utl_labellist.obj" \
	"$(INTDIR)\util\utl_strlist.obj" \
	"$(INTDIR)\util\utl_exceptlist.obj" \
	"$(INTDIR)\util\utl_identifier.obj" \
	"$(INTDIR)\util\utl_scope.obj" \
	"$(INTDIR)\util\utl_exprlist.obj" \
	"$(INTDIR)\util\utl_list.obj" \
	"$(INTDIR)\util\utl_err.obj" \
	"$(INTDIR)\util\utl_string.obj" \
	"$(INTDIR)\util\utl_stack.obj" \
	"$(INTDIR)\util\utl_namelist.obj" \
	"$(INTDIR)\util\utl_indenter.obj" \
	"$(INTDIR)\util\utl_idlist.obj" \
	"$(INTDIR)\narrow\narrow.obj"

"..\..\lib\TAO_IDL_FEd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_IDL_FEd.dll.manifest" mt.exe -manifest "..\..\lib\TAO_IDL_FEd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\TAO_IDL_FE\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_IDL_FE.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I"include" -I"fe" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_IDL_FE_BUILD_DLL -f "Makefile.TAO_IDL_FE.dep" "fe\fe_init.cpp" "fe\fe_declarator.cpp" "fe\fe_interface_header.cpp" "fe\fe_lookup.cpp" "fe\fe_extern.cpp" "fe\fe_global.cpp" "fe\y.tab.cpp" "fe\fe_private.cpp" "fe\lex.yy.cpp" "ast\ast_union_fwd.cpp" "ast\ast_interface.cpp" "ast\ast_decl.cpp" "ast\ast_union_label.cpp" "ast\ast_structure_fwd.cpp" "ast\ast_operation.cpp" "ast\ast_attribute.cpp" "ast\ast_valuetype.cpp" "ast\ast_root.cpp" "ast\ast_check.cpp" "ast\ast_union.cpp" "ast\ast_enum.cpp" "ast\ast_module.cpp" "ast\ast_eventtype.cpp" "ast\ast_recursive.cpp" "ast\ast_redef.cpp" "ast\ast_visitor.cpp" "ast\ast_valuetype_fwd.cpp" "ast\ast_native.cpp" "ast\ast_field.cpp" "ast\ast_generator.cpp" "ast\ast_factory.cpp" "ast\ast_constant.cpp" "ast\ast_union_branch.cpp" "ast\ast_component.cpp" "ast\ast_type.cpp" "ast\ast_typedef.cpp" "ast\ast_string.cpp" "ast\ast_predefined_type.cpp" "ast\ast_component_fwd.cpp" "ast\ast_interface_fwd.cpp" "ast\ast_argument.cpp" "ast\ast_eventtype_fwd.cpp" "ast\ast_structure.cpp" "ast\ast_home.cpp" "ast\ast_array.cpp" "ast\ast_enum_val.cpp" "ast\ast_exception.cpp" "ast\ast_expression.cpp" "ast\ast_valuebox.cpp" "ast\ast_concrete_type.cpp" "ast\ast_sequence.cpp" "util\utl_global.cpp" "util\utl_decllist.cpp" "util\utl_labellist.cpp" "util\utl_strlist.cpp" "util\utl_exceptlist.cpp" "util\utl_identifier.cpp" "util\utl_scope.cpp" "util\utl_exprlist.cpp" "util\utl_list.cpp" "util\utl_err.cpp" "util\utl_string.cpp" "util\utl_stack.cpp" "util\utl_namelist.cpp" "util\utl_indenter.cpp" "util\utl_idlist.cpp" "narrow\narrow.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\TAO_IDL_FE.dll"
	-@del /f/q "$(OUTDIR)\TAO_IDL_FE.lib"
	-@del /f/q "$(OUTDIR)\TAO_IDL_FE.exp"
	-@del /f/q "$(OUTDIR)\TAO_IDL_FE.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\TAO_IDL_FE\$(NULL)" mkdir "Release\TAO_IDL_FE"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\.." /I "include" /I "fe" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_IDL_FE_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\TAO_IDL_FE.dll" /implib:"$(OUTDIR)\TAO_IDL_FE.lib"
LINK32_OBJS= \
	"$(INTDIR)\fe\fe_init.obj" \
	"$(INTDIR)\fe\fe_declarator.obj" \
	"$(INTDIR)\fe\fe_interface_header.obj" \
	"$(INTDIR)\fe\fe_lookup.obj" \
	"$(INTDIR)\fe\fe_extern.obj" \
	"$(INTDIR)\fe\fe_global.obj" \
	"$(INTDIR)\fe\y.tab.obj" \
	"$(INTDIR)\fe\fe_private.obj" \
	"$(INTDIR)\fe\lex.yy.obj" \
	"$(INTDIR)\ast\ast_union_fwd.obj" \
	"$(INTDIR)\ast\ast_interface.obj" \
	"$(INTDIR)\ast\ast_decl.obj" \
	"$(INTDIR)\ast\ast_union_label.obj" \
	"$(INTDIR)\ast\ast_structure_fwd.obj" \
	"$(INTDIR)\ast\ast_operation.obj" \
	"$(INTDIR)\ast\ast_attribute.obj" \
	"$(INTDIR)\ast\ast_valuetype.obj" \
	"$(INTDIR)\ast\ast_root.obj" \
	"$(INTDIR)\ast\ast_check.obj" \
	"$(INTDIR)\ast\ast_union.obj" \
	"$(INTDIR)\ast\ast_enum.obj" \
	"$(INTDIR)\ast\ast_module.obj" \
	"$(INTDIR)\ast\ast_eventtype.obj" \
	"$(INTDIR)\ast\ast_recursive.obj" \
	"$(INTDIR)\ast\ast_redef.obj" \
	"$(INTDIR)\ast\ast_visitor.obj" \
	"$(INTDIR)\ast\ast_valuetype_fwd.obj" \
	"$(INTDIR)\ast\ast_native.obj" \
	"$(INTDIR)\ast\ast_field.obj" \
	"$(INTDIR)\ast\ast_generator.obj" \
	"$(INTDIR)\ast\ast_factory.obj" \
	"$(INTDIR)\ast\ast_constant.obj" \
	"$(INTDIR)\ast\ast_union_branch.obj" \
	"$(INTDIR)\ast\ast_component.obj" \
	"$(INTDIR)\ast\ast_type.obj" \
	"$(INTDIR)\ast\ast_typedef.obj" \
	"$(INTDIR)\ast\ast_string.obj" \
	"$(INTDIR)\ast\ast_predefined_type.obj" \
	"$(INTDIR)\ast\ast_component_fwd.obj" \
	"$(INTDIR)\ast\ast_interface_fwd.obj" \
	"$(INTDIR)\ast\ast_argument.obj" \
	"$(INTDIR)\ast\ast_eventtype_fwd.obj" \
	"$(INTDIR)\ast\ast_structure.obj" \
	"$(INTDIR)\ast\ast_home.obj" \
	"$(INTDIR)\ast\ast_array.obj" \
	"$(INTDIR)\ast\ast_enum_val.obj" \
	"$(INTDIR)\ast\ast_exception.obj" \
	"$(INTDIR)\ast\ast_expression.obj" \
	"$(INTDIR)\ast\ast_valuebox.obj" \
	"$(INTDIR)\ast\ast_concrete_type.obj" \
	"$(INTDIR)\ast\ast_sequence.obj" \
	"$(INTDIR)\util\utl_global.obj" \
	"$(INTDIR)\util\utl_decllist.obj" \
	"$(INTDIR)\util\utl_labellist.obj" \
	"$(INTDIR)\util\utl_strlist.obj" \
	"$(INTDIR)\util\utl_exceptlist.obj" \
	"$(INTDIR)\util\utl_identifier.obj" \
	"$(INTDIR)\util\utl_scope.obj" \
	"$(INTDIR)\util\utl_exprlist.obj" \
	"$(INTDIR)\util\utl_list.obj" \
	"$(INTDIR)\util\utl_err.obj" \
	"$(INTDIR)\util\utl_string.obj" \
	"$(INTDIR)\util\utl_stack.obj" \
	"$(INTDIR)\util\utl_namelist.obj" \
	"$(INTDIR)\util\utl_indenter.obj" \
	"$(INTDIR)\util\utl_idlist.obj" \
	"$(INTDIR)\narrow\narrow.obj"

"..\..\lib\TAO_IDL_FE.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_IDL_FE.dll.manifest" mt.exe -manifest "..\..\lib\TAO_IDL_FE.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\TAO_IDL_FE\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_IDL_FEsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I"include" -I"fe" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.TAO_IDL_FE.dep" "fe\fe_init.cpp" "fe\fe_declarator.cpp" "fe\fe_interface_header.cpp" "fe\fe_lookup.cpp" "fe\fe_extern.cpp" "fe\fe_global.cpp" "fe\y.tab.cpp" "fe\fe_private.cpp" "fe\lex.yy.cpp" "ast\ast_union_fwd.cpp" "ast\ast_interface.cpp" "ast\ast_decl.cpp" "ast\ast_union_label.cpp" "ast\ast_structure_fwd.cpp" "ast\ast_operation.cpp" "ast\ast_attribute.cpp" "ast\ast_valuetype.cpp" "ast\ast_root.cpp" "ast\ast_check.cpp" "ast\ast_union.cpp" "ast\ast_enum.cpp" "ast\ast_module.cpp" "ast\ast_eventtype.cpp" "ast\ast_recursive.cpp" "ast\ast_redef.cpp" "ast\ast_visitor.cpp" "ast\ast_valuetype_fwd.cpp" "ast\ast_native.cpp" "ast\ast_field.cpp" "ast\ast_generator.cpp" "ast\ast_factory.cpp" "ast\ast_constant.cpp" "ast\ast_union_branch.cpp" "ast\ast_component.cpp" "ast\ast_type.cpp" "ast\ast_typedef.cpp" "ast\ast_string.cpp" "ast\ast_predefined_type.cpp" "ast\ast_component_fwd.cpp" "ast\ast_interface_fwd.cpp" "ast\ast_argument.cpp" "ast\ast_eventtype_fwd.cpp" "ast\ast_structure.cpp" "ast\ast_home.cpp" "ast\ast_array.cpp" "ast\ast_enum_val.cpp" "ast\ast_exception.cpp" "ast\ast_expression.cpp" "ast\ast_valuebox.cpp" "ast\ast_concrete_type.cpp" "ast\ast_sequence.cpp" "util\utl_global.cpp" "util\utl_decllist.cpp" "util\utl_labellist.cpp" "util\utl_strlist.cpp" "util\utl_exceptlist.cpp" "util\utl_identifier.cpp" "util\utl_scope.cpp" "util\utl_exprlist.cpp" "util\utl_list.cpp" "util\utl_err.cpp" "util\utl_string.cpp" "util\utl_stack.cpp" "util\utl_namelist.cpp" "util\utl_indenter.cpp" "util\utl_idlist.cpp" "narrow\narrow.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_IDL_FEsd.lib"
	-@del /f/q "$(OUTDIR)\TAO_IDL_FEsd.exp"
	-@del /f/q "$(OUTDIR)\TAO_IDL_FEsd.ilk"
	-@del /f/q "..\..\lib\TAO_IDL_FEsd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\TAO_IDL_FE\$(NULL)" mkdir "Static_Debug\TAO_IDL_FE"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4355 /Fd"..\..\lib\TAO_IDL_FEsd.pdb" /I "..\.." /I "include" /I "fe" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_IDL_FEsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\fe\fe_init.obj" \
	"$(INTDIR)\fe\fe_declarator.obj" \
	"$(INTDIR)\fe\fe_interface_header.obj" \
	"$(INTDIR)\fe\fe_lookup.obj" \
	"$(INTDIR)\fe\fe_extern.obj" \
	"$(INTDIR)\fe\fe_global.obj" \
	"$(INTDIR)\fe\y.tab.obj" \
	"$(INTDIR)\fe\fe_private.obj" \
	"$(INTDIR)\fe\lex.yy.obj" \
	"$(INTDIR)\ast\ast_union_fwd.obj" \
	"$(INTDIR)\ast\ast_interface.obj" \
	"$(INTDIR)\ast\ast_decl.obj" \
	"$(INTDIR)\ast\ast_union_label.obj" \
	"$(INTDIR)\ast\ast_structure_fwd.obj" \
	"$(INTDIR)\ast\ast_operation.obj" \
	"$(INTDIR)\ast\ast_attribute.obj" \
	"$(INTDIR)\ast\ast_valuetype.obj" \
	"$(INTDIR)\ast\ast_root.obj" \
	"$(INTDIR)\ast\ast_check.obj" \
	"$(INTDIR)\ast\ast_union.obj" \
	"$(INTDIR)\ast\ast_enum.obj" \
	"$(INTDIR)\ast\ast_module.obj" \
	"$(INTDIR)\ast\ast_eventtype.obj" \
	"$(INTDIR)\ast\ast_recursive.obj" \
	"$(INTDIR)\ast\ast_redef.obj" \
	"$(INTDIR)\ast\ast_visitor.obj" \
	"$(INTDIR)\ast\ast_valuetype_fwd.obj" \
	"$(INTDIR)\ast\ast_native.obj" \
	"$(INTDIR)\ast\ast_field.obj" \
	"$(INTDIR)\ast\ast_generator.obj" \
	"$(INTDIR)\ast\ast_factory.obj" \
	"$(INTDIR)\ast\ast_constant.obj" \
	"$(INTDIR)\ast\ast_union_branch.obj" \
	"$(INTDIR)\ast\ast_component.obj" \
	"$(INTDIR)\ast\ast_type.obj" \
	"$(INTDIR)\ast\ast_typedef.obj" \
	"$(INTDIR)\ast\ast_string.obj" \
	"$(INTDIR)\ast\ast_predefined_type.obj" \
	"$(INTDIR)\ast\ast_component_fwd.obj" \
	"$(INTDIR)\ast\ast_interface_fwd.obj" \
	"$(INTDIR)\ast\ast_argument.obj" \
	"$(INTDIR)\ast\ast_eventtype_fwd.obj" \
	"$(INTDIR)\ast\ast_structure.obj" \
	"$(INTDIR)\ast\ast_home.obj" \
	"$(INTDIR)\ast\ast_array.obj" \
	"$(INTDIR)\ast\ast_enum_val.obj" \
	"$(INTDIR)\ast\ast_exception.obj" \
	"$(INTDIR)\ast\ast_expression.obj" \
	"$(INTDIR)\ast\ast_valuebox.obj" \
	"$(INTDIR)\ast\ast_concrete_type.obj" \
	"$(INTDIR)\ast\ast_sequence.obj" \
	"$(INTDIR)\util\utl_global.obj" \
	"$(INTDIR)\util\utl_decllist.obj" \
	"$(INTDIR)\util\utl_labellist.obj" \
	"$(INTDIR)\util\utl_strlist.obj" \
	"$(INTDIR)\util\utl_exceptlist.obj" \
	"$(INTDIR)\util\utl_identifier.obj" \
	"$(INTDIR)\util\utl_scope.obj" \
	"$(INTDIR)\util\utl_exprlist.obj" \
	"$(INTDIR)\util\utl_list.obj" \
	"$(INTDIR)\util\utl_err.obj" \
	"$(INTDIR)\util\utl_string.obj" \
	"$(INTDIR)\util\utl_stack.obj" \
	"$(INTDIR)\util\utl_namelist.obj" \
	"$(INTDIR)\util\utl_indenter.obj" \
	"$(INTDIR)\util\utl_idlist.obj" \
	"$(INTDIR)\narrow\narrow.obj"

"$(OUTDIR)\TAO_IDL_FEsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_IDL_FEsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_IDL_FEsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\TAO_IDL_FE\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_IDL_FEs.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I"include" -I"fe" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.TAO_IDL_FE.dep" "fe\fe_init.cpp" "fe\fe_declarator.cpp" "fe\fe_interface_header.cpp" "fe\fe_lookup.cpp" "fe\fe_extern.cpp" "fe\fe_global.cpp" "fe\y.tab.cpp" "fe\fe_private.cpp" "fe\lex.yy.cpp" "ast\ast_union_fwd.cpp" "ast\ast_interface.cpp" "ast\ast_decl.cpp" "ast\ast_union_label.cpp" "ast\ast_structure_fwd.cpp" "ast\ast_operation.cpp" "ast\ast_attribute.cpp" "ast\ast_valuetype.cpp" "ast\ast_root.cpp" "ast\ast_check.cpp" "ast\ast_union.cpp" "ast\ast_enum.cpp" "ast\ast_module.cpp" "ast\ast_eventtype.cpp" "ast\ast_recursive.cpp" "ast\ast_redef.cpp" "ast\ast_visitor.cpp" "ast\ast_valuetype_fwd.cpp" "ast\ast_native.cpp" "ast\ast_field.cpp" "ast\ast_generator.cpp" "ast\ast_factory.cpp" "ast\ast_constant.cpp" "ast\ast_union_branch.cpp" "ast\ast_component.cpp" "ast\ast_type.cpp" "ast\ast_typedef.cpp" "ast\ast_string.cpp" "ast\ast_predefined_type.cpp" "ast\ast_component_fwd.cpp" "ast\ast_interface_fwd.cpp" "ast\ast_argument.cpp" "ast\ast_eventtype_fwd.cpp" "ast\ast_structure.cpp" "ast\ast_home.cpp" "ast\ast_array.cpp" "ast\ast_enum_val.cpp" "ast\ast_exception.cpp" "ast\ast_expression.cpp" "ast\ast_valuebox.cpp" "ast\ast_concrete_type.cpp" "ast\ast_sequence.cpp" "util\utl_global.cpp" "util\utl_decllist.cpp" "util\utl_labellist.cpp" "util\utl_strlist.cpp" "util\utl_exceptlist.cpp" "util\utl_identifier.cpp" "util\utl_scope.cpp" "util\utl_exprlist.cpp" "util\utl_list.cpp" "util\utl_err.cpp" "util\utl_string.cpp" "util\utl_stack.cpp" "util\utl_namelist.cpp" "util\utl_indenter.cpp" "util\utl_idlist.cpp" "narrow\narrow.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_IDL_FEs.lib"
	-@del /f/q "$(OUTDIR)\TAO_IDL_FEs.exp"
	-@del /f/q "$(OUTDIR)\TAO_IDL_FEs.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\TAO_IDL_FE\$(NULL)" mkdir "Static_Release\TAO_IDL_FE"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\.." /I "include" /I "fe" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_IDL_FEs.lib"
LINK32_OBJS= \
	"$(INTDIR)\fe\fe_init.obj" \
	"$(INTDIR)\fe\fe_declarator.obj" \
	"$(INTDIR)\fe\fe_interface_header.obj" \
	"$(INTDIR)\fe\fe_lookup.obj" \
	"$(INTDIR)\fe\fe_extern.obj" \
	"$(INTDIR)\fe\fe_global.obj" \
	"$(INTDIR)\fe\y.tab.obj" \
	"$(INTDIR)\fe\fe_private.obj" \
	"$(INTDIR)\fe\lex.yy.obj" \
	"$(INTDIR)\ast\ast_union_fwd.obj" \
	"$(INTDIR)\ast\ast_interface.obj" \
	"$(INTDIR)\ast\ast_decl.obj" \
	"$(INTDIR)\ast\ast_union_label.obj" \
	"$(INTDIR)\ast\ast_structure_fwd.obj" \
	"$(INTDIR)\ast\ast_operation.obj" \
	"$(INTDIR)\ast\ast_attribute.obj" \
	"$(INTDIR)\ast\ast_valuetype.obj" \
	"$(INTDIR)\ast\ast_root.obj" \
	"$(INTDIR)\ast\ast_check.obj" \
	"$(INTDIR)\ast\ast_union.obj" \
	"$(INTDIR)\ast\ast_enum.obj" \
	"$(INTDIR)\ast\ast_module.obj" \
	"$(INTDIR)\ast\ast_eventtype.obj" \
	"$(INTDIR)\ast\ast_recursive.obj" \
	"$(INTDIR)\ast\ast_redef.obj" \
	"$(INTDIR)\ast\ast_visitor.obj" \
	"$(INTDIR)\ast\ast_valuetype_fwd.obj" \
	"$(INTDIR)\ast\ast_native.obj" \
	"$(INTDIR)\ast\ast_field.obj" \
	"$(INTDIR)\ast\ast_generator.obj" \
	"$(INTDIR)\ast\ast_factory.obj" \
	"$(INTDIR)\ast\ast_constant.obj" \
	"$(INTDIR)\ast\ast_union_branch.obj" \
	"$(INTDIR)\ast\ast_component.obj" \
	"$(INTDIR)\ast\ast_type.obj" \
	"$(INTDIR)\ast\ast_typedef.obj" \
	"$(INTDIR)\ast\ast_string.obj" \
	"$(INTDIR)\ast\ast_predefined_type.obj" \
	"$(INTDIR)\ast\ast_component_fwd.obj" \
	"$(INTDIR)\ast\ast_interface_fwd.obj" \
	"$(INTDIR)\ast\ast_argument.obj" \
	"$(INTDIR)\ast\ast_eventtype_fwd.obj" \
	"$(INTDIR)\ast\ast_structure.obj" \
	"$(INTDIR)\ast\ast_home.obj" \
	"$(INTDIR)\ast\ast_array.obj" \
	"$(INTDIR)\ast\ast_enum_val.obj" \
	"$(INTDIR)\ast\ast_exception.obj" \
	"$(INTDIR)\ast\ast_expression.obj" \
	"$(INTDIR)\ast\ast_valuebox.obj" \
	"$(INTDIR)\ast\ast_concrete_type.obj" \
	"$(INTDIR)\ast\ast_sequence.obj" \
	"$(INTDIR)\util\utl_global.obj" \
	"$(INTDIR)\util\utl_decllist.obj" \
	"$(INTDIR)\util\utl_labellist.obj" \
	"$(INTDIR)\util\utl_strlist.obj" \
	"$(INTDIR)\util\utl_exceptlist.obj" \
	"$(INTDIR)\util\utl_identifier.obj" \
	"$(INTDIR)\util\utl_scope.obj" \
	"$(INTDIR)\util\utl_exprlist.obj" \
	"$(INTDIR)\util\utl_list.obj" \
	"$(INTDIR)\util\utl_err.obj" \
	"$(INTDIR)\util\utl_string.obj" \
	"$(INTDIR)\util\utl_stack.obj" \
	"$(INTDIR)\util\utl_namelist.obj" \
	"$(INTDIR)\util\utl_indenter.obj" \
	"$(INTDIR)\util\utl_idlist.obj" \
	"$(INTDIR)\narrow\narrow.obj"

"$(OUTDIR)\TAO_IDL_FEs.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_IDL_FEs.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_IDL_FEs.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.TAO_IDL_FE.dep")
!INCLUDE "Makefile.TAO_IDL_FE.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="fe\fe_init.cpp"

"$(INTDIR)\fe\fe_init.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\fe\$(NULL)" mkdir "$(INTDIR)\fe\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\fe\fe_init.obj" $(SOURCE)

SOURCE="fe\fe_declarator.cpp"

"$(INTDIR)\fe\fe_declarator.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\fe\$(NULL)" mkdir "$(INTDIR)\fe\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\fe\fe_declarator.obj" $(SOURCE)

SOURCE="fe\fe_interface_header.cpp"

"$(INTDIR)\fe\fe_interface_header.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\fe\$(NULL)" mkdir "$(INTDIR)\fe\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\fe\fe_interface_header.obj" $(SOURCE)

SOURCE="fe\fe_lookup.cpp"

"$(INTDIR)\fe\fe_lookup.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\fe\$(NULL)" mkdir "$(INTDIR)\fe\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\fe\fe_lookup.obj" $(SOURCE)

SOURCE="fe\fe_extern.cpp"

"$(INTDIR)\fe\fe_extern.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\fe\$(NULL)" mkdir "$(INTDIR)\fe\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\fe\fe_extern.obj" $(SOURCE)

SOURCE="fe\fe_global.cpp"

"$(INTDIR)\fe\fe_global.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\fe\$(NULL)" mkdir "$(INTDIR)\fe\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\fe\fe_global.obj" $(SOURCE)

SOURCE="fe\y.tab.cpp"

"$(INTDIR)\fe\y.tab.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\fe\$(NULL)" mkdir "$(INTDIR)\fe\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\fe\y.tab.obj" $(SOURCE)

SOURCE="fe\fe_private.cpp"

"$(INTDIR)\fe\fe_private.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\fe\$(NULL)" mkdir "$(INTDIR)\fe\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\fe\fe_private.obj" $(SOURCE)

SOURCE="fe\lex.yy.cpp"

"$(INTDIR)\fe\lex.yy.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\fe\$(NULL)" mkdir "$(INTDIR)\fe\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\fe\lex.yy.obj" $(SOURCE)

SOURCE="ast\ast_union_fwd.cpp"

"$(INTDIR)\ast\ast_union_fwd.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_union_fwd.obj" $(SOURCE)

SOURCE="ast\ast_interface.cpp"

"$(INTDIR)\ast\ast_interface.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_interface.obj" $(SOURCE)

SOURCE="ast\ast_decl.cpp"

"$(INTDIR)\ast\ast_decl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_decl.obj" $(SOURCE)

SOURCE="ast\ast_union_label.cpp"

"$(INTDIR)\ast\ast_union_label.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_union_label.obj" $(SOURCE)

SOURCE="ast\ast_structure_fwd.cpp"

"$(INTDIR)\ast\ast_structure_fwd.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_structure_fwd.obj" $(SOURCE)

SOURCE="ast\ast_operation.cpp"

"$(INTDIR)\ast\ast_operation.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_operation.obj" $(SOURCE)

SOURCE="ast\ast_attribute.cpp"

"$(INTDIR)\ast\ast_attribute.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_attribute.obj" $(SOURCE)

SOURCE="ast\ast_valuetype.cpp"

"$(INTDIR)\ast\ast_valuetype.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_valuetype.obj" $(SOURCE)

SOURCE="ast\ast_root.cpp"

"$(INTDIR)\ast\ast_root.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_root.obj" $(SOURCE)

SOURCE="ast\ast_check.cpp"

"$(INTDIR)\ast\ast_check.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_check.obj" $(SOURCE)

SOURCE="ast\ast_union.cpp"

"$(INTDIR)\ast\ast_union.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_union.obj" $(SOURCE)

SOURCE="ast\ast_enum.cpp"

"$(INTDIR)\ast\ast_enum.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_enum.obj" $(SOURCE)

SOURCE="ast\ast_module.cpp"

"$(INTDIR)\ast\ast_module.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_module.obj" $(SOURCE)

SOURCE="ast\ast_eventtype.cpp"

"$(INTDIR)\ast\ast_eventtype.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_eventtype.obj" $(SOURCE)

SOURCE="ast\ast_recursive.cpp"

"$(INTDIR)\ast\ast_recursive.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_recursive.obj" $(SOURCE)

SOURCE="ast\ast_redef.cpp"

"$(INTDIR)\ast\ast_redef.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_redef.obj" $(SOURCE)

SOURCE="ast\ast_visitor.cpp"

"$(INTDIR)\ast\ast_visitor.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_visitor.obj" $(SOURCE)

SOURCE="ast\ast_valuetype_fwd.cpp"

"$(INTDIR)\ast\ast_valuetype_fwd.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_valuetype_fwd.obj" $(SOURCE)

SOURCE="ast\ast_native.cpp"

"$(INTDIR)\ast\ast_native.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_native.obj" $(SOURCE)

SOURCE="ast\ast_field.cpp"

"$(INTDIR)\ast\ast_field.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_field.obj" $(SOURCE)

SOURCE="ast\ast_generator.cpp"

"$(INTDIR)\ast\ast_generator.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_generator.obj" $(SOURCE)

SOURCE="ast\ast_factory.cpp"

"$(INTDIR)\ast\ast_factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_factory.obj" $(SOURCE)

SOURCE="ast\ast_constant.cpp"

"$(INTDIR)\ast\ast_constant.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_constant.obj" $(SOURCE)

SOURCE="ast\ast_union_branch.cpp"

"$(INTDIR)\ast\ast_union_branch.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_union_branch.obj" $(SOURCE)

SOURCE="ast\ast_component.cpp"

"$(INTDIR)\ast\ast_component.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_component.obj" $(SOURCE)

SOURCE="ast\ast_type.cpp"

"$(INTDIR)\ast\ast_type.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_type.obj" $(SOURCE)

SOURCE="ast\ast_typedef.cpp"

"$(INTDIR)\ast\ast_typedef.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_typedef.obj" $(SOURCE)

SOURCE="ast\ast_string.cpp"

"$(INTDIR)\ast\ast_string.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_string.obj" $(SOURCE)

SOURCE="ast\ast_predefined_type.cpp"

"$(INTDIR)\ast\ast_predefined_type.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_predefined_type.obj" $(SOURCE)

SOURCE="ast\ast_component_fwd.cpp"

"$(INTDIR)\ast\ast_component_fwd.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_component_fwd.obj" $(SOURCE)

SOURCE="ast\ast_interface_fwd.cpp"

"$(INTDIR)\ast\ast_interface_fwd.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_interface_fwd.obj" $(SOURCE)

SOURCE="ast\ast_argument.cpp"

"$(INTDIR)\ast\ast_argument.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_argument.obj" $(SOURCE)

SOURCE="ast\ast_eventtype_fwd.cpp"

"$(INTDIR)\ast\ast_eventtype_fwd.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_eventtype_fwd.obj" $(SOURCE)

SOURCE="ast\ast_structure.cpp"

"$(INTDIR)\ast\ast_structure.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_structure.obj" $(SOURCE)

SOURCE="ast\ast_home.cpp"

"$(INTDIR)\ast\ast_home.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_home.obj" $(SOURCE)

SOURCE="ast\ast_array.cpp"

"$(INTDIR)\ast\ast_array.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_array.obj" $(SOURCE)

SOURCE="ast\ast_enum_val.cpp"

"$(INTDIR)\ast\ast_enum_val.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_enum_val.obj" $(SOURCE)

SOURCE="ast\ast_exception.cpp"

"$(INTDIR)\ast\ast_exception.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_exception.obj" $(SOURCE)

SOURCE="ast\ast_expression.cpp"

"$(INTDIR)\ast\ast_expression.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_expression.obj" $(SOURCE)

SOURCE="ast\ast_valuebox.cpp"

"$(INTDIR)\ast\ast_valuebox.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_valuebox.obj" $(SOURCE)

SOURCE="ast\ast_concrete_type.cpp"

"$(INTDIR)\ast\ast_concrete_type.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_concrete_type.obj" $(SOURCE)

SOURCE="ast\ast_sequence.cpp"

"$(INTDIR)\ast\ast_sequence.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ast\$(NULL)" mkdir "$(INTDIR)\ast\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ast\ast_sequence.obj" $(SOURCE)

SOURCE="util\utl_global.cpp"

"$(INTDIR)\util\utl_global.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\util\$(NULL)" mkdir "$(INTDIR)\util\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\util\utl_global.obj" $(SOURCE)

SOURCE="util\utl_decllist.cpp"

"$(INTDIR)\util\utl_decllist.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\util\$(NULL)" mkdir "$(INTDIR)\util\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\util\utl_decllist.obj" $(SOURCE)

SOURCE="util\utl_labellist.cpp"

"$(INTDIR)\util\utl_labellist.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\util\$(NULL)" mkdir "$(INTDIR)\util\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\util\utl_labellist.obj" $(SOURCE)

SOURCE="util\utl_strlist.cpp"

"$(INTDIR)\util\utl_strlist.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\util\$(NULL)" mkdir "$(INTDIR)\util\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\util\utl_strlist.obj" $(SOURCE)

SOURCE="util\utl_exceptlist.cpp"

"$(INTDIR)\util\utl_exceptlist.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\util\$(NULL)" mkdir "$(INTDIR)\util\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\util\utl_exceptlist.obj" $(SOURCE)

SOURCE="util\utl_identifier.cpp"

"$(INTDIR)\util\utl_identifier.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\util\$(NULL)" mkdir "$(INTDIR)\util\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\util\utl_identifier.obj" $(SOURCE)

SOURCE="util\utl_scope.cpp"

"$(INTDIR)\util\utl_scope.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\util\$(NULL)" mkdir "$(INTDIR)\util\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\util\utl_scope.obj" $(SOURCE)

SOURCE="util\utl_exprlist.cpp"

"$(INTDIR)\util\utl_exprlist.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\util\$(NULL)" mkdir "$(INTDIR)\util\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\util\utl_exprlist.obj" $(SOURCE)

SOURCE="util\utl_list.cpp"

"$(INTDIR)\util\utl_list.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\util\$(NULL)" mkdir "$(INTDIR)\util\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\util\utl_list.obj" $(SOURCE)

SOURCE="util\utl_err.cpp"

"$(INTDIR)\util\utl_err.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\util\$(NULL)" mkdir "$(INTDIR)\util\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\util\utl_err.obj" $(SOURCE)

SOURCE="util\utl_string.cpp"

"$(INTDIR)\util\utl_string.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\util\$(NULL)" mkdir "$(INTDIR)\util\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\util\utl_string.obj" $(SOURCE)

SOURCE="util\utl_stack.cpp"

"$(INTDIR)\util\utl_stack.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\util\$(NULL)" mkdir "$(INTDIR)\util\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\util\utl_stack.obj" $(SOURCE)

SOURCE="util\utl_namelist.cpp"

"$(INTDIR)\util\utl_namelist.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\util\$(NULL)" mkdir "$(INTDIR)\util\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\util\utl_namelist.obj" $(SOURCE)

SOURCE="util\utl_indenter.cpp"

"$(INTDIR)\util\utl_indenter.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\util\$(NULL)" mkdir "$(INTDIR)\util\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\util\utl_indenter.obj" $(SOURCE)

SOURCE="util\utl_idlist.cpp"

"$(INTDIR)\util\utl_idlist.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\util\$(NULL)" mkdir "$(INTDIR)\util\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\util\utl_idlist.obj" $(SOURCE)

SOURCE="narrow\narrow.cpp"

"$(INTDIR)\narrow\narrow.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\narrow\$(NULL)" mkdir "$(INTDIR)\narrow\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\narrow\narrow.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.TAO_IDL_FE.dep")
	@echo Using "Makefile.TAO_IDL_FE.dep"
!ELSE
	@echo Warning: cannot find "Makefile.TAO_IDL_FE.dep"
!ENDIF
!ENDIF

