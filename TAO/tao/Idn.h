#pragma once

#include <string>

namespace IDN
{
    std::string hostnameToASCII(const std::wstring& wideString);
}

