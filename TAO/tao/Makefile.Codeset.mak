# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.Codeset.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "Codeset\CodeSetContextC.h" "Codeset\CodeSetContextS.h" "Codeset\CodeSetContextC.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\Codeset\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_Codesetd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_CODESET_BUILD_DLL -f "Makefile.Codeset.dep" "Codeset\CodeSetContextC.cpp" "Codeset\Codeset_Translator_Factory.cpp" "Codeset\Codeset_Manager_Factory.cpp" "Codeset\UTF16_BOM_Factory.cpp" "Codeset\Codeset_Descriptor.cpp" "Codeset\UTF16_BOM_Translator.cpp" "Codeset\UTF8_Latin1_Translator.cpp" "Codeset\Codeset.cpp" "Codeset\UTF8_Latin1_Factory.cpp" "Codeset\Codeset_Manager_i.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_Codesetd.pdb"
	-@del /f/q "..\..\lib\TAO_Codesetd.dll"
	-@del /f/q "$(OUTDIR)\TAO_Codesetd.lib"
	-@del /f/q "$(OUTDIR)\TAO_Codesetd.exp"
	-@del /f/q "$(OUTDIR)\TAO_Codesetd.ilk"
	-@del /f/q "Codeset\CodeSetContextC.h"
	-@del /f/q "Codeset\CodeSetContextS.h"
	-@del /f/q "Codeset\CodeSetContextC.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\Codeset\$(NULL)" mkdir "Debug\Codeset"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_CODESET_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\TAO_Codesetd.pdb" /machine:IA64 /out:"..\..\lib\TAO_Codesetd.dll" /implib:"$(OUTDIR)\TAO_Codesetd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Codeset\TAO_Codeset.res" \
	"$(INTDIR)\Codeset\CodeSetContextC.obj" \
	"$(INTDIR)\Codeset\Codeset_Translator_Factory.obj" \
	"$(INTDIR)\Codeset\Codeset_Manager_Factory.obj" \
	"$(INTDIR)\Codeset\UTF16_BOM_Factory.obj" \
	"$(INTDIR)\Codeset\Codeset_Descriptor.obj" \
	"$(INTDIR)\Codeset\UTF16_BOM_Translator.obj" \
	"$(INTDIR)\Codeset\UTF8_Latin1_Translator.obj" \
	"$(INTDIR)\Codeset\Codeset.obj" \
	"$(INTDIR)\Codeset\UTF8_Latin1_Factory.obj" \
	"$(INTDIR)\Codeset\Codeset_Manager_i.obj"

"..\..\lib\TAO_Codesetd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_Codesetd.dll.manifest" mt.exe -manifest "..\..\lib\TAO_Codesetd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\Codeset\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_Codeset.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_CODESET_BUILD_DLL -f "Makefile.Codeset.dep" "Codeset\CodeSetContextC.cpp" "Codeset\Codeset_Translator_Factory.cpp" "Codeset\Codeset_Manager_Factory.cpp" "Codeset\UTF16_BOM_Factory.cpp" "Codeset\Codeset_Descriptor.cpp" "Codeset\UTF16_BOM_Translator.cpp" "Codeset\UTF8_Latin1_Translator.cpp" "Codeset\Codeset.cpp" "Codeset\UTF8_Latin1_Factory.cpp" "Codeset\Codeset_Manager_i.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\TAO_Codeset.dll"
	-@del /f/q "$(OUTDIR)\TAO_Codeset.lib"
	-@del /f/q "$(OUTDIR)\TAO_Codeset.exp"
	-@del /f/q "$(OUTDIR)\TAO_Codeset.ilk"
	-@del /f/q "Codeset\CodeSetContextC.h"
	-@del /f/q "Codeset\CodeSetContextS.h"
	-@del /f/q "Codeset\CodeSetContextC.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\Codeset\$(NULL)" mkdir "Release\Codeset"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_CODESET_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\TAO_Codeset.dll" /implib:"$(OUTDIR)\TAO_Codeset.lib"
LINK32_OBJS= \
	"$(INTDIR)\Codeset\TAO_Codeset.res" \
	"$(INTDIR)\Codeset\CodeSetContextC.obj" \
	"$(INTDIR)\Codeset\Codeset_Translator_Factory.obj" \
	"$(INTDIR)\Codeset\Codeset_Manager_Factory.obj" \
	"$(INTDIR)\Codeset\UTF16_BOM_Factory.obj" \
	"$(INTDIR)\Codeset\Codeset_Descriptor.obj" \
	"$(INTDIR)\Codeset\UTF16_BOM_Translator.obj" \
	"$(INTDIR)\Codeset\UTF8_Latin1_Translator.obj" \
	"$(INTDIR)\Codeset\Codeset.obj" \
	"$(INTDIR)\Codeset\UTF8_Latin1_Factory.obj" \
	"$(INTDIR)\Codeset\Codeset_Manager_i.obj"

"..\..\lib\TAO_Codeset.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_Codeset.dll.manifest" mt.exe -manifest "..\..\lib\TAO_Codeset.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\Codeset\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_Codesetsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Codeset.dep" "Codeset\CodeSetContextC.cpp" "Codeset\Codeset_Translator_Factory.cpp" "Codeset\Codeset_Manager_Factory.cpp" "Codeset\UTF16_BOM_Factory.cpp" "Codeset\Codeset_Descriptor.cpp" "Codeset\UTF16_BOM_Translator.cpp" "Codeset\UTF8_Latin1_Translator.cpp" "Codeset\Codeset.cpp" "Codeset\UTF8_Latin1_Factory.cpp" "Codeset\Codeset_Manager_i.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_Codesetsd.lib"
	-@del /f/q "$(OUTDIR)\TAO_Codesetsd.exp"
	-@del /f/q "$(OUTDIR)\TAO_Codesetsd.ilk"
	-@del /f/q "..\..\lib\TAO_Codesetsd.pdb"
	-@del /f/q "Codeset\CodeSetContextC.h"
	-@del /f/q "Codeset\CodeSetContextS.h"
	-@del /f/q "Codeset\CodeSetContextC.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\Codeset\$(NULL)" mkdir "Static_Debug\Codeset"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"..\..\lib\TAO_Codesetsd.pdb" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_Codesetsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Codeset\CodeSetContextC.obj" \
	"$(INTDIR)\Codeset\Codeset_Translator_Factory.obj" \
	"$(INTDIR)\Codeset\Codeset_Manager_Factory.obj" \
	"$(INTDIR)\Codeset\UTF16_BOM_Factory.obj" \
	"$(INTDIR)\Codeset\Codeset_Descriptor.obj" \
	"$(INTDIR)\Codeset\UTF16_BOM_Translator.obj" \
	"$(INTDIR)\Codeset\UTF8_Latin1_Translator.obj" \
	"$(INTDIR)\Codeset\Codeset.obj" \
	"$(INTDIR)\Codeset\UTF8_Latin1_Factory.obj" \
	"$(INTDIR)\Codeset\Codeset_Manager_i.obj"

"$(OUTDIR)\TAO_Codesetsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_Codesetsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_Codesetsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\Codeset\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_Codesets.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Codeset.dep" "Codeset\CodeSetContextC.cpp" "Codeset\Codeset_Translator_Factory.cpp" "Codeset\Codeset_Manager_Factory.cpp" "Codeset\UTF16_BOM_Factory.cpp" "Codeset\Codeset_Descriptor.cpp" "Codeset\UTF16_BOM_Translator.cpp" "Codeset\UTF8_Latin1_Translator.cpp" "Codeset\Codeset.cpp" "Codeset\UTF8_Latin1_Factory.cpp" "Codeset\Codeset_Manager_i.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_Codesets.lib"
	-@del /f/q "$(OUTDIR)\TAO_Codesets.exp"
	-@del /f/q "$(OUTDIR)\TAO_Codesets.ilk"
	-@del /f/q "Codeset\CodeSetContextC.h"
	-@del /f/q "Codeset\CodeSetContextS.h"
	-@del /f/q "Codeset\CodeSetContextC.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\Codeset\$(NULL)" mkdir "Static_Release\Codeset"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_Codesets.lib"
LINK32_OBJS= \
	"$(INTDIR)\Codeset\CodeSetContextC.obj" \
	"$(INTDIR)\Codeset\Codeset_Translator_Factory.obj" \
	"$(INTDIR)\Codeset\Codeset_Manager_Factory.obj" \
	"$(INTDIR)\Codeset\UTF16_BOM_Factory.obj" \
	"$(INTDIR)\Codeset\Codeset_Descriptor.obj" \
	"$(INTDIR)\Codeset\UTF16_BOM_Translator.obj" \
	"$(INTDIR)\Codeset\UTF8_Latin1_Translator.obj" \
	"$(INTDIR)\Codeset\Codeset.obj" \
	"$(INTDIR)\Codeset\UTF8_Latin1_Factory.obj" \
	"$(INTDIR)\Codeset\Codeset_Manager_i.obj"

"$(OUTDIR)\TAO_Codesets.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_Codesets.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_Codesets.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Codeset.dep")
!INCLUDE "Makefile.Codeset.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Codeset\CodeSetContextC.cpp"

"$(INTDIR)\Codeset\CodeSetContextC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Codeset\$(NULL)" mkdir "$(INTDIR)\Codeset\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Codeset\CodeSetContextC.obj" $(SOURCE)

SOURCE="Codeset\Codeset_Translator_Factory.cpp"

"$(INTDIR)\Codeset\Codeset_Translator_Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Codeset\$(NULL)" mkdir "$(INTDIR)\Codeset\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Codeset\Codeset_Translator_Factory.obj" $(SOURCE)

SOURCE="Codeset\Codeset_Manager_Factory.cpp"

"$(INTDIR)\Codeset\Codeset_Manager_Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Codeset\$(NULL)" mkdir "$(INTDIR)\Codeset\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Codeset\Codeset_Manager_Factory.obj" $(SOURCE)

SOURCE="Codeset\UTF16_BOM_Factory.cpp"

"$(INTDIR)\Codeset\UTF16_BOM_Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Codeset\$(NULL)" mkdir "$(INTDIR)\Codeset\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Codeset\UTF16_BOM_Factory.obj" $(SOURCE)

SOURCE="Codeset\Codeset_Descriptor.cpp"

"$(INTDIR)\Codeset\Codeset_Descriptor.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Codeset\$(NULL)" mkdir "$(INTDIR)\Codeset\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Codeset\Codeset_Descriptor.obj" $(SOURCE)

SOURCE="Codeset\UTF16_BOM_Translator.cpp"

"$(INTDIR)\Codeset\UTF16_BOM_Translator.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Codeset\$(NULL)" mkdir "$(INTDIR)\Codeset\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Codeset\UTF16_BOM_Translator.obj" $(SOURCE)

SOURCE="Codeset\UTF8_Latin1_Translator.cpp"

"$(INTDIR)\Codeset\UTF8_Latin1_Translator.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Codeset\$(NULL)" mkdir "$(INTDIR)\Codeset\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Codeset\UTF8_Latin1_Translator.obj" $(SOURCE)

SOURCE="Codeset\Codeset.cpp"

"$(INTDIR)\Codeset\Codeset.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Codeset\$(NULL)" mkdir "$(INTDIR)\Codeset\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Codeset\Codeset.obj" $(SOURCE)

SOURCE="Codeset\UTF8_Latin1_Factory.cpp"

"$(INTDIR)\Codeset\UTF8_Latin1_Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Codeset\$(NULL)" mkdir "$(INTDIR)\Codeset\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Codeset\UTF8_Latin1_Factory.obj" $(SOURCE)

SOURCE="Codeset\Codeset_Manager_i.cpp"

"$(INTDIR)\Codeset\Codeset_Manager_i.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Codeset\$(NULL)" mkdir "$(INTDIR)\Codeset\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Codeset\Codeset_Manager_i.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="Codeset\CodeSetContext.pidl"

InputPath=Codeset\CodeSetContext.pidl

"Codeset\CodeSetContextC.h" "Codeset\CodeSetContextS.h" "Codeset\CodeSetContextC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Codeset_CodeSetContext_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -St -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Sorb -Wb,export_macro=TAO_Codeset_Export -Wb,export_include=tao/Codeset/codeset_export.h -o Codeset "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="Codeset\CodeSetContext.pidl"

InputPath=Codeset\CodeSetContext.pidl

"Codeset\CodeSetContextC.h" "Codeset\CodeSetContextS.h" "Codeset\CodeSetContextC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Codeset_CodeSetContext_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -St -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Sorb -Wb,export_macro=TAO_Codeset_Export -Wb,export_include=tao/Codeset/codeset_export.h -o Codeset "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="Codeset\CodeSetContext.pidl"

InputPath=Codeset\CodeSetContext.pidl

"Codeset\CodeSetContextC.h" "Codeset\CodeSetContextS.h" "Codeset\CodeSetContextC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Codeset_CodeSetContext_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -St -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Sorb -Wb,export_macro=TAO_Codeset_Export -Wb,export_include=tao/Codeset/codeset_export.h -o Codeset "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="Codeset\CodeSetContext.pidl"

InputPath=Codeset\CodeSetContext.pidl

"Codeset\CodeSetContextC.h" "Codeset\CodeSetContextS.h" "Codeset\CodeSetContextC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Codeset_CodeSetContext_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -St -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Sorb -Wb,export_macro=TAO_Codeset_Export -Wb,export_include=tao/Codeset/codeset_export.h -o Codeset "$(InputPath)"
<<

!ENDIF

SOURCE="Codeset\TAO_Codeset.rc"

"$(INTDIR)\Codeset\TAO_Codeset.res" : $(SOURCE)
	@if not exist "$(INTDIR)\Codeset\$(NULL)" mkdir "$(INTDIR)\Codeset\"
	$(RSC) /l 0x409 /fo"$(INTDIR)\Codeset\TAO_Codeset.res" /d NDEBUG /d WIN64 /d _CRT_SECURE_NO_WARNINGS /d _CRT_SECURE_NO_DEPRECATE /d _CRT_NONSTDC_NO_DEPRECATE /i "..\.." /i ".." $(SOURCE)



!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Codeset.dep")
	@echo Using "Makefile.Codeset.dep"
!ELSE
	@echo Warning: cannot find "Makefile.Codeset.dep"
!ENDIF
!ENDIF

