# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.Valuetype.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "Valuetype\StringValueC.inl" "Valuetype\StringValueC.h" "Valuetype\StringValueS.h" "Valuetype\StringValueC.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\Valuetype\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_Valuetyped.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_HAS_VALUETYPE_OUT_INDIRECTION -DTAO_VALUETYPE_BUILD_DLL -f "Makefile.Valuetype.dep" "Valuetype\StringValueC.cpp" "Valuetype\Valuetype_Adapter_Factory_Impl.cpp" "Valuetype\ValueBase.cpp" "Valuetype\AbstractBase_Invocation_Adapter.cpp" "Valuetype\Valuetype_Adapter_Impl.cpp" "Valuetype\ValueFactory.cpp" "Valuetype\ValueFactory_Map.cpp" "Valuetype\AbstractBase.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_Valuetyped.pdb"
	-@del /f/q "..\..\lib\TAO_Valuetyped.dll"
	-@del /f/q "$(OUTDIR)\TAO_Valuetyped.lib"
	-@del /f/q "$(OUTDIR)\TAO_Valuetyped.exp"
	-@del /f/q "$(OUTDIR)\TAO_Valuetyped.ilk"
	-@del /f/q "Valuetype\StringValueC.inl"
	-@del /f/q "Valuetype\StringValueC.h"
	-@del /f/q "Valuetype\StringValueS.h"
	-@del /f/q "Valuetype\StringValueC.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\Valuetype\$(NULL)" mkdir "Debug\Valuetype"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_HAS_VALUETYPE_OUT_INDIRECTION /D TAO_VALUETYPE_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\TAO_Valuetyped.pdb" /machine:IA64 /out:"..\..\lib\TAO_Valuetyped.dll" /implib:"$(OUTDIR)\TAO_Valuetyped.lib"
LINK32_OBJS= \
	"$(INTDIR)\Valuetype\TAO_Valuetype.res" \
	"$(INTDIR)\Valuetype\StringValueC.obj" \
	"$(INTDIR)\Valuetype\Valuetype_Adapter_Factory_Impl.obj" \
	"$(INTDIR)\Valuetype\ValueBase.obj" \
	"$(INTDIR)\Valuetype\AbstractBase_Invocation_Adapter.obj" \
	"$(INTDIR)\Valuetype\Valuetype_Adapter_Impl.obj" \
	"$(INTDIR)\Valuetype\ValueFactory.obj" \
	"$(INTDIR)\Valuetype\ValueFactory_Map.obj" \
	"$(INTDIR)\Valuetype\AbstractBase.obj"

"..\..\lib\TAO_Valuetyped.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_Valuetyped.dll.manifest" mt.exe -manifest "..\..\lib\TAO_Valuetyped.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\Valuetype\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_Valuetype.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_HAS_VALUETYPE_OUT_INDIRECTION -DTAO_VALUETYPE_BUILD_DLL -f "Makefile.Valuetype.dep" "Valuetype\StringValueC.cpp" "Valuetype\Valuetype_Adapter_Factory_Impl.cpp" "Valuetype\ValueBase.cpp" "Valuetype\AbstractBase_Invocation_Adapter.cpp" "Valuetype\Valuetype_Adapter_Impl.cpp" "Valuetype\ValueFactory.cpp" "Valuetype\ValueFactory_Map.cpp" "Valuetype\AbstractBase.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\TAO_Valuetype.dll"
	-@del /f/q "$(OUTDIR)\TAO_Valuetype.lib"
	-@del /f/q "$(OUTDIR)\TAO_Valuetype.exp"
	-@del /f/q "$(OUTDIR)\TAO_Valuetype.ilk"
	-@del /f/q "Valuetype\StringValueC.inl"
	-@del /f/q "Valuetype\StringValueC.h"
	-@del /f/q "Valuetype\StringValueS.h"
	-@del /f/q "Valuetype\StringValueC.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\Valuetype\$(NULL)" mkdir "Release\Valuetype"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_HAS_VALUETYPE_OUT_INDIRECTION /D TAO_VALUETYPE_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\TAO_Valuetype.dll" /implib:"$(OUTDIR)\TAO_Valuetype.lib"
LINK32_OBJS= \
	"$(INTDIR)\Valuetype\TAO_Valuetype.res" \
	"$(INTDIR)\Valuetype\StringValueC.obj" \
	"$(INTDIR)\Valuetype\Valuetype_Adapter_Factory_Impl.obj" \
	"$(INTDIR)\Valuetype\ValueBase.obj" \
	"$(INTDIR)\Valuetype\AbstractBase_Invocation_Adapter.obj" \
	"$(INTDIR)\Valuetype\Valuetype_Adapter_Impl.obj" \
	"$(INTDIR)\Valuetype\ValueFactory.obj" \
	"$(INTDIR)\Valuetype\ValueFactory_Map.obj" \
	"$(INTDIR)\Valuetype\AbstractBase.obj"

"..\..\lib\TAO_Valuetype.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_Valuetype.dll.manifest" mt.exe -manifest "..\..\lib\TAO_Valuetype.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\Valuetype\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_Valuetypesd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_HAS_VALUETYPE_OUT_INDIRECTION -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Valuetype.dep" "Valuetype\StringValueC.cpp" "Valuetype\Valuetype_Adapter_Factory_Impl.cpp" "Valuetype\ValueBase.cpp" "Valuetype\AbstractBase_Invocation_Adapter.cpp" "Valuetype\Valuetype_Adapter_Impl.cpp" "Valuetype\ValueFactory.cpp" "Valuetype\ValueFactory_Map.cpp" "Valuetype\AbstractBase.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_Valuetypesd.lib"
	-@del /f/q "$(OUTDIR)\TAO_Valuetypesd.exp"
	-@del /f/q "$(OUTDIR)\TAO_Valuetypesd.ilk"
	-@del /f/q "..\..\lib\TAO_Valuetypesd.pdb"
	-@del /f/q "Valuetype\StringValueC.inl"
	-@del /f/q "Valuetype\StringValueC.h"
	-@del /f/q "Valuetype\StringValueS.h"
	-@del /f/q "Valuetype\StringValueC.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\Valuetype\$(NULL)" mkdir "Static_Debug\Valuetype"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"..\..\lib\TAO_Valuetypesd.pdb" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_HAS_VALUETYPE_OUT_INDIRECTION /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_Valuetypesd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Valuetype\StringValueC.obj" \
	"$(INTDIR)\Valuetype\Valuetype_Adapter_Factory_Impl.obj" \
	"$(INTDIR)\Valuetype\ValueBase.obj" \
	"$(INTDIR)\Valuetype\AbstractBase_Invocation_Adapter.obj" \
	"$(INTDIR)\Valuetype\Valuetype_Adapter_Impl.obj" \
	"$(INTDIR)\Valuetype\ValueFactory.obj" \
	"$(INTDIR)\Valuetype\ValueFactory_Map.obj" \
	"$(INTDIR)\Valuetype\AbstractBase.obj"

"$(OUTDIR)\TAO_Valuetypesd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_Valuetypesd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_Valuetypesd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\Valuetype\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_Valuetypes.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_HAS_VALUETYPE_OUT_INDIRECTION -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Valuetype.dep" "Valuetype\StringValueC.cpp" "Valuetype\Valuetype_Adapter_Factory_Impl.cpp" "Valuetype\ValueBase.cpp" "Valuetype\AbstractBase_Invocation_Adapter.cpp" "Valuetype\Valuetype_Adapter_Impl.cpp" "Valuetype\ValueFactory.cpp" "Valuetype\ValueFactory_Map.cpp" "Valuetype\AbstractBase.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_Valuetypes.lib"
	-@del /f/q "$(OUTDIR)\TAO_Valuetypes.exp"
	-@del /f/q "$(OUTDIR)\TAO_Valuetypes.ilk"
	-@del /f/q "Valuetype\StringValueC.inl"
	-@del /f/q "Valuetype\StringValueC.h"
	-@del /f/q "Valuetype\StringValueS.h"
	-@del /f/q "Valuetype\StringValueC.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\Valuetype\$(NULL)" mkdir "Static_Release\Valuetype"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_HAS_VALUETYPE_OUT_INDIRECTION /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_Valuetypes.lib"
LINK32_OBJS= \
	"$(INTDIR)\Valuetype\StringValueC.obj" \
	"$(INTDIR)\Valuetype\Valuetype_Adapter_Factory_Impl.obj" \
	"$(INTDIR)\Valuetype\ValueBase.obj" \
	"$(INTDIR)\Valuetype\AbstractBase_Invocation_Adapter.obj" \
	"$(INTDIR)\Valuetype\Valuetype_Adapter_Impl.obj" \
	"$(INTDIR)\Valuetype\ValueFactory.obj" \
	"$(INTDIR)\Valuetype\ValueFactory_Map.obj" \
	"$(INTDIR)\Valuetype\AbstractBase.obj"

"$(OUTDIR)\TAO_Valuetypes.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_Valuetypes.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_Valuetypes.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Valuetype.dep")
!INCLUDE "Makefile.Valuetype.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Valuetype\StringValueC.cpp"

"$(INTDIR)\Valuetype\StringValueC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Valuetype\$(NULL)" mkdir "$(INTDIR)\Valuetype\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Valuetype\StringValueC.obj" $(SOURCE)

SOURCE="Valuetype\Valuetype_Adapter_Factory_Impl.cpp"

"$(INTDIR)\Valuetype\Valuetype_Adapter_Factory_Impl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Valuetype\$(NULL)" mkdir "$(INTDIR)\Valuetype\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Valuetype\Valuetype_Adapter_Factory_Impl.obj" $(SOURCE)

SOURCE="Valuetype\ValueBase.cpp"

"$(INTDIR)\Valuetype\ValueBase.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Valuetype\$(NULL)" mkdir "$(INTDIR)\Valuetype\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Valuetype\ValueBase.obj" $(SOURCE)

SOURCE="Valuetype\AbstractBase_Invocation_Adapter.cpp"

"$(INTDIR)\Valuetype\AbstractBase_Invocation_Adapter.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Valuetype\$(NULL)" mkdir "$(INTDIR)\Valuetype\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Valuetype\AbstractBase_Invocation_Adapter.obj" $(SOURCE)

SOURCE="Valuetype\Valuetype_Adapter_Impl.cpp"

"$(INTDIR)\Valuetype\Valuetype_Adapter_Impl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Valuetype\$(NULL)" mkdir "$(INTDIR)\Valuetype\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Valuetype\Valuetype_Adapter_Impl.obj" $(SOURCE)

SOURCE="Valuetype\ValueFactory.cpp"

"$(INTDIR)\Valuetype\ValueFactory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Valuetype\$(NULL)" mkdir "$(INTDIR)\Valuetype\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Valuetype\ValueFactory.obj" $(SOURCE)

SOURCE="Valuetype\ValueFactory_Map.cpp"

"$(INTDIR)\Valuetype\ValueFactory_Map.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Valuetype\$(NULL)" mkdir "$(INTDIR)\Valuetype\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Valuetype\ValueFactory_Map.obj" $(SOURCE)

SOURCE="Valuetype\AbstractBase.cpp"

"$(INTDIR)\Valuetype\AbstractBase.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Valuetype\$(NULL)" mkdir "$(INTDIR)\Valuetype\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Valuetype\AbstractBase.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="Valuetype\StringValue.pidl"

InputPath=Valuetype\StringValue.pidl

"Valuetype\StringValueC.inl" "Valuetype\StringValueC.h" "Valuetype\StringValueS.h" "Valuetype\StringValueC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Valuetype_StringValue_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -Wb,export_macro=TAO_Valuetype_Export -Wb,export_include=tao/Valuetype/valuetype_export.h -o Valuetype "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="Valuetype\StringValue.pidl"

InputPath=Valuetype\StringValue.pidl

"Valuetype\StringValueC.inl" "Valuetype\StringValueC.h" "Valuetype\StringValueS.h" "Valuetype\StringValueC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Valuetype_StringValue_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -Wb,export_macro=TAO_Valuetype_Export -Wb,export_include=tao/Valuetype/valuetype_export.h -o Valuetype "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="Valuetype\StringValue.pidl"

InputPath=Valuetype\StringValue.pidl

"Valuetype\StringValueC.inl" "Valuetype\StringValueC.h" "Valuetype\StringValueS.h" "Valuetype\StringValueC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Valuetype_StringValue_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -Wb,export_macro=TAO_Valuetype_Export -Wb,export_include=tao/Valuetype/valuetype_export.h -o Valuetype "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="Valuetype\StringValue.pidl"

InputPath=Valuetype\StringValue.pidl

"Valuetype\StringValueC.inl" "Valuetype\StringValueC.h" "Valuetype\StringValueS.h" "Valuetype\StringValueC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Valuetype_StringValue_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -Wb,export_macro=TAO_Valuetype_Export -Wb,export_include=tao/Valuetype/valuetype_export.h -o Valuetype "$(InputPath)"
<<

!ENDIF

SOURCE="Valuetype\TAO_Valuetype.rc"

"$(INTDIR)\Valuetype\TAO_Valuetype.res" : $(SOURCE)
	@if not exist "$(INTDIR)\Valuetype\$(NULL)" mkdir "$(INTDIR)\Valuetype\"
	$(RSC) /l 0x409 /fo"$(INTDIR)\Valuetype\TAO_Valuetype.res" /d NDEBUG /d WIN64 /d _CRT_SECURE_NO_WARNINGS /d _CRT_SECURE_NO_DEPRECATE /d _CRT_NONSTDC_NO_DEPRECATE /d TAO_HAS_VALUETYPE_OUT_INDIRECTION /i "..\.." /i ".." $(SOURCE)



!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Valuetype.dep")
	@echo Using "Makefile.Valuetype.dep"
!ELSE
	@echo Warning: cannot find "Makefile.Valuetype.dep"
!ENDIF
!ENDIF

