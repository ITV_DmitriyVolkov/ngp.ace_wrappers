
//$Id: target_specification.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/target_specification.h"

#if !defined (__ACE_INLINE__)
#include "tao/target_specification.inl"
#endif /* !defined INLINE */

ACE_RCSID (tao, 
           target_specification, 
           "$Id: target_specification.cpp 14 2007-02-01 15:49:12Z mitza $")
