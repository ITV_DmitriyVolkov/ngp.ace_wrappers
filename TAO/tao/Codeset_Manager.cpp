// $Id: Codeset_Manager.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/Codeset_Manager.h"
#include "tao/Codeset_Descriptor_Base.h"

ACE_RCSID (tao,
           Codeset_Manager,
           "$Id: Codeset_Manager.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Codeset_Manager::~TAO_Codeset_Manager (void)
{
}

// Add the destructor for the codeset_descriptor_base here, because
// it is only ever used in conjunction with the codeset manager.

TAO_Codeset_Descriptor_Base::~TAO_Codeset_Descriptor_Base (void)
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
