// $Id: ORBInitializer_Registry_Adapter.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/ORBInitializer_Registry_Adapter.h"

ACE_RCSID (tao,
           ORBInitializer_Registry_Adapter,
           "$Id: ORBInitializer_Registry_Adapter.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

namespace TAO
{
  ORBInitializer_Registry_Adapter::~ORBInitializer_Registry_Adapter (void)
  {
  }
}

TAO_END_VERSIONED_NAMESPACE_DECL
