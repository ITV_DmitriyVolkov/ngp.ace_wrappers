# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.CSD_Framework.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "CSD_Framework\CSD_FrameworkC.h" "CSD_Framework\CSD_FrameworkS.h" "CSD_Framework\CSD_FrameworkA.h" "CSD_Framework\CSD_FrameworkC.cpp" "CSD_Framework\CSD_FrameworkA.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\CSD_Framework\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_CSD_Frameworkd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -I"..\tao" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_CSD_FW_BUILD_DLL -f "Makefile.CSD_Framework.dep" "CSD_Framework\CSD_FrameworkC.cpp" "CSD_Framework\CSD_FrameworkA.cpp" "CSD_Framework\CSD_ORBInitializer.cpp" "CSD_Framework\CSD_Object_Adapter.cpp" "CSD_Framework\CSD_Object_Adapter_Factory.cpp" "CSD_Framework\CSD_Default_Servant_Dispatcher.cpp" "CSD_Framework\CSD_Strategy_Repository.cpp" "CSD_Framework\CSD_Strategy_Proxy.cpp" "CSD_Framework\CSD_Strategy_Base.cpp" "CSD_Framework\CSD_FW_Server_Request_Wrapper.cpp" "CSD_Framework\CSD_POA.cpp" "CSD_Framework\CSD_Framework_Loader.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_CSD_Frameworkd.pdb"
	-@del /f/q "..\..\lib\TAO_CSD_Frameworkd.dll"
	-@del /f/q "$(OUTDIR)\TAO_CSD_Frameworkd.lib"
	-@del /f/q "$(OUTDIR)\TAO_CSD_Frameworkd.exp"
	-@del /f/q "$(OUTDIR)\TAO_CSD_Frameworkd.ilk"
	-@del /f/q "CSD_Framework\CSD_FrameworkC.h"
	-@del /f/q "CSD_Framework\CSD_FrameworkS.h"
	-@del /f/q "CSD_Framework\CSD_FrameworkA.h"
	-@del /f/q "CSD_Framework\CSD_FrameworkC.cpp"
	-@del /f/q "CSD_Framework\CSD_FrameworkA.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\CSD_Framework\$(NULL)" mkdir "Debug\CSD_Framework"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\.." /I ".." /I "..\tao" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_CSD_FW_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_PortableServerd.lib TAO_CodecFactoryd.lib TAO_PId.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\TAO_CSD_Frameworkd.pdb" /machine:IA64 /out:"..\..\lib\TAO_CSD_Frameworkd.dll" /implib:"$(OUTDIR)\TAO_CSD_Frameworkd.lib"
LINK32_OBJS= \
	"$(INTDIR)\CSD_Framework\TAO_CSD_Framework.res" \
	"$(INTDIR)\CSD_Framework\CSD_FrameworkC.obj" \
	"$(INTDIR)\CSD_Framework\CSD_FrameworkA.obj" \
	"$(INTDIR)\CSD_Framework\CSD_ORBInitializer.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Object_Adapter.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Object_Adapter_Factory.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Default_Servant_Dispatcher.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Strategy_Repository.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Strategy_Proxy.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Strategy_Base.obj" \
	"$(INTDIR)\CSD_Framework\CSD_FW_Server_Request_Wrapper.obj" \
	"$(INTDIR)\CSD_Framework\CSD_POA.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Framework_Loader.obj"

"..\..\lib\TAO_CSD_Frameworkd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_CSD_Frameworkd.dll.manifest" mt.exe -manifest "..\..\lib\TAO_CSD_Frameworkd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\CSD_Framework\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_CSD_Framework.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -I"..\tao" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_CSD_FW_BUILD_DLL -f "Makefile.CSD_Framework.dep" "CSD_Framework\CSD_FrameworkC.cpp" "CSD_Framework\CSD_FrameworkA.cpp" "CSD_Framework\CSD_ORBInitializer.cpp" "CSD_Framework\CSD_Object_Adapter.cpp" "CSD_Framework\CSD_Object_Adapter_Factory.cpp" "CSD_Framework\CSD_Default_Servant_Dispatcher.cpp" "CSD_Framework\CSD_Strategy_Repository.cpp" "CSD_Framework\CSD_Strategy_Proxy.cpp" "CSD_Framework\CSD_Strategy_Base.cpp" "CSD_Framework\CSD_FW_Server_Request_Wrapper.cpp" "CSD_Framework\CSD_POA.cpp" "CSD_Framework\CSD_Framework_Loader.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\TAO_CSD_Framework.dll"
	-@del /f/q "$(OUTDIR)\TAO_CSD_Framework.lib"
	-@del /f/q "$(OUTDIR)\TAO_CSD_Framework.exp"
	-@del /f/q "$(OUTDIR)\TAO_CSD_Framework.ilk"
	-@del /f/q "CSD_Framework\CSD_FrameworkC.h"
	-@del /f/q "CSD_Framework\CSD_FrameworkS.h"
	-@del /f/q "CSD_Framework\CSD_FrameworkA.h"
	-@del /f/q "CSD_Framework\CSD_FrameworkC.cpp"
	-@del /f/q "CSD_Framework\CSD_FrameworkA.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\CSD_Framework\$(NULL)" mkdir "Release\CSD_Framework"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /I "..\tao" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_CSD_FW_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_PortableServer.lib TAO_CodecFactory.lib TAO_PI.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\TAO_CSD_Framework.dll" /implib:"$(OUTDIR)\TAO_CSD_Framework.lib"
LINK32_OBJS= \
	"$(INTDIR)\CSD_Framework\TAO_CSD_Framework.res" \
	"$(INTDIR)\CSD_Framework\CSD_FrameworkC.obj" \
	"$(INTDIR)\CSD_Framework\CSD_FrameworkA.obj" \
	"$(INTDIR)\CSD_Framework\CSD_ORBInitializer.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Object_Adapter.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Object_Adapter_Factory.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Default_Servant_Dispatcher.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Strategy_Repository.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Strategy_Proxy.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Strategy_Base.obj" \
	"$(INTDIR)\CSD_Framework\CSD_FW_Server_Request_Wrapper.obj" \
	"$(INTDIR)\CSD_Framework\CSD_POA.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Framework_Loader.obj"

"..\..\lib\TAO_CSD_Framework.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_CSD_Framework.dll.manifest" mt.exe -manifest "..\..\lib\TAO_CSD_Framework.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\CSD_Framework\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_CSD_Frameworksd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -I"..\tao" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CSD_Framework.dep" "CSD_Framework\CSD_FrameworkC.cpp" "CSD_Framework\CSD_FrameworkA.cpp" "CSD_Framework\CSD_ORBInitializer.cpp" "CSD_Framework\CSD_Object_Adapter.cpp" "CSD_Framework\CSD_Object_Adapter_Factory.cpp" "CSD_Framework\CSD_Default_Servant_Dispatcher.cpp" "CSD_Framework\CSD_Strategy_Repository.cpp" "CSD_Framework\CSD_Strategy_Proxy.cpp" "CSD_Framework\CSD_Strategy_Base.cpp" "CSD_Framework\CSD_FW_Server_Request_Wrapper.cpp" "CSD_Framework\CSD_POA.cpp" "CSD_Framework\CSD_Framework_Loader.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_CSD_Frameworksd.lib"
	-@del /f/q "$(OUTDIR)\TAO_CSD_Frameworksd.exp"
	-@del /f/q "$(OUTDIR)\TAO_CSD_Frameworksd.ilk"
	-@del /f/q "..\..\lib\TAO_CSD_Frameworksd.pdb"
	-@del /f/q "CSD_Framework\CSD_FrameworkC.h"
	-@del /f/q "CSD_Framework\CSD_FrameworkS.h"
	-@del /f/q "CSD_Framework\CSD_FrameworkA.h"
	-@del /f/q "CSD_Framework\CSD_FrameworkC.cpp"
	-@del /f/q "CSD_Framework\CSD_FrameworkA.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\CSD_Framework\$(NULL)" mkdir "Static_Debug\CSD_Framework"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"..\..\lib\TAO_CSD_Frameworksd.pdb" /I "..\.." /I ".." /I "..\tao" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_CSD_Frameworksd.lib"
LINK32_OBJS= \
	"$(INTDIR)\CSD_Framework\CSD_FrameworkC.obj" \
	"$(INTDIR)\CSD_Framework\CSD_FrameworkA.obj" \
	"$(INTDIR)\CSD_Framework\CSD_ORBInitializer.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Object_Adapter.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Object_Adapter_Factory.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Default_Servant_Dispatcher.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Strategy_Repository.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Strategy_Proxy.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Strategy_Base.obj" \
	"$(INTDIR)\CSD_Framework\CSD_FW_Server_Request_Wrapper.obj" \
	"$(INTDIR)\CSD_Framework\CSD_POA.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Framework_Loader.obj"

"$(OUTDIR)\TAO_CSD_Frameworksd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_CSD_Frameworksd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_CSD_Frameworksd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\CSD_Framework\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_CSD_Frameworks.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -I"..\tao" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CSD_Framework.dep" "CSD_Framework\CSD_FrameworkC.cpp" "CSD_Framework\CSD_FrameworkA.cpp" "CSD_Framework\CSD_ORBInitializer.cpp" "CSD_Framework\CSD_Object_Adapter.cpp" "CSD_Framework\CSD_Object_Adapter_Factory.cpp" "CSD_Framework\CSD_Default_Servant_Dispatcher.cpp" "CSD_Framework\CSD_Strategy_Repository.cpp" "CSD_Framework\CSD_Strategy_Proxy.cpp" "CSD_Framework\CSD_Strategy_Base.cpp" "CSD_Framework\CSD_FW_Server_Request_Wrapper.cpp" "CSD_Framework\CSD_POA.cpp" "CSD_Framework\CSD_Framework_Loader.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_CSD_Frameworks.lib"
	-@del /f/q "$(OUTDIR)\TAO_CSD_Frameworks.exp"
	-@del /f/q "$(OUTDIR)\TAO_CSD_Frameworks.ilk"
	-@del /f/q "CSD_Framework\CSD_FrameworkC.h"
	-@del /f/q "CSD_Framework\CSD_FrameworkS.h"
	-@del /f/q "CSD_Framework\CSD_FrameworkA.h"
	-@del /f/q "CSD_Framework\CSD_FrameworkC.cpp"
	-@del /f/q "CSD_Framework\CSD_FrameworkA.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\CSD_Framework\$(NULL)" mkdir "Static_Release\CSD_Framework"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /I "..\tao" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_CSD_Frameworks.lib"
LINK32_OBJS= \
	"$(INTDIR)\CSD_Framework\CSD_FrameworkC.obj" \
	"$(INTDIR)\CSD_Framework\CSD_FrameworkA.obj" \
	"$(INTDIR)\CSD_Framework\CSD_ORBInitializer.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Object_Adapter.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Object_Adapter_Factory.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Default_Servant_Dispatcher.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Strategy_Repository.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Strategy_Proxy.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Strategy_Base.obj" \
	"$(INTDIR)\CSD_Framework\CSD_FW_Server_Request_Wrapper.obj" \
	"$(INTDIR)\CSD_Framework\CSD_POA.obj" \
	"$(INTDIR)\CSD_Framework\CSD_Framework_Loader.obj"

"$(OUTDIR)\TAO_CSD_Frameworks.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_CSD_Frameworks.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_CSD_Frameworks.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CSD_Framework.dep")
!INCLUDE "Makefile.CSD_Framework.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="CSD_Framework\CSD_FrameworkC.cpp"

"$(INTDIR)\CSD_Framework\CSD_FrameworkC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_Framework\$(NULL)" mkdir "$(INTDIR)\CSD_Framework\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_Framework\CSD_FrameworkC.obj" $(SOURCE)

SOURCE="CSD_Framework\CSD_FrameworkA.cpp"

"$(INTDIR)\CSD_Framework\CSD_FrameworkA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_Framework\$(NULL)" mkdir "$(INTDIR)\CSD_Framework\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_Framework\CSD_FrameworkA.obj" $(SOURCE)

SOURCE="CSD_Framework\CSD_ORBInitializer.cpp"

"$(INTDIR)\CSD_Framework\CSD_ORBInitializer.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_Framework\$(NULL)" mkdir "$(INTDIR)\CSD_Framework\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_Framework\CSD_ORBInitializer.obj" $(SOURCE)

SOURCE="CSD_Framework\CSD_Object_Adapter.cpp"

"$(INTDIR)\CSD_Framework\CSD_Object_Adapter.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_Framework\$(NULL)" mkdir "$(INTDIR)\CSD_Framework\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_Framework\CSD_Object_Adapter.obj" $(SOURCE)

SOURCE="CSD_Framework\CSD_Object_Adapter_Factory.cpp"

"$(INTDIR)\CSD_Framework\CSD_Object_Adapter_Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_Framework\$(NULL)" mkdir "$(INTDIR)\CSD_Framework\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_Framework\CSD_Object_Adapter_Factory.obj" $(SOURCE)

SOURCE="CSD_Framework\CSD_Default_Servant_Dispatcher.cpp"

"$(INTDIR)\CSD_Framework\CSD_Default_Servant_Dispatcher.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_Framework\$(NULL)" mkdir "$(INTDIR)\CSD_Framework\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_Framework\CSD_Default_Servant_Dispatcher.obj" $(SOURCE)

SOURCE="CSD_Framework\CSD_Strategy_Repository.cpp"

"$(INTDIR)\CSD_Framework\CSD_Strategy_Repository.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_Framework\$(NULL)" mkdir "$(INTDIR)\CSD_Framework\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_Framework\CSD_Strategy_Repository.obj" $(SOURCE)

SOURCE="CSD_Framework\CSD_Strategy_Proxy.cpp"

"$(INTDIR)\CSD_Framework\CSD_Strategy_Proxy.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_Framework\$(NULL)" mkdir "$(INTDIR)\CSD_Framework\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_Framework\CSD_Strategy_Proxy.obj" $(SOURCE)

SOURCE="CSD_Framework\CSD_Strategy_Base.cpp"

"$(INTDIR)\CSD_Framework\CSD_Strategy_Base.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_Framework\$(NULL)" mkdir "$(INTDIR)\CSD_Framework\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_Framework\CSD_Strategy_Base.obj" $(SOURCE)

SOURCE="CSD_Framework\CSD_FW_Server_Request_Wrapper.cpp"

"$(INTDIR)\CSD_Framework\CSD_FW_Server_Request_Wrapper.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_Framework\$(NULL)" mkdir "$(INTDIR)\CSD_Framework\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_Framework\CSD_FW_Server_Request_Wrapper.obj" $(SOURCE)

SOURCE="CSD_Framework\CSD_POA.cpp"

"$(INTDIR)\CSD_Framework\CSD_POA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_Framework\$(NULL)" mkdir "$(INTDIR)\CSD_Framework\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_Framework\CSD_POA.obj" $(SOURCE)

SOURCE="CSD_Framework\CSD_Framework_Loader.cpp"

"$(INTDIR)\CSD_Framework\CSD_Framework_Loader.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_Framework\$(NULL)" mkdir "$(INTDIR)\CSD_Framework\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_Framework\CSD_Framework_Loader.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="CSD_Framework\CSD_Framework.pidl"

InputPath=CSD_Framework\CSD_Framework.pidl

"CSD_Framework\CSD_FrameworkC.h" "CSD_Framework\CSD_FrameworkS.h" "CSD_Framework\CSD_FrameworkA.h" "CSD_Framework\CSD_FrameworkC.cpp" "CSD_Framework\CSD_FrameworkA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CSD_Framework_CSD_Framework_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Gp -Gd -Sorb -GA -Sal -Wb,export_macro=TAO_CSD_FW_Export -Wb,export_include=tao/CSD_Framework/CSD_FW_Export.h -o CSD_Framework "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="CSD_Framework\CSD_Framework.pidl"

InputPath=CSD_Framework\CSD_Framework.pidl

"CSD_Framework\CSD_FrameworkC.h" "CSD_Framework\CSD_FrameworkS.h" "CSD_Framework\CSD_FrameworkA.h" "CSD_Framework\CSD_FrameworkC.cpp" "CSD_Framework\CSD_FrameworkA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CSD_Framework_CSD_Framework_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Gp -Gd -Sorb -GA -Sal -Wb,export_macro=TAO_CSD_FW_Export -Wb,export_include=tao/CSD_Framework/CSD_FW_Export.h -o CSD_Framework "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="CSD_Framework\CSD_Framework.pidl"

InputPath=CSD_Framework\CSD_Framework.pidl

"CSD_Framework\CSD_FrameworkC.h" "CSD_Framework\CSD_FrameworkS.h" "CSD_Framework\CSD_FrameworkA.h" "CSD_Framework\CSD_FrameworkC.cpp" "CSD_Framework\CSD_FrameworkA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CSD_Framework_CSD_Framework_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Gp -Gd -Sorb -GA -Sal -Wb,export_macro=TAO_CSD_FW_Export -Wb,export_include=tao/CSD_Framework/CSD_FW_Export.h -o CSD_Framework "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="CSD_Framework\CSD_Framework.pidl"

InputPath=CSD_Framework\CSD_Framework.pidl

"CSD_Framework\CSD_FrameworkC.h" "CSD_Framework\CSD_FrameworkS.h" "CSD_Framework\CSD_FrameworkA.h" "CSD_Framework\CSD_FrameworkC.cpp" "CSD_Framework\CSD_FrameworkA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CSD_Framework_CSD_Framework_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Gp -Gd -Sorb -GA -Sal -Wb,export_macro=TAO_CSD_FW_Export -Wb,export_include=tao/CSD_Framework/CSD_FW_Export.h -o CSD_Framework "$(InputPath)"
<<

!ENDIF

SOURCE="CSD_Framework\TAO_CSD_Framework.rc"

"$(INTDIR)\CSD_Framework\TAO_CSD_Framework.res" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_Framework\$(NULL)" mkdir "$(INTDIR)\CSD_Framework\"
	$(RSC) /l 0x409 /fo"$(INTDIR)\CSD_Framework\TAO_CSD_Framework.res" /d NDEBUG /d WIN64 /d _CRT_SECURE_NO_WARNINGS /d _CRT_SECURE_NO_DEPRECATE /d _CRT_NONSTDC_NO_DEPRECATE /i "..\.." /i ".." /i "..\tao" $(SOURCE)



!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CSD_Framework.dep")
	@echo Using "Makefile.CSD_Framework.dep"
!ELSE
	@echo Warning: cannot find "Makefile.CSD_Framework.dep"
!ENDIF
!ENDIF

