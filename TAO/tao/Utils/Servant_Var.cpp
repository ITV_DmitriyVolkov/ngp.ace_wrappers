#ifndef TAO_UTILS_SERVANT_VAR_CPP
#define TAO_UTILS_SERVANT_VAR_CPP

#include "tao/Utils/Servant_Var.h"

#if !defined (__ACE_INLINE__)
# include "tao/Utils/Servant_Var.inl"
#endif /* __ACE_INLINE__ */

ACE_RCSID (Utils,
           Servant_Var,
           "$Id: Servant_Var.cpp 14 2007-02-01 15:49:12Z mitza $")

#endif /*TAO_UTILS_SERVANT_VAR_CPP*/
