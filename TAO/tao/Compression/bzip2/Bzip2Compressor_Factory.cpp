#include "tao/Compression/bzip2/Bzip2Compressor_Factory.h"
#include "tao/Compression/bzip2/Bzip2Compressor.h"

ACE_RCSID (BZIP2,
           Bzip2_Compressor_Factory,
           "$Id: Bzip2Compressor_Factory.cpp 979 2008-12-31 20:22:32Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

namespace TAO
{

Bzip2_CompressorFactory::Bzip2_CompressorFactory (void) :
  ::TAO::CompressorFactory (::Compression::COMPRESSORID_BZIP2),
  compressor_ (::Compression::Compressor::_nil ())
{
}

::Compression::Compressor_ptr
Bzip2_CompressorFactory::get_compressor (
    ::Compression::CompressionLevel compression_level)
{
  // @todo, make a array based on compression level
  if (CORBA::is_nil (compressor_.in ()))
    {
      compressor_ = new Bzip2Compressor (compression_level, this);
    }

  return ::Compression::Compressor::_duplicate (compressor_.in ());
}
}

TAO_END_VERSIONED_NAMESPACE_DECL
