// $Id: CSD_TP_Custom_Synch_Request.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/CSD_ThreadPool/CSD_TP_Custom_Synch_Request.h"

ACE_RCSID (CSD_ThreadPool,
           TP_Custom_Synch_Request,
           "$Id: CSD_TP_Custom_Synch_Request.cpp 14 2007-02-01 15:49:12Z mitza $")

#if !defined (__ACE_INLINE__)
# include "tao/CSD_ThreadPool/CSD_TP_Custom_Synch_Request.inl"
#endif /* ! __ACE_INLINE__ */

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO::CSD::TP_Custom_Synch_Request::~TP_Custom_Synch_Request()
{
}


void
TAO::CSD::TP_Custom_Synch_Request::dispatch_i()
{
  this->execute_op();
  this->synch_helper_.dispatched();
}


void
TAO::CSD::TP_Custom_Synch_Request::cancel_i()
{
  this->cancel_op();
  this->synch_helper_.cancelled();
}

TAO_END_VERSIONED_NAMESPACE_DECL
