// $Id: CSD_TP_Corba_Request.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/CSD_ThreadPool/CSD_TP_Corba_Request.h"

ACE_RCSID (CSD_ThreadPool,
           TP_Corba_Request,
           "$Id: CSD_TP_Corba_Request.cpp 14 2007-02-01 15:49:12Z mitza $")

#if !defined (__ACE_INLINE__)
# include "tao/CSD_ThreadPool/CSD_TP_Corba_Request.inl"
#endif /* ! __ACE_INLINE__ */

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO::CSD::TP_Corba_Request::~TP_Corba_Request()
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
