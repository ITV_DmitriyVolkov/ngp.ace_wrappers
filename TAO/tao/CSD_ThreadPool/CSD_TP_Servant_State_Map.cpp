// $Id: CSD_TP_Servant_State_Map.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/CSD_ThreadPool/CSD_TP_Servant_State_Map.h"

ACE_RCSID (CSD_TP,
           Servant_State_Map,
           "$Id: CSD_TP_Servant_State_Map.cpp 14 2007-02-01 15:49:12Z mitza $")

#if !defined (__ACE_INLINE__)
# include "tao/CSD_ThreadPool/CSD_TP_Servant_State_Map.inl"
#endif /* ! __ACE_INLINE__ */
