// $Id: CSD_TP_Custom_Request_Operation.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/CSD_ThreadPool/CSD_TP_Custom_Request_Operation.h"

ACE_RCSID (CSD_ThreadPool,
           TP_Custom_Request_Operation,
           "$Id: CSD_TP_Custom_Request_Operation.cpp 14 2007-02-01 15:49:12Z mitza $")

#if !defined (__ACE_INLINE__)
# include "tao/CSD_ThreadPool/CSD_TP_Custom_Request_Operation.inl"
#endif /* ! __ACE_INLINE__ */

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO::CSD::TP_Custom_Request_Operation::~TP_Custom_Request_Operation()
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
