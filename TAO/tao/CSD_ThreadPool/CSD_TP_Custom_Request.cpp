// $Id: CSD_TP_Custom_Request.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/CSD_ThreadPool/CSD_TP_Custom_Request.h"

ACE_RCSID (CSD_ThreadPool,
           TP_Custom_Request,
           "$Id: CSD_TP_Custom_Request.cpp 14 2007-02-01 15:49:12Z mitza $")

#if !defined (__ACE_INLINE__)
# include "tao/CSD_ThreadPool/CSD_TP_Custom_Request.inl"
#endif /* ! __ACE_INLINE__ */

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO::CSD::TP_Custom_Request::~TP_Custom_Request()
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
