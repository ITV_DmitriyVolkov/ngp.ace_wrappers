// $Id: CSD_TP_Synch_Helper.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/CSD_ThreadPool/CSD_TP_Synch_Helper.h"

ACE_RCSID (CSD_ThreadPool,
           TP_Synch_Helper,
           "$Id: CSD_TP_Synch_Helper.cpp 14 2007-02-01 15:49:12Z mitza $")

#if !defined (__ACE_INLINE__)
# include "tao/CSD_ThreadPool/CSD_TP_Synch_Helper.inl"
#endif /* ! __ACE_INLINE__ */
