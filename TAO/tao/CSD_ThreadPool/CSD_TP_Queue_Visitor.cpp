// $Id: CSD_TP_Queue_Visitor.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/CSD_ThreadPool/CSD_TP_Queue_Visitor.h"

ACE_RCSID (CSD_ThreadPool,
           TP_Queue_Visitor,
           "$Id: CSD_TP_Queue_Visitor.cpp 14 2007-02-01 15:49:12Z mitza $")

#if !defined (__ACE_INLINE__)
# include "tao/CSD_ThreadPool/CSD_TP_Queue_Visitor.inl"
#endif /* ! __ACE_INLINE__ */

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO::CSD::TP_Queue_Visitor::~TP_Queue_Visitor()
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
