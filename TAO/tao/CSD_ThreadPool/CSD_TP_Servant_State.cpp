// $Id: CSD_TP_Servant_State.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/CSD_ThreadPool/CSD_TP_Servant_State.h"

ACE_RCSID (CSD_TP,
           Servant_State,
           "$Id: CSD_TP_Servant_State.cpp 14 2007-02-01 15:49:12Z mitza $")

#if !defined (__ACE_INLINE__)
# include "tao/CSD_ThreadPool/CSD_TP_Servant_State.inl"
#endif /* ! __ACE_INLINE__ */

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO::CSD::TP_Servant_State::~TP_Servant_State()
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
