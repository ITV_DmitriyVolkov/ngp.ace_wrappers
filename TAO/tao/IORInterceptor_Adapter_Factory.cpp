// $Id: IORInterceptor_Adapter_Factory.cpp 14 2007-02-01 15:49:12Z mitza $
#include "tao/IORInterceptor_Adapter_Factory.h"

ACE_RCSID (tao,
           IORInterceptor_Adapter_Factory,
           "$Id: IORInterceptor_Adapter_Factory.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_IORInterceptor_Adapter_Factory::~TAO_IORInterceptor_Adapter_Factory (void)
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
