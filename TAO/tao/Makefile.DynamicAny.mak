# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.DynamicAny.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "DynamicAny\DynamicAnyC.h" "DynamicAny\DynamicAnyS.h" "DynamicAny\DynamicAnyC.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\DynamicAny\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_DynamicAnyd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_DYNAMICANY_BUILD_DLL -f "Makefile.DynamicAny.dep" "DynamicAny\DynamicAny.cpp" "DynamicAny\DynamicAnyC.cpp" "DynamicAny\DynAny_i.cpp" "DynamicAny\DynArray_i.cpp" "DynamicAny\DynCommon.cpp" "DynamicAny\DynEnum_i.cpp" "DynamicAny\DynSequence_i.cpp" "DynamicAny\DynStruct_i.cpp" "DynamicAny\DynUnion_i.cpp" "DynamicAny\DynValue_i.cpp" "DynamicAny\DynValueBox_i.cpp" "DynamicAny\DynValueCommon_i.cpp" "DynamicAny\DynAnyFactory.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_DynamicAnyd.pdb"
	-@del /f/q "..\..\lib\TAO_DynamicAnyd.dll"
	-@del /f/q "$(OUTDIR)\TAO_DynamicAnyd.lib"
	-@del /f/q "$(OUTDIR)\TAO_DynamicAnyd.exp"
	-@del /f/q "$(OUTDIR)\TAO_DynamicAnyd.ilk"
	-@del /f/q "DynamicAny\DynamicAnyC.h"
	-@del /f/q "DynamicAny\DynamicAnyS.h"
	-@del /f/q "DynamicAny\DynamicAnyC.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\DynamicAny\$(NULL)" mkdir "Debug\DynamicAny"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_DYNAMICANY_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_Valuetyped.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\TAO_DynamicAnyd.pdb" /machine:IA64 /out:"..\..\lib\TAO_DynamicAnyd.dll" /implib:"$(OUTDIR)\TAO_DynamicAnyd.lib"
LINK32_OBJS= \
	"$(INTDIR)\DynamicAny\TAO_DynamicAny.res" \
	"$(INTDIR)\DynamicAny\DynamicAny.obj" \
	"$(INTDIR)\DynamicAny\DynamicAnyC.obj" \
	"$(INTDIR)\DynamicAny\DynAny_i.obj" \
	"$(INTDIR)\DynamicAny\DynArray_i.obj" \
	"$(INTDIR)\DynamicAny\DynCommon.obj" \
	"$(INTDIR)\DynamicAny\DynEnum_i.obj" \
	"$(INTDIR)\DynamicAny\DynSequence_i.obj" \
	"$(INTDIR)\DynamicAny\DynStruct_i.obj" \
	"$(INTDIR)\DynamicAny\DynUnion_i.obj" \
	"$(INTDIR)\DynamicAny\DynValue_i.obj" \
	"$(INTDIR)\DynamicAny\DynValueBox_i.obj" \
	"$(INTDIR)\DynamicAny\DynValueCommon_i.obj" \
	"$(INTDIR)\DynamicAny\DynAnyFactory.obj"

"..\..\lib\TAO_DynamicAnyd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_DynamicAnyd.dll.manifest" mt.exe -manifest "..\..\lib\TAO_DynamicAnyd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\DynamicAny\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_DynamicAny.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_DYNAMICANY_BUILD_DLL -f "Makefile.DynamicAny.dep" "DynamicAny\DynamicAny.cpp" "DynamicAny\DynamicAnyC.cpp" "DynamicAny\DynAny_i.cpp" "DynamicAny\DynArray_i.cpp" "DynamicAny\DynCommon.cpp" "DynamicAny\DynEnum_i.cpp" "DynamicAny\DynSequence_i.cpp" "DynamicAny\DynStruct_i.cpp" "DynamicAny\DynUnion_i.cpp" "DynamicAny\DynValue_i.cpp" "DynamicAny\DynValueBox_i.cpp" "DynamicAny\DynValueCommon_i.cpp" "DynamicAny\DynAnyFactory.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\TAO_DynamicAny.dll"
	-@del /f/q "$(OUTDIR)\TAO_DynamicAny.lib"
	-@del /f/q "$(OUTDIR)\TAO_DynamicAny.exp"
	-@del /f/q "$(OUTDIR)\TAO_DynamicAny.ilk"
	-@del /f/q "DynamicAny\DynamicAnyC.h"
	-@del /f/q "DynamicAny\DynamicAnyS.h"
	-@del /f/q "DynamicAny\DynamicAnyC.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\DynamicAny\$(NULL)" mkdir "Release\DynamicAny"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_DYNAMICANY_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_Valuetype.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\TAO_DynamicAny.dll" /implib:"$(OUTDIR)\TAO_DynamicAny.lib"
LINK32_OBJS= \
	"$(INTDIR)\DynamicAny\TAO_DynamicAny.res" \
	"$(INTDIR)\DynamicAny\DynamicAny.obj" \
	"$(INTDIR)\DynamicAny\DynamicAnyC.obj" \
	"$(INTDIR)\DynamicAny\DynAny_i.obj" \
	"$(INTDIR)\DynamicAny\DynArray_i.obj" \
	"$(INTDIR)\DynamicAny\DynCommon.obj" \
	"$(INTDIR)\DynamicAny\DynEnum_i.obj" \
	"$(INTDIR)\DynamicAny\DynSequence_i.obj" \
	"$(INTDIR)\DynamicAny\DynStruct_i.obj" \
	"$(INTDIR)\DynamicAny\DynUnion_i.obj" \
	"$(INTDIR)\DynamicAny\DynValue_i.obj" \
	"$(INTDIR)\DynamicAny\DynValueBox_i.obj" \
	"$(INTDIR)\DynamicAny\DynValueCommon_i.obj" \
	"$(INTDIR)\DynamicAny\DynAnyFactory.obj"

"..\..\lib\TAO_DynamicAny.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_DynamicAny.dll.manifest" mt.exe -manifest "..\..\lib\TAO_DynamicAny.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\DynamicAny\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_DynamicAnysd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.DynamicAny.dep" "DynamicAny\DynamicAny.cpp" "DynamicAny\DynamicAnyC.cpp" "DynamicAny\DynAny_i.cpp" "DynamicAny\DynArray_i.cpp" "DynamicAny\DynCommon.cpp" "DynamicAny\DynEnum_i.cpp" "DynamicAny\DynSequence_i.cpp" "DynamicAny\DynStruct_i.cpp" "DynamicAny\DynUnion_i.cpp" "DynamicAny\DynValue_i.cpp" "DynamicAny\DynValueBox_i.cpp" "DynamicAny\DynValueCommon_i.cpp" "DynamicAny\DynAnyFactory.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_DynamicAnysd.lib"
	-@del /f/q "$(OUTDIR)\TAO_DynamicAnysd.exp"
	-@del /f/q "$(OUTDIR)\TAO_DynamicAnysd.ilk"
	-@del /f/q "..\..\lib\TAO_DynamicAnysd.pdb"
	-@del /f/q "DynamicAny\DynamicAnyC.h"
	-@del /f/q "DynamicAny\DynamicAnyS.h"
	-@del /f/q "DynamicAny\DynamicAnyC.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\DynamicAny\$(NULL)" mkdir "Static_Debug\DynamicAny"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"..\..\lib\TAO_DynamicAnysd.pdb" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_DynamicAnysd.lib"
LINK32_OBJS= \
	"$(INTDIR)\DynamicAny\DynamicAny.obj" \
	"$(INTDIR)\DynamicAny\DynamicAnyC.obj" \
	"$(INTDIR)\DynamicAny\DynAny_i.obj" \
	"$(INTDIR)\DynamicAny\DynArray_i.obj" \
	"$(INTDIR)\DynamicAny\DynCommon.obj" \
	"$(INTDIR)\DynamicAny\DynEnum_i.obj" \
	"$(INTDIR)\DynamicAny\DynSequence_i.obj" \
	"$(INTDIR)\DynamicAny\DynStruct_i.obj" \
	"$(INTDIR)\DynamicAny\DynUnion_i.obj" \
	"$(INTDIR)\DynamicAny\DynValue_i.obj" \
	"$(INTDIR)\DynamicAny\DynValueBox_i.obj" \
	"$(INTDIR)\DynamicAny\DynValueCommon_i.obj" \
	"$(INTDIR)\DynamicAny\DynAnyFactory.obj"

"$(OUTDIR)\TAO_DynamicAnysd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_DynamicAnysd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_DynamicAnysd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\DynamicAny\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_DynamicAnys.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.DynamicAny.dep" "DynamicAny\DynamicAny.cpp" "DynamicAny\DynamicAnyC.cpp" "DynamicAny\DynAny_i.cpp" "DynamicAny\DynArray_i.cpp" "DynamicAny\DynCommon.cpp" "DynamicAny\DynEnum_i.cpp" "DynamicAny\DynSequence_i.cpp" "DynamicAny\DynStruct_i.cpp" "DynamicAny\DynUnion_i.cpp" "DynamicAny\DynValue_i.cpp" "DynamicAny\DynValueBox_i.cpp" "DynamicAny\DynValueCommon_i.cpp" "DynamicAny\DynAnyFactory.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_DynamicAnys.lib"
	-@del /f/q "$(OUTDIR)\TAO_DynamicAnys.exp"
	-@del /f/q "$(OUTDIR)\TAO_DynamicAnys.ilk"
	-@del /f/q "DynamicAny\DynamicAnyC.h"
	-@del /f/q "DynamicAny\DynamicAnyS.h"
	-@del /f/q "DynamicAny\DynamicAnyC.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\DynamicAny\$(NULL)" mkdir "Static_Release\DynamicAny"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_DynamicAnys.lib"
LINK32_OBJS= \
	"$(INTDIR)\DynamicAny\DynamicAny.obj" \
	"$(INTDIR)\DynamicAny\DynamicAnyC.obj" \
	"$(INTDIR)\DynamicAny\DynAny_i.obj" \
	"$(INTDIR)\DynamicAny\DynArray_i.obj" \
	"$(INTDIR)\DynamicAny\DynCommon.obj" \
	"$(INTDIR)\DynamicAny\DynEnum_i.obj" \
	"$(INTDIR)\DynamicAny\DynSequence_i.obj" \
	"$(INTDIR)\DynamicAny\DynStruct_i.obj" \
	"$(INTDIR)\DynamicAny\DynUnion_i.obj" \
	"$(INTDIR)\DynamicAny\DynValue_i.obj" \
	"$(INTDIR)\DynamicAny\DynValueBox_i.obj" \
	"$(INTDIR)\DynamicAny\DynValueCommon_i.obj" \
	"$(INTDIR)\DynamicAny\DynAnyFactory.obj"

"$(OUTDIR)\TAO_DynamicAnys.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_DynamicAnys.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_DynamicAnys.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.DynamicAny.dep")
!INCLUDE "Makefile.DynamicAny.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="DynamicAny\DynamicAny.cpp"

"$(INTDIR)\DynamicAny\DynamicAny.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\DynamicAny\$(NULL)" mkdir "$(INTDIR)\DynamicAny\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DynamicAny\DynamicAny.obj" $(SOURCE)

SOURCE="DynamicAny\DynamicAnyC.cpp"

"$(INTDIR)\DynamicAny\DynamicAnyC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\DynamicAny\$(NULL)" mkdir "$(INTDIR)\DynamicAny\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DynamicAny\DynamicAnyC.obj" $(SOURCE)

SOURCE="DynamicAny\DynAny_i.cpp"

"$(INTDIR)\DynamicAny\DynAny_i.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\DynamicAny\$(NULL)" mkdir "$(INTDIR)\DynamicAny\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DynamicAny\DynAny_i.obj" $(SOURCE)

SOURCE="DynamicAny\DynArray_i.cpp"

"$(INTDIR)\DynamicAny\DynArray_i.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\DynamicAny\$(NULL)" mkdir "$(INTDIR)\DynamicAny\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DynamicAny\DynArray_i.obj" $(SOURCE)

SOURCE="DynamicAny\DynCommon.cpp"

"$(INTDIR)\DynamicAny\DynCommon.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\DynamicAny\$(NULL)" mkdir "$(INTDIR)\DynamicAny\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DynamicAny\DynCommon.obj" $(SOURCE)

SOURCE="DynamicAny\DynEnum_i.cpp"

"$(INTDIR)\DynamicAny\DynEnum_i.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\DynamicAny\$(NULL)" mkdir "$(INTDIR)\DynamicAny\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DynamicAny\DynEnum_i.obj" $(SOURCE)

SOURCE="DynamicAny\DynSequence_i.cpp"

"$(INTDIR)\DynamicAny\DynSequence_i.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\DynamicAny\$(NULL)" mkdir "$(INTDIR)\DynamicAny\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DynamicAny\DynSequence_i.obj" $(SOURCE)

SOURCE="DynamicAny\DynStruct_i.cpp"

"$(INTDIR)\DynamicAny\DynStruct_i.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\DynamicAny\$(NULL)" mkdir "$(INTDIR)\DynamicAny\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DynamicAny\DynStruct_i.obj" $(SOURCE)

SOURCE="DynamicAny\DynUnion_i.cpp"

"$(INTDIR)\DynamicAny\DynUnion_i.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\DynamicAny\$(NULL)" mkdir "$(INTDIR)\DynamicAny\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DynamicAny\DynUnion_i.obj" $(SOURCE)

SOURCE="DynamicAny\DynValue_i.cpp"

"$(INTDIR)\DynamicAny\DynValue_i.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\DynamicAny\$(NULL)" mkdir "$(INTDIR)\DynamicAny\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DynamicAny\DynValue_i.obj" $(SOURCE)

SOURCE="DynamicAny\DynValueBox_i.cpp"

"$(INTDIR)\DynamicAny\DynValueBox_i.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\DynamicAny\$(NULL)" mkdir "$(INTDIR)\DynamicAny\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DynamicAny\DynValueBox_i.obj" $(SOURCE)

SOURCE="DynamicAny\DynValueCommon_i.cpp"

"$(INTDIR)\DynamicAny\DynValueCommon_i.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\DynamicAny\$(NULL)" mkdir "$(INTDIR)\DynamicAny\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DynamicAny\DynValueCommon_i.obj" $(SOURCE)

SOURCE="DynamicAny\DynAnyFactory.cpp"

"$(INTDIR)\DynamicAny\DynAnyFactory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\DynamicAny\$(NULL)" mkdir "$(INTDIR)\DynamicAny\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DynamicAny\DynAnyFactory.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="DynamicAny\DynamicAny.pidl"

InputPath=DynamicAny\DynamicAny.pidl

"DynamicAny\DynamicAnyC.h" "DynamicAny\DynamicAnyS.h" "DynamicAny\DynamicAnyC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-DynamicAny_DynamicAny_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_DynamicAny_Export -Wb,export_include=tao/DynamicAny/dynamicany_export.h -Wb,include_guard=TAO_DYNAMICANY_SAFE_INCLUDE -Wb,safe_include=tao/DynamicAny/DynamicAny.h -o DynamicAny "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="DynamicAny\DynamicAny.pidl"

InputPath=DynamicAny\DynamicAny.pidl

"DynamicAny\DynamicAnyC.h" "DynamicAny\DynamicAnyS.h" "DynamicAny\DynamicAnyC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-DynamicAny_DynamicAny_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_DynamicAny_Export -Wb,export_include=tao/DynamicAny/dynamicany_export.h -Wb,include_guard=TAO_DYNAMICANY_SAFE_INCLUDE -Wb,safe_include=tao/DynamicAny/DynamicAny.h -o DynamicAny "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="DynamicAny\DynamicAny.pidl"

InputPath=DynamicAny\DynamicAny.pidl

"DynamicAny\DynamicAnyC.h" "DynamicAny\DynamicAnyS.h" "DynamicAny\DynamicAnyC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-DynamicAny_DynamicAny_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_DynamicAny_Export -Wb,export_include=tao/DynamicAny/dynamicany_export.h -Wb,include_guard=TAO_DYNAMICANY_SAFE_INCLUDE -Wb,safe_include=tao/DynamicAny/DynamicAny.h -o DynamicAny "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="DynamicAny\DynamicAny.pidl"

InputPath=DynamicAny\DynamicAny.pidl

"DynamicAny\DynamicAnyC.h" "DynamicAny\DynamicAnyS.h" "DynamicAny\DynamicAnyC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-DynamicAny_DynamicAny_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_DynamicAny_Export -Wb,export_include=tao/DynamicAny/dynamicany_export.h -Wb,include_guard=TAO_DYNAMICANY_SAFE_INCLUDE -Wb,safe_include=tao/DynamicAny/DynamicAny.h -o DynamicAny "$(InputPath)"
<<

!ENDIF

SOURCE="DynamicAny\TAO_DynamicAny.rc"

"$(INTDIR)\DynamicAny\TAO_DynamicAny.res" : $(SOURCE)
	@if not exist "$(INTDIR)\DynamicAny\$(NULL)" mkdir "$(INTDIR)\DynamicAny\"
	$(RSC) /l 0x409 /fo"$(INTDIR)\DynamicAny\TAO_DynamicAny.res" /d NDEBUG /d WIN64 /d _CRT_SECURE_NO_WARNINGS /d _CRT_SECURE_NO_DEPRECATE /d _CRT_NONSTDC_NO_DEPRECATE /i "..\.." /i ".." $(SOURCE)



!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.DynamicAny.dep")
	@echo Using "Makefile.DynamicAny.dep"
!ELSE
	@echo Warning: cannot find "Makefile.DynamicAny.dep"
!ENDIF
!ENDIF

