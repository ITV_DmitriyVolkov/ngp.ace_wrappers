// $Id: Thread_Lane_Resources_Manager.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/Thread_Lane_Resources_Manager.h"
#include "tao/ORB_Core.h"
#include "tao/LF_Strategy.h"

#include "ace/Dynamic_Service.h"

ACE_RCSID (tao,
           Thread_Lane_Resources_Manager,
           "$Id: Thread_Lane_Resources_Manager.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Thread_Lane_Resources_Manager::TAO_Thread_Lane_Resources_Manager (TAO_ORB_Core &orb_core)
  : orb_core_ (&orb_core),
    lf_strategy_ (0)
{
  this->lf_strategy_ =
    this->orb_core_->resource_factory ()->create_lf_strategy ();
}

TAO_Thread_Lane_Resources_Manager::~TAO_Thread_Lane_Resources_Manager (void)
{
  delete this->lf_strategy_;
}

TAO_LF_Strategy &
TAO_Thread_Lane_Resources_Manager::lf_strategy (void)
{
  return *this->lf_strategy_;
}

TAO_Thread_Lane_Resources_Manager_Factory::~TAO_Thread_Lane_Resources_Manager_Factory (void)
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
