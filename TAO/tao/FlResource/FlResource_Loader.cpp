//$Id: FlResource_Loader.cpp 14 2007-02-01 15:49:12Z mitza $
#include "tao/FlResource/FlResource_Loader.h"
#include "tao/ORB_Core.h"
#include "tao/FlResource/FlResource_Factory.h"

ACE_RCSID( TAO_FlResource,
           FlResource_Loader,
           "$Id: FlResource_Loader.cpp 14 2007-02-01 15:49:12Z mitza $");

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

namespace TAO
{
  FlResource_Loader::FlResource_Loader (void)
  {
    FlResource_Factory *tmp = 0;

    ACE_NEW (tmp,
             FlResource_Factory ());

    TAO_ORB_Core::set_gui_resource_factory (tmp);
  }

  FlResource_Loader::~FlResource_Loader (void)
  {
  }
}

TAO_END_VERSIONED_NAMESPACE_DECL
