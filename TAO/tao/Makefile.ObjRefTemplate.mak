# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.ObjRefTemplate.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "ObjRefTemplate\ObjectReferenceTemplateC.inl" "ObjRefTemplate\ObjectReferenceTemplateC.h" "ObjRefTemplate\ObjectReferenceTemplateS.h" "ObjRefTemplate\ObjectReferenceTemplateA.h" "ObjRefTemplate\ObjectReferenceTemplateC.cpp" "ObjRefTemplate\ObjectReferenceTemplate_includeC.h" "ObjRefTemplate\ObjectReferenceTemplate_includeS.h" "ObjRefTemplate\ObjectReferenceTemplate_includeA.h" "ObjRefTemplate\ObjectReferenceTemplate_includeC.cpp" "ObjRefTemplate\Default_ORTC.inl" "ObjRefTemplate\Default_ORTC.h" "ObjRefTemplate\Default_ORTS.h" "ObjRefTemplate\Default_ORTA.h" "ObjRefTemplate\Default_ORTC.cpp" "ObjRefTemplate\Default_ORTA.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\ObjRefTemplate\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_ObjRefTemplated.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_ORT_BUILD_DLL -f "Makefile.ObjRefTemplate.dep" "ObjRefTemplate\ObjectReferenceTemplateC.cpp" "ObjRefTemplate\ObjectReferenceTemplate_includeC.cpp" "ObjRefTemplate\Default_ORTC.cpp" "ObjRefTemplate\Default_ORTA.cpp" "ObjRefTemplate\ORT_Adapter_Impl.cpp" "ObjRefTemplate\ObjectReferenceTemplate_i.cpp" "ObjRefTemplate\ORT_Adapter_Factory_Impl.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_ObjRefTemplated.pdb"
	-@del /f/q "..\..\lib\TAO_ObjRefTemplated.dll"
	-@del /f/q "$(OUTDIR)\TAO_ObjRefTemplated.lib"
	-@del /f/q "$(OUTDIR)\TAO_ObjRefTemplated.exp"
	-@del /f/q "$(OUTDIR)\TAO_ObjRefTemplated.ilk"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplateC.inl"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplateC.h"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplateS.h"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplateA.h"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplateC.cpp"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplate_includeC.h"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplate_includeS.h"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplate_includeA.h"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplate_includeC.cpp"
	-@del /f/q "ObjRefTemplate\Default_ORTC.inl"
	-@del /f/q "ObjRefTemplate\Default_ORTC.h"
	-@del /f/q "ObjRefTemplate\Default_ORTS.h"
	-@del /f/q "ObjRefTemplate\Default_ORTA.h"
	-@del /f/q "ObjRefTemplate\Default_ORTC.cpp"
	-@del /f/q "ObjRefTemplate\Default_ORTA.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\ObjRefTemplate\$(NULL)" mkdir "Debug\ObjRefTemplate"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_ORT_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_Valuetyped.lib TAO_PortableServerd.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\TAO_ObjRefTemplated.pdb" /machine:IA64 /out:"..\..\lib\TAO_ObjRefTemplated.dll" /implib:"$(OUTDIR)\TAO_ObjRefTemplated.lib"
LINK32_OBJS= \
	"$(INTDIR)\ObjRefTemplate\TAO_ObjRefTemplate.res" \
	"$(INTDIR)\ObjRefTemplate\ObjectReferenceTemplateC.obj" \
	"$(INTDIR)\ObjRefTemplate\ObjectReferenceTemplate_includeC.obj" \
	"$(INTDIR)\ObjRefTemplate\Default_ORTC.obj" \
	"$(INTDIR)\ObjRefTemplate\Default_ORTA.obj" \
	"$(INTDIR)\ObjRefTemplate\ORT_Adapter_Impl.obj" \
	"$(INTDIR)\ObjRefTemplate\ObjectReferenceTemplate_i.obj" \
	"$(INTDIR)\ObjRefTemplate\ORT_Adapter_Factory_Impl.obj"

"..\..\lib\TAO_ObjRefTemplated.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_ObjRefTemplated.dll.manifest" mt.exe -manifest "..\..\lib\TAO_ObjRefTemplated.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\ObjRefTemplate\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_ObjRefTemplate.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_ORT_BUILD_DLL -f "Makefile.ObjRefTemplate.dep" "ObjRefTemplate\ObjectReferenceTemplateC.cpp" "ObjRefTemplate\ObjectReferenceTemplate_includeC.cpp" "ObjRefTemplate\Default_ORTC.cpp" "ObjRefTemplate\Default_ORTA.cpp" "ObjRefTemplate\ORT_Adapter_Impl.cpp" "ObjRefTemplate\ObjectReferenceTemplate_i.cpp" "ObjRefTemplate\ORT_Adapter_Factory_Impl.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\TAO_ObjRefTemplate.dll"
	-@del /f/q "$(OUTDIR)\TAO_ObjRefTemplate.lib"
	-@del /f/q "$(OUTDIR)\TAO_ObjRefTemplate.exp"
	-@del /f/q "$(OUTDIR)\TAO_ObjRefTemplate.ilk"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplateC.inl"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplateC.h"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplateS.h"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplateA.h"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplateC.cpp"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplate_includeC.h"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplate_includeS.h"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplate_includeA.h"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplate_includeC.cpp"
	-@del /f/q "ObjRefTemplate\Default_ORTC.inl"
	-@del /f/q "ObjRefTemplate\Default_ORTC.h"
	-@del /f/q "ObjRefTemplate\Default_ORTS.h"
	-@del /f/q "ObjRefTemplate\Default_ORTA.h"
	-@del /f/q "ObjRefTemplate\Default_ORTC.cpp"
	-@del /f/q "ObjRefTemplate\Default_ORTA.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\ObjRefTemplate\$(NULL)" mkdir "Release\ObjRefTemplate"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_ORT_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_Valuetype.lib TAO_PortableServer.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\TAO_ObjRefTemplate.dll" /implib:"$(OUTDIR)\TAO_ObjRefTemplate.lib"
LINK32_OBJS= \
	"$(INTDIR)\ObjRefTemplate\TAO_ObjRefTemplate.res" \
	"$(INTDIR)\ObjRefTemplate\ObjectReferenceTemplateC.obj" \
	"$(INTDIR)\ObjRefTemplate\ObjectReferenceTemplate_includeC.obj" \
	"$(INTDIR)\ObjRefTemplate\Default_ORTC.obj" \
	"$(INTDIR)\ObjRefTemplate\Default_ORTA.obj" \
	"$(INTDIR)\ObjRefTemplate\ORT_Adapter_Impl.obj" \
	"$(INTDIR)\ObjRefTemplate\ObjectReferenceTemplate_i.obj" \
	"$(INTDIR)\ObjRefTemplate\ORT_Adapter_Factory_Impl.obj"

"..\..\lib\TAO_ObjRefTemplate.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_ObjRefTemplate.dll.manifest" mt.exe -manifest "..\..\lib\TAO_ObjRefTemplate.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\ObjRefTemplate\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_ObjRefTemplatesd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.ObjRefTemplate.dep" "ObjRefTemplate\ObjectReferenceTemplateC.cpp" "ObjRefTemplate\ObjectReferenceTemplate_includeC.cpp" "ObjRefTemplate\Default_ORTC.cpp" "ObjRefTemplate\Default_ORTA.cpp" "ObjRefTemplate\ORT_Adapter_Impl.cpp" "ObjRefTemplate\ObjectReferenceTemplate_i.cpp" "ObjRefTemplate\ORT_Adapter_Factory_Impl.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_ObjRefTemplatesd.lib"
	-@del /f/q "$(OUTDIR)\TAO_ObjRefTemplatesd.exp"
	-@del /f/q "$(OUTDIR)\TAO_ObjRefTemplatesd.ilk"
	-@del /f/q "..\..\lib\TAO_ObjRefTemplatesd.pdb"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplateC.inl"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplateC.h"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplateS.h"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplateA.h"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplateC.cpp"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplate_includeC.h"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplate_includeS.h"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplate_includeA.h"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplate_includeC.cpp"
	-@del /f/q "ObjRefTemplate\Default_ORTC.inl"
	-@del /f/q "ObjRefTemplate\Default_ORTC.h"
	-@del /f/q "ObjRefTemplate\Default_ORTS.h"
	-@del /f/q "ObjRefTemplate\Default_ORTA.h"
	-@del /f/q "ObjRefTemplate\Default_ORTC.cpp"
	-@del /f/q "ObjRefTemplate\Default_ORTA.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\ObjRefTemplate\$(NULL)" mkdir "Static_Debug\ObjRefTemplate"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"..\..\lib\TAO_ObjRefTemplatesd.pdb" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_ObjRefTemplatesd.lib"
LINK32_OBJS= \
	"$(INTDIR)\ObjRefTemplate\ObjectReferenceTemplateC.obj" \
	"$(INTDIR)\ObjRefTemplate\ObjectReferenceTemplate_includeC.obj" \
	"$(INTDIR)\ObjRefTemplate\Default_ORTC.obj" \
	"$(INTDIR)\ObjRefTemplate\Default_ORTA.obj" \
	"$(INTDIR)\ObjRefTemplate\ORT_Adapter_Impl.obj" \
	"$(INTDIR)\ObjRefTemplate\ObjectReferenceTemplate_i.obj" \
	"$(INTDIR)\ObjRefTemplate\ORT_Adapter_Factory_Impl.obj"

"$(OUTDIR)\TAO_ObjRefTemplatesd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_ObjRefTemplatesd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_ObjRefTemplatesd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\ObjRefTemplate\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_ObjRefTemplates.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.ObjRefTemplate.dep" "ObjRefTemplate\ObjectReferenceTemplateC.cpp" "ObjRefTemplate\ObjectReferenceTemplate_includeC.cpp" "ObjRefTemplate\Default_ORTC.cpp" "ObjRefTemplate\Default_ORTA.cpp" "ObjRefTemplate\ORT_Adapter_Impl.cpp" "ObjRefTemplate\ObjectReferenceTemplate_i.cpp" "ObjRefTemplate\ORT_Adapter_Factory_Impl.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_ObjRefTemplates.lib"
	-@del /f/q "$(OUTDIR)\TAO_ObjRefTemplates.exp"
	-@del /f/q "$(OUTDIR)\TAO_ObjRefTemplates.ilk"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplateC.inl"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplateC.h"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplateS.h"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplateA.h"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplateC.cpp"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplate_includeC.h"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplate_includeS.h"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplate_includeA.h"
	-@del /f/q "ObjRefTemplate\ObjectReferenceTemplate_includeC.cpp"
	-@del /f/q "ObjRefTemplate\Default_ORTC.inl"
	-@del /f/q "ObjRefTemplate\Default_ORTC.h"
	-@del /f/q "ObjRefTemplate\Default_ORTS.h"
	-@del /f/q "ObjRefTemplate\Default_ORTA.h"
	-@del /f/q "ObjRefTemplate\Default_ORTC.cpp"
	-@del /f/q "ObjRefTemplate\Default_ORTA.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\ObjRefTemplate\$(NULL)" mkdir "Static_Release\ObjRefTemplate"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_ObjRefTemplates.lib"
LINK32_OBJS= \
	"$(INTDIR)\ObjRefTemplate\ObjectReferenceTemplateC.obj" \
	"$(INTDIR)\ObjRefTemplate\ObjectReferenceTemplate_includeC.obj" \
	"$(INTDIR)\ObjRefTemplate\Default_ORTC.obj" \
	"$(INTDIR)\ObjRefTemplate\Default_ORTA.obj" \
	"$(INTDIR)\ObjRefTemplate\ORT_Adapter_Impl.obj" \
	"$(INTDIR)\ObjRefTemplate\ObjectReferenceTemplate_i.obj" \
	"$(INTDIR)\ObjRefTemplate\ORT_Adapter_Factory_Impl.obj"

"$(OUTDIR)\TAO_ObjRefTemplates.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_ObjRefTemplates.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_ObjRefTemplates.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.ObjRefTemplate.dep")
!INCLUDE "Makefile.ObjRefTemplate.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="ObjRefTemplate\ObjectReferenceTemplateC.cpp"

"$(INTDIR)\ObjRefTemplate\ObjectReferenceTemplateC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ObjRefTemplate\$(NULL)" mkdir "$(INTDIR)\ObjRefTemplate\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ObjRefTemplate\ObjectReferenceTemplateC.obj" $(SOURCE)

SOURCE="ObjRefTemplate\ObjectReferenceTemplate_includeC.cpp"

"$(INTDIR)\ObjRefTemplate\ObjectReferenceTemplate_includeC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ObjRefTemplate\$(NULL)" mkdir "$(INTDIR)\ObjRefTemplate\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ObjRefTemplate\ObjectReferenceTemplate_includeC.obj" $(SOURCE)

SOURCE="ObjRefTemplate\Default_ORTC.cpp"

"$(INTDIR)\ObjRefTemplate\Default_ORTC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ObjRefTemplate\$(NULL)" mkdir "$(INTDIR)\ObjRefTemplate\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ObjRefTemplate\Default_ORTC.obj" $(SOURCE)

SOURCE="ObjRefTemplate\Default_ORTA.cpp"

"$(INTDIR)\ObjRefTemplate\Default_ORTA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ObjRefTemplate\$(NULL)" mkdir "$(INTDIR)\ObjRefTemplate\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ObjRefTemplate\Default_ORTA.obj" $(SOURCE)

SOURCE="ObjRefTemplate\ORT_Adapter_Impl.cpp"

"$(INTDIR)\ObjRefTemplate\ORT_Adapter_Impl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ObjRefTemplate\$(NULL)" mkdir "$(INTDIR)\ObjRefTemplate\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ObjRefTemplate\ORT_Adapter_Impl.obj" $(SOURCE)

SOURCE="ObjRefTemplate\ObjectReferenceTemplate_i.cpp"

"$(INTDIR)\ObjRefTemplate\ObjectReferenceTemplate_i.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ObjRefTemplate\$(NULL)" mkdir "$(INTDIR)\ObjRefTemplate\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ObjRefTemplate\ObjectReferenceTemplate_i.obj" $(SOURCE)

SOURCE="ObjRefTemplate\ORT_Adapter_Factory_Impl.cpp"

"$(INTDIR)\ObjRefTemplate\ORT_Adapter_Factory_Impl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ObjRefTemplate\$(NULL)" mkdir "$(INTDIR)\ObjRefTemplate\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ObjRefTemplate\ORT_Adapter_Factory_Impl.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="ObjRefTemplate\ObjectReferenceTemplate.pidl"

InputPath=ObjRefTemplate\ObjectReferenceTemplate.pidl

"ObjRefTemplate\ObjectReferenceTemplateC.inl" "ObjRefTemplate\ObjectReferenceTemplateC.h" "ObjRefTemplate\ObjectReferenceTemplateS.h" "ObjRefTemplate\ObjectReferenceTemplateA.h" "ObjRefTemplate\ObjectReferenceTemplateC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-ObjRefTemplate_ObjectReferenceTemplate_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -SS -Sorb -GX -Wb,export_macro=TAO_ORT_Export -Wb,export_include=tao/ObjRefTemplate/ort_export.h -Wb,include_guard=TAO_OBJREF_TEMPLATE_SAFE_INCLUDE -Wb,safe_include=tao/ObjRefTemplate/ObjectReferenceTemplate.h -o ObjRefTemplate "$(InputPath)"
<<

SOURCE="ObjRefTemplate\ObjectReferenceTemplate_include.pidl"

InputPath=ObjRefTemplate\ObjectReferenceTemplate_include.pidl

"ObjRefTemplate\ObjectReferenceTemplate_includeC.h" "ObjRefTemplate\ObjectReferenceTemplate_includeS.h" "ObjRefTemplate\ObjectReferenceTemplate_includeA.h" "ObjRefTemplate\ObjectReferenceTemplate_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-ObjRefTemplate_ObjectReferenceTemplate_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GX -Sci -Wb,export_macro=TAO_ORT_Export -Wb,export_include=tao/ObjRefTemplate/ort_export.h -Wb,unique_include=tao/ObjRefTemplate/ObjectReferenceTemplate.h -o ObjRefTemplate "$(InputPath)"
<<

SOURCE="ObjRefTemplate\Default_ORT.pidl"

InputPath=ObjRefTemplate\Default_ORT.pidl

"ObjRefTemplate\Default_ORTC.inl" "ObjRefTemplate\Default_ORTC.h" "ObjRefTemplate\Default_ORTS.h" "ObjRefTemplate\Default_ORTA.h" "ObjRefTemplate\Default_ORTC.cpp" "ObjRefTemplate\Default_ORTA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-ObjRefTemplate_Default_ORT_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -SS -GA -Sorb -Wb,export_macro=TAO_ORT_Export -Wb,export_include=tao/ObjRefTemplate/ort_export.h -o ObjRefTemplate "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="ObjRefTemplate\ObjectReferenceTemplate.pidl"

InputPath=ObjRefTemplate\ObjectReferenceTemplate.pidl

"ObjRefTemplate\ObjectReferenceTemplateC.inl" "ObjRefTemplate\ObjectReferenceTemplateC.h" "ObjRefTemplate\ObjectReferenceTemplateS.h" "ObjRefTemplate\ObjectReferenceTemplateA.h" "ObjRefTemplate\ObjectReferenceTemplateC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-ObjRefTemplate_ObjectReferenceTemplate_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -SS -Sorb -GX -Wb,export_macro=TAO_ORT_Export -Wb,export_include=tao/ObjRefTemplate/ort_export.h -Wb,include_guard=TAO_OBJREF_TEMPLATE_SAFE_INCLUDE -Wb,safe_include=tao/ObjRefTemplate/ObjectReferenceTemplate.h -o ObjRefTemplate "$(InputPath)"
<<

SOURCE="ObjRefTemplate\ObjectReferenceTemplate_include.pidl"

InputPath=ObjRefTemplate\ObjectReferenceTemplate_include.pidl

"ObjRefTemplate\ObjectReferenceTemplate_includeC.h" "ObjRefTemplate\ObjectReferenceTemplate_includeS.h" "ObjRefTemplate\ObjectReferenceTemplate_includeA.h" "ObjRefTemplate\ObjectReferenceTemplate_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-ObjRefTemplate_ObjectReferenceTemplate_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GX -Sci -Wb,export_macro=TAO_ORT_Export -Wb,export_include=tao/ObjRefTemplate/ort_export.h -Wb,unique_include=tao/ObjRefTemplate/ObjectReferenceTemplate.h -o ObjRefTemplate "$(InputPath)"
<<

SOURCE="ObjRefTemplate\Default_ORT.pidl"

InputPath=ObjRefTemplate\Default_ORT.pidl

"ObjRefTemplate\Default_ORTC.inl" "ObjRefTemplate\Default_ORTC.h" "ObjRefTemplate\Default_ORTS.h" "ObjRefTemplate\Default_ORTA.h" "ObjRefTemplate\Default_ORTC.cpp" "ObjRefTemplate\Default_ORTA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-ObjRefTemplate_Default_ORT_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -SS -GA -Sorb -Wb,export_macro=TAO_ORT_Export -Wb,export_include=tao/ObjRefTemplate/ort_export.h -o ObjRefTemplate "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="ObjRefTemplate\ObjectReferenceTemplate.pidl"

InputPath=ObjRefTemplate\ObjectReferenceTemplate.pidl

"ObjRefTemplate\ObjectReferenceTemplateC.inl" "ObjRefTemplate\ObjectReferenceTemplateC.h" "ObjRefTemplate\ObjectReferenceTemplateS.h" "ObjRefTemplate\ObjectReferenceTemplateA.h" "ObjRefTemplate\ObjectReferenceTemplateC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-ObjRefTemplate_ObjectReferenceTemplate_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -SS -Sorb -GX -Wb,export_macro=TAO_ORT_Export -Wb,export_include=tao/ObjRefTemplate/ort_export.h -Wb,include_guard=TAO_OBJREF_TEMPLATE_SAFE_INCLUDE -Wb,safe_include=tao/ObjRefTemplate/ObjectReferenceTemplate.h -o ObjRefTemplate "$(InputPath)"
<<

SOURCE="ObjRefTemplate\ObjectReferenceTemplate_include.pidl"

InputPath=ObjRefTemplate\ObjectReferenceTemplate_include.pidl

"ObjRefTemplate\ObjectReferenceTemplate_includeC.h" "ObjRefTemplate\ObjectReferenceTemplate_includeS.h" "ObjRefTemplate\ObjectReferenceTemplate_includeA.h" "ObjRefTemplate\ObjectReferenceTemplate_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-ObjRefTemplate_ObjectReferenceTemplate_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GX -Sci -Wb,export_macro=TAO_ORT_Export -Wb,export_include=tao/ObjRefTemplate/ort_export.h -Wb,unique_include=tao/ObjRefTemplate/ObjectReferenceTemplate.h -o ObjRefTemplate "$(InputPath)"
<<

SOURCE="ObjRefTemplate\Default_ORT.pidl"

InputPath=ObjRefTemplate\Default_ORT.pidl

"ObjRefTemplate\Default_ORTC.inl" "ObjRefTemplate\Default_ORTC.h" "ObjRefTemplate\Default_ORTS.h" "ObjRefTemplate\Default_ORTA.h" "ObjRefTemplate\Default_ORTC.cpp" "ObjRefTemplate\Default_ORTA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-ObjRefTemplate_Default_ORT_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -SS -GA -Sorb -Wb,export_macro=TAO_ORT_Export -Wb,export_include=tao/ObjRefTemplate/ort_export.h -o ObjRefTemplate "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="ObjRefTemplate\ObjectReferenceTemplate.pidl"

InputPath=ObjRefTemplate\ObjectReferenceTemplate.pidl

"ObjRefTemplate\ObjectReferenceTemplateC.inl" "ObjRefTemplate\ObjectReferenceTemplateC.h" "ObjRefTemplate\ObjectReferenceTemplateS.h" "ObjRefTemplate\ObjectReferenceTemplateA.h" "ObjRefTemplate\ObjectReferenceTemplateC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-ObjRefTemplate_ObjectReferenceTemplate_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -SS -Sorb -GX -Wb,export_macro=TAO_ORT_Export -Wb,export_include=tao/ObjRefTemplate/ort_export.h -Wb,include_guard=TAO_OBJREF_TEMPLATE_SAFE_INCLUDE -Wb,safe_include=tao/ObjRefTemplate/ObjectReferenceTemplate.h -o ObjRefTemplate "$(InputPath)"
<<

SOURCE="ObjRefTemplate\ObjectReferenceTemplate_include.pidl"

InputPath=ObjRefTemplate\ObjectReferenceTemplate_include.pidl

"ObjRefTemplate\ObjectReferenceTemplate_includeC.h" "ObjRefTemplate\ObjectReferenceTemplate_includeS.h" "ObjRefTemplate\ObjectReferenceTemplate_includeA.h" "ObjRefTemplate\ObjectReferenceTemplate_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-ObjRefTemplate_ObjectReferenceTemplate_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GX -Sci -Wb,export_macro=TAO_ORT_Export -Wb,export_include=tao/ObjRefTemplate/ort_export.h -Wb,unique_include=tao/ObjRefTemplate/ObjectReferenceTemplate.h -o ObjRefTemplate "$(InputPath)"
<<

SOURCE="ObjRefTemplate\Default_ORT.pidl"

InputPath=ObjRefTemplate\Default_ORT.pidl

"ObjRefTemplate\Default_ORTC.inl" "ObjRefTemplate\Default_ORTC.h" "ObjRefTemplate\Default_ORTS.h" "ObjRefTemplate\Default_ORTA.h" "ObjRefTemplate\Default_ORTC.cpp" "ObjRefTemplate\Default_ORTA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-ObjRefTemplate_Default_ORT_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -SS -GA -Sorb -Wb,export_macro=TAO_ORT_Export -Wb,export_include=tao/ObjRefTemplate/ort_export.h -o ObjRefTemplate "$(InputPath)"
<<

!ENDIF

SOURCE="ObjRefTemplate\TAO_ObjRefTemplate.rc"

"$(INTDIR)\ObjRefTemplate\TAO_ObjRefTemplate.res" : $(SOURCE)
	@if not exist "$(INTDIR)\ObjRefTemplate\$(NULL)" mkdir "$(INTDIR)\ObjRefTemplate\"
	$(RSC) /l 0x409 /fo"$(INTDIR)\ObjRefTemplate\TAO_ObjRefTemplate.res" /d NDEBUG /d WIN64 /d _CRT_SECURE_NO_WARNINGS /d _CRT_SECURE_NO_DEPRECATE /d _CRT_NONSTDC_NO_DEPRECATE /i "..\.." /i ".." $(SOURCE)



!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.ObjRefTemplate.dep")
	@echo Using "Makefile.ObjRefTemplate.dep"
!ELSE
	@echo Warning: cannot find "Makefile.ObjRefTemplate.dep"
!ENDIF
!ENDIF

