# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.TAO.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "GIOPC.inl" "GIOPC.h" "GIOPS.h" "GIOPC.cpp" "AnyTypeCode\GIOPA.h" "AnyTypeCode\GIOPA.cpp" "CONV_FRAMEC.h" "CONV_FRAMES.h" "CONV_FRAMEC.cpp" "AnyTypeCode\CONV_FRAMEA.h" "AnyTypeCode\CONV_FRAMEA.cpp" "CurrentC.h" "CurrentS.h" "CurrentC.cpp" "AnyTypeCode\CurrentA.h" "AnyTypeCode\CurrentA.cpp" "IIOPC.h" "IIOPC.cpp" "AnyTypeCode\IIOPA.h" "AnyTypeCode\IIOPA.cpp" "IIOP_EndpointsC.h" "IIOP_EndpointsS.h" "IIOP_EndpointsC.cpp" "AnyTypeCode\IIOP_EndpointsA.h" "AnyTypeCode\IIOP_EndpointsA.cpp" "IOPC.h" "IOPS.h" "IOPC.cpp" "AnyTypeCode\IOPA.h" "AnyTypeCode\IOPA.cpp" "Messaging_PolicyValueC.h" "Messaging_PolicyValueS.h" "Messaging_PolicyValueC.cpp" "AnyTypeCode\Messaging_PolicyValueA.h" "AnyTypeCode\Messaging_PolicyValueA.cpp" "Messaging_SyncScopeC.h" "Messaging_SyncScopeS.h" "Messaging_SyncScopeC.cpp" "AnyTypeCode\Messaging_SyncScopeA.h" "AnyTypeCode\Messaging_SyncScopeA.cpp" "ObjectIdListC.h" "ObjectIdListS.h" "ObjectIdListC.cpp" "AnyTypeCode\ObjectIdListA.h" "AnyTypeCode\ObjectIdListA.cpp" "orb_typesC.h" "orb_typesS.h" "orb_typesC.cpp" "AnyTypeCode\orb_typesA.h" "AnyTypeCode\orb_typesA.cpp" "ParameterModeC.h" "ParameterModeS.h" "ParameterModeC.cpp" "AnyTypeCode\ParameterModeA.h" "AnyTypeCode\ParameterModeA.cpp" "Policy_ForwardC.h" "Policy_ForwardS.h" "Policy_ForwardC.cpp" "AnyTypeCode\Policy_ForwardA.h" "AnyTypeCode\Policy_ForwardA.cpp" "Policy_ManagerC.h" "Policy_ManagerS.h" "Policy_ManagerC.cpp" "AnyTypeCode\Policy_ManagerA.h" "AnyTypeCode\Policy_ManagerA.cpp" "Policy_CurrentC.h" "Policy_CurrentS.h" "Policy_CurrentC.cpp" "AnyTypeCode\Policy_CurrentA.h" "AnyTypeCode\Policy_CurrentA.cpp" "PI_ForwardC.h" "PI_ForwardS.h" "PI_ForwardC.cpp" "AnyTypeCode\PI_ForwardA.h" "AnyTypeCode\PI_ForwardA.cpp" "PortableInterceptorC.h" "PortableInterceptorS.h" "PortableInterceptorC.cpp" "AnyTypeCode\PortableInterceptorA.h" "AnyTypeCode\PortableInterceptorA.cpp" "ServicesC.h" "ServicesS.h" "ServicesC.cpp" "AnyTypeCode\ServicesA.h" "AnyTypeCode\ServicesA.cpp" "TAOC.h" "TAOS.h" "TAOC.cpp" "AnyTypeCode\TAOA.h" "AnyTypeCode\TAOA.cpp" "TimeBaseC.h" "TimeBaseS.h" "TimeBaseC.cpp" "AnyTypeCode\TimeBaseA.h" "AnyTypeCode\TimeBaseA.cpp" "BooleanSeqC.h" "BooleanSeqS.h" "BooleanSeqC.cpp" "AnyTypeCode\BooleanSeqA.h" "AnyTypeCode\BooleanSeqA.cpp" "CharSeqC.h" "CharSeqS.h" "CharSeqC.cpp" "AnyTypeCode\CharSeqA.h" "AnyTypeCode\CharSeqA.cpp" "DoubleSeqC.h" "DoubleSeqS.h" "DoubleSeqC.cpp" "AnyTypeCode\DoubleSeqA.h" "AnyTypeCode\DoubleSeqA.cpp" "FloatSeqC.h" "FloatSeqS.h" "FloatSeqC.cpp" "AnyTypeCode\FloatSeqA.h" "AnyTypeCode\FloatSeqA.cpp" "LongDoubleSeqC.h" "LongDoubleSeqS.h" "LongDoubleSeqC.cpp" "AnyTypeCode\LongDoubleSeqA.h" "AnyTypeCode\LongDoubleSeqA.cpp" "LongLongSeqC.h" "LongLongSeqS.h" "LongLongSeqC.cpp" "AnyTypeCode\LongLongSeqA.h" "AnyTypeCode\LongLongSeqA.cpp" "LongSeqC.h" "LongSeqS.h" "LongSeqC.cpp" "AnyTypeCode\LongSeqA.h" "AnyTypeCode\LongSeqA.cpp" "OctetSeqC.h" "OctetSeqS.h" "OctetSeqC.cpp" "AnyTypeCode\OctetSeqA.h" "AnyTypeCode\OctetSeqA.cpp" "ShortSeqC.h" "ShortSeqS.h" "ShortSeqC.cpp" "AnyTypeCode\ShortSeqA.h" "AnyTypeCode\ShortSeqA.cpp" "StringSeqC.h" "StringSeqS.h" "StringSeqC.cpp" "AnyTypeCode\StringSeqA.h" "AnyTypeCode\StringSeqA.cpp" "ULongLongSeqC.h" "ULongLongSeqS.h" "ULongLongSeqC.cpp" "AnyTypeCode\ULongLongSeqA.h" "AnyTypeCode\ULongLongSeqA.cpp" "ULongSeqC.h" "ULongSeqS.h" "ULongSeqC.cpp" "AnyTypeCode\ULongSeqA.h" "AnyTypeCode\ULongSeqA.cpp" "UShortSeqC.h" "UShortSeqS.h" "UShortSeqC.cpp" "AnyTypeCode\UShortSeqA.h" "AnyTypeCode\UShortSeqA.cpp" "WCharSeqC.h" "WCharSeqS.h" "WCharSeqC.cpp" "AnyTypeCode\WCharSeqA.h" "AnyTypeCode\WCharSeqA.cpp" "WStringSeqC.h" "WStringSeqS.h" "WStringSeqC.cpp" "AnyTypeCode\WStringSeqA.h" "AnyTypeCode\WStringSeqA.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\TAO\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAOd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_HAS_VALUETYPE_OUT_INDIRECTION -DTAO_BUILD_DLL -f "Makefile.TAO.dep" "Dynamic_Adapter.cpp" "Policy_Manager.cpp" "Abstract_Servant_Base.cpp" "Acceptor_Filter.cpp" "Acceptor_Registry.cpp" "Adapter.cpp" "Adapter_Factory.cpp" "Adapter_Registry.cpp" "AnyTypeCode_Adapter.cpp" "Argument.cpp" "Asynch_Queued_Message.cpp" "Asynch_Reply_Dispatcher_Base.cpp" "Base_Transport_Property.cpp" "BiDir_Adapter.cpp" "Bind_Dispatcher_Guard.cpp" "Block_Flushing_Strategy.cpp" "Blocked_Connect_Strategy.cpp" "BooleanSeqC.cpp" "Cache_Entries.cpp" "CDR.cpp" "CharSeqC.cpp" "Cleanup_Func_Registry.cpp" "Client_Strategy_Factory.cpp" "ClientRequestInterceptor_Adapter_Factory.cpp" "ClientRequestInterceptor_Adapter.cpp" "Codeset_Manager.cpp" "Codeset_Manager_Factory_Base.cpp" "Codeset_Translator_Base.cpp" "Collocated_Invocation.cpp" "Collocation_Proxy_Broker.cpp" "Collocation_Resolver.cpp" "Configurable_Refcount.cpp" "Connect_Strategy.cpp" "Connection_Handler.cpp" "Connection_Purging_Strategy.cpp" "Connector_Registry.cpp" "CONV_FRAMEC.cpp" "CORBA_String.cpp" "CORBALOC_Parser.cpp" "CORBANAME_Parser.cpp" "CurrentC.cpp" "debug.cpp" "default_client.cpp" "Default_Collocation_Resolver.cpp" "Default_Endpoint_Selector_Factory.cpp" "default_resource.cpp" "default_server.cpp" "Default_Stub_Factory.cpp" "Default_Thread_Lane_Resources_Manager.cpp" "DLL_Parser.cpp" "DoubleSeqC.cpp" "Endpoint.cpp" "Endpoint_Selector_Factory.cpp" "Environment.cpp" "Exception.cpp" "Exclusive_TMS.cpp" "Fault_Tolerance_Service.cpp" "FILE_Parser.cpp" "FloatSeqC.cpp" "Flushing_Strategy.cpp" "GIOP_Fragmentation_Strategy.cpp" "GIOP_Message_Base.cpp" "GIOP_Message_Generator_Parser.cpp" "GIOP_Message_Generator_Parser_10.cpp" "GIOP_Message_Generator_Parser_11.cpp" "GIOP_Message_Generator_Parser_12.cpp" "GIOP_Message_Generator_Parser_Impl.cpp" "GIOP_Message_Locate_Header.cpp" "GIOP_Message_State.cpp" "GIOP_Message_Version.cpp" "GIOPC.cpp" "HTTP_Client.cpp" "HTTP_Handler.cpp" "HTTP_Parser.cpp" "IFR_Client_Adapter.cpp" "IIOP_Acceptor.cpp" "IIOP_Connection_Handler.cpp" "IIOP_Connector.cpp" "IIOP_Endpoint.cpp" "IIOP_EndpointsC.cpp" "IIOP_Factory.cpp" "IIOP_Profile.cpp" "IIOP_Transport.cpp" "IIOPC.cpp" "Incoming_Message_Queue.cpp" "Incoming_Message_Stack.cpp" "Invocation_Adapter.cpp" "Invocation_Base.cpp" "Invocation_Endpoint_Selectors.cpp" "IOPC.cpp" "IOR_Parser.cpp" "IORInterceptor_Adapter.cpp" "IORInterceptor_Adapter_Factory.cpp" "Leader_Follower.cpp" "Leader_Follower_Flushing_Strategy.cpp" "LF_CH_Event.cpp" "LF_Connect_Strategy.cpp" "LF_Event.cpp" "LF_Event_Binder.cpp" "LF_Event_Loop_Thread_Helper.cpp" "LF_Follower.cpp" "LF_Follower_Auto_Adder.cpp" "LF_Follower_Auto_Ptr.cpp" "LF_Invocation_Event.cpp" "LF_Multi_Event.cpp" "LF_Strategy.cpp" "LF_Strategy_Complete.cpp" "LocalObject.cpp" "LocateRequest_Invocation.cpp" "LocateRequest_Invocation_Adapter.cpp" "LongDoubleSeqC.cpp" "LongLongSeqC.cpp" "LongSeqC.cpp" "LRU_Connection_Purging_Strategy.cpp" "MCAST_Parser.cpp" "Messaging_PolicyValueC.cpp" "Messaging_SyncScopeC.cpp" "MMAP_Allocator.cpp" "MProfile.cpp" "Muxed_TMS.cpp" "New_Leader_Generator.cpp" "NVList_Adapter.cpp" "Null_Fragmentation_Strategy.cpp" "Object.cpp" "Object_KeyC.cpp" "Object_Loader.cpp" "Object_Proxy_Broker.cpp" "Object_Ref_Table.cpp" "ObjectIdListC.cpp" "ObjectKey_Table.cpp" "OctetSeqC.cpp" "On_Demand_Fragmentation_Strategy.cpp" "operation_details.cpp" "ORB.cpp" "ORBInitializer_Registry.cpp" "ORBInitializer_Registry_Adapter.cpp" "orb_typesC.cpp" "ORB_Core.cpp" "ORB_Core_Auto_Ptr.cpp" "ORB_Core_TSS_Resources.cpp" "ORB_Table.cpp" "ParameterModeC.cpp" "params.cpp" "Parser_Registry.cpp" "PI_ForwardC.cpp" "Pluggable_Messaging_Utils.cpp" "Policy_Current.cpp" "Policy_CurrentC.cpp" "Policy_Current_Impl.cpp" "Policy_ForwardC.cpp" "Policy_ManagerC.cpp" "Policy_Set.cpp" "Policy_Validator.cpp" "PolicyC.cpp" "PolicyFactory_Registry_Adapter.cpp" "PolicyFactory_Registry_Factory.cpp" "PortableInterceptorC.cpp" "Principal.cpp" "Profile.cpp" "Profile_Transport_Resolver.cpp" "Protocol_Factory.cpp" "Protocols_Hooks.cpp" "Network_Priority_Protocols_Hooks.cpp" "Queued_Data.cpp" "Queued_Message.cpp" "Reactive_Connect_Strategy.cpp" "Reactive_Flushing_Strategy.cpp" "Refcounted_ObjectKey.cpp" "Remote_Invocation.cpp" "Remote_Object_Proxy_Broker.cpp" "Reply_Dispatcher.cpp" "Request_Dispatcher.cpp" "Resource_Factory.cpp" "Resume_Handle.cpp" "Server_Strategy_Factory.cpp" "ServerRequestInterceptor_Adapter.cpp" "ServerRequestInterceptor_Adapter_Factory.cpp" "Service_Callbacks.cpp" "Service_Context.cpp" "Service_Context_Handler.cpp" "Service_Context_Handler_Registry.cpp" "Services_Activate.cpp" "ServicesC.cpp" "ShortSeqC.cpp" "String_Alloc.cpp" "StringSeqC.cpp" "Stub.cpp" "Stub_Factory.cpp" "Synch_Invocation.cpp" "Synch_Queued_Message.cpp" "Synch_Reply_Dispatcher.cpp" "SystemException.cpp" "Tagged_Components.cpp" "Tagged_Profile.cpp" "TAO_Internal.cpp" "TAO_Server_Request.cpp" "TAO_Singleton_Manager.cpp" "TAOC.cpp" "target_specification.cpp" "Thread_Lane_Resources.cpp" "Thread_Lane_Resources_Manager.cpp" "Thread_Per_Connection_Handler.cpp" "TimeBaseC.cpp" "Transport.cpp" "Transport_Acceptor.cpp" "Transport_Cache_Manager.cpp" "Transport_Connector.cpp" "Transport_Descriptor_Interface.cpp" "Transport_Mux_Strategy.cpp" "Transport_Queueing_Strategies.cpp" "Transport_Selection_Guard.cpp" "Transport_Timer.cpp" "TSS_Resources.cpp" "TypeCodeFactory_Adapter.cpp" "Typecode_typesC.cpp" "ULongLongSeqC.cpp" "ULongSeqC.cpp" "UserException.cpp" "UShortSeqC.cpp" "Valuetype_Adapter.cpp" "Valuetype_Adapter_Factory.cpp" "Wait_On_Leader_Follower.cpp" "Wait_On_LF_No_Upcall.cpp" "Wait_On_Reactor.cpp" "Wait_On_Read.cpp" "Wait_Strategy.cpp" "WCharSeqC.cpp" "WrongTransactionC.cpp" "WStringSeqC.cpp" "GUIResource_Factory.cpp" "ZIOP_Adapter.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAOd.pdb"
	-@del /f/q "..\..\lib\TAOd.dll"
	-@del /f/q "$(OUTDIR)\TAOd.lib"
	-@del /f/q "$(OUTDIR)\TAOd.exp"
	-@del /f/q "$(OUTDIR)\TAOd.ilk"
	-@del /f/q "GIOPC.inl"
	-@del /f/q "GIOPC.h"
	-@del /f/q "GIOPS.h"
	-@del /f/q "GIOPC.cpp"
	-@del /f/q "AnyTypeCode\GIOPA.h"
	-@del /f/q "AnyTypeCode\GIOPA.cpp"
	-@del /f/q "CONV_FRAMEC.h"
	-@del /f/q "CONV_FRAMES.h"
	-@del /f/q "CONV_FRAMEC.cpp"
	-@del /f/q "AnyTypeCode\CONV_FRAMEA.h"
	-@del /f/q "AnyTypeCode\CONV_FRAMEA.cpp"
	-@del /f/q "CurrentC.h"
	-@del /f/q "CurrentS.h"
	-@del /f/q "CurrentC.cpp"
	-@del /f/q "AnyTypeCode\CurrentA.h"
	-@del /f/q "AnyTypeCode\CurrentA.cpp"
	-@del /f/q "IIOPC.h"
	-@del /f/q "IIOPC.cpp"
	-@del /f/q "AnyTypeCode\IIOPA.h"
	-@del /f/q "AnyTypeCode\IIOPA.cpp"
	-@del /f/q "IIOP_EndpointsC.h"
	-@del /f/q "IIOP_EndpointsS.h"
	-@del /f/q "IIOP_EndpointsC.cpp"
	-@del /f/q "AnyTypeCode\IIOP_EndpointsA.h"
	-@del /f/q "AnyTypeCode\IIOP_EndpointsA.cpp"
	-@del /f/q "IOPC.h"
	-@del /f/q "IOPS.h"
	-@del /f/q "IOPC.cpp"
	-@del /f/q "AnyTypeCode\IOPA.h"
	-@del /f/q "AnyTypeCode\IOPA.cpp"
	-@del /f/q "Messaging_PolicyValueC.h"
	-@del /f/q "Messaging_PolicyValueS.h"
	-@del /f/q "Messaging_PolicyValueC.cpp"
	-@del /f/q "AnyTypeCode\Messaging_PolicyValueA.h"
	-@del /f/q "AnyTypeCode\Messaging_PolicyValueA.cpp"
	-@del /f/q "Messaging_SyncScopeC.h"
	-@del /f/q "Messaging_SyncScopeS.h"
	-@del /f/q "Messaging_SyncScopeC.cpp"
	-@del /f/q "AnyTypeCode\Messaging_SyncScopeA.h"
	-@del /f/q "AnyTypeCode\Messaging_SyncScopeA.cpp"
	-@del /f/q "ObjectIdListC.h"
	-@del /f/q "ObjectIdListS.h"
	-@del /f/q "ObjectIdListC.cpp"
	-@del /f/q "AnyTypeCode\ObjectIdListA.h"
	-@del /f/q "AnyTypeCode\ObjectIdListA.cpp"
	-@del /f/q "orb_typesC.h"
	-@del /f/q "orb_typesS.h"
	-@del /f/q "orb_typesC.cpp"
	-@del /f/q "AnyTypeCode\orb_typesA.h"
	-@del /f/q "AnyTypeCode\orb_typesA.cpp"
	-@del /f/q "ParameterModeC.h"
	-@del /f/q "ParameterModeS.h"
	-@del /f/q "ParameterModeC.cpp"
	-@del /f/q "AnyTypeCode\ParameterModeA.h"
	-@del /f/q "AnyTypeCode\ParameterModeA.cpp"
	-@del /f/q "Policy_ForwardC.h"
	-@del /f/q "Policy_ForwardS.h"
	-@del /f/q "Policy_ForwardC.cpp"
	-@del /f/q "AnyTypeCode\Policy_ForwardA.h"
	-@del /f/q "AnyTypeCode\Policy_ForwardA.cpp"
	-@del /f/q "Policy_ManagerC.h"
	-@del /f/q "Policy_ManagerS.h"
	-@del /f/q "Policy_ManagerC.cpp"
	-@del /f/q "AnyTypeCode\Policy_ManagerA.h"
	-@del /f/q "AnyTypeCode\Policy_ManagerA.cpp"
	-@del /f/q "Policy_CurrentC.h"
	-@del /f/q "Policy_CurrentS.h"
	-@del /f/q "Policy_CurrentC.cpp"
	-@del /f/q "AnyTypeCode\Policy_CurrentA.h"
	-@del /f/q "AnyTypeCode\Policy_CurrentA.cpp"
	-@del /f/q "PI_ForwardC.h"
	-@del /f/q "PI_ForwardS.h"
	-@del /f/q "PI_ForwardC.cpp"
	-@del /f/q "AnyTypeCode\PI_ForwardA.h"
	-@del /f/q "AnyTypeCode\PI_ForwardA.cpp"
	-@del /f/q "PortableInterceptorC.h"
	-@del /f/q "PortableInterceptorS.h"
	-@del /f/q "PortableInterceptorC.cpp"
	-@del /f/q "AnyTypeCode\PortableInterceptorA.h"
	-@del /f/q "AnyTypeCode\PortableInterceptorA.cpp"
	-@del /f/q "ServicesC.h"
	-@del /f/q "ServicesS.h"
	-@del /f/q "ServicesC.cpp"
	-@del /f/q "AnyTypeCode\ServicesA.h"
	-@del /f/q "AnyTypeCode\ServicesA.cpp"
	-@del /f/q "TAOC.h"
	-@del /f/q "TAOS.h"
	-@del /f/q "TAOC.cpp"
	-@del /f/q "AnyTypeCode\TAOA.h"
	-@del /f/q "AnyTypeCode\TAOA.cpp"
	-@del /f/q "TimeBaseC.h"
	-@del /f/q "TimeBaseS.h"
	-@del /f/q "TimeBaseC.cpp"
	-@del /f/q "AnyTypeCode\TimeBaseA.h"
	-@del /f/q "AnyTypeCode\TimeBaseA.cpp"
	-@del /f/q "BooleanSeqC.h"
	-@del /f/q "BooleanSeqS.h"
	-@del /f/q "BooleanSeqC.cpp"
	-@del /f/q "AnyTypeCode\BooleanSeqA.h"
	-@del /f/q "AnyTypeCode\BooleanSeqA.cpp"
	-@del /f/q "CharSeqC.h"
	-@del /f/q "CharSeqS.h"
	-@del /f/q "CharSeqC.cpp"
	-@del /f/q "AnyTypeCode\CharSeqA.h"
	-@del /f/q "AnyTypeCode\CharSeqA.cpp"
	-@del /f/q "DoubleSeqC.h"
	-@del /f/q "DoubleSeqS.h"
	-@del /f/q "DoubleSeqC.cpp"
	-@del /f/q "AnyTypeCode\DoubleSeqA.h"
	-@del /f/q "AnyTypeCode\DoubleSeqA.cpp"
	-@del /f/q "FloatSeqC.h"
	-@del /f/q "FloatSeqS.h"
	-@del /f/q "FloatSeqC.cpp"
	-@del /f/q "AnyTypeCode\FloatSeqA.h"
	-@del /f/q "AnyTypeCode\FloatSeqA.cpp"
	-@del /f/q "LongDoubleSeqC.h"
	-@del /f/q "LongDoubleSeqS.h"
	-@del /f/q "LongDoubleSeqC.cpp"
	-@del /f/q "AnyTypeCode\LongDoubleSeqA.h"
	-@del /f/q "AnyTypeCode\LongDoubleSeqA.cpp"
	-@del /f/q "LongLongSeqC.h"
	-@del /f/q "LongLongSeqS.h"
	-@del /f/q "LongLongSeqC.cpp"
	-@del /f/q "AnyTypeCode\LongLongSeqA.h"
	-@del /f/q "AnyTypeCode\LongLongSeqA.cpp"
	-@del /f/q "LongSeqC.h"
	-@del /f/q "LongSeqS.h"
	-@del /f/q "LongSeqC.cpp"
	-@del /f/q "AnyTypeCode\LongSeqA.h"
	-@del /f/q "AnyTypeCode\LongSeqA.cpp"
	-@del /f/q "OctetSeqC.h"
	-@del /f/q "OctetSeqS.h"
	-@del /f/q "OctetSeqC.cpp"
	-@del /f/q "AnyTypeCode\OctetSeqA.h"
	-@del /f/q "AnyTypeCode\OctetSeqA.cpp"
	-@del /f/q "ShortSeqC.h"
	-@del /f/q "ShortSeqS.h"
	-@del /f/q "ShortSeqC.cpp"
	-@del /f/q "AnyTypeCode\ShortSeqA.h"
	-@del /f/q "AnyTypeCode\ShortSeqA.cpp"
	-@del /f/q "StringSeqC.h"
	-@del /f/q "StringSeqS.h"
	-@del /f/q "StringSeqC.cpp"
	-@del /f/q "AnyTypeCode\StringSeqA.h"
	-@del /f/q "AnyTypeCode\StringSeqA.cpp"
	-@del /f/q "ULongLongSeqC.h"
	-@del /f/q "ULongLongSeqS.h"
	-@del /f/q "ULongLongSeqC.cpp"
	-@del /f/q "AnyTypeCode\ULongLongSeqA.h"
	-@del /f/q "AnyTypeCode\ULongLongSeqA.cpp"
	-@del /f/q "ULongSeqC.h"
	-@del /f/q "ULongSeqS.h"
	-@del /f/q "ULongSeqC.cpp"
	-@del /f/q "AnyTypeCode\ULongSeqA.h"
	-@del /f/q "AnyTypeCode\ULongSeqA.cpp"
	-@del /f/q "UShortSeqC.h"
	-@del /f/q "UShortSeqS.h"
	-@del /f/q "UShortSeqC.cpp"
	-@del /f/q "AnyTypeCode\UShortSeqA.h"
	-@del /f/q "AnyTypeCode\UShortSeqA.cpp"
	-@del /f/q "WCharSeqC.h"
	-@del /f/q "WCharSeqS.h"
	-@del /f/q "WCharSeqC.cpp"
	-@del /f/q "AnyTypeCode\WCharSeqA.h"
	-@del /f/q "AnyTypeCode\WCharSeqA.cpp"
	-@del /f/q "WStringSeqC.h"
	-@del /f/q "WStringSeqS.h"
	-@del /f/q "WStringSeqC.cpp"
	-@del /f/q "AnyTypeCode\WStringSeqA.h"
	-@del /f/q "AnyTypeCode\WStringSeqA.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\TAO\$(NULL)" mkdir "Debug\TAO"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_HAS_VALUETYPE_OUT_INDIRECTION /D TAO_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\TAOd.pdb" /machine:IA64 /out:"..\..\lib\TAOd.dll" /implib:"$(OUTDIR)\TAOd.lib"
LINK32_OBJS= \
	"$(INTDIR)\tao.res" \
	"$(INTDIR)\Dynamic_Adapter.obj" \
	"$(INTDIR)\Policy_Manager.obj" \
	"$(INTDIR)\Abstract_Servant_Base.obj" \
	"$(INTDIR)\Acceptor_Filter.obj" \
	"$(INTDIR)\Acceptor_Registry.obj" \
	"$(INTDIR)\Adapter.obj" \
	"$(INTDIR)\Adapter_Factory.obj" \
	"$(INTDIR)\Adapter_Registry.obj" \
	"$(INTDIR)\AnyTypeCode_Adapter.obj" \
	"$(INTDIR)\Argument.obj" \
	"$(INTDIR)\Asynch_Queued_Message.obj" \
	"$(INTDIR)\Asynch_Reply_Dispatcher_Base.obj" \
	"$(INTDIR)\Base_Transport_Property.obj" \
	"$(INTDIR)\BiDir_Adapter.obj" \
	"$(INTDIR)\Bind_Dispatcher_Guard.obj" \
	"$(INTDIR)\Block_Flushing_Strategy.obj" \
	"$(INTDIR)\Blocked_Connect_Strategy.obj" \
	"$(INTDIR)\BooleanSeqC.obj" \
	"$(INTDIR)\Cache_Entries.obj" \
	"$(INTDIR)\CDR.obj" \
	"$(INTDIR)\CharSeqC.obj" \
	"$(INTDIR)\Cleanup_Func_Registry.obj" \
	"$(INTDIR)\Client_Strategy_Factory.obj" \
	"$(INTDIR)\ClientRequestInterceptor_Adapter_Factory.obj" \
	"$(INTDIR)\ClientRequestInterceptor_Adapter.obj" \
	"$(INTDIR)\Codeset_Manager.obj" \
	"$(INTDIR)\Codeset_Manager_Factory_Base.obj" \
	"$(INTDIR)\Codeset_Translator_Base.obj" \
	"$(INTDIR)\Collocated_Invocation.obj" \
	"$(INTDIR)\Collocation_Proxy_Broker.obj" \
	"$(INTDIR)\Collocation_Resolver.obj" \
	"$(INTDIR)\Configurable_Refcount.obj" \
	"$(INTDIR)\Connect_Strategy.obj" \
	"$(INTDIR)\Connection_Handler.obj" \
	"$(INTDIR)\Connection_Purging_Strategy.obj" \
	"$(INTDIR)\Connector_Registry.obj" \
	"$(INTDIR)\CONV_FRAMEC.obj" \
	"$(INTDIR)\CORBA_String.obj" \
	"$(INTDIR)\CORBALOC_Parser.obj" \
	"$(INTDIR)\CORBANAME_Parser.obj" \
	"$(INTDIR)\CurrentC.obj" \
	"$(INTDIR)\debug.obj" \
	"$(INTDIR)\default_client.obj" \
	"$(INTDIR)\Default_Collocation_Resolver.obj" \
	"$(INTDIR)\Default_Endpoint_Selector_Factory.obj" \
	"$(INTDIR)\default_resource.obj" \
	"$(INTDIR)\default_server.obj" \
	"$(INTDIR)\Default_Stub_Factory.obj" \
	"$(INTDIR)\Default_Thread_Lane_Resources_Manager.obj" \
	"$(INTDIR)\DLL_Parser.obj" \
	"$(INTDIR)\DoubleSeqC.obj" \
	"$(INTDIR)\Endpoint.obj" \
	"$(INTDIR)\Endpoint_Selector_Factory.obj" \
	"$(INTDIR)\Environment.obj" \
	"$(INTDIR)\Exception.obj" \
	"$(INTDIR)\Exclusive_TMS.obj" \
	"$(INTDIR)\Fault_Tolerance_Service.obj" \
	"$(INTDIR)\FILE_Parser.obj" \
	"$(INTDIR)\FloatSeqC.obj" \
	"$(INTDIR)\Flushing_Strategy.obj" \
	"$(INTDIR)\GIOP_Fragmentation_Strategy.obj" \
	"$(INTDIR)\GIOP_Message_Base.obj" \
	"$(INTDIR)\GIOP_Message_Generator_Parser.obj" \
	"$(INTDIR)\GIOP_Message_Generator_Parser_10.obj" \
	"$(INTDIR)\GIOP_Message_Generator_Parser_11.obj" \
	"$(INTDIR)\GIOP_Message_Generator_Parser_12.obj" \
	"$(INTDIR)\GIOP_Message_Generator_Parser_Impl.obj" \
	"$(INTDIR)\GIOP_Message_Locate_Header.obj" \
	"$(INTDIR)\GIOP_Message_State.obj" \
	"$(INTDIR)\GIOP_Message_Version.obj" \
	"$(INTDIR)\GIOPC.obj" \
	"$(INTDIR)\HTTP_Client.obj" \
	"$(INTDIR)\HTTP_Handler.obj" \
	"$(INTDIR)\HTTP_Parser.obj" \
	"$(INTDIR)\IFR_Client_Adapter.obj" \
	"$(INTDIR)\IIOP_Acceptor.obj" \
	"$(INTDIR)\IIOP_Connection_Handler.obj" \
	"$(INTDIR)\IIOP_Connector.obj" \
	"$(INTDIR)\IIOP_Endpoint.obj" \
	"$(INTDIR)\IIOP_EndpointsC.obj" \
	"$(INTDIR)\IIOP_Factory.obj" \
	"$(INTDIR)\IIOP_Profile.obj" \
	"$(INTDIR)\IIOP_Transport.obj" \
	"$(INTDIR)\IIOPC.obj" \
	"$(INTDIR)\Incoming_Message_Queue.obj" \
	"$(INTDIR)\Incoming_Message_Stack.obj" \
	"$(INTDIR)\Invocation_Adapter.obj" \
	"$(INTDIR)\Invocation_Base.obj" \
	"$(INTDIR)\Invocation_Endpoint_Selectors.obj" \
	"$(INTDIR)\IOPC.obj" \
	"$(INTDIR)\IOR_Parser.obj" \
	"$(INTDIR)\IORInterceptor_Adapter.obj" \
	"$(INTDIR)\IORInterceptor_Adapter_Factory.obj" \
	"$(INTDIR)\Leader_Follower.obj" \
	"$(INTDIR)\Leader_Follower_Flushing_Strategy.obj" \
	"$(INTDIR)\LF_CH_Event.obj" \
	"$(INTDIR)\LF_Connect_Strategy.obj" \
	"$(INTDIR)\LF_Event.obj" \
	"$(INTDIR)\LF_Event_Binder.obj" \
	"$(INTDIR)\LF_Event_Loop_Thread_Helper.obj" \
	"$(INTDIR)\LF_Follower.obj" \
	"$(INTDIR)\LF_Follower_Auto_Adder.obj" \
	"$(INTDIR)\LF_Follower_Auto_Ptr.obj" \
	"$(INTDIR)\LF_Invocation_Event.obj" \
	"$(INTDIR)\LF_Multi_Event.obj" \
	"$(INTDIR)\LF_Strategy.obj" \
	"$(INTDIR)\LF_Strategy_Complete.obj" \
	"$(INTDIR)\LocalObject.obj" \
	"$(INTDIR)\LocateRequest_Invocation.obj" \
	"$(INTDIR)\LocateRequest_Invocation_Adapter.obj" \
	"$(INTDIR)\LongDoubleSeqC.obj" \
	"$(INTDIR)\LongLongSeqC.obj" \
	"$(INTDIR)\LongSeqC.obj" \
	"$(INTDIR)\LRU_Connection_Purging_Strategy.obj" \
	"$(INTDIR)\MCAST_Parser.obj" \
	"$(INTDIR)\Messaging_PolicyValueC.obj" \
	"$(INTDIR)\Messaging_SyncScopeC.obj" \
	"$(INTDIR)\MMAP_Allocator.obj" \
	"$(INTDIR)\MProfile.obj" \
	"$(INTDIR)\Muxed_TMS.obj" \
	"$(INTDIR)\New_Leader_Generator.obj" \
	"$(INTDIR)\NVList_Adapter.obj" \
	"$(INTDIR)\Null_Fragmentation_Strategy.obj" \
	"$(INTDIR)\Object.obj" \
	"$(INTDIR)\Object_KeyC.obj" \
	"$(INTDIR)\Object_Loader.obj" \
	"$(INTDIR)\Object_Proxy_Broker.obj" \
	"$(INTDIR)\Object_Ref_Table.obj" \
	"$(INTDIR)\ObjectIdListC.obj" \
	"$(INTDIR)\ObjectKey_Table.obj" \
	"$(INTDIR)\OctetSeqC.obj" \
	"$(INTDIR)\On_Demand_Fragmentation_Strategy.obj" \
	"$(INTDIR)\operation_details.obj" \
	"$(INTDIR)\ORB.obj" \
	"$(INTDIR)\ORBInitializer_Registry.obj" \
	"$(INTDIR)\ORBInitializer_Registry_Adapter.obj" \
	"$(INTDIR)\orb_typesC.obj" \
	"$(INTDIR)\ORB_Core.obj" \
	"$(INTDIR)\ORB_Core_Auto_Ptr.obj" \
	"$(INTDIR)\ORB_Core_TSS_Resources.obj" \
	"$(INTDIR)\ORB_Table.obj" \
	"$(INTDIR)\ParameterModeC.obj" \
	"$(INTDIR)\params.obj" \
	"$(INTDIR)\Parser_Registry.obj" \
	"$(INTDIR)\PI_ForwardC.obj" \
	"$(INTDIR)\Pluggable_Messaging_Utils.obj" \
	"$(INTDIR)\Policy_Current.obj" \
	"$(INTDIR)\Policy_CurrentC.obj" \
	"$(INTDIR)\Policy_Current_Impl.obj" \
	"$(INTDIR)\Policy_ForwardC.obj" \
	"$(INTDIR)\Policy_ManagerC.obj" \
	"$(INTDIR)\Policy_Set.obj" \
	"$(INTDIR)\Policy_Validator.obj" \
	"$(INTDIR)\PolicyC.obj" \
	"$(INTDIR)\PolicyFactory_Registry_Adapter.obj" \
	"$(INTDIR)\PolicyFactory_Registry_Factory.obj" \
	"$(INTDIR)\PortableInterceptorC.obj" \
	"$(INTDIR)\Principal.obj" \
	"$(INTDIR)\Profile.obj" \
	"$(INTDIR)\Profile_Transport_Resolver.obj" \
	"$(INTDIR)\Protocol_Factory.obj" \
	"$(INTDIR)\Protocols_Hooks.obj" \
	"$(INTDIR)\Network_Priority_Protocols_Hooks.obj" \
	"$(INTDIR)\Queued_Data.obj" \
	"$(INTDIR)\Queued_Message.obj" \
	"$(INTDIR)\Reactive_Connect_Strategy.obj" \
	"$(INTDIR)\Reactive_Flushing_Strategy.obj" \
	"$(INTDIR)\Refcounted_ObjectKey.obj" \
	"$(INTDIR)\Remote_Invocation.obj" \
	"$(INTDIR)\Remote_Object_Proxy_Broker.obj" \
	"$(INTDIR)\Reply_Dispatcher.obj" \
	"$(INTDIR)\Request_Dispatcher.obj" \
	"$(INTDIR)\Resource_Factory.obj" \
	"$(INTDIR)\Resume_Handle.obj" \
	"$(INTDIR)\Server_Strategy_Factory.obj" \
	"$(INTDIR)\ServerRequestInterceptor_Adapter.obj" \
	"$(INTDIR)\ServerRequestInterceptor_Adapter_Factory.obj" \
	"$(INTDIR)\Service_Callbacks.obj" \
	"$(INTDIR)\Service_Context.obj" \
	"$(INTDIR)\Service_Context_Handler.obj" \
	"$(INTDIR)\Service_Context_Handler_Registry.obj" \
	"$(INTDIR)\Services_Activate.obj" \
	"$(INTDIR)\ServicesC.obj" \
	"$(INTDIR)\ShortSeqC.obj" \
	"$(INTDIR)\String_Alloc.obj" \
	"$(INTDIR)\StringSeqC.obj" \
	"$(INTDIR)\Stub.obj" \
	"$(INTDIR)\Stub_Factory.obj" \
	"$(INTDIR)\Synch_Invocation.obj" \
	"$(INTDIR)\Synch_Queued_Message.obj" \
	"$(INTDIR)\Synch_Reply_Dispatcher.obj" \
	"$(INTDIR)\SystemException.obj" \
	"$(INTDIR)\Tagged_Components.obj" \
	"$(INTDIR)\Tagged_Profile.obj" \
	"$(INTDIR)\TAO_Internal.obj" \
	"$(INTDIR)\TAO_Server_Request.obj" \
	"$(INTDIR)\TAO_Singleton_Manager.obj" \
	"$(INTDIR)\TAOC.obj" \
	"$(INTDIR)\target_specification.obj" \
	"$(INTDIR)\Thread_Lane_Resources.obj" \
	"$(INTDIR)\Thread_Lane_Resources_Manager.obj" \
	"$(INTDIR)\Thread_Per_Connection_Handler.obj" \
	"$(INTDIR)\TimeBaseC.obj" \
	"$(INTDIR)\Transport.obj" \
	"$(INTDIR)\Transport_Acceptor.obj" \
	"$(INTDIR)\Transport_Cache_Manager.obj" \
	"$(INTDIR)\Transport_Connector.obj" \
	"$(INTDIR)\Transport_Descriptor_Interface.obj" \
	"$(INTDIR)\Transport_Mux_Strategy.obj" \
	"$(INTDIR)\Transport_Queueing_Strategies.obj" \
	"$(INTDIR)\Transport_Selection_Guard.obj" \
	"$(INTDIR)\Transport_Timer.obj" \
	"$(INTDIR)\TSS_Resources.obj" \
	"$(INTDIR)\TypeCodeFactory_Adapter.obj" \
	"$(INTDIR)\Typecode_typesC.obj" \
	"$(INTDIR)\ULongLongSeqC.obj" \
	"$(INTDIR)\ULongSeqC.obj" \
	"$(INTDIR)\UserException.obj" \
	"$(INTDIR)\UShortSeqC.obj" \
	"$(INTDIR)\Valuetype_Adapter.obj" \
	"$(INTDIR)\Valuetype_Adapter_Factory.obj" \
	"$(INTDIR)\Wait_On_Leader_Follower.obj" \
	"$(INTDIR)\Wait_On_LF_No_Upcall.obj" \
	"$(INTDIR)\Wait_On_Reactor.obj" \
	"$(INTDIR)\Wait_On_Read.obj" \
	"$(INTDIR)\Wait_Strategy.obj" \
	"$(INTDIR)\WCharSeqC.obj" \
	"$(INTDIR)\WrongTransactionC.obj" \
	"$(INTDIR)\WStringSeqC.obj" \
	"$(INTDIR)\GUIResource_Factory.obj" \
	"$(INTDIR)\ZIOP_Adapter.obj"

"..\..\lib\TAOd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAOd.dll.manifest" mt.exe -manifest "..\..\lib\TAOd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\TAO\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_HAS_VALUETYPE_OUT_INDIRECTION -DTAO_BUILD_DLL -f "Makefile.TAO.dep" "Dynamic_Adapter.cpp" "Policy_Manager.cpp" "Abstract_Servant_Base.cpp" "Acceptor_Filter.cpp" "Acceptor_Registry.cpp" "Adapter.cpp" "Adapter_Factory.cpp" "Adapter_Registry.cpp" "AnyTypeCode_Adapter.cpp" "Argument.cpp" "Asynch_Queued_Message.cpp" "Asynch_Reply_Dispatcher_Base.cpp" "Base_Transport_Property.cpp" "BiDir_Adapter.cpp" "Bind_Dispatcher_Guard.cpp" "Block_Flushing_Strategy.cpp" "Blocked_Connect_Strategy.cpp" "BooleanSeqC.cpp" "Cache_Entries.cpp" "CDR.cpp" "CharSeqC.cpp" "Cleanup_Func_Registry.cpp" "Client_Strategy_Factory.cpp" "ClientRequestInterceptor_Adapter_Factory.cpp" "ClientRequestInterceptor_Adapter.cpp" "Codeset_Manager.cpp" "Codeset_Manager_Factory_Base.cpp" "Codeset_Translator_Base.cpp" "Collocated_Invocation.cpp" "Collocation_Proxy_Broker.cpp" "Collocation_Resolver.cpp" "Configurable_Refcount.cpp" "Connect_Strategy.cpp" "Connection_Handler.cpp" "Connection_Purging_Strategy.cpp" "Connector_Registry.cpp" "CONV_FRAMEC.cpp" "CORBA_String.cpp" "CORBALOC_Parser.cpp" "CORBANAME_Parser.cpp" "CurrentC.cpp" "debug.cpp" "default_client.cpp" "Default_Collocation_Resolver.cpp" "Default_Endpoint_Selector_Factory.cpp" "default_resource.cpp" "default_server.cpp" "Default_Stub_Factory.cpp" "Default_Thread_Lane_Resources_Manager.cpp" "DLL_Parser.cpp" "DoubleSeqC.cpp" "Endpoint.cpp" "Endpoint_Selector_Factory.cpp" "Environment.cpp" "Exception.cpp" "Exclusive_TMS.cpp" "Fault_Tolerance_Service.cpp" "FILE_Parser.cpp" "FloatSeqC.cpp" "Flushing_Strategy.cpp" "GIOP_Fragmentation_Strategy.cpp" "GIOP_Message_Base.cpp" "GIOP_Message_Generator_Parser.cpp" "GIOP_Message_Generator_Parser_10.cpp" "GIOP_Message_Generator_Parser_11.cpp" "GIOP_Message_Generator_Parser_12.cpp" "GIOP_Message_Generator_Parser_Impl.cpp" "GIOP_Message_Locate_Header.cpp" "GIOP_Message_State.cpp" "GIOP_Message_Version.cpp" "GIOPC.cpp" "HTTP_Client.cpp" "HTTP_Handler.cpp" "HTTP_Parser.cpp" "IFR_Client_Adapter.cpp" "IIOP_Acceptor.cpp" "IIOP_Connection_Handler.cpp" "IIOP_Connector.cpp" "IIOP_Endpoint.cpp" "IIOP_EndpointsC.cpp" "IIOP_Factory.cpp" "IIOP_Profile.cpp" "IIOP_Transport.cpp" "IIOPC.cpp" "Incoming_Message_Queue.cpp" "Incoming_Message_Stack.cpp" "Invocation_Adapter.cpp" "Invocation_Base.cpp" "Invocation_Endpoint_Selectors.cpp" "IOPC.cpp" "IOR_Parser.cpp" "IORInterceptor_Adapter.cpp" "IORInterceptor_Adapter_Factory.cpp" "Leader_Follower.cpp" "Leader_Follower_Flushing_Strategy.cpp" "LF_CH_Event.cpp" "LF_Connect_Strategy.cpp" "LF_Event.cpp" "LF_Event_Binder.cpp" "LF_Event_Loop_Thread_Helper.cpp" "LF_Follower.cpp" "LF_Follower_Auto_Adder.cpp" "LF_Follower_Auto_Ptr.cpp" "LF_Invocation_Event.cpp" "LF_Multi_Event.cpp" "LF_Strategy.cpp" "LF_Strategy_Complete.cpp" "LocalObject.cpp" "LocateRequest_Invocation.cpp" "LocateRequest_Invocation_Adapter.cpp" "LongDoubleSeqC.cpp" "LongLongSeqC.cpp" "LongSeqC.cpp" "LRU_Connection_Purging_Strategy.cpp" "MCAST_Parser.cpp" "Messaging_PolicyValueC.cpp" "Messaging_SyncScopeC.cpp" "MMAP_Allocator.cpp" "MProfile.cpp" "Muxed_TMS.cpp" "New_Leader_Generator.cpp" "NVList_Adapter.cpp" "Null_Fragmentation_Strategy.cpp" "Object.cpp" "Object_KeyC.cpp" "Object_Loader.cpp" "Object_Proxy_Broker.cpp" "Object_Ref_Table.cpp" "ObjectIdListC.cpp" "ObjectKey_Table.cpp" "OctetSeqC.cpp" "On_Demand_Fragmentation_Strategy.cpp" "operation_details.cpp" "ORB.cpp" "ORBInitializer_Registry.cpp" "ORBInitializer_Registry_Adapter.cpp" "orb_typesC.cpp" "ORB_Core.cpp" "ORB_Core_Auto_Ptr.cpp" "ORB_Core_TSS_Resources.cpp" "ORB_Table.cpp" "ParameterModeC.cpp" "params.cpp" "Parser_Registry.cpp" "PI_ForwardC.cpp" "Pluggable_Messaging_Utils.cpp" "Policy_Current.cpp" "Policy_CurrentC.cpp" "Policy_Current_Impl.cpp" "Policy_ForwardC.cpp" "Policy_ManagerC.cpp" "Policy_Set.cpp" "Policy_Validator.cpp" "PolicyC.cpp" "PolicyFactory_Registry_Adapter.cpp" "PolicyFactory_Registry_Factory.cpp" "PortableInterceptorC.cpp" "Principal.cpp" "Profile.cpp" "Profile_Transport_Resolver.cpp" "Protocol_Factory.cpp" "Protocols_Hooks.cpp" "Network_Priority_Protocols_Hooks.cpp" "Queued_Data.cpp" "Queued_Message.cpp" "Reactive_Connect_Strategy.cpp" "Reactive_Flushing_Strategy.cpp" "Refcounted_ObjectKey.cpp" "Remote_Invocation.cpp" "Remote_Object_Proxy_Broker.cpp" "Reply_Dispatcher.cpp" "Request_Dispatcher.cpp" "Resource_Factory.cpp" "Resume_Handle.cpp" "Server_Strategy_Factory.cpp" "ServerRequestInterceptor_Adapter.cpp" "ServerRequestInterceptor_Adapter_Factory.cpp" "Service_Callbacks.cpp" "Service_Context.cpp" "Service_Context_Handler.cpp" "Service_Context_Handler_Registry.cpp" "Services_Activate.cpp" "ServicesC.cpp" "ShortSeqC.cpp" "String_Alloc.cpp" "StringSeqC.cpp" "Stub.cpp" "Stub_Factory.cpp" "Synch_Invocation.cpp" "Synch_Queued_Message.cpp" "Synch_Reply_Dispatcher.cpp" "SystemException.cpp" "Tagged_Components.cpp" "Tagged_Profile.cpp" "TAO_Internal.cpp" "TAO_Server_Request.cpp" "TAO_Singleton_Manager.cpp" "TAOC.cpp" "target_specification.cpp" "Thread_Lane_Resources.cpp" "Thread_Lane_Resources_Manager.cpp" "Thread_Per_Connection_Handler.cpp" "TimeBaseC.cpp" "Transport.cpp" "Transport_Acceptor.cpp" "Transport_Cache_Manager.cpp" "Transport_Connector.cpp" "Transport_Descriptor_Interface.cpp" "Transport_Mux_Strategy.cpp" "Transport_Queueing_Strategies.cpp" "Transport_Selection_Guard.cpp" "Transport_Timer.cpp" "TSS_Resources.cpp" "TypeCodeFactory_Adapter.cpp" "Typecode_typesC.cpp" "ULongLongSeqC.cpp" "ULongSeqC.cpp" "UserException.cpp" "UShortSeqC.cpp" "Valuetype_Adapter.cpp" "Valuetype_Adapter_Factory.cpp" "Wait_On_Leader_Follower.cpp" "Wait_On_LF_No_Upcall.cpp" "Wait_On_Reactor.cpp" "Wait_On_Read.cpp" "Wait_Strategy.cpp" "WCharSeqC.cpp" "WrongTransactionC.cpp" "WStringSeqC.cpp" "GUIResource_Factory.cpp" "ZIOP_Adapter.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\TAO.dll"
	-@del /f/q "$(OUTDIR)\TAO.lib"
	-@del /f/q "$(OUTDIR)\TAO.exp"
	-@del /f/q "$(OUTDIR)\TAO.ilk"
	-@del /f/q "GIOPC.inl"
	-@del /f/q "GIOPC.h"
	-@del /f/q "GIOPS.h"
	-@del /f/q "GIOPC.cpp"
	-@del /f/q "AnyTypeCode\GIOPA.h"
	-@del /f/q "AnyTypeCode\GIOPA.cpp"
	-@del /f/q "CONV_FRAMEC.h"
	-@del /f/q "CONV_FRAMES.h"
	-@del /f/q "CONV_FRAMEC.cpp"
	-@del /f/q "AnyTypeCode\CONV_FRAMEA.h"
	-@del /f/q "AnyTypeCode\CONV_FRAMEA.cpp"
	-@del /f/q "CurrentC.h"
	-@del /f/q "CurrentS.h"
	-@del /f/q "CurrentC.cpp"
	-@del /f/q "AnyTypeCode\CurrentA.h"
	-@del /f/q "AnyTypeCode\CurrentA.cpp"
	-@del /f/q "IIOPC.h"
	-@del /f/q "IIOPC.cpp"
	-@del /f/q "AnyTypeCode\IIOPA.h"
	-@del /f/q "AnyTypeCode\IIOPA.cpp"
	-@del /f/q "IIOP_EndpointsC.h"
	-@del /f/q "IIOP_EndpointsS.h"
	-@del /f/q "IIOP_EndpointsC.cpp"
	-@del /f/q "AnyTypeCode\IIOP_EndpointsA.h"
	-@del /f/q "AnyTypeCode\IIOP_EndpointsA.cpp"
	-@del /f/q "IOPC.h"
	-@del /f/q "IOPS.h"
	-@del /f/q "IOPC.cpp"
	-@del /f/q "AnyTypeCode\IOPA.h"
	-@del /f/q "AnyTypeCode\IOPA.cpp"
	-@del /f/q "Messaging_PolicyValueC.h"
	-@del /f/q "Messaging_PolicyValueS.h"
	-@del /f/q "Messaging_PolicyValueC.cpp"
	-@del /f/q "AnyTypeCode\Messaging_PolicyValueA.h"
	-@del /f/q "AnyTypeCode\Messaging_PolicyValueA.cpp"
	-@del /f/q "Messaging_SyncScopeC.h"
	-@del /f/q "Messaging_SyncScopeS.h"
	-@del /f/q "Messaging_SyncScopeC.cpp"
	-@del /f/q "AnyTypeCode\Messaging_SyncScopeA.h"
	-@del /f/q "AnyTypeCode\Messaging_SyncScopeA.cpp"
	-@del /f/q "ObjectIdListC.h"
	-@del /f/q "ObjectIdListS.h"
	-@del /f/q "ObjectIdListC.cpp"
	-@del /f/q "AnyTypeCode\ObjectIdListA.h"
	-@del /f/q "AnyTypeCode\ObjectIdListA.cpp"
	-@del /f/q "orb_typesC.h"
	-@del /f/q "orb_typesS.h"
	-@del /f/q "orb_typesC.cpp"
	-@del /f/q "AnyTypeCode\orb_typesA.h"
	-@del /f/q "AnyTypeCode\orb_typesA.cpp"
	-@del /f/q "ParameterModeC.h"
	-@del /f/q "ParameterModeS.h"
	-@del /f/q "ParameterModeC.cpp"
	-@del /f/q "AnyTypeCode\ParameterModeA.h"
	-@del /f/q "AnyTypeCode\ParameterModeA.cpp"
	-@del /f/q "Policy_ForwardC.h"
	-@del /f/q "Policy_ForwardS.h"
	-@del /f/q "Policy_ForwardC.cpp"
	-@del /f/q "AnyTypeCode\Policy_ForwardA.h"
	-@del /f/q "AnyTypeCode\Policy_ForwardA.cpp"
	-@del /f/q "Policy_ManagerC.h"
	-@del /f/q "Policy_ManagerS.h"
	-@del /f/q "Policy_ManagerC.cpp"
	-@del /f/q "AnyTypeCode\Policy_ManagerA.h"
	-@del /f/q "AnyTypeCode\Policy_ManagerA.cpp"
	-@del /f/q "Policy_CurrentC.h"
	-@del /f/q "Policy_CurrentS.h"
	-@del /f/q "Policy_CurrentC.cpp"
	-@del /f/q "AnyTypeCode\Policy_CurrentA.h"
	-@del /f/q "AnyTypeCode\Policy_CurrentA.cpp"
	-@del /f/q "PI_ForwardC.h"
	-@del /f/q "PI_ForwardS.h"
	-@del /f/q "PI_ForwardC.cpp"
	-@del /f/q "AnyTypeCode\PI_ForwardA.h"
	-@del /f/q "AnyTypeCode\PI_ForwardA.cpp"
	-@del /f/q "PortableInterceptorC.h"
	-@del /f/q "PortableInterceptorS.h"
	-@del /f/q "PortableInterceptorC.cpp"
	-@del /f/q "AnyTypeCode\PortableInterceptorA.h"
	-@del /f/q "AnyTypeCode\PortableInterceptorA.cpp"
	-@del /f/q "ServicesC.h"
	-@del /f/q "ServicesS.h"
	-@del /f/q "ServicesC.cpp"
	-@del /f/q "AnyTypeCode\ServicesA.h"
	-@del /f/q "AnyTypeCode\ServicesA.cpp"
	-@del /f/q "TAOC.h"
	-@del /f/q "TAOS.h"
	-@del /f/q "TAOC.cpp"
	-@del /f/q "AnyTypeCode\TAOA.h"
	-@del /f/q "AnyTypeCode\TAOA.cpp"
	-@del /f/q "TimeBaseC.h"
	-@del /f/q "TimeBaseS.h"
	-@del /f/q "TimeBaseC.cpp"
	-@del /f/q "AnyTypeCode\TimeBaseA.h"
	-@del /f/q "AnyTypeCode\TimeBaseA.cpp"
	-@del /f/q "BooleanSeqC.h"
	-@del /f/q "BooleanSeqS.h"
	-@del /f/q "BooleanSeqC.cpp"
	-@del /f/q "AnyTypeCode\BooleanSeqA.h"
	-@del /f/q "AnyTypeCode\BooleanSeqA.cpp"
	-@del /f/q "CharSeqC.h"
	-@del /f/q "CharSeqS.h"
	-@del /f/q "CharSeqC.cpp"
	-@del /f/q "AnyTypeCode\CharSeqA.h"
	-@del /f/q "AnyTypeCode\CharSeqA.cpp"
	-@del /f/q "DoubleSeqC.h"
	-@del /f/q "DoubleSeqS.h"
	-@del /f/q "DoubleSeqC.cpp"
	-@del /f/q "AnyTypeCode\DoubleSeqA.h"
	-@del /f/q "AnyTypeCode\DoubleSeqA.cpp"
	-@del /f/q "FloatSeqC.h"
	-@del /f/q "FloatSeqS.h"
	-@del /f/q "FloatSeqC.cpp"
	-@del /f/q "AnyTypeCode\FloatSeqA.h"
	-@del /f/q "AnyTypeCode\FloatSeqA.cpp"
	-@del /f/q "LongDoubleSeqC.h"
	-@del /f/q "LongDoubleSeqS.h"
	-@del /f/q "LongDoubleSeqC.cpp"
	-@del /f/q "AnyTypeCode\LongDoubleSeqA.h"
	-@del /f/q "AnyTypeCode\LongDoubleSeqA.cpp"
	-@del /f/q "LongLongSeqC.h"
	-@del /f/q "LongLongSeqS.h"
	-@del /f/q "LongLongSeqC.cpp"
	-@del /f/q "AnyTypeCode\LongLongSeqA.h"
	-@del /f/q "AnyTypeCode\LongLongSeqA.cpp"
	-@del /f/q "LongSeqC.h"
	-@del /f/q "LongSeqS.h"
	-@del /f/q "LongSeqC.cpp"
	-@del /f/q "AnyTypeCode\LongSeqA.h"
	-@del /f/q "AnyTypeCode\LongSeqA.cpp"
	-@del /f/q "OctetSeqC.h"
	-@del /f/q "OctetSeqS.h"
	-@del /f/q "OctetSeqC.cpp"
	-@del /f/q "AnyTypeCode\OctetSeqA.h"
	-@del /f/q "AnyTypeCode\OctetSeqA.cpp"
	-@del /f/q "ShortSeqC.h"
	-@del /f/q "ShortSeqS.h"
	-@del /f/q "ShortSeqC.cpp"
	-@del /f/q "AnyTypeCode\ShortSeqA.h"
	-@del /f/q "AnyTypeCode\ShortSeqA.cpp"
	-@del /f/q "StringSeqC.h"
	-@del /f/q "StringSeqS.h"
	-@del /f/q "StringSeqC.cpp"
	-@del /f/q "AnyTypeCode\StringSeqA.h"
	-@del /f/q "AnyTypeCode\StringSeqA.cpp"
	-@del /f/q "ULongLongSeqC.h"
	-@del /f/q "ULongLongSeqS.h"
	-@del /f/q "ULongLongSeqC.cpp"
	-@del /f/q "AnyTypeCode\ULongLongSeqA.h"
	-@del /f/q "AnyTypeCode\ULongLongSeqA.cpp"
	-@del /f/q "ULongSeqC.h"
	-@del /f/q "ULongSeqS.h"
	-@del /f/q "ULongSeqC.cpp"
	-@del /f/q "AnyTypeCode\ULongSeqA.h"
	-@del /f/q "AnyTypeCode\ULongSeqA.cpp"
	-@del /f/q "UShortSeqC.h"
	-@del /f/q "UShortSeqS.h"
	-@del /f/q "UShortSeqC.cpp"
	-@del /f/q "AnyTypeCode\UShortSeqA.h"
	-@del /f/q "AnyTypeCode\UShortSeqA.cpp"
	-@del /f/q "WCharSeqC.h"
	-@del /f/q "WCharSeqS.h"
	-@del /f/q "WCharSeqC.cpp"
	-@del /f/q "AnyTypeCode\WCharSeqA.h"
	-@del /f/q "AnyTypeCode\WCharSeqA.cpp"
	-@del /f/q "WStringSeqC.h"
	-@del /f/q "WStringSeqS.h"
	-@del /f/q "WStringSeqC.cpp"
	-@del /f/q "AnyTypeCode\WStringSeqA.h"
	-@del /f/q "AnyTypeCode\WStringSeqA.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\TAO\$(NULL)" mkdir "Release\TAO"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_HAS_VALUETYPE_OUT_INDIRECTION /D TAO_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\TAO.dll" /implib:"$(OUTDIR)\TAO.lib"
LINK32_OBJS= \
	"$(INTDIR)\tao.res" \
	"$(INTDIR)\Dynamic_Adapter.obj" \
	"$(INTDIR)\Policy_Manager.obj" \
	"$(INTDIR)\Abstract_Servant_Base.obj" \
	"$(INTDIR)\Acceptor_Filter.obj" \
	"$(INTDIR)\Acceptor_Registry.obj" \
	"$(INTDIR)\Adapter.obj" \
	"$(INTDIR)\Adapter_Factory.obj" \
	"$(INTDIR)\Adapter_Registry.obj" \
	"$(INTDIR)\AnyTypeCode_Adapter.obj" \
	"$(INTDIR)\Argument.obj" \
	"$(INTDIR)\Asynch_Queued_Message.obj" \
	"$(INTDIR)\Asynch_Reply_Dispatcher_Base.obj" \
	"$(INTDIR)\Base_Transport_Property.obj" \
	"$(INTDIR)\BiDir_Adapter.obj" \
	"$(INTDIR)\Bind_Dispatcher_Guard.obj" \
	"$(INTDIR)\Block_Flushing_Strategy.obj" \
	"$(INTDIR)\Blocked_Connect_Strategy.obj" \
	"$(INTDIR)\BooleanSeqC.obj" \
	"$(INTDIR)\Cache_Entries.obj" \
	"$(INTDIR)\CDR.obj" \
	"$(INTDIR)\CharSeqC.obj" \
	"$(INTDIR)\Cleanup_Func_Registry.obj" \
	"$(INTDIR)\Client_Strategy_Factory.obj" \
	"$(INTDIR)\ClientRequestInterceptor_Adapter_Factory.obj" \
	"$(INTDIR)\ClientRequestInterceptor_Adapter.obj" \
	"$(INTDIR)\Codeset_Manager.obj" \
	"$(INTDIR)\Codeset_Manager_Factory_Base.obj" \
	"$(INTDIR)\Codeset_Translator_Base.obj" \
	"$(INTDIR)\Collocated_Invocation.obj" \
	"$(INTDIR)\Collocation_Proxy_Broker.obj" \
	"$(INTDIR)\Collocation_Resolver.obj" \
	"$(INTDIR)\Configurable_Refcount.obj" \
	"$(INTDIR)\Connect_Strategy.obj" \
	"$(INTDIR)\Connection_Handler.obj" \
	"$(INTDIR)\Connection_Purging_Strategy.obj" \
	"$(INTDIR)\Connector_Registry.obj" \
	"$(INTDIR)\CONV_FRAMEC.obj" \
	"$(INTDIR)\CORBA_String.obj" \
	"$(INTDIR)\CORBALOC_Parser.obj" \
	"$(INTDIR)\CORBANAME_Parser.obj" \
	"$(INTDIR)\CurrentC.obj" \
	"$(INTDIR)\debug.obj" \
	"$(INTDIR)\default_client.obj" \
	"$(INTDIR)\Default_Collocation_Resolver.obj" \
	"$(INTDIR)\Default_Endpoint_Selector_Factory.obj" \
	"$(INTDIR)\default_resource.obj" \
	"$(INTDIR)\default_server.obj" \
	"$(INTDIR)\Default_Stub_Factory.obj" \
	"$(INTDIR)\Default_Thread_Lane_Resources_Manager.obj" \
	"$(INTDIR)\DLL_Parser.obj" \
	"$(INTDIR)\DoubleSeqC.obj" \
	"$(INTDIR)\Endpoint.obj" \
	"$(INTDIR)\Endpoint_Selector_Factory.obj" \
	"$(INTDIR)\Environment.obj" \
	"$(INTDIR)\Exception.obj" \
	"$(INTDIR)\Exclusive_TMS.obj" \
	"$(INTDIR)\Fault_Tolerance_Service.obj" \
	"$(INTDIR)\FILE_Parser.obj" \
	"$(INTDIR)\FloatSeqC.obj" \
	"$(INTDIR)\Flushing_Strategy.obj" \
	"$(INTDIR)\GIOP_Fragmentation_Strategy.obj" \
	"$(INTDIR)\GIOP_Message_Base.obj" \
	"$(INTDIR)\GIOP_Message_Generator_Parser.obj" \
	"$(INTDIR)\GIOP_Message_Generator_Parser_10.obj" \
	"$(INTDIR)\GIOP_Message_Generator_Parser_11.obj" \
	"$(INTDIR)\GIOP_Message_Generator_Parser_12.obj" \
	"$(INTDIR)\GIOP_Message_Generator_Parser_Impl.obj" \
	"$(INTDIR)\GIOP_Message_Locate_Header.obj" \
	"$(INTDIR)\GIOP_Message_State.obj" \
	"$(INTDIR)\GIOP_Message_Version.obj" \
	"$(INTDIR)\GIOPC.obj" \
	"$(INTDIR)\HTTP_Client.obj" \
	"$(INTDIR)\HTTP_Handler.obj" \
	"$(INTDIR)\HTTP_Parser.obj" \
	"$(INTDIR)\IFR_Client_Adapter.obj" \
	"$(INTDIR)\IIOP_Acceptor.obj" \
	"$(INTDIR)\IIOP_Connection_Handler.obj" \
	"$(INTDIR)\IIOP_Connector.obj" \
	"$(INTDIR)\IIOP_Endpoint.obj" \
	"$(INTDIR)\IIOP_EndpointsC.obj" \
	"$(INTDIR)\IIOP_Factory.obj" \
	"$(INTDIR)\IIOP_Profile.obj" \
	"$(INTDIR)\IIOP_Transport.obj" \
	"$(INTDIR)\IIOPC.obj" \
	"$(INTDIR)\Incoming_Message_Queue.obj" \
	"$(INTDIR)\Incoming_Message_Stack.obj" \
	"$(INTDIR)\Invocation_Adapter.obj" \
	"$(INTDIR)\Invocation_Base.obj" \
	"$(INTDIR)\Invocation_Endpoint_Selectors.obj" \
	"$(INTDIR)\IOPC.obj" \
	"$(INTDIR)\IOR_Parser.obj" \
	"$(INTDIR)\IORInterceptor_Adapter.obj" \
	"$(INTDIR)\IORInterceptor_Adapter_Factory.obj" \
	"$(INTDIR)\Leader_Follower.obj" \
	"$(INTDIR)\Leader_Follower_Flushing_Strategy.obj" \
	"$(INTDIR)\LF_CH_Event.obj" \
	"$(INTDIR)\LF_Connect_Strategy.obj" \
	"$(INTDIR)\LF_Event.obj" \
	"$(INTDIR)\LF_Event_Binder.obj" \
	"$(INTDIR)\LF_Event_Loop_Thread_Helper.obj" \
	"$(INTDIR)\LF_Follower.obj" \
	"$(INTDIR)\LF_Follower_Auto_Adder.obj" \
	"$(INTDIR)\LF_Follower_Auto_Ptr.obj" \
	"$(INTDIR)\LF_Invocation_Event.obj" \
	"$(INTDIR)\LF_Multi_Event.obj" \
	"$(INTDIR)\LF_Strategy.obj" \
	"$(INTDIR)\LF_Strategy_Complete.obj" \
	"$(INTDIR)\LocalObject.obj" \
	"$(INTDIR)\LocateRequest_Invocation.obj" \
	"$(INTDIR)\LocateRequest_Invocation_Adapter.obj" \
	"$(INTDIR)\LongDoubleSeqC.obj" \
	"$(INTDIR)\LongLongSeqC.obj" \
	"$(INTDIR)\LongSeqC.obj" \
	"$(INTDIR)\LRU_Connection_Purging_Strategy.obj" \
	"$(INTDIR)\MCAST_Parser.obj" \
	"$(INTDIR)\Messaging_PolicyValueC.obj" \
	"$(INTDIR)\Messaging_SyncScopeC.obj" \
	"$(INTDIR)\MMAP_Allocator.obj" \
	"$(INTDIR)\MProfile.obj" \
	"$(INTDIR)\Muxed_TMS.obj" \
	"$(INTDIR)\New_Leader_Generator.obj" \
	"$(INTDIR)\NVList_Adapter.obj" \
	"$(INTDIR)\Null_Fragmentation_Strategy.obj" \
	"$(INTDIR)\Object.obj" \
	"$(INTDIR)\Object_KeyC.obj" \
	"$(INTDIR)\Object_Loader.obj" \
	"$(INTDIR)\Object_Proxy_Broker.obj" \
	"$(INTDIR)\Object_Ref_Table.obj" \
	"$(INTDIR)\ObjectIdListC.obj" \
	"$(INTDIR)\ObjectKey_Table.obj" \
	"$(INTDIR)\OctetSeqC.obj" \
	"$(INTDIR)\On_Demand_Fragmentation_Strategy.obj" \
	"$(INTDIR)\operation_details.obj" \
	"$(INTDIR)\ORB.obj" \
	"$(INTDIR)\ORBInitializer_Registry.obj" \
	"$(INTDIR)\ORBInitializer_Registry_Adapter.obj" \
	"$(INTDIR)\orb_typesC.obj" \
	"$(INTDIR)\ORB_Core.obj" \
	"$(INTDIR)\ORB_Core_Auto_Ptr.obj" \
	"$(INTDIR)\ORB_Core_TSS_Resources.obj" \
	"$(INTDIR)\ORB_Table.obj" \
	"$(INTDIR)\ParameterModeC.obj" \
	"$(INTDIR)\params.obj" \
	"$(INTDIR)\Parser_Registry.obj" \
	"$(INTDIR)\PI_ForwardC.obj" \
	"$(INTDIR)\Pluggable_Messaging_Utils.obj" \
	"$(INTDIR)\Policy_Current.obj" \
	"$(INTDIR)\Policy_CurrentC.obj" \
	"$(INTDIR)\Policy_Current_Impl.obj" \
	"$(INTDIR)\Policy_ForwardC.obj" \
	"$(INTDIR)\Policy_ManagerC.obj" \
	"$(INTDIR)\Policy_Set.obj" \
	"$(INTDIR)\Policy_Validator.obj" \
	"$(INTDIR)\PolicyC.obj" \
	"$(INTDIR)\PolicyFactory_Registry_Adapter.obj" \
	"$(INTDIR)\PolicyFactory_Registry_Factory.obj" \
	"$(INTDIR)\PortableInterceptorC.obj" \
	"$(INTDIR)\Principal.obj" \
	"$(INTDIR)\Profile.obj" \
	"$(INTDIR)\Profile_Transport_Resolver.obj" \
	"$(INTDIR)\Protocol_Factory.obj" \
	"$(INTDIR)\Protocols_Hooks.obj" \
	"$(INTDIR)\Network_Priority_Protocols_Hooks.obj" \
	"$(INTDIR)\Queued_Data.obj" \
	"$(INTDIR)\Queued_Message.obj" \
	"$(INTDIR)\Reactive_Connect_Strategy.obj" \
	"$(INTDIR)\Reactive_Flushing_Strategy.obj" \
	"$(INTDIR)\Refcounted_ObjectKey.obj" \
	"$(INTDIR)\Remote_Invocation.obj" \
	"$(INTDIR)\Remote_Object_Proxy_Broker.obj" \
	"$(INTDIR)\Reply_Dispatcher.obj" \
	"$(INTDIR)\Request_Dispatcher.obj" \
	"$(INTDIR)\Resource_Factory.obj" \
	"$(INTDIR)\Resume_Handle.obj" \
	"$(INTDIR)\Server_Strategy_Factory.obj" \
	"$(INTDIR)\ServerRequestInterceptor_Adapter.obj" \
	"$(INTDIR)\ServerRequestInterceptor_Adapter_Factory.obj" \
	"$(INTDIR)\Service_Callbacks.obj" \
	"$(INTDIR)\Service_Context.obj" \
	"$(INTDIR)\Service_Context_Handler.obj" \
	"$(INTDIR)\Service_Context_Handler_Registry.obj" \
	"$(INTDIR)\Services_Activate.obj" \
	"$(INTDIR)\ServicesC.obj" \
	"$(INTDIR)\ShortSeqC.obj" \
	"$(INTDIR)\String_Alloc.obj" \
	"$(INTDIR)\StringSeqC.obj" \
	"$(INTDIR)\Stub.obj" \
	"$(INTDIR)\Stub_Factory.obj" \
	"$(INTDIR)\Synch_Invocation.obj" \
	"$(INTDIR)\Synch_Queued_Message.obj" \
	"$(INTDIR)\Synch_Reply_Dispatcher.obj" \
	"$(INTDIR)\SystemException.obj" \
	"$(INTDIR)\Tagged_Components.obj" \
	"$(INTDIR)\Tagged_Profile.obj" \
	"$(INTDIR)\TAO_Internal.obj" \
	"$(INTDIR)\TAO_Server_Request.obj" \
	"$(INTDIR)\TAO_Singleton_Manager.obj" \
	"$(INTDIR)\TAOC.obj" \
	"$(INTDIR)\target_specification.obj" \
	"$(INTDIR)\Thread_Lane_Resources.obj" \
	"$(INTDIR)\Thread_Lane_Resources_Manager.obj" \
	"$(INTDIR)\Thread_Per_Connection_Handler.obj" \
	"$(INTDIR)\TimeBaseC.obj" \
	"$(INTDIR)\Transport.obj" \
	"$(INTDIR)\Transport_Acceptor.obj" \
	"$(INTDIR)\Transport_Cache_Manager.obj" \
	"$(INTDIR)\Transport_Connector.obj" \
	"$(INTDIR)\Transport_Descriptor_Interface.obj" \
	"$(INTDIR)\Transport_Mux_Strategy.obj" \
	"$(INTDIR)\Transport_Queueing_Strategies.obj" \
	"$(INTDIR)\Transport_Selection_Guard.obj" \
	"$(INTDIR)\Transport_Timer.obj" \
	"$(INTDIR)\TSS_Resources.obj" \
	"$(INTDIR)\TypeCodeFactory_Adapter.obj" \
	"$(INTDIR)\Typecode_typesC.obj" \
	"$(INTDIR)\ULongLongSeqC.obj" \
	"$(INTDIR)\ULongSeqC.obj" \
	"$(INTDIR)\UserException.obj" \
	"$(INTDIR)\UShortSeqC.obj" \
	"$(INTDIR)\Valuetype_Adapter.obj" \
	"$(INTDIR)\Valuetype_Adapter_Factory.obj" \
	"$(INTDIR)\Wait_On_Leader_Follower.obj" \
	"$(INTDIR)\Wait_On_LF_No_Upcall.obj" \
	"$(INTDIR)\Wait_On_Reactor.obj" \
	"$(INTDIR)\Wait_On_Read.obj" \
	"$(INTDIR)\Wait_Strategy.obj" \
	"$(INTDIR)\WCharSeqC.obj" \
	"$(INTDIR)\WrongTransactionC.obj" \
	"$(INTDIR)\WStringSeqC.obj" \
	"$(INTDIR)\GUIResource_Factory.obj" \
	"$(INTDIR)\ZIOP_Adapter.obj"

"..\..\lib\TAO.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO.dll.manifest" mt.exe -manifest "..\..\lib\TAO.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\TAO\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAOsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_HAS_VALUETYPE_OUT_INDIRECTION -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.TAO.dep" "Dynamic_Adapter.cpp" "Policy_Manager.cpp" "Abstract_Servant_Base.cpp" "Acceptor_Filter.cpp" "Acceptor_Registry.cpp" "Adapter.cpp" "Adapter_Factory.cpp" "Adapter_Registry.cpp" "AnyTypeCode_Adapter.cpp" "Argument.cpp" "Asynch_Queued_Message.cpp" "Asynch_Reply_Dispatcher_Base.cpp" "Base_Transport_Property.cpp" "BiDir_Adapter.cpp" "Bind_Dispatcher_Guard.cpp" "Block_Flushing_Strategy.cpp" "Blocked_Connect_Strategy.cpp" "BooleanSeqC.cpp" "Cache_Entries.cpp" "CDR.cpp" "CharSeqC.cpp" "Cleanup_Func_Registry.cpp" "Client_Strategy_Factory.cpp" "ClientRequestInterceptor_Adapter_Factory.cpp" "ClientRequestInterceptor_Adapter.cpp" "Codeset_Manager.cpp" "Codeset_Manager_Factory_Base.cpp" "Codeset_Translator_Base.cpp" "Collocated_Invocation.cpp" "Collocation_Proxy_Broker.cpp" "Collocation_Resolver.cpp" "Configurable_Refcount.cpp" "Connect_Strategy.cpp" "Connection_Handler.cpp" "Connection_Purging_Strategy.cpp" "Connector_Registry.cpp" "CONV_FRAMEC.cpp" "CORBA_String.cpp" "CORBALOC_Parser.cpp" "CORBANAME_Parser.cpp" "CurrentC.cpp" "debug.cpp" "default_client.cpp" "Default_Collocation_Resolver.cpp" "Default_Endpoint_Selector_Factory.cpp" "default_resource.cpp" "default_server.cpp" "Default_Stub_Factory.cpp" "Default_Thread_Lane_Resources_Manager.cpp" "DLL_Parser.cpp" "DoubleSeqC.cpp" "Endpoint.cpp" "Endpoint_Selector_Factory.cpp" "Environment.cpp" "Exception.cpp" "Exclusive_TMS.cpp" "Fault_Tolerance_Service.cpp" "FILE_Parser.cpp" "FloatSeqC.cpp" "Flushing_Strategy.cpp" "GIOP_Fragmentation_Strategy.cpp" "GIOP_Message_Base.cpp" "GIOP_Message_Generator_Parser.cpp" "GIOP_Message_Generator_Parser_10.cpp" "GIOP_Message_Generator_Parser_11.cpp" "GIOP_Message_Generator_Parser_12.cpp" "GIOP_Message_Generator_Parser_Impl.cpp" "GIOP_Message_Locate_Header.cpp" "GIOP_Message_State.cpp" "GIOP_Message_Version.cpp" "GIOPC.cpp" "HTTP_Client.cpp" "HTTP_Handler.cpp" "HTTP_Parser.cpp" "IFR_Client_Adapter.cpp" "IIOP_Acceptor.cpp" "IIOP_Connection_Handler.cpp" "IIOP_Connector.cpp" "IIOP_Endpoint.cpp" "IIOP_EndpointsC.cpp" "IIOP_Factory.cpp" "IIOP_Profile.cpp" "IIOP_Transport.cpp" "IIOPC.cpp" "Incoming_Message_Queue.cpp" "Incoming_Message_Stack.cpp" "Invocation_Adapter.cpp" "Invocation_Base.cpp" "Invocation_Endpoint_Selectors.cpp" "IOPC.cpp" "IOR_Parser.cpp" "IORInterceptor_Adapter.cpp" "IORInterceptor_Adapter_Factory.cpp" "Leader_Follower.cpp" "Leader_Follower_Flushing_Strategy.cpp" "LF_CH_Event.cpp" "LF_Connect_Strategy.cpp" "LF_Event.cpp" "LF_Event_Binder.cpp" "LF_Event_Loop_Thread_Helper.cpp" "LF_Follower.cpp" "LF_Follower_Auto_Adder.cpp" "LF_Follower_Auto_Ptr.cpp" "LF_Invocation_Event.cpp" "LF_Multi_Event.cpp" "LF_Strategy.cpp" "LF_Strategy_Complete.cpp" "LocalObject.cpp" "LocateRequest_Invocation.cpp" "LocateRequest_Invocation_Adapter.cpp" "LongDoubleSeqC.cpp" "LongLongSeqC.cpp" "LongSeqC.cpp" "LRU_Connection_Purging_Strategy.cpp" "MCAST_Parser.cpp" "Messaging_PolicyValueC.cpp" "Messaging_SyncScopeC.cpp" "MMAP_Allocator.cpp" "MProfile.cpp" "Muxed_TMS.cpp" "New_Leader_Generator.cpp" "NVList_Adapter.cpp" "Null_Fragmentation_Strategy.cpp" "Object.cpp" "Object_KeyC.cpp" "Object_Loader.cpp" "Object_Proxy_Broker.cpp" "Object_Ref_Table.cpp" "ObjectIdListC.cpp" "ObjectKey_Table.cpp" "OctetSeqC.cpp" "On_Demand_Fragmentation_Strategy.cpp" "operation_details.cpp" "ORB.cpp" "ORBInitializer_Registry.cpp" "ORBInitializer_Registry_Adapter.cpp" "orb_typesC.cpp" "ORB_Core.cpp" "ORB_Core_Auto_Ptr.cpp" "ORB_Core_TSS_Resources.cpp" "ORB_Table.cpp" "ParameterModeC.cpp" "params.cpp" "Parser_Registry.cpp" "PI_ForwardC.cpp" "Pluggable_Messaging_Utils.cpp" "Policy_Current.cpp" "Policy_CurrentC.cpp" "Policy_Current_Impl.cpp" "Policy_ForwardC.cpp" "Policy_ManagerC.cpp" "Policy_Set.cpp" "Policy_Validator.cpp" "PolicyC.cpp" "PolicyFactory_Registry_Adapter.cpp" "PolicyFactory_Registry_Factory.cpp" "PortableInterceptorC.cpp" "Principal.cpp" "Profile.cpp" "Profile_Transport_Resolver.cpp" "Protocol_Factory.cpp" "Protocols_Hooks.cpp" "Network_Priority_Protocols_Hooks.cpp" "Queued_Data.cpp" "Queued_Message.cpp" "Reactive_Connect_Strategy.cpp" "Reactive_Flushing_Strategy.cpp" "Refcounted_ObjectKey.cpp" "Remote_Invocation.cpp" "Remote_Object_Proxy_Broker.cpp" "Reply_Dispatcher.cpp" "Request_Dispatcher.cpp" "Resource_Factory.cpp" "Resume_Handle.cpp" "Server_Strategy_Factory.cpp" "ServerRequestInterceptor_Adapter.cpp" "ServerRequestInterceptor_Adapter_Factory.cpp" "Service_Callbacks.cpp" "Service_Context.cpp" "Service_Context_Handler.cpp" "Service_Context_Handler_Registry.cpp" "Services_Activate.cpp" "ServicesC.cpp" "ShortSeqC.cpp" "String_Alloc.cpp" "StringSeqC.cpp" "Stub.cpp" "Stub_Factory.cpp" "Synch_Invocation.cpp" "Synch_Queued_Message.cpp" "Synch_Reply_Dispatcher.cpp" "SystemException.cpp" "Tagged_Components.cpp" "Tagged_Profile.cpp" "TAO_Internal.cpp" "TAO_Server_Request.cpp" "TAO_Singleton_Manager.cpp" "TAOC.cpp" "target_specification.cpp" "Thread_Lane_Resources.cpp" "Thread_Lane_Resources_Manager.cpp" "Thread_Per_Connection_Handler.cpp" "TimeBaseC.cpp" "Transport.cpp" "Transport_Acceptor.cpp" "Transport_Cache_Manager.cpp" "Transport_Connector.cpp" "Transport_Descriptor_Interface.cpp" "Transport_Mux_Strategy.cpp" "Transport_Queueing_Strategies.cpp" "Transport_Selection_Guard.cpp" "Transport_Timer.cpp" "TSS_Resources.cpp" "TypeCodeFactory_Adapter.cpp" "Typecode_typesC.cpp" "ULongLongSeqC.cpp" "ULongSeqC.cpp" "UserException.cpp" "UShortSeqC.cpp" "Valuetype_Adapter.cpp" "Valuetype_Adapter_Factory.cpp" "Wait_On_Leader_Follower.cpp" "Wait_On_LF_No_Upcall.cpp" "Wait_On_Reactor.cpp" "Wait_On_Read.cpp" "Wait_Strategy.cpp" "WCharSeqC.cpp" "WrongTransactionC.cpp" "WStringSeqC.cpp" "GUIResource_Factory.cpp" "ZIOP_Adapter.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAOsd.lib"
	-@del /f/q "$(OUTDIR)\TAOsd.exp"
	-@del /f/q "$(OUTDIR)\TAOsd.ilk"
	-@del /f/q "..\..\lib\TAOsd.pdb"
	-@del /f/q "GIOPC.inl"
	-@del /f/q "GIOPC.h"
	-@del /f/q "GIOPS.h"
	-@del /f/q "GIOPC.cpp"
	-@del /f/q "AnyTypeCode\GIOPA.h"
	-@del /f/q "AnyTypeCode\GIOPA.cpp"
	-@del /f/q "CONV_FRAMEC.h"
	-@del /f/q "CONV_FRAMES.h"
	-@del /f/q "CONV_FRAMEC.cpp"
	-@del /f/q "AnyTypeCode\CONV_FRAMEA.h"
	-@del /f/q "AnyTypeCode\CONV_FRAMEA.cpp"
	-@del /f/q "CurrentC.h"
	-@del /f/q "CurrentS.h"
	-@del /f/q "CurrentC.cpp"
	-@del /f/q "AnyTypeCode\CurrentA.h"
	-@del /f/q "AnyTypeCode\CurrentA.cpp"
	-@del /f/q "IIOPC.h"
	-@del /f/q "IIOPC.cpp"
	-@del /f/q "AnyTypeCode\IIOPA.h"
	-@del /f/q "AnyTypeCode\IIOPA.cpp"
	-@del /f/q "IIOP_EndpointsC.h"
	-@del /f/q "IIOP_EndpointsS.h"
	-@del /f/q "IIOP_EndpointsC.cpp"
	-@del /f/q "AnyTypeCode\IIOP_EndpointsA.h"
	-@del /f/q "AnyTypeCode\IIOP_EndpointsA.cpp"
	-@del /f/q "IOPC.h"
	-@del /f/q "IOPS.h"
	-@del /f/q "IOPC.cpp"
	-@del /f/q "AnyTypeCode\IOPA.h"
	-@del /f/q "AnyTypeCode\IOPA.cpp"
	-@del /f/q "Messaging_PolicyValueC.h"
	-@del /f/q "Messaging_PolicyValueS.h"
	-@del /f/q "Messaging_PolicyValueC.cpp"
	-@del /f/q "AnyTypeCode\Messaging_PolicyValueA.h"
	-@del /f/q "AnyTypeCode\Messaging_PolicyValueA.cpp"
	-@del /f/q "Messaging_SyncScopeC.h"
	-@del /f/q "Messaging_SyncScopeS.h"
	-@del /f/q "Messaging_SyncScopeC.cpp"
	-@del /f/q "AnyTypeCode\Messaging_SyncScopeA.h"
	-@del /f/q "AnyTypeCode\Messaging_SyncScopeA.cpp"
	-@del /f/q "ObjectIdListC.h"
	-@del /f/q "ObjectIdListS.h"
	-@del /f/q "ObjectIdListC.cpp"
	-@del /f/q "AnyTypeCode\ObjectIdListA.h"
	-@del /f/q "AnyTypeCode\ObjectIdListA.cpp"
	-@del /f/q "orb_typesC.h"
	-@del /f/q "orb_typesS.h"
	-@del /f/q "orb_typesC.cpp"
	-@del /f/q "AnyTypeCode\orb_typesA.h"
	-@del /f/q "AnyTypeCode\orb_typesA.cpp"
	-@del /f/q "ParameterModeC.h"
	-@del /f/q "ParameterModeS.h"
	-@del /f/q "ParameterModeC.cpp"
	-@del /f/q "AnyTypeCode\ParameterModeA.h"
	-@del /f/q "AnyTypeCode\ParameterModeA.cpp"
	-@del /f/q "Policy_ForwardC.h"
	-@del /f/q "Policy_ForwardS.h"
	-@del /f/q "Policy_ForwardC.cpp"
	-@del /f/q "AnyTypeCode\Policy_ForwardA.h"
	-@del /f/q "AnyTypeCode\Policy_ForwardA.cpp"
	-@del /f/q "Policy_ManagerC.h"
	-@del /f/q "Policy_ManagerS.h"
	-@del /f/q "Policy_ManagerC.cpp"
	-@del /f/q "AnyTypeCode\Policy_ManagerA.h"
	-@del /f/q "AnyTypeCode\Policy_ManagerA.cpp"
	-@del /f/q "Policy_CurrentC.h"
	-@del /f/q "Policy_CurrentS.h"
	-@del /f/q "Policy_CurrentC.cpp"
	-@del /f/q "AnyTypeCode\Policy_CurrentA.h"
	-@del /f/q "AnyTypeCode\Policy_CurrentA.cpp"
	-@del /f/q "PI_ForwardC.h"
	-@del /f/q "PI_ForwardS.h"
	-@del /f/q "PI_ForwardC.cpp"
	-@del /f/q "AnyTypeCode\PI_ForwardA.h"
	-@del /f/q "AnyTypeCode\PI_ForwardA.cpp"
	-@del /f/q "PortableInterceptorC.h"
	-@del /f/q "PortableInterceptorS.h"
	-@del /f/q "PortableInterceptorC.cpp"
	-@del /f/q "AnyTypeCode\PortableInterceptorA.h"
	-@del /f/q "AnyTypeCode\PortableInterceptorA.cpp"
	-@del /f/q "ServicesC.h"
	-@del /f/q "ServicesS.h"
	-@del /f/q "ServicesC.cpp"
	-@del /f/q "AnyTypeCode\ServicesA.h"
	-@del /f/q "AnyTypeCode\ServicesA.cpp"
	-@del /f/q "TAOC.h"
	-@del /f/q "TAOS.h"
	-@del /f/q "TAOC.cpp"
	-@del /f/q "AnyTypeCode\TAOA.h"
	-@del /f/q "AnyTypeCode\TAOA.cpp"
	-@del /f/q "TimeBaseC.h"
	-@del /f/q "TimeBaseS.h"
	-@del /f/q "TimeBaseC.cpp"
	-@del /f/q "AnyTypeCode\TimeBaseA.h"
	-@del /f/q "AnyTypeCode\TimeBaseA.cpp"
	-@del /f/q "BooleanSeqC.h"
	-@del /f/q "BooleanSeqS.h"
	-@del /f/q "BooleanSeqC.cpp"
	-@del /f/q "AnyTypeCode\BooleanSeqA.h"
	-@del /f/q "AnyTypeCode\BooleanSeqA.cpp"
	-@del /f/q "CharSeqC.h"
	-@del /f/q "CharSeqS.h"
	-@del /f/q "CharSeqC.cpp"
	-@del /f/q "AnyTypeCode\CharSeqA.h"
	-@del /f/q "AnyTypeCode\CharSeqA.cpp"
	-@del /f/q "DoubleSeqC.h"
	-@del /f/q "DoubleSeqS.h"
	-@del /f/q "DoubleSeqC.cpp"
	-@del /f/q "AnyTypeCode\DoubleSeqA.h"
	-@del /f/q "AnyTypeCode\DoubleSeqA.cpp"
	-@del /f/q "FloatSeqC.h"
	-@del /f/q "FloatSeqS.h"
	-@del /f/q "FloatSeqC.cpp"
	-@del /f/q "AnyTypeCode\FloatSeqA.h"
	-@del /f/q "AnyTypeCode\FloatSeqA.cpp"
	-@del /f/q "LongDoubleSeqC.h"
	-@del /f/q "LongDoubleSeqS.h"
	-@del /f/q "LongDoubleSeqC.cpp"
	-@del /f/q "AnyTypeCode\LongDoubleSeqA.h"
	-@del /f/q "AnyTypeCode\LongDoubleSeqA.cpp"
	-@del /f/q "LongLongSeqC.h"
	-@del /f/q "LongLongSeqS.h"
	-@del /f/q "LongLongSeqC.cpp"
	-@del /f/q "AnyTypeCode\LongLongSeqA.h"
	-@del /f/q "AnyTypeCode\LongLongSeqA.cpp"
	-@del /f/q "LongSeqC.h"
	-@del /f/q "LongSeqS.h"
	-@del /f/q "LongSeqC.cpp"
	-@del /f/q "AnyTypeCode\LongSeqA.h"
	-@del /f/q "AnyTypeCode\LongSeqA.cpp"
	-@del /f/q "OctetSeqC.h"
	-@del /f/q "OctetSeqS.h"
	-@del /f/q "OctetSeqC.cpp"
	-@del /f/q "AnyTypeCode\OctetSeqA.h"
	-@del /f/q "AnyTypeCode\OctetSeqA.cpp"
	-@del /f/q "ShortSeqC.h"
	-@del /f/q "ShortSeqS.h"
	-@del /f/q "ShortSeqC.cpp"
	-@del /f/q "AnyTypeCode\ShortSeqA.h"
	-@del /f/q "AnyTypeCode\ShortSeqA.cpp"
	-@del /f/q "StringSeqC.h"
	-@del /f/q "StringSeqS.h"
	-@del /f/q "StringSeqC.cpp"
	-@del /f/q "AnyTypeCode\StringSeqA.h"
	-@del /f/q "AnyTypeCode\StringSeqA.cpp"
	-@del /f/q "ULongLongSeqC.h"
	-@del /f/q "ULongLongSeqS.h"
	-@del /f/q "ULongLongSeqC.cpp"
	-@del /f/q "AnyTypeCode\ULongLongSeqA.h"
	-@del /f/q "AnyTypeCode\ULongLongSeqA.cpp"
	-@del /f/q "ULongSeqC.h"
	-@del /f/q "ULongSeqS.h"
	-@del /f/q "ULongSeqC.cpp"
	-@del /f/q "AnyTypeCode\ULongSeqA.h"
	-@del /f/q "AnyTypeCode\ULongSeqA.cpp"
	-@del /f/q "UShortSeqC.h"
	-@del /f/q "UShortSeqS.h"
	-@del /f/q "UShortSeqC.cpp"
	-@del /f/q "AnyTypeCode\UShortSeqA.h"
	-@del /f/q "AnyTypeCode\UShortSeqA.cpp"
	-@del /f/q "WCharSeqC.h"
	-@del /f/q "WCharSeqS.h"
	-@del /f/q "WCharSeqC.cpp"
	-@del /f/q "AnyTypeCode\WCharSeqA.h"
	-@del /f/q "AnyTypeCode\WCharSeqA.cpp"
	-@del /f/q "WStringSeqC.h"
	-@del /f/q "WStringSeqS.h"
	-@del /f/q "WStringSeqC.cpp"
	-@del /f/q "AnyTypeCode\WStringSeqA.h"
	-@del /f/q "AnyTypeCode\WStringSeqA.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\TAO\$(NULL)" mkdir "Static_Debug\TAO"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"..\..\lib\TAOsd.pdb" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_HAS_VALUETYPE_OUT_INDIRECTION /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAOsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Dynamic_Adapter.obj" \
	"$(INTDIR)\Policy_Manager.obj" \
	"$(INTDIR)\Abstract_Servant_Base.obj" \
	"$(INTDIR)\Acceptor_Filter.obj" \
	"$(INTDIR)\Acceptor_Registry.obj" \
	"$(INTDIR)\Adapter.obj" \
	"$(INTDIR)\Adapter_Factory.obj" \
	"$(INTDIR)\Adapter_Registry.obj" \
	"$(INTDIR)\AnyTypeCode_Adapter.obj" \
	"$(INTDIR)\Argument.obj" \
	"$(INTDIR)\Asynch_Queued_Message.obj" \
	"$(INTDIR)\Asynch_Reply_Dispatcher_Base.obj" \
	"$(INTDIR)\Base_Transport_Property.obj" \
	"$(INTDIR)\BiDir_Adapter.obj" \
	"$(INTDIR)\Bind_Dispatcher_Guard.obj" \
	"$(INTDIR)\Block_Flushing_Strategy.obj" \
	"$(INTDIR)\Blocked_Connect_Strategy.obj" \
	"$(INTDIR)\BooleanSeqC.obj" \
	"$(INTDIR)\Cache_Entries.obj" \
	"$(INTDIR)\CDR.obj" \
	"$(INTDIR)\CharSeqC.obj" \
	"$(INTDIR)\Cleanup_Func_Registry.obj" \
	"$(INTDIR)\Client_Strategy_Factory.obj" \
	"$(INTDIR)\ClientRequestInterceptor_Adapter_Factory.obj" \
	"$(INTDIR)\ClientRequestInterceptor_Adapter.obj" \
	"$(INTDIR)\Codeset_Manager.obj" \
	"$(INTDIR)\Codeset_Manager_Factory_Base.obj" \
	"$(INTDIR)\Codeset_Translator_Base.obj" \
	"$(INTDIR)\Collocated_Invocation.obj" \
	"$(INTDIR)\Collocation_Proxy_Broker.obj" \
	"$(INTDIR)\Collocation_Resolver.obj" \
	"$(INTDIR)\Configurable_Refcount.obj" \
	"$(INTDIR)\Connect_Strategy.obj" \
	"$(INTDIR)\Connection_Handler.obj" \
	"$(INTDIR)\Connection_Purging_Strategy.obj" \
	"$(INTDIR)\Connector_Registry.obj" \
	"$(INTDIR)\CONV_FRAMEC.obj" \
	"$(INTDIR)\CORBA_String.obj" \
	"$(INTDIR)\CORBALOC_Parser.obj" \
	"$(INTDIR)\CORBANAME_Parser.obj" \
	"$(INTDIR)\CurrentC.obj" \
	"$(INTDIR)\debug.obj" \
	"$(INTDIR)\default_client.obj" \
	"$(INTDIR)\Default_Collocation_Resolver.obj" \
	"$(INTDIR)\Default_Endpoint_Selector_Factory.obj" \
	"$(INTDIR)\default_resource.obj" \
	"$(INTDIR)\default_server.obj" \
	"$(INTDIR)\Default_Stub_Factory.obj" \
	"$(INTDIR)\Default_Thread_Lane_Resources_Manager.obj" \
	"$(INTDIR)\DLL_Parser.obj" \
	"$(INTDIR)\DoubleSeqC.obj" \
	"$(INTDIR)\Endpoint.obj" \
	"$(INTDIR)\Endpoint_Selector_Factory.obj" \
	"$(INTDIR)\Environment.obj" \
	"$(INTDIR)\Exception.obj" \
	"$(INTDIR)\Exclusive_TMS.obj" \
	"$(INTDIR)\Fault_Tolerance_Service.obj" \
	"$(INTDIR)\FILE_Parser.obj" \
	"$(INTDIR)\FloatSeqC.obj" \
	"$(INTDIR)\Flushing_Strategy.obj" \
	"$(INTDIR)\GIOP_Fragmentation_Strategy.obj" \
	"$(INTDIR)\GIOP_Message_Base.obj" \
	"$(INTDIR)\GIOP_Message_Generator_Parser.obj" \
	"$(INTDIR)\GIOP_Message_Generator_Parser_10.obj" \
	"$(INTDIR)\GIOP_Message_Generator_Parser_11.obj" \
	"$(INTDIR)\GIOP_Message_Generator_Parser_12.obj" \
	"$(INTDIR)\GIOP_Message_Generator_Parser_Impl.obj" \
	"$(INTDIR)\GIOP_Message_Locate_Header.obj" \
	"$(INTDIR)\GIOP_Message_State.obj" \
	"$(INTDIR)\GIOP_Message_Version.obj" \
	"$(INTDIR)\GIOPC.obj" \
	"$(INTDIR)\HTTP_Client.obj" \
	"$(INTDIR)\HTTP_Handler.obj" \
	"$(INTDIR)\HTTP_Parser.obj" \
	"$(INTDIR)\IFR_Client_Adapter.obj" \
	"$(INTDIR)\IIOP_Acceptor.obj" \
	"$(INTDIR)\IIOP_Connection_Handler.obj" \
	"$(INTDIR)\IIOP_Connector.obj" \
	"$(INTDIR)\IIOP_Endpoint.obj" \
	"$(INTDIR)\IIOP_EndpointsC.obj" \
	"$(INTDIR)\IIOP_Factory.obj" \
	"$(INTDIR)\IIOP_Profile.obj" \
	"$(INTDIR)\IIOP_Transport.obj" \
	"$(INTDIR)\IIOPC.obj" \
	"$(INTDIR)\Incoming_Message_Queue.obj" \
	"$(INTDIR)\Incoming_Message_Stack.obj" \
	"$(INTDIR)\Invocation_Adapter.obj" \
	"$(INTDIR)\Invocation_Base.obj" \
	"$(INTDIR)\Invocation_Endpoint_Selectors.obj" \
	"$(INTDIR)\IOPC.obj" \
	"$(INTDIR)\IOR_Parser.obj" \
	"$(INTDIR)\IORInterceptor_Adapter.obj" \
	"$(INTDIR)\IORInterceptor_Adapter_Factory.obj" \
	"$(INTDIR)\Leader_Follower.obj" \
	"$(INTDIR)\Leader_Follower_Flushing_Strategy.obj" \
	"$(INTDIR)\LF_CH_Event.obj" \
	"$(INTDIR)\LF_Connect_Strategy.obj" \
	"$(INTDIR)\LF_Event.obj" \
	"$(INTDIR)\LF_Event_Binder.obj" \
	"$(INTDIR)\LF_Event_Loop_Thread_Helper.obj" \
	"$(INTDIR)\LF_Follower.obj" \
	"$(INTDIR)\LF_Follower_Auto_Adder.obj" \
	"$(INTDIR)\LF_Follower_Auto_Ptr.obj" \
	"$(INTDIR)\LF_Invocation_Event.obj" \
	"$(INTDIR)\LF_Multi_Event.obj" \
	"$(INTDIR)\LF_Strategy.obj" \
	"$(INTDIR)\LF_Strategy_Complete.obj" \
	"$(INTDIR)\LocalObject.obj" \
	"$(INTDIR)\LocateRequest_Invocation.obj" \
	"$(INTDIR)\LocateRequest_Invocation_Adapter.obj" \
	"$(INTDIR)\LongDoubleSeqC.obj" \
	"$(INTDIR)\LongLongSeqC.obj" \
	"$(INTDIR)\LongSeqC.obj" \
	"$(INTDIR)\LRU_Connection_Purging_Strategy.obj" \
	"$(INTDIR)\MCAST_Parser.obj" \
	"$(INTDIR)\Messaging_PolicyValueC.obj" \
	"$(INTDIR)\Messaging_SyncScopeC.obj" \
	"$(INTDIR)\MMAP_Allocator.obj" \
	"$(INTDIR)\MProfile.obj" \
	"$(INTDIR)\Muxed_TMS.obj" \
	"$(INTDIR)\New_Leader_Generator.obj" \
	"$(INTDIR)\NVList_Adapter.obj" \
	"$(INTDIR)\Null_Fragmentation_Strategy.obj" \
	"$(INTDIR)\Object.obj" \
	"$(INTDIR)\Object_KeyC.obj" \
	"$(INTDIR)\Object_Loader.obj" \
	"$(INTDIR)\Object_Proxy_Broker.obj" \
	"$(INTDIR)\Object_Ref_Table.obj" \
	"$(INTDIR)\ObjectIdListC.obj" \
	"$(INTDIR)\ObjectKey_Table.obj" \
	"$(INTDIR)\OctetSeqC.obj" \
	"$(INTDIR)\On_Demand_Fragmentation_Strategy.obj" \
	"$(INTDIR)\operation_details.obj" \
	"$(INTDIR)\ORB.obj" \
	"$(INTDIR)\ORBInitializer_Registry.obj" \
	"$(INTDIR)\ORBInitializer_Registry_Adapter.obj" \
	"$(INTDIR)\orb_typesC.obj" \
	"$(INTDIR)\ORB_Core.obj" \
	"$(INTDIR)\ORB_Core_Auto_Ptr.obj" \
	"$(INTDIR)\ORB_Core_TSS_Resources.obj" \
	"$(INTDIR)\ORB_Table.obj" \
	"$(INTDIR)\ParameterModeC.obj" \
	"$(INTDIR)\params.obj" \
	"$(INTDIR)\Parser_Registry.obj" \
	"$(INTDIR)\PI_ForwardC.obj" \
	"$(INTDIR)\Pluggable_Messaging_Utils.obj" \
	"$(INTDIR)\Policy_Current.obj" \
	"$(INTDIR)\Policy_CurrentC.obj" \
	"$(INTDIR)\Policy_Current_Impl.obj" \
	"$(INTDIR)\Policy_ForwardC.obj" \
	"$(INTDIR)\Policy_ManagerC.obj" \
	"$(INTDIR)\Policy_Set.obj" \
	"$(INTDIR)\Policy_Validator.obj" \
	"$(INTDIR)\PolicyC.obj" \
	"$(INTDIR)\PolicyFactory_Registry_Adapter.obj" \
	"$(INTDIR)\PolicyFactory_Registry_Factory.obj" \
	"$(INTDIR)\PortableInterceptorC.obj" \
	"$(INTDIR)\Principal.obj" \
	"$(INTDIR)\Profile.obj" \
	"$(INTDIR)\Profile_Transport_Resolver.obj" \
	"$(INTDIR)\Protocol_Factory.obj" \
	"$(INTDIR)\Protocols_Hooks.obj" \
	"$(INTDIR)\Network_Priority_Protocols_Hooks.obj" \
	"$(INTDIR)\Queued_Data.obj" \
	"$(INTDIR)\Queued_Message.obj" \
	"$(INTDIR)\Reactive_Connect_Strategy.obj" \
	"$(INTDIR)\Reactive_Flushing_Strategy.obj" \
	"$(INTDIR)\Refcounted_ObjectKey.obj" \
	"$(INTDIR)\Remote_Invocation.obj" \
	"$(INTDIR)\Remote_Object_Proxy_Broker.obj" \
	"$(INTDIR)\Reply_Dispatcher.obj" \
	"$(INTDIR)\Request_Dispatcher.obj" \
	"$(INTDIR)\Resource_Factory.obj" \
	"$(INTDIR)\Resume_Handle.obj" \
	"$(INTDIR)\Server_Strategy_Factory.obj" \
	"$(INTDIR)\ServerRequestInterceptor_Adapter.obj" \
	"$(INTDIR)\ServerRequestInterceptor_Adapter_Factory.obj" \
	"$(INTDIR)\Service_Callbacks.obj" \
	"$(INTDIR)\Service_Context.obj" \
	"$(INTDIR)\Service_Context_Handler.obj" \
	"$(INTDIR)\Service_Context_Handler_Registry.obj" \
	"$(INTDIR)\Services_Activate.obj" \
	"$(INTDIR)\ServicesC.obj" \
	"$(INTDIR)\ShortSeqC.obj" \
	"$(INTDIR)\String_Alloc.obj" \
	"$(INTDIR)\StringSeqC.obj" \
	"$(INTDIR)\Stub.obj" \
	"$(INTDIR)\Stub_Factory.obj" \
	"$(INTDIR)\Synch_Invocation.obj" \
	"$(INTDIR)\Synch_Queued_Message.obj" \
	"$(INTDIR)\Synch_Reply_Dispatcher.obj" \
	"$(INTDIR)\SystemException.obj" \
	"$(INTDIR)\Tagged_Components.obj" \
	"$(INTDIR)\Tagged_Profile.obj" \
	"$(INTDIR)\TAO_Internal.obj" \
	"$(INTDIR)\TAO_Server_Request.obj" \
	"$(INTDIR)\TAO_Singleton_Manager.obj" \
	"$(INTDIR)\TAOC.obj" \
	"$(INTDIR)\target_specification.obj" \
	"$(INTDIR)\Thread_Lane_Resources.obj" \
	"$(INTDIR)\Thread_Lane_Resources_Manager.obj" \
	"$(INTDIR)\Thread_Per_Connection_Handler.obj" \
	"$(INTDIR)\TimeBaseC.obj" \
	"$(INTDIR)\Transport.obj" \
	"$(INTDIR)\Transport_Acceptor.obj" \
	"$(INTDIR)\Transport_Cache_Manager.obj" \
	"$(INTDIR)\Transport_Connector.obj" \
	"$(INTDIR)\Transport_Descriptor_Interface.obj" \
	"$(INTDIR)\Transport_Mux_Strategy.obj" \
	"$(INTDIR)\Transport_Queueing_Strategies.obj" \
	"$(INTDIR)\Transport_Selection_Guard.obj" \
	"$(INTDIR)\Transport_Timer.obj" \
	"$(INTDIR)\TSS_Resources.obj" \
	"$(INTDIR)\TypeCodeFactory_Adapter.obj" \
	"$(INTDIR)\Typecode_typesC.obj" \
	"$(INTDIR)\ULongLongSeqC.obj" \
	"$(INTDIR)\ULongSeqC.obj" \
	"$(INTDIR)\UserException.obj" \
	"$(INTDIR)\UShortSeqC.obj" \
	"$(INTDIR)\Valuetype_Adapter.obj" \
	"$(INTDIR)\Valuetype_Adapter_Factory.obj" \
	"$(INTDIR)\Wait_On_Leader_Follower.obj" \
	"$(INTDIR)\Wait_On_LF_No_Upcall.obj" \
	"$(INTDIR)\Wait_On_Reactor.obj" \
	"$(INTDIR)\Wait_On_Read.obj" \
	"$(INTDIR)\Wait_Strategy.obj" \
	"$(INTDIR)\WCharSeqC.obj" \
	"$(INTDIR)\WrongTransactionC.obj" \
	"$(INTDIR)\WStringSeqC.obj" \
	"$(INTDIR)\GUIResource_Factory.obj" \
	"$(INTDIR)\ZIOP_Adapter.obj"

"$(OUTDIR)\TAOsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAOsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAOsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\TAO\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAOs.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_HAS_VALUETYPE_OUT_INDIRECTION -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.TAO.dep" "Dynamic_Adapter.cpp" "Policy_Manager.cpp" "Abstract_Servant_Base.cpp" "Acceptor_Filter.cpp" "Acceptor_Registry.cpp" "Adapter.cpp" "Adapter_Factory.cpp" "Adapter_Registry.cpp" "AnyTypeCode_Adapter.cpp" "Argument.cpp" "Asynch_Queued_Message.cpp" "Asynch_Reply_Dispatcher_Base.cpp" "Base_Transport_Property.cpp" "BiDir_Adapter.cpp" "Bind_Dispatcher_Guard.cpp" "Block_Flushing_Strategy.cpp" "Blocked_Connect_Strategy.cpp" "BooleanSeqC.cpp" "Cache_Entries.cpp" "CDR.cpp" "CharSeqC.cpp" "Cleanup_Func_Registry.cpp" "Client_Strategy_Factory.cpp" "ClientRequestInterceptor_Adapter_Factory.cpp" "ClientRequestInterceptor_Adapter.cpp" "Codeset_Manager.cpp" "Codeset_Manager_Factory_Base.cpp" "Codeset_Translator_Base.cpp" "Collocated_Invocation.cpp" "Collocation_Proxy_Broker.cpp" "Collocation_Resolver.cpp" "Configurable_Refcount.cpp" "Connect_Strategy.cpp" "Connection_Handler.cpp" "Connection_Purging_Strategy.cpp" "Connector_Registry.cpp" "CONV_FRAMEC.cpp" "CORBA_String.cpp" "CORBALOC_Parser.cpp" "CORBANAME_Parser.cpp" "CurrentC.cpp" "debug.cpp" "default_client.cpp" "Default_Collocation_Resolver.cpp" "Default_Endpoint_Selector_Factory.cpp" "default_resource.cpp" "default_server.cpp" "Default_Stub_Factory.cpp" "Default_Thread_Lane_Resources_Manager.cpp" "DLL_Parser.cpp" "DoubleSeqC.cpp" "Endpoint.cpp" "Endpoint_Selector_Factory.cpp" "Environment.cpp" "Exception.cpp" "Exclusive_TMS.cpp" "Fault_Tolerance_Service.cpp" "FILE_Parser.cpp" "FloatSeqC.cpp" "Flushing_Strategy.cpp" "GIOP_Fragmentation_Strategy.cpp" "GIOP_Message_Base.cpp" "GIOP_Message_Generator_Parser.cpp" "GIOP_Message_Generator_Parser_10.cpp" "GIOP_Message_Generator_Parser_11.cpp" "GIOP_Message_Generator_Parser_12.cpp" "GIOP_Message_Generator_Parser_Impl.cpp" "GIOP_Message_Locate_Header.cpp" "GIOP_Message_State.cpp" "GIOP_Message_Version.cpp" "GIOPC.cpp" "HTTP_Client.cpp" "HTTP_Handler.cpp" "HTTP_Parser.cpp" "IFR_Client_Adapter.cpp" "IIOP_Acceptor.cpp" "IIOP_Connection_Handler.cpp" "IIOP_Connector.cpp" "IIOP_Endpoint.cpp" "IIOP_EndpointsC.cpp" "IIOP_Factory.cpp" "IIOP_Profile.cpp" "IIOP_Transport.cpp" "IIOPC.cpp" "Incoming_Message_Queue.cpp" "Incoming_Message_Stack.cpp" "Invocation_Adapter.cpp" "Invocation_Base.cpp" "Invocation_Endpoint_Selectors.cpp" "IOPC.cpp" "IOR_Parser.cpp" "IORInterceptor_Adapter.cpp" "IORInterceptor_Adapter_Factory.cpp" "Leader_Follower.cpp" "Leader_Follower_Flushing_Strategy.cpp" "LF_CH_Event.cpp" "LF_Connect_Strategy.cpp" "LF_Event.cpp" "LF_Event_Binder.cpp" "LF_Event_Loop_Thread_Helper.cpp" "LF_Follower.cpp" "LF_Follower_Auto_Adder.cpp" "LF_Follower_Auto_Ptr.cpp" "LF_Invocation_Event.cpp" "LF_Multi_Event.cpp" "LF_Strategy.cpp" "LF_Strategy_Complete.cpp" "LocalObject.cpp" "LocateRequest_Invocation.cpp" "LocateRequest_Invocation_Adapter.cpp" "LongDoubleSeqC.cpp" "LongLongSeqC.cpp" "LongSeqC.cpp" "LRU_Connection_Purging_Strategy.cpp" "MCAST_Parser.cpp" "Messaging_PolicyValueC.cpp" "Messaging_SyncScopeC.cpp" "MMAP_Allocator.cpp" "MProfile.cpp" "Muxed_TMS.cpp" "New_Leader_Generator.cpp" "NVList_Adapter.cpp" "Null_Fragmentation_Strategy.cpp" "Object.cpp" "Object_KeyC.cpp" "Object_Loader.cpp" "Object_Proxy_Broker.cpp" "Object_Ref_Table.cpp" "ObjectIdListC.cpp" "ObjectKey_Table.cpp" "OctetSeqC.cpp" "On_Demand_Fragmentation_Strategy.cpp" "operation_details.cpp" "ORB.cpp" "ORBInitializer_Registry.cpp" "ORBInitializer_Registry_Adapter.cpp" "orb_typesC.cpp" "ORB_Core.cpp" "ORB_Core_Auto_Ptr.cpp" "ORB_Core_TSS_Resources.cpp" "ORB_Table.cpp" "ParameterModeC.cpp" "params.cpp" "Parser_Registry.cpp" "PI_ForwardC.cpp" "Pluggable_Messaging_Utils.cpp" "Policy_Current.cpp" "Policy_CurrentC.cpp" "Policy_Current_Impl.cpp" "Policy_ForwardC.cpp" "Policy_ManagerC.cpp" "Policy_Set.cpp" "Policy_Validator.cpp" "PolicyC.cpp" "PolicyFactory_Registry_Adapter.cpp" "PolicyFactory_Registry_Factory.cpp" "PortableInterceptorC.cpp" "Principal.cpp" "Profile.cpp" "Profile_Transport_Resolver.cpp" "Protocol_Factory.cpp" "Protocols_Hooks.cpp" "Network_Priority_Protocols_Hooks.cpp" "Queued_Data.cpp" "Queued_Message.cpp" "Reactive_Connect_Strategy.cpp" "Reactive_Flushing_Strategy.cpp" "Refcounted_ObjectKey.cpp" "Remote_Invocation.cpp" "Remote_Object_Proxy_Broker.cpp" "Reply_Dispatcher.cpp" "Request_Dispatcher.cpp" "Resource_Factory.cpp" "Resume_Handle.cpp" "Server_Strategy_Factory.cpp" "ServerRequestInterceptor_Adapter.cpp" "ServerRequestInterceptor_Adapter_Factory.cpp" "Service_Callbacks.cpp" "Service_Context.cpp" "Service_Context_Handler.cpp" "Service_Context_Handler_Registry.cpp" "Services_Activate.cpp" "ServicesC.cpp" "ShortSeqC.cpp" "String_Alloc.cpp" "StringSeqC.cpp" "Stub.cpp" "Stub_Factory.cpp" "Synch_Invocation.cpp" "Synch_Queued_Message.cpp" "Synch_Reply_Dispatcher.cpp" "SystemException.cpp" "Tagged_Components.cpp" "Tagged_Profile.cpp" "TAO_Internal.cpp" "TAO_Server_Request.cpp" "TAO_Singleton_Manager.cpp" "TAOC.cpp" "target_specification.cpp" "Thread_Lane_Resources.cpp" "Thread_Lane_Resources_Manager.cpp" "Thread_Per_Connection_Handler.cpp" "TimeBaseC.cpp" "Transport.cpp" "Transport_Acceptor.cpp" "Transport_Cache_Manager.cpp" "Transport_Connector.cpp" "Transport_Descriptor_Interface.cpp" "Transport_Mux_Strategy.cpp" "Transport_Queueing_Strategies.cpp" "Transport_Selection_Guard.cpp" "Transport_Timer.cpp" "TSS_Resources.cpp" "TypeCodeFactory_Adapter.cpp" "Typecode_typesC.cpp" "ULongLongSeqC.cpp" "ULongSeqC.cpp" "UserException.cpp" "UShortSeqC.cpp" "Valuetype_Adapter.cpp" "Valuetype_Adapter_Factory.cpp" "Wait_On_Leader_Follower.cpp" "Wait_On_LF_No_Upcall.cpp" "Wait_On_Reactor.cpp" "Wait_On_Read.cpp" "Wait_Strategy.cpp" "WCharSeqC.cpp" "WrongTransactionC.cpp" "WStringSeqC.cpp" "GUIResource_Factory.cpp" "ZIOP_Adapter.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAOs.lib"
	-@del /f/q "$(OUTDIR)\TAOs.exp"
	-@del /f/q "$(OUTDIR)\TAOs.ilk"
	-@del /f/q "GIOPC.inl"
	-@del /f/q "GIOPC.h"
	-@del /f/q "GIOPS.h"
	-@del /f/q "GIOPC.cpp"
	-@del /f/q "AnyTypeCode\GIOPA.h"
	-@del /f/q "AnyTypeCode\GIOPA.cpp"
	-@del /f/q "CONV_FRAMEC.h"
	-@del /f/q "CONV_FRAMES.h"
	-@del /f/q "CONV_FRAMEC.cpp"
	-@del /f/q "AnyTypeCode\CONV_FRAMEA.h"
	-@del /f/q "AnyTypeCode\CONV_FRAMEA.cpp"
	-@del /f/q "CurrentC.h"
	-@del /f/q "CurrentS.h"
	-@del /f/q "CurrentC.cpp"
	-@del /f/q "AnyTypeCode\CurrentA.h"
	-@del /f/q "AnyTypeCode\CurrentA.cpp"
	-@del /f/q "IIOPC.h"
	-@del /f/q "IIOPC.cpp"
	-@del /f/q "AnyTypeCode\IIOPA.h"
	-@del /f/q "AnyTypeCode\IIOPA.cpp"
	-@del /f/q "IIOP_EndpointsC.h"
	-@del /f/q "IIOP_EndpointsS.h"
	-@del /f/q "IIOP_EndpointsC.cpp"
	-@del /f/q "AnyTypeCode\IIOP_EndpointsA.h"
	-@del /f/q "AnyTypeCode\IIOP_EndpointsA.cpp"
	-@del /f/q "IOPC.h"
	-@del /f/q "IOPS.h"
	-@del /f/q "IOPC.cpp"
	-@del /f/q "AnyTypeCode\IOPA.h"
	-@del /f/q "AnyTypeCode\IOPA.cpp"
	-@del /f/q "Messaging_PolicyValueC.h"
	-@del /f/q "Messaging_PolicyValueS.h"
	-@del /f/q "Messaging_PolicyValueC.cpp"
	-@del /f/q "AnyTypeCode\Messaging_PolicyValueA.h"
	-@del /f/q "AnyTypeCode\Messaging_PolicyValueA.cpp"
	-@del /f/q "Messaging_SyncScopeC.h"
	-@del /f/q "Messaging_SyncScopeS.h"
	-@del /f/q "Messaging_SyncScopeC.cpp"
	-@del /f/q "AnyTypeCode\Messaging_SyncScopeA.h"
	-@del /f/q "AnyTypeCode\Messaging_SyncScopeA.cpp"
	-@del /f/q "ObjectIdListC.h"
	-@del /f/q "ObjectIdListS.h"
	-@del /f/q "ObjectIdListC.cpp"
	-@del /f/q "AnyTypeCode\ObjectIdListA.h"
	-@del /f/q "AnyTypeCode\ObjectIdListA.cpp"
	-@del /f/q "orb_typesC.h"
	-@del /f/q "orb_typesS.h"
	-@del /f/q "orb_typesC.cpp"
	-@del /f/q "AnyTypeCode\orb_typesA.h"
	-@del /f/q "AnyTypeCode\orb_typesA.cpp"
	-@del /f/q "ParameterModeC.h"
	-@del /f/q "ParameterModeS.h"
	-@del /f/q "ParameterModeC.cpp"
	-@del /f/q "AnyTypeCode\ParameterModeA.h"
	-@del /f/q "AnyTypeCode\ParameterModeA.cpp"
	-@del /f/q "Policy_ForwardC.h"
	-@del /f/q "Policy_ForwardS.h"
	-@del /f/q "Policy_ForwardC.cpp"
	-@del /f/q "AnyTypeCode\Policy_ForwardA.h"
	-@del /f/q "AnyTypeCode\Policy_ForwardA.cpp"
	-@del /f/q "Policy_ManagerC.h"
	-@del /f/q "Policy_ManagerS.h"
	-@del /f/q "Policy_ManagerC.cpp"
	-@del /f/q "AnyTypeCode\Policy_ManagerA.h"
	-@del /f/q "AnyTypeCode\Policy_ManagerA.cpp"
	-@del /f/q "Policy_CurrentC.h"
	-@del /f/q "Policy_CurrentS.h"
	-@del /f/q "Policy_CurrentC.cpp"
	-@del /f/q "AnyTypeCode\Policy_CurrentA.h"
	-@del /f/q "AnyTypeCode\Policy_CurrentA.cpp"
	-@del /f/q "PI_ForwardC.h"
	-@del /f/q "PI_ForwardS.h"
	-@del /f/q "PI_ForwardC.cpp"
	-@del /f/q "AnyTypeCode\PI_ForwardA.h"
	-@del /f/q "AnyTypeCode\PI_ForwardA.cpp"
	-@del /f/q "PortableInterceptorC.h"
	-@del /f/q "PortableInterceptorS.h"
	-@del /f/q "PortableInterceptorC.cpp"
	-@del /f/q "AnyTypeCode\PortableInterceptorA.h"
	-@del /f/q "AnyTypeCode\PortableInterceptorA.cpp"
	-@del /f/q "ServicesC.h"
	-@del /f/q "ServicesS.h"
	-@del /f/q "ServicesC.cpp"
	-@del /f/q "AnyTypeCode\ServicesA.h"
	-@del /f/q "AnyTypeCode\ServicesA.cpp"
	-@del /f/q "TAOC.h"
	-@del /f/q "TAOS.h"
	-@del /f/q "TAOC.cpp"
	-@del /f/q "AnyTypeCode\TAOA.h"
	-@del /f/q "AnyTypeCode\TAOA.cpp"
	-@del /f/q "TimeBaseC.h"
	-@del /f/q "TimeBaseS.h"
	-@del /f/q "TimeBaseC.cpp"
	-@del /f/q "AnyTypeCode\TimeBaseA.h"
	-@del /f/q "AnyTypeCode\TimeBaseA.cpp"
	-@del /f/q "BooleanSeqC.h"
	-@del /f/q "BooleanSeqS.h"
	-@del /f/q "BooleanSeqC.cpp"
	-@del /f/q "AnyTypeCode\BooleanSeqA.h"
	-@del /f/q "AnyTypeCode\BooleanSeqA.cpp"
	-@del /f/q "CharSeqC.h"
	-@del /f/q "CharSeqS.h"
	-@del /f/q "CharSeqC.cpp"
	-@del /f/q "AnyTypeCode\CharSeqA.h"
	-@del /f/q "AnyTypeCode\CharSeqA.cpp"
	-@del /f/q "DoubleSeqC.h"
	-@del /f/q "DoubleSeqS.h"
	-@del /f/q "DoubleSeqC.cpp"
	-@del /f/q "AnyTypeCode\DoubleSeqA.h"
	-@del /f/q "AnyTypeCode\DoubleSeqA.cpp"
	-@del /f/q "FloatSeqC.h"
	-@del /f/q "FloatSeqS.h"
	-@del /f/q "FloatSeqC.cpp"
	-@del /f/q "AnyTypeCode\FloatSeqA.h"
	-@del /f/q "AnyTypeCode\FloatSeqA.cpp"
	-@del /f/q "LongDoubleSeqC.h"
	-@del /f/q "LongDoubleSeqS.h"
	-@del /f/q "LongDoubleSeqC.cpp"
	-@del /f/q "AnyTypeCode\LongDoubleSeqA.h"
	-@del /f/q "AnyTypeCode\LongDoubleSeqA.cpp"
	-@del /f/q "LongLongSeqC.h"
	-@del /f/q "LongLongSeqS.h"
	-@del /f/q "LongLongSeqC.cpp"
	-@del /f/q "AnyTypeCode\LongLongSeqA.h"
	-@del /f/q "AnyTypeCode\LongLongSeqA.cpp"
	-@del /f/q "LongSeqC.h"
	-@del /f/q "LongSeqS.h"
	-@del /f/q "LongSeqC.cpp"
	-@del /f/q "AnyTypeCode\LongSeqA.h"
	-@del /f/q "AnyTypeCode\LongSeqA.cpp"
	-@del /f/q "OctetSeqC.h"
	-@del /f/q "OctetSeqS.h"
	-@del /f/q "OctetSeqC.cpp"
	-@del /f/q "AnyTypeCode\OctetSeqA.h"
	-@del /f/q "AnyTypeCode\OctetSeqA.cpp"
	-@del /f/q "ShortSeqC.h"
	-@del /f/q "ShortSeqS.h"
	-@del /f/q "ShortSeqC.cpp"
	-@del /f/q "AnyTypeCode\ShortSeqA.h"
	-@del /f/q "AnyTypeCode\ShortSeqA.cpp"
	-@del /f/q "StringSeqC.h"
	-@del /f/q "StringSeqS.h"
	-@del /f/q "StringSeqC.cpp"
	-@del /f/q "AnyTypeCode\StringSeqA.h"
	-@del /f/q "AnyTypeCode\StringSeqA.cpp"
	-@del /f/q "ULongLongSeqC.h"
	-@del /f/q "ULongLongSeqS.h"
	-@del /f/q "ULongLongSeqC.cpp"
	-@del /f/q "AnyTypeCode\ULongLongSeqA.h"
	-@del /f/q "AnyTypeCode\ULongLongSeqA.cpp"
	-@del /f/q "ULongSeqC.h"
	-@del /f/q "ULongSeqS.h"
	-@del /f/q "ULongSeqC.cpp"
	-@del /f/q "AnyTypeCode\ULongSeqA.h"
	-@del /f/q "AnyTypeCode\ULongSeqA.cpp"
	-@del /f/q "UShortSeqC.h"
	-@del /f/q "UShortSeqS.h"
	-@del /f/q "UShortSeqC.cpp"
	-@del /f/q "AnyTypeCode\UShortSeqA.h"
	-@del /f/q "AnyTypeCode\UShortSeqA.cpp"
	-@del /f/q "WCharSeqC.h"
	-@del /f/q "WCharSeqS.h"
	-@del /f/q "WCharSeqC.cpp"
	-@del /f/q "AnyTypeCode\WCharSeqA.h"
	-@del /f/q "AnyTypeCode\WCharSeqA.cpp"
	-@del /f/q "WStringSeqC.h"
	-@del /f/q "WStringSeqS.h"
	-@del /f/q "WStringSeqC.cpp"
	-@del /f/q "AnyTypeCode\WStringSeqA.h"
	-@del /f/q "AnyTypeCode\WStringSeqA.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\TAO\$(NULL)" mkdir "Static_Release\TAO"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_HAS_VALUETYPE_OUT_INDIRECTION /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAOs.lib"
LINK32_OBJS= \
	"$(INTDIR)\Dynamic_Adapter.obj" \
	"$(INTDIR)\Policy_Manager.obj" \
	"$(INTDIR)\Abstract_Servant_Base.obj" \
	"$(INTDIR)\Acceptor_Filter.obj" \
	"$(INTDIR)\Acceptor_Registry.obj" \
	"$(INTDIR)\Adapter.obj" \
	"$(INTDIR)\Adapter_Factory.obj" \
	"$(INTDIR)\Adapter_Registry.obj" \
	"$(INTDIR)\AnyTypeCode_Adapter.obj" \
	"$(INTDIR)\Argument.obj" \
	"$(INTDIR)\Asynch_Queued_Message.obj" \
	"$(INTDIR)\Asynch_Reply_Dispatcher_Base.obj" \
	"$(INTDIR)\Base_Transport_Property.obj" \
	"$(INTDIR)\BiDir_Adapter.obj" \
	"$(INTDIR)\Bind_Dispatcher_Guard.obj" \
	"$(INTDIR)\Block_Flushing_Strategy.obj" \
	"$(INTDIR)\Blocked_Connect_Strategy.obj" \
	"$(INTDIR)\BooleanSeqC.obj" \
	"$(INTDIR)\Cache_Entries.obj" \
	"$(INTDIR)\CDR.obj" \
	"$(INTDIR)\CharSeqC.obj" \
	"$(INTDIR)\Cleanup_Func_Registry.obj" \
	"$(INTDIR)\Client_Strategy_Factory.obj" \
	"$(INTDIR)\ClientRequestInterceptor_Adapter_Factory.obj" \
	"$(INTDIR)\ClientRequestInterceptor_Adapter.obj" \
	"$(INTDIR)\Codeset_Manager.obj" \
	"$(INTDIR)\Codeset_Manager_Factory_Base.obj" \
	"$(INTDIR)\Codeset_Translator_Base.obj" \
	"$(INTDIR)\Collocated_Invocation.obj" \
	"$(INTDIR)\Collocation_Proxy_Broker.obj" \
	"$(INTDIR)\Collocation_Resolver.obj" \
	"$(INTDIR)\Configurable_Refcount.obj" \
	"$(INTDIR)\Connect_Strategy.obj" \
	"$(INTDIR)\Connection_Handler.obj" \
	"$(INTDIR)\Connection_Purging_Strategy.obj" \
	"$(INTDIR)\Connector_Registry.obj" \
	"$(INTDIR)\CONV_FRAMEC.obj" \
	"$(INTDIR)\CORBA_String.obj" \
	"$(INTDIR)\CORBALOC_Parser.obj" \
	"$(INTDIR)\CORBANAME_Parser.obj" \
	"$(INTDIR)\CurrentC.obj" \
	"$(INTDIR)\debug.obj" \
	"$(INTDIR)\default_client.obj" \
	"$(INTDIR)\Default_Collocation_Resolver.obj" \
	"$(INTDIR)\Default_Endpoint_Selector_Factory.obj" \
	"$(INTDIR)\default_resource.obj" \
	"$(INTDIR)\default_server.obj" \
	"$(INTDIR)\Default_Stub_Factory.obj" \
	"$(INTDIR)\Default_Thread_Lane_Resources_Manager.obj" \
	"$(INTDIR)\DLL_Parser.obj" \
	"$(INTDIR)\DoubleSeqC.obj" \
	"$(INTDIR)\Endpoint.obj" \
	"$(INTDIR)\Endpoint_Selector_Factory.obj" \
	"$(INTDIR)\Environment.obj" \
	"$(INTDIR)\Exception.obj" \
	"$(INTDIR)\Exclusive_TMS.obj" \
	"$(INTDIR)\Fault_Tolerance_Service.obj" \
	"$(INTDIR)\FILE_Parser.obj" \
	"$(INTDIR)\FloatSeqC.obj" \
	"$(INTDIR)\Flushing_Strategy.obj" \
	"$(INTDIR)\GIOP_Fragmentation_Strategy.obj" \
	"$(INTDIR)\GIOP_Message_Base.obj" \
	"$(INTDIR)\GIOP_Message_Generator_Parser.obj" \
	"$(INTDIR)\GIOP_Message_Generator_Parser_10.obj" \
	"$(INTDIR)\GIOP_Message_Generator_Parser_11.obj" \
	"$(INTDIR)\GIOP_Message_Generator_Parser_12.obj" \
	"$(INTDIR)\GIOP_Message_Generator_Parser_Impl.obj" \
	"$(INTDIR)\GIOP_Message_Locate_Header.obj" \
	"$(INTDIR)\GIOP_Message_State.obj" \
	"$(INTDIR)\GIOP_Message_Version.obj" \
	"$(INTDIR)\GIOPC.obj" \
	"$(INTDIR)\HTTP_Client.obj" \
	"$(INTDIR)\HTTP_Handler.obj" \
	"$(INTDIR)\HTTP_Parser.obj" \
	"$(INTDIR)\IFR_Client_Adapter.obj" \
	"$(INTDIR)\IIOP_Acceptor.obj" \
	"$(INTDIR)\IIOP_Connection_Handler.obj" \
	"$(INTDIR)\IIOP_Connector.obj" \
	"$(INTDIR)\IIOP_Endpoint.obj" \
	"$(INTDIR)\IIOP_EndpointsC.obj" \
	"$(INTDIR)\IIOP_Factory.obj" \
	"$(INTDIR)\IIOP_Profile.obj" \
	"$(INTDIR)\IIOP_Transport.obj" \
	"$(INTDIR)\IIOPC.obj" \
	"$(INTDIR)\Incoming_Message_Queue.obj" \
	"$(INTDIR)\Incoming_Message_Stack.obj" \
	"$(INTDIR)\Invocation_Adapter.obj" \
	"$(INTDIR)\Invocation_Base.obj" \
	"$(INTDIR)\Invocation_Endpoint_Selectors.obj" \
	"$(INTDIR)\IOPC.obj" \
	"$(INTDIR)\IOR_Parser.obj" \
	"$(INTDIR)\IORInterceptor_Adapter.obj" \
	"$(INTDIR)\IORInterceptor_Adapter_Factory.obj" \
	"$(INTDIR)\Leader_Follower.obj" \
	"$(INTDIR)\Leader_Follower_Flushing_Strategy.obj" \
	"$(INTDIR)\LF_CH_Event.obj" \
	"$(INTDIR)\LF_Connect_Strategy.obj" \
	"$(INTDIR)\LF_Event.obj" \
	"$(INTDIR)\LF_Event_Binder.obj" \
	"$(INTDIR)\LF_Event_Loop_Thread_Helper.obj" \
	"$(INTDIR)\LF_Follower.obj" \
	"$(INTDIR)\LF_Follower_Auto_Adder.obj" \
	"$(INTDIR)\LF_Follower_Auto_Ptr.obj" \
	"$(INTDIR)\LF_Invocation_Event.obj" \
	"$(INTDIR)\LF_Multi_Event.obj" \
	"$(INTDIR)\LF_Strategy.obj" \
	"$(INTDIR)\LF_Strategy_Complete.obj" \
	"$(INTDIR)\LocalObject.obj" \
	"$(INTDIR)\LocateRequest_Invocation.obj" \
	"$(INTDIR)\LocateRequest_Invocation_Adapter.obj" \
	"$(INTDIR)\LongDoubleSeqC.obj" \
	"$(INTDIR)\LongLongSeqC.obj" \
	"$(INTDIR)\LongSeqC.obj" \
	"$(INTDIR)\LRU_Connection_Purging_Strategy.obj" \
	"$(INTDIR)\MCAST_Parser.obj" \
	"$(INTDIR)\Messaging_PolicyValueC.obj" \
	"$(INTDIR)\Messaging_SyncScopeC.obj" \
	"$(INTDIR)\MMAP_Allocator.obj" \
	"$(INTDIR)\MProfile.obj" \
	"$(INTDIR)\Muxed_TMS.obj" \
	"$(INTDIR)\New_Leader_Generator.obj" \
	"$(INTDIR)\NVList_Adapter.obj" \
	"$(INTDIR)\Null_Fragmentation_Strategy.obj" \
	"$(INTDIR)\Object.obj" \
	"$(INTDIR)\Object_KeyC.obj" \
	"$(INTDIR)\Object_Loader.obj" \
	"$(INTDIR)\Object_Proxy_Broker.obj" \
	"$(INTDIR)\Object_Ref_Table.obj" \
	"$(INTDIR)\ObjectIdListC.obj" \
	"$(INTDIR)\ObjectKey_Table.obj" \
	"$(INTDIR)\OctetSeqC.obj" \
	"$(INTDIR)\On_Demand_Fragmentation_Strategy.obj" \
	"$(INTDIR)\operation_details.obj" \
	"$(INTDIR)\ORB.obj" \
	"$(INTDIR)\ORBInitializer_Registry.obj" \
	"$(INTDIR)\ORBInitializer_Registry_Adapter.obj" \
	"$(INTDIR)\orb_typesC.obj" \
	"$(INTDIR)\ORB_Core.obj" \
	"$(INTDIR)\ORB_Core_Auto_Ptr.obj" \
	"$(INTDIR)\ORB_Core_TSS_Resources.obj" \
	"$(INTDIR)\ORB_Table.obj" \
	"$(INTDIR)\ParameterModeC.obj" \
	"$(INTDIR)\params.obj" \
	"$(INTDIR)\Parser_Registry.obj" \
	"$(INTDIR)\PI_ForwardC.obj" \
	"$(INTDIR)\Pluggable_Messaging_Utils.obj" \
	"$(INTDIR)\Policy_Current.obj" \
	"$(INTDIR)\Policy_CurrentC.obj" \
	"$(INTDIR)\Policy_Current_Impl.obj" \
	"$(INTDIR)\Policy_ForwardC.obj" \
	"$(INTDIR)\Policy_ManagerC.obj" \
	"$(INTDIR)\Policy_Set.obj" \
	"$(INTDIR)\Policy_Validator.obj" \
	"$(INTDIR)\PolicyC.obj" \
	"$(INTDIR)\PolicyFactory_Registry_Adapter.obj" \
	"$(INTDIR)\PolicyFactory_Registry_Factory.obj" \
	"$(INTDIR)\PortableInterceptorC.obj" \
	"$(INTDIR)\Principal.obj" \
	"$(INTDIR)\Profile.obj" \
	"$(INTDIR)\Profile_Transport_Resolver.obj" \
	"$(INTDIR)\Protocol_Factory.obj" \
	"$(INTDIR)\Protocols_Hooks.obj" \
	"$(INTDIR)\Network_Priority_Protocols_Hooks.obj" \
	"$(INTDIR)\Queued_Data.obj" \
	"$(INTDIR)\Queued_Message.obj" \
	"$(INTDIR)\Reactive_Connect_Strategy.obj" \
	"$(INTDIR)\Reactive_Flushing_Strategy.obj" \
	"$(INTDIR)\Refcounted_ObjectKey.obj" \
	"$(INTDIR)\Remote_Invocation.obj" \
	"$(INTDIR)\Remote_Object_Proxy_Broker.obj" \
	"$(INTDIR)\Reply_Dispatcher.obj" \
	"$(INTDIR)\Request_Dispatcher.obj" \
	"$(INTDIR)\Resource_Factory.obj" \
	"$(INTDIR)\Resume_Handle.obj" \
	"$(INTDIR)\Server_Strategy_Factory.obj" \
	"$(INTDIR)\ServerRequestInterceptor_Adapter.obj" \
	"$(INTDIR)\ServerRequestInterceptor_Adapter_Factory.obj" \
	"$(INTDIR)\Service_Callbacks.obj" \
	"$(INTDIR)\Service_Context.obj" \
	"$(INTDIR)\Service_Context_Handler.obj" \
	"$(INTDIR)\Service_Context_Handler_Registry.obj" \
	"$(INTDIR)\Services_Activate.obj" \
	"$(INTDIR)\ServicesC.obj" \
	"$(INTDIR)\ShortSeqC.obj" \
	"$(INTDIR)\String_Alloc.obj" \
	"$(INTDIR)\StringSeqC.obj" \
	"$(INTDIR)\Stub.obj" \
	"$(INTDIR)\Stub_Factory.obj" \
	"$(INTDIR)\Synch_Invocation.obj" \
	"$(INTDIR)\Synch_Queued_Message.obj" \
	"$(INTDIR)\Synch_Reply_Dispatcher.obj" \
	"$(INTDIR)\SystemException.obj" \
	"$(INTDIR)\Tagged_Components.obj" \
	"$(INTDIR)\Tagged_Profile.obj" \
	"$(INTDIR)\TAO_Internal.obj" \
	"$(INTDIR)\TAO_Server_Request.obj" \
	"$(INTDIR)\TAO_Singleton_Manager.obj" \
	"$(INTDIR)\TAOC.obj" \
	"$(INTDIR)\target_specification.obj" \
	"$(INTDIR)\Thread_Lane_Resources.obj" \
	"$(INTDIR)\Thread_Lane_Resources_Manager.obj" \
	"$(INTDIR)\Thread_Per_Connection_Handler.obj" \
	"$(INTDIR)\TimeBaseC.obj" \
	"$(INTDIR)\Transport.obj" \
	"$(INTDIR)\Transport_Acceptor.obj" \
	"$(INTDIR)\Transport_Cache_Manager.obj" \
	"$(INTDIR)\Transport_Connector.obj" \
	"$(INTDIR)\Transport_Descriptor_Interface.obj" \
	"$(INTDIR)\Transport_Mux_Strategy.obj" \
	"$(INTDIR)\Transport_Queueing_Strategies.obj" \
	"$(INTDIR)\Transport_Selection_Guard.obj" \
	"$(INTDIR)\Transport_Timer.obj" \
	"$(INTDIR)\TSS_Resources.obj" \
	"$(INTDIR)\TypeCodeFactory_Adapter.obj" \
	"$(INTDIR)\Typecode_typesC.obj" \
	"$(INTDIR)\ULongLongSeqC.obj" \
	"$(INTDIR)\ULongSeqC.obj" \
	"$(INTDIR)\UserException.obj" \
	"$(INTDIR)\UShortSeqC.obj" \
	"$(INTDIR)\Valuetype_Adapter.obj" \
	"$(INTDIR)\Valuetype_Adapter_Factory.obj" \
	"$(INTDIR)\Wait_On_Leader_Follower.obj" \
	"$(INTDIR)\Wait_On_LF_No_Upcall.obj" \
	"$(INTDIR)\Wait_On_Reactor.obj" \
	"$(INTDIR)\Wait_On_Read.obj" \
	"$(INTDIR)\Wait_Strategy.obj" \
	"$(INTDIR)\WCharSeqC.obj" \
	"$(INTDIR)\WrongTransactionC.obj" \
	"$(INTDIR)\WStringSeqC.obj" \
	"$(INTDIR)\GUIResource_Factory.obj" \
	"$(INTDIR)\ZIOP_Adapter.obj"

"$(OUTDIR)\TAOs.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAOs.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAOs.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.TAO.dep")
!INCLUDE "Makefile.TAO.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Dynamic_Adapter.cpp"

"$(INTDIR)\Dynamic_Adapter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Dynamic_Adapter.obj" $(SOURCE)

SOURCE="Policy_Manager.cpp"

"$(INTDIR)\Policy_Manager.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Policy_Manager.obj" $(SOURCE)

SOURCE="Abstract_Servant_Base.cpp"

"$(INTDIR)\Abstract_Servant_Base.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Abstract_Servant_Base.obj" $(SOURCE)

SOURCE="Acceptor_Filter.cpp"

"$(INTDIR)\Acceptor_Filter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Acceptor_Filter.obj" $(SOURCE)

SOURCE="Acceptor_Registry.cpp"

"$(INTDIR)\Acceptor_Registry.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Acceptor_Registry.obj" $(SOURCE)

SOURCE="Adapter.cpp"

"$(INTDIR)\Adapter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Adapter.obj" $(SOURCE)

SOURCE="Adapter_Factory.cpp"

"$(INTDIR)\Adapter_Factory.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Adapter_Factory.obj" $(SOURCE)

SOURCE="Adapter_Registry.cpp"

"$(INTDIR)\Adapter_Registry.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Adapter_Registry.obj" $(SOURCE)

SOURCE="AnyTypeCode_Adapter.cpp"

"$(INTDIR)\AnyTypeCode_Adapter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode_Adapter.obj" $(SOURCE)

SOURCE="Argument.cpp"

"$(INTDIR)\Argument.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Argument.obj" $(SOURCE)

SOURCE="Asynch_Queued_Message.cpp"

"$(INTDIR)\Asynch_Queued_Message.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Asynch_Queued_Message.obj" $(SOURCE)

SOURCE="Asynch_Reply_Dispatcher_Base.cpp"

"$(INTDIR)\Asynch_Reply_Dispatcher_Base.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Asynch_Reply_Dispatcher_Base.obj" $(SOURCE)

SOURCE="Base_Transport_Property.cpp"

"$(INTDIR)\Base_Transport_Property.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Base_Transport_Property.obj" $(SOURCE)

SOURCE="BiDir_Adapter.cpp"

"$(INTDIR)\BiDir_Adapter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\BiDir_Adapter.obj" $(SOURCE)

SOURCE="Bind_Dispatcher_Guard.cpp"

"$(INTDIR)\Bind_Dispatcher_Guard.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Bind_Dispatcher_Guard.obj" $(SOURCE)

SOURCE="Block_Flushing_Strategy.cpp"

"$(INTDIR)\Block_Flushing_Strategy.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Block_Flushing_Strategy.obj" $(SOURCE)

SOURCE="Blocked_Connect_Strategy.cpp"

"$(INTDIR)\Blocked_Connect_Strategy.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Blocked_Connect_Strategy.obj" $(SOURCE)

SOURCE="BooleanSeqC.cpp"

"$(INTDIR)\BooleanSeqC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\BooleanSeqC.obj" $(SOURCE)

SOURCE="Cache_Entries.cpp"

"$(INTDIR)\Cache_Entries.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Cache_Entries.obj" $(SOURCE)

SOURCE="CDR.cpp"

"$(INTDIR)\CDR.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CDR.obj" $(SOURCE)

SOURCE="CharSeqC.cpp"

"$(INTDIR)\CharSeqC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CharSeqC.obj" $(SOURCE)

SOURCE="Cleanup_Func_Registry.cpp"

"$(INTDIR)\Cleanup_Func_Registry.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Cleanup_Func_Registry.obj" $(SOURCE)

SOURCE="Client_Strategy_Factory.cpp"

"$(INTDIR)\Client_Strategy_Factory.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Client_Strategy_Factory.obj" $(SOURCE)

SOURCE="ClientRequestInterceptor_Adapter_Factory.cpp"

"$(INTDIR)\ClientRequestInterceptor_Adapter_Factory.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ClientRequestInterceptor_Adapter_Factory.obj" $(SOURCE)

SOURCE="ClientRequestInterceptor_Adapter.cpp"

"$(INTDIR)\ClientRequestInterceptor_Adapter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ClientRequestInterceptor_Adapter.obj" $(SOURCE)

SOURCE="Codeset_Manager.cpp"

"$(INTDIR)\Codeset_Manager.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Codeset_Manager.obj" $(SOURCE)

SOURCE="Codeset_Manager_Factory_Base.cpp"

"$(INTDIR)\Codeset_Manager_Factory_Base.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Codeset_Manager_Factory_Base.obj" $(SOURCE)

SOURCE="Codeset_Translator_Base.cpp"

"$(INTDIR)\Codeset_Translator_Base.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Codeset_Translator_Base.obj" $(SOURCE)

SOURCE="Collocated_Invocation.cpp"

"$(INTDIR)\Collocated_Invocation.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Collocated_Invocation.obj" $(SOURCE)

SOURCE="Collocation_Proxy_Broker.cpp"

"$(INTDIR)\Collocation_Proxy_Broker.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Collocation_Proxy_Broker.obj" $(SOURCE)

SOURCE="Collocation_Resolver.cpp"

"$(INTDIR)\Collocation_Resolver.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Collocation_Resolver.obj" $(SOURCE)

SOURCE="Configurable_Refcount.cpp"

"$(INTDIR)\Configurable_Refcount.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Configurable_Refcount.obj" $(SOURCE)

SOURCE="Connect_Strategy.cpp"

"$(INTDIR)\Connect_Strategy.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Connect_Strategy.obj" $(SOURCE)

SOURCE="Connection_Handler.cpp"

"$(INTDIR)\Connection_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Connection_Handler.obj" $(SOURCE)

SOURCE="Connection_Purging_Strategy.cpp"

"$(INTDIR)\Connection_Purging_Strategy.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Connection_Purging_Strategy.obj" $(SOURCE)

SOURCE="Connector_Registry.cpp"

"$(INTDIR)\Connector_Registry.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Connector_Registry.obj" $(SOURCE)

SOURCE="CONV_FRAMEC.cpp"

"$(INTDIR)\CONV_FRAMEC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CONV_FRAMEC.obj" $(SOURCE)

SOURCE="CORBA_String.cpp"

"$(INTDIR)\CORBA_String.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CORBA_String.obj" $(SOURCE)

SOURCE="CORBALOC_Parser.cpp"

"$(INTDIR)\CORBALOC_Parser.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CORBALOC_Parser.obj" $(SOURCE)

SOURCE="CORBANAME_Parser.cpp"

"$(INTDIR)\CORBANAME_Parser.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CORBANAME_Parser.obj" $(SOURCE)

SOURCE="CurrentC.cpp"

"$(INTDIR)\CurrentC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CurrentC.obj" $(SOURCE)

SOURCE="debug.cpp"

"$(INTDIR)\debug.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\debug.obj" $(SOURCE)

SOURCE="default_client.cpp"

"$(INTDIR)\default_client.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\default_client.obj" $(SOURCE)

SOURCE="Default_Collocation_Resolver.cpp"

"$(INTDIR)\Default_Collocation_Resolver.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Default_Collocation_Resolver.obj" $(SOURCE)

SOURCE="Default_Endpoint_Selector_Factory.cpp"

"$(INTDIR)\Default_Endpoint_Selector_Factory.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Default_Endpoint_Selector_Factory.obj" $(SOURCE)

SOURCE="default_resource.cpp"

"$(INTDIR)\default_resource.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\default_resource.obj" $(SOURCE)

SOURCE="default_server.cpp"

"$(INTDIR)\default_server.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\default_server.obj" $(SOURCE)

SOURCE="Default_Stub_Factory.cpp"

"$(INTDIR)\Default_Stub_Factory.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Default_Stub_Factory.obj" $(SOURCE)

SOURCE="Default_Thread_Lane_Resources_Manager.cpp"

"$(INTDIR)\Default_Thread_Lane_Resources_Manager.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Default_Thread_Lane_Resources_Manager.obj" $(SOURCE)

SOURCE="DLL_Parser.cpp"

"$(INTDIR)\DLL_Parser.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DLL_Parser.obj" $(SOURCE)

SOURCE="DoubleSeqC.cpp"

"$(INTDIR)\DoubleSeqC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DoubleSeqC.obj" $(SOURCE)

SOURCE="Endpoint.cpp"

"$(INTDIR)\Endpoint.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Endpoint.obj" $(SOURCE)

SOURCE="Endpoint_Selector_Factory.cpp"

"$(INTDIR)\Endpoint_Selector_Factory.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Endpoint_Selector_Factory.obj" $(SOURCE)

SOURCE="Environment.cpp"

"$(INTDIR)\Environment.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Environment.obj" $(SOURCE)

SOURCE="Exception.cpp"

"$(INTDIR)\Exception.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Exception.obj" $(SOURCE)

SOURCE="Exclusive_TMS.cpp"

"$(INTDIR)\Exclusive_TMS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Exclusive_TMS.obj" $(SOURCE)

SOURCE="Fault_Tolerance_Service.cpp"

"$(INTDIR)\Fault_Tolerance_Service.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Fault_Tolerance_Service.obj" $(SOURCE)

SOURCE="FILE_Parser.cpp"

"$(INTDIR)\FILE_Parser.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\FILE_Parser.obj" $(SOURCE)

SOURCE="FloatSeqC.cpp"

"$(INTDIR)\FloatSeqC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\FloatSeqC.obj" $(SOURCE)

SOURCE="Flushing_Strategy.cpp"

"$(INTDIR)\Flushing_Strategy.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Flushing_Strategy.obj" $(SOURCE)

SOURCE="GIOP_Fragmentation_Strategy.cpp"

"$(INTDIR)\GIOP_Fragmentation_Strategy.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\GIOP_Fragmentation_Strategy.obj" $(SOURCE)

SOURCE="GIOP_Message_Base.cpp"

"$(INTDIR)\GIOP_Message_Base.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\GIOP_Message_Base.obj" $(SOURCE)

SOURCE="GIOP_Message_Generator_Parser.cpp"

"$(INTDIR)\GIOP_Message_Generator_Parser.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\GIOP_Message_Generator_Parser.obj" $(SOURCE)

SOURCE="GIOP_Message_Generator_Parser_10.cpp"

"$(INTDIR)\GIOP_Message_Generator_Parser_10.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\GIOP_Message_Generator_Parser_10.obj" $(SOURCE)

SOURCE="GIOP_Message_Generator_Parser_11.cpp"

"$(INTDIR)\GIOP_Message_Generator_Parser_11.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\GIOP_Message_Generator_Parser_11.obj" $(SOURCE)

SOURCE="GIOP_Message_Generator_Parser_12.cpp"

"$(INTDIR)\GIOP_Message_Generator_Parser_12.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\GIOP_Message_Generator_Parser_12.obj" $(SOURCE)

SOURCE="GIOP_Message_Generator_Parser_Impl.cpp"

"$(INTDIR)\GIOP_Message_Generator_Parser_Impl.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\GIOP_Message_Generator_Parser_Impl.obj" $(SOURCE)

SOURCE="GIOP_Message_Locate_Header.cpp"

"$(INTDIR)\GIOP_Message_Locate_Header.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\GIOP_Message_Locate_Header.obj" $(SOURCE)

SOURCE="GIOP_Message_State.cpp"

"$(INTDIR)\GIOP_Message_State.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\GIOP_Message_State.obj" $(SOURCE)

SOURCE="GIOP_Message_Version.cpp"

"$(INTDIR)\GIOP_Message_Version.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\GIOP_Message_Version.obj" $(SOURCE)

SOURCE="GIOPC.cpp"

"$(INTDIR)\GIOPC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\GIOPC.obj" $(SOURCE)

SOURCE="HTTP_Client.cpp"

"$(INTDIR)\HTTP_Client.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\HTTP_Client.obj" $(SOURCE)

SOURCE="HTTP_Handler.cpp"

"$(INTDIR)\HTTP_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\HTTP_Handler.obj" $(SOURCE)

SOURCE="HTTP_Parser.cpp"

"$(INTDIR)\HTTP_Parser.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\HTTP_Parser.obj" $(SOURCE)

SOURCE="IFR_Client_Adapter.cpp"

"$(INTDIR)\IFR_Client_Adapter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IFR_Client_Adapter.obj" $(SOURCE)

SOURCE="IIOP_Acceptor.cpp"

"$(INTDIR)\IIOP_Acceptor.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IIOP_Acceptor.obj" $(SOURCE)

SOURCE="IIOP_Connection_Handler.cpp"

"$(INTDIR)\IIOP_Connection_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IIOP_Connection_Handler.obj" $(SOURCE)

SOURCE="IIOP_Connector.cpp"

"$(INTDIR)\IIOP_Connector.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IIOP_Connector.obj" $(SOURCE)

SOURCE="IIOP_Endpoint.cpp"

"$(INTDIR)\IIOP_Endpoint.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IIOP_Endpoint.obj" $(SOURCE)

SOURCE="IIOP_EndpointsC.cpp"

"$(INTDIR)\IIOP_EndpointsC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IIOP_EndpointsC.obj" $(SOURCE)

SOURCE="IIOP_Factory.cpp"

"$(INTDIR)\IIOP_Factory.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IIOP_Factory.obj" $(SOURCE)

SOURCE="IIOP_Profile.cpp"

"$(INTDIR)\IIOP_Profile.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IIOP_Profile.obj" $(SOURCE)

SOURCE="IIOP_Transport.cpp"

"$(INTDIR)\IIOP_Transport.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IIOP_Transport.obj" $(SOURCE)

SOURCE="IIOPC.cpp"

"$(INTDIR)\IIOPC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IIOPC.obj" $(SOURCE)

SOURCE="Incoming_Message_Queue.cpp"

"$(INTDIR)\Incoming_Message_Queue.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Incoming_Message_Queue.obj" $(SOURCE)

SOURCE="Incoming_Message_Stack.cpp"

"$(INTDIR)\Incoming_Message_Stack.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Incoming_Message_Stack.obj" $(SOURCE)

SOURCE="Invocation_Adapter.cpp"

"$(INTDIR)\Invocation_Adapter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Invocation_Adapter.obj" $(SOURCE)

SOURCE="Invocation_Base.cpp"

"$(INTDIR)\Invocation_Base.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Invocation_Base.obj" $(SOURCE)

SOURCE="Invocation_Endpoint_Selectors.cpp"

"$(INTDIR)\Invocation_Endpoint_Selectors.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Invocation_Endpoint_Selectors.obj" $(SOURCE)

SOURCE="IOPC.cpp"

"$(INTDIR)\IOPC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IOPC.obj" $(SOURCE)

SOURCE="IOR_Parser.cpp"

"$(INTDIR)\IOR_Parser.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IOR_Parser.obj" $(SOURCE)

SOURCE="IORInterceptor_Adapter.cpp"

"$(INTDIR)\IORInterceptor_Adapter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IORInterceptor_Adapter.obj" $(SOURCE)

SOURCE="IORInterceptor_Adapter_Factory.cpp"

"$(INTDIR)\IORInterceptor_Adapter_Factory.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IORInterceptor_Adapter_Factory.obj" $(SOURCE)

SOURCE="Leader_Follower.cpp"

"$(INTDIR)\Leader_Follower.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Leader_Follower.obj" $(SOURCE)

SOURCE="Leader_Follower_Flushing_Strategy.cpp"

"$(INTDIR)\Leader_Follower_Flushing_Strategy.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Leader_Follower_Flushing_Strategy.obj" $(SOURCE)

SOURCE="LF_CH_Event.cpp"

"$(INTDIR)\LF_CH_Event.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\LF_CH_Event.obj" $(SOURCE)

SOURCE="LF_Connect_Strategy.cpp"

"$(INTDIR)\LF_Connect_Strategy.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\LF_Connect_Strategy.obj" $(SOURCE)

SOURCE="LF_Event.cpp"

"$(INTDIR)\LF_Event.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\LF_Event.obj" $(SOURCE)

SOURCE="LF_Event_Binder.cpp"

"$(INTDIR)\LF_Event_Binder.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\LF_Event_Binder.obj" $(SOURCE)

SOURCE="LF_Event_Loop_Thread_Helper.cpp"

"$(INTDIR)\LF_Event_Loop_Thread_Helper.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\LF_Event_Loop_Thread_Helper.obj" $(SOURCE)

SOURCE="LF_Follower.cpp"

"$(INTDIR)\LF_Follower.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\LF_Follower.obj" $(SOURCE)

SOURCE="LF_Follower_Auto_Adder.cpp"

"$(INTDIR)\LF_Follower_Auto_Adder.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\LF_Follower_Auto_Adder.obj" $(SOURCE)

SOURCE="LF_Follower_Auto_Ptr.cpp"

"$(INTDIR)\LF_Follower_Auto_Ptr.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\LF_Follower_Auto_Ptr.obj" $(SOURCE)

SOURCE="LF_Invocation_Event.cpp"

"$(INTDIR)\LF_Invocation_Event.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\LF_Invocation_Event.obj" $(SOURCE)

SOURCE="LF_Multi_Event.cpp"

"$(INTDIR)\LF_Multi_Event.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\LF_Multi_Event.obj" $(SOURCE)

SOURCE="LF_Strategy.cpp"

"$(INTDIR)\LF_Strategy.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\LF_Strategy.obj" $(SOURCE)

SOURCE="LF_Strategy_Complete.cpp"

"$(INTDIR)\LF_Strategy_Complete.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\LF_Strategy_Complete.obj" $(SOURCE)

SOURCE="LocalObject.cpp"

"$(INTDIR)\LocalObject.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\LocalObject.obj" $(SOURCE)

SOURCE="LocateRequest_Invocation.cpp"

"$(INTDIR)\LocateRequest_Invocation.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\LocateRequest_Invocation.obj" $(SOURCE)

SOURCE="LocateRequest_Invocation_Adapter.cpp"

"$(INTDIR)\LocateRequest_Invocation_Adapter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\LocateRequest_Invocation_Adapter.obj" $(SOURCE)

SOURCE="LongDoubleSeqC.cpp"

"$(INTDIR)\LongDoubleSeqC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\LongDoubleSeqC.obj" $(SOURCE)

SOURCE="LongLongSeqC.cpp"

"$(INTDIR)\LongLongSeqC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\LongLongSeqC.obj" $(SOURCE)

SOURCE="LongSeqC.cpp"

"$(INTDIR)\LongSeqC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\LongSeqC.obj" $(SOURCE)

SOURCE="LRU_Connection_Purging_Strategy.cpp"

"$(INTDIR)\LRU_Connection_Purging_Strategy.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\LRU_Connection_Purging_Strategy.obj" $(SOURCE)

SOURCE="MCAST_Parser.cpp"

"$(INTDIR)\MCAST_Parser.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\MCAST_Parser.obj" $(SOURCE)

SOURCE="Messaging_PolicyValueC.cpp"

"$(INTDIR)\Messaging_PolicyValueC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Messaging_PolicyValueC.obj" $(SOURCE)

SOURCE="Messaging_SyncScopeC.cpp"

"$(INTDIR)\Messaging_SyncScopeC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Messaging_SyncScopeC.obj" $(SOURCE)

SOURCE="MMAP_Allocator.cpp"

"$(INTDIR)\MMAP_Allocator.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\MMAP_Allocator.obj" $(SOURCE)

SOURCE="MProfile.cpp"

"$(INTDIR)\MProfile.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\MProfile.obj" $(SOURCE)

SOURCE="Muxed_TMS.cpp"

"$(INTDIR)\Muxed_TMS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Muxed_TMS.obj" $(SOURCE)

SOURCE="New_Leader_Generator.cpp"

"$(INTDIR)\New_Leader_Generator.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\New_Leader_Generator.obj" $(SOURCE)

SOURCE="NVList_Adapter.cpp"

"$(INTDIR)\NVList_Adapter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\NVList_Adapter.obj" $(SOURCE)

SOURCE="Null_Fragmentation_Strategy.cpp"

"$(INTDIR)\Null_Fragmentation_Strategy.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Null_Fragmentation_Strategy.obj" $(SOURCE)

SOURCE="Object.cpp"

"$(INTDIR)\Object.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Object.obj" $(SOURCE)

SOURCE="Object_KeyC.cpp"

"$(INTDIR)\Object_KeyC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Object_KeyC.obj" $(SOURCE)

SOURCE="Object_Loader.cpp"

"$(INTDIR)\Object_Loader.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Object_Loader.obj" $(SOURCE)

SOURCE="Object_Proxy_Broker.cpp"

"$(INTDIR)\Object_Proxy_Broker.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Object_Proxy_Broker.obj" $(SOURCE)

SOURCE="Object_Ref_Table.cpp"

"$(INTDIR)\Object_Ref_Table.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Object_Ref_Table.obj" $(SOURCE)

SOURCE="ObjectIdListC.cpp"

"$(INTDIR)\ObjectIdListC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ObjectIdListC.obj" $(SOURCE)

SOURCE="ObjectKey_Table.cpp"

"$(INTDIR)\ObjectKey_Table.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ObjectKey_Table.obj" $(SOURCE)

SOURCE="OctetSeqC.cpp"

"$(INTDIR)\OctetSeqC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\OctetSeqC.obj" $(SOURCE)

SOURCE="On_Demand_Fragmentation_Strategy.cpp"

"$(INTDIR)\On_Demand_Fragmentation_Strategy.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\On_Demand_Fragmentation_Strategy.obj" $(SOURCE)

SOURCE="operation_details.cpp"

"$(INTDIR)\operation_details.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\operation_details.obj" $(SOURCE)

SOURCE="ORB.cpp"

"$(INTDIR)\ORB.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ORB.obj" $(SOURCE)

SOURCE="ORBInitializer_Registry.cpp"

"$(INTDIR)\ORBInitializer_Registry.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ORBInitializer_Registry.obj" $(SOURCE)

SOURCE="ORBInitializer_Registry_Adapter.cpp"

"$(INTDIR)\ORBInitializer_Registry_Adapter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ORBInitializer_Registry_Adapter.obj" $(SOURCE)

SOURCE="orb_typesC.cpp"

"$(INTDIR)\orb_typesC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\orb_typesC.obj" $(SOURCE)

SOURCE="ORB_Core.cpp"

"$(INTDIR)\ORB_Core.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ORB_Core.obj" $(SOURCE)

SOURCE="ORB_Core_Auto_Ptr.cpp"

"$(INTDIR)\ORB_Core_Auto_Ptr.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ORB_Core_Auto_Ptr.obj" $(SOURCE)

SOURCE="ORB_Core_TSS_Resources.cpp"

"$(INTDIR)\ORB_Core_TSS_Resources.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ORB_Core_TSS_Resources.obj" $(SOURCE)

SOURCE="ORB_Table.cpp"

"$(INTDIR)\ORB_Table.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ORB_Table.obj" $(SOURCE)

SOURCE="ParameterModeC.cpp"

"$(INTDIR)\ParameterModeC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ParameterModeC.obj" $(SOURCE)

SOURCE="params.cpp"

"$(INTDIR)\params.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\params.obj" $(SOURCE)

SOURCE="Parser_Registry.cpp"

"$(INTDIR)\Parser_Registry.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Parser_Registry.obj" $(SOURCE)

SOURCE="PI_ForwardC.cpp"

"$(INTDIR)\PI_ForwardC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI_ForwardC.obj" $(SOURCE)

SOURCE="Pluggable_Messaging_Utils.cpp"

"$(INTDIR)\Pluggable_Messaging_Utils.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Pluggable_Messaging_Utils.obj" $(SOURCE)

SOURCE="Policy_Current.cpp"

"$(INTDIR)\Policy_Current.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Policy_Current.obj" $(SOURCE)

SOURCE="Policy_CurrentC.cpp"

"$(INTDIR)\Policy_CurrentC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Policy_CurrentC.obj" $(SOURCE)

SOURCE="Policy_Current_Impl.cpp"

"$(INTDIR)\Policy_Current_Impl.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Policy_Current_Impl.obj" $(SOURCE)

SOURCE="Policy_ForwardC.cpp"

"$(INTDIR)\Policy_ForwardC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Policy_ForwardC.obj" $(SOURCE)

SOURCE="Policy_ManagerC.cpp"

"$(INTDIR)\Policy_ManagerC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Policy_ManagerC.obj" $(SOURCE)

SOURCE="Policy_Set.cpp"

"$(INTDIR)\Policy_Set.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Policy_Set.obj" $(SOURCE)

SOURCE="Policy_Validator.cpp"

"$(INTDIR)\Policy_Validator.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Policy_Validator.obj" $(SOURCE)

SOURCE="PolicyC.cpp"

"$(INTDIR)\PolicyC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PolicyC.obj" $(SOURCE)

SOURCE="PolicyFactory_Registry_Adapter.cpp"

"$(INTDIR)\PolicyFactory_Registry_Adapter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PolicyFactory_Registry_Adapter.obj" $(SOURCE)

SOURCE="PolicyFactory_Registry_Factory.cpp"

"$(INTDIR)\PolicyFactory_Registry_Factory.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PolicyFactory_Registry_Factory.obj" $(SOURCE)

SOURCE="PortableInterceptorC.cpp"

"$(INTDIR)\PortableInterceptorC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableInterceptorC.obj" $(SOURCE)

SOURCE="Principal.cpp"

"$(INTDIR)\Principal.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Principal.obj" $(SOURCE)

SOURCE="Profile.cpp"

"$(INTDIR)\Profile.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Profile.obj" $(SOURCE)

SOURCE="Profile_Transport_Resolver.cpp"

"$(INTDIR)\Profile_Transport_Resolver.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Profile_Transport_Resolver.obj" $(SOURCE)

SOURCE="Protocol_Factory.cpp"

"$(INTDIR)\Protocol_Factory.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Protocol_Factory.obj" $(SOURCE)

SOURCE="Protocols_Hooks.cpp"

"$(INTDIR)\Protocols_Hooks.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Protocols_Hooks.obj" $(SOURCE)

SOURCE="Network_Priority_Protocols_Hooks.cpp"

"$(INTDIR)\Network_Priority_Protocols_Hooks.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Network_Priority_Protocols_Hooks.obj" $(SOURCE)

SOURCE="Queued_Data.cpp"

"$(INTDIR)\Queued_Data.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Queued_Data.obj" $(SOURCE)

SOURCE="Queued_Message.cpp"

"$(INTDIR)\Queued_Message.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Queued_Message.obj" $(SOURCE)

SOURCE="Reactive_Connect_Strategy.cpp"

"$(INTDIR)\Reactive_Connect_Strategy.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Reactive_Connect_Strategy.obj" $(SOURCE)

SOURCE="Reactive_Flushing_Strategy.cpp"

"$(INTDIR)\Reactive_Flushing_Strategy.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Reactive_Flushing_Strategy.obj" $(SOURCE)

SOURCE="Refcounted_ObjectKey.cpp"

"$(INTDIR)\Refcounted_ObjectKey.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Refcounted_ObjectKey.obj" $(SOURCE)

SOURCE="Remote_Invocation.cpp"

"$(INTDIR)\Remote_Invocation.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Remote_Invocation.obj" $(SOURCE)

SOURCE="Remote_Object_Proxy_Broker.cpp"

"$(INTDIR)\Remote_Object_Proxy_Broker.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Remote_Object_Proxy_Broker.obj" $(SOURCE)

SOURCE="Reply_Dispatcher.cpp"

"$(INTDIR)\Reply_Dispatcher.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Reply_Dispatcher.obj" $(SOURCE)

SOURCE="Request_Dispatcher.cpp"

"$(INTDIR)\Request_Dispatcher.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Request_Dispatcher.obj" $(SOURCE)

SOURCE="Resource_Factory.cpp"

"$(INTDIR)\Resource_Factory.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Resource_Factory.obj" $(SOURCE)

SOURCE="Resume_Handle.cpp"

"$(INTDIR)\Resume_Handle.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Resume_Handle.obj" $(SOURCE)

SOURCE="Server_Strategy_Factory.cpp"

"$(INTDIR)\Server_Strategy_Factory.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Server_Strategy_Factory.obj" $(SOURCE)

SOURCE="ServerRequestInterceptor_Adapter.cpp"

"$(INTDIR)\ServerRequestInterceptor_Adapter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ServerRequestInterceptor_Adapter.obj" $(SOURCE)

SOURCE="ServerRequestInterceptor_Adapter_Factory.cpp"

"$(INTDIR)\ServerRequestInterceptor_Adapter_Factory.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ServerRequestInterceptor_Adapter_Factory.obj" $(SOURCE)

SOURCE="Service_Callbacks.cpp"

"$(INTDIR)\Service_Callbacks.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Service_Callbacks.obj" $(SOURCE)

SOURCE="Service_Context.cpp"

"$(INTDIR)\Service_Context.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Service_Context.obj" $(SOURCE)

SOURCE="Service_Context_Handler.cpp"

"$(INTDIR)\Service_Context_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Service_Context_Handler.obj" $(SOURCE)

SOURCE="Service_Context_Handler_Registry.cpp"

"$(INTDIR)\Service_Context_Handler_Registry.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Service_Context_Handler_Registry.obj" $(SOURCE)

SOURCE="Services_Activate.cpp"

"$(INTDIR)\Services_Activate.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Services_Activate.obj" $(SOURCE)

SOURCE="ServicesC.cpp"

"$(INTDIR)\ServicesC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ServicesC.obj" $(SOURCE)

SOURCE="ShortSeqC.cpp"

"$(INTDIR)\ShortSeqC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ShortSeqC.obj" $(SOURCE)

SOURCE="String_Alloc.cpp"

"$(INTDIR)\String_Alloc.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\String_Alloc.obj" $(SOURCE)

SOURCE="StringSeqC.cpp"

"$(INTDIR)\StringSeqC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\StringSeqC.obj" $(SOURCE)

SOURCE="Stub.cpp"

"$(INTDIR)\Stub.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Stub.obj" $(SOURCE)

SOURCE="Stub_Factory.cpp"

"$(INTDIR)\Stub_Factory.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Stub_Factory.obj" $(SOURCE)

SOURCE="Synch_Invocation.cpp"

"$(INTDIR)\Synch_Invocation.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Synch_Invocation.obj" $(SOURCE)

SOURCE="Synch_Queued_Message.cpp"

"$(INTDIR)\Synch_Queued_Message.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Synch_Queued_Message.obj" $(SOURCE)

SOURCE="Synch_Reply_Dispatcher.cpp"

"$(INTDIR)\Synch_Reply_Dispatcher.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Synch_Reply_Dispatcher.obj" $(SOURCE)

SOURCE="SystemException.cpp"

"$(INTDIR)\SystemException.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\SystemException.obj" $(SOURCE)

SOURCE="Tagged_Components.cpp"

"$(INTDIR)\Tagged_Components.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Tagged_Components.obj" $(SOURCE)

SOURCE="Tagged_Profile.cpp"

"$(INTDIR)\Tagged_Profile.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Tagged_Profile.obj" $(SOURCE)

SOURCE="TAO_Internal.cpp"

"$(INTDIR)\TAO_Internal.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TAO_Internal.obj" $(SOURCE)

SOURCE="TAO_Server_Request.cpp"

"$(INTDIR)\TAO_Server_Request.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TAO_Server_Request.obj" $(SOURCE)

SOURCE="TAO_Singleton_Manager.cpp"

"$(INTDIR)\TAO_Singleton_Manager.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TAO_Singleton_Manager.obj" $(SOURCE)

SOURCE="TAOC.cpp"

"$(INTDIR)\TAOC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TAOC.obj" $(SOURCE)

SOURCE="target_specification.cpp"

"$(INTDIR)\target_specification.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\target_specification.obj" $(SOURCE)

SOURCE="Thread_Lane_Resources.cpp"

"$(INTDIR)\Thread_Lane_Resources.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Thread_Lane_Resources.obj" $(SOURCE)

SOURCE="Thread_Lane_Resources_Manager.cpp"

"$(INTDIR)\Thread_Lane_Resources_Manager.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Thread_Lane_Resources_Manager.obj" $(SOURCE)

SOURCE="Thread_Per_Connection_Handler.cpp"

"$(INTDIR)\Thread_Per_Connection_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Thread_Per_Connection_Handler.obj" $(SOURCE)

SOURCE="TimeBaseC.cpp"

"$(INTDIR)\TimeBaseC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TimeBaseC.obj" $(SOURCE)

SOURCE="Transport.cpp"

"$(INTDIR)\Transport.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Transport.obj" $(SOURCE)

SOURCE="Transport_Acceptor.cpp"

"$(INTDIR)\Transport_Acceptor.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Transport_Acceptor.obj" $(SOURCE)

SOURCE="Transport_Cache_Manager.cpp"

"$(INTDIR)\Transport_Cache_Manager.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Transport_Cache_Manager.obj" $(SOURCE)

SOURCE="Transport_Connector.cpp"

"$(INTDIR)\Transport_Connector.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Transport_Connector.obj" $(SOURCE)

SOURCE="Transport_Descriptor_Interface.cpp"

"$(INTDIR)\Transport_Descriptor_Interface.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Transport_Descriptor_Interface.obj" $(SOURCE)

SOURCE="Transport_Mux_Strategy.cpp"

"$(INTDIR)\Transport_Mux_Strategy.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Transport_Mux_Strategy.obj" $(SOURCE)

SOURCE="Transport_Queueing_Strategies.cpp"

"$(INTDIR)\Transport_Queueing_Strategies.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Transport_Queueing_Strategies.obj" $(SOURCE)

SOURCE="Transport_Selection_Guard.cpp"

"$(INTDIR)\Transport_Selection_Guard.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Transport_Selection_Guard.obj" $(SOURCE)

SOURCE="Transport_Timer.cpp"

"$(INTDIR)\Transport_Timer.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Transport_Timer.obj" $(SOURCE)

SOURCE="TSS_Resources.cpp"

"$(INTDIR)\TSS_Resources.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TSS_Resources.obj" $(SOURCE)

SOURCE="TypeCodeFactory_Adapter.cpp"

"$(INTDIR)\TypeCodeFactory_Adapter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TypeCodeFactory_Adapter.obj" $(SOURCE)

SOURCE="Typecode_typesC.cpp"

"$(INTDIR)\Typecode_typesC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Typecode_typesC.obj" $(SOURCE)

SOURCE="ULongLongSeqC.cpp"

"$(INTDIR)\ULongLongSeqC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ULongLongSeqC.obj" $(SOURCE)

SOURCE="ULongSeqC.cpp"

"$(INTDIR)\ULongSeqC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ULongSeqC.obj" $(SOURCE)

SOURCE="UserException.cpp"

"$(INTDIR)\UserException.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\UserException.obj" $(SOURCE)

SOURCE="UShortSeqC.cpp"

"$(INTDIR)\UShortSeqC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\UShortSeqC.obj" $(SOURCE)

SOURCE="Valuetype_Adapter.cpp"

"$(INTDIR)\Valuetype_Adapter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Valuetype_Adapter.obj" $(SOURCE)

SOURCE="Valuetype_Adapter_Factory.cpp"

"$(INTDIR)\Valuetype_Adapter_Factory.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Valuetype_Adapter_Factory.obj" $(SOURCE)

SOURCE="Wait_On_Leader_Follower.cpp"

"$(INTDIR)\Wait_On_Leader_Follower.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Wait_On_Leader_Follower.obj" $(SOURCE)

SOURCE="Wait_On_LF_No_Upcall.cpp"

"$(INTDIR)\Wait_On_LF_No_Upcall.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Wait_On_LF_No_Upcall.obj" $(SOURCE)

SOURCE="Wait_On_Reactor.cpp"

"$(INTDIR)\Wait_On_Reactor.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Wait_On_Reactor.obj" $(SOURCE)

SOURCE="Wait_On_Read.cpp"

"$(INTDIR)\Wait_On_Read.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Wait_On_Read.obj" $(SOURCE)

SOURCE="Wait_Strategy.cpp"

"$(INTDIR)\Wait_Strategy.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Wait_Strategy.obj" $(SOURCE)

SOURCE="WCharSeqC.cpp"

"$(INTDIR)\WCharSeqC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\WCharSeqC.obj" $(SOURCE)

SOURCE="WrongTransactionC.cpp"

"$(INTDIR)\WrongTransactionC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\WrongTransactionC.obj" $(SOURCE)

SOURCE="WStringSeqC.cpp"

"$(INTDIR)\WStringSeqC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\WStringSeqC.obj" $(SOURCE)

SOURCE="GUIResource_Factory.cpp"

"$(INTDIR)\GUIResource_Factory.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\GUIResource_Factory.obj" $(SOURCE)

SOURCE="ZIOP_Adapter.cpp"

"$(INTDIR)\ZIOP_Adapter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ZIOP_Adapter.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="GIOP.pidl"

InputPath=GIOP.pidl

"GIOPC.inl" "GIOPC.h" "GIOPS.h" "GIOPC.cpp" "AnyTypeCode\GIOPA.h" "AnyTypeCode\GIOPA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-GIOP_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h "$(InputPath)"
<<

SOURCE="CONV_FRAME.pidl"

InputPath=CONV_FRAME.pidl

"CONV_FRAMEC.h" "CONV_FRAMES.h" "CONV_FRAMEC.cpp" "AnyTypeCode\CONV_FRAMEA.h" "AnyTypeCode\CONV_FRAMEA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CONV_FRAME_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Current.pidl"

InputPath=Current.pidl

"CurrentC.h" "CurrentS.h" "CurrentC.cpp" "AnyTypeCode\CurrentA.h" "AnyTypeCode\CurrentA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Current_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="IIOP.pidl"

InputPath=IIOP.pidl

"IIOPC.h" "IIOPC.cpp" "AnyTypeCode\IIOPA.h" "AnyTypeCode\IIOPA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-IIOP_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="IIOP_Endpoints.pidl"

InputPath=IIOP_Endpoints.pidl

"IIOP_EndpointsC.h" "IIOP_EndpointsS.h" "IIOP_EndpointsC.cpp" "AnyTypeCode\IIOP_EndpointsA.h" "AnyTypeCode\IIOP_EndpointsA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-IIOP_Endpoints_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="IOP.pidl"

InputPath=IOP.pidl

"IOPC.h" "IOPS.h" "IOPC.cpp" "AnyTypeCode\IOPA.h" "AnyTypeCode\IOPA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-IOP_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Messaging_PolicyValue.pidl"

InputPath=Messaging_PolicyValue.pidl

"Messaging_PolicyValueC.h" "Messaging_PolicyValueS.h" "Messaging_PolicyValueC.cpp" "AnyTypeCode\Messaging_PolicyValueA.h" "AnyTypeCode\Messaging_PolicyValueA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Messaging_PolicyValue_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Messaging_SyncScope.pidl"

InputPath=Messaging_SyncScope.pidl

"Messaging_SyncScopeC.h" "Messaging_SyncScopeS.h" "Messaging_SyncScopeC.cpp" "AnyTypeCode\Messaging_SyncScopeA.h" "AnyTypeCode\Messaging_SyncScopeA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Messaging_SyncScope_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="ObjectIdList.pidl"

InputPath=ObjectIdList.pidl

"ObjectIdListC.h" "ObjectIdListS.h" "ObjectIdListC.cpp" "AnyTypeCode\ObjectIdListA.h" "AnyTypeCode\ObjectIdListA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-ObjectIdList_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="orb_types.pidl"

InputPath=orb_types.pidl

"orb_typesC.h" "orb_typesS.h" "orb_typesC.cpp" "AnyTypeCode\orb_typesA.h" "AnyTypeCode\orb_typesA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-orb_types_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="ParameterMode.pidl"

InputPath=ParameterMode.pidl

"ParameterModeC.h" "ParameterModeS.h" "ParameterModeC.cpp" "AnyTypeCode\ParameterModeA.h" "AnyTypeCode\ParameterModeA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-ParameterMode_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Policy_Forward.pidl"

InputPath=Policy_Forward.pidl

"Policy_ForwardC.h" "Policy_ForwardS.h" "Policy_ForwardC.cpp" "AnyTypeCode\Policy_ForwardA.h" "AnyTypeCode\Policy_ForwardA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Policy_Forward_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Policy_Manager.pidl"

InputPath=Policy_Manager.pidl

"Policy_ManagerC.h" "Policy_ManagerS.h" "Policy_ManagerC.cpp" "AnyTypeCode\Policy_ManagerA.h" "AnyTypeCode\Policy_ManagerA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Policy_Manager_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Policy_Current.pidl"

InputPath=Policy_Current.pidl

"Policy_CurrentC.h" "Policy_CurrentS.h" "Policy_CurrentC.cpp" "AnyTypeCode\Policy_CurrentA.h" "AnyTypeCode\Policy_CurrentA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Policy_Current_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="PI_Forward.pidl"

InputPath=PI_Forward.pidl

"PI_ForwardC.h" "PI_ForwardS.h" "PI_ForwardC.cpp" "AnyTypeCode\PI_ForwardA.h" "AnyTypeCode\PI_ForwardA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PI_Forward_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="PortableInterceptor.pidl"

InputPath=PortableInterceptor.pidl

"PortableInterceptorC.h" "PortableInterceptorS.h" "PortableInterceptorC.cpp" "AnyTypeCode\PortableInterceptorA.h" "AnyTypeCode\PortableInterceptorA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PortableInterceptor_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Services.pidl"

InputPath=Services.pidl

"ServicesC.h" "ServicesS.h" "ServicesC.cpp" "AnyTypeCode\ServicesA.h" "AnyTypeCode\ServicesA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Services_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="TAO.pidl"

InputPath=TAO.pidl

"TAOC.h" "TAOS.h" "TAOC.cpp" "AnyTypeCode\TAOA.h" "AnyTypeCode\TAOA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-TAO_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="TimeBase.pidl"

InputPath=TimeBase.pidl

"TimeBaseC.h" "TimeBaseS.h" "TimeBaseC.cpp" "AnyTypeCode\TimeBaseA.h" "AnyTypeCode\TimeBaseA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-TimeBase_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="BooleanSeq.pidl"

InputPath=BooleanSeq.pidl

"BooleanSeqC.h" "BooleanSeqS.h" "BooleanSeqC.cpp" "AnyTypeCode\BooleanSeqA.h" "AnyTypeCode\BooleanSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-BooleanSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="CharSeq.pidl"

InputPath=CharSeq.pidl

"CharSeqC.h" "CharSeqS.h" "CharSeqC.cpp" "AnyTypeCode\CharSeqA.h" "AnyTypeCode\CharSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CharSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="DoubleSeq.pidl"

InputPath=DoubleSeq.pidl

"DoubleSeqC.h" "DoubleSeqS.h" "DoubleSeqC.cpp" "AnyTypeCode\DoubleSeqA.h" "AnyTypeCode\DoubleSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-DoubleSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="FloatSeq.pidl"

InputPath=FloatSeq.pidl

"FloatSeqC.h" "FloatSeqS.h" "FloatSeqC.cpp" "AnyTypeCode\FloatSeqA.h" "AnyTypeCode\FloatSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-FloatSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="LongDoubleSeq.pidl"

InputPath=LongDoubleSeq.pidl

"LongDoubleSeqC.h" "LongDoubleSeqS.h" "LongDoubleSeqC.cpp" "AnyTypeCode\LongDoubleSeqA.h" "AnyTypeCode\LongDoubleSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-LongDoubleSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="LongLongSeq.pidl"

InputPath=LongLongSeq.pidl

"LongLongSeqC.h" "LongLongSeqS.h" "LongLongSeqC.cpp" "AnyTypeCode\LongLongSeqA.h" "AnyTypeCode\LongLongSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-LongLongSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="LongSeq.pidl"

InputPath=LongSeq.pidl

"LongSeqC.h" "LongSeqS.h" "LongSeqC.cpp" "AnyTypeCode\LongSeqA.h" "AnyTypeCode\LongSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-LongSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="OctetSeq.pidl"

InputPath=OctetSeq.pidl

"OctetSeqC.h" "OctetSeqS.h" "OctetSeqC.cpp" "AnyTypeCode\OctetSeqA.h" "AnyTypeCode\OctetSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-OctetSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="ShortSeq.pidl"

InputPath=ShortSeq.pidl

"ShortSeqC.h" "ShortSeqS.h" "ShortSeqC.cpp" "AnyTypeCode\ShortSeqA.h" "AnyTypeCode\ShortSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-ShortSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="StringSeq.pidl"

InputPath=StringSeq.pidl

"StringSeqC.h" "StringSeqS.h" "StringSeqC.cpp" "AnyTypeCode\StringSeqA.h" "AnyTypeCode\StringSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-StringSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="ULongLongSeq.pidl"

InputPath=ULongLongSeq.pidl

"ULongLongSeqC.h" "ULongLongSeqS.h" "ULongLongSeqC.cpp" "AnyTypeCode\ULongLongSeqA.h" "AnyTypeCode\ULongLongSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-ULongLongSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="ULongSeq.pidl"

InputPath=ULongSeq.pidl

"ULongSeqC.h" "ULongSeqS.h" "ULongSeqC.cpp" "AnyTypeCode\ULongSeqA.h" "AnyTypeCode\ULongSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-ULongSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="UShortSeq.pidl"

InputPath=UShortSeq.pidl

"UShortSeqC.h" "UShortSeqS.h" "UShortSeqC.cpp" "AnyTypeCode\UShortSeqA.h" "AnyTypeCode\UShortSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-UShortSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="WCharSeq.pidl"

InputPath=WCharSeq.pidl

"WCharSeqC.h" "WCharSeqS.h" "WCharSeqC.cpp" "AnyTypeCode\WCharSeqA.h" "AnyTypeCode\WCharSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-WCharSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="WStringSeq.pidl"

InputPath=WStringSeq.pidl

"WStringSeqC.h" "WStringSeqS.h" "WStringSeqC.cpp" "AnyTypeCode\WStringSeqA.h" "AnyTypeCode\WStringSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-WStringSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="GIOP.pidl"

InputPath=GIOP.pidl

"GIOPC.inl" "GIOPC.h" "GIOPS.h" "GIOPC.cpp" "AnyTypeCode\GIOPA.h" "AnyTypeCode\GIOPA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-GIOP_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h "$(InputPath)"
<<

SOURCE="CONV_FRAME.pidl"

InputPath=CONV_FRAME.pidl

"CONV_FRAMEC.h" "CONV_FRAMES.h" "CONV_FRAMEC.cpp" "AnyTypeCode\CONV_FRAMEA.h" "AnyTypeCode\CONV_FRAMEA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CONV_FRAME_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Current.pidl"

InputPath=Current.pidl

"CurrentC.h" "CurrentS.h" "CurrentC.cpp" "AnyTypeCode\CurrentA.h" "AnyTypeCode\CurrentA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Current_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="IIOP.pidl"

InputPath=IIOP.pidl

"IIOPC.h" "IIOPC.cpp" "AnyTypeCode\IIOPA.h" "AnyTypeCode\IIOPA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-IIOP_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="IIOP_Endpoints.pidl"

InputPath=IIOP_Endpoints.pidl

"IIOP_EndpointsC.h" "IIOP_EndpointsS.h" "IIOP_EndpointsC.cpp" "AnyTypeCode\IIOP_EndpointsA.h" "AnyTypeCode\IIOP_EndpointsA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-IIOP_Endpoints_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="IOP.pidl"

InputPath=IOP.pidl

"IOPC.h" "IOPS.h" "IOPC.cpp" "AnyTypeCode\IOPA.h" "AnyTypeCode\IOPA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-IOP_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Messaging_PolicyValue.pidl"

InputPath=Messaging_PolicyValue.pidl

"Messaging_PolicyValueC.h" "Messaging_PolicyValueS.h" "Messaging_PolicyValueC.cpp" "AnyTypeCode\Messaging_PolicyValueA.h" "AnyTypeCode\Messaging_PolicyValueA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Messaging_PolicyValue_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Messaging_SyncScope.pidl"

InputPath=Messaging_SyncScope.pidl

"Messaging_SyncScopeC.h" "Messaging_SyncScopeS.h" "Messaging_SyncScopeC.cpp" "AnyTypeCode\Messaging_SyncScopeA.h" "AnyTypeCode\Messaging_SyncScopeA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Messaging_SyncScope_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="ObjectIdList.pidl"

InputPath=ObjectIdList.pidl

"ObjectIdListC.h" "ObjectIdListS.h" "ObjectIdListC.cpp" "AnyTypeCode\ObjectIdListA.h" "AnyTypeCode\ObjectIdListA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-ObjectIdList_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="orb_types.pidl"

InputPath=orb_types.pidl

"orb_typesC.h" "orb_typesS.h" "orb_typesC.cpp" "AnyTypeCode\orb_typesA.h" "AnyTypeCode\orb_typesA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-orb_types_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="ParameterMode.pidl"

InputPath=ParameterMode.pidl

"ParameterModeC.h" "ParameterModeS.h" "ParameterModeC.cpp" "AnyTypeCode\ParameterModeA.h" "AnyTypeCode\ParameterModeA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-ParameterMode_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Policy_Forward.pidl"

InputPath=Policy_Forward.pidl

"Policy_ForwardC.h" "Policy_ForwardS.h" "Policy_ForwardC.cpp" "AnyTypeCode\Policy_ForwardA.h" "AnyTypeCode\Policy_ForwardA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Policy_Forward_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Policy_Manager.pidl"

InputPath=Policy_Manager.pidl

"Policy_ManagerC.h" "Policy_ManagerS.h" "Policy_ManagerC.cpp" "AnyTypeCode\Policy_ManagerA.h" "AnyTypeCode\Policy_ManagerA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Policy_Manager_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Policy_Current.pidl"

InputPath=Policy_Current.pidl

"Policy_CurrentC.h" "Policy_CurrentS.h" "Policy_CurrentC.cpp" "AnyTypeCode\Policy_CurrentA.h" "AnyTypeCode\Policy_CurrentA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Policy_Current_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="PI_Forward.pidl"

InputPath=PI_Forward.pidl

"PI_ForwardC.h" "PI_ForwardS.h" "PI_ForwardC.cpp" "AnyTypeCode\PI_ForwardA.h" "AnyTypeCode\PI_ForwardA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PI_Forward_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="PortableInterceptor.pidl"

InputPath=PortableInterceptor.pidl

"PortableInterceptorC.h" "PortableInterceptorS.h" "PortableInterceptorC.cpp" "AnyTypeCode\PortableInterceptorA.h" "AnyTypeCode\PortableInterceptorA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PortableInterceptor_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Services.pidl"

InputPath=Services.pidl

"ServicesC.h" "ServicesS.h" "ServicesC.cpp" "AnyTypeCode\ServicesA.h" "AnyTypeCode\ServicesA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Services_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="TAO.pidl"

InputPath=TAO.pidl

"TAOC.h" "TAOS.h" "TAOC.cpp" "AnyTypeCode\TAOA.h" "AnyTypeCode\TAOA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-TAO_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="TimeBase.pidl"

InputPath=TimeBase.pidl

"TimeBaseC.h" "TimeBaseS.h" "TimeBaseC.cpp" "AnyTypeCode\TimeBaseA.h" "AnyTypeCode\TimeBaseA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-TimeBase_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="BooleanSeq.pidl"

InputPath=BooleanSeq.pidl

"BooleanSeqC.h" "BooleanSeqS.h" "BooleanSeqC.cpp" "AnyTypeCode\BooleanSeqA.h" "AnyTypeCode\BooleanSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-BooleanSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="CharSeq.pidl"

InputPath=CharSeq.pidl

"CharSeqC.h" "CharSeqS.h" "CharSeqC.cpp" "AnyTypeCode\CharSeqA.h" "AnyTypeCode\CharSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CharSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="DoubleSeq.pidl"

InputPath=DoubleSeq.pidl

"DoubleSeqC.h" "DoubleSeqS.h" "DoubleSeqC.cpp" "AnyTypeCode\DoubleSeqA.h" "AnyTypeCode\DoubleSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-DoubleSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="FloatSeq.pidl"

InputPath=FloatSeq.pidl

"FloatSeqC.h" "FloatSeqS.h" "FloatSeqC.cpp" "AnyTypeCode\FloatSeqA.h" "AnyTypeCode\FloatSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-FloatSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="LongDoubleSeq.pidl"

InputPath=LongDoubleSeq.pidl

"LongDoubleSeqC.h" "LongDoubleSeqS.h" "LongDoubleSeqC.cpp" "AnyTypeCode\LongDoubleSeqA.h" "AnyTypeCode\LongDoubleSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-LongDoubleSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="LongLongSeq.pidl"

InputPath=LongLongSeq.pidl

"LongLongSeqC.h" "LongLongSeqS.h" "LongLongSeqC.cpp" "AnyTypeCode\LongLongSeqA.h" "AnyTypeCode\LongLongSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-LongLongSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="LongSeq.pidl"

InputPath=LongSeq.pidl

"LongSeqC.h" "LongSeqS.h" "LongSeqC.cpp" "AnyTypeCode\LongSeqA.h" "AnyTypeCode\LongSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-LongSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="OctetSeq.pidl"

InputPath=OctetSeq.pidl

"OctetSeqC.h" "OctetSeqS.h" "OctetSeqC.cpp" "AnyTypeCode\OctetSeqA.h" "AnyTypeCode\OctetSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-OctetSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="ShortSeq.pidl"

InputPath=ShortSeq.pidl

"ShortSeqC.h" "ShortSeqS.h" "ShortSeqC.cpp" "AnyTypeCode\ShortSeqA.h" "AnyTypeCode\ShortSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-ShortSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="StringSeq.pidl"

InputPath=StringSeq.pidl

"StringSeqC.h" "StringSeqS.h" "StringSeqC.cpp" "AnyTypeCode\StringSeqA.h" "AnyTypeCode\StringSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-StringSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="ULongLongSeq.pidl"

InputPath=ULongLongSeq.pidl

"ULongLongSeqC.h" "ULongLongSeqS.h" "ULongLongSeqC.cpp" "AnyTypeCode\ULongLongSeqA.h" "AnyTypeCode\ULongLongSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-ULongLongSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="ULongSeq.pidl"

InputPath=ULongSeq.pidl

"ULongSeqC.h" "ULongSeqS.h" "ULongSeqC.cpp" "AnyTypeCode\ULongSeqA.h" "AnyTypeCode\ULongSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-ULongSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="UShortSeq.pidl"

InputPath=UShortSeq.pidl

"UShortSeqC.h" "UShortSeqS.h" "UShortSeqC.cpp" "AnyTypeCode\UShortSeqA.h" "AnyTypeCode\UShortSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-UShortSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="WCharSeq.pidl"

InputPath=WCharSeq.pidl

"WCharSeqC.h" "WCharSeqS.h" "WCharSeqC.cpp" "AnyTypeCode\WCharSeqA.h" "AnyTypeCode\WCharSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-WCharSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="WStringSeq.pidl"

InputPath=WStringSeq.pidl

"WStringSeqC.h" "WStringSeqS.h" "WStringSeqC.cpp" "AnyTypeCode\WStringSeqA.h" "AnyTypeCode\WStringSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-WStringSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="GIOP.pidl"

InputPath=GIOP.pidl

"GIOPC.inl" "GIOPC.h" "GIOPS.h" "GIOPC.cpp" "AnyTypeCode\GIOPA.h" "AnyTypeCode\GIOPA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-GIOP_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h "$(InputPath)"
<<

SOURCE="CONV_FRAME.pidl"

InputPath=CONV_FRAME.pidl

"CONV_FRAMEC.h" "CONV_FRAMES.h" "CONV_FRAMEC.cpp" "AnyTypeCode\CONV_FRAMEA.h" "AnyTypeCode\CONV_FRAMEA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CONV_FRAME_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Current.pidl"

InputPath=Current.pidl

"CurrentC.h" "CurrentS.h" "CurrentC.cpp" "AnyTypeCode\CurrentA.h" "AnyTypeCode\CurrentA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Current_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="IIOP.pidl"

InputPath=IIOP.pidl

"IIOPC.h" "IIOPC.cpp" "AnyTypeCode\IIOPA.h" "AnyTypeCode\IIOPA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-IIOP_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="IIOP_Endpoints.pidl"

InputPath=IIOP_Endpoints.pidl

"IIOP_EndpointsC.h" "IIOP_EndpointsS.h" "IIOP_EndpointsC.cpp" "AnyTypeCode\IIOP_EndpointsA.h" "AnyTypeCode\IIOP_EndpointsA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-IIOP_Endpoints_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="IOP.pidl"

InputPath=IOP.pidl

"IOPC.h" "IOPS.h" "IOPC.cpp" "AnyTypeCode\IOPA.h" "AnyTypeCode\IOPA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-IOP_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Messaging_PolicyValue.pidl"

InputPath=Messaging_PolicyValue.pidl

"Messaging_PolicyValueC.h" "Messaging_PolicyValueS.h" "Messaging_PolicyValueC.cpp" "AnyTypeCode\Messaging_PolicyValueA.h" "AnyTypeCode\Messaging_PolicyValueA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Messaging_PolicyValue_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Messaging_SyncScope.pidl"

InputPath=Messaging_SyncScope.pidl

"Messaging_SyncScopeC.h" "Messaging_SyncScopeS.h" "Messaging_SyncScopeC.cpp" "AnyTypeCode\Messaging_SyncScopeA.h" "AnyTypeCode\Messaging_SyncScopeA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Messaging_SyncScope_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="ObjectIdList.pidl"

InputPath=ObjectIdList.pidl

"ObjectIdListC.h" "ObjectIdListS.h" "ObjectIdListC.cpp" "AnyTypeCode\ObjectIdListA.h" "AnyTypeCode\ObjectIdListA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-ObjectIdList_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="orb_types.pidl"

InputPath=orb_types.pidl

"orb_typesC.h" "orb_typesS.h" "orb_typesC.cpp" "AnyTypeCode\orb_typesA.h" "AnyTypeCode\orb_typesA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-orb_types_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="ParameterMode.pidl"

InputPath=ParameterMode.pidl

"ParameterModeC.h" "ParameterModeS.h" "ParameterModeC.cpp" "AnyTypeCode\ParameterModeA.h" "AnyTypeCode\ParameterModeA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-ParameterMode_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Policy_Forward.pidl"

InputPath=Policy_Forward.pidl

"Policy_ForwardC.h" "Policy_ForwardS.h" "Policy_ForwardC.cpp" "AnyTypeCode\Policy_ForwardA.h" "AnyTypeCode\Policy_ForwardA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Policy_Forward_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Policy_Manager.pidl"

InputPath=Policy_Manager.pidl

"Policy_ManagerC.h" "Policy_ManagerS.h" "Policy_ManagerC.cpp" "AnyTypeCode\Policy_ManagerA.h" "AnyTypeCode\Policy_ManagerA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Policy_Manager_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Policy_Current.pidl"

InputPath=Policy_Current.pidl

"Policy_CurrentC.h" "Policy_CurrentS.h" "Policy_CurrentC.cpp" "AnyTypeCode\Policy_CurrentA.h" "AnyTypeCode\Policy_CurrentA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Policy_Current_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="PI_Forward.pidl"

InputPath=PI_Forward.pidl

"PI_ForwardC.h" "PI_ForwardS.h" "PI_ForwardC.cpp" "AnyTypeCode\PI_ForwardA.h" "AnyTypeCode\PI_ForwardA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PI_Forward_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="PortableInterceptor.pidl"

InputPath=PortableInterceptor.pidl

"PortableInterceptorC.h" "PortableInterceptorS.h" "PortableInterceptorC.cpp" "AnyTypeCode\PortableInterceptorA.h" "AnyTypeCode\PortableInterceptorA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PortableInterceptor_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Services.pidl"

InputPath=Services.pidl

"ServicesC.h" "ServicesS.h" "ServicesC.cpp" "AnyTypeCode\ServicesA.h" "AnyTypeCode\ServicesA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Services_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="TAO.pidl"

InputPath=TAO.pidl

"TAOC.h" "TAOS.h" "TAOC.cpp" "AnyTypeCode\TAOA.h" "AnyTypeCode\TAOA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-TAO_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="TimeBase.pidl"

InputPath=TimeBase.pidl

"TimeBaseC.h" "TimeBaseS.h" "TimeBaseC.cpp" "AnyTypeCode\TimeBaseA.h" "AnyTypeCode\TimeBaseA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-TimeBase_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="BooleanSeq.pidl"

InputPath=BooleanSeq.pidl

"BooleanSeqC.h" "BooleanSeqS.h" "BooleanSeqC.cpp" "AnyTypeCode\BooleanSeqA.h" "AnyTypeCode\BooleanSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-BooleanSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="CharSeq.pidl"

InputPath=CharSeq.pidl

"CharSeqC.h" "CharSeqS.h" "CharSeqC.cpp" "AnyTypeCode\CharSeqA.h" "AnyTypeCode\CharSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CharSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="DoubleSeq.pidl"

InputPath=DoubleSeq.pidl

"DoubleSeqC.h" "DoubleSeqS.h" "DoubleSeqC.cpp" "AnyTypeCode\DoubleSeqA.h" "AnyTypeCode\DoubleSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-DoubleSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="FloatSeq.pidl"

InputPath=FloatSeq.pidl

"FloatSeqC.h" "FloatSeqS.h" "FloatSeqC.cpp" "AnyTypeCode\FloatSeqA.h" "AnyTypeCode\FloatSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-FloatSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="LongDoubleSeq.pidl"

InputPath=LongDoubleSeq.pidl

"LongDoubleSeqC.h" "LongDoubleSeqS.h" "LongDoubleSeqC.cpp" "AnyTypeCode\LongDoubleSeqA.h" "AnyTypeCode\LongDoubleSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-LongDoubleSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="LongLongSeq.pidl"

InputPath=LongLongSeq.pidl

"LongLongSeqC.h" "LongLongSeqS.h" "LongLongSeqC.cpp" "AnyTypeCode\LongLongSeqA.h" "AnyTypeCode\LongLongSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-LongLongSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="LongSeq.pidl"

InputPath=LongSeq.pidl

"LongSeqC.h" "LongSeqS.h" "LongSeqC.cpp" "AnyTypeCode\LongSeqA.h" "AnyTypeCode\LongSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-LongSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="OctetSeq.pidl"

InputPath=OctetSeq.pidl

"OctetSeqC.h" "OctetSeqS.h" "OctetSeqC.cpp" "AnyTypeCode\OctetSeqA.h" "AnyTypeCode\OctetSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-OctetSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="ShortSeq.pidl"

InputPath=ShortSeq.pidl

"ShortSeqC.h" "ShortSeqS.h" "ShortSeqC.cpp" "AnyTypeCode\ShortSeqA.h" "AnyTypeCode\ShortSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-ShortSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="StringSeq.pidl"

InputPath=StringSeq.pidl

"StringSeqC.h" "StringSeqS.h" "StringSeqC.cpp" "AnyTypeCode\StringSeqA.h" "AnyTypeCode\StringSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-StringSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="ULongLongSeq.pidl"

InputPath=ULongLongSeq.pidl

"ULongLongSeqC.h" "ULongLongSeqS.h" "ULongLongSeqC.cpp" "AnyTypeCode\ULongLongSeqA.h" "AnyTypeCode\ULongLongSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-ULongLongSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="ULongSeq.pidl"

InputPath=ULongSeq.pidl

"ULongSeqC.h" "ULongSeqS.h" "ULongSeqC.cpp" "AnyTypeCode\ULongSeqA.h" "AnyTypeCode\ULongSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-ULongSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="UShortSeq.pidl"

InputPath=UShortSeq.pidl

"UShortSeqC.h" "UShortSeqS.h" "UShortSeqC.cpp" "AnyTypeCode\UShortSeqA.h" "AnyTypeCode\UShortSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-UShortSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="WCharSeq.pidl"

InputPath=WCharSeq.pidl

"WCharSeqC.h" "WCharSeqS.h" "WCharSeqC.cpp" "AnyTypeCode\WCharSeqA.h" "AnyTypeCode\WCharSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-WCharSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="WStringSeq.pidl"

InputPath=WStringSeq.pidl

"WStringSeqC.h" "WStringSeqS.h" "WStringSeqC.cpp" "AnyTypeCode\WStringSeqA.h" "AnyTypeCode\WStringSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-WStringSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="GIOP.pidl"

InputPath=GIOP.pidl

"GIOPC.inl" "GIOPC.h" "GIOPS.h" "GIOPC.cpp" "AnyTypeCode\GIOPA.h" "AnyTypeCode\GIOPA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-GIOP_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h "$(InputPath)"
<<

SOURCE="CONV_FRAME.pidl"

InputPath=CONV_FRAME.pidl

"CONV_FRAMEC.h" "CONV_FRAMES.h" "CONV_FRAMEC.cpp" "AnyTypeCode\CONV_FRAMEA.h" "AnyTypeCode\CONV_FRAMEA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CONV_FRAME_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Current.pidl"

InputPath=Current.pidl

"CurrentC.h" "CurrentS.h" "CurrentC.cpp" "AnyTypeCode\CurrentA.h" "AnyTypeCode\CurrentA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Current_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="IIOP.pidl"

InputPath=IIOP.pidl

"IIOPC.h" "IIOPC.cpp" "AnyTypeCode\IIOPA.h" "AnyTypeCode\IIOPA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-IIOP_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="IIOP_Endpoints.pidl"

InputPath=IIOP_Endpoints.pidl

"IIOP_EndpointsC.h" "IIOP_EndpointsS.h" "IIOP_EndpointsC.cpp" "AnyTypeCode\IIOP_EndpointsA.h" "AnyTypeCode\IIOP_EndpointsA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-IIOP_Endpoints_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="IOP.pidl"

InputPath=IOP.pidl

"IOPC.h" "IOPS.h" "IOPC.cpp" "AnyTypeCode\IOPA.h" "AnyTypeCode\IOPA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-IOP_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Messaging_PolicyValue.pidl"

InputPath=Messaging_PolicyValue.pidl

"Messaging_PolicyValueC.h" "Messaging_PolicyValueS.h" "Messaging_PolicyValueC.cpp" "AnyTypeCode\Messaging_PolicyValueA.h" "AnyTypeCode\Messaging_PolicyValueA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Messaging_PolicyValue_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Messaging_SyncScope.pidl"

InputPath=Messaging_SyncScope.pidl

"Messaging_SyncScopeC.h" "Messaging_SyncScopeS.h" "Messaging_SyncScopeC.cpp" "AnyTypeCode\Messaging_SyncScopeA.h" "AnyTypeCode\Messaging_SyncScopeA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Messaging_SyncScope_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="ObjectIdList.pidl"

InputPath=ObjectIdList.pidl

"ObjectIdListC.h" "ObjectIdListS.h" "ObjectIdListC.cpp" "AnyTypeCode\ObjectIdListA.h" "AnyTypeCode\ObjectIdListA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-ObjectIdList_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="orb_types.pidl"

InputPath=orb_types.pidl

"orb_typesC.h" "orb_typesS.h" "orb_typesC.cpp" "AnyTypeCode\orb_typesA.h" "AnyTypeCode\orb_typesA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-orb_types_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="ParameterMode.pidl"

InputPath=ParameterMode.pidl

"ParameterModeC.h" "ParameterModeS.h" "ParameterModeC.cpp" "AnyTypeCode\ParameterModeA.h" "AnyTypeCode\ParameterModeA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-ParameterMode_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Policy_Forward.pidl"

InputPath=Policy_Forward.pidl

"Policy_ForwardC.h" "Policy_ForwardS.h" "Policy_ForwardC.cpp" "AnyTypeCode\Policy_ForwardA.h" "AnyTypeCode\Policy_ForwardA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Policy_Forward_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Policy_Manager.pidl"

InputPath=Policy_Manager.pidl

"Policy_ManagerC.h" "Policy_ManagerS.h" "Policy_ManagerC.cpp" "AnyTypeCode\Policy_ManagerA.h" "AnyTypeCode\Policy_ManagerA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Policy_Manager_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Policy_Current.pidl"

InputPath=Policy_Current.pidl

"Policy_CurrentC.h" "Policy_CurrentS.h" "Policy_CurrentC.cpp" "AnyTypeCode\Policy_CurrentA.h" "AnyTypeCode\Policy_CurrentA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Policy_Current_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="PI_Forward.pidl"

InputPath=PI_Forward.pidl

"PI_ForwardC.h" "PI_ForwardS.h" "PI_ForwardC.cpp" "AnyTypeCode\PI_ForwardA.h" "AnyTypeCode\PI_ForwardA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PI_Forward_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="PortableInterceptor.pidl"

InputPath=PortableInterceptor.pidl

"PortableInterceptorC.h" "PortableInterceptorS.h" "PortableInterceptorC.cpp" "AnyTypeCode\PortableInterceptorA.h" "AnyTypeCode\PortableInterceptorA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PortableInterceptor_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="Services.pidl"

InputPath=Services.pidl

"ServicesC.h" "ServicesS.h" "ServicesC.cpp" "AnyTypeCode\ServicesA.h" "AnyTypeCode\ServicesA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Services_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="TAO.pidl"

InputPath=TAO.pidl

"TAOC.h" "TAOS.h" "TAOC.cpp" "AnyTypeCode\TAOA.h" "AnyTypeCode\TAOA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-TAO_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="TimeBase.pidl"

InputPath=TimeBase.pidl

"TimeBaseC.h" "TimeBaseS.h" "TimeBaseC.cpp" "AnyTypeCode\TimeBaseA.h" "AnyTypeCode\TimeBaseA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-TimeBase_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci "$(InputPath)"
<<

SOURCE="BooleanSeq.pidl"

InputPath=BooleanSeq.pidl

"BooleanSeqC.h" "BooleanSeqS.h" "BooleanSeqC.cpp" "AnyTypeCode\BooleanSeqA.h" "AnyTypeCode\BooleanSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-BooleanSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="CharSeq.pidl"

InputPath=CharSeq.pidl

"CharSeqC.h" "CharSeqS.h" "CharSeqC.cpp" "AnyTypeCode\CharSeqA.h" "AnyTypeCode\CharSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CharSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="DoubleSeq.pidl"

InputPath=DoubleSeq.pidl

"DoubleSeqC.h" "DoubleSeqS.h" "DoubleSeqC.cpp" "AnyTypeCode\DoubleSeqA.h" "AnyTypeCode\DoubleSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-DoubleSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="FloatSeq.pidl"

InputPath=FloatSeq.pidl

"FloatSeqC.h" "FloatSeqS.h" "FloatSeqC.cpp" "AnyTypeCode\FloatSeqA.h" "AnyTypeCode\FloatSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-FloatSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="LongDoubleSeq.pidl"

InputPath=LongDoubleSeq.pidl

"LongDoubleSeqC.h" "LongDoubleSeqS.h" "LongDoubleSeqC.cpp" "AnyTypeCode\LongDoubleSeqA.h" "AnyTypeCode\LongDoubleSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-LongDoubleSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="LongLongSeq.pidl"

InputPath=LongLongSeq.pidl

"LongLongSeqC.h" "LongLongSeqS.h" "LongLongSeqC.cpp" "AnyTypeCode\LongLongSeqA.h" "AnyTypeCode\LongLongSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-LongLongSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="LongSeq.pidl"

InputPath=LongSeq.pidl

"LongSeqC.h" "LongSeqS.h" "LongSeqC.cpp" "AnyTypeCode\LongSeqA.h" "AnyTypeCode\LongSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-LongSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="OctetSeq.pidl"

InputPath=OctetSeq.pidl

"OctetSeqC.h" "OctetSeqS.h" "OctetSeqC.cpp" "AnyTypeCode\OctetSeqA.h" "AnyTypeCode\OctetSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-OctetSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="ShortSeq.pidl"

InputPath=ShortSeq.pidl

"ShortSeqC.h" "ShortSeqS.h" "ShortSeqC.cpp" "AnyTypeCode\ShortSeqA.h" "AnyTypeCode\ShortSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-ShortSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="StringSeq.pidl"

InputPath=StringSeq.pidl

"StringSeqC.h" "StringSeqS.h" "StringSeqC.cpp" "AnyTypeCode\StringSeqA.h" "AnyTypeCode\StringSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-StringSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="ULongLongSeq.pidl"

InputPath=ULongLongSeq.pidl

"ULongLongSeqC.h" "ULongLongSeqS.h" "ULongLongSeqC.cpp" "AnyTypeCode\ULongLongSeqA.h" "AnyTypeCode\ULongLongSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-ULongLongSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="ULongSeq.pidl"

InputPath=ULongSeq.pidl

"ULongSeqC.h" "ULongSeqS.h" "ULongSeqC.cpp" "AnyTypeCode\ULongSeqA.h" "AnyTypeCode\ULongSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-ULongSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="UShortSeq.pidl"

InputPath=UShortSeq.pidl

"UShortSeqC.h" "UShortSeqS.h" "UShortSeqC.cpp" "AnyTypeCode\UShortSeqA.h" "AnyTypeCode\UShortSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-UShortSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="WCharSeq.pidl"

InputPath=WCharSeq.pidl

"WCharSeqC.h" "WCharSeqS.h" "WCharSeqC.cpp" "AnyTypeCode\WCharSeqA.h" "AnyTypeCode\WCharSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-WCharSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

SOURCE="WStringSeq.pidl"

InputPath=WStringSeq.pidl

"WStringSeqC.h" "WStringSeqS.h" "WStringSeqC.cpp" "AnyTypeCode\WStringSeqA.h" "AnyTypeCode\WStringSeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-WStringSeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GA -Gp -Gd -oA AnyTypeCode -Wb,export_macro=TAO_Export -Wb,export_include=tao/TAO_Export.h -Wb,anyop_export_macro=TAO_AnyTypeCode_Export -Wb,anyop_export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -Sci -Gse "$(InputPath)"
<<

!ENDIF

SOURCE="tao.rc"

"$(INTDIR)\tao.res" : $(SOURCE)
	$(RSC) /l 0x409 /fo"$(INTDIR)\tao.res" /d NDEBUG /d WIN64 /d _CRT_SECURE_NO_WARNINGS /d _CRT_SECURE_NO_DEPRECATE /d _CRT_NONSTDC_NO_DEPRECATE /d TAO_HAS_VALUETYPE_OUT_INDIRECTION /i "..\.." /i ".." $(SOURCE)



!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.TAO.dep")
	@echo Using "Makefile.TAO.dep"
!ELSE
	@echo Warning: cannot find "Makefile.TAO.dep"
!ENDIF
!ENDIF

