#include "Idn.h"

#include <vector>

#ifdef _WIN32

#include <Windows.h>

#else

#include <ctype.h>
#include <algorithm>

#endif

namespace IDN
{

#ifdef _WIN32

#pragma comment(lib, "Normaliz.lib")

std::string hostnameToASCII(const std::wstring& wideString)
{
    std::vector<wchar_t> output(1024, 0);
    auto length = IdnToAscii(IDN_ALLOW_UNASSIGNED, wideString.data(),
        static_cast<int>(wideString.length()), output.data(), static_cast<int>(output.size()));
    if (length == 0)
    {
        throw std::runtime_error("Could convert current host name to IDN ascii. GetLastError="
            + std::to_string(GetLastError()));
    }
    output.resize(length);
    // it is safe here to truncate 
    return std::string(output.begin(), output.end());
}

#else

std::string hostnameToASCII(const std::wstring& wideString)
{
    // Only ascii characters are expected
    std::string out;
    out.reserve(wideString.length());
    /// @note NGP advertises its hostname in uppercase
    std::transform(wideString.begin(), wideString.end(), std::back_inserter(out), toupper);
    return out;
}

#endif

}
