// $Id: Smart_Proxies.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/SmartProxies/Smart_Proxies.h"

ACE_RCSID(SmartProxies,
          Smart_Proxies,
          "$Id: Smart_Proxies.cpp 14 2007-02-01 15:49:12Z mitza $")

#if !defined (__ACE_INLINE__)
#include "tao/SmartProxies/Smart_Proxies.inl"
#endif /* defined INLINE */

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Smart_Proxy_Base::~TAO_Smart_Proxy_Base (void)
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
