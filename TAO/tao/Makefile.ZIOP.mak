# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.ZIOP.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "ZIOP\ZIOPC.h" "ZIOP\ZIOPS.h" "ZIOP\ZIOPC.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\ZIOP\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_ZIOPd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_ZIOP_BUILD_DLL -f "Makefile.ZIOP.dep" "ZIOP\ZIOPC.cpp" "ZIOP\ZIOP_Stub.cpp" "ZIOP\ZIOP_PolicyFactory.cpp" "ZIOP\ZIOP_Policy_Validator.cpp" "ZIOP\ZIOP_Stub_Factory.cpp" "ZIOP\ZIOP_Policy_i.cpp" "ZIOP\ZIOP.cpp" "ZIOP\ZIOP_ORBInitializer.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_ZIOPd.pdb"
	-@del /f/q "..\..\lib\TAO_ZIOPd.dll"
	-@del /f/q "$(OUTDIR)\TAO_ZIOPd.lib"
	-@del /f/q "$(OUTDIR)\TAO_ZIOPd.exp"
	-@del /f/q "$(OUTDIR)\TAO_ZIOPd.ilk"
	-@del /f/q "ZIOP\ZIOPC.h"
	-@del /f/q "ZIOP\ZIOPS.h"
	-@del /f/q "ZIOP\ZIOPC.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\ZIOP\$(NULL)" mkdir "Debug\ZIOP"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_ZIOP_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_Compressiond.lib TAO_AnyTypeCoded.lib TAO_CodecFactoryd.lib TAO_PId.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\TAO_ZIOPd.pdb" /machine:IA64 /out:"..\..\lib\TAO_ZIOPd.dll" /implib:"$(OUTDIR)\TAO_ZIOPd.lib"
LINK32_OBJS= \
	"$(INTDIR)\ZIOP\TAO_ZIOP.res" \
	"$(INTDIR)\ZIOP\ZIOPC.obj" \
	"$(INTDIR)\ZIOP\ZIOP_Stub.obj" \
	"$(INTDIR)\ZIOP\ZIOP_PolicyFactory.obj" \
	"$(INTDIR)\ZIOP\ZIOP_Policy_Validator.obj" \
	"$(INTDIR)\ZIOP\ZIOP_Stub_Factory.obj" \
	"$(INTDIR)\ZIOP\ZIOP_Policy_i.obj" \
	"$(INTDIR)\ZIOP\ZIOP.obj" \
	"$(INTDIR)\ZIOP\ZIOP_ORBInitializer.obj"

"..\..\lib\TAO_ZIOPd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_ZIOPd.dll.manifest" mt.exe -manifest "..\..\lib\TAO_ZIOPd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\ZIOP\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_ZIOP.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_ZIOP_BUILD_DLL -f "Makefile.ZIOP.dep" "ZIOP\ZIOPC.cpp" "ZIOP\ZIOP_Stub.cpp" "ZIOP\ZIOP_PolicyFactory.cpp" "ZIOP\ZIOP_Policy_Validator.cpp" "ZIOP\ZIOP_Stub_Factory.cpp" "ZIOP\ZIOP_Policy_i.cpp" "ZIOP\ZIOP.cpp" "ZIOP\ZIOP_ORBInitializer.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\TAO_ZIOP.dll"
	-@del /f/q "$(OUTDIR)\TAO_ZIOP.lib"
	-@del /f/q "$(OUTDIR)\TAO_ZIOP.exp"
	-@del /f/q "$(OUTDIR)\TAO_ZIOP.ilk"
	-@del /f/q "ZIOP\ZIOPC.h"
	-@del /f/q "ZIOP\ZIOPS.h"
	-@del /f/q "ZIOP\ZIOPC.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\ZIOP\$(NULL)" mkdir "Release\ZIOP"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_ZIOP_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_Compression.lib TAO_AnyTypeCode.lib TAO_CodecFactory.lib TAO_PI.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\TAO_ZIOP.dll" /implib:"$(OUTDIR)\TAO_ZIOP.lib"
LINK32_OBJS= \
	"$(INTDIR)\ZIOP\TAO_ZIOP.res" \
	"$(INTDIR)\ZIOP\ZIOPC.obj" \
	"$(INTDIR)\ZIOP\ZIOP_Stub.obj" \
	"$(INTDIR)\ZIOP\ZIOP_PolicyFactory.obj" \
	"$(INTDIR)\ZIOP\ZIOP_Policy_Validator.obj" \
	"$(INTDIR)\ZIOP\ZIOP_Stub_Factory.obj" \
	"$(INTDIR)\ZIOP\ZIOP_Policy_i.obj" \
	"$(INTDIR)\ZIOP\ZIOP.obj" \
	"$(INTDIR)\ZIOP\ZIOP_ORBInitializer.obj"

"..\..\lib\TAO_ZIOP.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_ZIOP.dll.manifest" mt.exe -manifest "..\..\lib\TAO_ZIOP.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\ZIOP\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_ZIOPsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.ZIOP.dep" "ZIOP\ZIOPC.cpp" "ZIOP\ZIOP_Stub.cpp" "ZIOP\ZIOP_PolicyFactory.cpp" "ZIOP\ZIOP_Policy_Validator.cpp" "ZIOP\ZIOP_Stub_Factory.cpp" "ZIOP\ZIOP_Policy_i.cpp" "ZIOP\ZIOP.cpp" "ZIOP\ZIOP_ORBInitializer.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_ZIOPsd.lib"
	-@del /f/q "$(OUTDIR)\TAO_ZIOPsd.exp"
	-@del /f/q "$(OUTDIR)\TAO_ZIOPsd.ilk"
	-@del /f/q "..\..\lib\TAO_ZIOPsd.pdb"
	-@del /f/q "ZIOP\ZIOPC.h"
	-@del /f/q "ZIOP\ZIOPS.h"
	-@del /f/q "ZIOP\ZIOPC.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\ZIOP\$(NULL)" mkdir "Static_Debug\ZIOP"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"..\..\lib\TAO_ZIOPsd.pdb" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_ZIOPsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\ZIOP\ZIOPC.obj" \
	"$(INTDIR)\ZIOP\ZIOP_Stub.obj" \
	"$(INTDIR)\ZIOP\ZIOP_PolicyFactory.obj" \
	"$(INTDIR)\ZIOP\ZIOP_Policy_Validator.obj" \
	"$(INTDIR)\ZIOP\ZIOP_Stub_Factory.obj" \
	"$(INTDIR)\ZIOP\ZIOP_Policy_i.obj" \
	"$(INTDIR)\ZIOP\ZIOP.obj" \
	"$(INTDIR)\ZIOP\ZIOP_ORBInitializer.obj"

"$(OUTDIR)\TAO_ZIOPsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_ZIOPsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_ZIOPsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\ZIOP\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_ZIOPs.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.ZIOP.dep" "ZIOP\ZIOPC.cpp" "ZIOP\ZIOP_Stub.cpp" "ZIOP\ZIOP_PolicyFactory.cpp" "ZIOP\ZIOP_Policy_Validator.cpp" "ZIOP\ZIOP_Stub_Factory.cpp" "ZIOP\ZIOP_Policy_i.cpp" "ZIOP\ZIOP.cpp" "ZIOP\ZIOP_ORBInitializer.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_ZIOPs.lib"
	-@del /f/q "$(OUTDIR)\TAO_ZIOPs.exp"
	-@del /f/q "$(OUTDIR)\TAO_ZIOPs.ilk"
	-@del /f/q "ZIOP\ZIOPC.h"
	-@del /f/q "ZIOP\ZIOPS.h"
	-@del /f/q "ZIOP\ZIOPC.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\ZIOP\$(NULL)" mkdir "Static_Release\ZIOP"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_ZIOPs.lib"
LINK32_OBJS= \
	"$(INTDIR)\ZIOP\ZIOPC.obj" \
	"$(INTDIR)\ZIOP\ZIOP_Stub.obj" \
	"$(INTDIR)\ZIOP\ZIOP_PolicyFactory.obj" \
	"$(INTDIR)\ZIOP\ZIOP_Policy_Validator.obj" \
	"$(INTDIR)\ZIOP\ZIOP_Stub_Factory.obj" \
	"$(INTDIR)\ZIOP\ZIOP_Policy_i.obj" \
	"$(INTDIR)\ZIOP\ZIOP.obj" \
	"$(INTDIR)\ZIOP\ZIOP_ORBInitializer.obj"

"$(OUTDIR)\TAO_ZIOPs.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_ZIOPs.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_ZIOPs.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.ZIOP.dep")
!INCLUDE "Makefile.ZIOP.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="ZIOP\ZIOPC.cpp"

"$(INTDIR)\ZIOP\ZIOPC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ZIOP\$(NULL)" mkdir "$(INTDIR)\ZIOP\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ZIOP\ZIOPC.obj" $(SOURCE)

SOURCE="ZIOP\ZIOP_Stub.cpp"

"$(INTDIR)\ZIOP\ZIOP_Stub.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ZIOP\$(NULL)" mkdir "$(INTDIR)\ZIOP\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ZIOP\ZIOP_Stub.obj" $(SOURCE)

SOURCE="ZIOP\ZIOP_PolicyFactory.cpp"

"$(INTDIR)\ZIOP\ZIOP_PolicyFactory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ZIOP\$(NULL)" mkdir "$(INTDIR)\ZIOP\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ZIOP\ZIOP_PolicyFactory.obj" $(SOURCE)

SOURCE="ZIOP\ZIOP_Policy_Validator.cpp"

"$(INTDIR)\ZIOP\ZIOP_Policy_Validator.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ZIOP\$(NULL)" mkdir "$(INTDIR)\ZIOP\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ZIOP\ZIOP_Policy_Validator.obj" $(SOURCE)

SOURCE="ZIOP\ZIOP_Stub_Factory.cpp"

"$(INTDIR)\ZIOP\ZIOP_Stub_Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ZIOP\$(NULL)" mkdir "$(INTDIR)\ZIOP\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ZIOP\ZIOP_Stub_Factory.obj" $(SOURCE)

SOURCE="ZIOP\ZIOP_Policy_i.cpp"

"$(INTDIR)\ZIOP\ZIOP_Policy_i.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ZIOP\$(NULL)" mkdir "$(INTDIR)\ZIOP\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ZIOP\ZIOP_Policy_i.obj" $(SOURCE)

SOURCE="ZIOP\ZIOP.cpp"

"$(INTDIR)\ZIOP\ZIOP.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ZIOP\$(NULL)" mkdir "$(INTDIR)\ZIOP\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ZIOP\ZIOP.obj" $(SOURCE)

SOURCE="ZIOP\ZIOP_ORBInitializer.cpp"

"$(INTDIR)\ZIOP\ZIOP_ORBInitializer.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\ZIOP\$(NULL)" mkdir "$(INTDIR)\ZIOP\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ZIOP\ZIOP_ORBInitializer.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="ZIOP\ZIOP.pidl"

InputPath=ZIOP\ZIOP.pidl

"ZIOP\ZIOPC.h" "ZIOP\ZIOPS.h" "ZIOP\ZIOPC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-ZIOP_ZIOP_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_ZIOP_Export -Wb,export_include=tao/ZIOP/ziop_export.h -Wb,include_guard=TAO_ZIOP_SAFE_INCLUDE -Wb,safe_include=tao/ZIOP/ZIOP.h -o ZIOP "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="ZIOP\ZIOP.pidl"

InputPath=ZIOP\ZIOP.pidl

"ZIOP\ZIOPC.h" "ZIOP\ZIOPS.h" "ZIOP\ZIOPC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-ZIOP_ZIOP_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_ZIOP_Export -Wb,export_include=tao/ZIOP/ziop_export.h -Wb,include_guard=TAO_ZIOP_SAFE_INCLUDE -Wb,safe_include=tao/ZIOP/ZIOP.h -o ZIOP "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="ZIOP\ZIOP.pidl"

InputPath=ZIOP\ZIOP.pidl

"ZIOP\ZIOPC.h" "ZIOP\ZIOPS.h" "ZIOP\ZIOPC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-ZIOP_ZIOP_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_ZIOP_Export -Wb,export_include=tao/ZIOP/ziop_export.h -Wb,include_guard=TAO_ZIOP_SAFE_INCLUDE -Wb,safe_include=tao/ZIOP/ZIOP.h -o ZIOP "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="ZIOP\ZIOP.pidl"

InputPath=ZIOP\ZIOP.pidl

"ZIOP\ZIOPC.h" "ZIOP\ZIOPS.h" "ZIOP\ZIOPC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-ZIOP_ZIOP_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_ZIOP_Export -Wb,export_include=tao/ZIOP/ziop_export.h -Wb,include_guard=TAO_ZIOP_SAFE_INCLUDE -Wb,safe_include=tao/ZIOP/ZIOP.h -o ZIOP "$(InputPath)"
<<

!ENDIF

SOURCE="ZIOP\TAO_ZIOP.rc"

"$(INTDIR)\ZIOP\TAO_ZIOP.res" : $(SOURCE)
	@if not exist "$(INTDIR)\ZIOP\$(NULL)" mkdir "$(INTDIR)\ZIOP\"
	$(RSC) /l 0x409 /fo"$(INTDIR)\ZIOP\TAO_ZIOP.res" /d NDEBUG /d WIN64 /d _CRT_SECURE_NO_WARNINGS /d _CRT_SECURE_NO_DEPRECATE /d _CRT_NONSTDC_NO_DEPRECATE /i "..\.." /i ".." $(SOURCE)



!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.ZIOP.dep")
	@echo Using "Makefile.ZIOP.dep"
!ELSE
	@echo Warning: cannot find "Makefile.ZIOP.dep"
!ENDIF
!ENDIF

