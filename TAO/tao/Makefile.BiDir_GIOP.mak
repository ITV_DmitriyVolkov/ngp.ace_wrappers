# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.BiDir_GIOP.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "BiDir_GIOP\BiDirPolicyC.h" "BiDir_GIOP\BiDirPolicyS.h" "BiDir_GIOP\BiDirPolicyC.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\BiDir_GIOP\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_BiDirGIOPd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_BIDIRGIOP_BUILD_DLL -f "Makefile.BiDir_GIOP.dep" "BiDir_GIOP\BiDirPolicyC.cpp" "BiDir_GIOP\BiDirGIOP.cpp" "BiDir_GIOP\BiDirPolicy_Validator.cpp" "BiDir_GIOP\BiDir_Service_Context_Handler.cpp" "BiDir_GIOP\BiDir_Policy_i.cpp" "BiDir_GIOP\BiDir_PolicyFactory.cpp" "BiDir_GIOP\BiDir_ORBInitializer.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_BiDirGIOPd.pdb"
	-@del /f/q "..\..\lib\TAO_BiDirGIOPd.dll"
	-@del /f/q "$(OUTDIR)\TAO_BiDirGIOPd.lib"
	-@del /f/q "$(OUTDIR)\TAO_BiDirGIOPd.exp"
	-@del /f/q "$(OUTDIR)\TAO_BiDirGIOPd.ilk"
	-@del /f/q "BiDir_GIOP\BiDirPolicyC.h"
	-@del /f/q "BiDir_GIOP\BiDirPolicyS.h"
	-@del /f/q "BiDir_GIOP\BiDirPolicyC.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\BiDir_GIOP\$(NULL)" mkdir "Debug\BiDir_GIOP"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_BIDIRGIOP_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_CodecFactoryd.lib TAO_PId.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\TAO_BiDirGIOPd.pdb" /machine:IA64 /out:"..\..\lib\TAO_BiDirGIOPd.dll" /implib:"$(OUTDIR)\TAO_BiDirGIOPd.lib"
LINK32_OBJS= \
	"$(INTDIR)\BiDir_GIOP\TAO_BiDir_GIOP.res" \
	"$(INTDIR)\BiDir_GIOP\BiDirPolicyC.obj" \
	"$(INTDIR)\BiDir_GIOP\BiDirGIOP.obj" \
	"$(INTDIR)\BiDir_GIOP\BiDirPolicy_Validator.obj" \
	"$(INTDIR)\BiDir_GIOP\BiDir_Service_Context_Handler.obj" \
	"$(INTDIR)\BiDir_GIOP\BiDir_Policy_i.obj" \
	"$(INTDIR)\BiDir_GIOP\BiDir_PolicyFactory.obj" \
	"$(INTDIR)\BiDir_GIOP\BiDir_ORBInitializer.obj"

"..\..\lib\TAO_BiDirGIOPd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_BiDirGIOPd.dll.manifest" mt.exe -manifest "..\..\lib\TAO_BiDirGIOPd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\BiDir_GIOP\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_BiDirGIOP.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_BIDIRGIOP_BUILD_DLL -f "Makefile.BiDir_GIOP.dep" "BiDir_GIOP\BiDirPolicyC.cpp" "BiDir_GIOP\BiDirGIOP.cpp" "BiDir_GIOP\BiDirPolicy_Validator.cpp" "BiDir_GIOP\BiDir_Service_Context_Handler.cpp" "BiDir_GIOP\BiDir_Policy_i.cpp" "BiDir_GIOP\BiDir_PolicyFactory.cpp" "BiDir_GIOP\BiDir_ORBInitializer.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\TAO_BiDirGIOP.dll"
	-@del /f/q "$(OUTDIR)\TAO_BiDirGIOP.lib"
	-@del /f/q "$(OUTDIR)\TAO_BiDirGIOP.exp"
	-@del /f/q "$(OUTDIR)\TAO_BiDirGIOP.ilk"
	-@del /f/q "BiDir_GIOP\BiDirPolicyC.h"
	-@del /f/q "BiDir_GIOP\BiDirPolicyS.h"
	-@del /f/q "BiDir_GIOP\BiDirPolicyC.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\BiDir_GIOP\$(NULL)" mkdir "Release\BiDir_GIOP"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_BIDIRGIOP_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_CodecFactory.lib TAO_PI.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\TAO_BiDirGIOP.dll" /implib:"$(OUTDIR)\TAO_BiDirGIOP.lib"
LINK32_OBJS= \
	"$(INTDIR)\BiDir_GIOP\TAO_BiDir_GIOP.res" \
	"$(INTDIR)\BiDir_GIOP\BiDirPolicyC.obj" \
	"$(INTDIR)\BiDir_GIOP\BiDirGIOP.obj" \
	"$(INTDIR)\BiDir_GIOP\BiDirPolicy_Validator.obj" \
	"$(INTDIR)\BiDir_GIOP\BiDir_Service_Context_Handler.obj" \
	"$(INTDIR)\BiDir_GIOP\BiDir_Policy_i.obj" \
	"$(INTDIR)\BiDir_GIOP\BiDir_PolicyFactory.obj" \
	"$(INTDIR)\BiDir_GIOP\BiDir_ORBInitializer.obj"

"..\..\lib\TAO_BiDirGIOP.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_BiDirGIOP.dll.manifest" mt.exe -manifest "..\..\lib\TAO_BiDirGIOP.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\BiDir_GIOP\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_BiDirGIOPsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.BiDir_GIOP.dep" "BiDir_GIOP\BiDirPolicyC.cpp" "BiDir_GIOP\BiDirGIOP.cpp" "BiDir_GIOP\BiDirPolicy_Validator.cpp" "BiDir_GIOP\BiDir_Service_Context_Handler.cpp" "BiDir_GIOP\BiDir_Policy_i.cpp" "BiDir_GIOP\BiDir_PolicyFactory.cpp" "BiDir_GIOP\BiDir_ORBInitializer.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_BiDirGIOPsd.lib"
	-@del /f/q "$(OUTDIR)\TAO_BiDirGIOPsd.exp"
	-@del /f/q "$(OUTDIR)\TAO_BiDirGIOPsd.ilk"
	-@del /f/q "..\..\lib\TAO_BiDirGIOPsd.pdb"
	-@del /f/q "BiDir_GIOP\BiDirPolicyC.h"
	-@del /f/q "BiDir_GIOP\BiDirPolicyS.h"
	-@del /f/q "BiDir_GIOP\BiDirPolicyC.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\BiDir_GIOP\$(NULL)" mkdir "Static_Debug\BiDir_GIOP"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"..\..\lib\TAO_BiDirGIOPsd.pdb" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_BiDirGIOPsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\BiDir_GIOP\BiDirPolicyC.obj" \
	"$(INTDIR)\BiDir_GIOP\BiDirGIOP.obj" \
	"$(INTDIR)\BiDir_GIOP\BiDirPolicy_Validator.obj" \
	"$(INTDIR)\BiDir_GIOP\BiDir_Service_Context_Handler.obj" \
	"$(INTDIR)\BiDir_GIOP\BiDir_Policy_i.obj" \
	"$(INTDIR)\BiDir_GIOP\BiDir_PolicyFactory.obj" \
	"$(INTDIR)\BiDir_GIOP\BiDir_ORBInitializer.obj"

"$(OUTDIR)\TAO_BiDirGIOPsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_BiDirGIOPsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_BiDirGIOPsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\BiDir_GIOP\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_BiDirGIOPs.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.BiDir_GIOP.dep" "BiDir_GIOP\BiDirPolicyC.cpp" "BiDir_GIOP\BiDirGIOP.cpp" "BiDir_GIOP\BiDirPolicy_Validator.cpp" "BiDir_GIOP\BiDir_Service_Context_Handler.cpp" "BiDir_GIOP\BiDir_Policy_i.cpp" "BiDir_GIOP\BiDir_PolicyFactory.cpp" "BiDir_GIOP\BiDir_ORBInitializer.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_BiDirGIOPs.lib"
	-@del /f/q "$(OUTDIR)\TAO_BiDirGIOPs.exp"
	-@del /f/q "$(OUTDIR)\TAO_BiDirGIOPs.ilk"
	-@del /f/q "BiDir_GIOP\BiDirPolicyC.h"
	-@del /f/q "BiDir_GIOP\BiDirPolicyS.h"
	-@del /f/q "BiDir_GIOP\BiDirPolicyC.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\BiDir_GIOP\$(NULL)" mkdir "Static_Release\BiDir_GIOP"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_BiDirGIOPs.lib"
LINK32_OBJS= \
	"$(INTDIR)\BiDir_GIOP\BiDirPolicyC.obj" \
	"$(INTDIR)\BiDir_GIOP\BiDirGIOP.obj" \
	"$(INTDIR)\BiDir_GIOP\BiDirPolicy_Validator.obj" \
	"$(INTDIR)\BiDir_GIOP\BiDir_Service_Context_Handler.obj" \
	"$(INTDIR)\BiDir_GIOP\BiDir_Policy_i.obj" \
	"$(INTDIR)\BiDir_GIOP\BiDir_PolicyFactory.obj" \
	"$(INTDIR)\BiDir_GIOP\BiDir_ORBInitializer.obj"

"$(OUTDIR)\TAO_BiDirGIOPs.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_BiDirGIOPs.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_BiDirGIOPs.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.BiDir_GIOP.dep")
!INCLUDE "Makefile.BiDir_GIOP.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="BiDir_GIOP\BiDirPolicyC.cpp"

"$(INTDIR)\BiDir_GIOP\BiDirPolicyC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\BiDir_GIOP\$(NULL)" mkdir "$(INTDIR)\BiDir_GIOP\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\BiDir_GIOP\BiDirPolicyC.obj" $(SOURCE)

SOURCE="BiDir_GIOP\BiDirGIOP.cpp"

"$(INTDIR)\BiDir_GIOP\BiDirGIOP.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\BiDir_GIOP\$(NULL)" mkdir "$(INTDIR)\BiDir_GIOP\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\BiDir_GIOP\BiDirGIOP.obj" $(SOURCE)

SOURCE="BiDir_GIOP\BiDirPolicy_Validator.cpp"

"$(INTDIR)\BiDir_GIOP\BiDirPolicy_Validator.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\BiDir_GIOP\$(NULL)" mkdir "$(INTDIR)\BiDir_GIOP\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\BiDir_GIOP\BiDirPolicy_Validator.obj" $(SOURCE)

SOURCE="BiDir_GIOP\BiDir_Service_Context_Handler.cpp"

"$(INTDIR)\BiDir_GIOP\BiDir_Service_Context_Handler.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\BiDir_GIOP\$(NULL)" mkdir "$(INTDIR)\BiDir_GIOP\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\BiDir_GIOP\BiDir_Service_Context_Handler.obj" $(SOURCE)

SOURCE="BiDir_GIOP\BiDir_Policy_i.cpp"

"$(INTDIR)\BiDir_GIOP\BiDir_Policy_i.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\BiDir_GIOP\$(NULL)" mkdir "$(INTDIR)\BiDir_GIOP\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\BiDir_GIOP\BiDir_Policy_i.obj" $(SOURCE)

SOURCE="BiDir_GIOP\BiDir_PolicyFactory.cpp"

"$(INTDIR)\BiDir_GIOP\BiDir_PolicyFactory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\BiDir_GIOP\$(NULL)" mkdir "$(INTDIR)\BiDir_GIOP\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\BiDir_GIOP\BiDir_PolicyFactory.obj" $(SOURCE)

SOURCE="BiDir_GIOP\BiDir_ORBInitializer.cpp"

"$(INTDIR)\BiDir_GIOP\BiDir_ORBInitializer.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\BiDir_GIOP\$(NULL)" mkdir "$(INTDIR)\BiDir_GIOP\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\BiDir_GIOP\BiDir_ORBInitializer.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="BiDir_GIOP\BiDirPolicy.pidl"

InputPath=BiDir_GIOP\BiDirPolicy.pidl

"BiDir_GIOP\BiDirPolicyC.h" "BiDir_GIOP\BiDirPolicyS.h" "BiDir_GIOP\BiDirPolicyC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-BiDir_GIOP_BiDirPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Sa -St -Wb,export_macro=TAO_BiDirGIOP_Export -Wb,export_include=tao/BiDir_GIOP/bidirgiop_export.h -Wb,include_guard=TAO_BIDIRGIOP_SAFE_INCLUDE -Wb,safe_include=tao/BiDir_GIOP/BiDirGIOP.h -o BiDir_GIOP "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="BiDir_GIOP\BiDirPolicy.pidl"

InputPath=BiDir_GIOP\BiDirPolicy.pidl

"BiDir_GIOP\BiDirPolicyC.h" "BiDir_GIOP\BiDirPolicyS.h" "BiDir_GIOP\BiDirPolicyC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-BiDir_GIOP_BiDirPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Sa -St -Wb,export_macro=TAO_BiDirGIOP_Export -Wb,export_include=tao/BiDir_GIOP/bidirgiop_export.h -Wb,include_guard=TAO_BIDIRGIOP_SAFE_INCLUDE -Wb,safe_include=tao/BiDir_GIOP/BiDirGIOP.h -o BiDir_GIOP "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="BiDir_GIOP\BiDirPolicy.pidl"

InputPath=BiDir_GIOP\BiDirPolicy.pidl

"BiDir_GIOP\BiDirPolicyC.h" "BiDir_GIOP\BiDirPolicyS.h" "BiDir_GIOP\BiDirPolicyC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-BiDir_GIOP_BiDirPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Sa -St -Wb,export_macro=TAO_BiDirGIOP_Export -Wb,export_include=tao/BiDir_GIOP/bidirgiop_export.h -Wb,include_guard=TAO_BIDIRGIOP_SAFE_INCLUDE -Wb,safe_include=tao/BiDir_GIOP/BiDirGIOP.h -o BiDir_GIOP "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="BiDir_GIOP\BiDirPolicy.pidl"

InputPath=BiDir_GIOP\BiDirPolicy.pidl

"BiDir_GIOP\BiDirPolicyC.h" "BiDir_GIOP\BiDirPolicyS.h" "BiDir_GIOP\BiDirPolicyC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-BiDir_GIOP_BiDirPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Sa -St -Wb,export_macro=TAO_BiDirGIOP_Export -Wb,export_include=tao/BiDir_GIOP/bidirgiop_export.h -Wb,include_guard=TAO_BIDIRGIOP_SAFE_INCLUDE -Wb,safe_include=tao/BiDir_GIOP/BiDirGIOP.h -o BiDir_GIOP "$(InputPath)"
<<

!ENDIF

SOURCE="BiDir_GIOP\TAO_BiDir_GIOP.rc"

"$(INTDIR)\BiDir_GIOP\TAO_BiDir_GIOP.res" : $(SOURCE)
	@if not exist "$(INTDIR)\BiDir_GIOP\$(NULL)" mkdir "$(INTDIR)\BiDir_GIOP\"
	$(RSC) /l 0x409 /fo"$(INTDIR)\BiDir_GIOP\TAO_BiDir_GIOP.res" /d NDEBUG /d WIN64 /d _CRT_SECURE_NO_WARNINGS /d _CRT_SECURE_NO_DEPRECATE /d _CRT_NONSTDC_NO_DEPRECATE /i "..\.." /i ".." $(SOURCE)



!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.BiDir_GIOP.dep")
	@echo Using "Makefile.BiDir_GIOP.dep"
!ELSE
	@echo Warning: cannot find "Makefile.BiDir_GIOP.dep"
!ENDIF
!ENDIF

