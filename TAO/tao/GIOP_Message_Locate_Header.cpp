//$Id: GIOP_Message_Locate_Header.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/GIOP_Message_Locate_Header.h"

#if !defined (__ACE_INLINE__)
# include "tao/GIOP_Message_Locate_Header.inl"
#endif /* __ACE_INLINE__ */

ACE_RCSID (tao,
           GIOP_Message_Locate_Header,
           "$Id: GIOP_Message_Locate_Header.cpp 14 2007-02-01 15:49:12Z mitza $")
