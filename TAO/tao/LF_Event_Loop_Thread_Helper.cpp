// $Id: LF_Event_Loop_Thread_Helper.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/LF_Event_Loop_Thread_Helper.h"

#if !defined (__ACE_INLINE__)
# include "tao/LF_Event_Loop_Thread_Helper.inl"
#endif /* __ACE_INLINE__ */

ACE_RCSID (tao,
           LF_Event_Loop_Thread_Helper,
           "$Id: LF_Event_Loop_Thread_Helper.cpp 14 2007-02-01 15:49:12Z mitza $")
