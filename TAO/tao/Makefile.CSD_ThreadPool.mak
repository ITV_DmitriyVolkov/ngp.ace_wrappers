# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.CSD_ThreadPool.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\CSD_ThreadPool\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_CSD_ThreadPoold.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -I"..\tao" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_CSD_TP_BUILD_DLL -f "Makefile.CSD_ThreadPool.dep" "CSD_ThreadPool\CSD_TP_Corba_Request.cpp" "CSD_ThreadPool\CSD_TP_Collocated_Synch_With_Server_Request.cpp" "CSD_ThreadPool\CSD_TP_Dispatchable_Visitor.cpp" "CSD_ThreadPool\CSD_TP_Strategy_Factory.cpp" "CSD_ThreadPool\CSD_TP_Strategy.cpp" "CSD_ThreadPool\CSD_TP_Custom_Request_Operation.cpp" "CSD_ThreadPool\CSD_ThreadPool.cpp" "CSD_ThreadPool\CSD_TP_Synch_Helper.cpp" "CSD_ThreadPool\CSD_TP_Collocated_Synch_Request.cpp" "CSD_ThreadPool\CSD_TP_Servant_State.cpp" "CSD_ThreadPool\CSD_TP_Task.cpp" "CSD_ThreadPool\CSD_TP_Custom_Request.cpp" "CSD_ThreadPool\CSD_TP_Collocated_Asynch_Request.cpp" "CSD_ThreadPool\CSD_TP_Servant_State_Map.cpp" "CSD_ThreadPool\CSD_TP_Custom_Synch_Request.cpp" "CSD_ThreadPool\CSD_TP_Request.cpp" "CSD_ThreadPool\CSD_TP_Cancel_Visitor.cpp" "CSD_ThreadPool\CSD_TP_Custom_Asynch_Request.cpp" "CSD_ThreadPool\CSD_TP_Queue.cpp" "CSD_ThreadPool\CSD_TP_Queue_Visitor.cpp" "CSD_ThreadPool\CSD_TP_Remote_Request.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_CSD_ThreadPoold.pdb"
	-@del /f/q "..\..\lib\TAO_CSD_ThreadPoold.dll"
	-@del /f/q "$(OUTDIR)\TAO_CSD_ThreadPoold.lib"
	-@del /f/q "$(OUTDIR)\TAO_CSD_ThreadPoold.exp"
	-@del /f/q "$(OUTDIR)\TAO_CSD_ThreadPoold.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\CSD_ThreadPool\$(NULL)" mkdir "Debug\CSD_ThreadPool"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\.." /I ".." /I "..\tao" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_CSD_TP_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_PortableServerd.lib TAO_CodecFactoryd.lib TAO_PId.lib TAO_CSD_Frameworkd.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\TAO_CSD_ThreadPoold.pdb" /machine:IA64 /out:"..\..\lib\TAO_CSD_ThreadPoold.dll" /implib:"$(OUTDIR)\TAO_CSD_ThreadPoold.lib"
LINK32_OBJS= \
	"$(INTDIR)\CSD_ThreadPool\TAO_CSD_ThreadPool.res" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Corba_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Collocated_Synch_With_Server_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Dispatchable_Visitor.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Strategy_Factory.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Strategy.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Custom_Request_Operation.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_ThreadPool.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Synch_Helper.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Collocated_Synch_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Servant_State.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Task.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Custom_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Collocated_Asynch_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Servant_State_Map.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Custom_Synch_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Cancel_Visitor.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Custom_Asynch_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Queue.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Queue_Visitor.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Remote_Request.obj"

"..\..\lib\TAO_CSD_ThreadPoold.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_CSD_ThreadPoold.dll.manifest" mt.exe -manifest "..\..\lib\TAO_CSD_ThreadPoold.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\CSD_ThreadPool\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_CSD_ThreadPool.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -I"..\tao" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_CSD_TP_BUILD_DLL -f "Makefile.CSD_ThreadPool.dep" "CSD_ThreadPool\CSD_TP_Corba_Request.cpp" "CSD_ThreadPool\CSD_TP_Collocated_Synch_With_Server_Request.cpp" "CSD_ThreadPool\CSD_TP_Dispatchable_Visitor.cpp" "CSD_ThreadPool\CSD_TP_Strategy_Factory.cpp" "CSD_ThreadPool\CSD_TP_Strategy.cpp" "CSD_ThreadPool\CSD_TP_Custom_Request_Operation.cpp" "CSD_ThreadPool\CSD_ThreadPool.cpp" "CSD_ThreadPool\CSD_TP_Synch_Helper.cpp" "CSD_ThreadPool\CSD_TP_Collocated_Synch_Request.cpp" "CSD_ThreadPool\CSD_TP_Servant_State.cpp" "CSD_ThreadPool\CSD_TP_Task.cpp" "CSD_ThreadPool\CSD_TP_Custom_Request.cpp" "CSD_ThreadPool\CSD_TP_Collocated_Asynch_Request.cpp" "CSD_ThreadPool\CSD_TP_Servant_State_Map.cpp" "CSD_ThreadPool\CSD_TP_Custom_Synch_Request.cpp" "CSD_ThreadPool\CSD_TP_Request.cpp" "CSD_ThreadPool\CSD_TP_Cancel_Visitor.cpp" "CSD_ThreadPool\CSD_TP_Custom_Asynch_Request.cpp" "CSD_ThreadPool\CSD_TP_Queue.cpp" "CSD_ThreadPool\CSD_TP_Queue_Visitor.cpp" "CSD_ThreadPool\CSD_TP_Remote_Request.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\TAO_CSD_ThreadPool.dll"
	-@del /f/q "$(OUTDIR)\TAO_CSD_ThreadPool.lib"
	-@del /f/q "$(OUTDIR)\TAO_CSD_ThreadPool.exp"
	-@del /f/q "$(OUTDIR)\TAO_CSD_ThreadPool.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\CSD_ThreadPool\$(NULL)" mkdir "Release\CSD_ThreadPool"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /I "..\tao" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_CSD_TP_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_PortableServer.lib TAO_CodecFactory.lib TAO_PI.lib TAO_CSD_Framework.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\TAO_CSD_ThreadPool.dll" /implib:"$(OUTDIR)\TAO_CSD_ThreadPool.lib"
LINK32_OBJS= \
	"$(INTDIR)\CSD_ThreadPool\TAO_CSD_ThreadPool.res" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Corba_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Collocated_Synch_With_Server_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Dispatchable_Visitor.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Strategy_Factory.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Strategy.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Custom_Request_Operation.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_ThreadPool.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Synch_Helper.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Collocated_Synch_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Servant_State.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Task.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Custom_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Collocated_Asynch_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Servant_State_Map.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Custom_Synch_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Cancel_Visitor.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Custom_Asynch_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Queue.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Queue_Visitor.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Remote_Request.obj"

"..\..\lib\TAO_CSD_ThreadPool.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_CSD_ThreadPool.dll.manifest" mt.exe -manifest "..\..\lib\TAO_CSD_ThreadPool.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\CSD_ThreadPool\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_CSD_ThreadPoolsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -I"..\tao" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CSD_ThreadPool.dep" "CSD_ThreadPool\CSD_TP_Corba_Request.cpp" "CSD_ThreadPool\CSD_TP_Collocated_Synch_With_Server_Request.cpp" "CSD_ThreadPool\CSD_TP_Dispatchable_Visitor.cpp" "CSD_ThreadPool\CSD_TP_Strategy_Factory.cpp" "CSD_ThreadPool\CSD_TP_Strategy.cpp" "CSD_ThreadPool\CSD_TP_Custom_Request_Operation.cpp" "CSD_ThreadPool\CSD_ThreadPool.cpp" "CSD_ThreadPool\CSD_TP_Synch_Helper.cpp" "CSD_ThreadPool\CSD_TP_Collocated_Synch_Request.cpp" "CSD_ThreadPool\CSD_TP_Servant_State.cpp" "CSD_ThreadPool\CSD_TP_Task.cpp" "CSD_ThreadPool\CSD_TP_Custom_Request.cpp" "CSD_ThreadPool\CSD_TP_Collocated_Asynch_Request.cpp" "CSD_ThreadPool\CSD_TP_Servant_State_Map.cpp" "CSD_ThreadPool\CSD_TP_Custom_Synch_Request.cpp" "CSD_ThreadPool\CSD_TP_Request.cpp" "CSD_ThreadPool\CSD_TP_Cancel_Visitor.cpp" "CSD_ThreadPool\CSD_TP_Custom_Asynch_Request.cpp" "CSD_ThreadPool\CSD_TP_Queue.cpp" "CSD_ThreadPool\CSD_TP_Queue_Visitor.cpp" "CSD_ThreadPool\CSD_TP_Remote_Request.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_CSD_ThreadPoolsd.lib"
	-@del /f/q "$(OUTDIR)\TAO_CSD_ThreadPoolsd.exp"
	-@del /f/q "$(OUTDIR)\TAO_CSD_ThreadPoolsd.ilk"
	-@del /f/q "..\..\lib\TAO_CSD_ThreadPoolsd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\CSD_ThreadPool\$(NULL)" mkdir "Static_Debug\CSD_ThreadPool"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"..\..\lib\TAO_CSD_ThreadPoolsd.pdb" /I "..\.." /I ".." /I "..\tao" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_CSD_ThreadPoolsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Corba_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Collocated_Synch_With_Server_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Dispatchable_Visitor.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Strategy_Factory.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Strategy.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Custom_Request_Operation.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_ThreadPool.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Synch_Helper.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Collocated_Synch_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Servant_State.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Task.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Custom_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Collocated_Asynch_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Servant_State_Map.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Custom_Synch_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Cancel_Visitor.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Custom_Asynch_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Queue.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Queue_Visitor.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Remote_Request.obj"

"$(OUTDIR)\TAO_CSD_ThreadPoolsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_CSD_ThreadPoolsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_CSD_ThreadPoolsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\CSD_ThreadPool\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_CSD_ThreadPools.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -I"..\tao" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CSD_ThreadPool.dep" "CSD_ThreadPool\CSD_TP_Corba_Request.cpp" "CSD_ThreadPool\CSD_TP_Collocated_Synch_With_Server_Request.cpp" "CSD_ThreadPool\CSD_TP_Dispatchable_Visitor.cpp" "CSD_ThreadPool\CSD_TP_Strategy_Factory.cpp" "CSD_ThreadPool\CSD_TP_Strategy.cpp" "CSD_ThreadPool\CSD_TP_Custom_Request_Operation.cpp" "CSD_ThreadPool\CSD_ThreadPool.cpp" "CSD_ThreadPool\CSD_TP_Synch_Helper.cpp" "CSD_ThreadPool\CSD_TP_Collocated_Synch_Request.cpp" "CSD_ThreadPool\CSD_TP_Servant_State.cpp" "CSD_ThreadPool\CSD_TP_Task.cpp" "CSD_ThreadPool\CSD_TP_Custom_Request.cpp" "CSD_ThreadPool\CSD_TP_Collocated_Asynch_Request.cpp" "CSD_ThreadPool\CSD_TP_Servant_State_Map.cpp" "CSD_ThreadPool\CSD_TP_Custom_Synch_Request.cpp" "CSD_ThreadPool\CSD_TP_Request.cpp" "CSD_ThreadPool\CSD_TP_Cancel_Visitor.cpp" "CSD_ThreadPool\CSD_TP_Custom_Asynch_Request.cpp" "CSD_ThreadPool\CSD_TP_Queue.cpp" "CSD_ThreadPool\CSD_TP_Queue_Visitor.cpp" "CSD_ThreadPool\CSD_TP_Remote_Request.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_CSD_ThreadPools.lib"
	-@del /f/q "$(OUTDIR)\TAO_CSD_ThreadPools.exp"
	-@del /f/q "$(OUTDIR)\TAO_CSD_ThreadPools.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\CSD_ThreadPool\$(NULL)" mkdir "Static_Release\CSD_ThreadPool"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /I "..\tao" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_CSD_ThreadPools.lib"
LINK32_OBJS= \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Corba_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Collocated_Synch_With_Server_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Dispatchable_Visitor.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Strategy_Factory.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Strategy.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Custom_Request_Operation.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_ThreadPool.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Synch_Helper.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Collocated_Synch_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Servant_State.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Task.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Custom_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Collocated_Asynch_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Servant_State_Map.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Custom_Synch_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Cancel_Visitor.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Custom_Asynch_Request.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Queue.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Queue_Visitor.obj" \
	"$(INTDIR)\CSD_ThreadPool\CSD_TP_Remote_Request.obj"

"$(OUTDIR)\TAO_CSD_ThreadPools.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_CSD_ThreadPools.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_CSD_ThreadPools.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CSD_ThreadPool.dep")
!INCLUDE "Makefile.CSD_ThreadPool.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="CSD_ThreadPool\CSD_TP_Corba_Request.cpp"

"$(INTDIR)\CSD_ThreadPool\CSD_TP_Corba_Request.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_ThreadPool\$(NULL)" mkdir "$(INTDIR)\CSD_ThreadPool\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_ThreadPool\CSD_TP_Corba_Request.obj" $(SOURCE)

SOURCE="CSD_ThreadPool\CSD_TP_Collocated_Synch_With_Server_Request.cpp"

"$(INTDIR)\CSD_ThreadPool\CSD_TP_Collocated_Synch_With_Server_Request.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_ThreadPool\$(NULL)" mkdir "$(INTDIR)\CSD_ThreadPool\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_ThreadPool\CSD_TP_Collocated_Synch_With_Server_Request.obj" $(SOURCE)

SOURCE="CSD_ThreadPool\CSD_TP_Dispatchable_Visitor.cpp"

"$(INTDIR)\CSD_ThreadPool\CSD_TP_Dispatchable_Visitor.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_ThreadPool\$(NULL)" mkdir "$(INTDIR)\CSD_ThreadPool\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_ThreadPool\CSD_TP_Dispatchable_Visitor.obj" $(SOURCE)

SOURCE="CSD_ThreadPool\CSD_TP_Strategy_Factory.cpp"

"$(INTDIR)\CSD_ThreadPool\CSD_TP_Strategy_Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_ThreadPool\$(NULL)" mkdir "$(INTDIR)\CSD_ThreadPool\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_ThreadPool\CSD_TP_Strategy_Factory.obj" $(SOURCE)

SOURCE="CSD_ThreadPool\CSD_TP_Strategy.cpp"

"$(INTDIR)\CSD_ThreadPool\CSD_TP_Strategy.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_ThreadPool\$(NULL)" mkdir "$(INTDIR)\CSD_ThreadPool\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_ThreadPool\CSD_TP_Strategy.obj" $(SOURCE)

SOURCE="CSD_ThreadPool\CSD_TP_Custom_Request_Operation.cpp"

"$(INTDIR)\CSD_ThreadPool\CSD_TP_Custom_Request_Operation.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_ThreadPool\$(NULL)" mkdir "$(INTDIR)\CSD_ThreadPool\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_ThreadPool\CSD_TP_Custom_Request_Operation.obj" $(SOURCE)

SOURCE="CSD_ThreadPool\CSD_ThreadPool.cpp"

"$(INTDIR)\CSD_ThreadPool\CSD_ThreadPool.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_ThreadPool\$(NULL)" mkdir "$(INTDIR)\CSD_ThreadPool\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_ThreadPool\CSD_ThreadPool.obj" $(SOURCE)

SOURCE="CSD_ThreadPool\CSD_TP_Synch_Helper.cpp"

"$(INTDIR)\CSD_ThreadPool\CSD_TP_Synch_Helper.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_ThreadPool\$(NULL)" mkdir "$(INTDIR)\CSD_ThreadPool\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_ThreadPool\CSD_TP_Synch_Helper.obj" $(SOURCE)

SOURCE="CSD_ThreadPool\CSD_TP_Collocated_Synch_Request.cpp"

"$(INTDIR)\CSD_ThreadPool\CSD_TP_Collocated_Synch_Request.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_ThreadPool\$(NULL)" mkdir "$(INTDIR)\CSD_ThreadPool\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_ThreadPool\CSD_TP_Collocated_Synch_Request.obj" $(SOURCE)

SOURCE="CSD_ThreadPool\CSD_TP_Servant_State.cpp"

"$(INTDIR)\CSD_ThreadPool\CSD_TP_Servant_State.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_ThreadPool\$(NULL)" mkdir "$(INTDIR)\CSD_ThreadPool\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_ThreadPool\CSD_TP_Servant_State.obj" $(SOURCE)

SOURCE="CSD_ThreadPool\CSD_TP_Task.cpp"

"$(INTDIR)\CSD_ThreadPool\CSD_TP_Task.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_ThreadPool\$(NULL)" mkdir "$(INTDIR)\CSD_ThreadPool\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_ThreadPool\CSD_TP_Task.obj" $(SOURCE)

SOURCE="CSD_ThreadPool\CSD_TP_Custom_Request.cpp"

"$(INTDIR)\CSD_ThreadPool\CSD_TP_Custom_Request.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_ThreadPool\$(NULL)" mkdir "$(INTDIR)\CSD_ThreadPool\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_ThreadPool\CSD_TP_Custom_Request.obj" $(SOURCE)

SOURCE="CSD_ThreadPool\CSD_TP_Collocated_Asynch_Request.cpp"

"$(INTDIR)\CSD_ThreadPool\CSD_TP_Collocated_Asynch_Request.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_ThreadPool\$(NULL)" mkdir "$(INTDIR)\CSD_ThreadPool\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_ThreadPool\CSD_TP_Collocated_Asynch_Request.obj" $(SOURCE)

SOURCE="CSD_ThreadPool\CSD_TP_Servant_State_Map.cpp"

"$(INTDIR)\CSD_ThreadPool\CSD_TP_Servant_State_Map.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_ThreadPool\$(NULL)" mkdir "$(INTDIR)\CSD_ThreadPool\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_ThreadPool\CSD_TP_Servant_State_Map.obj" $(SOURCE)

SOURCE="CSD_ThreadPool\CSD_TP_Custom_Synch_Request.cpp"

"$(INTDIR)\CSD_ThreadPool\CSD_TP_Custom_Synch_Request.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_ThreadPool\$(NULL)" mkdir "$(INTDIR)\CSD_ThreadPool\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_ThreadPool\CSD_TP_Custom_Synch_Request.obj" $(SOURCE)

SOURCE="CSD_ThreadPool\CSD_TP_Request.cpp"

"$(INTDIR)\CSD_ThreadPool\CSD_TP_Request.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_ThreadPool\$(NULL)" mkdir "$(INTDIR)\CSD_ThreadPool\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_ThreadPool\CSD_TP_Request.obj" $(SOURCE)

SOURCE="CSD_ThreadPool\CSD_TP_Cancel_Visitor.cpp"

"$(INTDIR)\CSD_ThreadPool\CSD_TP_Cancel_Visitor.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_ThreadPool\$(NULL)" mkdir "$(INTDIR)\CSD_ThreadPool\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_ThreadPool\CSD_TP_Cancel_Visitor.obj" $(SOURCE)

SOURCE="CSD_ThreadPool\CSD_TP_Custom_Asynch_Request.cpp"

"$(INTDIR)\CSD_ThreadPool\CSD_TP_Custom_Asynch_Request.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_ThreadPool\$(NULL)" mkdir "$(INTDIR)\CSD_ThreadPool\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_ThreadPool\CSD_TP_Custom_Asynch_Request.obj" $(SOURCE)

SOURCE="CSD_ThreadPool\CSD_TP_Queue.cpp"

"$(INTDIR)\CSD_ThreadPool\CSD_TP_Queue.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_ThreadPool\$(NULL)" mkdir "$(INTDIR)\CSD_ThreadPool\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_ThreadPool\CSD_TP_Queue.obj" $(SOURCE)

SOURCE="CSD_ThreadPool\CSD_TP_Queue_Visitor.cpp"

"$(INTDIR)\CSD_ThreadPool\CSD_TP_Queue_Visitor.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_ThreadPool\$(NULL)" mkdir "$(INTDIR)\CSD_ThreadPool\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_ThreadPool\CSD_TP_Queue_Visitor.obj" $(SOURCE)

SOURCE="CSD_ThreadPool\CSD_TP_Remote_Request.cpp"

"$(INTDIR)\CSD_ThreadPool\CSD_TP_Remote_Request.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_ThreadPool\$(NULL)" mkdir "$(INTDIR)\CSD_ThreadPool\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CSD_ThreadPool\CSD_TP_Remote_Request.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF

SOURCE="CSD_ThreadPool\TAO_CSD_ThreadPool.rc"

"$(INTDIR)\CSD_ThreadPool\TAO_CSD_ThreadPool.res" : $(SOURCE)
	@if not exist "$(INTDIR)\CSD_ThreadPool\$(NULL)" mkdir "$(INTDIR)\CSD_ThreadPool\"
	$(RSC) /l 0x409 /fo"$(INTDIR)\CSD_ThreadPool\TAO_CSD_ThreadPool.res" /d NDEBUG /d WIN64 /d _CRT_SECURE_NO_WARNINGS /d _CRT_SECURE_NO_DEPRECATE /d _CRT_NONSTDC_NO_DEPRECATE /i "..\.." /i ".." /i "..\tao" $(SOURCE)



!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CSD_ThreadPool.dep")
	@echo Using "Makefile.CSD_ThreadPool.dep"
!ELSE
	@echo Warning: cannot find "Makefile.CSD_ThreadPool.dep"
!ENDIF
!ENDIF

