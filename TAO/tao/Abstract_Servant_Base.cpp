// $Id: Abstract_Servant_Base.cpp 935 2008-12-10 21:47:27Z mitza $

#include "tao/Abstract_Servant_Base.h"

ACE_RCSID (tao,
           Abstract_Servant_Base,
           "$Id: Abstract_Servant_Base.cpp 935 2008-12-10 21:47:27Z mitza $")

#if !defined(__ACE_INLINE__)
#include "tao/Abstract_Servant_Base.inl"
#endif /* __ACE_INLINE__ */

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Abstract_ServantBase::~TAO_Abstract_ServantBase (void)
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
