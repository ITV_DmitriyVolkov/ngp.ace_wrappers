# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.PI_Server.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "PI_Server\ServerRequestInfoC.h" "PI_Server\ServerRequestInfoS.h" "PI_Server\ServerRequestInfoA.h" "PI_Server\ServerRequestInfoC.cpp" "PI_Server\ServerRequestInfoA.cpp" "PI_Server\PI_Server_includeC.h" "PI_Server\PI_Server_includeS.h" "PI_Server\PI_Server_includeA.h" "PI_Server\PI_Server_includeC.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\PI_Server\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_PI_Serverd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_PI_SERVER_BUILD_DLL -f "Makefile.PI_Server.dep" "PI_Server\ServerRequestInfoC.cpp" "PI_Server\ServerRequestInfoA.cpp" "PI_Server\PI_Server_includeC.cpp" "PI_Server\PI_Server.cpp" "PI_Server\PortableServer_ORBInitializer.cpp" "PI_Server\ServerRequestInterceptorC.cpp" "PI_Server\PICurrent_Guard.cpp" "PI_Server\ServerRequestInterceptor_Factory_Impl.cpp" "PI_Server\ServerRequestDetails.cpp" "PI_Server\ServerInterceptorAdapter.cpp" "PI_Server\PortableServer_PolicyFactory.cpp" "PI_Server\ServerRequestInterceptorA.cpp" "PI_Server\PI_Server_Loader.cpp" "PI_Server\ServerRequestInfo.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_PI_Serverd.pdb"
	-@del /f/q "..\..\lib\TAO_PI_Serverd.dll"
	-@del /f/q "$(OUTDIR)\TAO_PI_Serverd.lib"
	-@del /f/q "$(OUTDIR)\TAO_PI_Serverd.exp"
	-@del /f/q "$(OUTDIR)\TAO_PI_Serverd.ilk"
	-@del /f/q "PI_Server\ServerRequestInfoC.h"
	-@del /f/q "PI_Server\ServerRequestInfoS.h"
	-@del /f/q "PI_Server\ServerRequestInfoA.h"
	-@del /f/q "PI_Server\ServerRequestInfoC.cpp"
	-@del /f/q "PI_Server\ServerRequestInfoA.cpp"
	-@del /f/q "PI_Server\PI_Server_includeC.h"
	-@del /f/q "PI_Server\PI_Server_includeS.h"
	-@del /f/q "PI_Server\PI_Server_includeA.h"
	-@del /f/q "PI_Server\PI_Server_includeC.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\PI_Server\$(NULL)" mkdir "Debug\PI_Server"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_PI_SERVER_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_CodecFactoryd.lib TAO_PId.lib TAO_PortableServerd.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\TAO_PI_Serverd.pdb" /machine:IA64 /out:"..\..\lib\TAO_PI_Serverd.dll" /implib:"$(OUTDIR)\TAO_PI_Serverd.lib"
LINK32_OBJS= \
	"$(INTDIR)\PI_Server\TAO_PI_Server.res" \
	"$(INTDIR)\PI_Server\ServerRequestInfoC.obj" \
	"$(INTDIR)\PI_Server\ServerRequestInfoA.obj" \
	"$(INTDIR)\PI_Server\PI_Server_includeC.obj" \
	"$(INTDIR)\PI_Server\PI_Server.obj" \
	"$(INTDIR)\PI_Server\PortableServer_ORBInitializer.obj" \
	"$(INTDIR)\PI_Server\ServerRequestInterceptorC.obj" \
	"$(INTDIR)\PI_Server\PICurrent_Guard.obj" \
	"$(INTDIR)\PI_Server\ServerRequestInterceptor_Factory_Impl.obj" \
	"$(INTDIR)\PI_Server\ServerRequestDetails.obj" \
	"$(INTDIR)\PI_Server\ServerInterceptorAdapter.obj" \
	"$(INTDIR)\PI_Server\PortableServer_PolicyFactory.obj" \
	"$(INTDIR)\PI_Server\ServerRequestInterceptorA.obj" \
	"$(INTDIR)\PI_Server\PI_Server_Loader.obj" \
	"$(INTDIR)\PI_Server\ServerRequestInfo.obj"

"..\..\lib\TAO_PI_Serverd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_PI_Serverd.dll.manifest" mt.exe -manifest "..\..\lib\TAO_PI_Serverd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\PI_Server\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_PI_Server.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_PI_SERVER_BUILD_DLL -f "Makefile.PI_Server.dep" "PI_Server\ServerRequestInfoC.cpp" "PI_Server\ServerRequestInfoA.cpp" "PI_Server\PI_Server_includeC.cpp" "PI_Server\PI_Server.cpp" "PI_Server\PortableServer_ORBInitializer.cpp" "PI_Server\ServerRequestInterceptorC.cpp" "PI_Server\PICurrent_Guard.cpp" "PI_Server\ServerRequestInterceptor_Factory_Impl.cpp" "PI_Server\ServerRequestDetails.cpp" "PI_Server\ServerInterceptorAdapter.cpp" "PI_Server\PortableServer_PolicyFactory.cpp" "PI_Server\ServerRequestInterceptorA.cpp" "PI_Server\PI_Server_Loader.cpp" "PI_Server\ServerRequestInfo.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\TAO_PI_Server.dll"
	-@del /f/q "$(OUTDIR)\TAO_PI_Server.lib"
	-@del /f/q "$(OUTDIR)\TAO_PI_Server.exp"
	-@del /f/q "$(OUTDIR)\TAO_PI_Server.ilk"
	-@del /f/q "PI_Server\ServerRequestInfoC.h"
	-@del /f/q "PI_Server\ServerRequestInfoS.h"
	-@del /f/q "PI_Server\ServerRequestInfoA.h"
	-@del /f/q "PI_Server\ServerRequestInfoC.cpp"
	-@del /f/q "PI_Server\ServerRequestInfoA.cpp"
	-@del /f/q "PI_Server\PI_Server_includeC.h"
	-@del /f/q "PI_Server\PI_Server_includeS.h"
	-@del /f/q "PI_Server\PI_Server_includeA.h"
	-@del /f/q "PI_Server\PI_Server_includeC.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\PI_Server\$(NULL)" mkdir "Release\PI_Server"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_PI_SERVER_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_CodecFactory.lib TAO_PI.lib TAO_PortableServer.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\TAO_PI_Server.dll" /implib:"$(OUTDIR)\TAO_PI_Server.lib"
LINK32_OBJS= \
	"$(INTDIR)\PI_Server\TAO_PI_Server.res" \
	"$(INTDIR)\PI_Server\ServerRequestInfoC.obj" \
	"$(INTDIR)\PI_Server\ServerRequestInfoA.obj" \
	"$(INTDIR)\PI_Server\PI_Server_includeC.obj" \
	"$(INTDIR)\PI_Server\PI_Server.obj" \
	"$(INTDIR)\PI_Server\PortableServer_ORBInitializer.obj" \
	"$(INTDIR)\PI_Server\ServerRequestInterceptorC.obj" \
	"$(INTDIR)\PI_Server\PICurrent_Guard.obj" \
	"$(INTDIR)\PI_Server\ServerRequestInterceptor_Factory_Impl.obj" \
	"$(INTDIR)\PI_Server\ServerRequestDetails.obj" \
	"$(INTDIR)\PI_Server\ServerInterceptorAdapter.obj" \
	"$(INTDIR)\PI_Server\PortableServer_PolicyFactory.obj" \
	"$(INTDIR)\PI_Server\ServerRequestInterceptorA.obj" \
	"$(INTDIR)\PI_Server\PI_Server_Loader.obj" \
	"$(INTDIR)\PI_Server\ServerRequestInfo.obj"

"..\..\lib\TAO_PI_Server.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_PI_Server.dll.manifest" mt.exe -manifest "..\..\lib\TAO_PI_Server.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\PI_Server\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_PI_Serversd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.PI_Server.dep" "PI_Server\ServerRequestInfoC.cpp" "PI_Server\ServerRequestInfoA.cpp" "PI_Server\PI_Server_includeC.cpp" "PI_Server\PI_Server.cpp" "PI_Server\PortableServer_ORBInitializer.cpp" "PI_Server\ServerRequestInterceptorC.cpp" "PI_Server\PICurrent_Guard.cpp" "PI_Server\ServerRequestInterceptor_Factory_Impl.cpp" "PI_Server\ServerRequestDetails.cpp" "PI_Server\ServerInterceptorAdapter.cpp" "PI_Server\PortableServer_PolicyFactory.cpp" "PI_Server\ServerRequestInterceptorA.cpp" "PI_Server\PI_Server_Loader.cpp" "PI_Server\ServerRequestInfo.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_PI_Serversd.lib"
	-@del /f/q "$(OUTDIR)\TAO_PI_Serversd.exp"
	-@del /f/q "$(OUTDIR)\TAO_PI_Serversd.ilk"
	-@del /f/q "..\..\lib\TAO_PI_Serversd.pdb"
	-@del /f/q "PI_Server\ServerRequestInfoC.h"
	-@del /f/q "PI_Server\ServerRequestInfoS.h"
	-@del /f/q "PI_Server\ServerRequestInfoA.h"
	-@del /f/q "PI_Server\ServerRequestInfoC.cpp"
	-@del /f/q "PI_Server\ServerRequestInfoA.cpp"
	-@del /f/q "PI_Server\PI_Server_includeC.h"
	-@del /f/q "PI_Server\PI_Server_includeS.h"
	-@del /f/q "PI_Server\PI_Server_includeA.h"
	-@del /f/q "PI_Server\PI_Server_includeC.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\PI_Server\$(NULL)" mkdir "Static_Debug\PI_Server"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"..\..\lib\TAO_PI_Serversd.pdb" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_PI_Serversd.lib"
LINK32_OBJS= \
	"$(INTDIR)\PI_Server\ServerRequestInfoC.obj" \
	"$(INTDIR)\PI_Server\ServerRequestInfoA.obj" \
	"$(INTDIR)\PI_Server\PI_Server_includeC.obj" \
	"$(INTDIR)\PI_Server\PI_Server.obj" \
	"$(INTDIR)\PI_Server\PortableServer_ORBInitializer.obj" \
	"$(INTDIR)\PI_Server\ServerRequestInterceptorC.obj" \
	"$(INTDIR)\PI_Server\PICurrent_Guard.obj" \
	"$(INTDIR)\PI_Server\ServerRequestInterceptor_Factory_Impl.obj" \
	"$(INTDIR)\PI_Server\ServerRequestDetails.obj" \
	"$(INTDIR)\PI_Server\ServerInterceptorAdapter.obj" \
	"$(INTDIR)\PI_Server\PortableServer_PolicyFactory.obj" \
	"$(INTDIR)\PI_Server\ServerRequestInterceptorA.obj" \
	"$(INTDIR)\PI_Server\PI_Server_Loader.obj" \
	"$(INTDIR)\PI_Server\ServerRequestInfo.obj"

"$(OUTDIR)\TAO_PI_Serversd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_PI_Serversd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_PI_Serversd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\PI_Server\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_PI_Servers.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.PI_Server.dep" "PI_Server\ServerRequestInfoC.cpp" "PI_Server\ServerRequestInfoA.cpp" "PI_Server\PI_Server_includeC.cpp" "PI_Server\PI_Server.cpp" "PI_Server\PortableServer_ORBInitializer.cpp" "PI_Server\ServerRequestInterceptorC.cpp" "PI_Server\PICurrent_Guard.cpp" "PI_Server\ServerRequestInterceptor_Factory_Impl.cpp" "PI_Server\ServerRequestDetails.cpp" "PI_Server\ServerInterceptorAdapter.cpp" "PI_Server\PortableServer_PolicyFactory.cpp" "PI_Server\ServerRequestInterceptorA.cpp" "PI_Server\PI_Server_Loader.cpp" "PI_Server\ServerRequestInfo.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_PI_Servers.lib"
	-@del /f/q "$(OUTDIR)\TAO_PI_Servers.exp"
	-@del /f/q "$(OUTDIR)\TAO_PI_Servers.ilk"
	-@del /f/q "PI_Server\ServerRequestInfoC.h"
	-@del /f/q "PI_Server\ServerRequestInfoS.h"
	-@del /f/q "PI_Server\ServerRequestInfoA.h"
	-@del /f/q "PI_Server\ServerRequestInfoC.cpp"
	-@del /f/q "PI_Server\ServerRequestInfoA.cpp"
	-@del /f/q "PI_Server\PI_Server_includeC.h"
	-@del /f/q "PI_Server\PI_Server_includeS.h"
	-@del /f/q "PI_Server\PI_Server_includeA.h"
	-@del /f/q "PI_Server\PI_Server_includeC.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\PI_Server\$(NULL)" mkdir "Static_Release\PI_Server"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_PI_Servers.lib"
LINK32_OBJS= \
	"$(INTDIR)\PI_Server\ServerRequestInfoC.obj" \
	"$(INTDIR)\PI_Server\ServerRequestInfoA.obj" \
	"$(INTDIR)\PI_Server\PI_Server_includeC.obj" \
	"$(INTDIR)\PI_Server\PI_Server.obj" \
	"$(INTDIR)\PI_Server\PortableServer_ORBInitializer.obj" \
	"$(INTDIR)\PI_Server\ServerRequestInterceptorC.obj" \
	"$(INTDIR)\PI_Server\PICurrent_Guard.obj" \
	"$(INTDIR)\PI_Server\ServerRequestInterceptor_Factory_Impl.obj" \
	"$(INTDIR)\PI_Server\ServerRequestDetails.obj" \
	"$(INTDIR)\PI_Server\ServerInterceptorAdapter.obj" \
	"$(INTDIR)\PI_Server\PortableServer_PolicyFactory.obj" \
	"$(INTDIR)\PI_Server\ServerRequestInterceptorA.obj" \
	"$(INTDIR)\PI_Server\PI_Server_Loader.obj" \
	"$(INTDIR)\PI_Server\ServerRequestInfo.obj"

"$(OUTDIR)\TAO_PI_Servers.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_PI_Servers.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_PI_Servers.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.PI_Server.dep")
!INCLUDE "Makefile.PI_Server.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="PI_Server\ServerRequestInfoC.cpp"

"$(INTDIR)\PI_Server\ServerRequestInfoC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI_Server\$(NULL)" mkdir "$(INTDIR)\PI_Server\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI_Server\ServerRequestInfoC.obj" $(SOURCE)

SOURCE="PI_Server\ServerRequestInfoA.cpp"

"$(INTDIR)\PI_Server\ServerRequestInfoA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI_Server\$(NULL)" mkdir "$(INTDIR)\PI_Server\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI_Server\ServerRequestInfoA.obj" $(SOURCE)

SOURCE="PI_Server\PI_Server_includeC.cpp"

"$(INTDIR)\PI_Server\PI_Server_includeC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI_Server\$(NULL)" mkdir "$(INTDIR)\PI_Server\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI_Server\PI_Server_includeC.obj" $(SOURCE)

SOURCE="PI_Server\PI_Server.cpp"

"$(INTDIR)\PI_Server\PI_Server.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI_Server\$(NULL)" mkdir "$(INTDIR)\PI_Server\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI_Server\PI_Server.obj" $(SOURCE)

SOURCE="PI_Server\PortableServer_ORBInitializer.cpp"

"$(INTDIR)\PI_Server\PortableServer_ORBInitializer.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI_Server\$(NULL)" mkdir "$(INTDIR)\PI_Server\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI_Server\PortableServer_ORBInitializer.obj" $(SOURCE)

SOURCE="PI_Server\ServerRequestInterceptorC.cpp"

"$(INTDIR)\PI_Server\ServerRequestInterceptorC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI_Server\$(NULL)" mkdir "$(INTDIR)\PI_Server\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI_Server\ServerRequestInterceptorC.obj" $(SOURCE)

SOURCE="PI_Server\PICurrent_Guard.cpp"

"$(INTDIR)\PI_Server\PICurrent_Guard.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI_Server\$(NULL)" mkdir "$(INTDIR)\PI_Server\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI_Server\PICurrent_Guard.obj" $(SOURCE)

SOURCE="PI_Server\ServerRequestInterceptor_Factory_Impl.cpp"

"$(INTDIR)\PI_Server\ServerRequestInterceptor_Factory_Impl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI_Server\$(NULL)" mkdir "$(INTDIR)\PI_Server\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI_Server\ServerRequestInterceptor_Factory_Impl.obj" $(SOURCE)

SOURCE="PI_Server\ServerRequestDetails.cpp"

"$(INTDIR)\PI_Server\ServerRequestDetails.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI_Server\$(NULL)" mkdir "$(INTDIR)\PI_Server\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI_Server\ServerRequestDetails.obj" $(SOURCE)

SOURCE="PI_Server\ServerInterceptorAdapter.cpp"

"$(INTDIR)\PI_Server\ServerInterceptorAdapter.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI_Server\$(NULL)" mkdir "$(INTDIR)\PI_Server\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI_Server\ServerInterceptorAdapter.obj" $(SOURCE)

SOURCE="PI_Server\PortableServer_PolicyFactory.cpp"

"$(INTDIR)\PI_Server\PortableServer_PolicyFactory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI_Server\$(NULL)" mkdir "$(INTDIR)\PI_Server\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI_Server\PortableServer_PolicyFactory.obj" $(SOURCE)

SOURCE="PI_Server\ServerRequestInterceptorA.cpp"

"$(INTDIR)\PI_Server\ServerRequestInterceptorA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI_Server\$(NULL)" mkdir "$(INTDIR)\PI_Server\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI_Server\ServerRequestInterceptorA.obj" $(SOURCE)

SOURCE="PI_Server\PI_Server_Loader.cpp"

"$(INTDIR)\PI_Server\PI_Server_Loader.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI_Server\$(NULL)" mkdir "$(INTDIR)\PI_Server\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI_Server\PI_Server_Loader.obj" $(SOURCE)

SOURCE="PI_Server\ServerRequestInfo.cpp"

"$(INTDIR)\PI_Server\ServerRequestInfo.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI_Server\$(NULL)" mkdir "$(INTDIR)\PI_Server\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI_Server\ServerRequestInfo.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="PI_Server\ServerRequestInfo.pidl"

InputPath=PI_Server\ServerRequestInfo.pidl

"PI_Server\ServerRequestInfoC.h" "PI_Server\ServerRequestInfoS.h" "PI_Server\ServerRequestInfoA.h" "PI_Server\ServerRequestInfoC.cpp" "PI_Server\ServerRequestInfoA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PI_Server_ServerRequestInfo_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Wb,export_macro=TAO_PI_Server_Export -Wb,export_include=tao/PI_Server/pi_server_export.h -o PI_Server -Sci -SS -GA -Gp -Gd -Sorb -Sal "$(InputPath)"
<<

SOURCE="PI_Server\PI_Server_include.pidl"

InputPath=PI_Server\PI_Server_include.pidl

"PI_Server\PI_Server_includeC.h" "PI_Server\PI_Server_includeS.h" "PI_Server\PI_Server_includeA.h" "PI_Server\PI_Server_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PI_Server_PI_Server_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Wb,export_macro=TAO_PI_Server_Export -Wb,export_include=tao/PI_Server/pi_server_export.h -o PI_Server -SS -Sorb -GX -Sci -Wb,unique_include=tao/PI_Server/PI_Server.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="PI_Server\ServerRequestInfo.pidl"

InputPath=PI_Server\ServerRequestInfo.pidl

"PI_Server\ServerRequestInfoC.h" "PI_Server\ServerRequestInfoS.h" "PI_Server\ServerRequestInfoA.h" "PI_Server\ServerRequestInfoC.cpp" "PI_Server\ServerRequestInfoA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PI_Server_ServerRequestInfo_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Wb,export_macro=TAO_PI_Server_Export -Wb,export_include=tao/PI_Server/pi_server_export.h -o PI_Server -Sci -SS -GA -Gp -Gd -Sorb -Sal "$(InputPath)"
<<

SOURCE="PI_Server\PI_Server_include.pidl"

InputPath=PI_Server\PI_Server_include.pidl

"PI_Server\PI_Server_includeC.h" "PI_Server\PI_Server_includeS.h" "PI_Server\PI_Server_includeA.h" "PI_Server\PI_Server_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PI_Server_PI_Server_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Wb,export_macro=TAO_PI_Server_Export -Wb,export_include=tao/PI_Server/pi_server_export.h -o PI_Server -SS -Sorb -GX -Sci -Wb,unique_include=tao/PI_Server/PI_Server.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="PI_Server\ServerRequestInfo.pidl"

InputPath=PI_Server\ServerRequestInfo.pidl

"PI_Server\ServerRequestInfoC.h" "PI_Server\ServerRequestInfoS.h" "PI_Server\ServerRequestInfoA.h" "PI_Server\ServerRequestInfoC.cpp" "PI_Server\ServerRequestInfoA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PI_Server_ServerRequestInfo_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Wb,export_macro=TAO_PI_Server_Export -Wb,export_include=tao/PI_Server/pi_server_export.h -o PI_Server -Sci -SS -GA -Gp -Gd -Sorb -Sal "$(InputPath)"
<<

SOURCE="PI_Server\PI_Server_include.pidl"

InputPath=PI_Server\PI_Server_include.pidl

"PI_Server\PI_Server_includeC.h" "PI_Server\PI_Server_includeS.h" "PI_Server\PI_Server_includeA.h" "PI_Server\PI_Server_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PI_Server_PI_Server_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Wb,export_macro=TAO_PI_Server_Export -Wb,export_include=tao/PI_Server/pi_server_export.h -o PI_Server -SS -Sorb -GX -Sci -Wb,unique_include=tao/PI_Server/PI_Server.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="PI_Server\ServerRequestInfo.pidl"

InputPath=PI_Server\ServerRequestInfo.pidl

"PI_Server\ServerRequestInfoC.h" "PI_Server\ServerRequestInfoS.h" "PI_Server\ServerRequestInfoA.h" "PI_Server\ServerRequestInfoC.cpp" "PI_Server\ServerRequestInfoA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PI_Server_ServerRequestInfo_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Wb,export_macro=TAO_PI_Server_Export -Wb,export_include=tao/PI_Server/pi_server_export.h -o PI_Server -Sci -SS -GA -Gp -Gd -Sorb -Sal "$(InputPath)"
<<

SOURCE="PI_Server\PI_Server_include.pidl"

InputPath=PI_Server\PI_Server_include.pidl

"PI_Server\PI_Server_includeC.h" "PI_Server\PI_Server_includeS.h" "PI_Server\PI_Server_includeA.h" "PI_Server\PI_Server_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PI_Server_PI_Server_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Wb,export_macro=TAO_PI_Server_Export -Wb,export_include=tao/PI_Server/pi_server_export.h -o PI_Server -SS -Sorb -GX -Sci -Wb,unique_include=tao/PI_Server/PI_Server.h "$(InputPath)"
<<

!ENDIF

SOURCE="PI_Server\TAO_PI_Server.rc"

"$(INTDIR)\PI_Server\TAO_PI_Server.res" : $(SOURCE)
	@if not exist "$(INTDIR)\PI_Server\$(NULL)" mkdir "$(INTDIR)\PI_Server\"
	$(RSC) /l 0x409 /fo"$(INTDIR)\PI_Server\TAO_PI_Server.res" /d NDEBUG /d WIN64 /d _CRT_SECURE_NO_WARNINGS /d _CRT_SECURE_NO_DEPRECATE /d _CRT_NONSTDC_NO_DEPRECATE /i "..\.." /i ".." $(SOURCE)



!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.PI_Server.dep")
	@echo Using "Makefile.PI_Server.dep"
!ELSE
	@echo Warning: cannot find "Makefile.PI_Server.dep"
!ENDIF
!ENDIF

