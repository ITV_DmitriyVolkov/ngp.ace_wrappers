// $Id: Protocol_Factory.cpp 935 2008-12-10 21:47:27Z mitza $

#include "tao/Protocol_Factory.h"

ACE_RCSID (tao,
           Protocol_Factory,
           "$Id: Protocol_Factory.cpp 935 2008-12-10 21:47:27Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Protocol_Factory::TAO_Protocol_Factory (CORBA::ULong tag)
  : tag_ (tag)
{
}

TAO_Protocol_Factory::~TAO_Protocol_Factory (void)
{
}

CORBA::ULong
TAO_Protocol_Factory::tag (void) const
{
  return this->tag_;
}

TAO_END_VERSIONED_NAMESPACE_DECL
