//
// $Id: sciop_endpoints.pidl 935 2008-12-10 21:47:27Z mitza $

/**
 * This file contains idl definition for data structures used to
 * encapsulate data in TAO_TAG_ENDPOINTS tagged component.  This
 * TAO-specific component is used for transmission of multiple
 * endpoints per single profile.  Data structures defined here are
 * used for transmission of SCIOP Endpoints.  See SCIOP_Profile.*
 * for more details.
 *
 * This file is used to generate the code in
 * sciop_endpoints.* The command used to generate code
 * is:
 *
 * tao_idl
 *          -o orig -Gp -Gd -Sa -DCORBA3 -Sci
 *           -Wb,export_macro=TAO_Strategies_Export \
 *           -Wb,export_include="strategies_export.h" \
 *           -Wb,pre_include="ace/pre.h" \
 *           -Wb,post_include="ace/post.h" \
 *           sciop_endpoints.pidl
 */

#ifndef _SCIOP_ENDPOINTS_IDL_
#define _SCIOP_ENDPOINTS_IDL_

/// Stores information for a single SCIOP endpoint.
struct TAO_SCIOP_Endpoint_Info
{
  string host;
  short port;
  short priority;
};

/// Stores information for a collection of SCIOP endpoints.
typedef sequence <TAO_SCIOP_Endpoint_Info> TAO_SCIOPEndpointSequence;

#pragma prefix ""

#endif /* _SCIOP_ENDPOINTS_IDL_ */
