// =================================================================
/**
 * @file OC_Endpoint_Selector_Loader.cpp
 *
 * $Id: OC_Endpoint_Selector_Loader.cpp 14 2007-02-01 15:49:12Z mitza $
 *
 * @author Phil Mesnier <mesnier_p@ociweb.com>
 *
 */
// =================================================================
// $Id: OC_Endpoint_Selector_Loader.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/Strategies/OC_Endpoint_Selector_Loader.h"
#include "tao/Strategies/OC_Endpoint_Selector_Factory.h"

ACE_RCSID (tao,
           OC_Endpoint_Selector_Loader,
           "$Id: OC_Endpoint_Selector_Loader.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

int
TAO_OC_Endpoint_Selector_Loader::init (void)
{
  return  ACE_Service_Config::process_directive (ace_svc_desc_TAO_OC_Endpoint_Selector_Factory);
}

TAO_END_VERSIONED_NAMESPACE_DECL
