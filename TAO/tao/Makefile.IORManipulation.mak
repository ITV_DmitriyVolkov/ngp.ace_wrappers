# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.IORManipulation.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "IORManipulation\IORC.h" "IORManipulation\IORS.h" "IORManipulation\IORA.h" "IORManipulation\IORC.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\IORManipulation\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_IORManipd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_IORMANIP_BUILD_DLL -f "Makefile.IORManipulation.dep" "IORManipulation\IORC.cpp" "IORManipulation\IORManip_Filter.cpp" "IORManipulation\IORManip_Loader.cpp" "IORManipulation\IORManip_IIOP_Filter.cpp" "IORManipulation\IORManipulation.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_IORManipd.pdb"
	-@del /f/q "..\..\lib\TAO_IORManipd.dll"
	-@del /f/q "$(OUTDIR)\TAO_IORManipd.lib"
	-@del /f/q "$(OUTDIR)\TAO_IORManipd.exp"
	-@del /f/q "$(OUTDIR)\TAO_IORManipd.ilk"
	-@del /f/q "IORManipulation\IORC.h"
	-@del /f/q "IORManipulation\IORS.h"
	-@del /f/q "IORManipulation\IORA.h"
	-@del /f/q "IORManipulation\IORC.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\IORManipulation\$(NULL)" mkdir "Debug\IORManipulation"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_IORMANIP_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\TAO_IORManipd.pdb" /machine:IA64 /out:"..\..\lib\TAO_IORManipd.dll" /implib:"$(OUTDIR)\TAO_IORManipd.lib"
LINK32_OBJS= \
	"$(INTDIR)\IORManipulation\TAO_IORManip.res" \
	"$(INTDIR)\IORManipulation\IORC.obj" \
	"$(INTDIR)\IORManipulation\IORManip_Filter.obj" \
	"$(INTDIR)\IORManipulation\IORManip_Loader.obj" \
	"$(INTDIR)\IORManipulation\IORManip_IIOP_Filter.obj" \
	"$(INTDIR)\IORManipulation\IORManipulation.obj"

"..\..\lib\TAO_IORManipd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_IORManipd.dll.manifest" mt.exe -manifest "..\..\lib\TAO_IORManipd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\IORManipulation\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_IORManip.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_IORMANIP_BUILD_DLL -f "Makefile.IORManipulation.dep" "IORManipulation\IORC.cpp" "IORManipulation\IORManip_Filter.cpp" "IORManipulation\IORManip_Loader.cpp" "IORManipulation\IORManip_IIOP_Filter.cpp" "IORManipulation\IORManipulation.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\TAO_IORManip.dll"
	-@del /f/q "$(OUTDIR)\TAO_IORManip.lib"
	-@del /f/q "$(OUTDIR)\TAO_IORManip.exp"
	-@del /f/q "$(OUTDIR)\TAO_IORManip.ilk"
	-@del /f/q "IORManipulation\IORC.h"
	-@del /f/q "IORManipulation\IORS.h"
	-@del /f/q "IORManipulation\IORA.h"
	-@del /f/q "IORManipulation\IORC.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\IORManipulation\$(NULL)" mkdir "Release\IORManipulation"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_IORMANIP_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\TAO_IORManip.dll" /implib:"$(OUTDIR)\TAO_IORManip.lib"
LINK32_OBJS= \
	"$(INTDIR)\IORManipulation\TAO_IORManip.res" \
	"$(INTDIR)\IORManipulation\IORC.obj" \
	"$(INTDIR)\IORManipulation\IORManip_Filter.obj" \
	"$(INTDIR)\IORManipulation\IORManip_Loader.obj" \
	"$(INTDIR)\IORManipulation\IORManip_IIOP_Filter.obj" \
	"$(INTDIR)\IORManipulation\IORManipulation.obj"

"..\..\lib\TAO_IORManip.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_IORManip.dll.manifest" mt.exe -manifest "..\..\lib\TAO_IORManip.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\IORManipulation\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_IORManipsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.IORManipulation.dep" "IORManipulation\IORC.cpp" "IORManipulation\IORManip_Filter.cpp" "IORManipulation\IORManip_Loader.cpp" "IORManipulation\IORManip_IIOP_Filter.cpp" "IORManipulation\IORManipulation.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_IORManipsd.lib"
	-@del /f/q "$(OUTDIR)\TAO_IORManipsd.exp"
	-@del /f/q "$(OUTDIR)\TAO_IORManipsd.ilk"
	-@del /f/q "..\..\lib\TAO_IORManipsd.pdb"
	-@del /f/q "IORManipulation\IORC.h"
	-@del /f/q "IORManipulation\IORS.h"
	-@del /f/q "IORManipulation\IORA.h"
	-@del /f/q "IORManipulation\IORC.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\IORManipulation\$(NULL)" mkdir "Static_Debug\IORManipulation"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"..\..\lib\TAO_IORManipsd.pdb" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_IORManipsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\IORManipulation\IORC.obj" \
	"$(INTDIR)\IORManipulation\IORManip_Filter.obj" \
	"$(INTDIR)\IORManipulation\IORManip_Loader.obj" \
	"$(INTDIR)\IORManipulation\IORManip_IIOP_Filter.obj" \
	"$(INTDIR)\IORManipulation\IORManipulation.obj"

"$(OUTDIR)\TAO_IORManipsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_IORManipsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_IORManipsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\IORManipulation\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_IORManips.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.IORManipulation.dep" "IORManipulation\IORC.cpp" "IORManipulation\IORManip_Filter.cpp" "IORManipulation\IORManip_Loader.cpp" "IORManipulation\IORManip_IIOP_Filter.cpp" "IORManipulation\IORManipulation.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_IORManips.lib"
	-@del /f/q "$(OUTDIR)\TAO_IORManips.exp"
	-@del /f/q "$(OUTDIR)\TAO_IORManips.ilk"
	-@del /f/q "IORManipulation\IORC.h"
	-@del /f/q "IORManipulation\IORS.h"
	-@del /f/q "IORManipulation\IORA.h"
	-@del /f/q "IORManipulation\IORC.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\IORManipulation\$(NULL)" mkdir "Static_Release\IORManipulation"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_IORManips.lib"
LINK32_OBJS= \
	"$(INTDIR)\IORManipulation\IORC.obj" \
	"$(INTDIR)\IORManipulation\IORManip_Filter.obj" \
	"$(INTDIR)\IORManipulation\IORManip_Loader.obj" \
	"$(INTDIR)\IORManipulation\IORManip_IIOP_Filter.obj" \
	"$(INTDIR)\IORManipulation\IORManipulation.obj"

"$(OUTDIR)\TAO_IORManips.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_IORManips.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_IORManips.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.IORManipulation.dep")
!INCLUDE "Makefile.IORManipulation.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="IORManipulation\IORC.cpp"

"$(INTDIR)\IORManipulation\IORC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\IORManipulation\$(NULL)" mkdir "$(INTDIR)\IORManipulation\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IORManipulation\IORC.obj" $(SOURCE)

SOURCE="IORManipulation\IORManip_Filter.cpp"

"$(INTDIR)\IORManipulation\IORManip_Filter.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\IORManipulation\$(NULL)" mkdir "$(INTDIR)\IORManipulation\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IORManipulation\IORManip_Filter.obj" $(SOURCE)

SOURCE="IORManipulation\IORManip_Loader.cpp"

"$(INTDIR)\IORManipulation\IORManip_Loader.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\IORManipulation\$(NULL)" mkdir "$(INTDIR)\IORManipulation\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IORManipulation\IORManip_Loader.obj" $(SOURCE)

SOURCE="IORManipulation\IORManip_IIOP_Filter.cpp"

"$(INTDIR)\IORManipulation\IORManip_IIOP_Filter.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\IORManipulation\$(NULL)" mkdir "$(INTDIR)\IORManipulation\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IORManipulation\IORManip_IIOP_Filter.obj" $(SOURCE)

SOURCE="IORManipulation\IORManipulation.cpp"

"$(INTDIR)\IORManipulation\IORManipulation.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\IORManipulation\$(NULL)" mkdir "$(INTDIR)\IORManipulation\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IORManipulation\IORManipulation.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="IORManipulation\IOR.pidl"

InputPath=IORManipulation\IOR.pidl

"IORManipulation\IORC.h" "IORManipulation\IORS.h" "IORManipulation\IORA.h" "IORManipulation\IORC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-IORManipulation_IOR_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GX -Wb,export_macro=TAO_IORManip_Export -Wb,export_include=tao/IORManipulation/ior_manip_export.h -Wb,include_guard=TAO_IORMANIP_SAFE_INCLUDE -Wb,safe_include=tao/IORManipulation/IORManip_Loader.h -o IORManipulation "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="IORManipulation\IOR.pidl"

InputPath=IORManipulation\IOR.pidl

"IORManipulation\IORC.h" "IORManipulation\IORS.h" "IORManipulation\IORA.h" "IORManipulation\IORC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-IORManipulation_IOR_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GX -Wb,export_macro=TAO_IORManip_Export -Wb,export_include=tao/IORManipulation/ior_manip_export.h -Wb,include_guard=TAO_IORMANIP_SAFE_INCLUDE -Wb,safe_include=tao/IORManipulation/IORManip_Loader.h -o IORManipulation "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="IORManipulation\IOR.pidl"

InputPath=IORManipulation\IOR.pidl

"IORManipulation\IORC.h" "IORManipulation\IORS.h" "IORManipulation\IORA.h" "IORManipulation\IORC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-IORManipulation_IOR_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GX -Wb,export_macro=TAO_IORManip_Export -Wb,export_include=tao/IORManipulation/ior_manip_export.h -Wb,include_guard=TAO_IORMANIP_SAFE_INCLUDE -Wb,safe_include=tao/IORManipulation/IORManip_Loader.h -o IORManipulation "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="IORManipulation\IOR.pidl"

InputPath=IORManipulation\IOR.pidl

"IORManipulation\IORC.h" "IORManipulation\IORS.h" "IORManipulation\IORA.h" "IORManipulation\IORC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-IORManipulation_IOR_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GX -Wb,export_macro=TAO_IORManip_Export -Wb,export_include=tao/IORManipulation/ior_manip_export.h -Wb,include_guard=TAO_IORMANIP_SAFE_INCLUDE -Wb,safe_include=tao/IORManipulation/IORManip_Loader.h -o IORManipulation "$(InputPath)"
<<

!ENDIF

SOURCE="IORManipulation\TAO_IORManip.rc"

"$(INTDIR)\IORManipulation\TAO_IORManip.res" : $(SOURCE)
	@if not exist "$(INTDIR)\IORManipulation\$(NULL)" mkdir "$(INTDIR)\IORManipulation\"
	$(RSC) /l 0x409 /fo"$(INTDIR)\IORManipulation\TAO_IORManip.res" /d NDEBUG /d WIN64 /d _CRT_SECURE_NO_WARNINGS /d _CRT_SECURE_NO_DEPRECATE /d _CRT_NONSTDC_NO_DEPRECATE /i "..\.." /i ".." $(SOURCE)



!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.IORManipulation.dep")
	@echo Using "Makefile.IORManipulation.dep"
!ELSE
	@echo Warning: cannot find "Makefile.IORManipulation.dep"
!ENDIF
!ENDIF

