# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.Compression.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "Compression\CompressionC.h" "Compression\CompressionS.h" "Compression\CompressionA.h" "Compression\CompressionC.cpp" "Compression\CompressionA.cpp" "Compression\Compression_includeC.h" "Compression\Compression_includeS.h" "Compression\Compression_includeA.h" "Compression\Compression_includeC.cpp" "Compression\Compression_includeA.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\Compression\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_Compressiond.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_COMPRESSION_BUILD_DLL -f "Makefile.Compression.dep" "Compression\CompressionC.cpp" "Compression\CompressionA.cpp" "Compression\Compression_includeC.cpp" "Compression\Compression_includeA.cpp" "Compression\Compression_Manager.cpp" "Compression\Base_Compressor.cpp" "Compression\Compression.cpp" "Compression\Compressor_Factory.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_Compressiond.pdb"
	-@del /f/q "..\..\lib\TAO_Compressiond.dll"
	-@del /f/q "$(OUTDIR)\TAO_Compressiond.lib"
	-@del /f/q "$(OUTDIR)\TAO_Compressiond.exp"
	-@del /f/q "$(OUTDIR)\TAO_Compressiond.ilk"
	-@del /f/q "Compression\CompressionC.h"
	-@del /f/q "Compression\CompressionS.h"
	-@del /f/q "Compression\CompressionA.h"
	-@del /f/q "Compression\CompressionC.cpp"
	-@del /f/q "Compression\CompressionA.cpp"
	-@del /f/q "Compression\Compression_includeC.h"
	-@del /f/q "Compression\Compression_includeS.h"
	-@del /f/q "Compression\Compression_includeA.h"
	-@del /f/q "Compression\Compression_includeC.cpp"
	-@del /f/q "Compression\Compression_includeA.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\Compression\$(NULL)" mkdir "Debug\Compression"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_COMPRESSION_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\TAO_Compressiond.pdb" /machine:IA64 /out:"..\..\lib\TAO_Compressiond.dll" /implib:"$(OUTDIR)\TAO_Compressiond.lib"
LINK32_OBJS= \
	"$(INTDIR)\Compression\TAO_Compression.res" \
	"$(INTDIR)\Compression\CompressionC.obj" \
	"$(INTDIR)\Compression\CompressionA.obj" \
	"$(INTDIR)\Compression\Compression_includeC.obj" \
	"$(INTDIR)\Compression\Compression_includeA.obj" \
	"$(INTDIR)\Compression\Compression_Manager.obj" \
	"$(INTDIR)\Compression\Base_Compressor.obj" \
	"$(INTDIR)\Compression\Compression.obj" \
	"$(INTDIR)\Compression\Compressor_Factory.obj"

"..\..\lib\TAO_Compressiond.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_Compressiond.dll.manifest" mt.exe -manifest "..\..\lib\TAO_Compressiond.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\Compression\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_Compression.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_COMPRESSION_BUILD_DLL -f "Makefile.Compression.dep" "Compression\CompressionC.cpp" "Compression\CompressionA.cpp" "Compression\Compression_includeC.cpp" "Compression\Compression_includeA.cpp" "Compression\Compression_Manager.cpp" "Compression\Base_Compressor.cpp" "Compression\Compression.cpp" "Compression\Compressor_Factory.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\TAO_Compression.dll"
	-@del /f/q "$(OUTDIR)\TAO_Compression.lib"
	-@del /f/q "$(OUTDIR)\TAO_Compression.exp"
	-@del /f/q "$(OUTDIR)\TAO_Compression.ilk"
	-@del /f/q "Compression\CompressionC.h"
	-@del /f/q "Compression\CompressionS.h"
	-@del /f/q "Compression\CompressionA.h"
	-@del /f/q "Compression\CompressionC.cpp"
	-@del /f/q "Compression\CompressionA.cpp"
	-@del /f/q "Compression\Compression_includeC.h"
	-@del /f/q "Compression\Compression_includeS.h"
	-@del /f/q "Compression\Compression_includeA.h"
	-@del /f/q "Compression\Compression_includeC.cpp"
	-@del /f/q "Compression\Compression_includeA.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\Compression\$(NULL)" mkdir "Release\Compression"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_COMPRESSION_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\TAO_Compression.dll" /implib:"$(OUTDIR)\TAO_Compression.lib"
LINK32_OBJS= \
	"$(INTDIR)\Compression\TAO_Compression.res" \
	"$(INTDIR)\Compression\CompressionC.obj" \
	"$(INTDIR)\Compression\CompressionA.obj" \
	"$(INTDIR)\Compression\Compression_includeC.obj" \
	"$(INTDIR)\Compression\Compression_includeA.obj" \
	"$(INTDIR)\Compression\Compression_Manager.obj" \
	"$(INTDIR)\Compression\Base_Compressor.obj" \
	"$(INTDIR)\Compression\Compression.obj" \
	"$(INTDIR)\Compression\Compressor_Factory.obj"

"..\..\lib\TAO_Compression.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_Compression.dll.manifest" mt.exe -manifest "..\..\lib\TAO_Compression.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\Compression\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_Compressionsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Compression.dep" "Compression\CompressionC.cpp" "Compression\CompressionA.cpp" "Compression\Compression_includeC.cpp" "Compression\Compression_includeA.cpp" "Compression\Compression_Manager.cpp" "Compression\Base_Compressor.cpp" "Compression\Compression.cpp" "Compression\Compressor_Factory.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_Compressionsd.lib"
	-@del /f/q "$(OUTDIR)\TAO_Compressionsd.exp"
	-@del /f/q "$(OUTDIR)\TAO_Compressionsd.ilk"
	-@del /f/q "..\..\lib\TAO_Compressionsd.pdb"
	-@del /f/q "Compression\CompressionC.h"
	-@del /f/q "Compression\CompressionS.h"
	-@del /f/q "Compression\CompressionA.h"
	-@del /f/q "Compression\CompressionC.cpp"
	-@del /f/q "Compression\CompressionA.cpp"
	-@del /f/q "Compression\Compression_includeC.h"
	-@del /f/q "Compression\Compression_includeS.h"
	-@del /f/q "Compression\Compression_includeA.h"
	-@del /f/q "Compression\Compression_includeC.cpp"
	-@del /f/q "Compression\Compression_includeA.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\Compression\$(NULL)" mkdir "Static_Debug\Compression"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"..\..\lib\TAO_Compressionsd.pdb" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_Compressionsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Compression\CompressionC.obj" \
	"$(INTDIR)\Compression\CompressionA.obj" \
	"$(INTDIR)\Compression\Compression_includeC.obj" \
	"$(INTDIR)\Compression\Compression_includeA.obj" \
	"$(INTDIR)\Compression\Compression_Manager.obj" \
	"$(INTDIR)\Compression\Base_Compressor.obj" \
	"$(INTDIR)\Compression\Compression.obj" \
	"$(INTDIR)\Compression\Compressor_Factory.obj"

"$(OUTDIR)\TAO_Compressionsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_Compressionsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_Compressionsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\Compression\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_Compressions.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Compression.dep" "Compression\CompressionC.cpp" "Compression\CompressionA.cpp" "Compression\Compression_includeC.cpp" "Compression\Compression_includeA.cpp" "Compression\Compression_Manager.cpp" "Compression\Base_Compressor.cpp" "Compression\Compression.cpp" "Compression\Compressor_Factory.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_Compressions.lib"
	-@del /f/q "$(OUTDIR)\TAO_Compressions.exp"
	-@del /f/q "$(OUTDIR)\TAO_Compressions.ilk"
	-@del /f/q "Compression\CompressionC.h"
	-@del /f/q "Compression\CompressionS.h"
	-@del /f/q "Compression\CompressionA.h"
	-@del /f/q "Compression\CompressionC.cpp"
	-@del /f/q "Compression\CompressionA.cpp"
	-@del /f/q "Compression\Compression_includeC.h"
	-@del /f/q "Compression\Compression_includeS.h"
	-@del /f/q "Compression\Compression_includeA.h"
	-@del /f/q "Compression\Compression_includeC.cpp"
	-@del /f/q "Compression\Compression_includeA.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\Compression\$(NULL)" mkdir "Static_Release\Compression"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_Compressions.lib"
LINK32_OBJS= \
	"$(INTDIR)\Compression\CompressionC.obj" \
	"$(INTDIR)\Compression\CompressionA.obj" \
	"$(INTDIR)\Compression\Compression_includeC.obj" \
	"$(INTDIR)\Compression\Compression_includeA.obj" \
	"$(INTDIR)\Compression\Compression_Manager.obj" \
	"$(INTDIR)\Compression\Base_Compressor.obj" \
	"$(INTDIR)\Compression\Compression.obj" \
	"$(INTDIR)\Compression\Compressor_Factory.obj"

"$(OUTDIR)\TAO_Compressions.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_Compressions.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_Compressions.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Compression.dep")
!INCLUDE "Makefile.Compression.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Compression\CompressionC.cpp"

"$(INTDIR)\Compression\CompressionC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Compression\$(NULL)" mkdir "$(INTDIR)\Compression\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Compression\CompressionC.obj" $(SOURCE)

SOURCE="Compression\CompressionA.cpp"

"$(INTDIR)\Compression\CompressionA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Compression\$(NULL)" mkdir "$(INTDIR)\Compression\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Compression\CompressionA.obj" $(SOURCE)

SOURCE="Compression\Compression_includeC.cpp"

"$(INTDIR)\Compression\Compression_includeC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Compression\$(NULL)" mkdir "$(INTDIR)\Compression\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Compression\Compression_includeC.obj" $(SOURCE)

SOURCE="Compression\Compression_includeA.cpp"

"$(INTDIR)\Compression\Compression_includeA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Compression\$(NULL)" mkdir "$(INTDIR)\Compression\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Compression\Compression_includeA.obj" $(SOURCE)

SOURCE="Compression\Compression_Manager.cpp"

"$(INTDIR)\Compression\Compression_Manager.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Compression\$(NULL)" mkdir "$(INTDIR)\Compression\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Compression\Compression_Manager.obj" $(SOURCE)

SOURCE="Compression\Base_Compressor.cpp"

"$(INTDIR)\Compression\Base_Compressor.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Compression\$(NULL)" mkdir "$(INTDIR)\Compression\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Compression\Base_Compressor.obj" $(SOURCE)

SOURCE="Compression\Compression.cpp"

"$(INTDIR)\Compression\Compression.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Compression\$(NULL)" mkdir "$(INTDIR)\Compression\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Compression\Compression.obj" $(SOURCE)

SOURCE="Compression\Compressor_Factory.cpp"

"$(INTDIR)\Compression\Compressor_Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Compression\$(NULL)" mkdir "$(INTDIR)\Compression\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Compression\Compressor_Factory.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="Compression\Compression.pidl"

InputPath=Compression\Compression.pidl

"Compression\CompressionC.h" "Compression\CompressionS.h" "Compression\CompressionA.h" "Compression\CompressionC.cpp" "Compression\CompressionA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Compression_Compression_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -Sci -GA -Wb,export_macro=TAO_Compression_Export -o Compression -Gp -Gd -Sal -Wb,export_macro=TAO_Compression_Export -Wb,export_include=tao/Compression/compression_export.h -Wb,include_guard=TAO_COMPRESSION_SAFE_INCLUDE -Wb,safe_include=tao/Compression/Compression.h "$(InputPath)"
<<

SOURCE="Compression\Compression_include.pidl"

InputPath=Compression\Compression_include.pidl

"Compression\Compression_includeC.h" "Compression\Compression_includeS.h" "Compression\Compression_includeA.h" "Compression\Compression_includeC.cpp" "Compression\Compression_includeA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Compression_Compression_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -St -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -Sci -GA -Wb,export_macro=TAO_Compression_Export -o Compression -Wb,export_include=tao/Compression/compression_export.h -Wb,unique_include=tao/Compression/Compression.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="Compression\Compression.pidl"

InputPath=Compression\Compression.pidl

"Compression\CompressionC.h" "Compression\CompressionS.h" "Compression\CompressionA.h" "Compression\CompressionC.cpp" "Compression\CompressionA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Compression_Compression_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -Sci -GA -Wb,export_macro=TAO_Compression_Export -o Compression -Gp -Gd -Sal -Wb,export_macro=TAO_Compression_Export -Wb,export_include=tao/Compression/compression_export.h -Wb,include_guard=TAO_COMPRESSION_SAFE_INCLUDE -Wb,safe_include=tao/Compression/Compression.h "$(InputPath)"
<<

SOURCE="Compression\Compression_include.pidl"

InputPath=Compression\Compression_include.pidl

"Compression\Compression_includeC.h" "Compression\Compression_includeS.h" "Compression\Compression_includeA.h" "Compression\Compression_includeC.cpp" "Compression\Compression_includeA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Compression_Compression_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -St -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -Sci -GA -Wb,export_macro=TAO_Compression_Export -o Compression -Wb,export_include=tao/Compression/compression_export.h -Wb,unique_include=tao/Compression/Compression.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="Compression\Compression.pidl"

InputPath=Compression\Compression.pidl

"Compression\CompressionC.h" "Compression\CompressionS.h" "Compression\CompressionA.h" "Compression\CompressionC.cpp" "Compression\CompressionA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Compression_Compression_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -Sci -GA -Wb,export_macro=TAO_Compression_Export -o Compression -Gp -Gd -Sal -Wb,export_macro=TAO_Compression_Export -Wb,export_include=tao/Compression/compression_export.h -Wb,include_guard=TAO_COMPRESSION_SAFE_INCLUDE -Wb,safe_include=tao/Compression/Compression.h "$(InputPath)"
<<

SOURCE="Compression\Compression_include.pidl"

InputPath=Compression\Compression_include.pidl

"Compression\Compression_includeC.h" "Compression\Compression_includeS.h" "Compression\Compression_includeA.h" "Compression\Compression_includeC.cpp" "Compression\Compression_includeA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Compression_Compression_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -St -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -Sci -GA -Wb,export_macro=TAO_Compression_Export -o Compression -Wb,export_include=tao/Compression/compression_export.h -Wb,unique_include=tao/Compression/Compression.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="Compression\Compression.pidl"

InputPath=Compression\Compression.pidl

"Compression\CompressionC.h" "Compression\CompressionS.h" "Compression\CompressionA.h" "Compression\CompressionC.cpp" "Compression\CompressionA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Compression_Compression_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -Sci -GA -Wb,export_macro=TAO_Compression_Export -o Compression -Gp -Gd -Sal -Wb,export_macro=TAO_Compression_Export -Wb,export_include=tao/Compression/compression_export.h -Wb,include_guard=TAO_COMPRESSION_SAFE_INCLUDE -Wb,safe_include=tao/Compression/Compression.h "$(InputPath)"
<<

SOURCE="Compression\Compression_include.pidl"

InputPath=Compression\Compression_include.pidl

"Compression\Compression_includeC.h" "Compression\Compression_includeS.h" "Compression\Compression_includeA.h" "Compression\Compression_includeC.cpp" "Compression\Compression_includeA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Compression_Compression_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -St -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -Sci -GA -Wb,export_macro=TAO_Compression_Export -o Compression -Wb,export_include=tao/Compression/compression_export.h -Wb,unique_include=tao/Compression/Compression.h "$(InputPath)"
<<

!ENDIF

SOURCE="Compression\TAO_Compression.rc"

"$(INTDIR)\Compression\TAO_Compression.res" : $(SOURCE)
	@if not exist "$(INTDIR)\Compression\$(NULL)" mkdir "$(INTDIR)\Compression\"
	$(RSC) /l 0x409 /fo"$(INTDIR)\Compression\TAO_Compression.res" /d NDEBUG /d WIN64 /d _CRT_SECURE_NO_WARNINGS /d _CRT_SECURE_NO_DEPRECATE /d _CRT_NONSTDC_NO_DEPRECATE /i "..\.." /i ".." $(SOURCE)



!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Compression.dep")
	@echo Using "Makefile.Compression.dep"
!ELSE
	@echo Warning: cannot find "Makefile.Compression.dep"
!ENDIF
!ENDIF

