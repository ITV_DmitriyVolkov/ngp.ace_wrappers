// $Id: Priority_Mapping.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/orbconf.h"

#if defined (TAO_HAS_CORBA_MESSAGING) && TAO_HAS_CORBA_MESSAGING != 0

#include "tao/RTCORBA/Priority_Mapping.h"

ACE_RCSID(RTCORBA,
          Priority_Mapping,
          "$Id: Priority_Mapping.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Priority_Mapping::~TAO_Priority_Mapping (void)
{
}

TAO_END_VERSIONED_NAMESPACE_DECL

#endif /* TAO_HAS_CORBA_MESSAGING && TAO_HAS_CORBA_MESSAGING != 0 */
