// $Id: Local_Servant_Base.cpp 935 2008-12-10 21:47:27Z mitza $

#include "tao/PortableServer/Local_Servant_Base.h"
#include "tao/SystemException.h"

ACE_RCSID (PortableServer,
           Local_Servant_Base,
           "$Id: Local_Servant_Base.cpp 935 2008-12-10 21:47:27Z mitza $")

#if !defined (__ACE_INLINE__)
# include "tao/PortableServer/Local_Servant_Base.inl"
#endif /* ! __ACE_INLINE__ */

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

void
TAO_Local_ServantBase::_dispatch (TAO_ServerRequest &, void *)
{
  throw ::CORBA::BAD_OPERATION ();
}

TAO_END_VERSIONED_NAMESPACE_DECL
