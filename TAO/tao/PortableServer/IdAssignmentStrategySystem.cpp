// $Id: IdAssignmentStrategySystem.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/PortableServer/IdAssignmentStrategySystem.h"

ACE_RCSID (PortableServer,
           Id_Assignment_Strategy,
           "$Id: IdAssignmentStrategySystem.cpp 14 2007-02-01 15:49:12Z mitza $")


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

namespace TAO
{
  namespace Portable_Server
  {
    char
    IdAssignmentStrategySystem::id_assignment_key_type (void) const
    {
      return 'S';
    }

    bool
    IdAssignmentStrategySystem::has_system_id (void) const
    {
      return true;
    }
  }
}

TAO_END_VERSIONED_NAMESPACE_DECL

ACE_FACTORY_NAMESPACE_DEFINE (
  ACE_Local_Service,
  IdAssignmentStrategySystem,
  TAO::Portable_Server::IdAssignmentStrategySystem)

ACE_STATIC_SVC_DEFINE (
  IdAssignmentStrategySystem,
  ACE_TEXT ("IdAssignmentStrategySystem"),
  ACE_SVC_OBJ_T,
  &ACE_SVC_NAME (IdAssignmentStrategySystem),
  ACE_Service_Type::DELETE_THIS | ACE_Service_Type::DELETE_OBJ,
  0)

