// $Id: ThreadStrategy.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/PortableServer/ThreadStrategy.h"

ACE_RCSID (PortableServer,
           ThreadStrategy,
           "$Id: ThreadStrategy.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

namespace TAO
{
  namespace Portable_Server
  {
    void
    ThreadStrategy::strategy_init (TAO_Root_POA * /*poa*/)
    {
    }

    void
    ThreadStrategy::strategy_cleanup (void)
    {
    }
  }
}

TAO_END_VERSIONED_NAMESPACE_DECL
