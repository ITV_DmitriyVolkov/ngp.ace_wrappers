// $Id: IdAssignmentStrategyUser.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/PortableServer/IdAssignmentStrategyUser.h"

ACE_RCSID (PortableServer,
           Id_Assignment_Strategy,
           "$Id: IdAssignmentStrategyUser.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

namespace TAO
{
  namespace Portable_Server
  {
    char
    IdAssignmentStrategyUser::id_assignment_key_type (void) const
    {
      return 'U';
    }

    bool
    IdAssignmentStrategyUser::has_system_id (void) const
    {
      return false;
    }
  }
}

TAO_END_VERSIONED_NAMESPACE_DECL

ACE_FACTORY_NAMESPACE_DEFINE (
  ACE_Local_Service,
  IdAssignmentStrategyUser,
  TAO::Portable_Server::IdAssignmentStrategyUser)

ACE_STATIC_SVC_DEFINE (
  IdAssignmentStrategyUser,
  ACE_TEXT ("IdAssignmentStrategyUser"),
  ACE_SVC_OBJ_T,
  &ACE_SVC_NAME (IdAssignmentStrategyUser),
  ACE_Service_Type::DELETE_THIS | ACE_Service_Type::DELETE_OBJ,
  0)
