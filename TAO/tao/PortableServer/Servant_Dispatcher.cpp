// $Id: Servant_Dispatcher.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/PortableServer/Servant_Dispatcher.h"

ACE_RCSID(PortableServer,
          Servant_Dispatcher,
          "$Id: Servant_Dispatcher.cpp 14 2007-02-01 15:49:12Z mitza $")


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Servant_Dispatcher::~TAO_Servant_Dispatcher (void)
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
