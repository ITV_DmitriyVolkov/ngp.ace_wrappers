// $Id: ImR_Client_Adapter.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/PortableServer/ImR_Client_Adapter.h"

ACE_RCSID (tao,
           ImR_Client_Adapter,
           "$Id: ImR_Client_Adapter.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

namespace TAO
{
  namespace Portable_Server
  {
    ImR_Client_Adapter::~ImR_Client_Adapter (void)
    {
    }
  }
}

TAO_END_VERSIONED_NAMESPACE_DECL
