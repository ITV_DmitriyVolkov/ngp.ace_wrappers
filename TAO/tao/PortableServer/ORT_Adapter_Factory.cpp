// $Id: ORT_Adapter_Factory.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/PortableServer/ORT_Adapter_Factory.h"

ACE_RCSID (PortableServer,
           ORT_Adapter_Factory,
           "$Id: ORT_Adapter_Factory.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

namespace TAO
{
  ORT_Adapter_Factory::~ORT_Adapter_Factory (void)
  {
  }
}

TAO_END_VERSIONED_NAMESPACE_DECL
