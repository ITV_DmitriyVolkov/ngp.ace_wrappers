// $Id: StrategyFactory.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/PortableServer/StrategyFactory.h"
#include "ace/Dynamic_Service.h"

ACE_RCSID (PortableServer,
           StrategyFactory,
           "$Id: StrategyFactory.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

namespace TAO
{
  namespace Portable_Server
  {
    StrategyFactory::~StrategyFactory (void)
    {
    }
  } /* namespace Portable_Server */
} /* namespace TAO */

TAO_END_VERSIONED_NAMESPACE_DECL
