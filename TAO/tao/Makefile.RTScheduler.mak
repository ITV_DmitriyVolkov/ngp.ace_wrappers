# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.RTScheduler.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "RTScheduling\RTSchedulerC.h" "RTScheduling\RTSchedulerS.h" "RTScheduling\RTSchedulerA.h" "RTScheduling\RTSchedulerC.cpp" "RTScheduling\RTScheduler_includeC.h" "RTScheduling\RTScheduler_includeS.h" "RTScheduling\RTScheduler_includeA.h" "RTScheduling\RTScheduler_includeC.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\RTScheduler\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_RTSchedulerd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_RTSCHEDULER_BUILD_DLL -f "Makefile.RTScheduler.dep" "RTScheduling\RTSchedulerC.cpp" "RTScheduling\RTScheduler_includeC.cpp" "RTScheduling\Request_Interceptor.cpp" "RTScheduling\Current.cpp" "RTScheduling\RTScheduler_Initializer.cpp" "RTScheduling\Distributable_Thread.cpp" "RTScheduling\RTScheduler_Manager.cpp" "RTScheduling\RTScheduler.cpp" "RTScheduling\RTScheduler_Loader.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_RTSchedulerd.pdb"
	-@del /f/q "..\..\lib\TAO_RTSchedulerd.dll"
	-@del /f/q "$(OUTDIR)\TAO_RTSchedulerd.lib"
	-@del /f/q "$(OUTDIR)\TAO_RTSchedulerd.exp"
	-@del /f/q "$(OUTDIR)\TAO_RTSchedulerd.ilk"
	-@del /f/q "RTScheduling\RTSchedulerC.h"
	-@del /f/q "RTScheduling\RTSchedulerS.h"
	-@del /f/q "RTScheduling\RTSchedulerA.h"
	-@del /f/q "RTScheduling\RTSchedulerC.cpp"
	-@del /f/q "RTScheduling\RTScheduler_includeC.h"
	-@del /f/q "RTScheduling\RTScheduler_includeS.h"
	-@del /f/q "RTScheduling\RTScheduler_includeA.h"
	-@del /f/q "RTScheduling\RTScheduler_includeC.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\RTScheduler\$(NULL)" mkdir "Debug\RTScheduler"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_RTSCHEDULER_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_CodecFactoryd.lib TAO_PId.lib TAO_RTCORBAd.lib TAO_PortableServerd.lib TAO_PI_Serverd.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\TAO_RTSchedulerd.pdb" /machine:IA64 /out:"..\..\lib\TAO_RTSchedulerd.dll" /implib:"$(OUTDIR)\TAO_RTSchedulerd.lib"
LINK32_OBJS= \
	"$(INTDIR)\RTScheduling\TAO_RTScheduler.res" \
	"$(INTDIR)\RTScheduling\RTSchedulerC.obj" \
	"$(INTDIR)\RTScheduling\RTScheduler_includeC.obj" \
	"$(INTDIR)\RTScheduling\Request_Interceptor.obj" \
	"$(INTDIR)\RTScheduling\Current.obj" \
	"$(INTDIR)\RTScheduling\RTScheduler_Initializer.obj" \
	"$(INTDIR)\RTScheduling\Distributable_Thread.obj" \
	"$(INTDIR)\RTScheduling\RTScheduler_Manager.obj" \
	"$(INTDIR)\RTScheduling\RTScheduler.obj" \
	"$(INTDIR)\RTScheduling\RTScheduler_Loader.obj"

"..\..\lib\TAO_RTSchedulerd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_RTSchedulerd.dll.manifest" mt.exe -manifest "..\..\lib\TAO_RTSchedulerd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\RTScheduler\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_RTScheduler.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_RTSCHEDULER_BUILD_DLL -f "Makefile.RTScheduler.dep" "RTScheduling\RTSchedulerC.cpp" "RTScheduling\RTScheduler_includeC.cpp" "RTScheduling\Request_Interceptor.cpp" "RTScheduling\Current.cpp" "RTScheduling\RTScheduler_Initializer.cpp" "RTScheduling\Distributable_Thread.cpp" "RTScheduling\RTScheduler_Manager.cpp" "RTScheduling\RTScheduler.cpp" "RTScheduling\RTScheduler_Loader.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\TAO_RTScheduler.dll"
	-@del /f/q "$(OUTDIR)\TAO_RTScheduler.lib"
	-@del /f/q "$(OUTDIR)\TAO_RTScheduler.exp"
	-@del /f/q "$(OUTDIR)\TAO_RTScheduler.ilk"
	-@del /f/q "RTScheduling\RTSchedulerC.h"
	-@del /f/q "RTScheduling\RTSchedulerS.h"
	-@del /f/q "RTScheduling\RTSchedulerA.h"
	-@del /f/q "RTScheduling\RTSchedulerC.cpp"
	-@del /f/q "RTScheduling\RTScheduler_includeC.h"
	-@del /f/q "RTScheduling\RTScheduler_includeS.h"
	-@del /f/q "RTScheduling\RTScheduler_includeA.h"
	-@del /f/q "RTScheduling\RTScheduler_includeC.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\RTScheduler\$(NULL)" mkdir "Release\RTScheduler"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_RTSCHEDULER_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_CodecFactory.lib TAO_PI.lib TAO_RTCORBA.lib TAO_PortableServer.lib TAO_PI_Server.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\TAO_RTScheduler.dll" /implib:"$(OUTDIR)\TAO_RTScheduler.lib"
LINK32_OBJS= \
	"$(INTDIR)\RTScheduling\TAO_RTScheduler.res" \
	"$(INTDIR)\RTScheduling\RTSchedulerC.obj" \
	"$(INTDIR)\RTScheduling\RTScheduler_includeC.obj" \
	"$(INTDIR)\RTScheduling\Request_Interceptor.obj" \
	"$(INTDIR)\RTScheduling\Current.obj" \
	"$(INTDIR)\RTScheduling\RTScheduler_Initializer.obj" \
	"$(INTDIR)\RTScheduling\Distributable_Thread.obj" \
	"$(INTDIR)\RTScheduling\RTScheduler_Manager.obj" \
	"$(INTDIR)\RTScheduling\RTScheduler.obj" \
	"$(INTDIR)\RTScheduling\RTScheduler_Loader.obj"

"..\..\lib\TAO_RTScheduler.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_RTScheduler.dll.manifest" mt.exe -manifest "..\..\lib\TAO_RTScheduler.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\RTScheduler\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_RTSchedulersd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.RTScheduler.dep" "RTScheduling\RTSchedulerC.cpp" "RTScheduling\RTScheduler_includeC.cpp" "RTScheduling\Request_Interceptor.cpp" "RTScheduling\Current.cpp" "RTScheduling\RTScheduler_Initializer.cpp" "RTScheduling\Distributable_Thread.cpp" "RTScheduling\RTScheduler_Manager.cpp" "RTScheduling\RTScheduler.cpp" "RTScheduling\RTScheduler_Loader.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_RTSchedulersd.lib"
	-@del /f/q "$(OUTDIR)\TAO_RTSchedulersd.exp"
	-@del /f/q "$(OUTDIR)\TAO_RTSchedulersd.ilk"
	-@del /f/q "..\..\lib\TAO_RTSchedulersd.pdb"
	-@del /f/q "RTScheduling\RTSchedulerC.h"
	-@del /f/q "RTScheduling\RTSchedulerS.h"
	-@del /f/q "RTScheduling\RTSchedulerA.h"
	-@del /f/q "RTScheduling\RTSchedulerC.cpp"
	-@del /f/q "RTScheduling\RTScheduler_includeC.h"
	-@del /f/q "RTScheduling\RTScheduler_includeS.h"
	-@del /f/q "RTScheduling\RTScheduler_includeA.h"
	-@del /f/q "RTScheduling\RTScheduler_includeC.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\RTScheduler\$(NULL)" mkdir "Static_Debug\RTScheduler"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"..\..\lib\TAO_RTSchedulersd.pdb" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_RTSchedulersd.lib"
LINK32_OBJS= \
	"$(INTDIR)\RTScheduling\RTSchedulerC.obj" \
	"$(INTDIR)\RTScheduling\RTScheduler_includeC.obj" \
	"$(INTDIR)\RTScheduling\Request_Interceptor.obj" \
	"$(INTDIR)\RTScheduling\Current.obj" \
	"$(INTDIR)\RTScheduling\RTScheduler_Initializer.obj" \
	"$(INTDIR)\RTScheduling\Distributable_Thread.obj" \
	"$(INTDIR)\RTScheduling\RTScheduler_Manager.obj" \
	"$(INTDIR)\RTScheduling\RTScheduler.obj" \
	"$(INTDIR)\RTScheduling\RTScheduler_Loader.obj"

"$(OUTDIR)\TAO_RTSchedulersd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_RTSchedulersd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_RTSchedulersd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\RTScheduler\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_RTSchedulers.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.RTScheduler.dep" "RTScheduling\RTSchedulerC.cpp" "RTScheduling\RTScheduler_includeC.cpp" "RTScheduling\Request_Interceptor.cpp" "RTScheduling\Current.cpp" "RTScheduling\RTScheduler_Initializer.cpp" "RTScheduling\Distributable_Thread.cpp" "RTScheduling\RTScheduler_Manager.cpp" "RTScheduling\RTScheduler.cpp" "RTScheduling\RTScheduler_Loader.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_RTSchedulers.lib"
	-@del /f/q "$(OUTDIR)\TAO_RTSchedulers.exp"
	-@del /f/q "$(OUTDIR)\TAO_RTSchedulers.ilk"
	-@del /f/q "RTScheduling\RTSchedulerC.h"
	-@del /f/q "RTScheduling\RTSchedulerS.h"
	-@del /f/q "RTScheduling\RTSchedulerA.h"
	-@del /f/q "RTScheduling\RTSchedulerC.cpp"
	-@del /f/q "RTScheduling\RTScheduler_includeC.h"
	-@del /f/q "RTScheduling\RTScheduler_includeS.h"
	-@del /f/q "RTScheduling\RTScheduler_includeA.h"
	-@del /f/q "RTScheduling\RTScheduler_includeC.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\RTScheduler\$(NULL)" mkdir "Static_Release\RTScheduler"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_RTSchedulers.lib"
LINK32_OBJS= \
	"$(INTDIR)\RTScheduling\RTSchedulerC.obj" \
	"$(INTDIR)\RTScheduling\RTScheduler_includeC.obj" \
	"$(INTDIR)\RTScheduling\Request_Interceptor.obj" \
	"$(INTDIR)\RTScheduling\Current.obj" \
	"$(INTDIR)\RTScheduling\RTScheduler_Initializer.obj" \
	"$(INTDIR)\RTScheduling\Distributable_Thread.obj" \
	"$(INTDIR)\RTScheduling\RTScheduler_Manager.obj" \
	"$(INTDIR)\RTScheduling\RTScheduler.obj" \
	"$(INTDIR)\RTScheduling\RTScheduler_Loader.obj"

"$(OUTDIR)\TAO_RTSchedulers.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_RTSchedulers.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_RTSchedulers.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.RTScheduler.dep")
!INCLUDE "Makefile.RTScheduler.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="RTScheduling\RTSchedulerC.cpp"

"$(INTDIR)\RTScheduling\RTSchedulerC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTScheduling\$(NULL)" mkdir "$(INTDIR)\RTScheduling\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTScheduling\RTSchedulerC.obj" $(SOURCE)

SOURCE="RTScheduling\RTScheduler_includeC.cpp"

"$(INTDIR)\RTScheduling\RTScheduler_includeC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTScheduling\$(NULL)" mkdir "$(INTDIR)\RTScheduling\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTScheduling\RTScheduler_includeC.obj" $(SOURCE)

SOURCE="RTScheduling\Request_Interceptor.cpp"

"$(INTDIR)\RTScheduling\Request_Interceptor.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTScheduling\$(NULL)" mkdir "$(INTDIR)\RTScheduling\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTScheduling\Request_Interceptor.obj" $(SOURCE)

SOURCE="RTScheduling\Current.cpp"

"$(INTDIR)\RTScheduling\Current.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTScheduling\$(NULL)" mkdir "$(INTDIR)\RTScheduling\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTScheduling\Current.obj" $(SOURCE)

SOURCE="RTScheduling\RTScheduler_Initializer.cpp"

"$(INTDIR)\RTScheduling\RTScheduler_Initializer.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTScheduling\$(NULL)" mkdir "$(INTDIR)\RTScheduling\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTScheduling\RTScheduler_Initializer.obj" $(SOURCE)

SOURCE="RTScheduling\Distributable_Thread.cpp"

"$(INTDIR)\RTScheduling\Distributable_Thread.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTScheduling\$(NULL)" mkdir "$(INTDIR)\RTScheduling\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTScheduling\Distributable_Thread.obj" $(SOURCE)

SOURCE="RTScheduling\RTScheduler_Manager.cpp"

"$(INTDIR)\RTScheduling\RTScheduler_Manager.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTScheduling\$(NULL)" mkdir "$(INTDIR)\RTScheduling\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTScheduling\RTScheduler_Manager.obj" $(SOURCE)

SOURCE="RTScheduling\RTScheduler.cpp"

"$(INTDIR)\RTScheduling\RTScheduler.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTScheduling\$(NULL)" mkdir "$(INTDIR)\RTScheduling\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTScheduling\RTScheduler.obj" $(SOURCE)

SOURCE="RTScheduling\RTScheduler_Loader.cpp"

"$(INTDIR)\RTScheduling\RTScheduler_Loader.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTScheduling\$(NULL)" mkdir "$(INTDIR)\RTScheduling\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTScheduling\RTScheduler_Loader.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="RTScheduling\RTScheduler.pidl"

InputPath=RTScheduling\RTScheduler.pidl

"RTScheduling\RTSchedulerC.h" "RTScheduling\RTSchedulerS.h" "RTScheduling\RTSchedulerA.h" "RTScheduling\RTSchedulerC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-RTScheduling_RTScheduler_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GX -Wb,export_macro=TAO_RTScheduler_Export -Wb,export_include=tao/RTScheduling/rtscheduler_export.h -Wb,include_guard=TAO_RTSCHEDULER_SAFE_INCLUDE -Wb,safe_include=tao/RTScheduling/RTScheduler.h -o RTScheduling "$(InputPath)"
<<

SOURCE="RTScheduling\RTScheduler_include.pidl"

InputPath=RTScheduling\RTScheduler_include.pidl

"RTScheduling\RTScheduler_includeC.h" "RTScheduling\RTScheduler_includeS.h" "RTScheduling\RTScheduler_includeA.h" "RTScheduling\RTScheduler_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-RTScheduling_RTScheduler_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GX -Sci -Wb,export_macro=TAO_RTScheduler_Export -Wb,export_include=tao/RTScheduling/rtscheduler_export.h -Wb,unique_include=tao/RTScheduling/RTScheduler.h -o RTScheduling "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="RTScheduling\RTScheduler.pidl"

InputPath=RTScheduling\RTScheduler.pidl

"RTScheduling\RTSchedulerC.h" "RTScheduling\RTSchedulerS.h" "RTScheduling\RTSchedulerA.h" "RTScheduling\RTSchedulerC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-RTScheduling_RTScheduler_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GX -Wb,export_macro=TAO_RTScheduler_Export -Wb,export_include=tao/RTScheduling/rtscheduler_export.h -Wb,include_guard=TAO_RTSCHEDULER_SAFE_INCLUDE -Wb,safe_include=tao/RTScheduling/RTScheduler.h -o RTScheduling "$(InputPath)"
<<

SOURCE="RTScheduling\RTScheduler_include.pidl"

InputPath=RTScheduling\RTScheduler_include.pidl

"RTScheduling\RTScheduler_includeC.h" "RTScheduling\RTScheduler_includeS.h" "RTScheduling\RTScheduler_includeA.h" "RTScheduling\RTScheduler_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-RTScheduling_RTScheduler_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GX -Sci -Wb,export_macro=TAO_RTScheduler_Export -Wb,export_include=tao/RTScheduling/rtscheduler_export.h -Wb,unique_include=tao/RTScheduling/RTScheduler.h -o RTScheduling "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="RTScheduling\RTScheduler.pidl"

InputPath=RTScheduling\RTScheduler.pidl

"RTScheduling\RTSchedulerC.h" "RTScheduling\RTSchedulerS.h" "RTScheduling\RTSchedulerA.h" "RTScheduling\RTSchedulerC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-RTScheduling_RTScheduler_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GX -Wb,export_macro=TAO_RTScheduler_Export -Wb,export_include=tao/RTScheduling/rtscheduler_export.h -Wb,include_guard=TAO_RTSCHEDULER_SAFE_INCLUDE -Wb,safe_include=tao/RTScheduling/RTScheduler.h -o RTScheduling "$(InputPath)"
<<

SOURCE="RTScheduling\RTScheduler_include.pidl"

InputPath=RTScheduling\RTScheduler_include.pidl

"RTScheduling\RTScheduler_includeC.h" "RTScheduling\RTScheduler_includeS.h" "RTScheduling\RTScheduler_includeA.h" "RTScheduling\RTScheduler_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-RTScheduling_RTScheduler_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GX -Sci -Wb,export_macro=TAO_RTScheduler_Export -Wb,export_include=tao/RTScheduling/rtscheduler_export.h -Wb,unique_include=tao/RTScheduling/RTScheduler.h -o RTScheduling "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="RTScheduling\RTScheduler.pidl"

InputPath=RTScheduling\RTScheduler.pidl

"RTScheduling\RTSchedulerC.h" "RTScheduling\RTSchedulerS.h" "RTScheduling\RTSchedulerA.h" "RTScheduling\RTSchedulerC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-RTScheduling_RTScheduler_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GX -Wb,export_macro=TAO_RTScheduler_Export -Wb,export_include=tao/RTScheduling/rtscheduler_export.h -Wb,include_guard=TAO_RTSCHEDULER_SAFE_INCLUDE -Wb,safe_include=tao/RTScheduling/RTScheduler.h -o RTScheduling "$(InputPath)"
<<

SOURCE="RTScheduling\RTScheduler_include.pidl"

InputPath=RTScheduling\RTScheduler_include.pidl

"RTScheduling\RTScheduler_includeC.h" "RTScheduling\RTScheduler_includeS.h" "RTScheduling\RTScheduler_includeA.h" "RTScheduling\RTScheduler_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-RTScheduling_RTScheduler_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sorb -GX -Sci -Wb,export_macro=TAO_RTScheduler_Export -Wb,export_include=tao/RTScheduling/rtscheduler_export.h -Wb,unique_include=tao/RTScheduling/RTScheduler.h -o RTScheduling "$(InputPath)"
<<

!ENDIF

SOURCE="RTScheduling\TAO_RTScheduler.rc"

"$(INTDIR)\RTScheduling\TAO_RTScheduler.res" : $(SOURCE)
	@if not exist "$(INTDIR)\RTScheduling\$(NULL)" mkdir "$(INTDIR)\RTScheduling\"
	$(RSC) /l 0x409 /fo"$(INTDIR)\RTScheduling\TAO_RTScheduler.res" /d NDEBUG /d WIN64 /d _CRT_SECURE_NO_WARNINGS /d _CRT_SECURE_NO_DEPRECATE /d _CRT_NONSTDC_NO_DEPRECATE /i "..\.." /i ".." $(SOURCE)



!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.RTScheduler.dep")
	@echo Using "Makefile.RTScheduler.dep"
!ELSE
	@echo Warning: cannot find "Makefile.RTScheduler.dep"
!ENDIF
!ENDIF

