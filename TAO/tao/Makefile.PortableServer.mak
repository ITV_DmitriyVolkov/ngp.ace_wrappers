# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.PortableServer.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "PortableServer\ForwardRequestC.h" "PortableServer\ForwardRequestS.h" "PortableServer\ForwardRequestA.h" "PortableServer\ForwardRequestC.cpp" "PortableServer\ForwardRequestA.cpp" "PortableServer\AdapterActivatorC.h" "PortableServer\AdapterActivatorS.h" "PortableServer\AdapterActivatorA.h" "PortableServer\AdapterActivatorC.cpp" "PortableServer\AdapterActivatorA.cpp" "PortableServer\IdAssignmentPolicyC.h" "PortableServer\IdAssignmentPolicyS.h" "PortableServer\IdAssignmentPolicyA.h" "PortableServer\IdAssignmentPolicyC.cpp" "PortableServer\IdAssignmentPolicyA.cpp" "PortableServer\IdUniquenessPolicyC.h" "PortableServer\IdUniquenessPolicyS.h" "PortableServer\IdUniquenessPolicyA.h" "PortableServer\IdUniquenessPolicyC.cpp" "PortableServer\IdUniquenessPolicyA.cpp" "PortableServer\ImplicitActivationPolicyC.h" "PortableServer\ImplicitActivationPolicyS.h" "PortableServer\ImplicitActivationPolicyA.h" "PortableServer\ImplicitActivationPolicyC.cpp" "PortableServer\ImplicitActivationPolicyA.cpp" "PortableServer\LifespanPolicyC.h" "PortableServer\LifespanPolicyS.h" "PortableServer\LifespanPolicyA.h" "PortableServer\LifespanPolicyC.cpp" "PortableServer\LifespanPolicyA.cpp" "PortableServer\RequestProcessingPolicyC.h" "PortableServer\RequestProcessingPolicyS.h" "PortableServer\RequestProcessingPolicyA.h" "PortableServer\RequestProcessingPolicyC.cpp" "PortableServer\RequestProcessingPolicyA.cpp" "PortableServer\ServantActivatorC.h" "PortableServer\ServantActivatorS.h" "PortableServer\ServantActivatorA.h" "PortableServer\ServantActivatorC.cpp" "PortableServer\ServantActivatorA.cpp" "PortableServer\ServantLocatorC.h" "PortableServer\ServantLocatorS.h" "PortableServer\ServantLocatorA.h" "PortableServer\ServantLocatorC.cpp" "PortableServer\ServantLocatorA.cpp" "PortableServer\ServantManagerC.h" "PortableServer\ServantManagerS.h" "PortableServer\ServantManagerA.h" "PortableServer\ServantManagerC.cpp" "PortableServer\ServantManagerA.cpp" "PortableServer\ServantRetentionPolicyC.h" "PortableServer\ServantRetentionPolicyS.h" "PortableServer\ServantRetentionPolicyA.h" "PortableServer\ServantRetentionPolicyC.cpp" "PortableServer\ServantRetentionPolicyA.cpp" "PortableServer\ThreadPolicyC.h" "PortableServer\ThreadPolicyS.h" "PortableServer\ThreadPolicyA.h" "PortableServer\ThreadPolicyC.cpp" "PortableServer\ThreadPolicyA.cpp" "PortableServer\POAManagerC.h" "PortableServer\POAManagerS.h" "PortableServer\POAManagerC.cpp" "PortableServer\POAManagerFactoryC.h" "PortableServer\POAManagerFactoryS.h" "PortableServer\POAManagerFactoryC.cpp" "PortableServer\POAC.h" "PortableServer\POAS.h" "PortableServer\POAC.cpp" "PortableServer\PS_CurrentC.h" "PortableServer\PS_CurrentS.h" "PortableServer\PS_CurrentC.cpp" "PortableServer\PortableServer_includeC.h" "PortableServer\PortableServer_includeS.h" "PortableServer\PortableServer_includeA.h" "PortableServer\PortableServer_includeC.cpp" "PortableServer\PortableServerC.h" "PortableServer\PortableServerS.h" "PortableServer\PortableServerA.h" "PortableServer\PortableServerC.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\PortableServer\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_PortableServerd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_HAS_VALUETYPE_OUT_INDIRECTION -DTAO_PORTABLESERVER_BUILD_DLL -f "Makefile.PortableServer.dep" "PortableServer\ForwardRequestC.cpp" "PortableServer\ForwardRequestA.cpp" "PortableServer\AdapterActivatorC.cpp" "PortableServer\AdapterActivatorA.cpp" "PortableServer\IdAssignmentPolicyC.cpp" "PortableServer\IdAssignmentPolicyA.cpp" "PortableServer\IdUniquenessPolicyC.cpp" "PortableServer\IdUniquenessPolicyA.cpp" "PortableServer\ImplicitActivationPolicyC.cpp" "PortableServer\ImplicitActivationPolicyA.cpp" "PortableServer\LifespanPolicyC.cpp" "PortableServer\LifespanPolicyA.cpp" "PortableServer\RequestProcessingPolicyC.cpp" "PortableServer\RequestProcessingPolicyA.cpp" "PortableServer\ServantActivatorC.cpp" "PortableServer\ServantActivatorA.cpp" "PortableServer\ServantLocatorC.cpp" "PortableServer\ServantLocatorA.cpp" "PortableServer\ServantManagerC.cpp" "PortableServer\ServantManagerA.cpp" "PortableServer\ServantRetentionPolicyC.cpp" "PortableServer\ServantRetentionPolicyA.cpp" "PortableServer\ThreadPolicyC.cpp" "PortableServer\ThreadPolicyA.cpp" "PortableServer\POAManagerC.cpp" "PortableServer\POAManagerFactoryC.cpp" "PortableServer\POAC.cpp" "PortableServer\PS_CurrentC.cpp" "PortableServer\PortableServer_includeC.cpp" "PortableServer\PortableServerC.cpp" "PortableServer\IdUniquenessPolicy.cpp" "PortableServer\StrategyFactory.cpp" "PortableServer\LifespanStrategyTransientFactoryImpl.cpp" "PortableServer\Collocated_Arguments_Converter.cpp" "PortableServer\RequestProcessingStrategyDefaultServant.cpp" "PortableServer\RequestProcessingStrategyDefaultServantFI.cpp" "PortableServer\ServantRetentionStrategyRetainFactoryImpl.cpp" "PortableServer\IdAssignmentStrategy.cpp" "PortableServer\ThreadStrategySingle.cpp" "PortableServer\Operation_Table_Binary_Search.cpp" "PortableServer\Acceptor_Filter_Factory.cpp" "PortableServer\Default_Acceptor_Filter.cpp" "PortableServer\Local_Servant_Base.cpp" "PortableServer\Root_POA.cpp" "PortableServer\PS_ForwardC.cpp" "PortableServer\ImplicitActivationStrategyFactoryImpl.cpp" "PortableServer\LifespanStrategyPersistent.cpp" "PortableServer\POA_Current.cpp" "PortableServer\Servant_Base.cpp" "PortableServer\Upcall_Command.cpp" "PortableServer\RequestProcessingPolicy.cpp" "PortableServer\RequestProcessingStrategyServantActivatorFI.cpp" "PortableServer\IdUniquenessStrategyFactoryImpl.cpp" "PortableServer\POAManagerFactory.cpp" "PortableServer\Operation_Table.cpp" "PortableServer\ORT_Adapter_Factory.cpp" "PortableServer\Network_Priority_Hook.cpp" "PortableServer\LifespanStrategy.cpp" "PortableServer\ImplicitActivationPolicy.cpp" "PortableServer\ThreadStrategySingleFactoryImpl.cpp" "PortableServer\Servant_Dispatcher.cpp" "PortableServer\RequestProcessingStrategyServantLocatorFI.cpp" "PortableServer\Non_Servant_Upcall.cpp" "PortableServer\POA_Current_Factory.cpp" "PortableServer\ServantRetentionStrategyRetain.cpp" "PortableServer\IdAssignmentStrategyFactoryImpl.cpp" "PortableServer\Operation_Table_Perfect_Hash.cpp" "PortableServer\Adapter_Activator.cpp" "PortableServer\ServantRetentionStrategyFactoryImpl.cpp" "PortableServer\LifespanStrategyTransient.cpp" "PortableServer\RequestProcessingStrategyServantLocator.cpp" "PortableServer\ThreadStrategyORBControl.cpp" "PortableServer\Active_Policy_Strategies.cpp" "PortableServer\LifespanStrategyPersistentFactoryImpl.cpp" "PortableServer\RequestProcessingStrategyAOMOnlyFactoryImpl.cpp" "PortableServer\IdUniquenessStrategyUnique.cpp" "PortableServer\RequestProcessingStrategyServantManager.cpp" "PortableServer\ServantRetentionStrategyNonRetainFactoryImpl.cpp" "PortableServer\Upcall_Wrapper.cpp" "PortableServer\RequestProcessingStrategyServantActivator.cpp" "PortableServer\LifespanStrategyFactoryImpl.cpp" "PortableServer\Servant_Upcall.cpp" "PortableServer\POA_Cached_Policies.cpp" "PortableServer\PortableServer_Functions.cpp" "PortableServer\Active_Object_Map_Entry.cpp" "PortableServer\IdUniquenessStrategyUniqueFactoryImpl.cpp" "PortableServer\PortableServer.cpp" "PortableServer\Active_Object_Map.cpp" "PortableServer\ThreadPolicy.cpp" "PortableServer\ThreadStrategy.cpp" "PortableServer\RequestProcessingStrategyFactoryImpl.cpp" "PortableServer\Direct_Collocation_Upcall_Wrapper.cpp" "PortableServer\Operation_Table_Dynamic_Hash.cpp" "PortableServer\ImplicitActivationStrategyExplicit.cpp" "PortableServer\Regular_POA.cpp" "PortableServer\Creation_Time.cpp" "PortableServer\Operation_Table_Linear_Search.cpp" "PortableServer\PolicyS.cpp" "PortableServer\Object_Adapter.cpp" "PortableServer\RequestProcessingStrategy.cpp" "PortableServer\IdAssignmentStrategySystem.cpp" "PortableServer\Default_Servant_Dispatcher.cpp" "PortableServer\ServantRetentionStrategyNonRetain.cpp" "PortableServer\PS_ForwardA.cpp" "PortableServer\Key_Adapters.cpp" "PortableServer\POA_Policy_Set.cpp" "PortableServer\IdUniquenessStrategyMultiple.cpp" "PortableServer\PortableServer_WFunctions.cpp" "PortableServer\LifespanPolicy.cpp" "PortableServer\IdAssignmentPolicy.cpp" "PortableServer\ThreadStrategyFactoryImpl.cpp" "PortableServer\IdAssignmentStrategyUser.cpp" "PortableServer\ImplicitActivationStrategy.cpp" "PortableServer\Default_Policy_Validator.cpp" "PortableServer\POAManager.cpp" "PortableServer\ImplicitActivationStrategyImplicit.cpp" "PortableServer\POA_Current_Impl.cpp" "PortableServer\ImR_Client_Adapter.cpp" "PortableServer\Object_Adapter_Factory.cpp" "PortableServer\RequestProcessingStrategyAOMOnly.cpp" "PortableServer\ServantRetentionPolicy.cpp" "PortableServer\POA_Guard.cpp" "PortableServer\Collocated_Object_Proxy_Broker.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_PortableServerd.pdb"
	-@del /f/q "..\..\lib\TAO_PortableServerd.dll"
	-@del /f/q "$(OUTDIR)\TAO_PortableServerd.lib"
	-@del /f/q "$(OUTDIR)\TAO_PortableServerd.exp"
	-@del /f/q "$(OUTDIR)\TAO_PortableServerd.ilk"
	-@del /f/q "PortableServer\ForwardRequestC.h"
	-@del /f/q "PortableServer\ForwardRequestS.h"
	-@del /f/q "PortableServer\ForwardRequestA.h"
	-@del /f/q "PortableServer\ForwardRequestC.cpp"
	-@del /f/q "PortableServer\ForwardRequestA.cpp"
	-@del /f/q "PortableServer\AdapterActivatorC.h"
	-@del /f/q "PortableServer\AdapterActivatorS.h"
	-@del /f/q "PortableServer\AdapterActivatorA.h"
	-@del /f/q "PortableServer\AdapterActivatorC.cpp"
	-@del /f/q "PortableServer\AdapterActivatorA.cpp"
	-@del /f/q "PortableServer\IdAssignmentPolicyC.h"
	-@del /f/q "PortableServer\IdAssignmentPolicyS.h"
	-@del /f/q "PortableServer\IdAssignmentPolicyA.h"
	-@del /f/q "PortableServer\IdAssignmentPolicyC.cpp"
	-@del /f/q "PortableServer\IdAssignmentPolicyA.cpp"
	-@del /f/q "PortableServer\IdUniquenessPolicyC.h"
	-@del /f/q "PortableServer\IdUniquenessPolicyS.h"
	-@del /f/q "PortableServer\IdUniquenessPolicyA.h"
	-@del /f/q "PortableServer\IdUniquenessPolicyC.cpp"
	-@del /f/q "PortableServer\IdUniquenessPolicyA.cpp"
	-@del /f/q "PortableServer\ImplicitActivationPolicyC.h"
	-@del /f/q "PortableServer\ImplicitActivationPolicyS.h"
	-@del /f/q "PortableServer\ImplicitActivationPolicyA.h"
	-@del /f/q "PortableServer\ImplicitActivationPolicyC.cpp"
	-@del /f/q "PortableServer\ImplicitActivationPolicyA.cpp"
	-@del /f/q "PortableServer\LifespanPolicyC.h"
	-@del /f/q "PortableServer\LifespanPolicyS.h"
	-@del /f/q "PortableServer\LifespanPolicyA.h"
	-@del /f/q "PortableServer\LifespanPolicyC.cpp"
	-@del /f/q "PortableServer\LifespanPolicyA.cpp"
	-@del /f/q "PortableServer\RequestProcessingPolicyC.h"
	-@del /f/q "PortableServer\RequestProcessingPolicyS.h"
	-@del /f/q "PortableServer\RequestProcessingPolicyA.h"
	-@del /f/q "PortableServer\RequestProcessingPolicyC.cpp"
	-@del /f/q "PortableServer\RequestProcessingPolicyA.cpp"
	-@del /f/q "PortableServer\ServantActivatorC.h"
	-@del /f/q "PortableServer\ServantActivatorS.h"
	-@del /f/q "PortableServer\ServantActivatorA.h"
	-@del /f/q "PortableServer\ServantActivatorC.cpp"
	-@del /f/q "PortableServer\ServantActivatorA.cpp"
	-@del /f/q "PortableServer\ServantLocatorC.h"
	-@del /f/q "PortableServer\ServantLocatorS.h"
	-@del /f/q "PortableServer\ServantLocatorA.h"
	-@del /f/q "PortableServer\ServantLocatorC.cpp"
	-@del /f/q "PortableServer\ServantLocatorA.cpp"
	-@del /f/q "PortableServer\ServantManagerC.h"
	-@del /f/q "PortableServer\ServantManagerS.h"
	-@del /f/q "PortableServer\ServantManagerA.h"
	-@del /f/q "PortableServer\ServantManagerC.cpp"
	-@del /f/q "PortableServer\ServantManagerA.cpp"
	-@del /f/q "PortableServer\ServantRetentionPolicyC.h"
	-@del /f/q "PortableServer\ServantRetentionPolicyS.h"
	-@del /f/q "PortableServer\ServantRetentionPolicyA.h"
	-@del /f/q "PortableServer\ServantRetentionPolicyC.cpp"
	-@del /f/q "PortableServer\ServantRetentionPolicyA.cpp"
	-@del /f/q "PortableServer\ThreadPolicyC.h"
	-@del /f/q "PortableServer\ThreadPolicyS.h"
	-@del /f/q "PortableServer\ThreadPolicyA.h"
	-@del /f/q "PortableServer\ThreadPolicyC.cpp"
	-@del /f/q "PortableServer\ThreadPolicyA.cpp"
	-@del /f/q "PortableServer\POAManagerC.h"
	-@del /f/q "PortableServer\POAManagerS.h"
	-@del /f/q "PortableServer\POAManagerC.cpp"
	-@del /f/q "PortableServer\POAManagerFactoryC.h"
	-@del /f/q "PortableServer\POAManagerFactoryS.h"
	-@del /f/q "PortableServer\POAManagerFactoryC.cpp"
	-@del /f/q "PortableServer\POAC.h"
	-@del /f/q "PortableServer\POAS.h"
	-@del /f/q "PortableServer\POAC.cpp"
	-@del /f/q "PortableServer\PS_CurrentC.h"
	-@del /f/q "PortableServer\PS_CurrentS.h"
	-@del /f/q "PortableServer\PS_CurrentC.cpp"
	-@del /f/q "PortableServer\PortableServer_includeC.h"
	-@del /f/q "PortableServer\PortableServer_includeS.h"
	-@del /f/q "PortableServer\PortableServer_includeA.h"
	-@del /f/q "PortableServer\PortableServer_includeC.cpp"
	-@del /f/q "PortableServer\PortableServerC.h"
	-@del /f/q "PortableServer\PortableServerS.h"
	-@del /f/q "PortableServer\PortableServerA.h"
	-@del /f/q "PortableServer\PortableServerC.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\PortableServer\$(NULL)" mkdir "Debug\PortableServer"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_HAS_VALUETYPE_OUT_INDIRECTION /D TAO_PORTABLESERVER_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\TAO_PortableServerd.pdb" /machine:IA64 /out:"..\..\lib\TAO_PortableServerd.dll" /implib:"$(OUTDIR)\TAO_PortableServerd.lib"
LINK32_OBJS= \
	"$(INTDIR)\PortableServer\TAO_PortableServer.res" \
	"$(INTDIR)\PortableServer\ForwardRequestC.obj" \
	"$(INTDIR)\PortableServer\ForwardRequestA.obj" \
	"$(INTDIR)\PortableServer\AdapterActivatorC.obj" \
	"$(INTDIR)\PortableServer\AdapterActivatorA.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentPolicyC.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentPolicyA.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessPolicyC.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessPolicyA.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationPolicyC.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationPolicyA.obj" \
	"$(INTDIR)\PortableServer\LifespanPolicyC.obj" \
	"$(INTDIR)\PortableServer\LifespanPolicyA.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingPolicyC.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingPolicyA.obj" \
	"$(INTDIR)\PortableServer\ServantActivatorC.obj" \
	"$(INTDIR)\PortableServer\ServantActivatorA.obj" \
	"$(INTDIR)\PortableServer\ServantLocatorC.obj" \
	"$(INTDIR)\PortableServer\ServantLocatorA.obj" \
	"$(INTDIR)\PortableServer\ServantManagerC.obj" \
	"$(INTDIR)\PortableServer\ServantManagerA.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionPolicyC.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionPolicyA.obj" \
	"$(INTDIR)\PortableServer\ThreadPolicyC.obj" \
	"$(INTDIR)\PortableServer\ThreadPolicyA.obj" \
	"$(INTDIR)\PortableServer\POAManagerC.obj" \
	"$(INTDIR)\PortableServer\POAManagerFactoryC.obj" \
	"$(INTDIR)\PortableServer\POAC.obj" \
	"$(INTDIR)\PortableServer\PS_CurrentC.obj" \
	"$(INTDIR)\PortableServer\PortableServer_includeC.obj" \
	"$(INTDIR)\PortableServer\PortableServerC.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessPolicy.obj" \
	"$(INTDIR)\PortableServer\StrategyFactory.obj" \
	"$(INTDIR)\PortableServer\LifespanStrategyTransientFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\Collocated_Arguments_Converter.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyDefaultServant.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyDefaultServantFI.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionStrategyRetainFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentStrategy.obj" \
	"$(INTDIR)\PortableServer\ThreadStrategySingle.obj" \
	"$(INTDIR)\PortableServer\Operation_Table_Binary_Search.obj" \
	"$(INTDIR)\PortableServer\Acceptor_Filter_Factory.obj" \
	"$(INTDIR)\PortableServer\Default_Acceptor_Filter.obj" \
	"$(INTDIR)\PortableServer\Local_Servant_Base.obj" \
	"$(INTDIR)\PortableServer\Root_POA.obj" \
	"$(INTDIR)\PortableServer\PS_ForwardC.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\LifespanStrategyPersistent.obj" \
	"$(INTDIR)\PortableServer\POA_Current.obj" \
	"$(INTDIR)\PortableServer\Servant_Base.obj" \
	"$(INTDIR)\PortableServer\Upcall_Command.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingPolicy.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyServantActivatorFI.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\POAManagerFactory.obj" \
	"$(INTDIR)\PortableServer\Operation_Table.obj" \
	"$(INTDIR)\PortableServer\ORT_Adapter_Factory.obj" \
	"$(INTDIR)\PortableServer\Network_Priority_Hook.obj" \
	"$(INTDIR)\PortableServer\LifespanStrategy.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationPolicy.obj" \
	"$(INTDIR)\PortableServer\ThreadStrategySingleFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\Servant_Dispatcher.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyServantLocatorFI.obj" \
	"$(INTDIR)\PortableServer\Non_Servant_Upcall.obj" \
	"$(INTDIR)\PortableServer\POA_Current_Factory.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionStrategyRetain.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\Operation_Table_Perfect_Hash.obj" \
	"$(INTDIR)\PortableServer\Adapter_Activator.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\LifespanStrategyTransient.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyServantLocator.obj" \
	"$(INTDIR)\PortableServer\ThreadStrategyORBControl.obj" \
	"$(INTDIR)\PortableServer\Active_Policy_Strategies.obj" \
	"$(INTDIR)\PortableServer\LifespanStrategyPersistentFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyAOMOnlyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessStrategyUnique.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyServantManager.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionStrategyNonRetainFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\Upcall_Wrapper.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyServantActivator.obj" \
	"$(INTDIR)\PortableServer\LifespanStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\Servant_Upcall.obj" \
	"$(INTDIR)\PortableServer\POA_Cached_Policies.obj" \
	"$(INTDIR)\PortableServer\PortableServer_Functions.obj" \
	"$(INTDIR)\PortableServer\Active_Object_Map_Entry.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessStrategyUniqueFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\PortableServer.obj" \
	"$(INTDIR)\PortableServer\Active_Object_Map.obj" \
	"$(INTDIR)\PortableServer\ThreadPolicy.obj" \
	"$(INTDIR)\PortableServer\ThreadStrategy.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\Direct_Collocation_Upcall_Wrapper.obj" \
	"$(INTDIR)\PortableServer\Operation_Table_Dynamic_Hash.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationStrategyExplicit.obj" \
	"$(INTDIR)\PortableServer\Regular_POA.obj" \
	"$(INTDIR)\PortableServer\Creation_Time.obj" \
	"$(INTDIR)\PortableServer\Operation_Table_Linear_Search.obj" \
	"$(INTDIR)\PortableServer\PolicyS.obj" \
	"$(INTDIR)\PortableServer\Object_Adapter.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategy.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentStrategySystem.obj" \
	"$(INTDIR)\PortableServer\Default_Servant_Dispatcher.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionStrategyNonRetain.obj" \
	"$(INTDIR)\PortableServer\PS_ForwardA.obj" \
	"$(INTDIR)\PortableServer\Key_Adapters.obj" \
	"$(INTDIR)\PortableServer\POA_Policy_Set.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessStrategyMultiple.obj" \
	"$(INTDIR)\PortableServer\PortableServer_WFunctions.obj" \
	"$(INTDIR)\PortableServer\LifespanPolicy.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentPolicy.obj" \
	"$(INTDIR)\PortableServer\ThreadStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentStrategyUser.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationStrategy.obj" \
	"$(INTDIR)\PortableServer\Default_Policy_Validator.obj" \
	"$(INTDIR)\PortableServer\POAManager.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationStrategyImplicit.obj" \
	"$(INTDIR)\PortableServer\POA_Current_Impl.obj" \
	"$(INTDIR)\PortableServer\ImR_Client_Adapter.obj" \
	"$(INTDIR)\PortableServer\Object_Adapter_Factory.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyAOMOnly.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionPolicy.obj" \
	"$(INTDIR)\PortableServer\POA_Guard.obj" \
	"$(INTDIR)\PortableServer\Collocated_Object_Proxy_Broker.obj"

"..\..\lib\TAO_PortableServerd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_PortableServerd.dll.manifest" mt.exe -manifest "..\..\lib\TAO_PortableServerd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\PortableServer\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_PortableServer.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_HAS_VALUETYPE_OUT_INDIRECTION -DTAO_PORTABLESERVER_BUILD_DLL -f "Makefile.PortableServer.dep" "PortableServer\ForwardRequestC.cpp" "PortableServer\ForwardRequestA.cpp" "PortableServer\AdapterActivatorC.cpp" "PortableServer\AdapterActivatorA.cpp" "PortableServer\IdAssignmentPolicyC.cpp" "PortableServer\IdAssignmentPolicyA.cpp" "PortableServer\IdUniquenessPolicyC.cpp" "PortableServer\IdUniquenessPolicyA.cpp" "PortableServer\ImplicitActivationPolicyC.cpp" "PortableServer\ImplicitActivationPolicyA.cpp" "PortableServer\LifespanPolicyC.cpp" "PortableServer\LifespanPolicyA.cpp" "PortableServer\RequestProcessingPolicyC.cpp" "PortableServer\RequestProcessingPolicyA.cpp" "PortableServer\ServantActivatorC.cpp" "PortableServer\ServantActivatorA.cpp" "PortableServer\ServantLocatorC.cpp" "PortableServer\ServantLocatorA.cpp" "PortableServer\ServantManagerC.cpp" "PortableServer\ServantManagerA.cpp" "PortableServer\ServantRetentionPolicyC.cpp" "PortableServer\ServantRetentionPolicyA.cpp" "PortableServer\ThreadPolicyC.cpp" "PortableServer\ThreadPolicyA.cpp" "PortableServer\POAManagerC.cpp" "PortableServer\POAManagerFactoryC.cpp" "PortableServer\POAC.cpp" "PortableServer\PS_CurrentC.cpp" "PortableServer\PortableServer_includeC.cpp" "PortableServer\PortableServerC.cpp" "PortableServer\IdUniquenessPolicy.cpp" "PortableServer\StrategyFactory.cpp" "PortableServer\LifespanStrategyTransientFactoryImpl.cpp" "PortableServer\Collocated_Arguments_Converter.cpp" "PortableServer\RequestProcessingStrategyDefaultServant.cpp" "PortableServer\RequestProcessingStrategyDefaultServantFI.cpp" "PortableServer\ServantRetentionStrategyRetainFactoryImpl.cpp" "PortableServer\IdAssignmentStrategy.cpp" "PortableServer\ThreadStrategySingle.cpp" "PortableServer\Operation_Table_Binary_Search.cpp" "PortableServer\Acceptor_Filter_Factory.cpp" "PortableServer\Default_Acceptor_Filter.cpp" "PortableServer\Local_Servant_Base.cpp" "PortableServer\Root_POA.cpp" "PortableServer\PS_ForwardC.cpp" "PortableServer\ImplicitActivationStrategyFactoryImpl.cpp" "PortableServer\LifespanStrategyPersistent.cpp" "PortableServer\POA_Current.cpp" "PortableServer\Servant_Base.cpp" "PortableServer\Upcall_Command.cpp" "PortableServer\RequestProcessingPolicy.cpp" "PortableServer\RequestProcessingStrategyServantActivatorFI.cpp" "PortableServer\IdUniquenessStrategyFactoryImpl.cpp" "PortableServer\POAManagerFactory.cpp" "PortableServer\Operation_Table.cpp" "PortableServer\ORT_Adapter_Factory.cpp" "PortableServer\Network_Priority_Hook.cpp" "PortableServer\LifespanStrategy.cpp" "PortableServer\ImplicitActivationPolicy.cpp" "PortableServer\ThreadStrategySingleFactoryImpl.cpp" "PortableServer\Servant_Dispatcher.cpp" "PortableServer\RequestProcessingStrategyServantLocatorFI.cpp" "PortableServer\Non_Servant_Upcall.cpp" "PortableServer\POA_Current_Factory.cpp" "PortableServer\ServantRetentionStrategyRetain.cpp" "PortableServer\IdAssignmentStrategyFactoryImpl.cpp" "PortableServer\Operation_Table_Perfect_Hash.cpp" "PortableServer\Adapter_Activator.cpp" "PortableServer\ServantRetentionStrategyFactoryImpl.cpp" "PortableServer\LifespanStrategyTransient.cpp" "PortableServer\RequestProcessingStrategyServantLocator.cpp" "PortableServer\ThreadStrategyORBControl.cpp" "PortableServer\Active_Policy_Strategies.cpp" "PortableServer\LifespanStrategyPersistentFactoryImpl.cpp" "PortableServer\RequestProcessingStrategyAOMOnlyFactoryImpl.cpp" "PortableServer\IdUniquenessStrategyUnique.cpp" "PortableServer\RequestProcessingStrategyServantManager.cpp" "PortableServer\ServantRetentionStrategyNonRetainFactoryImpl.cpp" "PortableServer\Upcall_Wrapper.cpp" "PortableServer\RequestProcessingStrategyServantActivator.cpp" "PortableServer\LifespanStrategyFactoryImpl.cpp" "PortableServer\Servant_Upcall.cpp" "PortableServer\POA_Cached_Policies.cpp" "PortableServer\PortableServer_Functions.cpp" "PortableServer\Active_Object_Map_Entry.cpp" "PortableServer\IdUniquenessStrategyUniqueFactoryImpl.cpp" "PortableServer\PortableServer.cpp" "PortableServer\Active_Object_Map.cpp" "PortableServer\ThreadPolicy.cpp" "PortableServer\ThreadStrategy.cpp" "PortableServer\RequestProcessingStrategyFactoryImpl.cpp" "PortableServer\Direct_Collocation_Upcall_Wrapper.cpp" "PortableServer\Operation_Table_Dynamic_Hash.cpp" "PortableServer\ImplicitActivationStrategyExplicit.cpp" "PortableServer\Regular_POA.cpp" "PortableServer\Creation_Time.cpp" "PortableServer\Operation_Table_Linear_Search.cpp" "PortableServer\PolicyS.cpp" "PortableServer\Object_Adapter.cpp" "PortableServer\RequestProcessingStrategy.cpp" "PortableServer\IdAssignmentStrategySystem.cpp" "PortableServer\Default_Servant_Dispatcher.cpp" "PortableServer\ServantRetentionStrategyNonRetain.cpp" "PortableServer\PS_ForwardA.cpp" "PortableServer\Key_Adapters.cpp" "PortableServer\POA_Policy_Set.cpp" "PortableServer\IdUniquenessStrategyMultiple.cpp" "PortableServer\PortableServer_WFunctions.cpp" "PortableServer\LifespanPolicy.cpp" "PortableServer\IdAssignmentPolicy.cpp" "PortableServer\ThreadStrategyFactoryImpl.cpp" "PortableServer\IdAssignmentStrategyUser.cpp" "PortableServer\ImplicitActivationStrategy.cpp" "PortableServer\Default_Policy_Validator.cpp" "PortableServer\POAManager.cpp" "PortableServer\ImplicitActivationStrategyImplicit.cpp" "PortableServer\POA_Current_Impl.cpp" "PortableServer\ImR_Client_Adapter.cpp" "PortableServer\Object_Adapter_Factory.cpp" "PortableServer\RequestProcessingStrategyAOMOnly.cpp" "PortableServer\ServantRetentionPolicy.cpp" "PortableServer\POA_Guard.cpp" "PortableServer\Collocated_Object_Proxy_Broker.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\TAO_PortableServer.dll"
	-@del /f/q "$(OUTDIR)\TAO_PortableServer.lib"
	-@del /f/q "$(OUTDIR)\TAO_PortableServer.exp"
	-@del /f/q "$(OUTDIR)\TAO_PortableServer.ilk"
	-@del /f/q "PortableServer\ForwardRequestC.h"
	-@del /f/q "PortableServer\ForwardRequestS.h"
	-@del /f/q "PortableServer\ForwardRequestA.h"
	-@del /f/q "PortableServer\ForwardRequestC.cpp"
	-@del /f/q "PortableServer\ForwardRequestA.cpp"
	-@del /f/q "PortableServer\AdapterActivatorC.h"
	-@del /f/q "PortableServer\AdapterActivatorS.h"
	-@del /f/q "PortableServer\AdapterActivatorA.h"
	-@del /f/q "PortableServer\AdapterActivatorC.cpp"
	-@del /f/q "PortableServer\AdapterActivatorA.cpp"
	-@del /f/q "PortableServer\IdAssignmentPolicyC.h"
	-@del /f/q "PortableServer\IdAssignmentPolicyS.h"
	-@del /f/q "PortableServer\IdAssignmentPolicyA.h"
	-@del /f/q "PortableServer\IdAssignmentPolicyC.cpp"
	-@del /f/q "PortableServer\IdAssignmentPolicyA.cpp"
	-@del /f/q "PortableServer\IdUniquenessPolicyC.h"
	-@del /f/q "PortableServer\IdUniquenessPolicyS.h"
	-@del /f/q "PortableServer\IdUniquenessPolicyA.h"
	-@del /f/q "PortableServer\IdUniquenessPolicyC.cpp"
	-@del /f/q "PortableServer\IdUniquenessPolicyA.cpp"
	-@del /f/q "PortableServer\ImplicitActivationPolicyC.h"
	-@del /f/q "PortableServer\ImplicitActivationPolicyS.h"
	-@del /f/q "PortableServer\ImplicitActivationPolicyA.h"
	-@del /f/q "PortableServer\ImplicitActivationPolicyC.cpp"
	-@del /f/q "PortableServer\ImplicitActivationPolicyA.cpp"
	-@del /f/q "PortableServer\LifespanPolicyC.h"
	-@del /f/q "PortableServer\LifespanPolicyS.h"
	-@del /f/q "PortableServer\LifespanPolicyA.h"
	-@del /f/q "PortableServer\LifespanPolicyC.cpp"
	-@del /f/q "PortableServer\LifespanPolicyA.cpp"
	-@del /f/q "PortableServer\RequestProcessingPolicyC.h"
	-@del /f/q "PortableServer\RequestProcessingPolicyS.h"
	-@del /f/q "PortableServer\RequestProcessingPolicyA.h"
	-@del /f/q "PortableServer\RequestProcessingPolicyC.cpp"
	-@del /f/q "PortableServer\RequestProcessingPolicyA.cpp"
	-@del /f/q "PortableServer\ServantActivatorC.h"
	-@del /f/q "PortableServer\ServantActivatorS.h"
	-@del /f/q "PortableServer\ServantActivatorA.h"
	-@del /f/q "PortableServer\ServantActivatorC.cpp"
	-@del /f/q "PortableServer\ServantActivatorA.cpp"
	-@del /f/q "PortableServer\ServantLocatorC.h"
	-@del /f/q "PortableServer\ServantLocatorS.h"
	-@del /f/q "PortableServer\ServantLocatorA.h"
	-@del /f/q "PortableServer\ServantLocatorC.cpp"
	-@del /f/q "PortableServer\ServantLocatorA.cpp"
	-@del /f/q "PortableServer\ServantManagerC.h"
	-@del /f/q "PortableServer\ServantManagerS.h"
	-@del /f/q "PortableServer\ServantManagerA.h"
	-@del /f/q "PortableServer\ServantManagerC.cpp"
	-@del /f/q "PortableServer\ServantManagerA.cpp"
	-@del /f/q "PortableServer\ServantRetentionPolicyC.h"
	-@del /f/q "PortableServer\ServantRetentionPolicyS.h"
	-@del /f/q "PortableServer\ServantRetentionPolicyA.h"
	-@del /f/q "PortableServer\ServantRetentionPolicyC.cpp"
	-@del /f/q "PortableServer\ServantRetentionPolicyA.cpp"
	-@del /f/q "PortableServer\ThreadPolicyC.h"
	-@del /f/q "PortableServer\ThreadPolicyS.h"
	-@del /f/q "PortableServer\ThreadPolicyA.h"
	-@del /f/q "PortableServer\ThreadPolicyC.cpp"
	-@del /f/q "PortableServer\ThreadPolicyA.cpp"
	-@del /f/q "PortableServer\POAManagerC.h"
	-@del /f/q "PortableServer\POAManagerS.h"
	-@del /f/q "PortableServer\POAManagerC.cpp"
	-@del /f/q "PortableServer\POAManagerFactoryC.h"
	-@del /f/q "PortableServer\POAManagerFactoryS.h"
	-@del /f/q "PortableServer\POAManagerFactoryC.cpp"
	-@del /f/q "PortableServer\POAC.h"
	-@del /f/q "PortableServer\POAS.h"
	-@del /f/q "PortableServer\POAC.cpp"
	-@del /f/q "PortableServer\PS_CurrentC.h"
	-@del /f/q "PortableServer\PS_CurrentS.h"
	-@del /f/q "PortableServer\PS_CurrentC.cpp"
	-@del /f/q "PortableServer\PortableServer_includeC.h"
	-@del /f/q "PortableServer\PortableServer_includeS.h"
	-@del /f/q "PortableServer\PortableServer_includeA.h"
	-@del /f/q "PortableServer\PortableServer_includeC.cpp"
	-@del /f/q "PortableServer\PortableServerC.h"
	-@del /f/q "PortableServer\PortableServerS.h"
	-@del /f/q "PortableServer\PortableServerA.h"
	-@del /f/q "PortableServer\PortableServerC.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\PortableServer\$(NULL)" mkdir "Release\PortableServer"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_HAS_VALUETYPE_OUT_INDIRECTION /D TAO_PORTABLESERVER_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\TAO_PortableServer.dll" /implib:"$(OUTDIR)\TAO_PortableServer.lib"
LINK32_OBJS= \
	"$(INTDIR)\PortableServer\TAO_PortableServer.res" \
	"$(INTDIR)\PortableServer\ForwardRequestC.obj" \
	"$(INTDIR)\PortableServer\ForwardRequestA.obj" \
	"$(INTDIR)\PortableServer\AdapterActivatorC.obj" \
	"$(INTDIR)\PortableServer\AdapterActivatorA.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentPolicyC.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentPolicyA.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessPolicyC.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessPolicyA.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationPolicyC.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationPolicyA.obj" \
	"$(INTDIR)\PortableServer\LifespanPolicyC.obj" \
	"$(INTDIR)\PortableServer\LifespanPolicyA.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingPolicyC.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingPolicyA.obj" \
	"$(INTDIR)\PortableServer\ServantActivatorC.obj" \
	"$(INTDIR)\PortableServer\ServantActivatorA.obj" \
	"$(INTDIR)\PortableServer\ServantLocatorC.obj" \
	"$(INTDIR)\PortableServer\ServantLocatorA.obj" \
	"$(INTDIR)\PortableServer\ServantManagerC.obj" \
	"$(INTDIR)\PortableServer\ServantManagerA.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionPolicyC.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionPolicyA.obj" \
	"$(INTDIR)\PortableServer\ThreadPolicyC.obj" \
	"$(INTDIR)\PortableServer\ThreadPolicyA.obj" \
	"$(INTDIR)\PortableServer\POAManagerC.obj" \
	"$(INTDIR)\PortableServer\POAManagerFactoryC.obj" \
	"$(INTDIR)\PortableServer\POAC.obj" \
	"$(INTDIR)\PortableServer\PS_CurrentC.obj" \
	"$(INTDIR)\PortableServer\PortableServer_includeC.obj" \
	"$(INTDIR)\PortableServer\PortableServerC.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessPolicy.obj" \
	"$(INTDIR)\PortableServer\StrategyFactory.obj" \
	"$(INTDIR)\PortableServer\LifespanStrategyTransientFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\Collocated_Arguments_Converter.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyDefaultServant.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyDefaultServantFI.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionStrategyRetainFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentStrategy.obj" \
	"$(INTDIR)\PortableServer\ThreadStrategySingle.obj" \
	"$(INTDIR)\PortableServer\Operation_Table_Binary_Search.obj" \
	"$(INTDIR)\PortableServer\Acceptor_Filter_Factory.obj" \
	"$(INTDIR)\PortableServer\Default_Acceptor_Filter.obj" \
	"$(INTDIR)\PortableServer\Local_Servant_Base.obj" \
	"$(INTDIR)\PortableServer\Root_POA.obj" \
	"$(INTDIR)\PortableServer\PS_ForwardC.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\LifespanStrategyPersistent.obj" \
	"$(INTDIR)\PortableServer\POA_Current.obj" \
	"$(INTDIR)\PortableServer\Servant_Base.obj" \
	"$(INTDIR)\PortableServer\Upcall_Command.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingPolicy.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyServantActivatorFI.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\POAManagerFactory.obj" \
	"$(INTDIR)\PortableServer\Operation_Table.obj" \
	"$(INTDIR)\PortableServer\ORT_Adapter_Factory.obj" \
	"$(INTDIR)\PortableServer\Network_Priority_Hook.obj" \
	"$(INTDIR)\PortableServer\LifespanStrategy.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationPolicy.obj" \
	"$(INTDIR)\PortableServer\ThreadStrategySingleFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\Servant_Dispatcher.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyServantLocatorFI.obj" \
	"$(INTDIR)\PortableServer\Non_Servant_Upcall.obj" \
	"$(INTDIR)\PortableServer\POA_Current_Factory.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionStrategyRetain.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\Operation_Table_Perfect_Hash.obj" \
	"$(INTDIR)\PortableServer\Adapter_Activator.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\LifespanStrategyTransient.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyServantLocator.obj" \
	"$(INTDIR)\PortableServer\ThreadStrategyORBControl.obj" \
	"$(INTDIR)\PortableServer\Active_Policy_Strategies.obj" \
	"$(INTDIR)\PortableServer\LifespanStrategyPersistentFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyAOMOnlyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessStrategyUnique.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyServantManager.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionStrategyNonRetainFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\Upcall_Wrapper.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyServantActivator.obj" \
	"$(INTDIR)\PortableServer\LifespanStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\Servant_Upcall.obj" \
	"$(INTDIR)\PortableServer\POA_Cached_Policies.obj" \
	"$(INTDIR)\PortableServer\PortableServer_Functions.obj" \
	"$(INTDIR)\PortableServer\Active_Object_Map_Entry.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessStrategyUniqueFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\PortableServer.obj" \
	"$(INTDIR)\PortableServer\Active_Object_Map.obj" \
	"$(INTDIR)\PortableServer\ThreadPolicy.obj" \
	"$(INTDIR)\PortableServer\ThreadStrategy.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\Direct_Collocation_Upcall_Wrapper.obj" \
	"$(INTDIR)\PortableServer\Operation_Table_Dynamic_Hash.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationStrategyExplicit.obj" \
	"$(INTDIR)\PortableServer\Regular_POA.obj" \
	"$(INTDIR)\PortableServer\Creation_Time.obj" \
	"$(INTDIR)\PortableServer\Operation_Table_Linear_Search.obj" \
	"$(INTDIR)\PortableServer\PolicyS.obj" \
	"$(INTDIR)\PortableServer\Object_Adapter.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategy.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentStrategySystem.obj" \
	"$(INTDIR)\PortableServer\Default_Servant_Dispatcher.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionStrategyNonRetain.obj" \
	"$(INTDIR)\PortableServer\PS_ForwardA.obj" \
	"$(INTDIR)\PortableServer\Key_Adapters.obj" \
	"$(INTDIR)\PortableServer\POA_Policy_Set.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessStrategyMultiple.obj" \
	"$(INTDIR)\PortableServer\PortableServer_WFunctions.obj" \
	"$(INTDIR)\PortableServer\LifespanPolicy.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentPolicy.obj" \
	"$(INTDIR)\PortableServer\ThreadStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentStrategyUser.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationStrategy.obj" \
	"$(INTDIR)\PortableServer\Default_Policy_Validator.obj" \
	"$(INTDIR)\PortableServer\POAManager.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationStrategyImplicit.obj" \
	"$(INTDIR)\PortableServer\POA_Current_Impl.obj" \
	"$(INTDIR)\PortableServer\ImR_Client_Adapter.obj" \
	"$(INTDIR)\PortableServer\Object_Adapter_Factory.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyAOMOnly.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionPolicy.obj" \
	"$(INTDIR)\PortableServer\POA_Guard.obj" \
	"$(INTDIR)\PortableServer\Collocated_Object_Proxy_Broker.obj"

"..\..\lib\TAO_PortableServer.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_PortableServer.dll.manifest" mt.exe -manifest "..\..\lib\TAO_PortableServer.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\PortableServer\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_PortableServersd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_HAS_VALUETYPE_OUT_INDIRECTION -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.PortableServer.dep" "PortableServer\ForwardRequestC.cpp" "PortableServer\ForwardRequestA.cpp" "PortableServer\AdapterActivatorC.cpp" "PortableServer\AdapterActivatorA.cpp" "PortableServer\IdAssignmentPolicyC.cpp" "PortableServer\IdAssignmentPolicyA.cpp" "PortableServer\IdUniquenessPolicyC.cpp" "PortableServer\IdUniquenessPolicyA.cpp" "PortableServer\ImplicitActivationPolicyC.cpp" "PortableServer\ImplicitActivationPolicyA.cpp" "PortableServer\LifespanPolicyC.cpp" "PortableServer\LifespanPolicyA.cpp" "PortableServer\RequestProcessingPolicyC.cpp" "PortableServer\RequestProcessingPolicyA.cpp" "PortableServer\ServantActivatorC.cpp" "PortableServer\ServantActivatorA.cpp" "PortableServer\ServantLocatorC.cpp" "PortableServer\ServantLocatorA.cpp" "PortableServer\ServantManagerC.cpp" "PortableServer\ServantManagerA.cpp" "PortableServer\ServantRetentionPolicyC.cpp" "PortableServer\ServantRetentionPolicyA.cpp" "PortableServer\ThreadPolicyC.cpp" "PortableServer\ThreadPolicyA.cpp" "PortableServer\POAManagerC.cpp" "PortableServer\POAManagerFactoryC.cpp" "PortableServer\POAC.cpp" "PortableServer\PS_CurrentC.cpp" "PortableServer\PortableServer_includeC.cpp" "PortableServer\PortableServerC.cpp" "PortableServer\IdUniquenessPolicy.cpp" "PortableServer\StrategyFactory.cpp" "PortableServer\LifespanStrategyTransientFactoryImpl.cpp" "PortableServer\Collocated_Arguments_Converter.cpp" "PortableServer\RequestProcessingStrategyDefaultServant.cpp" "PortableServer\RequestProcessingStrategyDefaultServantFI.cpp" "PortableServer\ServantRetentionStrategyRetainFactoryImpl.cpp" "PortableServer\IdAssignmentStrategy.cpp" "PortableServer\ThreadStrategySingle.cpp" "PortableServer\Operation_Table_Binary_Search.cpp" "PortableServer\Acceptor_Filter_Factory.cpp" "PortableServer\Default_Acceptor_Filter.cpp" "PortableServer\Local_Servant_Base.cpp" "PortableServer\Root_POA.cpp" "PortableServer\PS_ForwardC.cpp" "PortableServer\ImplicitActivationStrategyFactoryImpl.cpp" "PortableServer\LifespanStrategyPersistent.cpp" "PortableServer\POA_Current.cpp" "PortableServer\Servant_Base.cpp" "PortableServer\Upcall_Command.cpp" "PortableServer\RequestProcessingPolicy.cpp" "PortableServer\RequestProcessingStrategyServantActivatorFI.cpp" "PortableServer\IdUniquenessStrategyFactoryImpl.cpp" "PortableServer\POAManagerFactory.cpp" "PortableServer\Operation_Table.cpp" "PortableServer\ORT_Adapter_Factory.cpp" "PortableServer\Network_Priority_Hook.cpp" "PortableServer\LifespanStrategy.cpp" "PortableServer\ImplicitActivationPolicy.cpp" "PortableServer\ThreadStrategySingleFactoryImpl.cpp" "PortableServer\Servant_Dispatcher.cpp" "PortableServer\RequestProcessingStrategyServantLocatorFI.cpp" "PortableServer\Non_Servant_Upcall.cpp" "PortableServer\POA_Current_Factory.cpp" "PortableServer\ServantRetentionStrategyRetain.cpp" "PortableServer\IdAssignmentStrategyFactoryImpl.cpp" "PortableServer\Operation_Table_Perfect_Hash.cpp" "PortableServer\Adapter_Activator.cpp" "PortableServer\ServantRetentionStrategyFactoryImpl.cpp" "PortableServer\LifespanStrategyTransient.cpp" "PortableServer\RequestProcessingStrategyServantLocator.cpp" "PortableServer\ThreadStrategyORBControl.cpp" "PortableServer\Active_Policy_Strategies.cpp" "PortableServer\LifespanStrategyPersistentFactoryImpl.cpp" "PortableServer\RequestProcessingStrategyAOMOnlyFactoryImpl.cpp" "PortableServer\IdUniquenessStrategyUnique.cpp" "PortableServer\RequestProcessingStrategyServantManager.cpp" "PortableServer\ServantRetentionStrategyNonRetainFactoryImpl.cpp" "PortableServer\Upcall_Wrapper.cpp" "PortableServer\RequestProcessingStrategyServantActivator.cpp" "PortableServer\LifespanStrategyFactoryImpl.cpp" "PortableServer\Servant_Upcall.cpp" "PortableServer\POA_Cached_Policies.cpp" "PortableServer\PortableServer_Functions.cpp" "PortableServer\Active_Object_Map_Entry.cpp" "PortableServer\IdUniquenessStrategyUniqueFactoryImpl.cpp" "PortableServer\PortableServer.cpp" "PortableServer\Active_Object_Map.cpp" "PortableServer\ThreadPolicy.cpp" "PortableServer\ThreadStrategy.cpp" "PortableServer\RequestProcessingStrategyFactoryImpl.cpp" "PortableServer\Direct_Collocation_Upcall_Wrapper.cpp" "PortableServer\Operation_Table_Dynamic_Hash.cpp" "PortableServer\ImplicitActivationStrategyExplicit.cpp" "PortableServer\Regular_POA.cpp" "PortableServer\Creation_Time.cpp" "PortableServer\Operation_Table_Linear_Search.cpp" "PortableServer\PolicyS.cpp" "PortableServer\Object_Adapter.cpp" "PortableServer\RequestProcessingStrategy.cpp" "PortableServer\IdAssignmentStrategySystem.cpp" "PortableServer\Default_Servant_Dispatcher.cpp" "PortableServer\ServantRetentionStrategyNonRetain.cpp" "PortableServer\PS_ForwardA.cpp" "PortableServer\Key_Adapters.cpp" "PortableServer\POA_Policy_Set.cpp" "PortableServer\IdUniquenessStrategyMultiple.cpp" "PortableServer\PortableServer_WFunctions.cpp" "PortableServer\LifespanPolicy.cpp" "PortableServer\IdAssignmentPolicy.cpp" "PortableServer\ThreadStrategyFactoryImpl.cpp" "PortableServer\IdAssignmentStrategyUser.cpp" "PortableServer\ImplicitActivationStrategy.cpp" "PortableServer\Default_Policy_Validator.cpp" "PortableServer\POAManager.cpp" "PortableServer\ImplicitActivationStrategyImplicit.cpp" "PortableServer\POA_Current_Impl.cpp" "PortableServer\ImR_Client_Adapter.cpp" "PortableServer\Object_Adapter_Factory.cpp" "PortableServer\RequestProcessingStrategyAOMOnly.cpp" "PortableServer\ServantRetentionPolicy.cpp" "PortableServer\POA_Guard.cpp" "PortableServer\Collocated_Object_Proxy_Broker.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_PortableServersd.lib"
	-@del /f/q "$(OUTDIR)\TAO_PortableServersd.exp"
	-@del /f/q "$(OUTDIR)\TAO_PortableServersd.ilk"
	-@del /f/q "..\..\lib\TAO_PortableServersd.pdb"
	-@del /f/q "PortableServer\ForwardRequestC.h"
	-@del /f/q "PortableServer\ForwardRequestS.h"
	-@del /f/q "PortableServer\ForwardRequestA.h"
	-@del /f/q "PortableServer\ForwardRequestC.cpp"
	-@del /f/q "PortableServer\ForwardRequestA.cpp"
	-@del /f/q "PortableServer\AdapterActivatorC.h"
	-@del /f/q "PortableServer\AdapterActivatorS.h"
	-@del /f/q "PortableServer\AdapterActivatorA.h"
	-@del /f/q "PortableServer\AdapterActivatorC.cpp"
	-@del /f/q "PortableServer\AdapterActivatorA.cpp"
	-@del /f/q "PortableServer\IdAssignmentPolicyC.h"
	-@del /f/q "PortableServer\IdAssignmentPolicyS.h"
	-@del /f/q "PortableServer\IdAssignmentPolicyA.h"
	-@del /f/q "PortableServer\IdAssignmentPolicyC.cpp"
	-@del /f/q "PortableServer\IdAssignmentPolicyA.cpp"
	-@del /f/q "PortableServer\IdUniquenessPolicyC.h"
	-@del /f/q "PortableServer\IdUniquenessPolicyS.h"
	-@del /f/q "PortableServer\IdUniquenessPolicyA.h"
	-@del /f/q "PortableServer\IdUniquenessPolicyC.cpp"
	-@del /f/q "PortableServer\IdUniquenessPolicyA.cpp"
	-@del /f/q "PortableServer\ImplicitActivationPolicyC.h"
	-@del /f/q "PortableServer\ImplicitActivationPolicyS.h"
	-@del /f/q "PortableServer\ImplicitActivationPolicyA.h"
	-@del /f/q "PortableServer\ImplicitActivationPolicyC.cpp"
	-@del /f/q "PortableServer\ImplicitActivationPolicyA.cpp"
	-@del /f/q "PortableServer\LifespanPolicyC.h"
	-@del /f/q "PortableServer\LifespanPolicyS.h"
	-@del /f/q "PortableServer\LifespanPolicyA.h"
	-@del /f/q "PortableServer\LifespanPolicyC.cpp"
	-@del /f/q "PortableServer\LifespanPolicyA.cpp"
	-@del /f/q "PortableServer\RequestProcessingPolicyC.h"
	-@del /f/q "PortableServer\RequestProcessingPolicyS.h"
	-@del /f/q "PortableServer\RequestProcessingPolicyA.h"
	-@del /f/q "PortableServer\RequestProcessingPolicyC.cpp"
	-@del /f/q "PortableServer\RequestProcessingPolicyA.cpp"
	-@del /f/q "PortableServer\ServantActivatorC.h"
	-@del /f/q "PortableServer\ServantActivatorS.h"
	-@del /f/q "PortableServer\ServantActivatorA.h"
	-@del /f/q "PortableServer\ServantActivatorC.cpp"
	-@del /f/q "PortableServer\ServantActivatorA.cpp"
	-@del /f/q "PortableServer\ServantLocatorC.h"
	-@del /f/q "PortableServer\ServantLocatorS.h"
	-@del /f/q "PortableServer\ServantLocatorA.h"
	-@del /f/q "PortableServer\ServantLocatorC.cpp"
	-@del /f/q "PortableServer\ServantLocatorA.cpp"
	-@del /f/q "PortableServer\ServantManagerC.h"
	-@del /f/q "PortableServer\ServantManagerS.h"
	-@del /f/q "PortableServer\ServantManagerA.h"
	-@del /f/q "PortableServer\ServantManagerC.cpp"
	-@del /f/q "PortableServer\ServantManagerA.cpp"
	-@del /f/q "PortableServer\ServantRetentionPolicyC.h"
	-@del /f/q "PortableServer\ServantRetentionPolicyS.h"
	-@del /f/q "PortableServer\ServantRetentionPolicyA.h"
	-@del /f/q "PortableServer\ServantRetentionPolicyC.cpp"
	-@del /f/q "PortableServer\ServantRetentionPolicyA.cpp"
	-@del /f/q "PortableServer\ThreadPolicyC.h"
	-@del /f/q "PortableServer\ThreadPolicyS.h"
	-@del /f/q "PortableServer\ThreadPolicyA.h"
	-@del /f/q "PortableServer\ThreadPolicyC.cpp"
	-@del /f/q "PortableServer\ThreadPolicyA.cpp"
	-@del /f/q "PortableServer\POAManagerC.h"
	-@del /f/q "PortableServer\POAManagerS.h"
	-@del /f/q "PortableServer\POAManagerC.cpp"
	-@del /f/q "PortableServer\POAManagerFactoryC.h"
	-@del /f/q "PortableServer\POAManagerFactoryS.h"
	-@del /f/q "PortableServer\POAManagerFactoryC.cpp"
	-@del /f/q "PortableServer\POAC.h"
	-@del /f/q "PortableServer\POAS.h"
	-@del /f/q "PortableServer\POAC.cpp"
	-@del /f/q "PortableServer\PS_CurrentC.h"
	-@del /f/q "PortableServer\PS_CurrentS.h"
	-@del /f/q "PortableServer\PS_CurrentC.cpp"
	-@del /f/q "PortableServer\PortableServer_includeC.h"
	-@del /f/q "PortableServer\PortableServer_includeS.h"
	-@del /f/q "PortableServer\PortableServer_includeA.h"
	-@del /f/q "PortableServer\PortableServer_includeC.cpp"
	-@del /f/q "PortableServer\PortableServerC.h"
	-@del /f/q "PortableServer\PortableServerS.h"
	-@del /f/q "PortableServer\PortableServerA.h"
	-@del /f/q "PortableServer\PortableServerC.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\PortableServer\$(NULL)" mkdir "Static_Debug\PortableServer"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"..\..\lib\TAO_PortableServersd.pdb" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_HAS_VALUETYPE_OUT_INDIRECTION /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_PortableServersd.lib"
LINK32_OBJS= \
	"$(INTDIR)\PortableServer\ForwardRequestC.obj" \
	"$(INTDIR)\PortableServer\ForwardRequestA.obj" \
	"$(INTDIR)\PortableServer\AdapterActivatorC.obj" \
	"$(INTDIR)\PortableServer\AdapterActivatorA.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentPolicyC.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentPolicyA.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessPolicyC.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessPolicyA.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationPolicyC.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationPolicyA.obj" \
	"$(INTDIR)\PortableServer\LifespanPolicyC.obj" \
	"$(INTDIR)\PortableServer\LifespanPolicyA.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingPolicyC.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingPolicyA.obj" \
	"$(INTDIR)\PortableServer\ServantActivatorC.obj" \
	"$(INTDIR)\PortableServer\ServantActivatorA.obj" \
	"$(INTDIR)\PortableServer\ServantLocatorC.obj" \
	"$(INTDIR)\PortableServer\ServantLocatorA.obj" \
	"$(INTDIR)\PortableServer\ServantManagerC.obj" \
	"$(INTDIR)\PortableServer\ServantManagerA.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionPolicyC.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionPolicyA.obj" \
	"$(INTDIR)\PortableServer\ThreadPolicyC.obj" \
	"$(INTDIR)\PortableServer\ThreadPolicyA.obj" \
	"$(INTDIR)\PortableServer\POAManagerC.obj" \
	"$(INTDIR)\PortableServer\POAManagerFactoryC.obj" \
	"$(INTDIR)\PortableServer\POAC.obj" \
	"$(INTDIR)\PortableServer\PS_CurrentC.obj" \
	"$(INTDIR)\PortableServer\PortableServer_includeC.obj" \
	"$(INTDIR)\PortableServer\PortableServerC.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessPolicy.obj" \
	"$(INTDIR)\PortableServer\StrategyFactory.obj" \
	"$(INTDIR)\PortableServer\LifespanStrategyTransientFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\Collocated_Arguments_Converter.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyDefaultServant.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyDefaultServantFI.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionStrategyRetainFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentStrategy.obj" \
	"$(INTDIR)\PortableServer\ThreadStrategySingle.obj" \
	"$(INTDIR)\PortableServer\Operation_Table_Binary_Search.obj" \
	"$(INTDIR)\PortableServer\Acceptor_Filter_Factory.obj" \
	"$(INTDIR)\PortableServer\Default_Acceptor_Filter.obj" \
	"$(INTDIR)\PortableServer\Local_Servant_Base.obj" \
	"$(INTDIR)\PortableServer\Root_POA.obj" \
	"$(INTDIR)\PortableServer\PS_ForwardC.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\LifespanStrategyPersistent.obj" \
	"$(INTDIR)\PortableServer\POA_Current.obj" \
	"$(INTDIR)\PortableServer\Servant_Base.obj" \
	"$(INTDIR)\PortableServer\Upcall_Command.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingPolicy.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyServantActivatorFI.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\POAManagerFactory.obj" \
	"$(INTDIR)\PortableServer\Operation_Table.obj" \
	"$(INTDIR)\PortableServer\ORT_Adapter_Factory.obj" \
	"$(INTDIR)\PortableServer\Network_Priority_Hook.obj" \
	"$(INTDIR)\PortableServer\LifespanStrategy.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationPolicy.obj" \
	"$(INTDIR)\PortableServer\ThreadStrategySingleFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\Servant_Dispatcher.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyServantLocatorFI.obj" \
	"$(INTDIR)\PortableServer\Non_Servant_Upcall.obj" \
	"$(INTDIR)\PortableServer\POA_Current_Factory.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionStrategyRetain.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\Operation_Table_Perfect_Hash.obj" \
	"$(INTDIR)\PortableServer\Adapter_Activator.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\LifespanStrategyTransient.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyServantLocator.obj" \
	"$(INTDIR)\PortableServer\ThreadStrategyORBControl.obj" \
	"$(INTDIR)\PortableServer\Active_Policy_Strategies.obj" \
	"$(INTDIR)\PortableServer\LifespanStrategyPersistentFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyAOMOnlyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessStrategyUnique.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyServantManager.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionStrategyNonRetainFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\Upcall_Wrapper.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyServantActivator.obj" \
	"$(INTDIR)\PortableServer\LifespanStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\Servant_Upcall.obj" \
	"$(INTDIR)\PortableServer\POA_Cached_Policies.obj" \
	"$(INTDIR)\PortableServer\PortableServer_Functions.obj" \
	"$(INTDIR)\PortableServer\Active_Object_Map_Entry.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessStrategyUniqueFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\PortableServer.obj" \
	"$(INTDIR)\PortableServer\Active_Object_Map.obj" \
	"$(INTDIR)\PortableServer\ThreadPolicy.obj" \
	"$(INTDIR)\PortableServer\ThreadStrategy.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\Direct_Collocation_Upcall_Wrapper.obj" \
	"$(INTDIR)\PortableServer\Operation_Table_Dynamic_Hash.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationStrategyExplicit.obj" \
	"$(INTDIR)\PortableServer\Regular_POA.obj" \
	"$(INTDIR)\PortableServer\Creation_Time.obj" \
	"$(INTDIR)\PortableServer\Operation_Table_Linear_Search.obj" \
	"$(INTDIR)\PortableServer\PolicyS.obj" \
	"$(INTDIR)\PortableServer\Object_Adapter.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategy.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentStrategySystem.obj" \
	"$(INTDIR)\PortableServer\Default_Servant_Dispatcher.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionStrategyNonRetain.obj" \
	"$(INTDIR)\PortableServer\PS_ForwardA.obj" \
	"$(INTDIR)\PortableServer\Key_Adapters.obj" \
	"$(INTDIR)\PortableServer\POA_Policy_Set.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessStrategyMultiple.obj" \
	"$(INTDIR)\PortableServer\PortableServer_WFunctions.obj" \
	"$(INTDIR)\PortableServer\LifespanPolicy.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentPolicy.obj" \
	"$(INTDIR)\PortableServer\ThreadStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentStrategyUser.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationStrategy.obj" \
	"$(INTDIR)\PortableServer\Default_Policy_Validator.obj" \
	"$(INTDIR)\PortableServer\POAManager.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationStrategyImplicit.obj" \
	"$(INTDIR)\PortableServer\POA_Current_Impl.obj" \
	"$(INTDIR)\PortableServer\ImR_Client_Adapter.obj" \
	"$(INTDIR)\PortableServer\Object_Adapter_Factory.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyAOMOnly.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionPolicy.obj" \
	"$(INTDIR)\PortableServer\POA_Guard.obj" \
	"$(INTDIR)\PortableServer\Collocated_Object_Proxy_Broker.obj"

"$(OUTDIR)\TAO_PortableServersd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_PortableServersd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_PortableServersd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\PortableServer\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_PortableServers.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_HAS_VALUETYPE_OUT_INDIRECTION -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.PortableServer.dep" "PortableServer\ForwardRequestC.cpp" "PortableServer\ForwardRequestA.cpp" "PortableServer\AdapterActivatorC.cpp" "PortableServer\AdapterActivatorA.cpp" "PortableServer\IdAssignmentPolicyC.cpp" "PortableServer\IdAssignmentPolicyA.cpp" "PortableServer\IdUniquenessPolicyC.cpp" "PortableServer\IdUniquenessPolicyA.cpp" "PortableServer\ImplicitActivationPolicyC.cpp" "PortableServer\ImplicitActivationPolicyA.cpp" "PortableServer\LifespanPolicyC.cpp" "PortableServer\LifespanPolicyA.cpp" "PortableServer\RequestProcessingPolicyC.cpp" "PortableServer\RequestProcessingPolicyA.cpp" "PortableServer\ServantActivatorC.cpp" "PortableServer\ServantActivatorA.cpp" "PortableServer\ServantLocatorC.cpp" "PortableServer\ServantLocatorA.cpp" "PortableServer\ServantManagerC.cpp" "PortableServer\ServantManagerA.cpp" "PortableServer\ServantRetentionPolicyC.cpp" "PortableServer\ServantRetentionPolicyA.cpp" "PortableServer\ThreadPolicyC.cpp" "PortableServer\ThreadPolicyA.cpp" "PortableServer\POAManagerC.cpp" "PortableServer\POAManagerFactoryC.cpp" "PortableServer\POAC.cpp" "PortableServer\PS_CurrentC.cpp" "PortableServer\PortableServer_includeC.cpp" "PortableServer\PortableServerC.cpp" "PortableServer\IdUniquenessPolicy.cpp" "PortableServer\StrategyFactory.cpp" "PortableServer\LifespanStrategyTransientFactoryImpl.cpp" "PortableServer\Collocated_Arguments_Converter.cpp" "PortableServer\RequestProcessingStrategyDefaultServant.cpp" "PortableServer\RequestProcessingStrategyDefaultServantFI.cpp" "PortableServer\ServantRetentionStrategyRetainFactoryImpl.cpp" "PortableServer\IdAssignmentStrategy.cpp" "PortableServer\ThreadStrategySingle.cpp" "PortableServer\Operation_Table_Binary_Search.cpp" "PortableServer\Acceptor_Filter_Factory.cpp" "PortableServer\Default_Acceptor_Filter.cpp" "PortableServer\Local_Servant_Base.cpp" "PortableServer\Root_POA.cpp" "PortableServer\PS_ForwardC.cpp" "PortableServer\ImplicitActivationStrategyFactoryImpl.cpp" "PortableServer\LifespanStrategyPersistent.cpp" "PortableServer\POA_Current.cpp" "PortableServer\Servant_Base.cpp" "PortableServer\Upcall_Command.cpp" "PortableServer\RequestProcessingPolicy.cpp" "PortableServer\RequestProcessingStrategyServantActivatorFI.cpp" "PortableServer\IdUniquenessStrategyFactoryImpl.cpp" "PortableServer\POAManagerFactory.cpp" "PortableServer\Operation_Table.cpp" "PortableServer\ORT_Adapter_Factory.cpp" "PortableServer\Network_Priority_Hook.cpp" "PortableServer\LifespanStrategy.cpp" "PortableServer\ImplicitActivationPolicy.cpp" "PortableServer\ThreadStrategySingleFactoryImpl.cpp" "PortableServer\Servant_Dispatcher.cpp" "PortableServer\RequestProcessingStrategyServantLocatorFI.cpp" "PortableServer\Non_Servant_Upcall.cpp" "PortableServer\POA_Current_Factory.cpp" "PortableServer\ServantRetentionStrategyRetain.cpp" "PortableServer\IdAssignmentStrategyFactoryImpl.cpp" "PortableServer\Operation_Table_Perfect_Hash.cpp" "PortableServer\Adapter_Activator.cpp" "PortableServer\ServantRetentionStrategyFactoryImpl.cpp" "PortableServer\LifespanStrategyTransient.cpp" "PortableServer\RequestProcessingStrategyServantLocator.cpp" "PortableServer\ThreadStrategyORBControl.cpp" "PortableServer\Active_Policy_Strategies.cpp" "PortableServer\LifespanStrategyPersistentFactoryImpl.cpp" "PortableServer\RequestProcessingStrategyAOMOnlyFactoryImpl.cpp" "PortableServer\IdUniquenessStrategyUnique.cpp" "PortableServer\RequestProcessingStrategyServantManager.cpp" "PortableServer\ServantRetentionStrategyNonRetainFactoryImpl.cpp" "PortableServer\Upcall_Wrapper.cpp" "PortableServer\RequestProcessingStrategyServantActivator.cpp" "PortableServer\LifespanStrategyFactoryImpl.cpp" "PortableServer\Servant_Upcall.cpp" "PortableServer\POA_Cached_Policies.cpp" "PortableServer\PortableServer_Functions.cpp" "PortableServer\Active_Object_Map_Entry.cpp" "PortableServer\IdUniquenessStrategyUniqueFactoryImpl.cpp" "PortableServer\PortableServer.cpp" "PortableServer\Active_Object_Map.cpp" "PortableServer\ThreadPolicy.cpp" "PortableServer\ThreadStrategy.cpp" "PortableServer\RequestProcessingStrategyFactoryImpl.cpp" "PortableServer\Direct_Collocation_Upcall_Wrapper.cpp" "PortableServer\Operation_Table_Dynamic_Hash.cpp" "PortableServer\ImplicitActivationStrategyExplicit.cpp" "PortableServer\Regular_POA.cpp" "PortableServer\Creation_Time.cpp" "PortableServer\Operation_Table_Linear_Search.cpp" "PortableServer\PolicyS.cpp" "PortableServer\Object_Adapter.cpp" "PortableServer\RequestProcessingStrategy.cpp" "PortableServer\IdAssignmentStrategySystem.cpp" "PortableServer\Default_Servant_Dispatcher.cpp" "PortableServer\ServantRetentionStrategyNonRetain.cpp" "PortableServer\PS_ForwardA.cpp" "PortableServer\Key_Adapters.cpp" "PortableServer\POA_Policy_Set.cpp" "PortableServer\IdUniquenessStrategyMultiple.cpp" "PortableServer\PortableServer_WFunctions.cpp" "PortableServer\LifespanPolicy.cpp" "PortableServer\IdAssignmentPolicy.cpp" "PortableServer\ThreadStrategyFactoryImpl.cpp" "PortableServer\IdAssignmentStrategyUser.cpp" "PortableServer\ImplicitActivationStrategy.cpp" "PortableServer\Default_Policy_Validator.cpp" "PortableServer\POAManager.cpp" "PortableServer\ImplicitActivationStrategyImplicit.cpp" "PortableServer\POA_Current_Impl.cpp" "PortableServer\ImR_Client_Adapter.cpp" "PortableServer\Object_Adapter_Factory.cpp" "PortableServer\RequestProcessingStrategyAOMOnly.cpp" "PortableServer\ServantRetentionPolicy.cpp" "PortableServer\POA_Guard.cpp" "PortableServer\Collocated_Object_Proxy_Broker.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_PortableServers.lib"
	-@del /f/q "$(OUTDIR)\TAO_PortableServers.exp"
	-@del /f/q "$(OUTDIR)\TAO_PortableServers.ilk"
	-@del /f/q "PortableServer\ForwardRequestC.h"
	-@del /f/q "PortableServer\ForwardRequestS.h"
	-@del /f/q "PortableServer\ForwardRequestA.h"
	-@del /f/q "PortableServer\ForwardRequestC.cpp"
	-@del /f/q "PortableServer\ForwardRequestA.cpp"
	-@del /f/q "PortableServer\AdapterActivatorC.h"
	-@del /f/q "PortableServer\AdapterActivatorS.h"
	-@del /f/q "PortableServer\AdapterActivatorA.h"
	-@del /f/q "PortableServer\AdapterActivatorC.cpp"
	-@del /f/q "PortableServer\AdapterActivatorA.cpp"
	-@del /f/q "PortableServer\IdAssignmentPolicyC.h"
	-@del /f/q "PortableServer\IdAssignmentPolicyS.h"
	-@del /f/q "PortableServer\IdAssignmentPolicyA.h"
	-@del /f/q "PortableServer\IdAssignmentPolicyC.cpp"
	-@del /f/q "PortableServer\IdAssignmentPolicyA.cpp"
	-@del /f/q "PortableServer\IdUniquenessPolicyC.h"
	-@del /f/q "PortableServer\IdUniquenessPolicyS.h"
	-@del /f/q "PortableServer\IdUniquenessPolicyA.h"
	-@del /f/q "PortableServer\IdUniquenessPolicyC.cpp"
	-@del /f/q "PortableServer\IdUniquenessPolicyA.cpp"
	-@del /f/q "PortableServer\ImplicitActivationPolicyC.h"
	-@del /f/q "PortableServer\ImplicitActivationPolicyS.h"
	-@del /f/q "PortableServer\ImplicitActivationPolicyA.h"
	-@del /f/q "PortableServer\ImplicitActivationPolicyC.cpp"
	-@del /f/q "PortableServer\ImplicitActivationPolicyA.cpp"
	-@del /f/q "PortableServer\LifespanPolicyC.h"
	-@del /f/q "PortableServer\LifespanPolicyS.h"
	-@del /f/q "PortableServer\LifespanPolicyA.h"
	-@del /f/q "PortableServer\LifespanPolicyC.cpp"
	-@del /f/q "PortableServer\LifespanPolicyA.cpp"
	-@del /f/q "PortableServer\RequestProcessingPolicyC.h"
	-@del /f/q "PortableServer\RequestProcessingPolicyS.h"
	-@del /f/q "PortableServer\RequestProcessingPolicyA.h"
	-@del /f/q "PortableServer\RequestProcessingPolicyC.cpp"
	-@del /f/q "PortableServer\RequestProcessingPolicyA.cpp"
	-@del /f/q "PortableServer\ServantActivatorC.h"
	-@del /f/q "PortableServer\ServantActivatorS.h"
	-@del /f/q "PortableServer\ServantActivatorA.h"
	-@del /f/q "PortableServer\ServantActivatorC.cpp"
	-@del /f/q "PortableServer\ServantActivatorA.cpp"
	-@del /f/q "PortableServer\ServantLocatorC.h"
	-@del /f/q "PortableServer\ServantLocatorS.h"
	-@del /f/q "PortableServer\ServantLocatorA.h"
	-@del /f/q "PortableServer\ServantLocatorC.cpp"
	-@del /f/q "PortableServer\ServantLocatorA.cpp"
	-@del /f/q "PortableServer\ServantManagerC.h"
	-@del /f/q "PortableServer\ServantManagerS.h"
	-@del /f/q "PortableServer\ServantManagerA.h"
	-@del /f/q "PortableServer\ServantManagerC.cpp"
	-@del /f/q "PortableServer\ServantManagerA.cpp"
	-@del /f/q "PortableServer\ServantRetentionPolicyC.h"
	-@del /f/q "PortableServer\ServantRetentionPolicyS.h"
	-@del /f/q "PortableServer\ServantRetentionPolicyA.h"
	-@del /f/q "PortableServer\ServantRetentionPolicyC.cpp"
	-@del /f/q "PortableServer\ServantRetentionPolicyA.cpp"
	-@del /f/q "PortableServer\ThreadPolicyC.h"
	-@del /f/q "PortableServer\ThreadPolicyS.h"
	-@del /f/q "PortableServer\ThreadPolicyA.h"
	-@del /f/q "PortableServer\ThreadPolicyC.cpp"
	-@del /f/q "PortableServer\ThreadPolicyA.cpp"
	-@del /f/q "PortableServer\POAManagerC.h"
	-@del /f/q "PortableServer\POAManagerS.h"
	-@del /f/q "PortableServer\POAManagerC.cpp"
	-@del /f/q "PortableServer\POAManagerFactoryC.h"
	-@del /f/q "PortableServer\POAManagerFactoryS.h"
	-@del /f/q "PortableServer\POAManagerFactoryC.cpp"
	-@del /f/q "PortableServer\POAC.h"
	-@del /f/q "PortableServer\POAS.h"
	-@del /f/q "PortableServer\POAC.cpp"
	-@del /f/q "PortableServer\PS_CurrentC.h"
	-@del /f/q "PortableServer\PS_CurrentS.h"
	-@del /f/q "PortableServer\PS_CurrentC.cpp"
	-@del /f/q "PortableServer\PortableServer_includeC.h"
	-@del /f/q "PortableServer\PortableServer_includeS.h"
	-@del /f/q "PortableServer\PortableServer_includeA.h"
	-@del /f/q "PortableServer\PortableServer_includeC.cpp"
	-@del /f/q "PortableServer\PortableServerC.h"
	-@del /f/q "PortableServer\PortableServerS.h"
	-@del /f/q "PortableServer\PortableServerA.h"
	-@del /f/q "PortableServer\PortableServerC.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\PortableServer\$(NULL)" mkdir "Static_Release\PortableServer"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_HAS_VALUETYPE_OUT_INDIRECTION /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_PortableServers.lib"
LINK32_OBJS= \
	"$(INTDIR)\PortableServer\ForwardRequestC.obj" \
	"$(INTDIR)\PortableServer\ForwardRequestA.obj" \
	"$(INTDIR)\PortableServer\AdapterActivatorC.obj" \
	"$(INTDIR)\PortableServer\AdapterActivatorA.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentPolicyC.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentPolicyA.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessPolicyC.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessPolicyA.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationPolicyC.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationPolicyA.obj" \
	"$(INTDIR)\PortableServer\LifespanPolicyC.obj" \
	"$(INTDIR)\PortableServer\LifespanPolicyA.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingPolicyC.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingPolicyA.obj" \
	"$(INTDIR)\PortableServer\ServantActivatorC.obj" \
	"$(INTDIR)\PortableServer\ServantActivatorA.obj" \
	"$(INTDIR)\PortableServer\ServantLocatorC.obj" \
	"$(INTDIR)\PortableServer\ServantLocatorA.obj" \
	"$(INTDIR)\PortableServer\ServantManagerC.obj" \
	"$(INTDIR)\PortableServer\ServantManagerA.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionPolicyC.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionPolicyA.obj" \
	"$(INTDIR)\PortableServer\ThreadPolicyC.obj" \
	"$(INTDIR)\PortableServer\ThreadPolicyA.obj" \
	"$(INTDIR)\PortableServer\POAManagerC.obj" \
	"$(INTDIR)\PortableServer\POAManagerFactoryC.obj" \
	"$(INTDIR)\PortableServer\POAC.obj" \
	"$(INTDIR)\PortableServer\PS_CurrentC.obj" \
	"$(INTDIR)\PortableServer\PortableServer_includeC.obj" \
	"$(INTDIR)\PortableServer\PortableServerC.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessPolicy.obj" \
	"$(INTDIR)\PortableServer\StrategyFactory.obj" \
	"$(INTDIR)\PortableServer\LifespanStrategyTransientFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\Collocated_Arguments_Converter.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyDefaultServant.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyDefaultServantFI.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionStrategyRetainFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentStrategy.obj" \
	"$(INTDIR)\PortableServer\ThreadStrategySingle.obj" \
	"$(INTDIR)\PortableServer\Operation_Table_Binary_Search.obj" \
	"$(INTDIR)\PortableServer\Acceptor_Filter_Factory.obj" \
	"$(INTDIR)\PortableServer\Default_Acceptor_Filter.obj" \
	"$(INTDIR)\PortableServer\Local_Servant_Base.obj" \
	"$(INTDIR)\PortableServer\Root_POA.obj" \
	"$(INTDIR)\PortableServer\PS_ForwardC.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\LifespanStrategyPersistent.obj" \
	"$(INTDIR)\PortableServer\POA_Current.obj" \
	"$(INTDIR)\PortableServer\Servant_Base.obj" \
	"$(INTDIR)\PortableServer\Upcall_Command.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingPolicy.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyServantActivatorFI.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\POAManagerFactory.obj" \
	"$(INTDIR)\PortableServer\Operation_Table.obj" \
	"$(INTDIR)\PortableServer\ORT_Adapter_Factory.obj" \
	"$(INTDIR)\PortableServer\Network_Priority_Hook.obj" \
	"$(INTDIR)\PortableServer\LifespanStrategy.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationPolicy.obj" \
	"$(INTDIR)\PortableServer\ThreadStrategySingleFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\Servant_Dispatcher.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyServantLocatorFI.obj" \
	"$(INTDIR)\PortableServer\Non_Servant_Upcall.obj" \
	"$(INTDIR)\PortableServer\POA_Current_Factory.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionStrategyRetain.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\Operation_Table_Perfect_Hash.obj" \
	"$(INTDIR)\PortableServer\Adapter_Activator.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\LifespanStrategyTransient.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyServantLocator.obj" \
	"$(INTDIR)\PortableServer\ThreadStrategyORBControl.obj" \
	"$(INTDIR)\PortableServer\Active_Policy_Strategies.obj" \
	"$(INTDIR)\PortableServer\LifespanStrategyPersistentFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyAOMOnlyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessStrategyUnique.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyServantManager.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionStrategyNonRetainFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\Upcall_Wrapper.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyServantActivator.obj" \
	"$(INTDIR)\PortableServer\LifespanStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\Servant_Upcall.obj" \
	"$(INTDIR)\PortableServer\POA_Cached_Policies.obj" \
	"$(INTDIR)\PortableServer\PortableServer_Functions.obj" \
	"$(INTDIR)\PortableServer\Active_Object_Map_Entry.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessStrategyUniqueFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\PortableServer.obj" \
	"$(INTDIR)\PortableServer\Active_Object_Map.obj" \
	"$(INTDIR)\PortableServer\ThreadPolicy.obj" \
	"$(INTDIR)\PortableServer\ThreadStrategy.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\Direct_Collocation_Upcall_Wrapper.obj" \
	"$(INTDIR)\PortableServer\Operation_Table_Dynamic_Hash.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationStrategyExplicit.obj" \
	"$(INTDIR)\PortableServer\Regular_POA.obj" \
	"$(INTDIR)\PortableServer\Creation_Time.obj" \
	"$(INTDIR)\PortableServer\Operation_Table_Linear_Search.obj" \
	"$(INTDIR)\PortableServer\PolicyS.obj" \
	"$(INTDIR)\PortableServer\Object_Adapter.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategy.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentStrategySystem.obj" \
	"$(INTDIR)\PortableServer\Default_Servant_Dispatcher.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionStrategyNonRetain.obj" \
	"$(INTDIR)\PortableServer\PS_ForwardA.obj" \
	"$(INTDIR)\PortableServer\Key_Adapters.obj" \
	"$(INTDIR)\PortableServer\POA_Policy_Set.obj" \
	"$(INTDIR)\PortableServer\IdUniquenessStrategyMultiple.obj" \
	"$(INTDIR)\PortableServer\PortableServer_WFunctions.obj" \
	"$(INTDIR)\PortableServer\LifespanPolicy.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentPolicy.obj" \
	"$(INTDIR)\PortableServer\ThreadStrategyFactoryImpl.obj" \
	"$(INTDIR)\PortableServer\IdAssignmentStrategyUser.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationStrategy.obj" \
	"$(INTDIR)\PortableServer\Default_Policy_Validator.obj" \
	"$(INTDIR)\PortableServer\POAManager.obj" \
	"$(INTDIR)\PortableServer\ImplicitActivationStrategyImplicit.obj" \
	"$(INTDIR)\PortableServer\POA_Current_Impl.obj" \
	"$(INTDIR)\PortableServer\ImR_Client_Adapter.obj" \
	"$(INTDIR)\PortableServer\Object_Adapter_Factory.obj" \
	"$(INTDIR)\PortableServer\RequestProcessingStrategyAOMOnly.obj" \
	"$(INTDIR)\PortableServer\ServantRetentionPolicy.obj" \
	"$(INTDIR)\PortableServer\POA_Guard.obj" \
	"$(INTDIR)\PortableServer\Collocated_Object_Proxy_Broker.obj"

"$(OUTDIR)\TAO_PortableServers.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_PortableServers.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_PortableServers.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.PortableServer.dep")
!INCLUDE "Makefile.PortableServer.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="PortableServer\ForwardRequestC.cpp"

"$(INTDIR)\PortableServer\ForwardRequestC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ForwardRequestC.obj" $(SOURCE)

SOURCE="PortableServer\ForwardRequestA.cpp"

"$(INTDIR)\PortableServer\ForwardRequestA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ForwardRequestA.obj" $(SOURCE)

SOURCE="PortableServer\AdapterActivatorC.cpp"

"$(INTDIR)\PortableServer\AdapterActivatorC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\AdapterActivatorC.obj" $(SOURCE)

SOURCE="PortableServer\AdapterActivatorA.cpp"

"$(INTDIR)\PortableServer\AdapterActivatorA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\AdapterActivatorA.obj" $(SOURCE)

SOURCE="PortableServer\IdAssignmentPolicyC.cpp"

"$(INTDIR)\PortableServer\IdAssignmentPolicyC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\IdAssignmentPolicyC.obj" $(SOURCE)

SOURCE="PortableServer\IdAssignmentPolicyA.cpp"

"$(INTDIR)\PortableServer\IdAssignmentPolicyA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\IdAssignmentPolicyA.obj" $(SOURCE)

SOURCE="PortableServer\IdUniquenessPolicyC.cpp"

"$(INTDIR)\PortableServer\IdUniquenessPolicyC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\IdUniquenessPolicyC.obj" $(SOURCE)

SOURCE="PortableServer\IdUniquenessPolicyA.cpp"

"$(INTDIR)\PortableServer\IdUniquenessPolicyA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\IdUniquenessPolicyA.obj" $(SOURCE)

SOURCE="PortableServer\ImplicitActivationPolicyC.cpp"

"$(INTDIR)\PortableServer\ImplicitActivationPolicyC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ImplicitActivationPolicyC.obj" $(SOURCE)

SOURCE="PortableServer\ImplicitActivationPolicyA.cpp"

"$(INTDIR)\PortableServer\ImplicitActivationPolicyA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ImplicitActivationPolicyA.obj" $(SOURCE)

SOURCE="PortableServer\LifespanPolicyC.cpp"

"$(INTDIR)\PortableServer\LifespanPolicyC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\LifespanPolicyC.obj" $(SOURCE)

SOURCE="PortableServer\LifespanPolicyA.cpp"

"$(INTDIR)\PortableServer\LifespanPolicyA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\LifespanPolicyA.obj" $(SOURCE)

SOURCE="PortableServer\RequestProcessingPolicyC.cpp"

"$(INTDIR)\PortableServer\RequestProcessingPolicyC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\RequestProcessingPolicyC.obj" $(SOURCE)

SOURCE="PortableServer\RequestProcessingPolicyA.cpp"

"$(INTDIR)\PortableServer\RequestProcessingPolicyA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\RequestProcessingPolicyA.obj" $(SOURCE)

SOURCE="PortableServer\ServantActivatorC.cpp"

"$(INTDIR)\PortableServer\ServantActivatorC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ServantActivatorC.obj" $(SOURCE)

SOURCE="PortableServer\ServantActivatorA.cpp"

"$(INTDIR)\PortableServer\ServantActivatorA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ServantActivatorA.obj" $(SOURCE)

SOURCE="PortableServer\ServantLocatorC.cpp"

"$(INTDIR)\PortableServer\ServantLocatorC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ServantLocatorC.obj" $(SOURCE)

SOURCE="PortableServer\ServantLocatorA.cpp"

"$(INTDIR)\PortableServer\ServantLocatorA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ServantLocatorA.obj" $(SOURCE)

SOURCE="PortableServer\ServantManagerC.cpp"

"$(INTDIR)\PortableServer\ServantManagerC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ServantManagerC.obj" $(SOURCE)

SOURCE="PortableServer\ServantManagerA.cpp"

"$(INTDIR)\PortableServer\ServantManagerA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ServantManagerA.obj" $(SOURCE)

SOURCE="PortableServer\ServantRetentionPolicyC.cpp"

"$(INTDIR)\PortableServer\ServantRetentionPolicyC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ServantRetentionPolicyC.obj" $(SOURCE)

SOURCE="PortableServer\ServantRetentionPolicyA.cpp"

"$(INTDIR)\PortableServer\ServantRetentionPolicyA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ServantRetentionPolicyA.obj" $(SOURCE)

SOURCE="PortableServer\ThreadPolicyC.cpp"

"$(INTDIR)\PortableServer\ThreadPolicyC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ThreadPolicyC.obj" $(SOURCE)

SOURCE="PortableServer\ThreadPolicyA.cpp"

"$(INTDIR)\PortableServer\ThreadPolicyA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ThreadPolicyA.obj" $(SOURCE)

SOURCE="PortableServer\POAManagerC.cpp"

"$(INTDIR)\PortableServer\POAManagerC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\POAManagerC.obj" $(SOURCE)

SOURCE="PortableServer\POAManagerFactoryC.cpp"

"$(INTDIR)\PortableServer\POAManagerFactoryC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\POAManagerFactoryC.obj" $(SOURCE)

SOURCE="PortableServer\POAC.cpp"

"$(INTDIR)\PortableServer\POAC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\POAC.obj" $(SOURCE)

SOURCE="PortableServer\PS_CurrentC.cpp"

"$(INTDIR)\PortableServer\PS_CurrentC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\PS_CurrentC.obj" $(SOURCE)

SOURCE="PortableServer\PortableServer_includeC.cpp"

"$(INTDIR)\PortableServer\PortableServer_includeC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\PortableServer_includeC.obj" $(SOURCE)

SOURCE="PortableServer\PortableServerC.cpp"

"$(INTDIR)\PortableServer\PortableServerC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\PortableServerC.obj" $(SOURCE)

SOURCE="PortableServer\IdUniquenessPolicy.cpp"

"$(INTDIR)\PortableServer\IdUniquenessPolicy.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\IdUniquenessPolicy.obj" $(SOURCE)

SOURCE="PortableServer\StrategyFactory.cpp"

"$(INTDIR)\PortableServer\StrategyFactory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\StrategyFactory.obj" $(SOURCE)

SOURCE="PortableServer\LifespanStrategyTransientFactoryImpl.cpp"

"$(INTDIR)\PortableServer\LifespanStrategyTransientFactoryImpl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\LifespanStrategyTransientFactoryImpl.obj" $(SOURCE)

SOURCE="PortableServer\Collocated_Arguments_Converter.cpp"

"$(INTDIR)\PortableServer\Collocated_Arguments_Converter.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Collocated_Arguments_Converter.obj" $(SOURCE)

SOURCE="PortableServer\RequestProcessingStrategyDefaultServant.cpp"

"$(INTDIR)\PortableServer\RequestProcessingStrategyDefaultServant.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\RequestProcessingStrategyDefaultServant.obj" $(SOURCE)

SOURCE="PortableServer\RequestProcessingStrategyDefaultServantFI.cpp"

"$(INTDIR)\PortableServer\RequestProcessingStrategyDefaultServantFI.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\RequestProcessingStrategyDefaultServantFI.obj" $(SOURCE)

SOURCE="PortableServer\ServantRetentionStrategyRetainFactoryImpl.cpp"

"$(INTDIR)\PortableServer\ServantRetentionStrategyRetainFactoryImpl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ServantRetentionStrategyRetainFactoryImpl.obj" $(SOURCE)

SOURCE="PortableServer\IdAssignmentStrategy.cpp"

"$(INTDIR)\PortableServer\IdAssignmentStrategy.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\IdAssignmentStrategy.obj" $(SOURCE)

SOURCE="PortableServer\ThreadStrategySingle.cpp"

"$(INTDIR)\PortableServer\ThreadStrategySingle.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ThreadStrategySingle.obj" $(SOURCE)

SOURCE="PortableServer\Operation_Table_Binary_Search.cpp"

"$(INTDIR)\PortableServer\Operation_Table_Binary_Search.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Operation_Table_Binary_Search.obj" $(SOURCE)

SOURCE="PortableServer\Acceptor_Filter_Factory.cpp"

"$(INTDIR)\PortableServer\Acceptor_Filter_Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Acceptor_Filter_Factory.obj" $(SOURCE)

SOURCE="PortableServer\Default_Acceptor_Filter.cpp"

"$(INTDIR)\PortableServer\Default_Acceptor_Filter.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Default_Acceptor_Filter.obj" $(SOURCE)

SOURCE="PortableServer\Local_Servant_Base.cpp"

"$(INTDIR)\PortableServer\Local_Servant_Base.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Local_Servant_Base.obj" $(SOURCE)

SOURCE="PortableServer\Root_POA.cpp"

"$(INTDIR)\PortableServer\Root_POA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Root_POA.obj" $(SOURCE)

SOURCE="PortableServer\PS_ForwardC.cpp"

"$(INTDIR)\PortableServer\PS_ForwardC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\PS_ForwardC.obj" $(SOURCE)

SOURCE="PortableServer\ImplicitActivationStrategyFactoryImpl.cpp"

"$(INTDIR)\PortableServer\ImplicitActivationStrategyFactoryImpl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ImplicitActivationStrategyFactoryImpl.obj" $(SOURCE)

SOURCE="PortableServer\LifespanStrategyPersistent.cpp"

"$(INTDIR)\PortableServer\LifespanStrategyPersistent.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\LifespanStrategyPersistent.obj" $(SOURCE)

SOURCE="PortableServer\POA_Current.cpp"

"$(INTDIR)\PortableServer\POA_Current.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\POA_Current.obj" $(SOURCE)

SOURCE="PortableServer\Servant_Base.cpp"

"$(INTDIR)\PortableServer\Servant_Base.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Servant_Base.obj" $(SOURCE)

SOURCE="PortableServer\Upcall_Command.cpp"

"$(INTDIR)\PortableServer\Upcall_Command.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Upcall_Command.obj" $(SOURCE)

SOURCE="PortableServer\RequestProcessingPolicy.cpp"

"$(INTDIR)\PortableServer\RequestProcessingPolicy.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\RequestProcessingPolicy.obj" $(SOURCE)

SOURCE="PortableServer\RequestProcessingStrategyServantActivatorFI.cpp"

"$(INTDIR)\PortableServer\RequestProcessingStrategyServantActivatorFI.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\RequestProcessingStrategyServantActivatorFI.obj" $(SOURCE)

SOURCE="PortableServer\IdUniquenessStrategyFactoryImpl.cpp"

"$(INTDIR)\PortableServer\IdUniquenessStrategyFactoryImpl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\IdUniquenessStrategyFactoryImpl.obj" $(SOURCE)

SOURCE="PortableServer\POAManagerFactory.cpp"

"$(INTDIR)\PortableServer\POAManagerFactory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\POAManagerFactory.obj" $(SOURCE)

SOURCE="PortableServer\Operation_Table.cpp"

"$(INTDIR)\PortableServer\Operation_Table.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Operation_Table.obj" $(SOURCE)

SOURCE="PortableServer\ORT_Adapter_Factory.cpp"

"$(INTDIR)\PortableServer\ORT_Adapter_Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ORT_Adapter_Factory.obj" $(SOURCE)

SOURCE="PortableServer\Network_Priority_Hook.cpp"

"$(INTDIR)\PortableServer\Network_Priority_Hook.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Network_Priority_Hook.obj" $(SOURCE)

SOURCE="PortableServer\LifespanStrategy.cpp"

"$(INTDIR)\PortableServer\LifespanStrategy.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\LifespanStrategy.obj" $(SOURCE)

SOURCE="PortableServer\ImplicitActivationPolicy.cpp"

"$(INTDIR)\PortableServer\ImplicitActivationPolicy.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ImplicitActivationPolicy.obj" $(SOURCE)

SOURCE="PortableServer\ThreadStrategySingleFactoryImpl.cpp"

"$(INTDIR)\PortableServer\ThreadStrategySingleFactoryImpl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ThreadStrategySingleFactoryImpl.obj" $(SOURCE)

SOURCE="PortableServer\Servant_Dispatcher.cpp"

"$(INTDIR)\PortableServer\Servant_Dispatcher.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Servant_Dispatcher.obj" $(SOURCE)

SOURCE="PortableServer\RequestProcessingStrategyServantLocatorFI.cpp"

"$(INTDIR)\PortableServer\RequestProcessingStrategyServantLocatorFI.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\RequestProcessingStrategyServantLocatorFI.obj" $(SOURCE)

SOURCE="PortableServer\Non_Servant_Upcall.cpp"

"$(INTDIR)\PortableServer\Non_Servant_Upcall.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Non_Servant_Upcall.obj" $(SOURCE)

SOURCE="PortableServer\POA_Current_Factory.cpp"

"$(INTDIR)\PortableServer\POA_Current_Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\POA_Current_Factory.obj" $(SOURCE)

SOURCE="PortableServer\ServantRetentionStrategyRetain.cpp"

"$(INTDIR)\PortableServer\ServantRetentionStrategyRetain.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ServantRetentionStrategyRetain.obj" $(SOURCE)

SOURCE="PortableServer\IdAssignmentStrategyFactoryImpl.cpp"

"$(INTDIR)\PortableServer\IdAssignmentStrategyFactoryImpl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\IdAssignmentStrategyFactoryImpl.obj" $(SOURCE)

SOURCE="PortableServer\Operation_Table_Perfect_Hash.cpp"

"$(INTDIR)\PortableServer\Operation_Table_Perfect_Hash.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Operation_Table_Perfect_Hash.obj" $(SOURCE)

SOURCE="PortableServer\Adapter_Activator.cpp"

"$(INTDIR)\PortableServer\Adapter_Activator.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Adapter_Activator.obj" $(SOURCE)

SOURCE="PortableServer\ServantRetentionStrategyFactoryImpl.cpp"

"$(INTDIR)\PortableServer\ServantRetentionStrategyFactoryImpl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ServantRetentionStrategyFactoryImpl.obj" $(SOURCE)

SOURCE="PortableServer\LifespanStrategyTransient.cpp"

"$(INTDIR)\PortableServer\LifespanStrategyTransient.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\LifespanStrategyTransient.obj" $(SOURCE)

SOURCE="PortableServer\RequestProcessingStrategyServantLocator.cpp"

"$(INTDIR)\PortableServer\RequestProcessingStrategyServantLocator.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\RequestProcessingStrategyServantLocator.obj" $(SOURCE)

SOURCE="PortableServer\ThreadStrategyORBControl.cpp"

"$(INTDIR)\PortableServer\ThreadStrategyORBControl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ThreadStrategyORBControl.obj" $(SOURCE)

SOURCE="PortableServer\Active_Policy_Strategies.cpp"

"$(INTDIR)\PortableServer\Active_Policy_Strategies.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Active_Policy_Strategies.obj" $(SOURCE)

SOURCE="PortableServer\LifespanStrategyPersistentFactoryImpl.cpp"

"$(INTDIR)\PortableServer\LifespanStrategyPersistentFactoryImpl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\LifespanStrategyPersistentFactoryImpl.obj" $(SOURCE)

SOURCE="PortableServer\RequestProcessingStrategyAOMOnlyFactoryImpl.cpp"

"$(INTDIR)\PortableServer\RequestProcessingStrategyAOMOnlyFactoryImpl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\RequestProcessingStrategyAOMOnlyFactoryImpl.obj" $(SOURCE)

SOURCE="PortableServer\IdUniquenessStrategyUnique.cpp"

"$(INTDIR)\PortableServer\IdUniquenessStrategyUnique.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\IdUniquenessStrategyUnique.obj" $(SOURCE)

SOURCE="PortableServer\RequestProcessingStrategyServantManager.cpp"

"$(INTDIR)\PortableServer\RequestProcessingStrategyServantManager.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\RequestProcessingStrategyServantManager.obj" $(SOURCE)

SOURCE="PortableServer\ServantRetentionStrategyNonRetainFactoryImpl.cpp"

"$(INTDIR)\PortableServer\ServantRetentionStrategyNonRetainFactoryImpl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ServantRetentionStrategyNonRetainFactoryImpl.obj" $(SOURCE)

SOURCE="PortableServer\Upcall_Wrapper.cpp"

"$(INTDIR)\PortableServer\Upcall_Wrapper.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Upcall_Wrapper.obj" $(SOURCE)

SOURCE="PortableServer\RequestProcessingStrategyServantActivator.cpp"

"$(INTDIR)\PortableServer\RequestProcessingStrategyServantActivator.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\RequestProcessingStrategyServantActivator.obj" $(SOURCE)

SOURCE="PortableServer\LifespanStrategyFactoryImpl.cpp"

"$(INTDIR)\PortableServer\LifespanStrategyFactoryImpl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\LifespanStrategyFactoryImpl.obj" $(SOURCE)

SOURCE="PortableServer\Servant_Upcall.cpp"

"$(INTDIR)\PortableServer\Servant_Upcall.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Servant_Upcall.obj" $(SOURCE)

SOURCE="PortableServer\POA_Cached_Policies.cpp"

"$(INTDIR)\PortableServer\POA_Cached_Policies.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\POA_Cached_Policies.obj" $(SOURCE)

SOURCE="PortableServer\PortableServer_Functions.cpp"

"$(INTDIR)\PortableServer\PortableServer_Functions.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\PortableServer_Functions.obj" $(SOURCE)

SOURCE="PortableServer\Active_Object_Map_Entry.cpp"

"$(INTDIR)\PortableServer\Active_Object_Map_Entry.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Active_Object_Map_Entry.obj" $(SOURCE)

SOURCE="PortableServer\IdUniquenessStrategyUniqueFactoryImpl.cpp"

"$(INTDIR)\PortableServer\IdUniquenessStrategyUniqueFactoryImpl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\IdUniquenessStrategyUniqueFactoryImpl.obj" $(SOURCE)

SOURCE="PortableServer\PortableServer.cpp"

"$(INTDIR)\PortableServer\PortableServer.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\PortableServer.obj" $(SOURCE)

SOURCE="PortableServer\Active_Object_Map.cpp"

"$(INTDIR)\PortableServer\Active_Object_Map.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Active_Object_Map.obj" $(SOURCE)

SOURCE="PortableServer\ThreadPolicy.cpp"

"$(INTDIR)\PortableServer\ThreadPolicy.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ThreadPolicy.obj" $(SOURCE)

SOURCE="PortableServer\ThreadStrategy.cpp"

"$(INTDIR)\PortableServer\ThreadStrategy.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ThreadStrategy.obj" $(SOURCE)

SOURCE="PortableServer\RequestProcessingStrategyFactoryImpl.cpp"

"$(INTDIR)\PortableServer\RequestProcessingStrategyFactoryImpl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\RequestProcessingStrategyFactoryImpl.obj" $(SOURCE)

SOURCE="PortableServer\Direct_Collocation_Upcall_Wrapper.cpp"

"$(INTDIR)\PortableServer\Direct_Collocation_Upcall_Wrapper.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Direct_Collocation_Upcall_Wrapper.obj" $(SOURCE)

SOURCE="PortableServer\Operation_Table_Dynamic_Hash.cpp"

"$(INTDIR)\PortableServer\Operation_Table_Dynamic_Hash.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Operation_Table_Dynamic_Hash.obj" $(SOURCE)

SOURCE="PortableServer\ImplicitActivationStrategyExplicit.cpp"

"$(INTDIR)\PortableServer\ImplicitActivationStrategyExplicit.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ImplicitActivationStrategyExplicit.obj" $(SOURCE)

SOURCE="PortableServer\Regular_POA.cpp"

"$(INTDIR)\PortableServer\Regular_POA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Regular_POA.obj" $(SOURCE)

SOURCE="PortableServer\Creation_Time.cpp"

"$(INTDIR)\PortableServer\Creation_Time.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Creation_Time.obj" $(SOURCE)

SOURCE="PortableServer\Operation_Table_Linear_Search.cpp"

"$(INTDIR)\PortableServer\Operation_Table_Linear_Search.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Operation_Table_Linear_Search.obj" $(SOURCE)

SOURCE="PortableServer\PolicyS.cpp"

"$(INTDIR)\PortableServer\PolicyS.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\PolicyS.obj" $(SOURCE)

SOURCE="PortableServer\Object_Adapter.cpp"

"$(INTDIR)\PortableServer\Object_Adapter.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Object_Adapter.obj" $(SOURCE)

SOURCE="PortableServer\RequestProcessingStrategy.cpp"

"$(INTDIR)\PortableServer\RequestProcessingStrategy.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\RequestProcessingStrategy.obj" $(SOURCE)

SOURCE="PortableServer\IdAssignmentStrategySystem.cpp"

"$(INTDIR)\PortableServer\IdAssignmentStrategySystem.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\IdAssignmentStrategySystem.obj" $(SOURCE)

SOURCE="PortableServer\Default_Servant_Dispatcher.cpp"

"$(INTDIR)\PortableServer\Default_Servant_Dispatcher.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Default_Servant_Dispatcher.obj" $(SOURCE)

SOURCE="PortableServer\ServantRetentionStrategyNonRetain.cpp"

"$(INTDIR)\PortableServer\ServantRetentionStrategyNonRetain.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ServantRetentionStrategyNonRetain.obj" $(SOURCE)

SOURCE="PortableServer\PS_ForwardA.cpp"

"$(INTDIR)\PortableServer\PS_ForwardA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\PS_ForwardA.obj" $(SOURCE)

SOURCE="PortableServer\Key_Adapters.cpp"

"$(INTDIR)\PortableServer\Key_Adapters.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Key_Adapters.obj" $(SOURCE)

SOURCE="PortableServer\POA_Policy_Set.cpp"

"$(INTDIR)\PortableServer\POA_Policy_Set.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\POA_Policy_Set.obj" $(SOURCE)

SOURCE="PortableServer\IdUniquenessStrategyMultiple.cpp"

"$(INTDIR)\PortableServer\IdUniquenessStrategyMultiple.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\IdUniquenessStrategyMultiple.obj" $(SOURCE)

SOURCE="PortableServer\PortableServer_WFunctions.cpp"

"$(INTDIR)\PortableServer\PortableServer_WFunctions.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\PortableServer_WFunctions.obj" $(SOURCE)

SOURCE="PortableServer\LifespanPolicy.cpp"

"$(INTDIR)\PortableServer\LifespanPolicy.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\LifespanPolicy.obj" $(SOURCE)

SOURCE="PortableServer\IdAssignmentPolicy.cpp"

"$(INTDIR)\PortableServer\IdAssignmentPolicy.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\IdAssignmentPolicy.obj" $(SOURCE)

SOURCE="PortableServer\ThreadStrategyFactoryImpl.cpp"

"$(INTDIR)\PortableServer\ThreadStrategyFactoryImpl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ThreadStrategyFactoryImpl.obj" $(SOURCE)

SOURCE="PortableServer\IdAssignmentStrategyUser.cpp"

"$(INTDIR)\PortableServer\IdAssignmentStrategyUser.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\IdAssignmentStrategyUser.obj" $(SOURCE)

SOURCE="PortableServer\ImplicitActivationStrategy.cpp"

"$(INTDIR)\PortableServer\ImplicitActivationStrategy.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ImplicitActivationStrategy.obj" $(SOURCE)

SOURCE="PortableServer\Default_Policy_Validator.cpp"

"$(INTDIR)\PortableServer\Default_Policy_Validator.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Default_Policy_Validator.obj" $(SOURCE)

SOURCE="PortableServer\POAManager.cpp"

"$(INTDIR)\PortableServer\POAManager.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\POAManager.obj" $(SOURCE)

SOURCE="PortableServer\ImplicitActivationStrategyImplicit.cpp"

"$(INTDIR)\PortableServer\ImplicitActivationStrategyImplicit.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ImplicitActivationStrategyImplicit.obj" $(SOURCE)

SOURCE="PortableServer\POA_Current_Impl.cpp"

"$(INTDIR)\PortableServer\POA_Current_Impl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\POA_Current_Impl.obj" $(SOURCE)

SOURCE="PortableServer\ImR_Client_Adapter.cpp"

"$(INTDIR)\PortableServer\ImR_Client_Adapter.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ImR_Client_Adapter.obj" $(SOURCE)

SOURCE="PortableServer\Object_Adapter_Factory.cpp"

"$(INTDIR)\PortableServer\Object_Adapter_Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Object_Adapter_Factory.obj" $(SOURCE)

SOURCE="PortableServer\RequestProcessingStrategyAOMOnly.cpp"

"$(INTDIR)\PortableServer\RequestProcessingStrategyAOMOnly.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\RequestProcessingStrategyAOMOnly.obj" $(SOURCE)

SOURCE="PortableServer\ServantRetentionPolicy.cpp"

"$(INTDIR)\PortableServer\ServantRetentionPolicy.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\ServantRetentionPolicy.obj" $(SOURCE)

SOURCE="PortableServer\POA_Guard.cpp"

"$(INTDIR)\PortableServer\POA_Guard.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\POA_Guard.obj" $(SOURCE)

SOURCE="PortableServer\Collocated_Object_Proxy_Broker.cpp"

"$(INTDIR)\PortableServer\Collocated_Object_Proxy_Broker.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PortableServer\Collocated_Object_Proxy_Broker.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="PortableServer\ForwardRequest.pidl"

InputPath=PortableServer\ForwardRequest.pidl

"PortableServer\ForwardRequestC.h" "PortableServer\ForwardRequestS.h" "PortableServer\ForwardRequestA.h" "PortableServer\ForwardRequestC.cpp" "PortableServer\ForwardRequestA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PortableServer_ForwardRequest_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\AdapterActivator.pidl"

InputPath=PortableServer\AdapterActivator.pidl

"PortableServer\AdapterActivatorC.h" "PortableServer\AdapterActivatorS.h" "PortableServer\AdapterActivatorA.h" "PortableServer\AdapterActivatorC.cpp" "PortableServer\AdapterActivatorA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PortableServer_AdapterActivator_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\IdAssignmentPolicy.pidl"

InputPath=PortableServer\IdAssignmentPolicy.pidl

"PortableServer\IdAssignmentPolicyC.h" "PortableServer\IdAssignmentPolicyS.h" "PortableServer\IdAssignmentPolicyA.h" "PortableServer\IdAssignmentPolicyC.cpp" "PortableServer\IdAssignmentPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PortableServer_IdAssignmentPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\IdUniquenessPolicy.pidl"

InputPath=PortableServer\IdUniquenessPolicy.pidl

"PortableServer\IdUniquenessPolicyC.h" "PortableServer\IdUniquenessPolicyS.h" "PortableServer\IdUniquenessPolicyA.h" "PortableServer\IdUniquenessPolicyC.cpp" "PortableServer\IdUniquenessPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PortableServer_IdUniquenessPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\ImplicitActivationPolicy.pidl"

InputPath=PortableServer\ImplicitActivationPolicy.pidl

"PortableServer\ImplicitActivationPolicyC.h" "PortableServer\ImplicitActivationPolicyS.h" "PortableServer\ImplicitActivationPolicyA.h" "PortableServer\ImplicitActivationPolicyC.cpp" "PortableServer\ImplicitActivationPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PortableServer_ImplicitActivationPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\LifespanPolicy.pidl"

InputPath=PortableServer\LifespanPolicy.pidl

"PortableServer\LifespanPolicyC.h" "PortableServer\LifespanPolicyS.h" "PortableServer\LifespanPolicyA.h" "PortableServer\LifespanPolicyC.cpp" "PortableServer\LifespanPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PortableServer_LifespanPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\RequestProcessingPolicy.pidl"

InputPath=PortableServer\RequestProcessingPolicy.pidl

"PortableServer\RequestProcessingPolicyC.h" "PortableServer\RequestProcessingPolicyS.h" "PortableServer\RequestProcessingPolicyA.h" "PortableServer\RequestProcessingPolicyC.cpp" "PortableServer\RequestProcessingPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PortableServer_RequestProcessingPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\ServantActivator.pidl"

InputPath=PortableServer\ServantActivator.pidl

"PortableServer\ServantActivatorC.h" "PortableServer\ServantActivatorS.h" "PortableServer\ServantActivatorA.h" "PortableServer\ServantActivatorC.cpp" "PortableServer\ServantActivatorA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PortableServer_ServantActivator_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\ServantLocator.pidl"

InputPath=PortableServer\ServantLocator.pidl

"PortableServer\ServantLocatorC.h" "PortableServer\ServantLocatorS.h" "PortableServer\ServantLocatorA.h" "PortableServer\ServantLocatorC.cpp" "PortableServer\ServantLocatorA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PortableServer_ServantLocator_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\ServantManager.pidl"

InputPath=PortableServer\ServantManager.pidl

"PortableServer\ServantManagerC.h" "PortableServer\ServantManagerS.h" "PortableServer\ServantManagerA.h" "PortableServer\ServantManagerC.cpp" "PortableServer\ServantManagerA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PortableServer_ServantManager_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\ServantRetentionPolicy.pidl"

InputPath=PortableServer\ServantRetentionPolicy.pidl

"PortableServer\ServantRetentionPolicyC.h" "PortableServer\ServantRetentionPolicyS.h" "PortableServer\ServantRetentionPolicyA.h" "PortableServer\ServantRetentionPolicyC.cpp" "PortableServer\ServantRetentionPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PortableServer_ServantRetentionPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\ThreadPolicy.pidl"

InputPath=PortableServer\ThreadPolicy.pidl

"PortableServer\ThreadPolicyC.h" "PortableServer\ThreadPolicyS.h" "PortableServer\ThreadPolicyA.h" "PortableServer\ThreadPolicyC.cpp" "PortableServer\ThreadPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PortableServer_ThreadPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\POAManager.pidl"

InputPath=PortableServer\POAManager.pidl

"PortableServer\POAManagerC.h" "PortableServer\POAManagerS.h" "PortableServer\POAManagerC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PortableServer_POAManager_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\POAManagerFactory.pidl"

InputPath=PortableServer\POAManagerFactory.pidl

"PortableServer\POAManagerFactoryC.h" "PortableServer\POAManagerFactoryS.h" "PortableServer\POAManagerFactoryC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PortableServer_POAManagerFactory_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\POA.pidl"

InputPath=PortableServer\POA.pidl

"PortableServer\POAC.h" "PortableServer\POAS.h" "PortableServer\POAC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PortableServer_POA_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\PS_Current.pidl"

InputPath=PortableServer\PS_Current.pidl

"PortableServer\PS_CurrentC.h" "PortableServer\PS_CurrentS.h" "PortableServer\PS_CurrentC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PortableServer_PS_Current_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\PortableServer_include.pidl"

InputPath=PortableServer\PortableServer_include.pidl

"PortableServer\PortableServer_includeC.h" "PortableServer\PortableServer_includeS.h" "PortableServer\PortableServer_includeA.h" "PortableServer\PortableServer_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PortableServer_PortableServer_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -St -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Sorb -Sa -GX -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -Wb,unique_include=tao/PortableServer/PortableServer.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\PortableServer.pidl"

InputPath=PortableServer\PortableServer.pidl

"PortableServer\PortableServerC.h" "PortableServer\PortableServerS.h" "PortableServer\PortableServerA.h" "PortableServer\PortableServerC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PortableServer_PortableServer_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -St -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Sorb -Sa -GX -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -Wb,include_guard=TAO_PORTABLESERVER_SAFE_INCLUDE -Wb,safe_include=tao/PortableServer/PortableServer.h -o PortableServer "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="PortableServer\ForwardRequest.pidl"

InputPath=PortableServer\ForwardRequest.pidl

"PortableServer\ForwardRequestC.h" "PortableServer\ForwardRequestS.h" "PortableServer\ForwardRequestA.h" "PortableServer\ForwardRequestC.cpp" "PortableServer\ForwardRequestA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PortableServer_ForwardRequest_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\AdapterActivator.pidl"

InputPath=PortableServer\AdapterActivator.pidl

"PortableServer\AdapterActivatorC.h" "PortableServer\AdapterActivatorS.h" "PortableServer\AdapterActivatorA.h" "PortableServer\AdapterActivatorC.cpp" "PortableServer\AdapterActivatorA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PortableServer_AdapterActivator_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\IdAssignmentPolicy.pidl"

InputPath=PortableServer\IdAssignmentPolicy.pidl

"PortableServer\IdAssignmentPolicyC.h" "PortableServer\IdAssignmentPolicyS.h" "PortableServer\IdAssignmentPolicyA.h" "PortableServer\IdAssignmentPolicyC.cpp" "PortableServer\IdAssignmentPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PortableServer_IdAssignmentPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\IdUniquenessPolicy.pidl"

InputPath=PortableServer\IdUniquenessPolicy.pidl

"PortableServer\IdUniquenessPolicyC.h" "PortableServer\IdUniquenessPolicyS.h" "PortableServer\IdUniquenessPolicyA.h" "PortableServer\IdUniquenessPolicyC.cpp" "PortableServer\IdUniquenessPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PortableServer_IdUniquenessPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\ImplicitActivationPolicy.pidl"

InputPath=PortableServer\ImplicitActivationPolicy.pidl

"PortableServer\ImplicitActivationPolicyC.h" "PortableServer\ImplicitActivationPolicyS.h" "PortableServer\ImplicitActivationPolicyA.h" "PortableServer\ImplicitActivationPolicyC.cpp" "PortableServer\ImplicitActivationPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PortableServer_ImplicitActivationPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\LifespanPolicy.pidl"

InputPath=PortableServer\LifespanPolicy.pidl

"PortableServer\LifespanPolicyC.h" "PortableServer\LifespanPolicyS.h" "PortableServer\LifespanPolicyA.h" "PortableServer\LifespanPolicyC.cpp" "PortableServer\LifespanPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PortableServer_LifespanPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\RequestProcessingPolicy.pidl"

InputPath=PortableServer\RequestProcessingPolicy.pidl

"PortableServer\RequestProcessingPolicyC.h" "PortableServer\RequestProcessingPolicyS.h" "PortableServer\RequestProcessingPolicyA.h" "PortableServer\RequestProcessingPolicyC.cpp" "PortableServer\RequestProcessingPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PortableServer_RequestProcessingPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\ServantActivator.pidl"

InputPath=PortableServer\ServantActivator.pidl

"PortableServer\ServantActivatorC.h" "PortableServer\ServantActivatorS.h" "PortableServer\ServantActivatorA.h" "PortableServer\ServantActivatorC.cpp" "PortableServer\ServantActivatorA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PortableServer_ServantActivator_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\ServantLocator.pidl"

InputPath=PortableServer\ServantLocator.pidl

"PortableServer\ServantLocatorC.h" "PortableServer\ServantLocatorS.h" "PortableServer\ServantLocatorA.h" "PortableServer\ServantLocatorC.cpp" "PortableServer\ServantLocatorA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PortableServer_ServantLocator_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\ServantManager.pidl"

InputPath=PortableServer\ServantManager.pidl

"PortableServer\ServantManagerC.h" "PortableServer\ServantManagerS.h" "PortableServer\ServantManagerA.h" "PortableServer\ServantManagerC.cpp" "PortableServer\ServantManagerA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PortableServer_ServantManager_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\ServantRetentionPolicy.pidl"

InputPath=PortableServer\ServantRetentionPolicy.pidl

"PortableServer\ServantRetentionPolicyC.h" "PortableServer\ServantRetentionPolicyS.h" "PortableServer\ServantRetentionPolicyA.h" "PortableServer\ServantRetentionPolicyC.cpp" "PortableServer\ServantRetentionPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PortableServer_ServantRetentionPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\ThreadPolicy.pidl"

InputPath=PortableServer\ThreadPolicy.pidl

"PortableServer\ThreadPolicyC.h" "PortableServer\ThreadPolicyS.h" "PortableServer\ThreadPolicyA.h" "PortableServer\ThreadPolicyC.cpp" "PortableServer\ThreadPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PortableServer_ThreadPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\POAManager.pidl"

InputPath=PortableServer\POAManager.pidl

"PortableServer\POAManagerC.h" "PortableServer\POAManagerS.h" "PortableServer\POAManagerC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PortableServer_POAManager_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\POAManagerFactory.pidl"

InputPath=PortableServer\POAManagerFactory.pidl

"PortableServer\POAManagerFactoryC.h" "PortableServer\POAManagerFactoryS.h" "PortableServer\POAManagerFactoryC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PortableServer_POAManagerFactory_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\POA.pidl"

InputPath=PortableServer\POA.pidl

"PortableServer\POAC.h" "PortableServer\POAS.h" "PortableServer\POAC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PortableServer_POA_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\PS_Current.pidl"

InputPath=PortableServer\PS_Current.pidl

"PortableServer\PS_CurrentC.h" "PortableServer\PS_CurrentS.h" "PortableServer\PS_CurrentC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PortableServer_PS_Current_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\PortableServer_include.pidl"

InputPath=PortableServer\PortableServer_include.pidl

"PortableServer\PortableServer_includeC.h" "PortableServer\PortableServer_includeS.h" "PortableServer\PortableServer_includeA.h" "PortableServer\PortableServer_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PortableServer_PortableServer_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -St -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Sorb -Sa -GX -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -Wb,unique_include=tao/PortableServer/PortableServer.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\PortableServer.pidl"

InputPath=PortableServer\PortableServer.pidl

"PortableServer\PortableServerC.h" "PortableServer\PortableServerS.h" "PortableServer\PortableServerA.h" "PortableServer\PortableServerC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PortableServer_PortableServer_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -St -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Sorb -Sa -GX -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -Wb,include_guard=TAO_PORTABLESERVER_SAFE_INCLUDE -Wb,safe_include=tao/PortableServer/PortableServer.h -o PortableServer "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="PortableServer\ForwardRequest.pidl"

InputPath=PortableServer\ForwardRequest.pidl

"PortableServer\ForwardRequestC.h" "PortableServer\ForwardRequestS.h" "PortableServer\ForwardRequestA.h" "PortableServer\ForwardRequestC.cpp" "PortableServer\ForwardRequestA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PortableServer_ForwardRequest_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\AdapterActivator.pidl"

InputPath=PortableServer\AdapterActivator.pidl

"PortableServer\AdapterActivatorC.h" "PortableServer\AdapterActivatorS.h" "PortableServer\AdapterActivatorA.h" "PortableServer\AdapterActivatorC.cpp" "PortableServer\AdapterActivatorA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PortableServer_AdapterActivator_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\IdAssignmentPolicy.pidl"

InputPath=PortableServer\IdAssignmentPolicy.pidl

"PortableServer\IdAssignmentPolicyC.h" "PortableServer\IdAssignmentPolicyS.h" "PortableServer\IdAssignmentPolicyA.h" "PortableServer\IdAssignmentPolicyC.cpp" "PortableServer\IdAssignmentPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PortableServer_IdAssignmentPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\IdUniquenessPolicy.pidl"

InputPath=PortableServer\IdUniquenessPolicy.pidl

"PortableServer\IdUniquenessPolicyC.h" "PortableServer\IdUniquenessPolicyS.h" "PortableServer\IdUniquenessPolicyA.h" "PortableServer\IdUniquenessPolicyC.cpp" "PortableServer\IdUniquenessPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PortableServer_IdUniquenessPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\ImplicitActivationPolicy.pidl"

InputPath=PortableServer\ImplicitActivationPolicy.pidl

"PortableServer\ImplicitActivationPolicyC.h" "PortableServer\ImplicitActivationPolicyS.h" "PortableServer\ImplicitActivationPolicyA.h" "PortableServer\ImplicitActivationPolicyC.cpp" "PortableServer\ImplicitActivationPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PortableServer_ImplicitActivationPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\LifespanPolicy.pidl"

InputPath=PortableServer\LifespanPolicy.pidl

"PortableServer\LifespanPolicyC.h" "PortableServer\LifespanPolicyS.h" "PortableServer\LifespanPolicyA.h" "PortableServer\LifespanPolicyC.cpp" "PortableServer\LifespanPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PortableServer_LifespanPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\RequestProcessingPolicy.pidl"

InputPath=PortableServer\RequestProcessingPolicy.pidl

"PortableServer\RequestProcessingPolicyC.h" "PortableServer\RequestProcessingPolicyS.h" "PortableServer\RequestProcessingPolicyA.h" "PortableServer\RequestProcessingPolicyC.cpp" "PortableServer\RequestProcessingPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PortableServer_RequestProcessingPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\ServantActivator.pidl"

InputPath=PortableServer\ServantActivator.pidl

"PortableServer\ServantActivatorC.h" "PortableServer\ServantActivatorS.h" "PortableServer\ServantActivatorA.h" "PortableServer\ServantActivatorC.cpp" "PortableServer\ServantActivatorA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PortableServer_ServantActivator_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\ServantLocator.pidl"

InputPath=PortableServer\ServantLocator.pidl

"PortableServer\ServantLocatorC.h" "PortableServer\ServantLocatorS.h" "PortableServer\ServantLocatorA.h" "PortableServer\ServantLocatorC.cpp" "PortableServer\ServantLocatorA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PortableServer_ServantLocator_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\ServantManager.pidl"

InputPath=PortableServer\ServantManager.pidl

"PortableServer\ServantManagerC.h" "PortableServer\ServantManagerS.h" "PortableServer\ServantManagerA.h" "PortableServer\ServantManagerC.cpp" "PortableServer\ServantManagerA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PortableServer_ServantManager_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\ServantRetentionPolicy.pidl"

InputPath=PortableServer\ServantRetentionPolicy.pidl

"PortableServer\ServantRetentionPolicyC.h" "PortableServer\ServantRetentionPolicyS.h" "PortableServer\ServantRetentionPolicyA.h" "PortableServer\ServantRetentionPolicyC.cpp" "PortableServer\ServantRetentionPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PortableServer_ServantRetentionPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\ThreadPolicy.pidl"

InputPath=PortableServer\ThreadPolicy.pidl

"PortableServer\ThreadPolicyC.h" "PortableServer\ThreadPolicyS.h" "PortableServer\ThreadPolicyA.h" "PortableServer\ThreadPolicyC.cpp" "PortableServer\ThreadPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PortableServer_ThreadPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\POAManager.pidl"

InputPath=PortableServer\POAManager.pidl

"PortableServer\POAManagerC.h" "PortableServer\POAManagerS.h" "PortableServer\POAManagerC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PortableServer_POAManager_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\POAManagerFactory.pidl"

InputPath=PortableServer\POAManagerFactory.pidl

"PortableServer\POAManagerFactoryC.h" "PortableServer\POAManagerFactoryS.h" "PortableServer\POAManagerFactoryC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PortableServer_POAManagerFactory_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\POA.pidl"

InputPath=PortableServer\POA.pidl

"PortableServer\POAC.h" "PortableServer\POAS.h" "PortableServer\POAC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PortableServer_POA_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\PS_Current.pidl"

InputPath=PortableServer\PS_Current.pidl

"PortableServer\PS_CurrentC.h" "PortableServer\PS_CurrentS.h" "PortableServer\PS_CurrentC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PortableServer_PS_Current_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\PortableServer_include.pidl"

InputPath=PortableServer\PortableServer_include.pidl

"PortableServer\PortableServer_includeC.h" "PortableServer\PortableServer_includeS.h" "PortableServer\PortableServer_includeA.h" "PortableServer\PortableServer_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PortableServer_PortableServer_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -St -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Sorb -Sa -GX -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -Wb,unique_include=tao/PortableServer/PortableServer.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\PortableServer.pidl"

InputPath=PortableServer\PortableServer.pidl

"PortableServer\PortableServerC.h" "PortableServer\PortableServerS.h" "PortableServer\PortableServerA.h" "PortableServer\PortableServerC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PortableServer_PortableServer_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -St -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Sorb -Sa -GX -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -Wb,include_guard=TAO_PORTABLESERVER_SAFE_INCLUDE -Wb,safe_include=tao/PortableServer/PortableServer.h -o PortableServer "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="PortableServer\ForwardRequest.pidl"

InputPath=PortableServer\ForwardRequest.pidl

"PortableServer\ForwardRequestC.h" "PortableServer\ForwardRequestS.h" "PortableServer\ForwardRequestA.h" "PortableServer\ForwardRequestC.cpp" "PortableServer\ForwardRequestA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PortableServer_ForwardRequest_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\AdapterActivator.pidl"

InputPath=PortableServer\AdapterActivator.pidl

"PortableServer\AdapterActivatorC.h" "PortableServer\AdapterActivatorS.h" "PortableServer\AdapterActivatorA.h" "PortableServer\AdapterActivatorC.cpp" "PortableServer\AdapterActivatorA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PortableServer_AdapterActivator_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\IdAssignmentPolicy.pidl"

InputPath=PortableServer\IdAssignmentPolicy.pidl

"PortableServer\IdAssignmentPolicyC.h" "PortableServer\IdAssignmentPolicyS.h" "PortableServer\IdAssignmentPolicyA.h" "PortableServer\IdAssignmentPolicyC.cpp" "PortableServer\IdAssignmentPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PortableServer_IdAssignmentPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\IdUniquenessPolicy.pidl"

InputPath=PortableServer\IdUniquenessPolicy.pidl

"PortableServer\IdUniquenessPolicyC.h" "PortableServer\IdUniquenessPolicyS.h" "PortableServer\IdUniquenessPolicyA.h" "PortableServer\IdUniquenessPolicyC.cpp" "PortableServer\IdUniquenessPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PortableServer_IdUniquenessPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\ImplicitActivationPolicy.pidl"

InputPath=PortableServer\ImplicitActivationPolicy.pidl

"PortableServer\ImplicitActivationPolicyC.h" "PortableServer\ImplicitActivationPolicyS.h" "PortableServer\ImplicitActivationPolicyA.h" "PortableServer\ImplicitActivationPolicyC.cpp" "PortableServer\ImplicitActivationPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PortableServer_ImplicitActivationPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\LifespanPolicy.pidl"

InputPath=PortableServer\LifespanPolicy.pidl

"PortableServer\LifespanPolicyC.h" "PortableServer\LifespanPolicyS.h" "PortableServer\LifespanPolicyA.h" "PortableServer\LifespanPolicyC.cpp" "PortableServer\LifespanPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PortableServer_LifespanPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\RequestProcessingPolicy.pidl"

InputPath=PortableServer\RequestProcessingPolicy.pidl

"PortableServer\RequestProcessingPolicyC.h" "PortableServer\RequestProcessingPolicyS.h" "PortableServer\RequestProcessingPolicyA.h" "PortableServer\RequestProcessingPolicyC.cpp" "PortableServer\RequestProcessingPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PortableServer_RequestProcessingPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\ServantActivator.pidl"

InputPath=PortableServer\ServantActivator.pidl

"PortableServer\ServantActivatorC.h" "PortableServer\ServantActivatorS.h" "PortableServer\ServantActivatorA.h" "PortableServer\ServantActivatorC.cpp" "PortableServer\ServantActivatorA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PortableServer_ServantActivator_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\ServantLocator.pidl"

InputPath=PortableServer\ServantLocator.pidl

"PortableServer\ServantLocatorC.h" "PortableServer\ServantLocatorS.h" "PortableServer\ServantLocatorA.h" "PortableServer\ServantLocatorC.cpp" "PortableServer\ServantLocatorA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PortableServer_ServantLocator_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\ServantManager.pidl"

InputPath=PortableServer\ServantManager.pidl

"PortableServer\ServantManagerC.h" "PortableServer\ServantManagerS.h" "PortableServer\ServantManagerA.h" "PortableServer\ServantManagerC.cpp" "PortableServer\ServantManagerA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PortableServer_ServantManager_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\ServantRetentionPolicy.pidl"

InputPath=PortableServer\ServantRetentionPolicy.pidl

"PortableServer\ServantRetentionPolicyC.h" "PortableServer\ServantRetentionPolicyS.h" "PortableServer\ServantRetentionPolicyA.h" "PortableServer\ServantRetentionPolicyC.cpp" "PortableServer\ServantRetentionPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PortableServer_ServantRetentionPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\ThreadPolicy.pidl"

InputPath=PortableServer\ThreadPolicy.pidl

"PortableServer\ThreadPolicyC.h" "PortableServer\ThreadPolicyS.h" "PortableServer\ThreadPolicyA.h" "PortableServer\ThreadPolicyC.cpp" "PortableServer\ThreadPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PortableServer_ThreadPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -GA -Sal -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\POAManager.pidl"

InputPath=PortableServer\POAManager.pidl

"PortableServer\POAManagerC.h" "PortableServer\POAManagerS.h" "PortableServer\POAManagerC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PortableServer_POAManager_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\POAManagerFactory.pidl"

InputPath=PortableServer\POAManagerFactory.pidl

"PortableServer\POAManagerFactoryC.h" "PortableServer\POAManagerFactoryS.h" "PortableServer\POAManagerFactoryC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PortableServer_POAManagerFactory_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\POA.pidl"

InputPath=PortableServer\POA.pidl

"PortableServer\POAC.h" "PortableServer\POAS.h" "PortableServer\POAC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PortableServer_POA_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\PS_Current.pidl"

InputPath=PortableServer\PS_Current.pidl

"PortableServer\PS_CurrentC.h" "PortableServer\PS_CurrentS.h" "PortableServer\PS_CurrentC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PortableServer_PS_Current_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\PortableServer_include.pidl"

InputPath=PortableServer\PortableServer_include.pidl

"PortableServer\PortableServer_includeC.h" "PortableServer\PortableServer_includeS.h" "PortableServer\PortableServer_includeA.h" "PortableServer\PortableServer_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PortableServer_PortableServer_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -St -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Sorb -Sa -GX -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -Wb,unique_include=tao/PortableServer/PortableServer.h -o PortableServer "$(InputPath)"
<<

SOURCE="PortableServer\PortableServer.pidl"

InputPath=PortableServer\PortableServer.pidl

"PortableServer\PortableServerC.h" "PortableServer\PortableServerS.h" "PortableServer\PortableServerA.h" "PortableServer\PortableServerC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PortableServer_PortableServer_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -St -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Sorb -Sa -GX -Wb,export_macro=TAO_PortableServer_Export -Wb,export_include=tao/PortableServer/portableserver_export.h -Wb,include_guard=TAO_PORTABLESERVER_SAFE_INCLUDE -Wb,safe_include=tao/PortableServer/PortableServer.h -o PortableServer "$(InputPath)"
<<

!ENDIF

SOURCE="PortableServer\TAO_PortableServer.rc"

"$(INTDIR)\PortableServer\TAO_PortableServer.res" : $(SOURCE)
	@if not exist "$(INTDIR)\PortableServer\$(NULL)" mkdir "$(INTDIR)\PortableServer\"
	$(RSC) /l 0x409 /fo"$(INTDIR)\PortableServer\TAO_PortableServer.res" /d NDEBUG /d WIN64 /d _CRT_SECURE_NO_WARNINGS /d _CRT_SECURE_NO_DEPRECATE /d _CRT_NONSTDC_NO_DEPRECATE /d TAO_HAS_VALUETYPE_OUT_INDIRECTION /i "..\.." /i ".." $(SOURCE)



!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.PortableServer.dep")
	@echo Using "Makefile.PortableServer.dep"
!ELSE
	@echo Warning: cannot find "Makefile.PortableServer.dep"
!ENDIF
!ENDIF

