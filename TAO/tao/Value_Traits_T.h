#ifndef guard_value_traits_hpp
#define guard_value_traits_hpp
/**
 * @file
 *
 * @brief Implement the element manipulation traits for types with
 * value-like semantics.
 *
 * $Id: Value_Traits_T.h 1804 2011-02-16 21:19:17Z mesnierp $
 *
 * @author Carlos O'Ryan
 */

#include "ace/OS_NS_string.h"

#include <algorithm>

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

namespace TAO
{
namespace details
{

template<typename T, bool dummy>
struct value_traits
{
  typedef T value_type;
  typedef T const const_value_type;

  inline static void zero_range(
      value_type * begin , value_type * end)
  {
    ACE_OS::memset (begin, 0, (end - begin) * sizeof (value_type));
  }

  inline static void initialize_range(
      value_type * begin, value_type * end)
  {
    std::fill(begin, end, value_type ());
  }

  inline static void release_range(
      value_type *, value_type *)
  {
    // Noop for value sequences
  }

# ifndef ACE_LACKS_MEMBER_TEMPLATES
  // Allow MSVC++ >= 8 checked iterators to be used.
  template <typename iter>
  inline static void copy_range(
      value_type * begin, value_type * end, iter dst)
  {
    std::copy(begin, end, dst);
  }
# else
  inline static void copy_range(
      value_type * begin, value_type * end, value_type * dst)
  {
    std::copy(begin, end, dst);
  }
# endif  /* !ACE_LACKS_MEMBER_TEMPLATES */

#ifndef ACE_LACKS_MEMBER_TEMPLATES
  // Allow MSVC++ >= 8 checked iterators to be used.
  template <typename iter>
  inline static bool copy_swap_range(
      value_type * begin, value_type * end, iter dst)
  {
    copy_range(begin, end, dst);
    return false;
  }
#else
  inline static bool copy_swap_range(
      value_type * begin, value_type * end, value_type * dst)
  {
    copy_range(begin, end, dst);
    return false;
  }
#endif  /* !ACE_LACKS_MEMBER_TEMPLATES */
};

} // namespace details
} // namespace CORBA

TAO_END_VERSIONED_NAMESPACE_DECL

#endif // guard_value_traits_hpp
