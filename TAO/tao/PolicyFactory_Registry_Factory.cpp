//$Id: PolicyFactory_Registry_Factory.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/PolicyFactory_Registry_Factory.h"

ACE_RCSID (tao,
           PolicyFactory_Registry_Factory,
           "$Id: PolicyFactory_Registry_Factory.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_PolicyFactory_Registry_Factory::~TAO_PolicyFactory_Registry_Factory (void)
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
