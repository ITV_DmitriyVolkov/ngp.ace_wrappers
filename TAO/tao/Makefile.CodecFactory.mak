# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.CodecFactory.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "CodecFactory\IOP_CodecC.h" "CodecFactory\IOP_CodecS.h" "CodecFactory\IOP_CodecA.h" "CodecFactory\IOP_CodecC.cpp" "CodecFactory\IOP_Codec_includeC.h" "CodecFactory\IOP_Codec_includeS.h" "CodecFactory\IOP_Codec_includeA.h" "CodecFactory\IOP_Codec_includeC.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\CodecFactory\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_CodecFactoryd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_CODECFACTORY_BUILD_DLL -f "Makefile.CodecFactory.dep" "CodecFactory\IOP_CodecC.cpp" "CodecFactory\IOP_Codec_includeC.cpp" "CodecFactory\CDR_Encaps_Codec.cpp" "CodecFactory\CodecFactory_impl.cpp" "CodecFactory\CodecFactory.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_CodecFactoryd.pdb"
	-@del /f/q "..\..\lib\TAO_CodecFactoryd.dll"
	-@del /f/q "$(OUTDIR)\TAO_CodecFactoryd.lib"
	-@del /f/q "$(OUTDIR)\TAO_CodecFactoryd.exp"
	-@del /f/q "$(OUTDIR)\TAO_CodecFactoryd.ilk"
	-@del /f/q "CodecFactory\IOP_CodecC.h"
	-@del /f/q "CodecFactory\IOP_CodecS.h"
	-@del /f/q "CodecFactory\IOP_CodecA.h"
	-@del /f/q "CodecFactory\IOP_CodecC.cpp"
	-@del /f/q "CodecFactory\IOP_Codec_includeC.h"
	-@del /f/q "CodecFactory\IOP_Codec_includeS.h"
	-@del /f/q "CodecFactory\IOP_Codec_includeA.h"
	-@del /f/q "CodecFactory\IOP_Codec_includeC.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\CodecFactory\$(NULL)" mkdir "Debug\CodecFactory"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_CODECFACTORY_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\TAO_CodecFactoryd.pdb" /machine:IA64 /out:"..\..\lib\TAO_CodecFactoryd.dll" /implib:"$(OUTDIR)\TAO_CodecFactoryd.lib"
LINK32_OBJS= \
	"$(INTDIR)\CodecFactory\TAO_CodecFactory.res" \
	"$(INTDIR)\CodecFactory\IOP_CodecC.obj" \
	"$(INTDIR)\CodecFactory\IOP_Codec_includeC.obj" \
	"$(INTDIR)\CodecFactory\CDR_Encaps_Codec.obj" \
	"$(INTDIR)\CodecFactory\CodecFactory_impl.obj" \
	"$(INTDIR)\CodecFactory\CodecFactory.obj"

"..\..\lib\TAO_CodecFactoryd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_CodecFactoryd.dll.manifest" mt.exe -manifest "..\..\lib\TAO_CodecFactoryd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\CodecFactory\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_CodecFactory.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_CODECFACTORY_BUILD_DLL -f "Makefile.CodecFactory.dep" "CodecFactory\IOP_CodecC.cpp" "CodecFactory\IOP_Codec_includeC.cpp" "CodecFactory\CDR_Encaps_Codec.cpp" "CodecFactory\CodecFactory_impl.cpp" "CodecFactory\CodecFactory.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\TAO_CodecFactory.dll"
	-@del /f/q "$(OUTDIR)\TAO_CodecFactory.lib"
	-@del /f/q "$(OUTDIR)\TAO_CodecFactory.exp"
	-@del /f/q "$(OUTDIR)\TAO_CodecFactory.ilk"
	-@del /f/q "CodecFactory\IOP_CodecC.h"
	-@del /f/q "CodecFactory\IOP_CodecS.h"
	-@del /f/q "CodecFactory\IOP_CodecA.h"
	-@del /f/q "CodecFactory\IOP_CodecC.cpp"
	-@del /f/q "CodecFactory\IOP_Codec_includeC.h"
	-@del /f/q "CodecFactory\IOP_Codec_includeS.h"
	-@del /f/q "CodecFactory\IOP_Codec_includeA.h"
	-@del /f/q "CodecFactory\IOP_Codec_includeC.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\CodecFactory\$(NULL)" mkdir "Release\CodecFactory"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_CODECFACTORY_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\TAO_CodecFactory.dll" /implib:"$(OUTDIR)\TAO_CodecFactory.lib"
LINK32_OBJS= \
	"$(INTDIR)\CodecFactory\TAO_CodecFactory.res" \
	"$(INTDIR)\CodecFactory\IOP_CodecC.obj" \
	"$(INTDIR)\CodecFactory\IOP_Codec_includeC.obj" \
	"$(INTDIR)\CodecFactory\CDR_Encaps_Codec.obj" \
	"$(INTDIR)\CodecFactory\CodecFactory_impl.obj" \
	"$(INTDIR)\CodecFactory\CodecFactory.obj"

"..\..\lib\TAO_CodecFactory.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_CodecFactory.dll.manifest" mt.exe -manifest "..\..\lib\TAO_CodecFactory.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\CodecFactory\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_CodecFactorysd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CodecFactory.dep" "CodecFactory\IOP_CodecC.cpp" "CodecFactory\IOP_Codec_includeC.cpp" "CodecFactory\CDR_Encaps_Codec.cpp" "CodecFactory\CodecFactory_impl.cpp" "CodecFactory\CodecFactory.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_CodecFactorysd.lib"
	-@del /f/q "$(OUTDIR)\TAO_CodecFactorysd.exp"
	-@del /f/q "$(OUTDIR)\TAO_CodecFactorysd.ilk"
	-@del /f/q "..\..\lib\TAO_CodecFactorysd.pdb"
	-@del /f/q "CodecFactory\IOP_CodecC.h"
	-@del /f/q "CodecFactory\IOP_CodecS.h"
	-@del /f/q "CodecFactory\IOP_CodecA.h"
	-@del /f/q "CodecFactory\IOP_CodecC.cpp"
	-@del /f/q "CodecFactory\IOP_Codec_includeC.h"
	-@del /f/q "CodecFactory\IOP_Codec_includeS.h"
	-@del /f/q "CodecFactory\IOP_Codec_includeA.h"
	-@del /f/q "CodecFactory\IOP_Codec_includeC.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\CodecFactory\$(NULL)" mkdir "Static_Debug\CodecFactory"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"..\..\lib\TAO_CodecFactorysd.pdb" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_CodecFactorysd.lib"
LINK32_OBJS= \
	"$(INTDIR)\CodecFactory\IOP_CodecC.obj" \
	"$(INTDIR)\CodecFactory\IOP_Codec_includeC.obj" \
	"$(INTDIR)\CodecFactory\CDR_Encaps_Codec.obj" \
	"$(INTDIR)\CodecFactory\CodecFactory_impl.obj" \
	"$(INTDIR)\CodecFactory\CodecFactory.obj"

"$(OUTDIR)\TAO_CodecFactorysd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_CodecFactorysd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_CodecFactorysd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\CodecFactory\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_CodecFactorys.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CodecFactory.dep" "CodecFactory\IOP_CodecC.cpp" "CodecFactory\IOP_Codec_includeC.cpp" "CodecFactory\CDR_Encaps_Codec.cpp" "CodecFactory\CodecFactory_impl.cpp" "CodecFactory\CodecFactory.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_CodecFactorys.lib"
	-@del /f/q "$(OUTDIR)\TAO_CodecFactorys.exp"
	-@del /f/q "$(OUTDIR)\TAO_CodecFactorys.ilk"
	-@del /f/q "CodecFactory\IOP_CodecC.h"
	-@del /f/q "CodecFactory\IOP_CodecS.h"
	-@del /f/q "CodecFactory\IOP_CodecA.h"
	-@del /f/q "CodecFactory\IOP_CodecC.cpp"
	-@del /f/q "CodecFactory\IOP_Codec_includeC.h"
	-@del /f/q "CodecFactory\IOP_Codec_includeS.h"
	-@del /f/q "CodecFactory\IOP_Codec_includeA.h"
	-@del /f/q "CodecFactory\IOP_Codec_includeC.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\CodecFactory\$(NULL)" mkdir "Static_Release\CodecFactory"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_CodecFactorys.lib"
LINK32_OBJS= \
	"$(INTDIR)\CodecFactory\IOP_CodecC.obj" \
	"$(INTDIR)\CodecFactory\IOP_Codec_includeC.obj" \
	"$(INTDIR)\CodecFactory\CDR_Encaps_Codec.obj" \
	"$(INTDIR)\CodecFactory\CodecFactory_impl.obj" \
	"$(INTDIR)\CodecFactory\CodecFactory.obj"

"$(OUTDIR)\TAO_CodecFactorys.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_CodecFactorys.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_CodecFactorys.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CodecFactory.dep")
!INCLUDE "Makefile.CodecFactory.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="CodecFactory\IOP_CodecC.cpp"

"$(INTDIR)\CodecFactory\IOP_CodecC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CodecFactory\$(NULL)" mkdir "$(INTDIR)\CodecFactory\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CodecFactory\IOP_CodecC.obj" $(SOURCE)

SOURCE="CodecFactory\IOP_Codec_includeC.cpp"

"$(INTDIR)\CodecFactory\IOP_Codec_includeC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CodecFactory\$(NULL)" mkdir "$(INTDIR)\CodecFactory\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CodecFactory\IOP_Codec_includeC.obj" $(SOURCE)

SOURCE="CodecFactory\CDR_Encaps_Codec.cpp"

"$(INTDIR)\CodecFactory\CDR_Encaps_Codec.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CodecFactory\$(NULL)" mkdir "$(INTDIR)\CodecFactory\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CodecFactory\CDR_Encaps_Codec.obj" $(SOURCE)

SOURCE="CodecFactory\CodecFactory_impl.cpp"

"$(INTDIR)\CodecFactory\CodecFactory_impl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CodecFactory\$(NULL)" mkdir "$(INTDIR)\CodecFactory\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CodecFactory\CodecFactory_impl.obj" $(SOURCE)

SOURCE="CodecFactory\CodecFactory.cpp"

"$(INTDIR)\CodecFactory\CodecFactory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\CodecFactory\$(NULL)" mkdir "$(INTDIR)\CodecFactory\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CodecFactory\CodecFactory.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="CodecFactory\IOP_Codec.pidl"

InputPath=CodecFactory\IOP_Codec.pidl

"CodecFactory\IOP_CodecC.h" "CodecFactory\IOP_CodecS.h" "CodecFactory\IOP_CodecA.h" "CodecFactory\IOP_CodecC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CodecFactory_IOP_Codec_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Sal -GX -Wb,export_macro=TAO_CodecFactory_Export -Wb,export_include=tao/CodecFactory/codecfactory_export.h -Wb,include_guard=TAO_CODECFACTORY_SAFE_INCLUDE -Wb,safe_include=tao/CodecFactory/CodecFactory.h -o CodecFactory "$(InputPath)"
<<

SOURCE="CodecFactory\IOP_Codec_include.pidl"

InputPath=CodecFactory\IOP_Codec_include.pidl

"CodecFactory\IOP_Codec_includeC.h" "CodecFactory\IOP_Codec_includeS.h" "CodecFactory\IOP_Codec_includeA.h" "CodecFactory\IOP_Codec_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CodecFactory_IOP_Codec_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -St -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Sorb -Sa -GX -Wb,export_macro=TAO_CodecFactory_Export -Wb,export_include=tao/CodecFactory/codecfactory_export.h -Wb,unique_include=tao/CodecFactory/CodecFactory.h -o CodecFactory "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="CodecFactory\IOP_Codec.pidl"

InputPath=CodecFactory\IOP_Codec.pidl

"CodecFactory\IOP_CodecC.h" "CodecFactory\IOP_CodecS.h" "CodecFactory\IOP_CodecA.h" "CodecFactory\IOP_CodecC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CodecFactory_IOP_Codec_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Sal -GX -Wb,export_macro=TAO_CodecFactory_Export -Wb,export_include=tao/CodecFactory/codecfactory_export.h -Wb,include_guard=TAO_CODECFACTORY_SAFE_INCLUDE -Wb,safe_include=tao/CodecFactory/CodecFactory.h -o CodecFactory "$(InputPath)"
<<

SOURCE="CodecFactory\IOP_Codec_include.pidl"

InputPath=CodecFactory\IOP_Codec_include.pidl

"CodecFactory\IOP_Codec_includeC.h" "CodecFactory\IOP_Codec_includeS.h" "CodecFactory\IOP_Codec_includeA.h" "CodecFactory\IOP_Codec_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CodecFactory_IOP_Codec_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -St -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Sorb -Sa -GX -Wb,export_macro=TAO_CodecFactory_Export -Wb,export_include=tao/CodecFactory/codecfactory_export.h -Wb,unique_include=tao/CodecFactory/CodecFactory.h -o CodecFactory "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="CodecFactory\IOP_Codec.pidl"

InputPath=CodecFactory\IOP_Codec.pidl

"CodecFactory\IOP_CodecC.h" "CodecFactory\IOP_CodecS.h" "CodecFactory\IOP_CodecA.h" "CodecFactory\IOP_CodecC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CodecFactory_IOP_Codec_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Sal -GX -Wb,export_macro=TAO_CodecFactory_Export -Wb,export_include=tao/CodecFactory/codecfactory_export.h -Wb,include_guard=TAO_CODECFACTORY_SAFE_INCLUDE -Wb,safe_include=tao/CodecFactory/CodecFactory.h -o CodecFactory "$(InputPath)"
<<

SOURCE="CodecFactory\IOP_Codec_include.pidl"

InputPath=CodecFactory\IOP_Codec_include.pidl

"CodecFactory\IOP_Codec_includeC.h" "CodecFactory\IOP_Codec_includeS.h" "CodecFactory\IOP_Codec_includeA.h" "CodecFactory\IOP_Codec_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CodecFactory_IOP_Codec_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -St -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Sorb -Sa -GX -Wb,export_macro=TAO_CodecFactory_Export -Wb,export_include=tao/CodecFactory/codecfactory_export.h -Wb,unique_include=tao/CodecFactory/CodecFactory.h -o CodecFactory "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="CodecFactory\IOP_Codec.pidl"

InputPath=CodecFactory\IOP_Codec.pidl

"CodecFactory\IOP_CodecC.h" "CodecFactory\IOP_CodecS.h" "CodecFactory\IOP_CodecA.h" "CodecFactory\IOP_CodecC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CodecFactory_IOP_Codec_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Sal -GX -Wb,export_macro=TAO_CodecFactory_Export -Wb,export_include=tao/CodecFactory/codecfactory_export.h -Wb,include_guard=TAO_CODECFACTORY_SAFE_INCLUDE -Wb,safe_include=tao/CodecFactory/CodecFactory.h -o CodecFactory "$(InputPath)"
<<

SOURCE="CodecFactory\IOP_Codec_include.pidl"

InputPath=CodecFactory\IOP_Codec_include.pidl

"CodecFactory\IOP_Codec_includeC.h" "CodecFactory\IOP_Codec_includeS.h" "CodecFactory\IOP_Codec_includeA.h" "CodecFactory\IOP_Codec_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CodecFactory_IOP_Codec_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -St -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Sorb -Sa -GX -Wb,export_macro=TAO_CodecFactory_Export -Wb,export_include=tao/CodecFactory/codecfactory_export.h -Wb,unique_include=tao/CodecFactory/CodecFactory.h -o CodecFactory "$(InputPath)"
<<

!ENDIF

SOURCE="CodecFactory\TAO_CodecFactory.rc"

"$(INTDIR)\CodecFactory\TAO_CodecFactory.res" : $(SOURCE)
	@if not exist "$(INTDIR)\CodecFactory\$(NULL)" mkdir "$(INTDIR)\CodecFactory\"
	$(RSC) /l 0x409 /fo"$(INTDIR)\CodecFactory\TAO_CodecFactory.res" /d NDEBUG /d WIN64 /d _CRT_SECURE_NO_WARNINGS /d _CRT_SECURE_NO_DEPRECATE /d _CRT_NONSTDC_NO_DEPRECATE /i "..\.." /i ".." $(SOURCE)



!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CodecFactory.dep")
	@echo Using "Makefile.CodecFactory.dep"
!ELSE
	@echo Warning: cannot find "Makefile.CodecFactory.dep"
!ENDIF
!ENDIF

