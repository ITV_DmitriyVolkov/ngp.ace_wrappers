# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.Strategies.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "Strategies\uiop_endpointsC.h" "Strategies\uiop_endpointsS.h" "Strategies\uiop_endpointsC.cpp" "Strategies\sciop_endpointsC.h" "Strategies\sciop_endpointsS.h" "Strategies\sciop_endpointsC.cpp" "Strategies\COIOP_EndpointsC.h" "Strategies\COIOP_EndpointsS.h" "Strategies\COIOP_EndpointsC.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\Strategies\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_Strategiesd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_STRATEGIES_BUILD_DLL -f "Makefile.Strategies.dep" "Strategies\uiop_endpointsC.cpp" "Strategies\sciop_endpointsC.cpp" "Strategies\COIOP_EndpointsC.cpp" "Strategies\SHMIOP_Acceptor.cpp" "Strategies\SHMIOP_Factory.cpp" "Strategies\LF_Strategy_Null.cpp" "Strategies\SCIOP_Transport.cpp" "Strategies\SHMIOP_Endpoint.cpp" "Strategies\DIOP_Connector.cpp" "Strategies\UIOP_Endpoint.cpp" "Strategies\SHMIOP_Transport.cpp" "Strategies\UIOP_Connector.cpp" "Strategies\COIOP_Endpoint.cpp" "Strategies\COIOP_Acceptor.cpp" "Strategies\SCIOP_Connector.cpp" "Strategies\DIOP_Endpoint.cpp" "Strategies\OC_Endpoint_Selector_Factory.cpp" "Strategies\UIOP_Acceptor.cpp" "Strategies\UIOP_Profile.cpp" "Strategies\SHMIOP_Connector.cpp" "Strategies\SCIOP_Factory.cpp" "Strategies\Optimized_Connection_Endpoint_Selector.cpp" "Strategies\NULL_Connection_Purging_Strategy.cpp" "Strategies\DIOP_Profile.cpp" "Strategies\SCIOP_Endpoint.cpp" "Strategies\COIOP_Transport.cpp" "Strategies\UIOP_Factory.cpp" "Strategies\DIOP_Acceptor.cpp" "Strategies\SHMIOP_Connection_Handler.cpp" "Strategies\LFU_Connection_Purging_Strategy.cpp" "Strategies\DIOP_Connection_Handler.cpp" "Strategies\UIOP_Connection_Handler.cpp" "Strategies\COIOP_Profile.cpp" "Strategies\Strategies_ORBInitializer.cpp" "Strategies\SCIOP_Acceptor.cpp" "Strategies\COIOP_Connection_Handler.cpp" "Strategies\SCIOP_Connection_Handler.cpp" "Strategies\DIOP_Factory.cpp" "Strategies\DIOP_Transport.cpp" "Strategies\SHMIOP_Profile.cpp" "Strategies\advanced_resource.cpp" "Strategies\UIOP_Transport.cpp" "Strategies\OC_Endpoint_Selector_Loader.cpp" "Strategies\COIOP_Connector.cpp" "Strategies\SCIOP_Profile.cpp" "Strategies\FIFO_Connection_Purging_Strategy.cpp" "Strategies\COIOP_Factory.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_Strategiesd.pdb"
	-@del /f/q "..\..\lib\TAO_Strategiesd.dll"
	-@del /f/q "$(OUTDIR)\TAO_Strategiesd.lib"
	-@del /f/q "$(OUTDIR)\TAO_Strategiesd.exp"
	-@del /f/q "$(OUTDIR)\TAO_Strategiesd.ilk"
	-@del /f/q "Strategies\uiop_endpointsC.h"
	-@del /f/q "Strategies\uiop_endpointsS.h"
	-@del /f/q "Strategies\uiop_endpointsC.cpp"
	-@del /f/q "Strategies\sciop_endpointsC.h"
	-@del /f/q "Strategies\sciop_endpointsS.h"
	-@del /f/q "Strategies\sciop_endpointsC.cpp"
	-@del /f/q "Strategies\COIOP_EndpointsC.h"
	-@del /f/q "Strategies\COIOP_EndpointsS.h"
	-@del /f/q "Strategies\COIOP_EndpointsC.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\Strategies\$(NULL)" mkdir "Debug\Strategies"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_STRATEGIES_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_CodecFactoryd.lib TAO_PId.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\TAO_Strategiesd.pdb" /machine:IA64 /out:"..\..\lib\TAO_Strategiesd.dll" /implib:"$(OUTDIR)\TAO_Strategiesd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Strategies\TAO_Strategies.res" \
	"$(INTDIR)\Strategies\uiop_endpointsC.obj" \
	"$(INTDIR)\Strategies\sciop_endpointsC.obj" \
	"$(INTDIR)\Strategies\COIOP_EndpointsC.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Acceptor.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Factory.obj" \
	"$(INTDIR)\Strategies\LF_Strategy_Null.obj" \
	"$(INTDIR)\Strategies\SCIOP_Transport.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Endpoint.obj" \
	"$(INTDIR)\Strategies\DIOP_Connector.obj" \
	"$(INTDIR)\Strategies\UIOP_Endpoint.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Transport.obj" \
	"$(INTDIR)\Strategies\UIOP_Connector.obj" \
	"$(INTDIR)\Strategies\COIOP_Endpoint.obj" \
	"$(INTDIR)\Strategies\COIOP_Acceptor.obj" \
	"$(INTDIR)\Strategies\SCIOP_Connector.obj" \
	"$(INTDIR)\Strategies\DIOP_Endpoint.obj" \
	"$(INTDIR)\Strategies\OC_Endpoint_Selector_Factory.obj" \
	"$(INTDIR)\Strategies\UIOP_Acceptor.obj" \
	"$(INTDIR)\Strategies\UIOP_Profile.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Connector.obj" \
	"$(INTDIR)\Strategies\SCIOP_Factory.obj" \
	"$(INTDIR)\Strategies\Optimized_Connection_Endpoint_Selector.obj" \
	"$(INTDIR)\Strategies\NULL_Connection_Purging_Strategy.obj" \
	"$(INTDIR)\Strategies\DIOP_Profile.obj" \
	"$(INTDIR)\Strategies\SCIOP_Endpoint.obj" \
	"$(INTDIR)\Strategies\COIOP_Transport.obj" \
	"$(INTDIR)\Strategies\UIOP_Factory.obj" \
	"$(INTDIR)\Strategies\DIOP_Acceptor.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Connection_Handler.obj" \
	"$(INTDIR)\Strategies\LFU_Connection_Purging_Strategy.obj" \
	"$(INTDIR)\Strategies\DIOP_Connection_Handler.obj" \
	"$(INTDIR)\Strategies\UIOP_Connection_Handler.obj" \
	"$(INTDIR)\Strategies\COIOP_Profile.obj" \
	"$(INTDIR)\Strategies\Strategies_ORBInitializer.obj" \
	"$(INTDIR)\Strategies\SCIOP_Acceptor.obj" \
	"$(INTDIR)\Strategies\COIOP_Connection_Handler.obj" \
	"$(INTDIR)\Strategies\SCIOP_Connection_Handler.obj" \
	"$(INTDIR)\Strategies\DIOP_Factory.obj" \
	"$(INTDIR)\Strategies\DIOP_Transport.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Profile.obj" \
	"$(INTDIR)\Strategies\advanced_resource.obj" \
	"$(INTDIR)\Strategies\UIOP_Transport.obj" \
	"$(INTDIR)\Strategies\OC_Endpoint_Selector_Loader.obj" \
	"$(INTDIR)\Strategies\COIOP_Connector.obj" \
	"$(INTDIR)\Strategies\SCIOP_Profile.obj" \
	"$(INTDIR)\Strategies\FIFO_Connection_Purging_Strategy.obj" \
	"$(INTDIR)\Strategies\COIOP_Factory.obj"

"..\..\lib\TAO_Strategiesd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_Strategiesd.dll.manifest" mt.exe -manifest "..\..\lib\TAO_Strategiesd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\Strategies\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_Strategies.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_STRATEGIES_BUILD_DLL -f "Makefile.Strategies.dep" "Strategies\uiop_endpointsC.cpp" "Strategies\sciop_endpointsC.cpp" "Strategies\COIOP_EndpointsC.cpp" "Strategies\SHMIOP_Acceptor.cpp" "Strategies\SHMIOP_Factory.cpp" "Strategies\LF_Strategy_Null.cpp" "Strategies\SCIOP_Transport.cpp" "Strategies\SHMIOP_Endpoint.cpp" "Strategies\DIOP_Connector.cpp" "Strategies\UIOP_Endpoint.cpp" "Strategies\SHMIOP_Transport.cpp" "Strategies\UIOP_Connector.cpp" "Strategies\COIOP_Endpoint.cpp" "Strategies\COIOP_Acceptor.cpp" "Strategies\SCIOP_Connector.cpp" "Strategies\DIOP_Endpoint.cpp" "Strategies\OC_Endpoint_Selector_Factory.cpp" "Strategies\UIOP_Acceptor.cpp" "Strategies\UIOP_Profile.cpp" "Strategies\SHMIOP_Connector.cpp" "Strategies\SCIOP_Factory.cpp" "Strategies\Optimized_Connection_Endpoint_Selector.cpp" "Strategies\NULL_Connection_Purging_Strategy.cpp" "Strategies\DIOP_Profile.cpp" "Strategies\SCIOP_Endpoint.cpp" "Strategies\COIOP_Transport.cpp" "Strategies\UIOP_Factory.cpp" "Strategies\DIOP_Acceptor.cpp" "Strategies\SHMIOP_Connection_Handler.cpp" "Strategies\LFU_Connection_Purging_Strategy.cpp" "Strategies\DIOP_Connection_Handler.cpp" "Strategies\UIOP_Connection_Handler.cpp" "Strategies\COIOP_Profile.cpp" "Strategies\Strategies_ORBInitializer.cpp" "Strategies\SCIOP_Acceptor.cpp" "Strategies\COIOP_Connection_Handler.cpp" "Strategies\SCIOP_Connection_Handler.cpp" "Strategies\DIOP_Factory.cpp" "Strategies\DIOP_Transport.cpp" "Strategies\SHMIOP_Profile.cpp" "Strategies\advanced_resource.cpp" "Strategies\UIOP_Transport.cpp" "Strategies\OC_Endpoint_Selector_Loader.cpp" "Strategies\COIOP_Connector.cpp" "Strategies\SCIOP_Profile.cpp" "Strategies\FIFO_Connection_Purging_Strategy.cpp" "Strategies\COIOP_Factory.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\TAO_Strategies.dll"
	-@del /f/q "$(OUTDIR)\TAO_Strategies.lib"
	-@del /f/q "$(OUTDIR)\TAO_Strategies.exp"
	-@del /f/q "$(OUTDIR)\TAO_Strategies.ilk"
	-@del /f/q "Strategies\uiop_endpointsC.h"
	-@del /f/q "Strategies\uiop_endpointsS.h"
	-@del /f/q "Strategies\uiop_endpointsC.cpp"
	-@del /f/q "Strategies\sciop_endpointsC.h"
	-@del /f/q "Strategies\sciop_endpointsS.h"
	-@del /f/q "Strategies\sciop_endpointsC.cpp"
	-@del /f/q "Strategies\COIOP_EndpointsC.h"
	-@del /f/q "Strategies\COIOP_EndpointsS.h"
	-@del /f/q "Strategies\COIOP_EndpointsC.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\Strategies\$(NULL)" mkdir "Release\Strategies"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_STRATEGIES_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_CodecFactory.lib TAO_PI.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\TAO_Strategies.dll" /implib:"$(OUTDIR)\TAO_Strategies.lib"
LINK32_OBJS= \
	"$(INTDIR)\Strategies\TAO_Strategies.res" \
	"$(INTDIR)\Strategies\uiop_endpointsC.obj" \
	"$(INTDIR)\Strategies\sciop_endpointsC.obj" \
	"$(INTDIR)\Strategies\COIOP_EndpointsC.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Acceptor.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Factory.obj" \
	"$(INTDIR)\Strategies\LF_Strategy_Null.obj" \
	"$(INTDIR)\Strategies\SCIOP_Transport.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Endpoint.obj" \
	"$(INTDIR)\Strategies\DIOP_Connector.obj" \
	"$(INTDIR)\Strategies\UIOP_Endpoint.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Transport.obj" \
	"$(INTDIR)\Strategies\UIOP_Connector.obj" \
	"$(INTDIR)\Strategies\COIOP_Endpoint.obj" \
	"$(INTDIR)\Strategies\COIOP_Acceptor.obj" \
	"$(INTDIR)\Strategies\SCIOP_Connector.obj" \
	"$(INTDIR)\Strategies\DIOP_Endpoint.obj" \
	"$(INTDIR)\Strategies\OC_Endpoint_Selector_Factory.obj" \
	"$(INTDIR)\Strategies\UIOP_Acceptor.obj" \
	"$(INTDIR)\Strategies\UIOP_Profile.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Connector.obj" \
	"$(INTDIR)\Strategies\SCIOP_Factory.obj" \
	"$(INTDIR)\Strategies\Optimized_Connection_Endpoint_Selector.obj" \
	"$(INTDIR)\Strategies\NULL_Connection_Purging_Strategy.obj" \
	"$(INTDIR)\Strategies\DIOP_Profile.obj" \
	"$(INTDIR)\Strategies\SCIOP_Endpoint.obj" \
	"$(INTDIR)\Strategies\COIOP_Transport.obj" \
	"$(INTDIR)\Strategies\UIOP_Factory.obj" \
	"$(INTDIR)\Strategies\DIOP_Acceptor.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Connection_Handler.obj" \
	"$(INTDIR)\Strategies\LFU_Connection_Purging_Strategy.obj" \
	"$(INTDIR)\Strategies\DIOP_Connection_Handler.obj" \
	"$(INTDIR)\Strategies\UIOP_Connection_Handler.obj" \
	"$(INTDIR)\Strategies\COIOP_Profile.obj" \
	"$(INTDIR)\Strategies\Strategies_ORBInitializer.obj" \
	"$(INTDIR)\Strategies\SCIOP_Acceptor.obj" \
	"$(INTDIR)\Strategies\COIOP_Connection_Handler.obj" \
	"$(INTDIR)\Strategies\SCIOP_Connection_Handler.obj" \
	"$(INTDIR)\Strategies\DIOP_Factory.obj" \
	"$(INTDIR)\Strategies\DIOP_Transport.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Profile.obj" \
	"$(INTDIR)\Strategies\advanced_resource.obj" \
	"$(INTDIR)\Strategies\UIOP_Transport.obj" \
	"$(INTDIR)\Strategies\OC_Endpoint_Selector_Loader.obj" \
	"$(INTDIR)\Strategies\COIOP_Connector.obj" \
	"$(INTDIR)\Strategies\SCIOP_Profile.obj" \
	"$(INTDIR)\Strategies\FIFO_Connection_Purging_Strategy.obj" \
	"$(INTDIR)\Strategies\COIOP_Factory.obj"

"..\..\lib\TAO_Strategies.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_Strategies.dll.manifest" mt.exe -manifest "..\..\lib\TAO_Strategies.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\Strategies\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_Strategiessd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Strategies.dep" "Strategies\uiop_endpointsC.cpp" "Strategies\sciop_endpointsC.cpp" "Strategies\COIOP_EndpointsC.cpp" "Strategies\SHMIOP_Acceptor.cpp" "Strategies\SHMIOP_Factory.cpp" "Strategies\LF_Strategy_Null.cpp" "Strategies\SCIOP_Transport.cpp" "Strategies\SHMIOP_Endpoint.cpp" "Strategies\DIOP_Connector.cpp" "Strategies\UIOP_Endpoint.cpp" "Strategies\SHMIOP_Transport.cpp" "Strategies\UIOP_Connector.cpp" "Strategies\COIOP_Endpoint.cpp" "Strategies\COIOP_Acceptor.cpp" "Strategies\SCIOP_Connector.cpp" "Strategies\DIOP_Endpoint.cpp" "Strategies\OC_Endpoint_Selector_Factory.cpp" "Strategies\UIOP_Acceptor.cpp" "Strategies\UIOP_Profile.cpp" "Strategies\SHMIOP_Connector.cpp" "Strategies\SCIOP_Factory.cpp" "Strategies\Optimized_Connection_Endpoint_Selector.cpp" "Strategies\NULL_Connection_Purging_Strategy.cpp" "Strategies\DIOP_Profile.cpp" "Strategies\SCIOP_Endpoint.cpp" "Strategies\COIOP_Transport.cpp" "Strategies\UIOP_Factory.cpp" "Strategies\DIOP_Acceptor.cpp" "Strategies\SHMIOP_Connection_Handler.cpp" "Strategies\LFU_Connection_Purging_Strategy.cpp" "Strategies\DIOP_Connection_Handler.cpp" "Strategies\UIOP_Connection_Handler.cpp" "Strategies\COIOP_Profile.cpp" "Strategies\Strategies_ORBInitializer.cpp" "Strategies\SCIOP_Acceptor.cpp" "Strategies\COIOP_Connection_Handler.cpp" "Strategies\SCIOP_Connection_Handler.cpp" "Strategies\DIOP_Factory.cpp" "Strategies\DIOP_Transport.cpp" "Strategies\SHMIOP_Profile.cpp" "Strategies\advanced_resource.cpp" "Strategies\UIOP_Transport.cpp" "Strategies\OC_Endpoint_Selector_Loader.cpp" "Strategies\COIOP_Connector.cpp" "Strategies\SCIOP_Profile.cpp" "Strategies\FIFO_Connection_Purging_Strategy.cpp" "Strategies\COIOP_Factory.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_Strategiessd.lib"
	-@del /f/q "$(OUTDIR)\TAO_Strategiessd.exp"
	-@del /f/q "$(OUTDIR)\TAO_Strategiessd.ilk"
	-@del /f/q "..\..\lib\TAO_Strategiessd.pdb"
	-@del /f/q "Strategies\uiop_endpointsC.h"
	-@del /f/q "Strategies\uiop_endpointsS.h"
	-@del /f/q "Strategies\uiop_endpointsC.cpp"
	-@del /f/q "Strategies\sciop_endpointsC.h"
	-@del /f/q "Strategies\sciop_endpointsS.h"
	-@del /f/q "Strategies\sciop_endpointsC.cpp"
	-@del /f/q "Strategies\COIOP_EndpointsC.h"
	-@del /f/q "Strategies\COIOP_EndpointsS.h"
	-@del /f/q "Strategies\COIOP_EndpointsC.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\Strategies\$(NULL)" mkdir "Static_Debug\Strategies"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"..\..\lib\TAO_Strategiessd.pdb" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_Strategiessd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Strategies\uiop_endpointsC.obj" \
	"$(INTDIR)\Strategies\sciop_endpointsC.obj" \
	"$(INTDIR)\Strategies\COIOP_EndpointsC.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Acceptor.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Factory.obj" \
	"$(INTDIR)\Strategies\LF_Strategy_Null.obj" \
	"$(INTDIR)\Strategies\SCIOP_Transport.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Endpoint.obj" \
	"$(INTDIR)\Strategies\DIOP_Connector.obj" \
	"$(INTDIR)\Strategies\UIOP_Endpoint.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Transport.obj" \
	"$(INTDIR)\Strategies\UIOP_Connector.obj" \
	"$(INTDIR)\Strategies\COIOP_Endpoint.obj" \
	"$(INTDIR)\Strategies\COIOP_Acceptor.obj" \
	"$(INTDIR)\Strategies\SCIOP_Connector.obj" \
	"$(INTDIR)\Strategies\DIOP_Endpoint.obj" \
	"$(INTDIR)\Strategies\OC_Endpoint_Selector_Factory.obj" \
	"$(INTDIR)\Strategies\UIOP_Acceptor.obj" \
	"$(INTDIR)\Strategies\UIOP_Profile.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Connector.obj" \
	"$(INTDIR)\Strategies\SCIOP_Factory.obj" \
	"$(INTDIR)\Strategies\Optimized_Connection_Endpoint_Selector.obj" \
	"$(INTDIR)\Strategies\NULL_Connection_Purging_Strategy.obj" \
	"$(INTDIR)\Strategies\DIOP_Profile.obj" \
	"$(INTDIR)\Strategies\SCIOP_Endpoint.obj" \
	"$(INTDIR)\Strategies\COIOP_Transport.obj" \
	"$(INTDIR)\Strategies\UIOP_Factory.obj" \
	"$(INTDIR)\Strategies\DIOP_Acceptor.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Connection_Handler.obj" \
	"$(INTDIR)\Strategies\LFU_Connection_Purging_Strategy.obj" \
	"$(INTDIR)\Strategies\DIOP_Connection_Handler.obj" \
	"$(INTDIR)\Strategies\UIOP_Connection_Handler.obj" \
	"$(INTDIR)\Strategies\COIOP_Profile.obj" \
	"$(INTDIR)\Strategies\Strategies_ORBInitializer.obj" \
	"$(INTDIR)\Strategies\SCIOP_Acceptor.obj" \
	"$(INTDIR)\Strategies\COIOP_Connection_Handler.obj" \
	"$(INTDIR)\Strategies\SCIOP_Connection_Handler.obj" \
	"$(INTDIR)\Strategies\DIOP_Factory.obj" \
	"$(INTDIR)\Strategies\DIOP_Transport.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Profile.obj" \
	"$(INTDIR)\Strategies\advanced_resource.obj" \
	"$(INTDIR)\Strategies\UIOP_Transport.obj" \
	"$(INTDIR)\Strategies\OC_Endpoint_Selector_Loader.obj" \
	"$(INTDIR)\Strategies\COIOP_Connector.obj" \
	"$(INTDIR)\Strategies\SCIOP_Profile.obj" \
	"$(INTDIR)\Strategies\FIFO_Connection_Purging_Strategy.obj" \
	"$(INTDIR)\Strategies\COIOP_Factory.obj"

"$(OUTDIR)\TAO_Strategiessd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_Strategiessd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_Strategiessd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\Strategies\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_Strategiess.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Strategies.dep" "Strategies\uiop_endpointsC.cpp" "Strategies\sciop_endpointsC.cpp" "Strategies\COIOP_EndpointsC.cpp" "Strategies\SHMIOP_Acceptor.cpp" "Strategies\SHMIOP_Factory.cpp" "Strategies\LF_Strategy_Null.cpp" "Strategies\SCIOP_Transport.cpp" "Strategies\SHMIOP_Endpoint.cpp" "Strategies\DIOP_Connector.cpp" "Strategies\UIOP_Endpoint.cpp" "Strategies\SHMIOP_Transport.cpp" "Strategies\UIOP_Connector.cpp" "Strategies\COIOP_Endpoint.cpp" "Strategies\COIOP_Acceptor.cpp" "Strategies\SCIOP_Connector.cpp" "Strategies\DIOP_Endpoint.cpp" "Strategies\OC_Endpoint_Selector_Factory.cpp" "Strategies\UIOP_Acceptor.cpp" "Strategies\UIOP_Profile.cpp" "Strategies\SHMIOP_Connector.cpp" "Strategies\SCIOP_Factory.cpp" "Strategies\Optimized_Connection_Endpoint_Selector.cpp" "Strategies\NULL_Connection_Purging_Strategy.cpp" "Strategies\DIOP_Profile.cpp" "Strategies\SCIOP_Endpoint.cpp" "Strategies\COIOP_Transport.cpp" "Strategies\UIOP_Factory.cpp" "Strategies\DIOP_Acceptor.cpp" "Strategies\SHMIOP_Connection_Handler.cpp" "Strategies\LFU_Connection_Purging_Strategy.cpp" "Strategies\DIOP_Connection_Handler.cpp" "Strategies\UIOP_Connection_Handler.cpp" "Strategies\COIOP_Profile.cpp" "Strategies\Strategies_ORBInitializer.cpp" "Strategies\SCIOP_Acceptor.cpp" "Strategies\COIOP_Connection_Handler.cpp" "Strategies\SCIOP_Connection_Handler.cpp" "Strategies\DIOP_Factory.cpp" "Strategies\DIOP_Transport.cpp" "Strategies\SHMIOP_Profile.cpp" "Strategies\advanced_resource.cpp" "Strategies\UIOP_Transport.cpp" "Strategies\OC_Endpoint_Selector_Loader.cpp" "Strategies\COIOP_Connector.cpp" "Strategies\SCIOP_Profile.cpp" "Strategies\FIFO_Connection_Purging_Strategy.cpp" "Strategies\COIOP_Factory.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_Strategiess.lib"
	-@del /f/q "$(OUTDIR)\TAO_Strategiess.exp"
	-@del /f/q "$(OUTDIR)\TAO_Strategiess.ilk"
	-@del /f/q "Strategies\uiop_endpointsC.h"
	-@del /f/q "Strategies\uiop_endpointsS.h"
	-@del /f/q "Strategies\uiop_endpointsC.cpp"
	-@del /f/q "Strategies\sciop_endpointsC.h"
	-@del /f/q "Strategies\sciop_endpointsS.h"
	-@del /f/q "Strategies\sciop_endpointsC.cpp"
	-@del /f/q "Strategies\COIOP_EndpointsC.h"
	-@del /f/q "Strategies\COIOP_EndpointsS.h"
	-@del /f/q "Strategies\COIOP_EndpointsC.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\Strategies\$(NULL)" mkdir "Static_Release\Strategies"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_Strategiess.lib"
LINK32_OBJS= \
	"$(INTDIR)\Strategies\uiop_endpointsC.obj" \
	"$(INTDIR)\Strategies\sciop_endpointsC.obj" \
	"$(INTDIR)\Strategies\COIOP_EndpointsC.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Acceptor.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Factory.obj" \
	"$(INTDIR)\Strategies\LF_Strategy_Null.obj" \
	"$(INTDIR)\Strategies\SCIOP_Transport.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Endpoint.obj" \
	"$(INTDIR)\Strategies\DIOP_Connector.obj" \
	"$(INTDIR)\Strategies\UIOP_Endpoint.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Transport.obj" \
	"$(INTDIR)\Strategies\UIOP_Connector.obj" \
	"$(INTDIR)\Strategies\COIOP_Endpoint.obj" \
	"$(INTDIR)\Strategies\COIOP_Acceptor.obj" \
	"$(INTDIR)\Strategies\SCIOP_Connector.obj" \
	"$(INTDIR)\Strategies\DIOP_Endpoint.obj" \
	"$(INTDIR)\Strategies\OC_Endpoint_Selector_Factory.obj" \
	"$(INTDIR)\Strategies\UIOP_Acceptor.obj" \
	"$(INTDIR)\Strategies\UIOP_Profile.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Connector.obj" \
	"$(INTDIR)\Strategies\SCIOP_Factory.obj" \
	"$(INTDIR)\Strategies\Optimized_Connection_Endpoint_Selector.obj" \
	"$(INTDIR)\Strategies\NULL_Connection_Purging_Strategy.obj" \
	"$(INTDIR)\Strategies\DIOP_Profile.obj" \
	"$(INTDIR)\Strategies\SCIOP_Endpoint.obj" \
	"$(INTDIR)\Strategies\COIOP_Transport.obj" \
	"$(INTDIR)\Strategies\UIOP_Factory.obj" \
	"$(INTDIR)\Strategies\DIOP_Acceptor.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Connection_Handler.obj" \
	"$(INTDIR)\Strategies\LFU_Connection_Purging_Strategy.obj" \
	"$(INTDIR)\Strategies\DIOP_Connection_Handler.obj" \
	"$(INTDIR)\Strategies\UIOP_Connection_Handler.obj" \
	"$(INTDIR)\Strategies\COIOP_Profile.obj" \
	"$(INTDIR)\Strategies\Strategies_ORBInitializer.obj" \
	"$(INTDIR)\Strategies\SCIOP_Acceptor.obj" \
	"$(INTDIR)\Strategies\COIOP_Connection_Handler.obj" \
	"$(INTDIR)\Strategies\SCIOP_Connection_Handler.obj" \
	"$(INTDIR)\Strategies\DIOP_Factory.obj" \
	"$(INTDIR)\Strategies\DIOP_Transport.obj" \
	"$(INTDIR)\Strategies\SHMIOP_Profile.obj" \
	"$(INTDIR)\Strategies\advanced_resource.obj" \
	"$(INTDIR)\Strategies\UIOP_Transport.obj" \
	"$(INTDIR)\Strategies\OC_Endpoint_Selector_Loader.obj" \
	"$(INTDIR)\Strategies\COIOP_Connector.obj" \
	"$(INTDIR)\Strategies\SCIOP_Profile.obj" \
	"$(INTDIR)\Strategies\FIFO_Connection_Purging_Strategy.obj" \
	"$(INTDIR)\Strategies\COIOP_Factory.obj"

"$(OUTDIR)\TAO_Strategiess.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_Strategiess.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_Strategiess.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Strategies.dep")
!INCLUDE "Makefile.Strategies.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Strategies\uiop_endpointsC.cpp"

"$(INTDIR)\Strategies\uiop_endpointsC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\uiop_endpointsC.obj" $(SOURCE)

SOURCE="Strategies\sciop_endpointsC.cpp"

"$(INTDIR)\Strategies\sciop_endpointsC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\sciop_endpointsC.obj" $(SOURCE)

SOURCE="Strategies\COIOP_EndpointsC.cpp"

"$(INTDIR)\Strategies\COIOP_EndpointsC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\COIOP_EndpointsC.obj" $(SOURCE)

SOURCE="Strategies\SHMIOP_Acceptor.cpp"

"$(INTDIR)\Strategies\SHMIOP_Acceptor.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\SHMIOP_Acceptor.obj" $(SOURCE)

SOURCE="Strategies\SHMIOP_Factory.cpp"

"$(INTDIR)\Strategies\SHMIOP_Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\SHMIOP_Factory.obj" $(SOURCE)

SOURCE="Strategies\LF_Strategy_Null.cpp"

"$(INTDIR)\Strategies\LF_Strategy_Null.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\LF_Strategy_Null.obj" $(SOURCE)

SOURCE="Strategies\SCIOP_Transport.cpp"

"$(INTDIR)\Strategies\SCIOP_Transport.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\SCIOP_Transport.obj" $(SOURCE)

SOURCE="Strategies\SHMIOP_Endpoint.cpp"

"$(INTDIR)\Strategies\SHMIOP_Endpoint.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\SHMIOP_Endpoint.obj" $(SOURCE)

SOURCE="Strategies\DIOP_Connector.cpp"

"$(INTDIR)\Strategies\DIOP_Connector.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\DIOP_Connector.obj" $(SOURCE)

SOURCE="Strategies\UIOP_Endpoint.cpp"

"$(INTDIR)\Strategies\UIOP_Endpoint.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\UIOP_Endpoint.obj" $(SOURCE)

SOURCE="Strategies\SHMIOP_Transport.cpp"

"$(INTDIR)\Strategies\SHMIOP_Transport.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\SHMIOP_Transport.obj" $(SOURCE)

SOURCE="Strategies\UIOP_Connector.cpp"

"$(INTDIR)\Strategies\UIOP_Connector.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\UIOP_Connector.obj" $(SOURCE)

SOURCE="Strategies\COIOP_Endpoint.cpp"

"$(INTDIR)\Strategies\COIOP_Endpoint.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\COIOP_Endpoint.obj" $(SOURCE)

SOURCE="Strategies\COIOP_Acceptor.cpp"

"$(INTDIR)\Strategies\COIOP_Acceptor.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\COIOP_Acceptor.obj" $(SOURCE)

SOURCE="Strategies\SCIOP_Connector.cpp"

"$(INTDIR)\Strategies\SCIOP_Connector.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\SCIOP_Connector.obj" $(SOURCE)

SOURCE="Strategies\DIOP_Endpoint.cpp"

"$(INTDIR)\Strategies\DIOP_Endpoint.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\DIOP_Endpoint.obj" $(SOURCE)

SOURCE="Strategies\OC_Endpoint_Selector_Factory.cpp"

"$(INTDIR)\Strategies\OC_Endpoint_Selector_Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\OC_Endpoint_Selector_Factory.obj" $(SOURCE)

SOURCE="Strategies\UIOP_Acceptor.cpp"

"$(INTDIR)\Strategies\UIOP_Acceptor.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\UIOP_Acceptor.obj" $(SOURCE)

SOURCE="Strategies\UIOP_Profile.cpp"

"$(INTDIR)\Strategies\UIOP_Profile.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\UIOP_Profile.obj" $(SOURCE)

SOURCE="Strategies\SHMIOP_Connector.cpp"

"$(INTDIR)\Strategies\SHMIOP_Connector.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\SHMIOP_Connector.obj" $(SOURCE)

SOURCE="Strategies\SCIOP_Factory.cpp"

"$(INTDIR)\Strategies\SCIOP_Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\SCIOP_Factory.obj" $(SOURCE)

SOURCE="Strategies\Optimized_Connection_Endpoint_Selector.cpp"

"$(INTDIR)\Strategies\Optimized_Connection_Endpoint_Selector.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\Optimized_Connection_Endpoint_Selector.obj" $(SOURCE)

SOURCE="Strategies\NULL_Connection_Purging_Strategy.cpp"

"$(INTDIR)\Strategies\NULL_Connection_Purging_Strategy.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\NULL_Connection_Purging_Strategy.obj" $(SOURCE)

SOURCE="Strategies\DIOP_Profile.cpp"

"$(INTDIR)\Strategies\DIOP_Profile.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\DIOP_Profile.obj" $(SOURCE)

SOURCE="Strategies\SCIOP_Endpoint.cpp"

"$(INTDIR)\Strategies\SCIOP_Endpoint.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\SCIOP_Endpoint.obj" $(SOURCE)

SOURCE="Strategies\COIOP_Transport.cpp"

"$(INTDIR)\Strategies\COIOP_Transport.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\COIOP_Transport.obj" $(SOURCE)

SOURCE="Strategies\UIOP_Factory.cpp"

"$(INTDIR)\Strategies\UIOP_Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\UIOP_Factory.obj" $(SOURCE)

SOURCE="Strategies\DIOP_Acceptor.cpp"

"$(INTDIR)\Strategies\DIOP_Acceptor.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\DIOP_Acceptor.obj" $(SOURCE)

SOURCE="Strategies\SHMIOP_Connection_Handler.cpp"

"$(INTDIR)\Strategies\SHMIOP_Connection_Handler.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\SHMIOP_Connection_Handler.obj" $(SOURCE)

SOURCE="Strategies\LFU_Connection_Purging_Strategy.cpp"

"$(INTDIR)\Strategies\LFU_Connection_Purging_Strategy.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\LFU_Connection_Purging_Strategy.obj" $(SOURCE)

SOURCE="Strategies\DIOP_Connection_Handler.cpp"

"$(INTDIR)\Strategies\DIOP_Connection_Handler.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\DIOP_Connection_Handler.obj" $(SOURCE)

SOURCE="Strategies\UIOP_Connection_Handler.cpp"

"$(INTDIR)\Strategies\UIOP_Connection_Handler.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\UIOP_Connection_Handler.obj" $(SOURCE)

SOURCE="Strategies\COIOP_Profile.cpp"

"$(INTDIR)\Strategies\COIOP_Profile.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\COIOP_Profile.obj" $(SOURCE)

SOURCE="Strategies\Strategies_ORBInitializer.cpp"

"$(INTDIR)\Strategies\Strategies_ORBInitializer.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\Strategies_ORBInitializer.obj" $(SOURCE)

SOURCE="Strategies\SCIOP_Acceptor.cpp"

"$(INTDIR)\Strategies\SCIOP_Acceptor.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\SCIOP_Acceptor.obj" $(SOURCE)

SOURCE="Strategies\COIOP_Connection_Handler.cpp"

"$(INTDIR)\Strategies\COIOP_Connection_Handler.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\COIOP_Connection_Handler.obj" $(SOURCE)

SOURCE="Strategies\SCIOP_Connection_Handler.cpp"

"$(INTDIR)\Strategies\SCIOP_Connection_Handler.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\SCIOP_Connection_Handler.obj" $(SOURCE)

SOURCE="Strategies\DIOP_Factory.cpp"

"$(INTDIR)\Strategies\DIOP_Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\DIOP_Factory.obj" $(SOURCE)

SOURCE="Strategies\DIOP_Transport.cpp"

"$(INTDIR)\Strategies\DIOP_Transport.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\DIOP_Transport.obj" $(SOURCE)

SOURCE="Strategies\SHMIOP_Profile.cpp"

"$(INTDIR)\Strategies\SHMIOP_Profile.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\SHMIOP_Profile.obj" $(SOURCE)

SOURCE="Strategies\advanced_resource.cpp"

"$(INTDIR)\Strategies\advanced_resource.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\advanced_resource.obj" $(SOURCE)

SOURCE="Strategies\UIOP_Transport.cpp"

"$(INTDIR)\Strategies\UIOP_Transport.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\UIOP_Transport.obj" $(SOURCE)

SOURCE="Strategies\OC_Endpoint_Selector_Loader.cpp"

"$(INTDIR)\Strategies\OC_Endpoint_Selector_Loader.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\OC_Endpoint_Selector_Loader.obj" $(SOURCE)

SOURCE="Strategies\COIOP_Connector.cpp"

"$(INTDIR)\Strategies\COIOP_Connector.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\COIOP_Connector.obj" $(SOURCE)

SOURCE="Strategies\SCIOP_Profile.cpp"

"$(INTDIR)\Strategies\SCIOP_Profile.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\SCIOP_Profile.obj" $(SOURCE)

SOURCE="Strategies\FIFO_Connection_Purging_Strategy.cpp"

"$(INTDIR)\Strategies\FIFO_Connection_Purging_Strategy.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\FIFO_Connection_Purging_Strategy.obj" $(SOURCE)

SOURCE="Strategies\COIOP_Factory.cpp"

"$(INTDIR)\Strategies\COIOP_Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Strategies\COIOP_Factory.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="Strategies\uiop_endpoints.pidl"

InputPath=Strategies\uiop_endpoints.pidl

"Strategies\uiop_endpointsC.h" "Strategies\uiop_endpointsS.h" "Strategies\uiop_endpointsC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Strategies_uiop_endpoints_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Gp -Gd -Sorb -DCORBA3 -Wb,export_macro=TAO_Strategies_Export -Wb,export_include=tao/Strategies/strategies_export.h -o Strategies "$(InputPath)"
<<

SOURCE="Strategies\sciop_endpoints.pidl"

InputPath=Strategies\sciop_endpoints.pidl

"Strategies\sciop_endpointsC.h" "Strategies\sciop_endpointsS.h" "Strategies\sciop_endpointsC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Strategies_sciop_endpoints_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Gp -Gd -Sorb -DCORBA3 -Wb,export_macro=TAO_Strategies_Export -Wb,export_include=tao/Strategies/strategies_export.h -o Strategies "$(InputPath)"
<<

SOURCE="Strategies\COIOP_Endpoints.pidl"

InputPath=Strategies\COIOP_Endpoints.pidl

"Strategies\COIOP_EndpointsC.h" "Strategies\COIOP_EndpointsS.h" "Strategies\COIOP_EndpointsC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Strategies_COIOP_Endpoints_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Gp -Gd -Sorb -DCORBA3 -Wb,export_macro=TAO_Strategies_Export -Wb,export_include=tao/Strategies/strategies_export.h -o Strategies "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="Strategies\uiop_endpoints.pidl"

InputPath=Strategies\uiop_endpoints.pidl

"Strategies\uiop_endpointsC.h" "Strategies\uiop_endpointsS.h" "Strategies\uiop_endpointsC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Strategies_uiop_endpoints_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Gp -Gd -Sorb -DCORBA3 -Wb,export_macro=TAO_Strategies_Export -Wb,export_include=tao/Strategies/strategies_export.h -o Strategies "$(InputPath)"
<<

SOURCE="Strategies\sciop_endpoints.pidl"

InputPath=Strategies\sciop_endpoints.pidl

"Strategies\sciop_endpointsC.h" "Strategies\sciop_endpointsS.h" "Strategies\sciop_endpointsC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Strategies_sciop_endpoints_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Gp -Gd -Sorb -DCORBA3 -Wb,export_macro=TAO_Strategies_Export -Wb,export_include=tao/Strategies/strategies_export.h -o Strategies "$(InputPath)"
<<

SOURCE="Strategies\COIOP_Endpoints.pidl"

InputPath=Strategies\COIOP_Endpoints.pidl

"Strategies\COIOP_EndpointsC.h" "Strategies\COIOP_EndpointsS.h" "Strategies\COIOP_EndpointsC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Strategies_COIOP_Endpoints_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Gp -Gd -Sorb -DCORBA3 -Wb,export_macro=TAO_Strategies_Export -Wb,export_include=tao/Strategies/strategies_export.h -o Strategies "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="Strategies\uiop_endpoints.pidl"

InputPath=Strategies\uiop_endpoints.pidl

"Strategies\uiop_endpointsC.h" "Strategies\uiop_endpointsS.h" "Strategies\uiop_endpointsC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Strategies_uiop_endpoints_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Gp -Gd -Sorb -DCORBA3 -Wb,export_macro=TAO_Strategies_Export -Wb,export_include=tao/Strategies/strategies_export.h -o Strategies "$(InputPath)"
<<

SOURCE="Strategies\sciop_endpoints.pidl"

InputPath=Strategies\sciop_endpoints.pidl

"Strategies\sciop_endpointsC.h" "Strategies\sciop_endpointsS.h" "Strategies\sciop_endpointsC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Strategies_sciop_endpoints_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Gp -Gd -Sorb -DCORBA3 -Wb,export_macro=TAO_Strategies_Export -Wb,export_include=tao/Strategies/strategies_export.h -o Strategies "$(InputPath)"
<<

SOURCE="Strategies\COIOP_Endpoints.pidl"

InputPath=Strategies\COIOP_Endpoints.pidl

"Strategies\COIOP_EndpointsC.h" "Strategies\COIOP_EndpointsS.h" "Strategies\COIOP_EndpointsC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Strategies_COIOP_Endpoints_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Gp -Gd -Sorb -DCORBA3 -Wb,export_macro=TAO_Strategies_Export -Wb,export_include=tao/Strategies/strategies_export.h -o Strategies "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="Strategies\uiop_endpoints.pidl"

InputPath=Strategies\uiop_endpoints.pidl

"Strategies\uiop_endpointsC.h" "Strategies\uiop_endpointsS.h" "Strategies\uiop_endpointsC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Strategies_uiop_endpoints_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Gp -Gd -Sorb -DCORBA3 -Wb,export_macro=TAO_Strategies_Export -Wb,export_include=tao/Strategies/strategies_export.h -o Strategies "$(InputPath)"
<<

SOURCE="Strategies\sciop_endpoints.pidl"

InputPath=Strategies\sciop_endpoints.pidl

"Strategies\sciop_endpointsC.h" "Strategies\sciop_endpointsS.h" "Strategies\sciop_endpointsC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Strategies_sciop_endpoints_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Gp -Gd -Sorb -DCORBA3 -Wb,export_macro=TAO_Strategies_Export -Wb,export_include=tao/Strategies/strategies_export.h -o Strategies "$(InputPath)"
<<

SOURCE="Strategies\COIOP_Endpoints.pidl"

InputPath=Strategies\COIOP_Endpoints.pidl

"Strategies\COIOP_EndpointsC.h" "Strategies\COIOP_EndpointsS.h" "Strategies\COIOP_EndpointsC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Strategies_COIOP_Endpoints_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Gp -Gd -Sorb -DCORBA3 -Wb,export_macro=TAO_Strategies_Export -Wb,export_include=tao/Strategies/strategies_export.h -o Strategies "$(InputPath)"
<<

!ENDIF

SOURCE="Strategies\TAO_Strategies.rc"

"$(INTDIR)\Strategies\TAO_Strategies.res" : $(SOURCE)
	@if not exist "$(INTDIR)\Strategies\$(NULL)" mkdir "$(INTDIR)\Strategies\"
	$(RSC) /l 0x409 /fo"$(INTDIR)\Strategies\TAO_Strategies.res" /d NDEBUG /d WIN64 /d _CRT_SECURE_NO_WARNINGS /d _CRT_SECURE_NO_DEPRECATE /d _CRT_NONSTDC_NO_DEPRECATE /i "..\.." /i ".." $(SOURCE)



!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Strategies.dep")
	@echo Using "Makefile.Strategies.dep"
!ELSE
	@echo Warning: cannot find "Makefile.Strategies.dep"
!ENDIF
!ENDIF

