# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.AnyTypeCode.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "AnyTypeCode\AnySeqC.h" "AnyTypeCode\AnySeqS.h" "AnyTypeCode\AnySeqA.h" "AnyTypeCode\AnySeqC.cpp" "AnyTypeCode\AnySeqA.cpp" "AnyTypeCode\Dynamic_ParameterC.h" "AnyTypeCode\Dynamic_ParameterS.h" "AnyTypeCode\Dynamic_ParameterA.h" "AnyTypeCode\Dynamic_ParameterC.cpp" "AnyTypeCode\Dynamic_ParameterA.cpp" "AnyTypeCode\ValueModifierC.h" "AnyTypeCode\ValueModifierS.h" "AnyTypeCode\ValueModifierA.h" "AnyTypeCode\ValueModifierC.cpp" "AnyTypeCode\ValueModifierA.cpp" "AnyTypeCode\VisibilityC.h" "AnyTypeCode\VisibilityS.h" "AnyTypeCode\VisibilityA.h" "AnyTypeCode\VisibilityC.cpp" "AnyTypeCode\VisibilityA.cpp" "AnyTypeCode\DynamicC.h" "AnyTypeCode\DynamicS.h" "AnyTypeCode\DynamicA.h" "AnyTypeCode\DynamicC.cpp" "AnyTypeCode\DynamicA.cpp" "AnyTypeCode\BoundsC.h" "AnyTypeCode\BoundsS.h" "AnyTypeCode\BoundsA.h" "AnyTypeCode\BoundsC.cpp" "AnyTypeCode\BoundsA.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\AnyTypeCode\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_AnyTypeCoded.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_ANYTYPECODE_BUILD_DLL -f "Makefile.AnyTypeCode.dep" "AnyTypeCode\ServicesA.cpp" "AnyTypeCode\Alias_TypeCode_Static.cpp" "AnyTypeCode\Any.cpp" "AnyTypeCode\AnySeqA.cpp" "AnyTypeCode\AnySeqC.cpp" "AnyTypeCode\Any_Basic_Impl.cpp" "AnyTypeCode\Any_Impl.cpp" "AnyTypeCode\Any_SystemException.cpp" "AnyTypeCode\Any_Unknown_IDL_Type.cpp" "AnyTypeCode\AnyTypeCode_Adapter_Impl.cpp" "AnyTypeCode\append.cpp" "AnyTypeCode\BasicTypeTraits.cpp" "AnyTypeCode\BooleanSeqA.cpp" "AnyTypeCode\BoundsA.cpp" "AnyTypeCode\BoundsC.cpp" "AnyTypeCode\CharSeqA.cpp" "AnyTypeCode\CONV_FRAMEA.cpp" "AnyTypeCode\CurrentA.cpp" "AnyTypeCode\DoubleSeqA.cpp" "AnyTypeCode\DynamicA.cpp" "AnyTypeCode\DynamicC.cpp" "AnyTypeCode\Dynamic_ParameterA.cpp" "AnyTypeCode\Dynamic_ParameterC.cpp" "AnyTypeCode\Empty_Param_TypeCode.cpp" "AnyTypeCode\Enum_TypeCode_Static.cpp" "AnyTypeCode\ExceptionA.cpp" "AnyTypeCode\FloatSeqA.cpp" "AnyTypeCode\GIOPA.cpp" "AnyTypeCode\IIOPA.cpp" "AnyTypeCode\IOPA.cpp" "AnyTypeCode\IIOP_EndpointsA.cpp" "AnyTypeCode\LongDoubleSeqA.cpp" "AnyTypeCode\LongLongSeqA.cpp" "AnyTypeCode\LongSeqA.cpp" "AnyTypeCode\Marshal.cpp" "AnyTypeCode\Messaging_PolicyValueA.cpp" "AnyTypeCode\NVList.cpp" "AnyTypeCode\NVList_Adapter_Impl.cpp" "AnyTypeCode\ObjectIdListA.cpp" "AnyTypeCode\Objref_TypeCode_Static.cpp" "AnyTypeCode\OctetSeqA.cpp" "AnyTypeCode\orb_typesA.cpp" "AnyTypeCode\ParameterModeA.cpp" "AnyTypeCode\PI_ForwardA.cpp" "AnyTypeCode\PolicyA.cpp" "AnyTypeCode\Policy_CurrentA.cpp" "AnyTypeCode\Policy_ForwardA.cpp" "AnyTypeCode\Policy_ManagerA.cpp" "AnyTypeCode\PortableInterceptorA.cpp" "AnyTypeCode\Sequence_TypeCode_Static.cpp" "AnyTypeCode\ShortSeqA.cpp" "AnyTypeCode\skip.cpp" "AnyTypeCode\StringSeqA.cpp" "AnyTypeCode\String_TypeCode_Static.cpp" "AnyTypeCode\Struct_TypeCode_Static.cpp" "AnyTypeCode\SystemExceptionA.cpp" "AnyTypeCode\TAOA.cpp" "AnyTypeCode\TimeBaseA.cpp" "AnyTypeCode\True_RefCount_Policy.cpp" "AnyTypeCode\TypeCode.cpp" "AnyTypeCode\TypeCodeA.cpp" "AnyTypeCode\TypeCode_CDR_Extraction.cpp" "AnyTypeCode\TypeCode_Constants.cpp" "AnyTypeCode\ULongLongSeqA.cpp" "AnyTypeCode\ULongSeqA.cpp" "AnyTypeCode\Union_TypeCode_Static.cpp" "AnyTypeCode\UShortSeqA.cpp" "AnyTypeCode\Value_TypeCode_Static.cpp" "AnyTypeCode\ValueModifierC.cpp" "AnyTypeCode\ValueModifierA.cpp" "AnyTypeCode\VisibilityA.cpp" "AnyTypeCode\VisibilityC.cpp" "AnyTypeCode\WCharSeqA.cpp" "AnyTypeCode\WrongTransactionA.cpp" "AnyTypeCode\WStringSeqA.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_AnyTypeCoded.pdb"
	-@del /f/q "..\..\lib\TAO_AnyTypeCoded.dll"
	-@del /f/q "$(OUTDIR)\TAO_AnyTypeCoded.lib"
	-@del /f/q "$(OUTDIR)\TAO_AnyTypeCoded.exp"
	-@del /f/q "$(OUTDIR)\TAO_AnyTypeCoded.ilk"
	-@del /f/q "AnyTypeCode\AnySeqC.h"
	-@del /f/q "AnyTypeCode\AnySeqS.h"
	-@del /f/q "AnyTypeCode\AnySeqA.h"
	-@del /f/q "AnyTypeCode\AnySeqC.cpp"
	-@del /f/q "AnyTypeCode\AnySeqA.cpp"
	-@del /f/q "AnyTypeCode\Dynamic_ParameterC.h"
	-@del /f/q "AnyTypeCode\Dynamic_ParameterS.h"
	-@del /f/q "AnyTypeCode\Dynamic_ParameterA.h"
	-@del /f/q "AnyTypeCode\Dynamic_ParameterC.cpp"
	-@del /f/q "AnyTypeCode\Dynamic_ParameterA.cpp"
	-@del /f/q "AnyTypeCode\ValueModifierC.h"
	-@del /f/q "AnyTypeCode\ValueModifierS.h"
	-@del /f/q "AnyTypeCode\ValueModifierA.h"
	-@del /f/q "AnyTypeCode\ValueModifierC.cpp"
	-@del /f/q "AnyTypeCode\ValueModifierA.cpp"
	-@del /f/q "AnyTypeCode\VisibilityC.h"
	-@del /f/q "AnyTypeCode\VisibilityS.h"
	-@del /f/q "AnyTypeCode\VisibilityA.h"
	-@del /f/q "AnyTypeCode\VisibilityC.cpp"
	-@del /f/q "AnyTypeCode\VisibilityA.cpp"
	-@del /f/q "AnyTypeCode\DynamicC.h"
	-@del /f/q "AnyTypeCode\DynamicS.h"
	-@del /f/q "AnyTypeCode\DynamicA.h"
	-@del /f/q "AnyTypeCode\DynamicC.cpp"
	-@del /f/q "AnyTypeCode\DynamicA.cpp"
	-@del /f/q "AnyTypeCode\BoundsC.h"
	-@del /f/q "AnyTypeCode\BoundsS.h"
	-@del /f/q "AnyTypeCode\BoundsA.h"
	-@del /f/q "AnyTypeCode\BoundsC.cpp"
	-@del /f/q "AnyTypeCode\BoundsA.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\AnyTypeCode\$(NULL)" mkdir "Debug\AnyTypeCode"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_ANYTYPECODE_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\TAO_AnyTypeCoded.pdb" /machine:IA64 /out:"..\..\lib\TAO_AnyTypeCoded.dll" /implib:"$(OUTDIR)\TAO_AnyTypeCoded.lib"
LINK32_OBJS= \
	"$(INTDIR)\AnyTypeCode\TAO_AnyTypeCode.res" \
	"$(INTDIR)\AnyTypeCode\ServicesA.obj" \
	"$(INTDIR)\AnyTypeCode\Alias_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\Any.obj" \
	"$(INTDIR)\AnyTypeCode\AnySeqA.obj" \
	"$(INTDIR)\AnyTypeCode\AnySeqC.obj" \
	"$(INTDIR)\AnyTypeCode\Any_Basic_Impl.obj" \
	"$(INTDIR)\AnyTypeCode\Any_Impl.obj" \
	"$(INTDIR)\AnyTypeCode\Any_SystemException.obj" \
	"$(INTDIR)\AnyTypeCode\Any_Unknown_IDL_Type.obj" \
	"$(INTDIR)\AnyTypeCode\AnyTypeCode_Adapter_Impl.obj" \
	"$(INTDIR)\AnyTypeCode\append.obj" \
	"$(INTDIR)\AnyTypeCode\BasicTypeTraits.obj" \
	"$(INTDIR)\AnyTypeCode\BooleanSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\BoundsA.obj" \
	"$(INTDIR)\AnyTypeCode\BoundsC.obj" \
	"$(INTDIR)\AnyTypeCode\CharSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\CONV_FRAMEA.obj" \
	"$(INTDIR)\AnyTypeCode\CurrentA.obj" \
	"$(INTDIR)\AnyTypeCode\DoubleSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\DynamicA.obj" \
	"$(INTDIR)\AnyTypeCode\DynamicC.obj" \
	"$(INTDIR)\AnyTypeCode\Dynamic_ParameterA.obj" \
	"$(INTDIR)\AnyTypeCode\Dynamic_ParameterC.obj" \
	"$(INTDIR)\AnyTypeCode\Empty_Param_TypeCode.obj" \
	"$(INTDIR)\AnyTypeCode\Enum_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\ExceptionA.obj" \
	"$(INTDIR)\AnyTypeCode\FloatSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\GIOPA.obj" \
	"$(INTDIR)\AnyTypeCode\IIOPA.obj" \
	"$(INTDIR)\AnyTypeCode\IOPA.obj" \
	"$(INTDIR)\AnyTypeCode\IIOP_EndpointsA.obj" \
	"$(INTDIR)\AnyTypeCode\LongDoubleSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\LongLongSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\LongSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\Marshal.obj" \
	"$(INTDIR)\AnyTypeCode\Messaging_PolicyValueA.obj" \
	"$(INTDIR)\AnyTypeCode\NVList.obj" \
	"$(INTDIR)\AnyTypeCode\NVList_Adapter_Impl.obj" \
	"$(INTDIR)\AnyTypeCode\ObjectIdListA.obj" \
	"$(INTDIR)\AnyTypeCode\Objref_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\OctetSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\orb_typesA.obj" \
	"$(INTDIR)\AnyTypeCode\ParameterModeA.obj" \
	"$(INTDIR)\AnyTypeCode\PI_ForwardA.obj" \
	"$(INTDIR)\AnyTypeCode\PolicyA.obj" \
	"$(INTDIR)\AnyTypeCode\Policy_CurrentA.obj" \
	"$(INTDIR)\AnyTypeCode\Policy_ForwardA.obj" \
	"$(INTDIR)\AnyTypeCode\Policy_ManagerA.obj" \
	"$(INTDIR)\AnyTypeCode\PortableInterceptorA.obj" \
	"$(INTDIR)\AnyTypeCode\Sequence_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\ShortSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\skip.obj" \
	"$(INTDIR)\AnyTypeCode\StringSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\String_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\Struct_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\SystemExceptionA.obj" \
	"$(INTDIR)\AnyTypeCode\TAOA.obj" \
	"$(INTDIR)\AnyTypeCode\TimeBaseA.obj" \
	"$(INTDIR)\AnyTypeCode\True_RefCount_Policy.obj" \
	"$(INTDIR)\AnyTypeCode\TypeCode.obj" \
	"$(INTDIR)\AnyTypeCode\TypeCodeA.obj" \
	"$(INTDIR)\AnyTypeCode\TypeCode_CDR_Extraction.obj" \
	"$(INTDIR)\AnyTypeCode\TypeCode_Constants.obj" \
	"$(INTDIR)\AnyTypeCode\ULongLongSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\ULongSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\Union_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\UShortSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\Value_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\ValueModifierC.obj" \
	"$(INTDIR)\AnyTypeCode\ValueModifierA.obj" \
	"$(INTDIR)\AnyTypeCode\VisibilityA.obj" \
	"$(INTDIR)\AnyTypeCode\VisibilityC.obj" \
	"$(INTDIR)\AnyTypeCode\WCharSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\WrongTransactionA.obj" \
	"$(INTDIR)\AnyTypeCode\WStringSeqA.obj"

"..\..\lib\TAO_AnyTypeCoded.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_AnyTypeCoded.dll.manifest" mt.exe -manifest "..\..\lib\TAO_AnyTypeCoded.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\AnyTypeCode\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_AnyTypeCode.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_ANYTYPECODE_BUILD_DLL -f "Makefile.AnyTypeCode.dep" "AnyTypeCode\ServicesA.cpp" "AnyTypeCode\Alias_TypeCode_Static.cpp" "AnyTypeCode\Any.cpp" "AnyTypeCode\AnySeqA.cpp" "AnyTypeCode\AnySeqC.cpp" "AnyTypeCode\Any_Basic_Impl.cpp" "AnyTypeCode\Any_Impl.cpp" "AnyTypeCode\Any_SystemException.cpp" "AnyTypeCode\Any_Unknown_IDL_Type.cpp" "AnyTypeCode\AnyTypeCode_Adapter_Impl.cpp" "AnyTypeCode\append.cpp" "AnyTypeCode\BasicTypeTraits.cpp" "AnyTypeCode\BooleanSeqA.cpp" "AnyTypeCode\BoundsA.cpp" "AnyTypeCode\BoundsC.cpp" "AnyTypeCode\CharSeqA.cpp" "AnyTypeCode\CONV_FRAMEA.cpp" "AnyTypeCode\CurrentA.cpp" "AnyTypeCode\DoubleSeqA.cpp" "AnyTypeCode\DynamicA.cpp" "AnyTypeCode\DynamicC.cpp" "AnyTypeCode\Dynamic_ParameterA.cpp" "AnyTypeCode\Dynamic_ParameterC.cpp" "AnyTypeCode\Empty_Param_TypeCode.cpp" "AnyTypeCode\Enum_TypeCode_Static.cpp" "AnyTypeCode\ExceptionA.cpp" "AnyTypeCode\FloatSeqA.cpp" "AnyTypeCode\GIOPA.cpp" "AnyTypeCode\IIOPA.cpp" "AnyTypeCode\IOPA.cpp" "AnyTypeCode\IIOP_EndpointsA.cpp" "AnyTypeCode\LongDoubleSeqA.cpp" "AnyTypeCode\LongLongSeqA.cpp" "AnyTypeCode\LongSeqA.cpp" "AnyTypeCode\Marshal.cpp" "AnyTypeCode\Messaging_PolicyValueA.cpp" "AnyTypeCode\NVList.cpp" "AnyTypeCode\NVList_Adapter_Impl.cpp" "AnyTypeCode\ObjectIdListA.cpp" "AnyTypeCode\Objref_TypeCode_Static.cpp" "AnyTypeCode\OctetSeqA.cpp" "AnyTypeCode\orb_typesA.cpp" "AnyTypeCode\ParameterModeA.cpp" "AnyTypeCode\PI_ForwardA.cpp" "AnyTypeCode\PolicyA.cpp" "AnyTypeCode\Policy_CurrentA.cpp" "AnyTypeCode\Policy_ForwardA.cpp" "AnyTypeCode\Policy_ManagerA.cpp" "AnyTypeCode\PortableInterceptorA.cpp" "AnyTypeCode\Sequence_TypeCode_Static.cpp" "AnyTypeCode\ShortSeqA.cpp" "AnyTypeCode\skip.cpp" "AnyTypeCode\StringSeqA.cpp" "AnyTypeCode\String_TypeCode_Static.cpp" "AnyTypeCode\Struct_TypeCode_Static.cpp" "AnyTypeCode\SystemExceptionA.cpp" "AnyTypeCode\TAOA.cpp" "AnyTypeCode\TimeBaseA.cpp" "AnyTypeCode\True_RefCount_Policy.cpp" "AnyTypeCode\TypeCode.cpp" "AnyTypeCode\TypeCodeA.cpp" "AnyTypeCode\TypeCode_CDR_Extraction.cpp" "AnyTypeCode\TypeCode_Constants.cpp" "AnyTypeCode\ULongLongSeqA.cpp" "AnyTypeCode\ULongSeqA.cpp" "AnyTypeCode\Union_TypeCode_Static.cpp" "AnyTypeCode\UShortSeqA.cpp" "AnyTypeCode\Value_TypeCode_Static.cpp" "AnyTypeCode\ValueModifierC.cpp" "AnyTypeCode\ValueModifierA.cpp" "AnyTypeCode\VisibilityA.cpp" "AnyTypeCode\VisibilityC.cpp" "AnyTypeCode\WCharSeqA.cpp" "AnyTypeCode\WrongTransactionA.cpp" "AnyTypeCode\WStringSeqA.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\TAO_AnyTypeCode.dll"
	-@del /f/q "$(OUTDIR)\TAO_AnyTypeCode.lib"
	-@del /f/q "$(OUTDIR)\TAO_AnyTypeCode.exp"
	-@del /f/q "$(OUTDIR)\TAO_AnyTypeCode.ilk"
	-@del /f/q "AnyTypeCode\AnySeqC.h"
	-@del /f/q "AnyTypeCode\AnySeqS.h"
	-@del /f/q "AnyTypeCode\AnySeqA.h"
	-@del /f/q "AnyTypeCode\AnySeqC.cpp"
	-@del /f/q "AnyTypeCode\AnySeqA.cpp"
	-@del /f/q "AnyTypeCode\Dynamic_ParameterC.h"
	-@del /f/q "AnyTypeCode\Dynamic_ParameterS.h"
	-@del /f/q "AnyTypeCode\Dynamic_ParameterA.h"
	-@del /f/q "AnyTypeCode\Dynamic_ParameterC.cpp"
	-@del /f/q "AnyTypeCode\Dynamic_ParameterA.cpp"
	-@del /f/q "AnyTypeCode\ValueModifierC.h"
	-@del /f/q "AnyTypeCode\ValueModifierS.h"
	-@del /f/q "AnyTypeCode\ValueModifierA.h"
	-@del /f/q "AnyTypeCode\ValueModifierC.cpp"
	-@del /f/q "AnyTypeCode\ValueModifierA.cpp"
	-@del /f/q "AnyTypeCode\VisibilityC.h"
	-@del /f/q "AnyTypeCode\VisibilityS.h"
	-@del /f/q "AnyTypeCode\VisibilityA.h"
	-@del /f/q "AnyTypeCode\VisibilityC.cpp"
	-@del /f/q "AnyTypeCode\VisibilityA.cpp"
	-@del /f/q "AnyTypeCode\DynamicC.h"
	-@del /f/q "AnyTypeCode\DynamicS.h"
	-@del /f/q "AnyTypeCode\DynamicA.h"
	-@del /f/q "AnyTypeCode\DynamicC.cpp"
	-@del /f/q "AnyTypeCode\DynamicA.cpp"
	-@del /f/q "AnyTypeCode\BoundsC.h"
	-@del /f/q "AnyTypeCode\BoundsS.h"
	-@del /f/q "AnyTypeCode\BoundsA.h"
	-@del /f/q "AnyTypeCode\BoundsC.cpp"
	-@del /f/q "AnyTypeCode\BoundsA.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\AnyTypeCode\$(NULL)" mkdir "Release\AnyTypeCode"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_ANYTYPECODE_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\TAO_AnyTypeCode.dll" /implib:"$(OUTDIR)\TAO_AnyTypeCode.lib"
LINK32_OBJS= \
	"$(INTDIR)\AnyTypeCode\TAO_AnyTypeCode.res" \
	"$(INTDIR)\AnyTypeCode\ServicesA.obj" \
	"$(INTDIR)\AnyTypeCode\Alias_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\Any.obj" \
	"$(INTDIR)\AnyTypeCode\AnySeqA.obj" \
	"$(INTDIR)\AnyTypeCode\AnySeqC.obj" \
	"$(INTDIR)\AnyTypeCode\Any_Basic_Impl.obj" \
	"$(INTDIR)\AnyTypeCode\Any_Impl.obj" \
	"$(INTDIR)\AnyTypeCode\Any_SystemException.obj" \
	"$(INTDIR)\AnyTypeCode\Any_Unknown_IDL_Type.obj" \
	"$(INTDIR)\AnyTypeCode\AnyTypeCode_Adapter_Impl.obj" \
	"$(INTDIR)\AnyTypeCode\append.obj" \
	"$(INTDIR)\AnyTypeCode\BasicTypeTraits.obj" \
	"$(INTDIR)\AnyTypeCode\BooleanSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\BoundsA.obj" \
	"$(INTDIR)\AnyTypeCode\BoundsC.obj" \
	"$(INTDIR)\AnyTypeCode\CharSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\CONV_FRAMEA.obj" \
	"$(INTDIR)\AnyTypeCode\CurrentA.obj" \
	"$(INTDIR)\AnyTypeCode\DoubleSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\DynamicA.obj" \
	"$(INTDIR)\AnyTypeCode\DynamicC.obj" \
	"$(INTDIR)\AnyTypeCode\Dynamic_ParameterA.obj" \
	"$(INTDIR)\AnyTypeCode\Dynamic_ParameterC.obj" \
	"$(INTDIR)\AnyTypeCode\Empty_Param_TypeCode.obj" \
	"$(INTDIR)\AnyTypeCode\Enum_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\ExceptionA.obj" \
	"$(INTDIR)\AnyTypeCode\FloatSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\GIOPA.obj" \
	"$(INTDIR)\AnyTypeCode\IIOPA.obj" \
	"$(INTDIR)\AnyTypeCode\IOPA.obj" \
	"$(INTDIR)\AnyTypeCode\IIOP_EndpointsA.obj" \
	"$(INTDIR)\AnyTypeCode\LongDoubleSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\LongLongSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\LongSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\Marshal.obj" \
	"$(INTDIR)\AnyTypeCode\Messaging_PolicyValueA.obj" \
	"$(INTDIR)\AnyTypeCode\NVList.obj" \
	"$(INTDIR)\AnyTypeCode\NVList_Adapter_Impl.obj" \
	"$(INTDIR)\AnyTypeCode\ObjectIdListA.obj" \
	"$(INTDIR)\AnyTypeCode\Objref_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\OctetSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\orb_typesA.obj" \
	"$(INTDIR)\AnyTypeCode\ParameterModeA.obj" \
	"$(INTDIR)\AnyTypeCode\PI_ForwardA.obj" \
	"$(INTDIR)\AnyTypeCode\PolicyA.obj" \
	"$(INTDIR)\AnyTypeCode\Policy_CurrentA.obj" \
	"$(INTDIR)\AnyTypeCode\Policy_ForwardA.obj" \
	"$(INTDIR)\AnyTypeCode\Policy_ManagerA.obj" \
	"$(INTDIR)\AnyTypeCode\PortableInterceptorA.obj" \
	"$(INTDIR)\AnyTypeCode\Sequence_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\ShortSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\skip.obj" \
	"$(INTDIR)\AnyTypeCode\StringSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\String_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\Struct_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\SystemExceptionA.obj" \
	"$(INTDIR)\AnyTypeCode\TAOA.obj" \
	"$(INTDIR)\AnyTypeCode\TimeBaseA.obj" \
	"$(INTDIR)\AnyTypeCode\True_RefCount_Policy.obj" \
	"$(INTDIR)\AnyTypeCode\TypeCode.obj" \
	"$(INTDIR)\AnyTypeCode\TypeCodeA.obj" \
	"$(INTDIR)\AnyTypeCode\TypeCode_CDR_Extraction.obj" \
	"$(INTDIR)\AnyTypeCode\TypeCode_Constants.obj" \
	"$(INTDIR)\AnyTypeCode\ULongLongSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\ULongSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\Union_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\UShortSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\Value_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\ValueModifierC.obj" \
	"$(INTDIR)\AnyTypeCode\ValueModifierA.obj" \
	"$(INTDIR)\AnyTypeCode\VisibilityA.obj" \
	"$(INTDIR)\AnyTypeCode\VisibilityC.obj" \
	"$(INTDIR)\AnyTypeCode\WCharSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\WrongTransactionA.obj" \
	"$(INTDIR)\AnyTypeCode\WStringSeqA.obj"

"..\..\lib\TAO_AnyTypeCode.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_AnyTypeCode.dll.manifest" mt.exe -manifest "..\..\lib\TAO_AnyTypeCode.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\AnyTypeCode\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_AnyTypeCodesd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.AnyTypeCode.dep" "AnyTypeCode\ServicesA.cpp" "AnyTypeCode\Alias_TypeCode_Static.cpp" "AnyTypeCode\Any.cpp" "AnyTypeCode\AnySeqA.cpp" "AnyTypeCode\AnySeqC.cpp" "AnyTypeCode\Any_Basic_Impl.cpp" "AnyTypeCode\Any_Impl.cpp" "AnyTypeCode\Any_SystemException.cpp" "AnyTypeCode\Any_Unknown_IDL_Type.cpp" "AnyTypeCode\AnyTypeCode_Adapter_Impl.cpp" "AnyTypeCode\append.cpp" "AnyTypeCode\BasicTypeTraits.cpp" "AnyTypeCode\BooleanSeqA.cpp" "AnyTypeCode\BoundsA.cpp" "AnyTypeCode\BoundsC.cpp" "AnyTypeCode\CharSeqA.cpp" "AnyTypeCode\CONV_FRAMEA.cpp" "AnyTypeCode\CurrentA.cpp" "AnyTypeCode\DoubleSeqA.cpp" "AnyTypeCode\DynamicA.cpp" "AnyTypeCode\DynamicC.cpp" "AnyTypeCode\Dynamic_ParameterA.cpp" "AnyTypeCode\Dynamic_ParameterC.cpp" "AnyTypeCode\Empty_Param_TypeCode.cpp" "AnyTypeCode\Enum_TypeCode_Static.cpp" "AnyTypeCode\ExceptionA.cpp" "AnyTypeCode\FloatSeqA.cpp" "AnyTypeCode\GIOPA.cpp" "AnyTypeCode\IIOPA.cpp" "AnyTypeCode\IOPA.cpp" "AnyTypeCode\IIOP_EndpointsA.cpp" "AnyTypeCode\LongDoubleSeqA.cpp" "AnyTypeCode\LongLongSeqA.cpp" "AnyTypeCode\LongSeqA.cpp" "AnyTypeCode\Marshal.cpp" "AnyTypeCode\Messaging_PolicyValueA.cpp" "AnyTypeCode\NVList.cpp" "AnyTypeCode\NVList_Adapter_Impl.cpp" "AnyTypeCode\ObjectIdListA.cpp" "AnyTypeCode\Objref_TypeCode_Static.cpp" "AnyTypeCode\OctetSeqA.cpp" "AnyTypeCode\orb_typesA.cpp" "AnyTypeCode\ParameterModeA.cpp" "AnyTypeCode\PI_ForwardA.cpp" "AnyTypeCode\PolicyA.cpp" "AnyTypeCode\Policy_CurrentA.cpp" "AnyTypeCode\Policy_ForwardA.cpp" "AnyTypeCode\Policy_ManagerA.cpp" "AnyTypeCode\PortableInterceptorA.cpp" "AnyTypeCode\Sequence_TypeCode_Static.cpp" "AnyTypeCode\ShortSeqA.cpp" "AnyTypeCode\skip.cpp" "AnyTypeCode\StringSeqA.cpp" "AnyTypeCode\String_TypeCode_Static.cpp" "AnyTypeCode\Struct_TypeCode_Static.cpp" "AnyTypeCode\SystemExceptionA.cpp" "AnyTypeCode\TAOA.cpp" "AnyTypeCode\TimeBaseA.cpp" "AnyTypeCode\True_RefCount_Policy.cpp" "AnyTypeCode\TypeCode.cpp" "AnyTypeCode\TypeCodeA.cpp" "AnyTypeCode\TypeCode_CDR_Extraction.cpp" "AnyTypeCode\TypeCode_Constants.cpp" "AnyTypeCode\ULongLongSeqA.cpp" "AnyTypeCode\ULongSeqA.cpp" "AnyTypeCode\Union_TypeCode_Static.cpp" "AnyTypeCode\UShortSeqA.cpp" "AnyTypeCode\Value_TypeCode_Static.cpp" "AnyTypeCode\ValueModifierC.cpp" "AnyTypeCode\ValueModifierA.cpp" "AnyTypeCode\VisibilityA.cpp" "AnyTypeCode\VisibilityC.cpp" "AnyTypeCode\WCharSeqA.cpp" "AnyTypeCode\WrongTransactionA.cpp" "AnyTypeCode\WStringSeqA.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_AnyTypeCodesd.lib"
	-@del /f/q "$(OUTDIR)\TAO_AnyTypeCodesd.exp"
	-@del /f/q "$(OUTDIR)\TAO_AnyTypeCodesd.ilk"
	-@del /f/q "..\..\lib\TAO_AnyTypeCodesd.pdb"
	-@del /f/q "AnyTypeCode\AnySeqC.h"
	-@del /f/q "AnyTypeCode\AnySeqS.h"
	-@del /f/q "AnyTypeCode\AnySeqA.h"
	-@del /f/q "AnyTypeCode\AnySeqC.cpp"
	-@del /f/q "AnyTypeCode\AnySeqA.cpp"
	-@del /f/q "AnyTypeCode\Dynamic_ParameterC.h"
	-@del /f/q "AnyTypeCode\Dynamic_ParameterS.h"
	-@del /f/q "AnyTypeCode\Dynamic_ParameterA.h"
	-@del /f/q "AnyTypeCode\Dynamic_ParameterC.cpp"
	-@del /f/q "AnyTypeCode\Dynamic_ParameterA.cpp"
	-@del /f/q "AnyTypeCode\ValueModifierC.h"
	-@del /f/q "AnyTypeCode\ValueModifierS.h"
	-@del /f/q "AnyTypeCode\ValueModifierA.h"
	-@del /f/q "AnyTypeCode\ValueModifierC.cpp"
	-@del /f/q "AnyTypeCode\ValueModifierA.cpp"
	-@del /f/q "AnyTypeCode\VisibilityC.h"
	-@del /f/q "AnyTypeCode\VisibilityS.h"
	-@del /f/q "AnyTypeCode\VisibilityA.h"
	-@del /f/q "AnyTypeCode\VisibilityC.cpp"
	-@del /f/q "AnyTypeCode\VisibilityA.cpp"
	-@del /f/q "AnyTypeCode\DynamicC.h"
	-@del /f/q "AnyTypeCode\DynamicS.h"
	-@del /f/q "AnyTypeCode\DynamicA.h"
	-@del /f/q "AnyTypeCode\DynamicC.cpp"
	-@del /f/q "AnyTypeCode\DynamicA.cpp"
	-@del /f/q "AnyTypeCode\BoundsC.h"
	-@del /f/q "AnyTypeCode\BoundsS.h"
	-@del /f/q "AnyTypeCode\BoundsA.h"
	-@del /f/q "AnyTypeCode\BoundsC.cpp"
	-@del /f/q "AnyTypeCode\BoundsA.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\AnyTypeCode\$(NULL)" mkdir "Static_Debug\AnyTypeCode"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"..\..\lib\TAO_AnyTypeCodesd.pdb" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_AnyTypeCodesd.lib"
LINK32_OBJS= \
	"$(INTDIR)\AnyTypeCode\ServicesA.obj" \
	"$(INTDIR)\AnyTypeCode\Alias_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\Any.obj" \
	"$(INTDIR)\AnyTypeCode\AnySeqA.obj" \
	"$(INTDIR)\AnyTypeCode\AnySeqC.obj" \
	"$(INTDIR)\AnyTypeCode\Any_Basic_Impl.obj" \
	"$(INTDIR)\AnyTypeCode\Any_Impl.obj" \
	"$(INTDIR)\AnyTypeCode\Any_SystemException.obj" \
	"$(INTDIR)\AnyTypeCode\Any_Unknown_IDL_Type.obj" \
	"$(INTDIR)\AnyTypeCode\AnyTypeCode_Adapter_Impl.obj" \
	"$(INTDIR)\AnyTypeCode\append.obj" \
	"$(INTDIR)\AnyTypeCode\BasicTypeTraits.obj" \
	"$(INTDIR)\AnyTypeCode\BooleanSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\BoundsA.obj" \
	"$(INTDIR)\AnyTypeCode\BoundsC.obj" \
	"$(INTDIR)\AnyTypeCode\CharSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\CONV_FRAMEA.obj" \
	"$(INTDIR)\AnyTypeCode\CurrentA.obj" \
	"$(INTDIR)\AnyTypeCode\DoubleSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\DynamicA.obj" \
	"$(INTDIR)\AnyTypeCode\DynamicC.obj" \
	"$(INTDIR)\AnyTypeCode\Dynamic_ParameterA.obj" \
	"$(INTDIR)\AnyTypeCode\Dynamic_ParameterC.obj" \
	"$(INTDIR)\AnyTypeCode\Empty_Param_TypeCode.obj" \
	"$(INTDIR)\AnyTypeCode\Enum_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\ExceptionA.obj" \
	"$(INTDIR)\AnyTypeCode\FloatSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\GIOPA.obj" \
	"$(INTDIR)\AnyTypeCode\IIOPA.obj" \
	"$(INTDIR)\AnyTypeCode\IOPA.obj" \
	"$(INTDIR)\AnyTypeCode\IIOP_EndpointsA.obj" \
	"$(INTDIR)\AnyTypeCode\LongDoubleSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\LongLongSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\LongSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\Marshal.obj" \
	"$(INTDIR)\AnyTypeCode\Messaging_PolicyValueA.obj" \
	"$(INTDIR)\AnyTypeCode\NVList.obj" \
	"$(INTDIR)\AnyTypeCode\NVList_Adapter_Impl.obj" \
	"$(INTDIR)\AnyTypeCode\ObjectIdListA.obj" \
	"$(INTDIR)\AnyTypeCode\Objref_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\OctetSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\orb_typesA.obj" \
	"$(INTDIR)\AnyTypeCode\ParameterModeA.obj" \
	"$(INTDIR)\AnyTypeCode\PI_ForwardA.obj" \
	"$(INTDIR)\AnyTypeCode\PolicyA.obj" \
	"$(INTDIR)\AnyTypeCode\Policy_CurrentA.obj" \
	"$(INTDIR)\AnyTypeCode\Policy_ForwardA.obj" \
	"$(INTDIR)\AnyTypeCode\Policy_ManagerA.obj" \
	"$(INTDIR)\AnyTypeCode\PortableInterceptorA.obj" \
	"$(INTDIR)\AnyTypeCode\Sequence_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\ShortSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\skip.obj" \
	"$(INTDIR)\AnyTypeCode\StringSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\String_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\Struct_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\SystemExceptionA.obj" \
	"$(INTDIR)\AnyTypeCode\TAOA.obj" \
	"$(INTDIR)\AnyTypeCode\TimeBaseA.obj" \
	"$(INTDIR)\AnyTypeCode\True_RefCount_Policy.obj" \
	"$(INTDIR)\AnyTypeCode\TypeCode.obj" \
	"$(INTDIR)\AnyTypeCode\TypeCodeA.obj" \
	"$(INTDIR)\AnyTypeCode\TypeCode_CDR_Extraction.obj" \
	"$(INTDIR)\AnyTypeCode\TypeCode_Constants.obj" \
	"$(INTDIR)\AnyTypeCode\ULongLongSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\ULongSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\Union_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\UShortSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\Value_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\ValueModifierC.obj" \
	"$(INTDIR)\AnyTypeCode\ValueModifierA.obj" \
	"$(INTDIR)\AnyTypeCode\VisibilityA.obj" \
	"$(INTDIR)\AnyTypeCode\VisibilityC.obj" \
	"$(INTDIR)\AnyTypeCode\WCharSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\WrongTransactionA.obj" \
	"$(INTDIR)\AnyTypeCode\WStringSeqA.obj"

"$(OUTDIR)\TAO_AnyTypeCodesd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_AnyTypeCodesd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_AnyTypeCodesd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\AnyTypeCode\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_AnyTypeCodes.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.AnyTypeCode.dep" "AnyTypeCode\ServicesA.cpp" "AnyTypeCode\Alias_TypeCode_Static.cpp" "AnyTypeCode\Any.cpp" "AnyTypeCode\AnySeqA.cpp" "AnyTypeCode\AnySeqC.cpp" "AnyTypeCode\Any_Basic_Impl.cpp" "AnyTypeCode\Any_Impl.cpp" "AnyTypeCode\Any_SystemException.cpp" "AnyTypeCode\Any_Unknown_IDL_Type.cpp" "AnyTypeCode\AnyTypeCode_Adapter_Impl.cpp" "AnyTypeCode\append.cpp" "AnyTypeCode\BasicTypeTraits.cpp" "AnyTypeCode\BooleanSeqA.cpp" "AnyTypeCode\BoundsA.cpp" "AnyTypeCode\BoundsC.cpp" "AnyTypeCode\CharSeqA.cpp" "AnyTypeCode\CONV_FRAMEA.cpp" "AnyTypeCode\CurrentA.cpp" "AnyTypeCode\DoubleSeqA.cpp" "AnyTypeCode\DynamicA.cpp" "AnyTypeCode\DynamicC.cpp" "AnyTypeCode\Dynamic_ParameterA.cpp" "AnyTypeCode\Dynamic_ParameterC.cpp" "AnyTypeCode\Empty_Param_TypeCode.cpp" "AnyTypeCode\Enum_TypeCode_Static.cpp" "AnyTypeCode\ExceptionA.cpp" "AnyTypeCode\FloatSeqA.cpp" "AnyTypeCode\GIOPA.cpp" "AnyTypeCode\IIOPA.cpp" "AnyTypeCode\IOPA.cpp" "AnyTypeCode\IIOP_EndpointsA.cpp" "AnyTypeCode\LongDoubleSeqA.cpp" "AnyTypeCode\LongLongSeqA.cpp" "AnyTypeCode\LongSeqA.cpp" "AnyTypeCode\Marshal.cpp" "AnyTypeCode\Messaging_PolicyValueA.cpp" "AnyTypeCode\NVList.cpp" "AnyTypeCode\NVList_Adapter_Impl.cpp" "AnyTypeCode\ObjectIdListA.cpp" "AnyTypeCode\Objref_TypeCode_Static.cpp" "AnyTypeCode\OctetSeqA.cpp" "AnyTypeCode\orb_typesA.cpp" "AnyTypeCode\ParameterModeA.cpp" "AnyTypeCode\PI_ForwardA.cpp" "AnyTypeCode\PolicyA.cpp" "AnyTypeCode\Policy_CurrentA.cpp" "AnyTypeCode\Policy_ForwardA.cpp" "AnyTypeCode\Policy_ManagerA.cpp" "AnyTypeCode\PortableInterceptorA.cpp" "AnyTypeCode\Sequence_TypeCode_Static.cpp" "AnyTypeCode\ShortSeqA.cpp" "AnyTypeCode\skip.cpp" "AnyTypeCode\StringSeqA.cpp" "AnyTypeCode\String_TypeCode_Static.cpp" "AnyTypeCode\Struct_TypeCode_Static.cpp" "AnyTypeCode\SystemExceptionA.cpp" "AnyTypeCode\TAOA.cpp" "AnyTypeCode\TimeBaseA.cpp" "AnyTypeCode\True_RefCount_Policy.cpp" "AnyTypeCode\TypeCode.cpp" "AnyTypeCode\TypeCodeA.cpp" "AnyTypeCode\TypeCode_CDR_Extraction.cpp" "AnyTypeCode\TypeCode_Constants.cpp" "AnyTypeCode\ULongLongSeqA.cpp" "AnyTypeCode\ULongSeqA.cpp" "AnyTypeCode\Union_TypeCode_Static.cpp" "AnyTypeCode\UShortSeqA.cpp" "AnyTypeCode\Value_TypeCode_Static.cpp" "AnyTypeCode\ValueModifierC.cpp" "AnyTypeCode\ValueModifierA.cpp" "AnyTypeCode\VisibilityA.cpp" "AnyTypeCode\VisibilityC.cpp" "AnyTypeCode\WCharSeqA.cpp" "AnyTypeCode\WrongTransactionA.cpp" "AnyTypeCode\WStringSeqA.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_AnyTypeCodes.lib"
	-@del /f/q "$(OUTDIR)\TAO_AnyTypeCodes.exp"
	-@del /f/q "$(OUTDIR)\TAO_AnyTypeCodes.ilk"
	-@del /f/q "AnyTypeCode\AnySeqC.h"
	-@del /f/q "AnyTypeCode\AnySeqS.h"
	-@del /f/q "AnyTypeCode\AnySeqA.h"
	-@del /f/q "AnyTypeCode\AnySeqC.cpp"
	-@del /f/q "AnyTypeCode\AnySeqA.cpp"
	-@del /f/q "AnyTypeCode\Dynamic_ParameterC.h"
	-@del /f/q "AnyTypeCode\Dynamic_ParameterS.h"
	-@del /f/q "AnyTypeCode\Dynamic_ParameterA.h"
	-@del /f/q "AnyTypeCode\Dynamic_ParameterC.cpp"
	-@del /f/q "AnyTypeCode\Dynamic_ParameterA.cpp"
	-@del /f/q "AnyTypeCode\ValueModifierC.h"
	-@del /f/q "AnyTypeCode\ValueModifierS.h"
	-@del /f/q "AnyTypeCode\ValueModifierA.h"
	-@del /f/q "AnyTypeCode\ValueModifierC.cpp"
	-@del /f/q "AnyTypeCode\ValueModifierA.cpp"
	-@del /f/q "AnyTypeCode\VisibilityC.h"
	-@del /f/q "AnyTypeCode\VisibilityS.h"
	-@del /f/q "AnyTypeCode\VisibilityA.h"
	-@del /f/q "AnyTypeCode\VisibilityC.cpp"
	-@del /f/q "AnyTypeCode\VisibilityA.cpp"
	-@del /f/q "AnyTypeCode\DynamicC.h"
	-@del /f/q "AnyTypeCode\DynamicS.h"
	-@del /f/q "AnyTypeCode\DynamicA.h"
	-@del /f/q "AnyTypeCode\DynamicC.cpp"
	-@del /f/q "AnyTypeCode\DynamicA.cpp"
	-@del /f/q "AnyTypeCode\BoundsC.h"
	-@del /f/q "AnyTypeCode\BoundsS.h"
	-@del /f/q "AnyTypeCode\BoundsA.h"
	-@del /f/q "AnyTypeCode\BoundsC.cpp"
	-@del /f/q "AnyTypeCode\BoundsA.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\AnyTypeCode\$(NULL)" mkdir "Static_Release\AnyTypeCode"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_AnyTypeCodes.lib"
LINK32_OBJS= \
	"$(INTDIR)\AnyTypeCode\ServicesA.obj" \
	"$(INTDIR)\AnyTypeCode\Alias_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\Any.obj" \
	"$(INTDIR)\AnyTypeCode\AnySeqA.obj" \
	"$(INTDIR)\AnyTypeCode\AnySeqC.obj" \
	"$(INTDIR)\AnyTypeCode\Any_Basic_Impl.obj" \
	"$(INTDIR)\AnyTypeCode\Any_Impl.obj" \
	"$(INTDIR)\AnyTypeCode\Any_SystemException.obj" \
	"$(INTDIR)\AnyTypeCode\Any_Unknown_IDL_Type.obj" \
	"$(INTDIR)\AnyTypeCode\AnyTypeCode_Adapter_Impl.obj" \
	"$(INTDIR)\AnyTypeCode\append.obj" \
	"$(INTDIR)\AnyTypeCode\BasicTypeTraits.obj" \
	"$(INTDIR)\AnyTypeCode\BooleanSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\BoundsA.obj" \
	"$(INTDIR)\AnyTypeCode\BoundsC.obj" \
	"$(INTDIR)\AnyTypeCode\CharSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\CONV_FRAMEA.obj" \
	"$(INTDIR)\AnyTypeCode\CurrentA.obj" \
	"$(INTDIR)\AnyTypeCode\DoubleSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\DynamicA.obj" \
	"$(INTDIR)\AnyTypeCode\DynamicC.obj" \
	"$(INTDIR)\AnyTypeCode\Dynamic_ParameterA.obj" \
	"$(INTDIR)\AnyTypeCode\Dynamic_ParameterC.obj" \
	"$(INTDIR)\AnyTypeCode\Empty_Param_TypeCode.obj" \
	"$(INTDIR)\AnyTypeCode\Enum_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\ExceptionA.obj" \
	"$(INTDIR)\AnyTypeCode\FloatSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\GIOPA.obj" \
	"$(INTDIR)\AnyTypeCode\IIOPA.obj" \
	"$(INTDIR)\AnyTypeCode\IOPA.obj" \
	"$(INTDIR)\AnyTypeCode\IIOP_EndpointsA.obj" \
	"$(INTDIR)\AnyTypeCode\LongDoubleSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\LongLongSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\LongSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\Marshal.obj" \
	"$(INTDIR)\AnyTypeCode\Messaging_PolicyValueA.obj" \
	"$(INTDIR)\AnyTypeCode\NVList.obj" \
	"$(INTDIR)\AnyTypeCode\NVList_Adapter_Impl.obj" \
	"$(INTDIR)\AnyTypeCode\ObjectIdListA.obj" \
	"$(INTDIR)\AnyTypeCode\Objref_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\OctetSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\orb_typesA.obj" \
	"$(INTDIR)\AnyTypeCode\ParameterModeA.obj" \
	"$(INTDIR)\AnyTypeCode\PI_ForwardA.obj" \
	"$(INTDIR)\AnyTypeCode\PolicyA.obj" \
	"$(INTDIR)\AnyTypeCode\Policy_CurrentA.obj" \
	"$(INTDIR)\AnyTypeCode\Policy_ForwardA.obj" \
	"$(INTDIR)\AnyTypeCode\Policy_ManagerA.obj" \
	"$(INTDIR)\AnyTypeCode\PortableInterceptorA.obj" \
	"$(INTDIR)\AnyTypeCode\Sequence_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\ShortSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\skip.obj" \
	"$(INTDIR)\AnyTypeCode\StringSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\String_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\Struct_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\SystemExceptionA.obj" \
	"$(INTDIR)\AnyTypeCode\TAOA.obj" \
	"$(INTDIR)\AnyTypeCode\TimeBaseA.obj" \
	"$(INTDIR)\AnyTypeCode\True_RefCount_Policy.obj" \
	"$(INTDIR)\AnyTypeCode\TypeCode.obj" \
	"$(INTDIR)\AnyTypeCode\TypeCodeA.obj" \
	"$(INTDIR)\AnyTypeCode\TypeCode_CDR_Extraction.obj" \
	"$(INTDIR)\AnyTypeCode\TypeCode_Constants.obj" \
	"$(INTDIR)\AnyTypeCode\ULongLongSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\ULongSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\Union_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\UShortSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\Value_TypeCode_Static.obj" \
	"$(INTDIR)\AnyTypeCode\ValueModifierC.obj" \
	"$(INTDIR)\AnyTypeCode\ValueModifierA.obj" \
	"$(INTDIR)\AnyTypeCode\VisibilityA.obj" \
	"$(INTDIR)\AnyTypeCode\VisibilityC.obj" \
	"$(INTDIR)\AnyTypeCode\WCharSeqA.obj" \
	"$(INTDIR)\AnyTypeCode\WrongTransactionA.obj" \
	"$(INTDIR)\AnyTypeCode\WStringSeqA.obj"

"$(OUTDIR)\TAO_AnyTypeCodes.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_AnyTypeCodes.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_AnyTypeCodes.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.AnyTypeCode.dep")
!INCLUDE "Makefile.AnyTypeCode.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="AnyTypeCode\ServicesA.cpp"

"$(INTDIR)\AnyTypeCode\ServicesA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\ServicesA.obj" $(SOURCE)

SOURCE="AnyTypeCode\Alias_TypeCode_Static.cpp"

"$(INTDIR)\AnyTypeCode\Alias_TypeCode_Static.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\Alias_TypeCode_Static.obj" $(SOURCE)

SOURCE="AnyTypeCode\Any.cpp"

"$(INTDIR)\AnyTypeCode\Any.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\Any.obj" $(SOURCE)

SOURCE="AnyTypeCode\AnySeqA.cpp"

"$(INTDIR)\AnyTypeCode\AnySeqA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\AnySeqA.obj" $(SOURCE)

SOURCE="AnyTypeCode\AnySeqC.cpp"

"$(INTDIR)\AnyTypeCode\AnySeqC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\AnySeqC.obj" $(SOURCE)

SOURCE="AnyTypeCode\Any_Basic_Impl.cpp"

"$(INTDIR)\AnyTypeCode\Any_Basic_Impl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\Any_Basic_Impl.obj" $(SOURCE)

SOURCE="AnyTypeCode\Any_Impl.cpp"

"$(INTDIR)\AnyTypeCode\Any_Impl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\Any_Impl.obj" $(SOURCE)

SOURCE="AnyTypeCode\Any_SystemException.cpp"

"$(INTDIR)\AnyTypeCode\Any_SystemException.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\Any_SystemException.obj" $(SOURCE)

SOURCE="AnyTypeCode\Any_Unknown_IDL_Type.cpp"

"$(INTDIR)\AnyTypeCode\Any_Unknown_IDL_Type.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\Any_Unknown_IDL_Type.obj" $(SOURCE)

SOURCE="AnyTypeCode\AnyTypeCode_Adapter_Impl.cpp"

"$(INTDIR)\AnyTypeCode\AnyTypeCode_Adapter_Impl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\AnyTypeCode_Adapter_Impl.obj" $(SOURCE)

SOURCE="AnyTypeCode\append.cpp"

"$(INTDIR)\AnyTypeCode\append.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\append.obj" $(SOURCE)

SOURCE="AnyTypeCode\BasicTypeTraits.cpp"

"$(INTDIR)\AnyTypeCode\BasicTypeTraits.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\BasicTypeTraits.obj" $(SOURCE)

SOURCE="AnyTypeCode\BooleanSeqA.cpp"

"$(INTDIR)\AnyTypeCode\BooleanSeqA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\BooleanSeqA.obj" $(SOURCE)

SOURCE="AnyTypeCode\BoundsA.cpp"

"$(INTDIR)\AnyTypeCode\BoundsA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\BoundsA.obj" $(SOURCE)

SOURCE="AnyTypeCode\BoundsC.cpp"

"$(INTDIR)\AnyTypeCode\BoundsC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\BoundsC.obj" $(SOURCE)

SOURCE="AnyTypeCode\CharSeqA.cpp"

"$(INTDIR)\AnyTypeCode\CharSeqA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\CharSeqA.obj" $(SOURCE)

SOURCE="AnyTypeCode\CONV_FRAMEA.cpp"

"$(INTDIR)\AnyTypeCode\CONV_FRAMEA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\CONV_FRAMEA.obj" $(SOURCE)

SOURCE="AnyTypeCode\CurrentA.cpp"

"$(INTDIR)\AnyTypeCode\CurrentA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\CurrentA.obj" $(SOURCE)

SOURCE="AnyTypeCode\DoubleSeqA.cpp"

"$(INTDIR)\AnyTypeCode\DoubleSeqA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\DoubleSeqA.obj" $(SOURCE)

SOURCE="AnyTypeCode\DynamicA.cpp"

"$(INTDIR)\AnyTypeCode\DynamicA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\DynamicA.obj" $(SOURCE)

SOURCE="AnyTypeCode\DynamicC.cpp"

"$(INTDIR)\AnyTypeCode\DynamicC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\DynamicC.obj" $(SOURCE)

SOURCE="AnyTypeCode\Dynamic_ParameterA.cpp"

"$(INTDIR)\AnyTypeCode\Dynamic_ParameterA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\Dynamic_ParameterA.obj" $(SOURCE)

SOURCE="AnyTypeCode\Dynamic_ParameterC.cpp"

"$(INTDIR)\AnyTypeCode\Dynamic_ParameterC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\Dynamic_ParameterC.obj" $(SOURCE)

SOURCE="AnyTypeCode\Empty_Param_TypeCode.cpp"

"$(INTDIR)\AnyTypeCode\Empty_Param_TypeCode.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\Empty_Param_TypeCode.obj" $(SOURCE)

SOURCE="AnyTypeCode\Enum_TypeCode_Static.cpp"

"$(INTDIR)\AnyTypeCode\Enum_TypeCode_Static.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\Enum_TypeCode_Static.obj" $(SOURCE)

SOURCE="AnyTypeCode\ExceptionA.cpp"

"$(INTDIR)\AnyTypeCode\ExceptionA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\ExceptionA.obj" $(SOURCE)

SOURCE="AnyTypeCode\FloatSeqA.cpp"

"$(INTDIR)\AnyTypeCode\FloatSeqA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\FloatSeqA.obj" $(SOURCE)

SOURCE="AnyTypeCode\GIOPA.cpp"

"$(INTDIR)\AnyTypeCode\GIOPA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\GIOPA.obj" $(SOURCE)

SOURCE="AnyTypeCode\IIOPA.cpp"

"$(INTDIR)\AnyTypeCode\IIOPA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\IIOPA.obj" $(SOURCE)

SOURCE="AnyTypeCode\IOPA.cpp"

"$(INTDIR)\AnyTypeCode\IOPA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\IOPA.obj" $(SOURCE)

SOURCE="AnyTypeCode\IIOP_EndpointsA.cpp"

"$(INTDIR)\AnyTypeCode\IIOP_EndpointsA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\IIOP_EndpointsA.obj" $(SOURCE)

SOURCE="AnyTypeCode\LongDoubleSeqA.cpp"

"$(INTDIR)\AnyTypeCode\LongDoubleSeqA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\LongDoubleSeqA.obj" $(SOURCE)

SOURCE="AnyTypeCode\LongLongSeqA.cpp"

"$(INTDIR)\AnyTypeCode\LongLongSeqA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\LongLongSeqA.obj" $(SOURCE)

SOURCE="AnyTypeCode\LongSeqA.cpp"

"$(INTDIR)\AnyTypeCode\LongSeqA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\LongSeqA.obj" $(SOURCE)

SOURCE="AnyTypeCode\Marshal.cpp"

"$(INTDIR)\AnyTypeCode\Marshal.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\Marshal.obj" $(SOURCE)

SOURCE="AnyTypeCode\Messaging_PolicyValueA.cpp"

"$(INTDIR)\AnyTypeCode\Messaging_PolicyValueA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\Messaging_PolicyValueA.obj" $(SOURCE)

SOURCE="AnyTypeCode\NVList.cpp"

"$(INTDIR)\AnyTypeCode\NVList.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\NVList.obj" $(SOURCE)

SOURCE="AnyTypeCode\NVList_Adapter_Impl.cpp"

"$(INTDIR)\AnyTypeCode\NVList_Adapter_Impl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\NVList_Adapter_Impl.obj" $(SOURCE)

SOURCE="AnyTypeCode\ObjectIdListA.cpp"

"$(INTDIR)\AnyTypeCode\ObjectIdListA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\ObjectIdListA.obj" $(SOURCE)

SOURCE="AnyTypeCode\Objref_TypeCode_Static.cpp"

"$(INTDIR)\AnyTypeCode\Objref_TypeCode_Static.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\Objref_TypeCode_Static.obj" $(SOURCE)

SOURCE="AnyTypeCode\OctetSeqA.cpp"

"$(INTDIR)\AnyTypeCode\OctetSeqA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\OctetSeqA.obj" $(SOURCE)

SOURCE="AnyTypeCode\orb_typesA.cpp"

"$(INTDIR)\AnyTypeCode\orb_typesA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\orb_typesA.obj" $(SOURCE)

SOURCE="AnyTypeCode\ParameterModeA.cpp"

"$(INTDIR)\AnyTypeCode\ParameterModeA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\ParameterModeA.obj" $(SOURCE)

SOURCE="AnyTypeCode\PI_ForwardA.cpp"

"$(INTDIR)\AnyTypeCode\PI_ForwardA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\PI_ForwardA.obj" $(SOURCE)

SOURCE="AnyTypeCode\PolicyA.cpp"

"$(INTDIR)\AnyTypeCode\PolicyA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\PolicyA.obj" $(SOURCE)

SOURCE="AnyTypeCode\Policy_CurrentA.cpp"

"$(INTDIR)\AnyTypeCode\Policy_CurrentA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\Policy_CurrentA.obj" $(SOURCE)

SOURCE="AnyTypeCode\Policy_ForwardA.cpp"

"$(INTDIR)\AnyTypeCode\Policy_ForwardA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\Policy_ForwardA.obj" $(SOURCE)

SOURCE="AnyTypeCode\Policy_ManagerA.cpp"

"$(INTDIR)\AnyTypeCode\Policy_ManagerA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\Policy_ManagerA.obj" $(SOURCE)

SOURCE="AnyTypeCode\PortableInterceptorA.cpp"

"$(INTDIR)\AnyTypeCode\PortableInterceptorA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\PortableInterceptorA.obj" $(SOURCE)

SOURCE="AnyTypeCode\Sequence_TypeCode_Static.cpp"

"$(INTDIR)\AnyTypeCode\Sequence_TypeCode_Static.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\Sequence_TypeCode_Static.obj" $(SOURCE)

SOURCE="AnyTypeCode\ShortSeqA.cpp"

"$(INTDIR)\AnyTypeCode\ShortSeqA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\ShortSeqA.obj" $(SOURCE)

SOURCE="AnyTypeCode\skip.cpp"

"$(INTDIR)\AnyTypeCode\skip.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\skip.obj" $(SOURCE)

SOURCE="AnyTypeCode\StringSeqA.cpp"

"$(INTDIR)\AnyTypeCode\StringSeqA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\StringSeqA.obj" $(SOURCE)

SOURCE="AnyTypeCode\String_TypeCode_Static.cpp"

"$(INTDIR)\AnyTypeCode\String_TypeCode_Static.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\String_TypeCode_Static.obj" $(SOURCE)

SOURCE="AnyTypeCode\Struct_TypeCode_Static.cpp"

"$(INTDIR)\AnyTypeCode\Struct_TypeCode_Static.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\Struct_TypeCode_Static.obj" $(SOURCE)

SOURCE="AnyTypeCode\SystemExceptionA.cpp"

"$(INTDIR)\AnyTypeCode\SystemExceptionA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\SystemExceptionA.obj" $(SOURCE)

SOURCE="AnyTypeCode\TAOA.cpp"

"$(INTDIR)\AnyTypeCode\TAOA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\TAOA.obj" $(SOURCE)

SOURCE="AnyTypeCode\TimeBaseA.cpp"

"$(INTDIR)\AnyTypeCode\TimeBaseA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\TimeBaseA.obj" $(SOURCE)

SOURCE="AnyTypeCode\True_RefCount_Policy.cpp"

"$(INTDIR)\AnyTypeCode\True_RefCount_Policy.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\True_RefCount_Policy.obj" $(SOURCE)

SOURCE="AnyTypeCode\TypeCode.cpp"

"$(INTDIR)\AnyTypeCode\TypeCode.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\TypeCode.obj" $(SOURCE)

SOURCE="AnyTypeCode\TypeCodeA.cpp"

"$(INTDIR)\AnyTypeCode\TypeCodeA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\TypeCodeA.obj" $(SOURCE)

SOURCE="AnyTypeCode\TypeCode_CDR_Extraction.cpp"

"$(INTDIR)\AnyTypeCode\TypeCode_CDR_Extraction.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\TypeCode_CDR_Extraction.obj" $(SOURCE)

SOURCE="AnyTypeCode\TypeCode_Constants.cpp"

"$(INTDIR)\AnyTypeCode\TypeCode_Constants.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\TypeCode_Constants.obj" $(SOURCE)

SOURCE="AnyTypeCode\ULongLongSeqA.cpp"

"$(INTDIR)\AnyTypeCode\ULongLongSeqA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\ULongLongSeqA.obj" $(SOURCE)

SOURCE="AnyTypeCode\ULongSeqA.cpp"

"$(INTDIR)\AnyTypeCode\ULongSeqA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\ULongSeqA.obj" $(SOURCE)

SOURCE="AnyTypeCode\Union_TypeCode_Static.cpp"

"$(INTDIR)\AnyTypeCode\Union_TypeCode_Static.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\Union_TypeCode_Static.obj" $(SOURCE)

SOURCE="AnyTypeCode\UShortSeqA.cpp"

"$(INTDIR)\AnyTypeCode\UShortSeqA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\UShortSeqA.obj" $(SOURCE)

SOURCE="AnyTypeCode\Value_TypeCode_Static.cpp"

"$(INTDIR)\AnyTypeCode\Value_TypeCode_Static.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\Value_TypeCode_Static.obj" $(SOURCE)

SOURCE="AnyTypeCode\ValueModifierC.cpp"

"$(INTDIR)\AnyTypeCode\ValueModifierC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\ValueModifierC.obj" $(SOURCE)

SOURCE="AnyTypeCode\ValueModifierA.cpp"

"$(INTDIR)\AnyTypeCode\ValueModifierA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\ValueModifierA.obj" $(SOURCE)

SOURCE="AnyTypeCode\VisibilityA.cpp"

"$(INTDIR)\AnyTypeCode\VisibilityA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\VisibilityA.obj" $(SOURCE)

SOURCE="AnyTypeCode\VisibilityC.cpp"

"$(INTDIR)\AnyTypeCode\VisibilityC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\VisibilityC.obj" $(SOURCE)

SOURCE="AnyTypeCode\WCharSeqA.cpp"

"$(INTDIR)\AnyTypeCode\WCharSeqA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\WCharSeqA.obj" $(SOURCE)

SOURCE="AnyTypeCode\WrongTransactionA.cpp"

"$(INTDIR)\AnyTypeCode\WrongTransactionA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\WrongTransactionA.obj" $(SOURCE)

SOURCE="AnyTypeCode\WStringSeqA.cpp"

"$(INTDIR)\AnyTypeCode\WStringSeqA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AnyTypeCode\WStringSeqA.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="AnyTypeCode\AnySeq.pidl"

InputPath=AnyTypeCode\AnySeq.pidl

"AnyTypeCode\AnySeqC.h" "AnyTypeCode\AnySeqS.h" "AnyTypeCode\AnySeqA.h" "AnyTypeCode\AnySeqC.cpp" "AnyTypeCode\AnySeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-AnyTypeCode_AnySeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -GA -Wb,export_macro=TAO_AnyTypeCode_Export -Wb,export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -o AnyTypeCode -Sorb "$(InputPath)"
<<

SOURCE="AnyTypeCode\Dynamic_Parameter.pidl"

InputPath=AnyTypeCode\Dynamic_Parameter.pidl

"AnyTypeCode\Dynamic_ParameterC.h" "AnyTypeCode\Dynamic_ParameterS.h" "AnyTypeCode\Dynamic_ParameterA.h" "AnyTypeCode\Dynamic_ParameterC.cpp" "AnyTypeCode\Dynamic_ParameterA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-AnyTypeCode_Dynamic_Parameter_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -GA -Wb,export_macro=TAO_AnyTypeCode_Export -Wb,export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -o AnyTypeCode -Sorb "$(InputPath)"
<<

SOURCE="AnyTypeCode\ValueModifier.pidl"

InputPath=AnyTypeCode\ValueModifier.pidl

"AnyTypeCode\ValueModifierC.h" "AnyTypeCode\ValueModifierS.h" "AnyTypeCode\ValueModifierA.h" "AnyTypeCode\ValueModifierC.cpp" "AnyTypeCode\ValueModifierA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-AnyTypeCode_ValueModifier_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -GA -Wb,export_macro=TAO_AnyTypeCode_Export -Wb,export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -o AnyTypeCode -Sorb "$(InputPath)"
<<

SOURCE="AnyTypeCode\Visibility.pidl"

InputPath=AnyTypeCode\Visibility.pidl

"AnyTypeCode\VisibilityC.h" "AnyTypeCode\VisibilityS.h" "AnyTypeCode\VisibilityA.h" "AnyTypeCode\VisibilityC.cpp" "AnyTypeCode\VisibilityA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-AnyTypeCode_Visibility_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -GA -Wb,export_macro=TAO_AnyTypeCode_Export -Wb,export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -o AnyTypeCode -Sorb "$(InputPath)"
<<

SOURCE="AnyTypeCode\Dynamic.pidl"

InputPath=AnyTypeCode\Dynamic.pidl

"AnyTypeCode\DynamicC.h" "AnyTypeCode\DynamicS.h" "AnyTypeCode\DynamicA.h" "AnyTypeCode\DynamicC.cpp" "AnyTypeCode\DynamicA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-AnyTypeCode_Dynamic_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -GA -Wb,export_macro=TAO_AnyTypeCode_Export -Wb,export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -o AnyTypeCode -Sorb "$(InputPath)"
<<

SOURCE="AnyTypeCode\Bounds.pidl"

InputPath=AnyTypeCode\Bounds.pidl

"AnyTypeCode\BoundsC.h" "AnyTypeCode\BoundsS.h" "AnyTypeCode\BoundsA.h" "AnyTypeCode\BoundsC.cpp" "AnyTypeCode\BoundsA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-AnyTypeCode_Bounds_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -GA -Wb,export_macro=TAO_AnyTypeCode_Export -Wb,export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -o AnyTypeCode "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="AnyTypeCode\AnySeq.pidl"

InputPath=AnyTypeCode\AnySeq.pidl

"AnyTypeCode\AnySeqC.h" "AnyTypeCode\AnySeqS.h" "AnyTypeCode\AnySeqA.h" "AnyTypeCode\AnySeqC.cpp" "AnyTypeCode\AnySeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-AnyTypeCode_AnySeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -GA -Wb,export_macro=TAO_AnyTypeCode_Export -Wb,export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -o AnyTypeCode -Sorb "$(InputPath)"
<<

SOURCE="AnyTypeCode\Dynamic_Parameter.pidl"

InputPath=AnyTypeCode\Dynamic_Parameter.pidl

"AnyTypeCode\Dynamic_ParameterC.h" "AnyTypeCode\Dynamic_ParameterS.h" "AnyTypeCode\Dynamic_ParameterA.h" "AnyTypeCode\Dynamic_ParameterC.cpp" "AnyTypeCode\Dynamic_ParameterA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-AnyTypeCode_Dynamic_Parameter_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -GA -Wb,export_macro=TAO_AnyTypeCode_Export -Wb,export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -o AnyTypeCode -Sorb "$(InputPath)"
<<

SOURCE="AnyTypeCode\ValueModifier.pidl"

InputPath=AnyTypeCode\ValueModifier.pidl

"AnyTypeCode\ValueModifierC.h" "AnyTypeCode\ValueModifierS.h" "AnyTypeCode\ValueModifierA.h" "AnyTypeCode\ValueModifierC.cpp" "AnyTypeCode\ValueModifierA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-AnyTypeCode_ValueModifier_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -GA -Wb,export_macro=TAO_AnyTypeCode_Export -Wb,export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -o AnyTypeCode -Sorb "$(InputPath)"
<<

SOURCE="AnyTypeCode\Visibility.pidl"

InputPath=AnyTypeCode\Visibility.pidl

"AnyTypeCode\VisibilityC.h" "AnyTypeCode\VisibilityS.h" "AnyTypeCode\VisibilityA.h" "AnyTypeCode\VisibilityC.cpp" "AnyTypeCode\VisibilityA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-AnyTypeCode_Visibility_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -GA -Wb,export_macro=TAO_AnyTypeCode_Export -Wb,export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -o AnyTypeCode -Sorb "$(InputPath)"
<<

SOURCE="AnyTypeCode\Dynamic.pidl"

InputPath=AnyTypeCode\Dynamic.pidl

"AnyTypeCode\DynamicC.h" "AnyTypeCode\DynamicS.h" "AnyTypeCode\DynamicA.h" "AnyTypeCode\DynamicC.cpp" "AnyTypeCode\DynamicA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-AnyTypeCode_Dynamic_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -GA -Wb,export_macro=TAO_AnyTypeCode_Export -Wb,export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -o AnyTypeCode -Sorb "$(InputPath)"
<<

SOURCE="AnyTypeCode\Bounds.pidl"

InputPath=AnyTypeCode\Bounds.pidl

"AnyTypeCode\BoundsC.h" "AnyTypeCode\BoundsS.h" "AnyTypeCode\BoundsA.h" "AnyTypeCode\BoundsC.cpp" "AnyTypeCode\BoundsA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-AnyTypeCode_Bounds_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -GA -Wb,export_macro=TAO_AnyTypeCode_Export -Wb,export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -o AnyTypeCode "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="AnyTypeCode\AnySeq.pidl"

InputPath=AnyTypeCode\AnySeq.pidl

"AnyTypeCode\AnySeqC.h" "AnyTypeCode\AnySeqS.h" "AnyTypeCode\AnySeqA.h" "AnyTypeCode\AnySeqC.cpp" "AnyTypeCode\AnySeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-AnyTypeCode_AnySeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -GA -Wb,export_macro=TAO_AnyTypeCode_Export -Wb,export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -o AnyTypeCode -Sorb "$(InputPath)"
<<

SOURCE="AnyTypeCode\Dynamic_Parameter.pidl"

InputPath=AnyTypeCode\Dynamic_Parameter.pidl

"AnyTypeCode\Dynamic_ParameterC.h" "AnyTypeCode\Dynamic_ParameterS.h" "AnyTypeCode\Dynamic_ParameterA.h" "AnyTypeCode\Dynamic_ParameterC.cpp" "AnyTypeCode\Dynamic_ParameterA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-AnyTypeCode_Dynamic_Parameter_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -GA -Wb,export_macro=TAO_AnyTypeCode_Export -Wb,export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -o AnyTypeCode -Sorb "$(InputPath)"
<<

SOURCE="AnyTypeCode\ValueModifier.pidl"

InputPath=AnyTypeCode\ValueModifier.pidl

"AnyTypeCode\ValueModifierC.h" "AnyTypeCode\ValueModifierS.h" "AnyTypeCode\ValueModifierA.h" "AnyTypeCode\ValueModifierC.cpp" "AnyTypeCode\ValueModifierA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-AnyTypeCode_ValueModifier_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -GA -Wb,export_macro=TAO_AnyTypeCode_Export -Wb,export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -o AnyTypeCode -Sorb "$(InputPath)"
<<

SOURCE="AnyTypeCode\Visibility.pidl"

InputPath=AnyTypeCode\Visibility.pidl

"AnyTypeCode\VisibilityC.h" "AnyTypeCode\VisibilityS.h" "AnyTypeCode\VisibilityA.h" "AnyTypeCode\VisibilityC.cpp" "AnyTypeCode\VisibilityA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-AnyTypeCode_Visibility_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -GA -Wb,export_macro=TAO_AnyTypeCode_Export -Wb,export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -o AnyTypeCode -Sorb "$(InputPath)"
<<

SOURCE="AnyTypeCode\Dynamic.pidl"

InputPath=AnyTypeCode\Dynamic.pidl

"AnyTypeCode\DynamicC.h" "AnyTypeCode\DynamicS.h" "AnyTypeCode\DynamicA.h" "AnyTypeCode\DynamicC.cpp" "AnyTypeCode\DynamicA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-AnyTypeCode_Dynamic_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -GA -Wb,export_macro=TAO_AnyTypeCode_Export -Wb,export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -o AnyTypeCode -Sorb "$(InputPath)"
<<

SOURCE="AnyTypeCode\Bounds.pidl"

InputPath=AnyTypeCode\Bounds.pidl

"AnyTypeCode\BoundsC.h" "AnyTypeCode\BoundsS.h" "AnyTypeCode\BoundsA.h" "AnyTypeCode\BoundsC.cpp" "AnyTypeCode\BoundsA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-AnyTypeCode_Bounds_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -GA -Wb,export_macro=TAO_AnyTypeCode_Export -Wb,export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -o AnyTypeCode "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="AnyTypeCode\AnySeq.pidl"

InputPath=AnyTypeCode\AnySeq.pidl

"AnyTypeCode\AnySeqC.h" "AnyTypeCode\AnySeqS.h" "AnyTypeCode\AnySeqA.h" "AnyTypeCode\AnySeqC.cpp" "AnyTypeCode\AnySeqA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-AnyTypeCode_AnySeq_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -GA -Wb,export_macro=TAO_AnyTypeCode_Export -Wb,export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -o AnyTypeCode -Sorb "$(InputPath)"
<<

SOURCE="AnyTypeCode\Dynamic_Parameter.pidl"

InputPath=AnyTypeCode\Dynamic_Parameter.pidl

"AnyTypeCode\Dynamic_ParameterC.h" "AnyTypeCode\Dynamic_ParameterS.h" "AnyTypeCode\Dynamic_ParameterA.h" "AnyTypeCode\Dynamic_ParameterC.cpp" "AnyTypeCode\Dynamic_ParameterA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-AnyTypeCode_Dynamic_Parameter_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -GA -Wb,export_macro=TAO_AnyTypeCode_Export -Wb,export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -o AnyTypeCode -Sorb "$(InputPath)"
<<

SOURCE="AnyTypeCode\ValueModifier.pidl"

InputPath=AnyTypeCode\ValueModifier.pidl

"AnyTypeCode\ValueModifierC.h" "AnyTypeCode\ValueModifierS.h" "AnyTypeCode\ValueModifierA.h" "AnyTypeCode\ValueModifierC.cpp" "AnyTypeCode\ValueModifierA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-AnyTypeCode_ValueModifier_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -GA -Wb,export_macro=TAO_AnyTypeCode_Export -Wb,export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -o AnyTypeCode -Sorb "$(InputPath)"
<<

SOURCE="AnyTypeCode\Visibility.pidl"

InputPath=AnyTypeCode\Visibility.pidl

"AnyTypeCode\VisibilityC.h" "AnyTypeCode\VisibilityS.h" "AnyTypeCode\VisibilityA.h" "AnyTypeCode\VisibilityC.cpp" "AnyTypeCode\VisibilityA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-AnyTypeCode_Visibility_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -GA -Wb,export_macro=TAO_AnyTypeCode_Export -Wb,export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -o AnyTypeCode -Sorb "$(InputPath)"
<<

SOURCE="AnyTypeCode\Dynamic.pidl"

InputPath=AnyTypeCode\Dynamic.pidl

"AnyTypeCode\DynamicC.h" "AnyTypeCode\DynamicS.h" "AnyTypeCode\DynamicA.h" "AnyTypeCode\DynamicC.cpp" "AnyTypeCode\DynamicA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-AnyTypeCode_Dynamic_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -GA -Wb,export_macro=TAO_AnyTypeCode_Export -Wb,export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -o AnyTypeCode -Sorb "$(InputPath)"
<<

SOURCE="AnyTypeCode\Bounds.pidl"

InputPath=AnyTypeCode\Bounds.pidl

"AnyTypeCode\BoundsC.h" "AnyTypeCode\BoundsS.h" "AnyTypeCode\BoundsA.h" "AnyTypeCode\BoundsC.cpp" "AnyTypeCode\BoundsA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-AnyTypeCode_Bounds_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -GA -Wb,export_macro=TAO_AnyTypeCode_Export -Wb,export_include=tao/AnyTypeCode/TAO_AnyTypeCode_Export.h -o AnyTypeCode "$(InputPath)"
<<

!ENDIF

SOURCE="AnyTypeCode\TAO_AnyTypeCode.rc"

"$(INTDIR)\AnyTypeCode\TAO_AnyTypeCode.res" : $(SOURCE)
	@if not exist "$(INTDIR)\AnyTypeCode\$(NULL)" mkdir "$(INTDIR)\AnyTypeCode\"
	$(RSC) /l 0x409 /fo"$(INTDIR)\AnyTypeCode\TAO_AnyTypeCode.res" /d NDEBUG /d WIN64 /d _CRT_SECURE_NO_WARNINGS /d _CRT_SECURE_NO_DEPRECATE /d _CRT_NONSTDC_NO_DEPRECATE /i "..\.." /i ".." $(SOURCE)



!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.AnyTypeCode.dep")
	@echo Using "Makefile.AnyTypeCode.dep"
!ELSE
	@echo Warning: cannot find "Makefile.AnyTypeCode.dep"
!ENDIF
!ENDIF

