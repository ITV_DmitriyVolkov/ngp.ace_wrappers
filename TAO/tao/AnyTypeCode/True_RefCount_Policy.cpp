// $Id: True_RefCount_Policy.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/AnyTypeCode/True_RefCount_Policy.h"

#ifndef  __ACE_INLINE__
# include  "True_RefCount_Policy.inl"
#endif  /* !__ACE_INLINE__ */


ACE_RCSID (AnyTypeCode,
           True_RefCount_Policy,
           "$Id: True_RefCount_Policy.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO::True_RefCount_Policy::~True_RefCount_Policy (void)
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
