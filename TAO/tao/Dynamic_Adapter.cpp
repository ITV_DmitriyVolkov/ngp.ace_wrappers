// $Id: Dynamic_Adapter.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/Dynamic_Adapter.h"

ACE_RCSID (tao,
           Dynamic_Adapter,
           "$Id: Dynamic_Adapter.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Dynamic_Adapter::~TAO_Dynamic_Adapter (void)
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
