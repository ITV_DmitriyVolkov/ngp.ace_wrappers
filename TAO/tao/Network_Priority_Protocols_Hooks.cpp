// $Id: Network_Priority_Protocols_Hooks.cpp 979 2008-12-31 20:22:32Z mitza $

#include "tao/Network_Priority_Protocols_Hooks.h"

ACE_RCSID (tao,
           Network_Priority_Protocols_Hooks,
           "$Id: Network_Priority_Protocols_Hooks.cpp 979 2008-12-31 20:22:32Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Network_Priority_Protocols_Hooks::
~TAO_Network_Priority_Protocols_Hooks (void)
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
