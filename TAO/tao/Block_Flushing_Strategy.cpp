// $Id: Block_Flushing_Strategy.cpp 1219 2009-06-26 20:24:08Z mitza $

#include "tao/Block_Flushing_Strategy.h"
#include "tao/Transport.h"
#include "tao/Queued_Message.h"
#include "tao/Connection_Handler.h"
#include "ace/Countdown_Time.h"

ACE_RCSID(tao, Block_Flushing_Strategy, "$Id: Block_Flushing_Strategy.cpp 1219 2009-06-26 20:24:08Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

int
TAO_Block_Flushing_Strategy::schedule_output (TAO_Transport *)
{
  return MUST_FLUSH;
}

int
TAO_Block_Flushing_Strategy::cancel_output (TAO_Transport *)
{
  return 0;
}

int
TAO_Block_Flushing_Strategy::call_handle_output (TAO_Transport *transport,
                                                 ACE_Time_Value *timeout)
{
  switch (transport->handle_output (timeout).dre_)
    {
    case TAO_Transport::DR_ERROR: return -1;
    case TAO_Transport::DR_QUEUE_EMPTY: // won't happen, fall-through anyway
    case TAO_Transport::DR_OK: return 0;
    case TAO_Transport::DR_WOULDBLOCK:
      {
        ACE_Countdown_Time counter (timeout);
        TAO_Connection_Handler &ch = *transport->connection_handler ();
        if (ch.handle_write_ready (timeout) == -1)
          {
            return -1;
          }
        return 0;
      }
    }
  return 0;
}

int
TAO_Block_Flushing_Strategy::flush_message (TAO_Transport *transport,
                                            TAO_Queued_Message *msg,
                                            ACE_Time_Value *max_wait_time)
{
  while (!msg->all_data_sent ())
    {
      if (this->call_handle_output (transport, max_wait_time) == -1)
        return -1;
    }
  return 0;
}

int
TAO_Block_Flushing_Strategy::flush_transport (TAO_Transport *transport
                                              , ACE_Time_Value *max_wait_time)
{
  while (!transport->queue_is_empty ())
    {
      if (this->call_handle_output (transport, max_wait_time) == -1)
        return -1;
    }
  return 0;
}

TAO_END_VERSIONED_NAMESPACE_DECL
