# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.PI.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "PI\PI_includeC.h" "PI\PI_includeS.h" "PI\PI_includeA.h" "PI\PI_includeC.cpp" "PI\ORBInitInfoC.h" "PI\ORBInitInfoS.h" "PI\ORBInitInfoA.h" "PI\ORBInitInfoC.cpp" "PI\ORBInitializerC.h" "PI\ORBInitializerS.h" "PI\ORBInitializerA.h" "PI\ORBInitializerC.cpp" "PI\ORBInitializerA.cpp" "PI\ClientRequestInterceptorC.h" "PI\ClientRequestInterceptorS.h" "PI\ClientRequestInterceptorA.h" "PI\ClientRequestInterceptorC.cpp" "PI\ClientRequestInterceptorA.cpp" "PI\ClientRequestInfoC.h" "PI\ClientRequestInfoS.h" "PI\ClientRequestInfoA.h" "PI\ClientRequestInfoC.cpp" "PI\ClientRequestInfoA.cpp" "PI\RequestInfoC.h" "PI\RequestInfoS.h" "PI\RequestInfoA.h" "PI\RequestInfoC.cpp" "PI\RequestInfoA.cpp" "PI\PIForwardRequestC.h" "PI\PIForwardRequestS.h" "PI\PIForwardRequestA.h" "PI\PIForwardRequestC.cpp" "PI\PIForwardRequestA.cpp" "PI\PICurrentC.h" "PI\PICurrentS.h" "PI\PICurrentA.h" "PI\PICurrentC.cpp" "PI\PICurrentA.cpp" "PI\ProcessingModePolicyC.h" "PI\ProcessingModePolicyS.h" "PI\ProcessingModePolicyA.h" "PI\ProcessingModePolicyC.cpp" "PI\ProcessingModePolicyA.cpp" "PI\InterceptorC.h" "PI\InterceptorS.h" "PI\InterceptorA.h" "PI\InterceptorC.cpp" "PI\InterceptorA.cpp" "PI\InvalidSlotC.h" "PI\InvalidSlotS.h" "PI\InvalidSlotA.h" "PI\InvalidSlotC.cpp" "PI\InvalidSlotA.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\PI\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_PId.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_PI_BUILD_DLL -f "Makefile.PI.dep" "PI\PI_includeC.cpp" "PI\ORBInitInfoC.cpp" "PI\ORBInitializerC.cpp" "PI\ORBInitializerA.cpp" "PI\ClientRequestInterceptorC.cpp" "PI\ClientRequestInterceptorA.cpp" "PI\ClientRequestInfoC.cpp" "PI\ClientRequestInfoA.cpp" "PI\RequestInfoC.cpp" "PI\RequestInfoA.cpp" "PI\PIForwardRequestC.cpp" "PI\PIForwardRequestA.cpp" "PI\PICurrentC.cpp" "PI\PICurrentA.cpp" "PI\ProcessingModePolicyC.cpp" "PI\ProcessingModePolicyA.cpp" "PI\InterceptorC.cpp" "PI\InterceptorA.cpp" "PI\InvalidSlotC.cpp" "PI\InvalidSlotA.cpp" "PI\PI.cpp" "PI\PI_PolicyFactory.cpp" "PI\PolicyFactoryC.cpp" "PI\PICurrent.cpp" "PI\PICurrent_Loader.cpp" "PI\ORBInitInfo.cpp" "PI\ClientRequestInterceptor_Adapter_Impl.cpp" "PI\ClientRequestDetails.cpp" "PI\PI_ORBInitializer.cpp" "PI\DLL_Resident_ORB_Initializer.cpp" "PI\PolicyFactory_Loader.cpp" "PI\ProcessingModePolicy.cpp" "PI\ClientRequestInterceptor_Factory_Impl.cpp" "PI\ORBInitializer_Registry_Impl.cpp" "PI\PolicyFactoryA.cpp" "PI\PICurrent_Impl.cpp" "PI\ClientRequestInfo.cpp" "PI\PolicyFactory_Registry.cpp" "PI\RequestInfo_Util.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_PId.pdb"
	-@del /f/q "..\..\lib\TAO_PId.dll"
	-@del /f/q "$(OUTDIR)\TAO_PId.lib"
	-@del /f/q "$(OUTDIR)\TAO_PId.exp"
	-@del /f/q "$(OUTDIR)\TAO_PId.ilk"
	-@del /f/q "PI\PI_includeC.h"
	-@del /f/q "PI\PI_includeS.h"
	-@del /f/q "PI\PI_includeA.h"
	-@del /f/q "PI\PI_includeC.cpp"
	-@del /f/q "PI\ORBInitInfoC.h"
	-@del /f/q "PI\ORBInitInfoS.h"
	-@del /f/q "PI\ORBInitInfoA.h"
	-@del /f/q "PI\ORBInitInfoC.cpp"
	-@del /f/q "PI\ORBInitializerC.h"
	-@del /f/q "PI\ORBInitializerS.h"
	-@del /f/q "PI\ORBInitializerA.h"
	-@del /f/q "PI\ORBInitializerC.cpp"
	-@del /f/q "PI\ORBInitializerA.cpp"
	-@del /f/q "PI\ClientRequestInterceptorC.h"
	-@del /f/q "PI\ClientRequestInterceptorS.h"
	-@del /f/q "PI\ClientRequestInterceptorA.h"
	-@del /f/q "PI\ClientRequestInterceptorC.cpp"
	-@del /f/q "PI\ClientRequestInterceptorA.cpp"
	-@del /f/q "PI\ClientRequestInfoC.h"
	-@del /f/q "PI\ClientRequestInfoS.h"
	-@del /f/q "PI\ClientRequestInfoA.h"
	-@del /f/q "PI\ClientRequestInfoC.cpp"
	-@del /f/q "PI\ClientRequestInfoA.cpp"
	-@del /f/q "PI\RequestInfoC.h"
	-@del /f/q "PI\RequestInfoS.h"
	-@del /f/q "PI\RequestInfoA.h"
	-@del /f/q "PI\RequestInfoC.cpp"
	-@del /f/q "PI\RequestInfoA.cpp"
	-@del /f/q "PI\PIForwardRequestC.h"
	-@del /f/q "PI\PIForwardRequestS.h"
	-@del /f/q "PI\PIForwardRequestA.h"
	-@del /f/q "PI\PIForwardRequestC.cpp"
	-@del /f/q "PI\PIForwardRequestA.cpp"
	-@del /f/q "PI\PICurrentC.h"
	-@del /f/q "PI\PICurrentS.h"
	-@del /f/q "PI\PICurrentA.h"
	-@del /f/q "PI\PICurrentC.cpp"
	-@del /f/q "PI\PICurrentA.cpp"
	-@del /f/q "PI\ProcessingModePolicyC.h"
	-@del /f/q "PI\ProcessingModePolicyS.h"
	-@del /f/q "PI\ProcessingModePolicyA.h"
	-@del /f/q "PI\ProcessingModePolicyC.cpp"
	-@del /f/q "PI\ProcessingModePolicyA.cpp"
	-@del /f/q "PI\InterceptorC.h"
	-@del /f/q "PI\InterceptorS.h"
	-@del /f/q "PI\InterceptorA.h"
	-@del /f/q "PI\InterceptorC.cpp"
	-@del /f/q "PI\InterceptorA.cpp"
	-@del /f/q "PI\InvalidSlotC.h"
	-@del /f/q "PI\InvalidSlotS.h"
	-@del /f/q "PI\InvalidSlotA.h"
	-@del /f/q "PI\InvalidSlotC.cpp"
	-@del /f/q "PI\InvalidSlotA.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\PI\$(NULL)" mkdir "Debug\PI"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_PI_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_CodecFactoryd.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\TAO_PId.pdb" /machine:IA64 /out:"..\..\lib\TAO_PId.dll" /implib:"$(OUTDIR)\TAO_PId.lib"
LINK32_OBJS= \
	"$(INTDIR)\PI\TAO_PI.res" \
	"$(INTDIR)\PI\PI_includeC.obj" \
	"$(INTDIR)\PI\ORBInitInfoC.obj" \
	"$(INTDIR)\PI\ORBInitializerC.obj" \
	"$(INTDIR)\PI\ORBInitializerA.obj" \
	"$(INTDIR)\PI\ClientRequestInterceptorC.obj" \
	"$(INTDIR)\PI\ClientRequestInterceptorA.obj" \
	"$(INTDIR)\PI\ClientRequestInfoC.obj" \
	"$(INTDIR)\PI\ClientRequestInfoA.obj" \
	"$(INTDIR)\PI\RequestInfoC.obj" \
	"$(INTDIR)\PI\RequestInfoA.obj" \
	"$(INTDIR)\PI\PIForwardRequestC.obj" \
	"$(INTDIR)\PI\PIForwardRequestA.obj" \
	"$(INTDIR)\PI\PICurrentC.obj" \
	"$(INTDIR)\PI\PICurrentA.obj" \
	"$(INTDIR)\PI\ProcessingModePolicyC.obj" \
	"$(INTDIR)\PI\ProcessingModePolicyA.obj" \
	"$(INTDIR)\PI\InterceptorC.obj" \
	"$(INTDIR)\PI\InterceptorA.obj" \
	"$(INTDIR)\PI\InvalidSlotC.obj" \
	"$(INTDIR)\PI\InvalidSlotA.obj" \
	"$(INTDIR)\PI\PI.obj" \
	"$(INTDIR)\PI\PI_PolicyFactory.obj" \
	"$(INTDIR)\PI\PolicyFactoryC.obj" \
	"$(INTDIR)\PI\PICurrent.obj" \
	"$(INTDIR)\PI\PICurrent_Loader.obj" \
	"$(INTDIR)\PI\ORBInitInfo.obj" \
	"$(INTDIR)\PI\ClientRequestInterceptor_Adapter_Impl.obj" \
	"$(INTDIR)\PI\ClientRequestDetails.obj" \
	"$(INTDIR)\PI\PI_ORBInitializer.obj" \
	"$(INTDIR)\PI\DLL_Resident_ORB_Initializer.obj" \
	"$(INTDIR)\PI\PolicyFactory_Loader.obj" \
	"$(INTDIR)\PI\ProcessingModePolicy.obj" \
	"$(INTDIR)\PI\ClientRequestInterceptor_Factory_Impl.obj" \
	"$(INTDIR)\PI\ORBInitializer_Registry_Impl.obj" \
	"$(INTDIR)\PI\PolicyFactoryA.obj" \
	"$(INTDIR)\PI\PICurrent_Impl.obj" \
	"$(INTDIR)\PI\ClientRequestInfo.obj" \
	"$(INTDIR)\PI\PolicyFactory_Registry.obj" \
	"$(INTDIR)\PI\RequestInfo_Util.obj"

"..\..\lib\TAO_PId.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_PId.dll.manifest" mt.exe -manifest "..\..\lib\TAO_PId.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\PI\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_PI.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_PI_BUILD_DLL -f "Makefile.PI.dep" "PI\PI_includeC.cpp" "PI\ORBInitInfoC.cpp" "PI\ORBInitializerC.cpp" "PI\ORBInitializerA.cpp" "PI\ClientRequestInterceptorC.cpp" "PI\ClientRequestInterceptorA.cpp" "PI\ClientRequestInfoC.cpp" "PI\ClientRequestInfoA.cpp" "PI\RequestInfoC.cpp" "PI\RequestInfoA.cpp" "PI\PIForwardRequestC.cpp" "PI\PIForwardRequestA.cpp" "PI\PICurrentC.cpp" "PI\PICurrentA.cpp" "PI\ProcessingModePolicyC.cpp" "PI\ProcessingModePolicyA.cpp" "PI\InterceptorC.cpp" "PI\InterceptorA.cpp" "PI\InvalidSlotC.cpp" "PI\InvalidSlotA.cpp" "PI\PI.cpp" "PI\PI_PolicyFactory.cpp" "PI\PolicyFactoryC.cpp" "PI\PICurrent.cpp" "PI\PICurrent_Loader.cpp" "PI\ORBInitInfo.cpp" "PI\ClientRequestInterceptor_Adapter_Impl.cpp" "PI\ClientRequestDetails.cpp" "PI\PI_ORBInitializer.cpp" "PI\DLL_Resident_ORB_Initializer.cpp" "PI\PolicyFactory_Loader.cpp" "PI\ProcessingModePolicy.cpp" "PI\ClientRequestInterceptor_Factory_Impl.cpp" "PI\ORBInitializer_Registry_Impl.cpp" "PI\PolicyFactoryA.cpp" "PI\PICurrent_Impl.cpp" "PI\ClientRequestInfo.cpp" "PI\PolicyFactory_Registry.cpp" "PI\RequestInfo_Util.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\TAO_PI.dll"
	-@del /f/q "$(OUTDIR)\TAO_PI.lib"
	-@del /f/q "$(OUTDIR)\TAO_PI.exp"
	-@del /f/q "$(OUTDIR)\TAO_PI.ilk"
	-@del /f/q "PI\PI_includeC.h"
	-@del /f/q "PI\PI_includeS.h"
	-@del /f/q "PI\PI_includeA.h"
	-@del /f/q "PI\PI_includeC.cpp"
	-@del /f/q "PI\ORBInitInfoC.h"
	-@del /f/q "PI\ORBInitInfoS.h"
	-@del /f/q "PI\ORBInitInfoA.h"
	-@del /f/q "PI\ORBInitInfoC.cpp"
	-@del /f/q "PI\ORBInitializerC.h"
	-@del /f/q "PI\ORBInitializerS.h"
	-@del /f/q "PI\ORBInitializerA.h"
	-@del /f/q "PI\ORBInitializerC.cpp"
	-@del /f/q "PI\ORBInitializerA.cpp"
	-@del /f/q "PI\ClientRequestInterceptorC.h"
	-@del /f/q "PI\ClientRequestInterceptorS.h"
	-@del /f/q "PI\ClientRequestInterceptorA.h"
	-@del /f/q "PI\ClientRequestInterceptorC.cpp"
	-@del /f/q "PI\ClientRequestInterceptorA.cpp"
	-@del /f/q "PI\ClientRequestInfoC.h"
	-@del /f/q "PI\ClientRequestInfoS.h"
	-@del /f/q "PI\ClientRequestInfoA.h"
	-@del /f/q "PI\ClientRequestInfoC.cpp"
	-@del /f/q "PI\ClientRequestInfoA.cpp"
	-@del /f/q "PI\RequestInfoC.h"
	-@del /f/q "PI\RequestInfoS.h"
	-@del /f/q "PI\RequestInfoA.h"
	-@del /f/q "PI\RequestInfoC.cpp"
	-@del /f/q "PI\RequestInfoA.cpp"
	-@del /f/q "PI\PIForwardRequestC.h"
	-@del /f/q "PI\PIForwardRequestS.h"
	-@del /f/q "PI\PIForwardRequestA.h"
	-@del /f/q "PI\PIForwardRequestC.cpp"
	-@del /f/q "PI\PIForwardRequestA.cpp"
	-@del /f/q "PI\PICurrentC.h"
	-@del /f/q "PI\PICurrentS.h"
	-@del /f/q "PI\PICurrentA.h"
	-@del /f/q "PI\PICurrentC.cpp"
	-@del /f/q "PI\PICurrentA.cpp"
	-@del /f/q "PI\ProcessingModePolicyC.h"
	-@del /f/q "PI\ProcessingModePolicyS.h"
	-@del /f/q "PI\ProcessingModePolicyA.h"
	-@del /f/q "PI\ProcessingModePolicyC.cpp"
	-@del /f/q "PI\ProcessingModePolicyA.cpp"
	-@del /f/q "PI\InterceptorC.h"
	-@del /f/q "PI\InterceptorS.h"
	-@del /f/q "PI\InterceptorA.h"
	-@del /f/q "PI\InterceptorC.cpp"
	-@del /f/q "PI\InterceptorA.cpp"
	-@del /f/q "PI\InvalidSlotC.h"
	-@del /f/q "PI\InvalidSlotS.h"
	-@del /f/q "PI\InvalidSlotA.h"
	-@del /f/q "PI\InvalidSlotC.cpp"
	-@del /f/q "PI\InvalidSlotA.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\PI\$(NULL)" mkdir "Release\PI"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_PI_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_CodecFactory.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\TAO_PI.dll" /implib:"$(OUTDIR)\TAO_PI.lib"
LINK32_OBJS= \
	"$(INTDIR)\PI\TAO_PI.res" \
	"$(INTDIR)\PI\PI_includeC.obj" \
	"$(INTDIR)\PI\ORBInitInfoC.obj" \
	"$(INTDIR)\PI\ORBInitializerC.obj" \
	"$(INTDIR)\PI\ORBInitializerA.obj" \
	"$(INTDIR)\PI\ClientRequestInterceptorC.obj" \
	"$(INTDIR)\PI\ClientRequestInterceptorA.obj" \
	"$(INTDIR)\PI\ClientRequestInfoC.obj" \
	"$(INTDIR)\PI\ClientRequestInfoA.obj" \
	"$(INTDIR)\PI\RequestInfoC.obj" \
	"$(INTDIR)\PI\RequestInfoA.obj" \
	"$(INTDIR)\PI\PIForwardRequestC.obj" \
	"$(INTDIR)\PI\PIForwardRequestA.obj" \
	"$(INTDIR)\PI\PICurrentC.obj" \
	"$(INTDIR)\PI\PICurrentA.obj" \
	"$(INTDIR)\PI\ProcessingModePolicyC.obj" \
	"$(INTDIR)\PI\ProcessingModePolicyA.obj" \
	"$(INTDIR)\PI\InterceptorC.obj" \
	"$(INTDIR)\PI\InterceptorA.obj" \
	"$(INTDIR)\PI\InvalidSlotC.obj" \
	"$(INTDIR)\PI\InvalidSlotA.obj" \
	"$(INTDIR)\PI\PI.obj" \
	"$(INTDIR)\PI\PI_PolicyFactory.obj" \
	"$(INTDIR)\PI\PolicyFactoryC.obj" \
	"$(INTDIR)\PI\PICurrent.obj" \
	"$(INTDIR)\PI\PICurrent_Loader.obj" \
	"$(INTDIR)\PI\ORBInitInfo.obj" \
	"$(INTDIR)\PI\ClientRequestInterceptor_Adapter_Impl.obj" \
	"$(INTDIR)\PI\ClientRequestDetails.obj" \
	"$(INTDIR)\PI\PI_ORBInitializer.obj" \
	"$(INTDIR)\PI\DLL_Resident_ORB_Initializer.obj" \
	"$(INTDIR)\PI\PolicyFactory_Loader.obj" \
	"$(INTDIR)\PI\ProcessingModePolicy.obj" \
	"$(INTDIR)\PI\ClientRequestInterceptor_Factory_Impl.obj" \
	"$(INTDIR)\PI\ORBInitializer_Registry_Impl.obj" \
	"$(INTDIR)\PI\PolicyFactoryA.obj" \
	"$(INTDIR)\PI\PICurrent_Impl.obj" \
	"$(INTDIR)\PI\ClientRequestInfo.obj" \
	"$(INTDIR)\PI\PolicyFactory_Registry.obj" \
	"$(INTDIR)\PI\RequestInfo_Util.obj"

"..\..\lib\TAO_PI.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_PI.dll.manifest" mt.exe -manifest "..\..\lib\TAO_PI.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\PI\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_PIsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.PI.dep" "PI\PI_includeC.cpp" "PI\ORBInitInfoC.cpp" "PI\ORBInitializerC.cpp" "PI\ORBInitializerA.cpp" "PI\ClientRequestInterceptorC.cpp" "PI\ClientRequestInterceptorA.cpp" "PI\ClientRequestInfoC.cpp" "PI\ClientRequestInfoA.cpp" "PI\RequestInfoC.cpp" "PI\RequestInfoA.cpp" "PI\PIForwardRequestC.cpp" "PI\PIForwardRequestA.cpp" "PI\PICurrentC.cpp" "PI\PICurrentA.cpp" "PI\ProcessingModePolicyC.cpp" "PI\ProcessingModePolicyA.cpp" "PI\InterceptorC.cpp" "PI\InterceptorA.cpp" "PI\InvalidSlotC.cpp" "PI\InvalidSlotA.cpp" "PI\PI.cpp" "PI\PI_PolicyFactory.cpp" "PI\PolicyFactoryC.cpp" "PI\PICurrent.cpp" "PI\PICurrent_Loader.cpp" "PI\ORBInitInfo.cpp" "PI\ClientRequestInterceptor_Adapter_Impl.cpp" "PI\ClientRequestDetails.cpp" "PI\PI_ORBInitializer.cpp" "PI\DLL_Resident_ORB_Initializer.cpp" "PI\PolicyFactory_Loader.cpp" "PI\ProcessingModePolicy.cpp" "PI\ClientRequestInterceptor_Factory_Impl.cpp" "PI\ORBInitializer_Registry_Impl.cpp" "PI\PolicyFactoryA.cpp" "PI\PICurrent_Impl.cpp" "PI\ClientRequestInfo.cpp" "PI\PolicyFactory_Registry.cpp" "PI\RequestInfo_Util.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_PIsd.lib"
	-@del /f/q "$(OUTDIR)\TAO_PIsd.exp"
	-@del /f/q "$(OUTDIR)\TAO_PIsd.ilk"
	-@del /f/q "..\..\lib\TAO_PIsd.pdb"
	-@del /f/q "PI\PI_includeC.h"
	-@del /f/q "PI\PI_includeS.h"
	-@del /f/q "PI\PI_includeA.h"
	-@del /f/q "PI\PI_includeC.cpp"
	-@del /f/q "PI\ORBInitInfoC.h"
	-@del /f/q "PI\ORBInitInfoS.h"
	-@del /f/q "PI\ORBInitInfoA.h"
	-@del /f/q "PI\ORBInitInfoC.cpp"
	-@del /f/q "PI\ORBInitializerC.h"
	-@del /f/q "PI\ORBInitializerS.h"
	-@del /f/q "PI\ORBInitializerA.h"
	-@del /f/q "PI\ORBInitializerC.cpp"
	-@del /f/q "PI\ORBInitializerA.cpp"
	-@del /f/q "PI\ClientRequestInterceptorC.h"
	-@del /f/q "PI\ClientRequestInterceptorS.h"
	-@del /f/q "PI\ClientRequestInterceptorA.h"
	-@del /f/q "PI\ClientRequestInterceptorC.cpp"
	-@del /f/q "PI\ClientRequestInterceptorA.cpp"
	-@del /f/q "PI\ClientRequestInfoC.h"
	-@del /f/q "PI\ClientRequestInfoS.h"
	-@del /f/q "PI\ClientRequestInfoA.h"
	-@del /f/q "PI\ClientRequestInfoC.cpp"
	-@del /f/q "PI\ClientRequestInfoA.cpp"
	-@del /f/q "PI\RequestInfoC.h"
	-@del /f/q "PI\RequestInfoS.h"
	-@del /f/q "PI\RequestInfoA.h"
	-@del /f/q "PI\RequestInfoC.cpp"
	-@del /f/q "PI\RequestInfoA.cpp"
	-@del /f/q "PI\PIForwardRequestC.h"
	-@del /f/q "PI\PIForwardRequestS.h"
	-@del /f/q "PI\PIForwardRequestA.h"
	-@del /f/q "PI\PIForwardRequestC.cpp"
	-@del /f/q "PI\PIForwardRequestA.cpp"
	-@del /f/q "PI\PICurrentC.h"
	-@del /f/q "PI\PICurrentS.h"
	-@del /f/q "PI\PICurrentA.h"
	-@del /f/q "PI\PICurrentC.cpp"
	-@del /f/q "PI\PICurrentA.cpp"
	-@del /f/q "PI\ProcessingModePolicyC.h"
	-@del /f/q "PI\ProcessingModePolicyS.h"
	-@del /f/q "PI\ProcessingModePolicyA.h"
	-@del /f/q "PI\ProcessingModePolicyC.cpp"
	-@del /f/q "PI\ProcessingModePolicyA.cpp"
	-@del /f/q "PI\InterceptorC.h"
	-@del /f/q "PI\InterceptorS.h"
	-@del /f/q "PI\InterceptorA.h"
	-@del /f/q "PI\InterceptorC.cpp"
	-@del /f/q "PI\InterceptorA.cpp"
	-@del /f/q "PI\InvalidSlotC.h"
	-@del /f/q "PI\InvalidSlotS.h"
	-@del /f/q "PI\InvalidSlotA.h"
	-@del /f/q "PI\InvalidSlotC.cpp"
	-@del /f/q "PI\InvalidSlotA.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\PI\$(NULL)" mkdir "Static_Debug\PI"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"..\..\lib\TAO_PIsd.pdb" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_PIsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\PI\PI_includeC.obj" \
	"$(INTDIR)\PI\ORBInitInfoC.obj" \
	"$(INTDIR)\PI\ORBInitializerC.obj" \
	"$(INTDIR)\PI\ORBInitializerA.obj" \
	"$(INTDIR)\PI\ClientRequestInterceptorC.obj" \
	"$(INTDIR)\PI\ClientRequestInterceptorA.obj" \
	"$(INTDIR)\PI\ClientRequestInfoC.obj" \
	"$(INTDIR)\PI\ClientRequestInfoA.obj" \
	"$(INTDIR)\PI\RequestInfoC.obj" \
	"$(INTDIR)\PI\RequestInfoA.obj" \
	"$(INTDIR)\PI\PIForwardRequestC.obj" \
	"$(INTDIR)\PI\PIForwardRequestA.obj" \
	"$(INTDIR)\PI\PICurrentC.obj" \
	"$(INTDIR)\PI\PICurrentA.obj" \
	"$(INTDIR)\PI\ProcessingModePolicyC.obj" \
	"$(INTDIR)\PI\ProcessingModePolicyA.obj" \
	"$(INTDIR)\PI\InterceptorC.obj" \
	"$(INTDIR)\PI\InterceptorA.obj" \
	"$(INTDIR)\PI\InvalidSlotC.obj" \
	"$(INTDIR)\PI\InvalidSlotA.obj" \
	"$(INTDIR)\PI\PI.obj" \
	"$(INTDIR)\PI\PI_PolicyFactory.obj" \
	"$(INTDIR)\PI\PolicyFactoryC.obj" \
	"$(INTDIR)\PI\PICurrent.obj" \
	"$(INTDIR)\PI\PICurrent_Loader.obj" \
	"$(INTDIR)\PI\ORBInitInfo.obj" \
	"$(INTDIR)\PI\ClientRequestInterceptor_Adapter_Impl.obj" \
	"$(INTDIR)\PI\ClientRequestDetails.obj" \
	"$(INTDIR)\PI\PI_ORBInitializer.obj" \
	"$(INTDIR)\PI\DLL_Resident_ORB_Initializer.obj" \
	"$(INTDIR)\PI\PolicyFactory_Loader.obj" \
	"$(INTDIR)\PI\ProcessingModePolicy.obj" \
	"$(INTDIR)\PI\ClientRequestInterceptor_Factory_Impl.obj" \
	"$(INTDIR)\PI\ORBInitializer_Registry_Impl.obj" \
	"$(INTDIR)\PI\PolicyFactoryA.obj" \
	"$(INTDIR)\PI\PICurrent_Impl.obj" \
	"$(INTDIR)\PI\ClientRequestInfo.obj" \
	"$(INTDIR)\PI\PolicyFactory_Registry.obj" \
	"$(INTDIR)\PI\RequestInfo_Util.obj"

"$(OUTDIR)\TAO_PIsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_PIsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_PIsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\PI\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_PIs.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.PI.dep" "PI\PI_includeC.cpp" "PI\ORBInitInfoC.cpp" "PI\ORBInitializerC.cpp" "PI\ORBInitializerA.cpp" "PI\ClientRequestInterceptorC.cpp" "PI\ClientRequestInterceptorA.cpp" "PI\ClientRequestInfoC.cpp" "PI\ClientRequestInfoA.cpp" "PI\RequestInfoC.cpp" "PI\RequestInfoA.cpp" "PI\PIForwardRequestC.cpp" "PI\PIForwardRequestA.cpp" "PI\PICurrentC.cpp" "PI\PICurrentA.cpp" "PI\ProcessingModePolicyC.cpp" "PI\ProcessingModePolicyA.cpp" "PI\InterceptorC.cpp" "PI\InterceptorA.cpp" "PI\InvalidSlotC.cpp" "PI\InvalidSlotA.cpp" "PI\PI.cpp" "PI\PI_PolicyFactory.cpp" "PI\PolicyFactoryC.cpp" "PI\PICurrent.cpp" "PI\PICurrent_Loader.cpp" "PI\ORBInitInfo.cpp" "PI\ClientRequestInterceptor_Adapter_Impl.cpp" "PI\ClientRequestDetails.cpp" "PI\PI_ORBInitializer.cpp" "PI\DLL_Resident_ORB_Initializer.cpp" "PI\PolicyFactory_Loader.cpp" "PI\ProcessingModePolicy.cpp" "PI\ClientRequestInterceptor_Factory_Impl.cpp" "PI\ORBInitializer_Registry_Impl.cpp" "PI\PolicyFactoryA.cpp" "PI\PICurrent_Impl.cpp" "PI\ClientRequestInfo.cpp" "PI\PolicyFactory_Registry.cpp" "PI\RequestInfo_Util.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_PIs.lib"
	-@del /f/q "$(OUTDIR)\TAO_PIs.exp"
	-@del /f/q "$(OUTDIR)\TAO_PIs.ilk"
	-@del /f/q "PI\PI_includeC.h"
	-@del /f/q "PI\PI_includeS.h"
	-@del /f/q "PI\PI_includeA.h"
	-@del /f/q "PI\PI_includeC.cpp"
	-@del /f/q "PI\ORBInitInfoC.h"
	-@del /f/q "PI\ORBInitInfoS.h"
	-@del /f/q "PI\ORBInitInfoA.h"
	-@del /f/q "PI\ORBInitInfoC.cpp"
	-@del /f/q "PI\ORBInitializerC.h"
	-@del /f/q "PI\ORBInitializerS.h"
	-@del /f/q "PI\ORBInitializerA.h"
	-@del /f/q "PI\ORBInitializerC.cpp"
	-@del /f/q "PI\ORBInitializerA.cpp"
	-@del /f/q "PI\ClientRequestInterceptorC.h"
	-@del /f/q "PI\ClientRequestInterceptorS.h"
	-@del /f/q "PI\ClientRequestInterceptorA.h"
	-@del /f/q "PI\ClientRequestInterceptorC.cpp"
	-@del /f/q "PI\ClientRequestInterceptorA.cpp"
	-@del /f/q "PI\ClientRequestInfoC.h"
	-@del /f/q "PI\ClientRequestInfoS.h"
	-@del /f/q "PI\ClientRequestInfoA.h"
	-@del /f/q "PI\ClientRequestInfoC.cpp"
	-@del /f/q "PI\ClientRequestInfoA.cpp"
	-@del /f/q "PI\RequestInfoC.h"
	-@del /f/q "PI\RequestInfoS.h"
	-@del /f/q "PI\RequestInfoA.h"
	-@del /f/q "PI\RequestInfoC.cpp"
	-@del /f/q "PI\RequestInfoA.cpp"
	-@del /f/q "PI\PIForwardRequestC.h"
	-@del /f/q "PI\PIForwardRequestS.h"
	-@del /f/q "PI\PIForwardRequestA.h"
	-@del /f/q "PI\PIForwardRequestC.cpp"
	-@del /f/q "PI\PIForwardRequestA.cpp"
	-@del /f/q "PI\PICurrentC.h"
	-@del /f/q "PI\PICurrentS.h"
	-@del /f/q "PI\PICurrentA.h"
	-@del /f/q "PI\PICurrentC.cpp"
	-@del /f/q "PI\PICurrentA.cpp"
	-@del /f/q "PI\ProcessingModePolicyC.h"
	-@del /f/q "PI\ProcessingModePolicyS.h"
	-@del /f/q "PI\ProcessingModePolicyA.h"
	-@del /f/q "PI\ProcessingModePolicyC.cpp"
	-@del /f/q "PI\ProcessingModePolicyA.cpp"
	-@del /f/q "PI\InterceptorC.h"
	-@del /f/q "PI\InterceptorS.h"
	-@del /f/q "PI\InterceptorA.h"
	-@del /f/q "PI\InterceptorC.cpp"
	-@del /f/q "PI\InterceptorA.cpp"
	-@del /f/q "PI\InvalidSlotC.h"
	-@del /f/q "PI\InvalidSlotS.h"
	-@del /f/q "PI\InvalidSlotA.h"
	-@del /f/q "PI\InvalidSlotC.cpp"
	-@del /f/q "PI\InvalidSlotA.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\PI\$(NULL)" mkdir "Static_Release\PI"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_PIs.lib"
LINK32_OBJS= \
	"$(INTDIR)\PI\PI_includeC.obj" \
	"$(INTDIR)\PI\ORBInitInfoC.obj" \
	"$(INTDIR)\PI\ORBInitializerC.obj" \
	"$(INTDIR)\PI\ORBInitializerA.obj" \
	"$(INTDIR)\PI\ClientRequestInterceptorC.obj" \
	"$(INTDIR)\PI\ClientRequestInterceptorA.obj" \
	"$(INTDIR)\PI\ClientRequestInfoC.obj" \
	"$(INTDIR)\PI\ClientRequestInfoA.obj" \
	"$(INTDIR)\PI\RequestInfoC.obj" \
	"$(INTDIR)\PI\RequestInfoA.obj" \
	"$(INTDIR)\PI\PIForwardRequestC.obj" \
	"$(INTDIR)\PI\PIForwardRequestA.obj" \
	"$(INTDIR)\PI\PICurrentC.obj" \
	"$(INTDIR)\PI\PICurrentA.obj" \
	"$(INTDIR)\PI\ProcessingModePolicyC.obj" \
	"$(INTDIR)\PI\ProcessingModePolicyA.obj" \
	"$(INTDIR)\PI\InterceptorC.obj" \
	"$(INTDIR)\PI\InterceptorA.obj" \
	"$(INTDIR)\PI\InvalidSlotC.obj" \
	"$(INTDIR)\PI\InvalidSlotA.obj" \
	"$(INTDIR)\PI\PI.obj" \
	"$(INTDIR)\PI\PI_PolicyFactory.obj" \
	"$(INTDIR)\PI\PolicyFactoryC.obj" \
	"$(INTDIR)\PI\PICurrent.obj" \
	"$(INTDIR)\PI\PICurrent_Loader.obj" \
	"$(INTDIR)\PI\ORBInitInfo.obj" \
	"$(INTDIR)\PI\ClientRequestInterceptor_Adapter_Impl.obj" \
	"$(INTDIR)\PI\ClientRequestDetails.obj" \
	"$(INTDIR)\PI\PI_ORBInitializer.obj" \
	"$(INTDIR)\PI\DLL_Resident_ORB_Initializer.obj" \
	"$(INTDIR)\PI\PolicyFactory_Loader.obj" \
	"$(INTDIR)\PI\ProcessingModePolicy.obj" \
	"$(INTDIR)\PI\ClientRequestInterceptor_Factory_Impl.obj" \
	"$(INTDIR)\PI\ORBInitializer_Registry_Impl.obj" \
	"$(INTDIR)\PI\PolicyFactoryA.obj" \
	"$(INTDIR)\PI\PICurrent_Impl.obj" \
	"$(INTDIR)\PI\ClientRequestInfo.obj" \
	"$(INTDIR)\PI\PolicyFactory_Registry.obj" \
	"$(INTDIR)\PI\RequestInfo_Util.obj"

"$(OUTDIR)\TAO_PIs.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_PIs.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_PIs.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.PI.dep")
!INCLUDE "Makefile.PI.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="PI\PI_includeC.cpp"

"$(INTDIR)\PI\PI_includeC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\PI_includeC.obj" $(SOURCE)

SOURCE="PI\ORBInitInfoC.cpp"

"$(INTDIR)\PI\ORBInitInfoC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\ORBInitInfoC.obj" $(SOURCE)

SOURCE="PI\ORBInitializerC.cpp"

"$(INTDIR)\PI\ORBInitializerC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\ORBInitializerC.obj" $(SOURCE)

SOURCE="PI\ORBInitializerA.cpp"

"$(INTDIR)\PI\ORBInitializerA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\ORBInitializerA.obj" $(SOURCE)

SOURCE="PI\ClientRequestInterceptorC.cpp"

"$(INTDIR)\PI\ClientRequestInterceptorC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\ClientRequestInterceptorC.obj" $(SOURCE)

SOURCE="PI\ClientRequestInterceptorA.cpp"

"$(INTDIR)\PI\ClientRequestInterceptorA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\ClientRequestInterceptorA.obj" $(SOURCE)

SOURCE="PI\ClientRequestInfoC.cpp"

"$(INTDIR)\PI\ClientRequestInfoC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\ClientRequestInfoC.obj" $(SOURCE)

SOURCE="PI\ClientRequestInfoA.cpp"

"$(INTDIR)\PI\ClientRequestInfoA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\ClientRequestInfoA.obj" $(SOURCE)

SOURCE="PI\RequestInfoC.cpp"

"$(INTDIR)\PI\RequestInfoC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\RequestInfoC.obj" $(SOURCE)

SOURCE="PI\RequestInfoA.cpp"

"$(INTDIR)\PI\RequestInfoA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\RequestInfoA.obj" $(SOURCE)

SOURCE="PI\PIForwardRequestC.cpp"

"$(INTDIR)\PI\PIForwardRequestC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\PIForwardRequestC.obj" $(SOURCE)

SOURCE="PI\PIForwardRequestA.cpp"

"$(INTDIR)\PI\PIForwardRequestA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\PIForwardRequestA.obj" $(SOURCE)

SOURCE="PI\PICurrentC.cpp"

"$(INTDIR)\PI\PICurrentC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\PICurrentC.obj" $(SOURCE)

SOURCE="PI\PICurrentA.cpp"

"$(INTDIR)\PI\PICurrentA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\PICurrentA.obj" $(SOURCE)

SOURCE="PI\ProcessingModePolicyC.cpp"

"$(INTDIR)\PI\ProcessingModePolicyC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\ProcessingModePolicyC.obj" $(SOURCE)

SOURCE="PI\ProcessingModePolicyA.cpp"

"$(INTDIR)\PI\ProcessingModePolicyA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\ProcessingModePolicyA.obj" $(SOURCE)

SOURCE="PI\InterceptorC.cpp"

"$(INTDIR)\PI\InterceptorC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\InterceptorC.obj" $(SOURCE)

SOURCE="PI\InterceptorA.cpp"

"$(INTDIR)\PI\InterceptorA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\InterceptorA.obj" $(SOURCE)

SOURCE="PI\InvalidSlotC.cpp"

"$(INTDIR)\PI\InvalidSlotC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\InvalidSlotC.obj" $(SOURCE)

SOURCE="PI\InvalidSlotA.cpp"

"$(INTDIR)\PI\InvalidSlotA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\InvalidSlotA.obj" $(SOURCE)

SOURCE="PI\PI.cpp"

"$(INTDIR)\PI\PI.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\PI.obj" $(SOURCE)

SOURCE="PI\PI_PolicyFactory.cpp"

"$(INTDIR)\PI\PI_PolicyFactory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\PI_PolicyFactory.obj" $(SOURCE)

SOURCE="PI\PolicyFactoryC.cpp"

"$(INTDIR)\PI\PolicyFactoryC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\PolicyFactoryC.obj" $(SOURCE)

SOURCE="PI\PICurrent.cpp"

"$(INTDIR)\PI\PICurrent.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\PICurrent.obj" $(SOURCE)

SOURCE="PI\PICurrent_Loader.cpp"

"$(INTDIR)\PI\PICurrent_Loader.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\PICurrent_Loader.obj" $(SOURCE)

SOURCE="PI\ORBInitInfo.cpp"

"$(INTDIR)\PI\ORBInitInfo.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\ORBInitInfo.obj" $(SOURCE)

SOURCE="PI\ClientRequestInterceptor_Adapter_Impl.cpp"

"$(INTDIR)\PI\ClientRequestInterceptor_Adapter_Impl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\ClientRequestInterceptor_Adapter_Impl.obj" $(SOURCE)

SOURCE="PI\ClientRequestDetails.cpp"

"$(INTDIR)\PI\ClientRequestDetails.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\ClientRequestDetails.obj" $(SOURCE)

SOURCE="PI\PI_ORBInitializer.cpp"

"$(INTDIR)\PI\PI_ORBInitializer.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\PI_ORBInitializer.obj" $(SOURCE)

SOURCE="PI\DLL_Resident_ORB_Initializer.cpp"

"$(INTDIR)\PI\DLL_Resident_ORB_Initializer.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\DLL_Resident_ORB_Initializer.obj" $(SOURCE)

SOURCE="PI\PolicyFactory_Loader.cpp"

"$(INTDIR)\PI\PolicyFactory_Loader.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\PolicyFactory_Loader.obj" $(SOURCE)

SOURCE="PI\ProcessingModePolicy.cpp"

"$(INTDIR)\PI\ProcessingModePolicy.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\ProcessingModePolicy.obj" $(SOURCE)

SOURCE="PI\ClientRequestInterceptor_Factory_Impl.cpp"

"$(INTDIR)\PI\ClientRequestInterceptor_Factory_Impl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\ClientRequestInterceptor_Factory_Impl.obj" $(SOURCE)

SOURCE="PI\ORBInitializer_Registry_Impl.cpp"

"$(INTDIR)\PI\ORBInitializer_Registry_Impl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\ORBInitializer_Registry_Impl.obj" $(SOURCE)

SOURCE="PI\PolicyFactoryA.cpp"

"$(INTDIR)\PI\PolicyFactoryA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\PolicyFactoryA.obj" $(SOURCE)

SOURCE="PI\PICurrent_Impl.cpp"

"$(INTDIR)\PI\PICurrent_Impl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\PICurrent_Impl.obj" $(SOURCE)

SOURCE="PI\ClientRequestInfo.cpp"

"$(INTDIR)\PI\ClientRequestInfo.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\ClientRequestInfo.obj" $(SOURCE)

SOURCE="PI\PolicyFactory_Registry.cpp"

"$(INTDIR)\PI\PolicyFactory_Registry.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\PolicyFactory_Registry.obj" $(SOURCE)

SOURCE="PI\RequestInfo_Util.cpp"

"$(INTDIR)\PI\RequestInfo_Util.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PI\RequestInfo_Util.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="PI\PI_include.pidl"

InputPath=PI\PI_include.pidl

"PI\PI_includeC.h" "PI\PI_includeS.h" "PI\PI_includeA.h" "PI\PI_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PI_PI_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -Sorb -GX -Wb,unique_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\ORBInitInfo.pidl"

InputPath=PI\ORBInitInfo.pidl

"PI\ORBInitInfoC.h" "PI\ORBInitInfoS.h" "PI\ORBInitInfoA.h" "PI\ORBInitInfoC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PI_ORBInitInfo_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -Sorb -Sal -GX -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\ORBInitializer.pidl"

InputPath=PI\ORBInitializer.pidl

"PI\ORBInitializerC.h" "PI\ORBInitializerS.h" "PI\ORBInitializerA.h" "PI\ORBInitializerC.cpp" "PI\ORBInitializerA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PI_ORBInitializer_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\ClientRequestInterceptor.pidl"

InputPath=PI\ClientRequestInterceptor.pidl

"PI\ClientRequestInterceptorC.h" "PI\ClientRequestInterceptorS.h" "PI\ClientRequestInterceptorA.h" "PI\ClientRequestInterceptorC.cpp" "PI\ClientRequestInterceptorA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PI_ClientRequestInterceptor_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\ClientRequestInfo.pidl"

InputPath=PI\ClientRequestInfo.pidl

"PI\ClientRequestInfoC.h" "PI\ClientRequestInfoS.h" "PI\ClientRequestInfoA.h" "PI\ClientRequestInfoC.cpp" "PI\ClientRequestInfoA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PI_ClientRequestInfo_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\RequestInfo.pidl"

InputPath=PI\RequestInfo.pidl

"PI\RequestInfoC.h" "PI\RequestInfoS.h" "PI\RequestInfoA.h" "PI\RequestInfoC.cpp" "PI\RequestInfoA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PI_RequestInfo_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\PIForwardRequest.pidl"

InputPath=PI\PIForwardRequest.pidl

"PI\PIForwardRequestC.h" "PI\PIForwardRequestS.h" "PI\PIForwardRequestA.h" "PI\PIForwardRequestC.cpp" "PI\PIForwardRequestA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PI_PIForwardRequest_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\PICurrent.pidl"

InputPath=PI\PICurrent.pidl

"PI\PICurrentC.h" "PI\PICurrentS.h" "PI\PICurrentA.h" "PI\PICurrentC.cpp" "PI\PICurrentA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PI_PICurrent_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\ProcessingModePolicy.pidl"

InputPath=PI\ProcessingModePolicy.pidl

"PI\ProcessingModePolicyC.h" "PI\ProcessingModePolicyS.h" "PI\ProcessingModePolicyA.h" "PI\ProcessingModePolicyC.cpp" "PI\ProcessingModePolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PI_ProcessingModePolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\Interceptor.pidl"

InputPath=PI\Interceptor.pidl

"PI\InterceptorC.h" "PI\InterceptorS.h" "PI\InterceptorA.h" "PI\InterceptorC.cpp" "PI\InterceptorA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PI_Interceptor_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal "$(InputPath)"
<<

SOURCE="PI\InvalidSlot.pidl"

InputPath=PI\InvalidSlot.pidl

"PI\InvalidSlotC.h" "PI\InvalidSlotS.h" "PI\InvalidSlotA.h" "PI\InvalidSlotC.cpp" "PI\InvalidSlotA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-PI_InvalidSlot_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="PI\PI_include.pidl"

InputPath=PI\PI_include.pidl

"PI\PI_includeC.h" "PI\PI_includeS.h" "PI\PI_includeA.h" "PI\PI_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PI_PI_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -Sorb -GX -Wb,unique_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\ORBInitInfo.pidl"

InputPath=PI\ORBInitInfo.pidl

"PI\ORBInitInfoC.h" "PI\ORBInitInfoS.h" "PI\ORBInitInfoA.h" "PI\ORBInitInfoC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PI_ORBInitInfo_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -Sorb -Sal -GX -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\ORBInitializer.pidl"

InputPath=PI\ORBInitializer.pidl

"PI\ORBInitializerC.h" "PI\ORBInitializerS.h" "PI\ORBInitializerA.h" "PI\ORBInitializerC.cpp" "PI\ORBInitializerA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PI_ORBInitializer_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\ClientRequestInterceptor.pidl"

InputPath=PI\ClientRequestInterceptor.pidl

"PI\ClientRequestInterceptorC.h" "PI\ClientRequestInterceptorS.h" "PI\ClientRequestInterceptorA.h" "PI\ClientRequestInterceptorC.cpp" "PI\ClientRequestInterceptorA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PI_ClientRequestInterceptor_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\ClientRequestInfo.pidl"

InputPath=PI\ClientRequestInfo.pidl

"PI\ClientRequestInfoC.h" "PI\ClientRequestInfoS.h" "PI\ClientRequestInfoA.h" "PI\ClientRequestInfoC.cpp" "PI\ClientRequestInfoA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PI_ClientRequestInfo_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\RequestInfo.pidl"

InputPath=PI\RequestInfo.pidl

"PI\RequestInfoC.h" "PI\RequestInfoS.h" "PI\RequestInfoA.h" "PI\RequestInfoC.cpp" "PI\RequestInfoA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PI_RequestInfo_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\PIForwardRequest.pidl"

InputPath=PI\PIForwardRequest.pidl

"PI\PIForwardRequestC.h" "PI\PIForwardRequestS.h" "PI\PIForwardRequestA.h" "PI\PIForwardRequestC.cpp" "PI\PIForwardRequestA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PI_PIForwardRequest_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\PICurrent.pidl"

InputPath=PI\PICurrent.pidl

"PI\PICurrentC.h" "PI\PICurrentS.h" "PI\PICurrentA.h" "PI\PICurrentC.cpp" "PI\PICurrentA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PI_PICurrent_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\ProcessingModePolicy.pidl"

InputPath=PI\ProcessingModePolicy.pidl

"PI\ProcessingModePolicyC.h" "PI\ProcessingModePolicyS.h" "PI\ProcessingModePolicyA.h" "PI\ProcessingModePolicyC.cpp" "PI\ProcessingModePolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PI_ProcessingModePolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\Interceptor.pidl"

InputPath=PI\Interceptor.pidl

"PI\InterceptorC.h" "PI\InterceptorS.h" "PI\InterceptorA.h" "PI\InterceptorC.cpp" "PI\InterceptorA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PI_Interceptor_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal "$(InputPath)"
<<

SOURCE="PI\InvalidSlot.pidl"

InputPath=PI\InvalidSlot.pidl

"PI\InvalidSlotC.h" "PI\InvalidSlotS.h" "PI\InvalidSlotA.h" "PI\InvalidSlotC.cpp" "PI\InvalidSlotA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-PI_InvalidSlot_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="PI\PI_include.pidl"

InputPath=PI\PI_include.pidl

"PI\PI_includeC.h" "PI\PI_includeS.h" "PI\PI_includeA.h" "PI\PI_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PI_PI_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -Sorb -GX -Wb,unique_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\ORBInitInfo.pidl"

InputPath=PI\ORBInitInfo.pidl

"PI\ORBInitInfoC.h" "PI\ORBInitInfoS.h" "PI\ORBInitInfoA.h" "PI\ORBInitInfoC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PI_ORBInitInfo_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -Sorb -Sal -GX -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\ORBInitializer.pidl"

InputPath=PI\ORBInitializer.pidl

"PI\ORBInitializerC.h" "PI\ORBInitializerS.h" "PI\ORBInitializerA.h" "PI\ORBInitializerC.cpp" "PI\ORBInitializerA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PI_ORBInitializer_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\ClientRequestInterceptor.pidl"

InputPath=PI\ClientRequestInterceptor.pidl

"PI\ClientRequestInterceptorC.h" "PI\ClientRequestInterceptorS.h" "PI\ClientRequestInterceptorA.h" "PI\ClientRequestInterceptorC.cpp" "PI\ClientRequestInterceptorA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PI_ClientRequestInterceptor_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\ClientRequestInfo.pidl"

InputPath=PI\ClientRequestInfo.pidl

"PI\ClientRequestInfoC.h" "PI\ClientRequestInfoS.h" "PI\ClientRequestInfoA.h" "PI\ClientRequestInfoC.cpp" "PI\ClientRequestInfoA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PI_ClientRequestInfo_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\RequestInfo.pidl"

InputPath=PI\RequestInfo.pidl

"PI\RequestInfoC.h" "PI\RequestInfoS.h" "PI\RequestInfoA.h" "PI\RequestInfoC.cpp" "PI\RequestInfoA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PI_RequestInfo_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\PIForwardRequest.pidl"

InputPath=PI\PIForwardRequest.pidl

"PI\PIForwardRequestC.h" "PI\PIForwardRequestS.h" "PI\PIForwardRequestA.h" "PI\PIForwardRequestC.cpp" "PI\PIForwardRequestA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PI_PIForwardRequest_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\PICurrent.pidl"

InputPath=PI\PICurrent.pidl

"PI\PICurrentC.h" "PI\PICurrentS.h" "PI\PICurrentA.h" "PI\PICurrentC.cpp" "PI\PICurrentA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PI_PICurrent_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\ProcessingModePolicy.pidl"

InputPath=PI\ProcessingModePolicy.pidl

"PI\ProcessingModePolicyC.h" "PI\ProcessingModePolicyS.h" "PI\ProcessingModePolicyA.h" "PI\ProcessingModePolicyC.cpp" "PI\ProcessingModePolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PI_ProcessingModePolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\Interceptor.pidl"

InputPath=PI\Interceptor.pidl

"PI\InterceptorC.h" "PI\InterceptorS.h" "PI\InterceptorA.h" "PI\InterceptorC.cpp" "PI\InterceptorA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PI_Interceptor_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal "$(InputPath)"
<<

SOURCE="PI\InvalidSlot.pidl"

InputPath=PI\InvalidSlot.pidl

"PI\InvalidSlotC.h" "PI\InvalidSlotS.h" "PI\InvalidSlotA.h" "PI\InvalidSlotC.cpp" "PI\InvalidSlotA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-PI_InvalidSlot_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="PI\PI_include.pidl"

InputPath=PI\PI_include.pidl

"PI\PI_includeC.h" "PI\PI_includeS.h" "PI\PI_includeA.h" "PI\PI_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PI_PI_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -Sorb -GX -Wb,unique_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\ORBInitInfo.pidl"

InputPath=PI\ORBInitInfo.pidl

"PI\ORBInitInfoC.h" "PI\ORBInitInfoS.h" "PI\ORBInitInfoA.h" "PI\ORBInitInfoC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PI_ORBInitInfo_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -Sorb -Sal -GX -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\ORBInitializer.pidl"

InputPath=PI\ORBInitializer.pidl

"PI\ORBInitializerC.h" "PI\ORBInitializerS.h" "PI\ORBInitializerA.h" "PI\ORBInitializerC.cpp" "PI\ORBInitializerA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PI_ORBInitializer_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\ClientRequestInterceptor.pidl"

InputPath=PI\ClientRequestInterceptor.pidl

"PI\ClientRequestInterceptorC.h" "PI\ClientRequestInterceptorS.h" "PI\ClientRequestInterceptorA.h" "PI\ClientRequestInterceptorC.cpp" "PI\ClientRequestInterceptorA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PI_ClientRequestInterceptor_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\ClientRequestInfo.pidl"

InputPath=PI\ClientRequestInfo.pidl

"PI\ClientRequestInfoC.h" "PI\ClientRequestInfoS.h" "PI\ClientRequestInfoA.h" "PI\ClientRequestInfoC.cpp" "PI\ClientRequestInfoA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PI_ClientRequestInfo_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\RequestInfo.pidl"

InputPath=PI\RequestInfo.pidl

"PI\RequestInfoC.h" "PI\RequestInfoS.h" "PI\RequestInfoA.h" "PI\RequestInfoC.cpp" "PI\RequestInfoA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PI_RequestInfo_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\PIForwardRequest.pidl"

InputPath=PI\PIForwardRequest.pidl

"PI\PIForwardRequestC.h" "PI\PIForwardRequestS.h" "PI\PIForwardRequestA.h" "PI\PIForwardRequestC.cpp" "PI\PIForwardRequestA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PI_PIForwardRequest_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\PICurrent.pidl"

InputPath=PI\PICurrent.pidl

"PI\PICurrentC.h" "PI\PICurrentS.h" "PI\PICurrentA.h" "PI\PICurrentC.cpp" "PI\PICurrentA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PI_PICurrent_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\ProcessingModePolicy.pidl"

InputPath=PI\ProcessingModePolicy.pidl

"PI\ProcessingModePolicyC.h" "PI\ProcessingModePolicyS.h" "PI\ProcessingModePolicyA.h" "PI\ProcessingModePolicyC.cpp" "PI\ProcessingModePolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PI_ProcessingModePolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

SOURCE="PI\Interceptor.pidl"

InputPath=PI\Interceptor.pidl

"PI\InterceptorC.h" "PI\InterceptorS.h" "PI\InterceptorA.h" "PI\InterceptorC.cpp" "PI\InterceptorA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PI_Interceptor_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sorb -Sal "$(InputPath)"
<<

SOURCE="PI\InvalidSlot.pidl"

InputPath=PI\InvalidSlot.pidl

"PI\InvalidSlotC.h" "PI\InvalidSlotS.h" "PI\InvalidSlotA.h" "PI\InvalidSlotC.cpp" "PI\InvalidSlotA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-PI_InvalidSlot_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -SS -Sci -Gp -Gd -Wb,export_macro=TAO_PI_Export -Wb,export_include=tao/PI/pi_export.h -o PI -GA -Sal -Wb,include_guard=TAO_PI_SAFE_INCLUDE -Wb,safe_include=tao/PI/PI.h "$(InputPath)"
<<

!ENDIF

SOURCE="PI\TAO_PI.rc"

"$(INTDIR)\PI\TAO_PI.res" : $(SOURCE)
	@if not exist "$(INTDIR)\PI\$(NULL)" mkdir "$(INTDIR)\PI\"
	$(RSC) /l 0x409 /fo"$(INTDIR)\PI\TAO_PI.res" /d NDEBUG /d WIN64 /d _CRT_SECURE_NO_WARNINGS /d _CRT_SECURE_NO_DEPRECATE /d _CRT_NONSTDC_NO_DEPRECATE /i "..\.." /i ".." $(SOURCE)



!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.PI.dep")
	@echo Using "Makefile.PI.dep"
!ELSE
	@echo Warning: cannot find "Makefile.PI.dep"
!ENDIF
!ENDIF

