// $Id: Request_Dispatcher.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/Request_Dispatcher.h"
#include "tao/TAO_Server_Request.h"
#include "tao/ORB_Core.h"

ACE_RCSID (tao,
           Request_Dispatcher,
           "$Id: Request_Dispatcher.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Request_Dispatcher::~TAO_Request_Dispatcher (void)
{
}

void
TAO_Request_Dispatcher::dispatch (TAO_ORB_Core *orb_core,
                                  TAO_ServerRequest &request,
                                  CORBA::Object_out forward_to)
{
  // Dispatch based on object key
  orb_core->adapter_registry ()->dispatch (request.object_key (),
                                           request,
                                           forward_to);
}

TAO_END_VERSIONED_NAMESPACE_DECL
