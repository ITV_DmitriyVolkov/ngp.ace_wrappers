# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.TC.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "TransportCurrent\TCC.inl" "TransportCurrent\TCS.inl" "TransportCurrent\TCC.h" "TransportCurrent\TCS.h" "TransportCurrent\TCC.cpp" "TransportCurrent\TCS.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\TC\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_TCd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_TRANSPORT_CURRENT_BUILD_DLL -f "Makefile.TC.dep" "TransportCurrent\TCC.cpp" "TransportCurrent\TCS.cpp" "TransportCurrent\Current_Impl.cpp" "TransportCurrent\Current_Loader.cpp" "TransportCurrent\Current_ORBInitializer_Base.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_TCd.pdb"
	-@del /f/q "..\..\lib\TAO_TCd.dll"
	-@del /f/q "$(OUTDIR)\TAO_TCd.lib"
	-@del /f/q "$(OUTDIR)\TAO_TCd.exp"
	-@del /f/q "$(OUTDIR)\TAO_TCd.ilk"
	-@del /f/q "TransportCurrent\TCC.inl"
	-@del /f/q "TransportCurrent\TCS.inl"
	-@del /f/q "TransportCurrent\TCC.h"
	-@del /f/q "TransportCurrent\TCS.h"
	-@del /f/q "TransportCurrent\TCC.cpp"
	-@del /f/q "TransportCurrent\TCS.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\TC\$(NULL)" mkdir "Debug\TC"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_TRANSPORT_CURRENT_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_CodecFactoryd.lib TAO_PId.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\TAO_TCd.pdb" /machine:IA64 /out:"..\..\lib\TAO_TCd.dll" /implib:"$(OUTDIR)\TAO_TCd.lib"
LINK32_OBJS= \
	"$(INTDIR)\TransportCurrent\TAO_TC.res" \
	"$(INTDIR)\TransportCurrent\TCC.obj" \
	"$(INTDIR)\TransportCurrent\TCS.obj" \
	"$(INTDIR)\TransportCurrent\Current_Impl.obj" \
	"$(INTDIR)\TransportCurrent\Current_Loader.obj" \
	"$(INTDIR)\TransportCurrent\Current_ORBInitializer_Base.obj"

"..\..\lib\TAO_TCd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_TCd.dll.manifest" mt.exe -manifest "..\..\lib\TAO_TCd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\TC\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_TC.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_TRANSPORT_CURRENT_BUILD_DLL -f "Makefile.TC.dep" "TransportCurrent\TCC.cpp" "TransportCurrent\TCS.cpp" "TransportCurrent\Current_Impl.cpp" "TransportCurrent\Current_Loader.cpp" "TransportCurrent\Current_ORBInitializer_Base.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\TAO_TC.dll"
	-@del /f/q "$(OUTDIR)\TAO_TC.lib"
	-@del /f/q "$(OUTDIR)\TAO_TC.exp"
	-@del /f/q "$(OUTDIR)\TAO_TC.ilk"
	-@del /f/q "TransportCurrent\TCC.inl"
	-@del /f/q "TransportCurrent\TCS.inl"
	-@del /f/q "TransportCurrent\TCC.h"
	-@del /f/q "TransportCurrent\TCS.h"
	-@del /f/q "TransportCurrent\TCC.cpp"
	-@del /f/q "TransportCurrent\TCS.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\TC\$(NULL)" mkdir "Release\TC"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_TRANSPORT_CURRENT_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_CodecFactory.lib TAO_PI.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\TAO_TC.dll" /implib:"$(OUTDIR)\TAO_TC.lib"
LINK32_OBJS= \
	"$(INTDIR)\TransportCurrent\TAO_TC.res" \
	"$(INTDIR)\TransportCurrent\TCC.obj" \
	"$(INTDIR)\TransportCurrent\TCS.obj" \
	"$(INTDIR)\TransportCurrent\Current_Impl.obj" \
	"$(INTDIR)\TransportCurrent\Current_Loader.obj" \
	"$(INTDIR)\TransportCurrent\Current_ORBInitializer_Base.obj"

"..\..\lib\TAO_TC.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_TC.dll.manifest" mt.exe -manifest "..\..\lib\TAO_TC.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\TC\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_TCsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.TC.dep" "TransportCurrent\TCC.cpp" "TransportCurrent\TCS.cpp" "TransportCurrent\Current_Impl.cpp" "TransportCurrent\Current_Loader.cpp" "TransportCurrent\Current_ORBInitializer_Base.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_TCsd.lib"
	-@del /f/q "$(OUTDIR)\TAO_TCsd.exp"
	-@del /f/q "$(OUTDIR)\TAO_TCsd.ilk"
	-@del /f/q "..\..\lib\TAO_TCsd.pdb"
	-@del /f/q "TransportCurrent\TCC.inl"
	-@del /f/q "TransportCurrent\TCS.inl"
	-@del /f/q "TransportCurrent\TCC.h"
	-@del /f/q "TransportCurrent\TCS.h"
	-@del /f/q "TransportCurrent\TCC.cpp"
	-@del /f/q "TransportCurrent\TCS.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\TC\$(NULL)" mkdir "Static_Debug\TC"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"..\..\lib\TAO_TCsd.pdb" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_TCsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\TransportCurrent\TCC.obj" \
	"$(INTDIR)\TransportCurrent\TCS.obj" \
	"$(INTDIR)\TransportCurrent\Current_Impl.obj" \
	"$(INTDIR)\TransportCurrent\Current_Loader.obj" \
	"$(INTDIR)\TransportCurrent\Current_ORBInitializer_Base.obj"

"$(OUTDIR)\TAO_TCsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_TCsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_TCsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\TC\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_TCs.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.TC.dep" "TransportCurrent\TCC.cpp" "TransportCurrent\TCS.cpp" "TransportCurrent\Current_Impl.cpp" "TransportCurrent\Current_Loader.cpp" "TransportCurrent\Current_ORBInitializer_Base.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_TCs.lib"
	-@del /f/q "$(OUTDIR)\TAO_TCs.exp"
	-@del /f/q "$(OUTDIR)\TAO_TCs.ilk"
	-@del /f/q "TransportCurrent\TCC.inl"
	-@del /f/q "TransportCurrent\TCS.inl"
	-@del /f/q "TransportCurrent\TCC.h"
	-@del /f/q "TransportCurrent\TCS.h"
	-@del /f/q "TransportCurrent\TCC.cpp"
	-@del /f/q "TransportCurrent\TCS.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\TC\$(NULL)" mkdir "Static_Release\TC"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_TCs.lib"
LINK32_OBJS= \
	"$(INTDIR)\TransportCurrent\TCC.obj" \
	"$(INTDIR)\TransportCurrent\TCS.obj" \
	"$(INTDIR)\TransportCurrent\Current_Impl.obj" \
	"$(INTDIR)\TransportCurrent\Current_Loader.obj" \
	"$(INTDIR)\TransportCurrent\Current_ORBInitializer_Base.obj"

"$(OUTDIR)\TAO_TCs.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_TCs.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_TCs.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.TC.dep")
!INCLUDE "Makefile.TC.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="TransportCurrent\TCC.cpp"

"$(INTDIR)\TransportCurrent\TCC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\TransportCurrent\$(NULL)" mkdir "$(INTDIR)\TransportCurrent\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TransportCurrent\TCC.obj" $(SOURCE)

SOURCE="TransportCurrent\TCS.cpp"

"$(INTDIR)\TransportCurrent\TCS.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\TransportCurrent\$(NULL)" mkdir "$(INTDIR)\TransportCurrent\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TransportCurrent\TCS.obj" $(SOURCE)

SOURCE="TransportCurrent\Current_Impl.cpp"

"$(INTDIR)\TransportCurrent\Current_Impl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\TransportCurrent\$(NULL)" mkdir "$(INTDIR)\TransportCurrent\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TransportCurrent\Current_Impl.obj" $(SOURCE)

SOURCE="TransportCurrent\Current_Loader.cpp"

"$(INTDIR)\TransportCurrent\Current_Loader.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\TransportCurrent\$(NULL)" mkdir "$(INTDIR)\TransportCurrent\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TransportCurrent\Current_Loader.obj" $(SOURCE)

SOURCE="TransportCurrent\Current_ORBInitializer_Base.cpp"

"$(INTDIR)\TransportCurrent\Current_ORBInitializer_Base.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\TransportCurrent\$(NULL)" mkdir "$(INTDIR)\TransportCurrent\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TransportCurrent\Current_ORBInitializer_Base.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="TransportCurrent\TC.idl"

InputPath=TransportCurrent\TC.idl

"TransportCurrent\TCC.inl" "TransportCurrent\TCS.inl" "TransportCurrent\TCC.h" "TransportCurrent\TCS.h" "TransportCurrent\TCC.cpp" "TransportCurrent\TCS.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-TransportCurrent_TC_idl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Wb,export_include=tao/TransportCurrent/Transport_Current_Export.h -Wb,export_macro=TAO_Transport_Current_Export -o TransportCurrent -I./TransportCurrent "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="TransportCurrent\TC.idl"

InputPath=TransportCurrent\TC.idl

"TransportCurrent\TCC.inl" "TransportCurrent\TCS.inl" "TransportCurrent\TCC.h" "TransportCurrent\TCS.h" "TransportCurrent\TCC.cpp" "TransportCurrent\TCS.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-TransportCurrent_TC_idl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Wb,export_include=tao/TransportCurrent/Transport_Current_Export.h -Wb,export_macro=TAO_Transport_Current_Export -o TransportCurrent -I./TransportCurrent "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="TransportCurrent\TC.idl"

InputPath=TransportCurrent\TC.idl

"TransportCurrent\TCC.inl" "TransportCurrent\TCS.inl" "TransportCurrent\TCC.h" "TransportCurrent\TCS.h" "TransportCurrent\TCC.cpp" "TransportCurrent\TCS.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-TransportCurrent_TC_idl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Wb,export_include=tao/TransportCurrent/Transport_Current_Export.h -Wb,export_macro=TAO_Transport_Current_Export -o TransportCurrent -I./TransportCurrent "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="TransportCurrent\TC.idl"

InputPath=TransportCurrent\TC.idl

"TransportCurrent\TCC.inl" "TransportCurrent\TCS.inl" "TransportCurrent\TCC.h" "TransportCurrent\TCS.h" "TransportCurrent\TCC.cpp" "TransportCurrent\TCS.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-TransportCurrent_TC_idl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Wb,export_include=tao/TransportCurrent/Transport_Current_Export.h -Wb,export_macro=TAO_Transport_Current_Export -o TransportCurrent -I./TransportCurrent "$(InputPath)"
<<

!ENDIF

SOURCE="TransportCurrent\TAO_TC.rc"

"$(INTDIR)\TransportCurrent\TAO_TC.res" : $(SOURCE)
	@if not exist "$(INTDIR)\TransportCurrent\$(NULL)" mkdir "$(INTDIR)\TransportCurrent\"
	$(RSC) /l 0x409 /fo"$(INTDIR)\TransportCurrent\TAO_TC.res" /d NDEBUG /d WIN64 /d _CRT_SECURE_NO_WARNINGS /d _CRT_SECURE_NO_DEPRECATE /d _CRT_NONSTDC_NO_DEPRECATE /i "..\.." /i ".." $(SOURCE)



!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.TC.dep")
	@echo Using "Makefile.TC.dep"
!ELSE
	@echo Warning: cannot find "Makefile.TC.dep"
!ENDIF
!ENDIF

