# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.IORTable.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "IORTable\IORTableC.h" "IORTable\IORTableS.h" "IORTable\IORTableC.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\IORTable\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_IORTabled.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_IORTABLE_BUILD_DLL -f "Makefile.IORTable.dep" "IORTable\IORTableC.cpp" "IORTable\Table_Adapter.cpp" "IORTable\IORTable.cpp" "IORTable\IOR_Table_Impl.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_IORTabled.pdb"
	-@del /f/q "..\..\lib\TAO_IORTabled.dll"
	-@del /f/q "$(OUTDIR)\TAO_IORTabled.lib"
	-@del /f/q "$(OUTDIR)\TAO_IORTabled.exp"
	-@del /f/q "$(OUTDIR)\TAO_IORTabled.ilk"
	-@del /f/q "IORTable\IORTableC.h"
	-@del /f/q "IORTable\IORTableS.h"
	-@del /f/q "IORTable\IORTableC.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\IORTable\$(NULL)" mkdir "Debug\IORTable"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_IORTABLE_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\TAO_IORTabled.pdb" /machine:IA64 /out:"..\..\lib\TAO_IORTabled.dll" /implib:"$(OUTDIR)\TAO_IORTabled.lib"
LINK32_OBJS= \
	"$(INTDIR)\IORTable\TAO_IORTable.res" \
	"$(INTDIR)\IORTable\IORTableC.obj" \
	"$(INTDIR)\IORTable\Table_Adapter.obj" \
	"$(INTDIR)\IORTable\IORTable.obj" \
	"$(INTDIR)\IORTable\IOR_Table_Impl.obj"

"..\..\lib\TAO_IORTabled.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_IORTabled.dll.manifest" mt.exe -manifest "..\..\lib\TAO_IORTabled.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\IORTable\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_IORTable.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_IORTABLE_BUILD_DLL -f "Makefile.IORTable.dep" "IORTable\IORTableC.cpp" "IORTable\Table_Adapter.cpp" "IORTable\IORTable.cpp" "IORTable\IOR_Table_Impl.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\TAO_IORTable.dll"
	-@del /f/q "$(OUTDIR)\TAO_IORTable.lib"
	-@del /f/q "$(OUTDIR)\TAO_IORTable.exp"
	-@del /f/q "$(OUTDIR)\TAO_IORTable.ilk"
	-@del /f/q "IORTable\IORTableC.h"
	-@del /f/q "IORTable\IORTableS.h"
	-@del /f/q "IORTable\IORTableC.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\IORTable\$(NULL)" mkdir "Release\IORTable"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_IORTABLE_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\TAO_IORTable.dll" /implib:"$(OUTDIR)\TAO_IORTable.lib"
LINK32_OBJS= \
	"$(INTDIR)\IORTable\TAO_IORTable.res" \
	"$(INTDIR)\IORTable\IORTableC.obj" \
	"$(INTDIR)\IORTable\Table_Adapter.obj" \
	"$(INTDIR)\IORTable\IORTable.obj" \
	"$(INTDIR)\IORTable\IOR_Table_Impl.obj"

"..\..\lib\TAO_IORTable.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_IORTable.dll.manifest" mt.exe -manifest "..\..\lib\TAO_IORTable.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\IORTable\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_IORTablesd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.IORTable.dep" "IORTable\IORTableC.cpp" "IORTable\Table_Adapter.cpp" "IORTable\IORTable.cpp" "IORTable\IOR_Table_Impl.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_IORTablesd.lib"
	-@del /f/q "$(OUTDIR)\TAO_IORTablesd.exp"
	-@del /f/q "$(OUTDIR)\TAO_IORTablesd.ilk"
	-@del /f/q "..\..\lib\TAO_IORTablesd.pdb"
	-@del /f/q "IORTable\IORTableC.h"
	-@del /f/q "IORTable\IORTableS.h"
	-@del /f/q "IORTable\IORTableC.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\IORTable\$(NULL)" mkdir "Static_Debug\IORTable"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"..\..\lib\TAO_IORTablesd.pdb" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_IORTablesd.lib"
LINK32_OBJS= \
	"$(INTDIR)\IORTable\IORTableC.obj" \
	"$(INTDIR)\IORTable\Table_Adapter.obj" \
	"$(INTDIR)\IORTable\IORTable.obj" \
	"$(INTDIR)\IORTable\IOR_Table_Impl.obj"

"$(OUTDIR)\TAO_IORTablesd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_IORTablesd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_IORTablesd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\IORTable\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_IORTables.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.IORTable.dep" "IORTable\IORTableC.cpp" "IORTable\Table_Adapter.cpp" "IORTable\IORTable.cpp" "IORTable\IOR_Table_Impl.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_IORTables.lib"
	-@del /f/q "$(OUTDIR)\TAO_IORTables.exp"
	-@del /f/q "$(OUTDIR)\TAO_IORTables.ilk"
	-@del /f/q "IORTable\IORTableC.h"
	-@del /f/q "IORTable\IORTableS.h"
	-@del /f/q "IORTable\IORTableC.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\IORTable\$(NULL)" mkdir "Static_Release\IORTable"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_IORTables.lib"
LINK32_OBJS= \
	"$(INTDIR)\IORTable\IORTableC.obj" \
	"$(INTDIR)\IORTable\Table_Adapter.obj" \
	"$(INTDIR)\IORTable\IORTable.obj" \
	"$(INTDIR)\IORTable\IOR_Table_Impl.obj"

"$(OUTDIR)\TAO_IORTables.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_IORTables.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_IORTables.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.IORTable.dep")
!INCLUDE "Makefile.IORTable.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="IORTable\IORTableC.cpp"

"$(INTDIR)\IORTable\IORTableC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\IORTable\$(NULL)" mkdir "$(INTDIR)\IORTable\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IORTable\IORTableC.obj" $(SOURCE)

SOURCE="IORTable\Table_Adapter.cpp"

"$(INTDIR)\IORTable\Table_Adapter.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\IORTable\$(NULL)" mkdir "$(INTDIR)\IORTable\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IORTable\Table_Adapter.obj" $(SOURCE)

SOURCE="IORTable\IORTable.cpp"

"$(INTDIR)\IORTable\IORTable.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\IORTable\$(NULL)" mkdir "$(INTDIR)\IORTable\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IORTable\IORTable.obj" $(SOURCE)

SOURCE="IORTable\IOR_Table_Impl.cpp"

"$(INTDIR)\IORTable\IOR_Table_Impl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\IORTable\$(NULL)" mkdir "$(INTDIR)\IORTable\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IORTable\IOR_Table_Impl.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="IORTable\IORTable.pidl"

InputPath=IORTable\IORTable.pidl

"IORTable\IORTableC.h" "IORTable\IORTableS.h" "IORTable\IORTableC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-IORTable_IORTable_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -St -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_IORTable_Export -Wb,export_include=tao/IORTable/iortable_export.h -Wb,include_guard=TAO_IORTABLE_SAFE_INCLUDE -Wb,safe_include=tao/IORTable/IORTable.h -o IORTable "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="IORTable\IORTable.pidl"

InputPath=IORTable\IORTable.pidl

"IORTable\IORTableC.h" "IORTable\IORTableS.h" "IORTable\IORTableC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-IORTable_IORTable_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -St -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_IORTable_Export -Wb,export_include=tao/IORTable/iortable_export.h -Wb,include_guard=TAO_IORTABLE_SAFE_INCLUDE -Wb,safe_include=tao/IORTable/IORTable.h -o IORTable "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="IORTable\IORTable.pidl"

InputPath=IORTable\IORTable.pidl

"IORTable\IORTableC.h" "IORTable\IORTableS.h" "IORTable\IORTableC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-IORTable_IORTable_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -St -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_IORTable_Export -Wb,export_include=tao/IORTable/iortable_export.h -Wb,include_guard=TAO_IORTABLE_SAFE_INCLUDE -Wb,safe_include=tao/IORTable/IORTable.h -o IORTable "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="IORTable\IORTable.pidl"

InputPath=IORTable\IORTable.pidl

"IORTable\IORTableC.h" "IORTable\IORTableS.h" "IORTable\IORTableC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-IORTable_IORTable_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sa -St -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Gp -Gd -Sci -SS -Sorb -Wb,export_macro=TAO_IORTable_Export -Wb,export_include=tao/IORTable/iortable_export.h -Wb,include_guard=TAO_IORTABLE_SAFE_INCLUDE -Wb,safe_include=tao/IORTable/IORTable.h -o IORTable "$(InputPath)"
<<

!ENDIF

SOURCE="IORTable\TAO_IORTable.rc"

"$(INTDIR)\IORTable\TAO_IORTable.res" : $(SOURCE)
	@if not exist "$(INTDIR)\IORTable\$(NULL)" mkdir "$(INTDIR)\IORTable\"
	$(RSC) /l 0x409 /fo"$(INTDIR)\IORTable\TAO_IORTable.res" /d NDEBUG /d WIN64 /d _CRT_SECURE_NO_WARNINGS /d _CRT_SECURE_NO_DEPRECATE /d _CRT_NONSTDC_NO_DEPRECATE /i "..\.." /i ".." $(SOURCE)



!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.IORTable.dep")
	@echo Using "Makefile.IORTable.dep"
!ELSE
	@echo Warning: cannot find "Makefile.IORTable.dep"
!ENDIF
!ENDIF

