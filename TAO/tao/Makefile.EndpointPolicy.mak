# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.EndpointPolicy.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "EndpointPolicy\EndpointPolicyC.h" "EndpointPolicy\EndpointPolicyS.h" "EndpointPolicy\EndpointPolicyA.h" "EndpointPolicy\EndpointPolicyC.cpp" "EndpointPolicy\EndpointPolicyA.cpp" "EndpointPolicy\IIOPEndpointValueC.h" "EndpointPolicy\IIOPEndpointValueS.h" "EndpointPolicy\IIOPEndpointValueA.h" "EndpointPolicy\IIOPEndpointValueC.cpp" "EndpointPolicy\IIOPEndpointValueA.cpp" "EndpointPolicy\EndpointPolicyTypeC.h" "EndpointPolicy\EndpointPolicyTypeS.h" "EndpointPolicy\EndpointPolicyTypeA.h" "EndpointPolicy\EndpointPolicyTypeC.cpp" "EndpointPolicy\EndpointPolicyTypeA.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\EndpointPolicy\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_EndpointPolicyd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_ENDPOINTPOLICY_BUILD_DLL -f "Makefile.EndpointPolicy.dep" "EndpointPolicy\EndpointPolicyC.cpp" "EndpointPolicy\EndpointPolicyA.cpp" "EndpointPolicy\IIOPEndpointValueC.cpp" "EndpointPolicy\IIOPEndpointValueA.cpp" "EndpointPolicy\EndpointPolicyTypeC.cpp" "EndpointPolicy\EndpointPolicyTypeA.cpp" "EndpointPolicy\Endpoint_Acceptor_Filter.cpp" "EndpointPolicy\IIOPEndpointValue_i.cpp" "EndpointPolicy\EndpointPolicy_ORBInitializer.cpp" "EndpointPolicy\Endpoint_Acceptor_Filter_Factory.cpp" "EndpointPolicy\EndpointPolicy_i.cpp" "EndpointPolicy\EndpointPolicy.cpp" "EndpointPolicy\Endpoint_Value_Impl.cpp" "EndpointPolicy\EndpointPolicy_Factory.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_EndpointPolicyd.pdb"
	-@del /f/q "..\..\lib\TAO_EndpointPolicyd.dll"
	-@del /f/q "$(OUTDIR)\TAO_EndpointPolicyd.lib"
	-@del /f/q "$(OUTDIR)\TAO_EndpointPolicyd.exp"
	-@del /f/q "$(OUTDIR)\TAO_EndpointPolicyd.ilk"
	-@del /f/q "EndpointPolicy\EndpointPolicyC.h"
	-@del /f/q "EndpointPolicy\EndpointPolicyS.h"
	-@del /f/q "EndpointPolicy\EndpointPolicyA.h"
	-@del /f/q "EndpointPolicy\EndpointPolicyC.cpp"
	-@del /f/q "EndpointPolicy\EndpointPolicyA.cpp"
	-@del /f/q "EndpointPolicy\IIOPEndpointValueC.h"
	-@del /f/q "EndpointPolicy\IIOPEndpointValueS.h"
	-@del /f/q "EndpointPolicy\IIOPEndpointValueA.h"
	-@del /f/q "EndpointPolicy\IIOPEndpointValueC.cpp"
	-@del /f/q "EndpointPolicy\IIOPEndpointValueA.cpp"
	-@del /f/q "EndpointPolicy\EndpointPolicyTypeC.h"
	-@del /f/q "EndpointPolicy\EndpointPolicyTypeS.h"
	-@del /f/q "EndpointPolicy\EndpointPolicyTypeA.h"
	-@del /f/q "EndpointPolicy\EndpointPolicyTypeC.cpp"
	-@del /f/q "EndpointPolicy\EndpointPolicyTypeA.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\EndpointPolicy\$(NULL)" mkdir "Debug\EndpointPolicy"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_ENDPOINTPOLICY_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_PortableServerd.lib TAO_CodecFactoryd.lib TAO_PId.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\TAO_EndpointPolicyd.pdb" /machine:IA64 /out:"..\..\lib\TAO_EndpointPolicyd.dll" /implib:"$(OUTDIR)\TAO_EndpointPolicyd.lib"
LINK32_OBJS= \
	"$(INTDIR)\EndpointPolicy\TAO_EndpointPolicy.res" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicyC.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicyA.obj" \
	"$(INTDIR)\EndpointPolicy\IIOPEndpointValueC.obj" \
	"$(INTDIR)\EndpointPolicy\IIOPEndpointValueA.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicyTypeC.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicyTypeA.obj" \
	"$(INTDIR)\EndpointPolicy\Endpoint_Acceptor_Filter.obj" \
	"$(INTDIR)\EndpointPolicy\IIOPEndpointValue_i.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicy_ORBInitializer.obj" \
	"$(INTDIR)\EndpointPolicy\Endpoint_Acceptor_Filter_Factory.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicy_i.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicy.obj" \
	"$(INTDIR)\EndpointPolicy\Endpoint_Value_Impl.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicy_Factory.obj"

"..\..\lib\TAO_EndpointPolicyd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_EndpointPolicyd.dll.manifest" mt.exe -manifest "..\..\lib\TAO_EndpointPolicyd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\EndpointPolicy\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_EndpointPolicy.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_ENDPOINTPOLICY_BUILD_DLL -f "Makefile.EndpointPolicy.dep" "EndpointPolicy\EndpointPolicyC.cpp" "EndpointPolicy\EndpointPolicyA.cpp" "EndpointPolicy\IIOPEndpointValueC.cpp" "EndpointPolicy\IIOPEndpointValueA.cpp" "EndpointPolicy\EndpointPolicyTypeC.cpp" "EndpointPolicy\EndpointPolicyTypeA.cpp" "EndpointPolicy\Endpoint_Acceptor_Filter.cpp" "EndpointPolicy\IIOPEndpointValue_i.cpp" "EndpointPolicy\EndpointPolicy_ORBInitializer.cpp" "EndpointPolicy\Endpoint_Acceptor_Filter_Factory.cpp" "EndpointPolicy\EndpointPolicy_i.cpp" "EndpointPolicy\EndpointPolicy.cpp" "EndpointPolicy\Endpoint_Value_Impl.cpp" "EndpointPolicy\EndpointPolicy_Factory.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\TAO_EndpointPolicy.dll"
	-@del /f/q "$(OUTDIR)\TAO_EndpointPolicy.lib"
	-@del /f/q "$(OUTDIR)\TAO_EndpointPolicy.exp"
	-@del /f/q "$(OUTDIR)\TAO_EndpointPolicy.ilk"
	-@del /f/q "EndpointPolicy\EndpointPolicyC.h"
	-@del /f/q "EndpointPolicy\EndpointPolicyS.h"
	-@del /f/q "EndpointPolicy\EndpointPolicyA.h"
	-@del /f/q "EndpointPolicy\EndpointPolicyC.cpp"
	-@del /f/q "EndpointPolicy\EndpointPolicyA.cpp"
	-@del /f/q "EndpointPolicy\IIOPEndpointValueC.h"
	-@del /f/q "EndpointPolicy\IIOPEndpointValueS.h"
	-@del /f/q "EndpointPolicy\IIOPEndpointValueA.h"
	-@del /f/q "EndpointPolicy\IIOPEndpointValueC.cpp"
	-@del /f/q "EndpointPolicy\IIOPEndpointValueA.cpp"
	-@del /f/q "EndpointPolicy\EndpointPolicyTypeC.h"
	-@del /f/q "EndpointPolicy\EndpointPolicyTypeS.h"
	-@del /f/q "EndpointPolicy\EndpointPolicyTypeA.h"
	-@del /f/q "EndpointPolicy\EndpointPolicyTypeC.cpp"
	-@del /f/q "EndpointPolicy\EndpointPolicyTypeA.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\EndpointPolicy\$(NULL)" mkdir "Release\EndpointPolicy"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_ENDPOINTPOLICY_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_PortableServer.lib TAO_CodecFactory.lib TAO_PI.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\TAO_EndpointPolicy.dll" /implib:"$(OUTDIR)\TAO_EndpointPolicy.lib"
LINK32_OBJS= \
	"$(INTDIR)\EndpointPolicy\TAO_EndpointPolicy.res" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicyC.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicyA.obj" \
	"$(INTDIR)\EndpointPolicy\IIOPEndpointValueC.obj" \
	"$(INTDIR)\EndpointPolicy\IIOPEndpointValueA.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicyTypeC.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicyTypeA.obj" \
	"$(INTDIR)\EndpointPolicy\Endpoint_Acceptor_Filter.obj" \
	"$(INTDIR)\EndpointPolicy\IIOPEndpointValue_i.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicy_ORBInitializer.obj" \
	"$(INTDIR)\EndpointPolicy\Endpoint_Acceptor_Filter_Factory.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicy_i.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicy.obj" \
	"$(INTDIR)\EndpointPolicy\Endpoint_Value_Impl.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicy_Factory.obj"

"..\..\lib\TAO_EndpointPolicy.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_EndpointPolicy.dll.manifest" mt.exe -manifest "..\..\lib\TAO_EndpointPolicy.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\EndpointPolicy\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_EndpointPolicysd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.EndpointPolicy.dep" "EndpointPolicy\EndpointPolicyC.cpp" "EndpointPolicy\EndpointPolicyA.cpp" "EndpointPolicy\IIOPEndpointValueC.cpp" "EndpointPolicy\IIOPEndpointValueA.cpp" "EndpointPolicy\EndpointPolicyTypeC.cpp" "EndpointPolicy\EndpointPolicyTypeA.cpp" "EndpointPolicy\Endpoint_Acceptor_Filter.cpp" "EndpointPolicy\IIOPEndpointValue_i.cpp" "EndpointPolicy\EndpointPolicy_ORBInitializer.cpp" "EndpointPolicy\Endpoint_Acceptor_Filter_Factory.cpp" "EndpointPolicy\EndpointPolicy_i.cpp" "EndpointPolicy\EndpointPolicy.cpp" "EndpointPolicy\Endpoint_Value_Impl.cpp" "EndpointPolicy\EndpointPolicy_Factory.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_EndpointPolicysd.lib"
	-@del /f/q "$(OUTDIR)\TAO_EndpointPolicysd.exp"
	-@del /f/q "$(OUTDIR)\TAO_EndpointPolicysd.ilk"
	-@del /f/q "..\..\lib\TAO_EndpointPolicysd.pdb"
	-@del /f/q "EndpointPolicy\EndpointPolicyC.h"
	-@del /f/q "EndpointPolicy\EndpointPolicyS.h"
	-@del /f/q "EndpointPolicy\EndpointPolicyA.h"
	-@del /f/q "EndpointPolicy\EndpointPolicyC.cpp"
	-@del /f/q "EndpointPolicy\EndpointPolicyA.cpp"
	-@del /f/q "EndpointPolicy\IIOPEndpointValueC.h"
	-@del /f/q "EndpointPolicy\IIOPEndpointValueS.h"
	-@del /f/q "EndpointPolicy\IIOPEndpointValueA.h"
	-@del /f/q "EndpointPolicy\IIOPEndpointValueC.cpp"
	-@del /f/q "EndpointPolicy\IIOPEndpointValueA.cpp"
	-@del /f/q "EndpointPolicy\EndpointPolicyTypeC.h"
	-@del /f/q "EndpointPolicy\EndpointPolicyTypeS.h"
	-@del /f/q "EndpointPolicy\EndpointPolicyTypeA.h"
	-@del /f/q "EndpointPolicy\EndpointPolicyTypeC.cpp"
	-@del /f/q "EndpointPolicy\EndpointPolicyTypeA.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\EndpointPolicy\$(NULL)" mkdir "Static_Debug\EndpointPolicy"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"..\..\lib\TAO_EndpointPolicysd.pdb" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_EndpointPolicysd.lib"
LINK32_OBJS= \
	"$(INTDIR)\EndpointPolicy\EndpointPolicyC.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicyA.obj" \
	"$(INTDIR)\EndpointPolicy\IIOPEndpointValueC.obj" \
	"$(INTDIR)\EndpointPolicy\IIOPEndpointValueA.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicyTypeC.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicyTypeA.obj" \
	"$(INTDIR)\EndpointPolicy\Endpoint_Acceptor_Filter.obj" \
	"$(INTDIR)\EndpointPolicy\IIOPEndpointValue_i.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicy_ORBInitializer.obj" \
	"$(INTDIR)\EndpointPolicy\Endpoint_Acceptor_Filter_Factory.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicy_i.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicy.obj" \
	"$(INTDIR)\EndpointPolicy\Endpoint_Value_Impl.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicy_Factory.obj"

"$(OUTDIR)\TAO_EndpointPolicysd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_EndpointPolicysd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_EndpointPolicysd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\EndpointPolicy\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_EndpointPolicys.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.EndpointPolicy.dep" "EndpointPolicy\EndpointPolicyC.cpp" "EndpointPolicy\EndpointPolicyA.cpp" "EndpointPolicy\IIOPEndpointValueC.cpp" "EndpointPolicy\IIOPEndpointValueA.cpp" "EndpointPolicy\EndpointPolicyTypeC.cpp" "EndpointPolicy\EndpointPolicyTypeA.cpp" "EndpointPolicy\Endpoint_Acceptor_Filter.cpp" "EndpointPolicy\IIOPEndpointValue_i.cpp" "EndpointPolicy\EndpointPolicy_ORBInitializer.cpp" "EndpointPolicy\Endpoint_Acceptor_Filter_Factory.cpp" "EndpointPolicy\EndpointPolicy_i.cpp" "EndpointPolicy\EndpointPolicy.cpp" "EndpointPolicy\Endpoint_Value_Impl.cpp" "EndpointPolicy\EndpointPolicy_Factory.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_EndpointPolicys.lib"
	-@del /f/q "$(OUTDIR)\TAO_EndpointPolicys.exp"
	-@del /f/q "$(OUTDIR)\TAO_EndpointPolicys.ilk"
	-@del /f/q "EndpointPolicy\EndpointPolicyC.h"
	-@del /f/q "EndpointPolicy\EndpointPolicyS.h"
	-@del /f/q "EndpointPolicy\EndpointPolicyA.h"
	-@del /f/q "EndpointPolicy\EndpointPolicyC.cpp"
	-@del /f/q "EndpointPolicy\EndpointPolicyA.cpp"
	-@del /f/q "EndpointPolicy\IIOPEndpointValueC.h"
	-@del /f/q "EndpointPolicy\IIOPEndpointValueS.h"
	-@del /f/q "EndpointPolicy\IIOPEndpointValueA.h"
	-@del /f/q "EndpointPolicy\IIOPEndpointValueC.cpp"
	-@del /f/q "EndpointPolicy\IIOPEndpointValueA.cpp"
	-@del /f/q "EndpointPolicy\EndpointPolicyTypeC.h"
	-@del /f/q "EndpointPolicy\EndpointPolicyTypeS.h"
	-@del /f/q "EndpointPolicy\EndpointPolicyTypeA.h"
	-@del /f/q "EndpointPolicy\EndpointPolicyTypeC.cpp"
	-@del /f/q "EndpointPolicy\EndpointPolicyTypeA.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\EndpointPolicy\$(NULL)" mkdir "Static_Release\EndpointPolicy"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_EndpointPolicys.lib"
LINK32_OBJS= \
	"$(INTDIR)\EndpointPolicy\EndpointPolicyC.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicyA.obj" \
	"$(INTDIR)\EndpointPolicy\IIOPEndpointValueC.obj" \
	"$(INTDIR)\EndpointPolicy\IIOPEndpointValueA.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicyTypeC.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicyTypeA.obj" \
	"$(INTDIR)\EndpointPolicy\Endpoint_Acceptor_Filter.obj" \
	"$(INTDIR)\EndpointPolicy\IIOPEndpointValue_i.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicy_ORBInitializer.obj" \
	"$(INTDIR)\EndpointPolicy\Endpoint_Acceptor_Filter_Factory.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicy_i.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicy.obj" \
	"$(INTDIR)\EndpointPolicy\Endpoint_Value_Impl.obj" \
	"$(INTDIR)\EndpointPolicy\EndpointPolicy_Factory.obj"

"$(OUTDIR)\TAO_EndpointPolicys.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_EndpointPolicys.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_EndpointPolicys.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.EndpointPolicy.dep")
!INCLUDE "Makefile.EndpointPolicy.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="EndpointPolicy\EndpointPolicyC.cpp"

"$(INTDIR)\EndpointPolicy\EndpointPolicyC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\EndpointPolicy\$(NULL)" mkdir "$(INTDIR)\EndpointPolicy\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\EndpointPolicy\EndpointPolicyC.obj" $(SOURCE)

SOURCE="EndpointPolicy\EndpointPolicyA.cpp"

"$(INTDIR)\EndpointPolicy\EndpointPolicyA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\EndpointPolicy\$(NULL)" mkdir "$(INTDIR)\EndpointPolicy\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\EndpointPolicy\EndpointPolicyA.obj" $(SOURCE)

SOURCE="EndpointPolicy\IIOPEndpointValueC.cpp"

"$(INTDIR)\EndpointPolicy\IIOPEndpointValueC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\EndpointPolicy\$(NULL)" mkdir "$(INTDIR)\EndpointPolicy\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\EndpointPolicy\IIOPEndpointValueC.obj" $(SOURCE)

SOURCE="EndpointPolicy\IIOPEndpointValueA.cpp"

"$(INTDIR)\EndpointPolicy\IIOPEndpointValueA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\EndpointPolicy\$(NULL)" mkdir "$(INTDIR)\EndpointPolicy\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\EndpointPolicy\IIOPEndpointValueA.obj" $(SOURCE)

SOURCE="EndpointPolicy\EndpointPolicyTypeC.cpp"

"$(INTDIR)\EndpointPolicy\EndpointPolicyTypeC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\EndpointPolicy\$(NULL)" mkdir "$(INTDIR)\EndpointPolicy\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\EndpointPolicy\EndpointPolicyTypeC.obj" $(SOURCE)

SOURCE="EndpointPolicy\EndpointPolicyTypeA.cpp"

"$(INTDIR)\EndpointPolicy\EndpointPolicyTypeA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\EndpointPolicy\$(NULL)" mkdir "$(INTDIR)\EndpointPolicy\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\EndpointPolicy\EndpointPolicyTypeA.obj" $(SOURCE)

SOURCE="EndpointPolicy\Endpoint_Acceptor_Filter.cpp"

"$(INTDIR)\EndpointPolicy\Endpoint_Acceptor_Filter.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\EndpointPolicy\$(NULL)" mkdir "$(INTDIR)\EndpointPolicy\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\EndpointPolicy\Endpoint_Acceptor_Filter.obj" $(SOURCE)

SOURCE="EndpointPolicy\IIOPEndpointValue_i.cpp"

"$(INTDIR)\EndpointPolicy\IIOPEndpointValue_i.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\EndpointPolicy\$(NULL)" mkdir "$(INTDIR)\EndpointPolicy\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\EndpointPolicy\IIOPEndpointValue_i.obj" $(SOURCE)

SOURCE="EndpointPolicy\EndpointPolicy_ORBInitializer.cpp"

"$(INTDIR)\EndpointPolicy\EndpointPolicy_ORBInitializer.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\EndpointPolicy\$(NULL)" mkdir "$(INTDIR)\EndpointPolicy\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\EndpointPolicy\EndpointPolicy_ORBInitializer.obj" $(SOURCE)

SOURCE="EndpointPolicy\Endpoint_Acceptor_Filter_Factory.cpp"

"$(INTDIR)\EndpointPolicy\Endpoint_Acceptor_Filter_Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\EndpointPolicy\$(NULL)" mkdir "$(INTDIR)\EndpointPolicy\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\EndpointPolicy\Endpoint_Acceptor_Filter_Factory.obj" $(SOURCE)

SOURCE="EndpointPolicy\EndpointPolicy_i.cpp"

"$(INTDIR)\EndpointPolicy\EndpointPolicy_i.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\EndpointPolicy\$(NULL)" mkdir "$(INTDIR)\EndpointPolicy\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\EndpointPolicy\EndpointPolicy_i.obj" $(SOURCE)

SOURCE="EndpointPolicy\EndpointPolicy.cpp"

"$(INTDIR)\EndpointPolicy\EndpointPolicy.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\EndpointPolicy\$(NULL)" mkdir "$(INTDIR)\EndpointPolicy\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\EndpointPolicy\EndpointPolicy.obj" $(SOURCE)

SOURCE="EndpointPolicy\Endpoint_Value_Impl.cpp"

"$(INTDIR)\EndpointPolicy\Endpoint_Value_Impl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\EndpointPolicy\$(NULL)" mkdir "$(INTDIR)\EndpointPolicy\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\EndpointPolicy\Endpoint_Value_Impl.obj" $(SOURCE)

SOURCE="EndpointPolicy\EndpointPolicy_Factory.cpp"

"$(INTDIR)\EndpointPolicy\EndpointPolicy_Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\EndpointPolicy\$(NULL)" mkdir "$(INTDIR)\EndpointPolicy\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\EndpointPolicy\EndpointPolicy_Factory.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="EndpointPolicy\EndpointPolicy.pidl"

InputPath=EndpointPolicy\EndpointPolicy.pidl

"EndpointPolicy\EndpointPolicyC.h" "EndpointPolicy\EndpointPolicyS.h" "EndpointPolicy\EndpointPolicyA.h" "EndpointPolicy\EndpointPolicyC.cpp" "EndpointPolicy\EndpointPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-EndpointPolicy_EndpointPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -GA -SS -Sci -Sorb -Wb,export_macro=TAO_EndpointPolicy_Export -Wb,export_include=tao/EndpointPolicy/EndpointPolicy_Export.h -o EndpointPolicy "$(InputPath)"
<<

SOURCE="EndpointPolicy\IIOPEndpointValue.pidl"

InputPath=EndpointPolicy\IIOPEndpointValue.pidl

"EndpointPolicy\IIOPEndpointValueC.h" "EndpointPolicy\IIOPEndpointValueS.h" "EndpointPolicy\IIOPEndpointValueA.h" "EndpointPolicy\IIOPEndpointValueC.cpp" "EndpointPolicy\IIOPEndpointValueA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-EndpointPolicy_IIOPEndpointValue_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -GA -SS -Sci -Sorb -Wb,export_macro=TAO_EndpointPolicy_Export -Wb,export_include=tao/EndpointPolicy/EndpointPolicy_Export.h -o EndpointPolicy "$(InputPath)"
<<

SOURCE="EndpointPolicy\EndpointPolicyType.pidl"

InputPath=EndpointPolicy\EndpointPolicyType.pidl

"EndpointPolicy\EndpointPolicyTypeC.h" "EndpointPolicy\EndpointPolicyTypeS.h" "EndpointPolicy\EndpointPolicyTypeA.h" "EndpointPolicy\EndpointPolicyTypeC.cpp" "EndpointPolicy\EndpointPolicyTypeA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-EndpointPolicy_EndpointPolicyType_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -GA -SS -Sci -Sorb -Wb,export_macro=TAO_EndpointPolicy_Export -Wb,export_include=tao/EndpointPolicy/EndpointPolicy_Export.h -o EndpointPolicy "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="EndpointPolicy\EndpointPolicy.pidl"

InputPath=EndpointPolicy\EndpointPolicy.pidl

"EndpointPolicy\EndpointPolicyC.h" "EndpointPolicy\EndpointPolicyS.h" "EndpointPolicy\EndpointPolicyA.h" "EndpointPolicy\EndpointPolicyC.cpp" "EndpointPolicy\EndpointPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-EndpointPolicy_EndpointPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -GA -SS -Sci -Sorb -Wb,export_macro=TAO_EndpointPolicy_Export -Wb,export_include=tao/EndpointPolicy/EndpointPolicy_Export.h -o EndpointPolicy "$(InputPath)"
<<

SOURCE="EndpointPolicy\IIOPEndpointValue.pidl"

InputPath=EndpointPolicy\IIOPEndpointValue.pidl

"EndpointPolicy\IIOPEndpointValueC.h" "EndpointPolicy\IIOPEndpointValueS.h" "EndpointPolicy\IIOPEndpointValueA.h" "EndpointPolicy\IIOPEndpointValueC.cpp" "EndpointPolicy\IIOPEndpointValueA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-EndpointPolicy_IIOPEndpointValue_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -GA -SS -Sci -Sorb -Wb,export_macro=TAO_EndpointPolicy_Export -Wb,export_include=tao/EndpointPolicy/EndpointPolicy_Export.h -o EndpointPolicy "$(InputPath)"
<<

SOURCE="EndpointPolicy\EndpointPolicyType.pidl"

InputPath=EndpointPolicy\EndpointPolicyType.pidl

"EndpointPolicy\EndpointPolicyTypeC.h" "EndpointPolicy\EndpointPolicyTypeS.h" "EndpointPolicy\EndpointPolicyTypeA.h" "EndpointPolicy\EndpointPolicyTypeC.cpp" "EndpointPolicy\EndpointPolicyTypeA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-EndpointPolicy_EndpointPolicyType_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -GA -SS -Sci -Sorb -Wb,export_macro=TAO_EndpointPolicy_Export -Wb,export_include=tao/EndpointPolicy/EndpointPolicy_Export.h -o EndpointPolicy "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="EndpointPolicy\EndpointPolicy.pidl"

InputPath=EndpointPolicy\EndpointPolicy.pidl

"EndpointPolicy\EndpointPolicyC.h" "EndpointPolicy\EndpointPolicyS.h" "EndpointPolicy\EndpointPolicyA.h" "EndpointPolicy\EndpointPolicyC.cpp" "EndpointPolicy\EndpointPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-EndpointPolicy_EndpointPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -GA -SS -Sci -Sorb -Wb,export_macro=TAO_EndpointPolicy_Export -Wb,export_include=tao/EndpointPolicy/EndpointPolicy_Export.h -o EndpointPolicy "$(InputPath)"
<<

SOURCE="EndpointPolicy\IIOPEndpointValue.pidl"

InputPath=EndpointPolicy\IIOPEndpointValue.pidl

"EndpointPolicy\IIOPEndpointValueC.h" "EndpointPolicy\IIOPEndpointValueS.h" "EndpointPolicy\IIOPEndpointValueA.h" "EndpointPolicy\IIOPEndpointValueC.cpp" "EndpointPolicy\IIOPEndpointValueA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-EndpointPolicy_IIOPEndpointValue_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -GA -SS -Sci -Sorb -Wb,export_macro=TAO_EndpointPolicy_Export -Wb,export_include=tao/EndpointPolicy/EndpointPolicy_Export.h -o EndpointPolicy "$(InputPath)"
<<

SOURCE="EndpointPolicy\EndpointPolicyType.pidl"

InputPath=EndpointPolicy\EndpointPolicyType.pidl

"EndpointPolicy\EndpointPolicyTypeC.h" "EndpointPolicy\EndpointPolicyTypeS.h" "EndpointPolicy\EndpointPolicyTypeA.h" "EndpointPolicy\EndpointPolicyTypeC.cpp" "EndpointPolicy\EndpointPolicyTypeA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-EndpointPolicy_EndpointPolicyType_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -GA -SS -Sci -Sorb -Wb,export_macro=TAO_EndpointPolicy_Export -Wb,export_include=tao/EndpointPolicy/EndpointPolicy_Export.h -o EndpointPolicy "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="EndpointPolicy\EndpointPolicy.pidl"

InputPath=EndpointPolicy\EndpointPolicy.pidl

"EndpointPolicy\EndpointPolicyC.h" "EndpointPolicy\EndpointPolicyS.h" "EndpointPolicy\EndpointPolicyA.h" "EndpointPolicy\EndpointPolicyC.cpp" "EndpointPolicy\EndpointPolicyA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-EndpointPolicy_EndpointPolicy_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -GA -SS -Sci -Sorb -Wb,export_macro=TAO_EndpointPolicy_Export -Wb,export_include=tao/EndpointPolicy/EndpointPolicy_Export.h -o EndpointPolicy "$(InputPath)"
<<

SOURCE="EndpointPolicy\IIOPEndpointValue.pidl"

InputPath=EndpointPolicy\IIOPEndpointValue.pidl

"EndpointPolicy\IIOPEndpointValueC.h" "EndpointPolicy\IIOPEndpointValueS.h" "EndpointPolicy\IIOPEndpointValueA.h" "EndpointPolicy\IIOPEndpointValueC.cpp" "EndpointPolicy\IIOPEndpointValueA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-EndpointPolicy_IIOPEndpointValue_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -GA -SS -Sci -Sorb -Wb,export_macro=TAO_EndpointPolicy_Export -Wb,export_include=tao/EndpointPolicy/EndpointPolicy_Export.h -o EndpointPolicy "$(InputPath)"
<<

SOURCE="EndpointPolicy\EndpointPolicyType.pidl"

InputPath=EndpointPolicy\EndpointPolicyType.pidl

"EndpointPolicy\EndpointPolicyTypeC.h" "EndpointPolicy\EndpointPolicyTypeS.h" "EndpointPolicy\EndpointPolicyTypeA.h" "EndpointPolicy\EndpointPolicyTypeC.cpp" "EndpointPolicy\EndpointPolicyTypeA.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-EndpointPolicy_EndpointPolicyType_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -GA -SS -Sci -Sorb -Wb,export_macro=TAO_EndpointPolicy_Export -Wb,export_include=tao/EndpointPolicy/EndpointPolicy_Export.h -o EndpointPolicy "$(InputPath)"
<<

!ENDIF

SOURCE="EndpointPolicy\TAO_EndpointPolicy.rc"

"$(INTDIR)\EndpointPolicy\TAO_EndpointPolicy.res" : $(SOURCE)
	@if not exist "$(INTDIR)\EndpointPolicy\$(NULL)" mkdir "$(INTDIR)\EndpointPolicy\"
	$(RSC) /l 0x409 /fo"$(INTDIR)\EndpointPolicy\TAO_EndpointPolicy.res" /d NDEBUG /d WIN64 /d _CRT_SECURE_NO_WARNINGS /d _CRT_SECURE_NO_DEPRECATE /d _CRT_NONSTDC_NO_DEPRECATE /i "..\.." /i ".." $(SOURCE)



!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.EndpointPolicy.dep")
	@echo Using "Makefile.EndpointPolicy.dep"
!ELSE
	@echo Warning: cannot find "Makefile.EndpointPolicy.dep"
!ENDIF
!ENDIF

