// $Id: IORInterceptor_Adapter.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/IORInterceptor_Adapter.h"

ACE_RCSID (tao,
           IORInterceptor_Adapter,
           "$Id: IORInterceptor_Adapter.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_IORInterceptor_Adapter::~TAO_IORInterceptor_Adapter (void)
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
