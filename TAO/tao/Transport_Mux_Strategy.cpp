// $Id: Transport_Mux_Strategy.cpp 935 2008-12-10 21:47:27Z mitza $

#include "tao/Transport_Mux_Strategy.h"

ACE_RCSID (tao,
           Transport_Mux_Strategy,
           "$Id: Transport_Mux_Strategy.cpp 935 2008-12-10 21:47:27Z mitza $")


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Transport_Mux_Strategy::TAO_Transport_Mux_Strategy (
  TAO_Transport *transport)
  : transport_ (transport)
{
}

TAO_Transport_Mux_Strategy::~TAO_Transport_Mux_Strategy (void)
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
