# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.RTPortableServer.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "RTPortableServer\RTPortableServerC.h" "RTPortableServer\RTPortableServerS.h" "RTPortableServer\RTPortableServerA.h" "RTPortableServer\RTPortableServerC.cpp" "RTPortableServer\RTPortableServer_includeC.h" "RTPortableServer\RTPortableServer_includeS.h" "RTPortableServer\RTPortableServer_includeA.h" "RTPortableServer\RTPortableServer_includeC.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\RTPortableServer\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_RTPortableServerd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_RTPORTABLESERVER_BUILD_DLL -f "Makefile.RTPortableServer.dep" "RTPortableServer\RTPortableServerC.cpp" "RTPortableServer\RTPortableServer_includeC.cpp" "RTPortableServer\RT_Servant_Dispatcher.cpp" "RTPortableServer\RT_Object_Adapter_Factory.cpp" "RTPortableServer\RT_Policy_Validator.cpp" "RTPortableServer\RT_Collocation_Resolver.cpp" "RTPortableServer\RT_Acceptor_Filters.cpp" "RTPortableServer\RTPortableServer.cpp" "RTPortableServer\RT_POA.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_RTPortableServerd.pdb"
	-@del /f/q "..\..\lib\TAO_RTPortableServerd.dll"
	-@del /f/q "$(OUTDIR)\TAO_RTPortableServerd.lib"
	-@del /f/q "$(OUTDIR)\TAO_RTPortableServerd.exp"
	-@del /f/q "$(OUTDIR)\TAO_RTPortableServerd.ilk"
	-@del /f/q "RTPortableServer\RTPortableServerC.h"
	-@del /f/q "RTPortableServer\RTPortableServerS.h"
	-@del /f/q "RTPortableServer\RTPortableServerA.h"
	-@del /f/q "RTPortableServer\RTPortableServerC.cpp"
	-@del /f/q "RTPortableServer\RTPortableServer_includeC.h"
	-@del /f/q "RTPortableServer\RTPortableServer_includeS.h"
	-@del /f/q "RTPortableServer\RTPortableServer_includeA.h"
	-@del /f/q "RTPortableServer\RTPortableServer_includeC.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\RTPortableServer\$(NULL)" mkdir "Debug\RTPortableServer"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_RTPORTABLESERVER_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_CodecFactoryd.lib TAO_PId.lib TAO_RTCORBAd.lib TAO_PortableServerd.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\TAO_RTPortableServerd.pdb" /machine:IA64 /out:"..\..\lib\TAO_RTPortableServerd.dll" /implib:"$(OUTDIR)\TAO_RTPortableServerd.lib"
LINK32_OBJS= \
	"$(INTDIR)\RTPortableServer\TAO_RTPortableServer.res" \
	"$(INTDIR)\RTPortableServer\RTPortableServerC.obj" \
	"$(INTDIR)\RTPortableServer\RTPortableServer_includeC.obj" \
	"$(INTDIR)\RTPortableServer\RT_Servant_Dispatcher.obj" \
	"$(INTDIR)\RTPortableServer\RT_Object_Adapter_Factory.obj" \
	"$(INTDIR)\RTPortableServer\RT_Policy_Validator.obj" \
	"$(INTDIR)\RTPortableServer\RT_Collocation_Resolver.obj" \
	"$(INTDIR)\RTPortableServer\RT_Acceptor_Filters.obj" \
	"$(INTDIR)\RTPortableServer\RTPortableServer.obj" \
	"$(INTDIR)\RTPortableServer\RT_POA.obj"

"..\..\lib\TAO_RTPortableServerd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_RTPortableServerd.dll.manifest" mt.exe -manifest "..\..\lib\TAO_RTPortableServerd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\RTPortableServer\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_RTPortableServer.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_RTPORTABLESERVER_BUILD_DLL -f "Makefile.RTPortableServer.dep" "RTPortableServer\RTPortableServerC.cpp" "RTPortableServer\RTPortableServer_includeC.cpp" "RTPortableServer\RT_Servant_Dispatcher.cpp" "RTPortableServer\RT_Object_Adapter_Factory.cpp" "RTPortableServer\RT_Policy_Validator.cpp" "RTPortableServer\RT_Collocation_Resolver.cpp" "RTPortableServer\RT_Acceptor_Filters.cpp" "RTPortableServer\RTPortableServer.cpp" "RTPortableServer\RT_POA.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\TAO_RTPortableServer.dll"
	-@del /f/q "$(OUTDIR)\TAO_RTPortableServer.lib"
	-@del /f/q "$(OUTDIR)\TAO_RTPortableServer.exp"
	-@del /f/q "$(OUTDIR)\TAO_RTPortableServer.ilk"
	-@del /f/q "RTPortableServer\RTPortableServerC.h"
	-@del /f/q "RTPortableServer\RTPortableServerS.h"
	-@del /f/q "RTPortableServer\RTPortableServerA.h"
	-@del /f/q "RTPortableServer\RTPortableServerC.cpp"
	-@del /f/q "RTPortableServer\RTPortableServer_includeC.h"
	-@del /f/q "RTPortableServer\RTPortableServer_includeS.h"
	-@del /f/q "RTPortableServer\RTPortableServer_includeA.h"
	-@del /f/q "RTPortableServer\RTPortableServer_includeC.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\RTPortableServer\$(NULL)" mkdir "Release\RTPortableServer"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_RTPORTABLESERVER_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_CodecFactory.lib TAO_PI.lib TAO_RTCORBA.lib TAO_PortableServer.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\TAO_RTPortableServer.dll" /implib:"$(OUTDIR)\TAO_RTPortableServer.lib"
LINK32_OBJS= \
	"$(INTDIR)\RTPortableServer\TAO_RTPortableServer.res" \
	"$(INTDIR)\RTPortableServer\RTPortableServerC.obj" \
	"$(INTDIR)\RTPortableServer\RTPortableServer_includeC.obj" \
	"$(INTDIR)\RTPortableServer\RT_Servant_Dispatcher.obj" \
	"$(INTDIR)\RTPortableServer\RT_Object_Adapter_Factory.obj" \
	"$(INTDIR)\RTPortableServer\RT_Policy_Validator.obj" \
	"$(INTDIR)\RTPortableServer\RT_Collocation_Resolver.obj" \
	"$(INTDIR)\RTPortableServer\RT_Acceptor_Filters.obj" \
	"$(INTDIR)\RTPortableServer\RTPortableServer.obj" \
	"$(INTDIR)\RTPortableServer\RT_POA.obj"

"..\..\lib\TAO_RTPortableServer.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_RTPortableServer.dll.manifest" mt.exe -manifest "..\..\lib\TAO_RTPortableServer.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\RTPortableServer\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_RTPortableServersd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.RTPortableServer.dep" "RTPortableServer\RTPortableServerC.cpp" "RTPortableServer\RTPortableServer_includeC.cpp" "RTPortableServer\RT_Servant_Dispatcher.cpp" "RTPortableServer\RT_Object_Adapter_Factory.cpp" "RTPortableServer\RT_Policy_Validator.cpp" "RTPortableServer\RT_Collocation_Resolver.cpp" "RTPortableServer\RT_Acceptor_Filters.cpp" "RTPortableServer\RTPortableServer.cpp" "RTPortableServer\RT_POA.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_RTPortableServersd.lib"
	-@del /f/q "$(OUTDIR)\TAO_RTPortableServersd.exp"
	-@del /f/q "$(OUTDIR)\TAO_RTPortableServersd.ilk"
	-@del /f/q "..\..\lib\TAO_RTPortableServersd.pdb"
	-@del /f/q "RTPortableServer\RTPortableServerC.h"
	-@del /f/q "RTPortableServer\RTPortableServerS.h"
	-@del /f/q "RTPortableServer\RTPortableServerA.h"
	-@del /f/q "RTPortableServer\RTPortableServerC.cpp"
	-@del /f/q "RTPortableServer\RTPortableServer_includeC.h"
	-@del /f/q "RTPortableServer\RTPortableServer_includeS.h"
	-@del /f/q "RTPortableServer\RTPortableServer_includeA.h"
	-@del /f/q "RTPortableServer\RTPortableServer_includeC.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\RTPortableServer\$(NULL)" mkdir "Static_Debug\RTPortableServer"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"..\..\lib\TAO_RTPortableServersd.pdb" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_RTPortableServersd.lib"
LINK32_OBJS= \
	"$(INTDIR)\RTPortableServer\RTPortableServerC.obj" \
	"$(INTDIR)\RTPortableServer\RTPortableServer_includeC.obj" \
	"$(INTDIR)\RTPortableServer\RT_Servant_Dispatcher.obj" \
	"$(INTDIR)\RTPortableServer\RT_Object_Adapter_Factory.obj" \
	"$(INTDIR)\RTPortableServer\RT_Policy_Validator.obj" \
	"$(INTDIR)\RTPortableServer\RT_Collocation_Resolver.obj" \
	"$(INTDIR)\RTPortableServer\RT_Acceptor_Filters.obj" \
	"$(INTDIR)\RTPortableServer\RTPortableServer.obj" \
	"$(INTDIR)\RTPortableServer\RT_POA.obj"

"$(OUTDIR)\TAO_RTPortableServersd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_RTPortableServersd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_RTPortableServersd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\RTPortableServer\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_RTPortableServers.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.RTPortableServer.dep" "RTPortableServer\RTPortableServerC.cpp" "RTPortableServer\RTPortableServer_includeC.cpp" "RTPortableServer\RT_Servant_Dispatcher.cpp" "RTPortableServer\RT_Object_Adapter_Factory.cpp" "RTPortableServer\RT_Policy_Validator.cpp" "RTPortableServer\RT_Collocation_Resolver.cpp" "RTPortableServer\RT_Acceptor_Filters.cpp" "RTPortableServer\RTPortableServer.cpp" "RTPortableServer\RT_POA.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_RTPortableServers.lib"
	-@del /f/q "$(OUTDIR)\TAO_RTPortableServers.exp"
	-@del /f/q "$(OUTDIR)\TAO_RTPortableServers.ilk"
	-@del /f/q "RTPortableServer\RTPortableServerC.h"
	-@del /f/q "RTPortableServer\RTPortableServerS.h"
	-@del /f/q "RTPortableServer\RTPortableServerA.h"
	-@del /f/q "RTPortableServer\RTPortableServerC.cpp"
	-@del /f/q "RTPortableServer\RTPortableServer_includeC.h"
	-@del /f/q "RTPortableServer\RTPortableServer_includeS.h"
	-@del /f/q "RTPortableServer\RTPortableServer_includeA.h"
	-@del /f/q "RTPortableServer\RTPortableServer_includeC.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\RTPortableServer\$(NULL)" mkdir "Static_Release\RTPortableServer"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_RTPortableServers.lib"
LINK32_OBJS= \
	"$(INTDIR)\RTPortableServer\RTPortableServerC.obj" \
	"$(INTDIR)\RTPortableServer\RTPortableServer_includeC.obj" \
	"$(INTDIR)\RTPortableServer\RT_Servant_Dispatcher.obj" \
	"$(INTDIR)\RTPortableServer\RT_Object_Adapter_Factory.obj" \
	"$(INTDIR)\RTPortableServer\RT_Policy_Validator.obj" \
	"$(INTDIR)\RTPortableServer\RT_Collocation_Resolver.obj" \
	"$(INTDIR)\RTPortableServer\RT_Acceptor_Filters.obj" \
	"$(INTDIR)\RTPortableServer\RTPortableServer.obj" \
	"$(INTDIR)\RTPortableServer\RT_POA.obj"

"$(OUTDIR)\TAO_RTPortableServers.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_RTPortableServers.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_RTPortableServers.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.RTPortableServer.dep")
!INCLUDE "Makefile.RTPortableServer.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="RTPortableServer\RTPortableServerC.cpp"

"$(INTDIR)\RTPortableServer\RTPortableServerC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTPortableServer\$(NULL)" mkdir "$(INTDIR)\RTPortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTPortableServer\RTPortableServerC.obj" $(SOURCE)

SOURCE="RTPortableServer\RTPortableServer_includeC.cpp"

"$(INTDIR)\RTPortableServer\RTPortableServer_includeC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTPortableServer\$(NULL)" mkdir "$(INTDIR)\RTPortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTPortableServer\RTPortableServer_includeC.obj" $(SOURCE)

SOURCE="RTPortableServer\RT_Servant_Dispatcher.cpp"

"$(INTDIR)\RTPortableServer\RT_Servant_Dispatcher.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTPortableServer\$(NULL)" mkdir "$(INTDIR)\RTPortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTPortableServer\RT_Servant_Dispatcher.obj" $(SOURCE)

SOURCE="RTPortableServer\RT_Object_Adapter_Factory.cpp"

"$(INTDIR)\RTPortableServer\RT_Object_Adapter_Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTPortableServer\$(NULL)" mkdir "$(INTDIR)\RTPortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTPortableServer\RT_Object_Adapter_Factory.obj" $(SOURCE)

SOURCE="RTPortableServer\RT_Policy_Validator.cpp"

"$(INTDIR)\RTPortableServer\RT_Policy_Validator.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTPortableServer\$(NULL)" mkdir "$(INTDIR)\RTPortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTPortableServer\RT_Policy_Validator.obj" $(SOURCE)

SOURCE="RTPortableServer\RT_Collocation_Resolver.cpp"

"$(INTDIR)\RTPortableServer\RT_Collocation_Resolver.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTPortableServer\$(NULL)" mkdir "$(INTDIR)\RTPortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTPortableServer\RT_Collocation_Resolver.obj" $(SOURCE)

SOURCE="RTPortableServer\RT_Acceptor_Filters.cpp"

"$(INTDIR)\RTPortableServer\RT_Acceptor_Filters.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTPortableServer\$(NULL)" mkdir "$(INTDIR)\RTPortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTPortableServer\RT_Acceptor_Filters.obj" $(SOURCE)

SOURCE="RTPortableServer\RTPortableServer.cpp"

"$(INTDIR)\RTPortableServer\RTPortableServer.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTPortableServer\$(NULL)" mkdir "$(INTDIR)\RTPortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTPortableServer\RTPortableServer.obj" $(SOURCE)

SOURCE="RTPortableServer\RT_POA.cpp"

"$(INTDIR)\RTPortableServer\RT_POA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTPortableServer\$(NULL)" mkdir "$(INTDIR)\RTPortableServer\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTPortableServer\RT_POA.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="RTPortableServer\RTPortableServer.pidl"

InputPath=RTPortableServer\RTPortableServer.pidl

"RTPortableServer\RTPortableServerC.h" "RTPortableServer\RTPortableServerS.h" "RTPortableServer\RTPortableServerA.h" "RTPortableServer\RTPortableServerC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-RTPortableServer_RTPortableServer_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sci -SS -Sorb -GX -Wb,export_macro=TAO_RTPortableServer_Export -Wb,export_include=tao/RTPortableServer/rtportableserver_export.h -o RTPortableServer -Gp -Gd -Wb,include_guard=TAO_RT_PORTABLESERVER_SAFE_INCLUDE -Wb,safe_include=tao/RTPortableServer/RTPortableServer.h "$(InputPath)"
<<

SOURCE="RTPortableServer\RTPortableServer_include.pidl"

InputPath=RTPortableServer\RTPortableServer_include.pidl

"RTPortableServer\RTPortableServer_includeC.h" "RTPortableServer\RTPortableServer_includeS.h" "RTPortableServer\RTPortableServer_includeA.h" "RTPortableServer\RTPortableServer_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-RTPortableServer_RTPortableServer_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sci -SS -Sorb -GX -Wb,export_macro=TAO_RTPortableServer_Export -Wb,export_include=tao/RTPortableServer/rtportableserver_export.h -o RTPortableServer -Sa -Wb,unique_include=tao/RTPortableServer/RTPortableServer.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="RTPortableServer\RTPortableServer.pidl"

InputPath=RTPortableServer\RTPortableServer.pidl

"RTPortableServer\RTPortableServerC.h" "RTPortableServer\RTPortableServerS.h" "RTPortableServer\RTPortableServerA.h" "RTPortableServer\RTPortableServerC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-RTPortableServer_RTPortableServer_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sci -SS -Sorb -GX -Wb,export_macro=TAO_RTPortableServer_Export -Wb,export_include=tao/RTPortableServer/rtportableserver_export.h -o RTPortableServer -Gp -Gd -Wb,include_guard=TAO_RT_PORTABLESERVER_SAFE_INCLUDE -Wb,safe_include=tao/RTPortableServer/RTPortableServer.h "$(InputPath)"
<<

SOURCE="RTPortableServer\RTPortableServer_include.pidl"

InputPath=RTPortableServer\RTPortableServer_include.pidl

"RTPortableServer\RTPortableServer_includeC.h" "RTPortableServer\RTPortableServer_includeS.h" "RTPortableServer\RTPortableServer_includeA.h" "RTPortableServer\RTPortableServer_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-RTPortableServer_RTPortableServer_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sci -SS -Sorb -GX -Wb,export_macro=TAO_RTPortableServer_Export -Wb,export_include=tao/RTPortableServer/rtportableserver_export.h -o RTPortableServer -Sa -Wb,unique_include=tao/RTPortableServer/RTPortableServer.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="RTPortableServer\RTPortableServer.pidl"

InputPath=RTPortableServer\RTPortableServer.pidl

"RTPortableServer\RTPortableServerC.h" "RTPortableServer\RTPortableServerS.h" "RTPortableServer\RTPortableServerA.h" "RTPortableServer\RTPortableServerC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-RTPortableServer_RTPortableServer_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sci -SS -Sorb -GX -Wb,export_macro=TAO_RTPortableServer_Export -Wb,export_include=tao/RTPortableServer/rtportableserver_export.h -o RTPortableServer -Gp -Gd -Wb,include_guard=TAO_RT_PORTABLESERVER_SAFE_INCLUDE -Wb,safe_include=tao/RTPortableServer/RTPortableServer.h "$(InputPath)"
<<

SOURCE="RTPortableServer\RTPortableServer_include.pidl"

InputPath=RTPortableServer\RTPortableServer_include.pidl

"RTPortableServer\RTPortableServer_includeC.h" "RTPortableServer\RTPortableServer_includeS.h" "RTPortableServer\RTPortableServer_includeA.h" "RTPortableServer\RTPortableServer_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-RTPortableServer_RTPortableServer_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sci -SS -Sorb -GX -Wb,export_macro=TAO_RTPortableServer_Export -Wb,export_include=tao/RTPortableServer/rtportableserver_export.h -o RTPortableServer -Sa -Wb,unique_include=tao/RTPortableServer/RTPortableServer.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="RTPortableServer\RTPortableServer.pidl"

InputPath=RTPortableServer\RTPortableServer.pidl

"RTPortableServer\RTPortableServerC.h" "RTPortableServer\RTPortableServerS.h" "RTPortableServer\RTPortableServerA.h" "RTPortableServer\RTPortableServerC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-RTPortableServer_RTPortableServer_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sci -SS -Sorb -GX -Wb,export_macro=TAO_RTPortableServer_Export -Wb,export_include=tao/RTPortableServer/rtportableserver_export.h -o RTPortableServer -Gp -Gd -Wb,include_guard=TAO_RT_PORTABLESERVER_SAFE_INCLUDE -Wb,safe_include=tao/RTPortableServer/RTPortableServer.h "$(InputPath)"
<<

SOURCE="RTPortableServer\RTPortableServer_include.pidl"

InputPath=RTPortableServer\RTPortableServer_include.pidl

"RTPortableServer\RTPortableServer_includeC.h" "RTPortableServer\RTPortableServer_includeS.h" "RTPortableServer\RTPortableServer_includeA.h" "RTPortableServer\RTPortableServer_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-RTPortableServer_RTPortableServer_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Sci -SS -Sorb -GX -Wb,export_macro=TAO_RTPortableServer_Export -Wb,export_include=tao/RTPortableServer/rtportableserver_export.h -o RTPortableServer -Sa -Wb,unique_include=tao/RTPortableServer/RTPortableServer.h "$(InputPath)"
<<

!ENDIF

SOURCE="RTPortableServer\TAO_RTPortableServer.rc"

"$(INTDIR)\RTPortableServer\TAO_RTPortableServer.res" : $(SOURCE)
	@if not exist "$(INTDIR)\RTPortableServer\$(NULL)" mkdir "$(INTDIR)\RTPortableServer\"
	$(RSC) /l 0x409 /fo"$(INTDIR)\RTPortableServer\TAO_RTPortableServer.res" /d NDEBUG /d WIN64 /d _CRT_SECURE_NO_WARNINGS /d _CRT_SECURE_NO_DEPRECATE /d _CRT_NONSTDC_NO_DEPRECATE /i "..\.." /i ".." $(SOURCE)



!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.RTPortableServer.dep")
	@echo Using "Makefile.RTPortableServer.dep"
!ELSE
	@echo Warning: cannot find "Makefile.RTPortableServer.dep"
!ENDIF
!ENDIF

