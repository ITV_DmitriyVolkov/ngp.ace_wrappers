//$Id: GIOP_Message_Version.cpp 14 2007-02-01 15:49:12Z mitza $
# include "tao/GIOP_Message_Version.h"

#if !defined (__ACE_INLINE__)
# include "tao/GIOP_Message_Version.inl"
#endif /* __ACE_INLINE__ */

ACE_RCSID (tao, 
           GIOP_Message_Version, 
           "$Id: GIOP_Message_Version.cpp 14 2007-02-01 15:49:12Z mitza $")
