# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.RTCORBA.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "RTCORBA\RTCORBA_includeC.h" "RTCORBA\RTCORBA_includeS.h" "RTCORBA\RTCORBA_includeA.h" "RTCORBA\RTCORBA_includeC.cpp" "RTCORBA\RTCORBAC.h" "RTCORBA\RTCORBAS.h" "RTCORBA\RTCORBAA.h" "RTCORBA\RTCORBAC.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\RTCORBA\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_RTCORBAd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_RTCORBA_BUILD_DLL -f "Makefile.RTCORBA.dep" "RTCORBA\RTCORBA_includeC.cpp" "RTCORBA\RTCORBAC.cpp" "RTCORBA\Priority_Mapping.cpp" "RTCORBA\Multi_Priority_Mapping.cpp" "RTCORBA\RT_Endpoint_Selector_Factory.cpp" "RTCORBA\Continuous_Priority_Mapping.cpp" "RTCORBA\Linear_Network_Priority_Mapping.cpp" "RTCORBA\RT_ORBInitializer.cpp" "RTCORBA\RT_Transport_Descriptor.cpp" "RTCORBA\RT_Protocols_Hooks.cpp" "RTCORBA\RT_Policy_i.cpp" "RTCORBA\RT_Endpoint_Utils.cpp" "RTCORBA\RT_ProtocolPropertiesA.cpp" "RTCORBA\RT_ProtocolPropertiesC.cpp" "RTCORBA\RT_Transport_Descriptor_Property.cpp" "RTCORBA\Priority_Mapping_Manager.cpp" "RTCORBA\RT_Stub.cpp" "RTCORBA\RT_Thread_Lane_Resources_Manager.cpp" "RTCORBA\Network_Priority_Mapping_Manager.cpp" "RTCORBA\RT_Mutex.cpp" "RTCORBA\RT_Current.cpp" "RTCORBA\RT_Stub_Factory.cpp" "RTCORBA\Network_Priority_Mapping.cpp" "RTCORBA\Linear_Priority_Mapping.cpp" "RTCORBA\RT_PolicyFactory.cpp" "RTCORBA\RT_ORB_Loader.cpp" "RTCORBA\RT_ORB.cpp" "RTCORBA\Thread_Pool.cpp" "RTCORBA\RT_Invocation_Endpoint_Selectors.cpp" "RTCORBA\Direct_Priority_Mapping.cpp" "RTCORBA\RTCORBA.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_RTCORBAd.pdb"
	-@del /f/q "..\..\lib\TAO_RTCORBAd.dll"
	-@del /f/q "$(OUTDIR)\TAO_RTCORBAd.lib"
	-@del /f/q "$(OUTDIR)\TAO_RTCORBAd.exp"
	-@del /f/q "$(OUTDIR)\TAO_RTCORBAd.ilk"
	-@del /f/q "RTCORBA\RTCORBA_includeC.h"
	-@del /f/q "RTCORBA\RTCORBA_includeS.h"
	-@del /f/q "RTCORBA\RTCORBA_includeA.h"
	-@del /f/q "RTCORBA\RTCORBA_includeC.cpp"
	-@del /f/q "RTCORBA\RTCORBAC.h"
	-@del /f/q "RTCORBA\RTCORBAS.h"
	-@del /f/q "RTCORBA\RTCORBAA.h"
	-@del /f/q "RTCORBA\RTCORBAC.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\RTCORBA\$(NULL)" mkdir "Debug\RTCORBA"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_RTCORBA_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_CodecFactoryd.lib TAO_PId.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\TAO_RTCORBAd.pdb" /machine:IA64 /out:"..\..\lib\TAO_RTCORBAd.dll" /implib:"$(OUTDIR)\TAO_RTCORBAd.lib"
LINK32_OBJS= \
	"$(INTDIR)\RTCORBA\TAO_RTCORBA.res" \
	"$(INTDIR)\RTCORBA\RTCORBA_includeC.obj" \
	"$(INTDIR)\RTCORBA\RTCORBAC.obj" \
	"$(INTDIR)\RTCORBA\Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\Multi_Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\RT_Endpoint_Selector_Factory.obj" \
	"$(INTDIR)\RTCORBA\Continuous_Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\Linear_Network_Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\RT_ORBInitializer.obj" \
	"$(INTDIR)\RTCORBA\RT_Transport_Descriptor.obj" \
	"$(INTDIR)\RTCORBA\RT_Protocols_Hooks.obj" \
	"$(INTDIR)\RTCORBA\RT_Policy_i.obj" \
	"$(INTDIR)\RTCORBA\RT_Endpoint_Utils.obj" \
	"$(INTDIR)\RTCORBA\RT_ProtocolPropertiesA.obj" \
	"$(INTDIR)\RTCORBA\RT_ProtocolPropertiesC.obj" \
	"$(INTDIR)\RTCORBA\RT_Transport_Descriptor_Property.obj" \
	"$(INTDIR)\RTCORBA\Priority_Mapping_Manager.obj" \
	"$(INTDIR)\RTCORBA\RT_Stub.obj" \
	"$(INTDIR)\RTCORBA\RT_Thread_Lane_Resources_Manager.obj" \
	"$(INTDIR)\RTCORBA\Network_Priority_Mapping_Manager.obj" \
	"$(INTDIR)\RTCORBA\RT_Mutex.obj" \
	"$(INTDIR)\RTCORBA\RT_Current.obj" \
	"$(INTDIR)\RTCORBA\RT_Stub_Factory.obj" \
	"$(INTDIR)\RTCORBA\Network_Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\Linear_Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\RT_PolicyFactory.obj" \
	"$(INTDIR)\RTCORBA\RT_ORB_Loader.obj" \
	"$(INTDIR)\RTCORBA\RT_ORB.obj" \
	"$(INTDIR)\RTCORBA\Thread_Pool.obj" \
	"$(INTDIR)\RTCORBA\RT_Invocation_Endpoint_Selectors.obj" \
	"$(INTDIR)\RTCORBA\Direct_Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\RTCORBA.obj"

"..\..\lib\TAO_RTCORBAd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_RTCORBAd.dll.manifest" mt.exe -manifest "..\..\lib\TAO_RTCORBAd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\RTCORBA\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\TAO_RTCORBA.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_RTCORBA_BUILD_DLL -f "Makefile.RTCORBA.dep" "RTCORBA\RTCORBA_includeC.cpp" "RTCORBA\RTCORBAC.cpp" "RTCORBA\Priority_Mapping.cpp" "RTCORBA\Multi_Priority_Mapping.cpp" "RTCORBA\RT_Endpoint_Selector_Factory.cpp" "RTCORBA\Continuous_Priority_Mapping.cpp" "RTCORBA\Linear_Network_Priority_Mapping.cpp" "RTCORBA\RT_ORBInitializer.cpp" "RTCORBA\RT_Transport_Descriptor.cpp" "RTCORBA\RT_Protocols_Hooks.cpp" "RTCORBA\RT_Policy_i.cpp" "RTCORBA\RT_Endpoint_Utils.cpp" "RTCORBA\RT_ProtocolPropertiesA.cpp" "RTCORBA\RT_ProtocolPropertiesC.cpp" "RTCORBA\RT_Transport_Descriptor_Property.cpp" "RTCORBA\Priority_Mapping_Manager.cpp" "RTCORBA\RT_Stub.cpp" "RTCORBA\RT_Thread_Lane_Resources_Manager.cpp" "RTCORBA\Network_Priority_Mapping_Manager.cpp" "RTCORBA\RT_Mutex.cpp" "RTCORBA\RT_Current.cpp" "RTCORBA\RT_Stub_Factory.cpp" "RTCORBA\Network_Priority_Mapping.cpp" "RTCORBA\Linear_Priority_Mapping.cpp" "RTCORBA\RT_PolicyFactory.cpp" "RTCORBA\RT_ORB_Loader.cpp" "RTCORBA\RT_ORB.cpp" "RTCORBA\Thread_Pool.cpp" "RTCORBA\RT_Invocation_Endpoint_Selectors.cpp" "RTCORBA\Direct_Priority_Mapping.cpp" "RTCORBA\RTCORBA.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\TAO_RTCORBA.dll"
	-@del /f/q "$(OUTDIR)\TAO_RTCORBA.lib"
	-@del /f/q "$(OUTDIR)\TAO_RTCORBA.exp"
	-@del /f/q "$(OUTDIR)\TAO_RTCORBA.ilk"
	-@del /f/q "RTCORBA\RTCORBA_includeC.h"
	-@del /f/q "RTCORBA\RTCORBA_includeS.h"
	-@del /f/q "RTCORBA\RTCORBA_includeA.h"
	-@del /f/q "RTCORBA\RTCORBA_includeC.cpp"
	-@del /f/q "RTCORBA\RTCORBAC.h"
	-@del /f/q "RTCORBA\RTCORBAS.h"
	-@del /f/q "RTCORBA\RTCORBAA.h"
	-@del /f/q "RTCORBA\RTCORBAC.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\RTCORBA\$(NULL)" mkdir "Release\RTCORBA"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_RTCORBA_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_CodecFactory.lib TAO_PI.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\TAO_RTCORBA.dll" /implib:"$(OUTDIR)\TAO_RTCORBA.lib"
LINK32_OBJS= \
	"$(INTDIR)\RTCORBA\TAO_RTCORBA.res" \
	"$(INTDIR)\RTCORBA\RTCORBA_includeC.obj" \
	"$(INTDIR)\RTCORBA\RTCORBAC.obj" \
	"$(INTDIR)\RTCORBA\Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\Multi_Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\RT_Endpoint_Selector_Factory.obj" \
	"$(INTDIR)\RTCORBA\Continuous_Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\Linear_Network_Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\RT_ORBInitializer.obj" \
	"$(INTDIR)\RTCORBA\RT_Transport_Descriptor.obj" \
	"$(INTDIR)\RTCORBA\RT_Protocols_Hooks.obj" \
	"$(INTDIR)\RTCORBA\RT_Policy_i.obj" \
	"$(INTDIR)\RTCORBA\RT_Endpoint_Utils.obj" \
	"$(INTDIR)\RTCORBA\RT_ProtocolPropertiesA.obj" \
	"$(INTDIR)\RTCORBA\RT_ProtocolPropertiesC.obj" \
	"$(INTDIR)\RTCORBA\RT_Transport_Descriptor_Property.obj" \
	"$(INTDIR)\RTCORBA\Priority_Mapping_Manager.obj" \
	"$(INTDIR)\RTCORBA\RT_Stub.obj" \
	"$(INTDIR)\RTCORBA\RT_Thread_Lane_Resources_Manager.obj" \
	"$(INTDIR)\RTCORBA\Network_Priority_Mapping_Manager.obj" \
	"$(INTDIR)\RTCORBA\RT_Mutex.obj" \
	"$(INTDIR)\RTCORBA\RT_Current.obj" \
	"$(INTDIR)\RTCORBA\RT_Stub_Factory.obj" \
	"$(INTDIR)\RTCORBA\Network_Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\Linear_Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\RT_PolicyFactory.obj" \
	"$(INTDIR)\RTCORBA\RT_ORB_Loader.obj" \
	"$(INTDIR)\RTCORBA\RT_ORB.obj" \
	"$(INTDIR)\RTCORBA\Thread_Pool.obj" \
	"$(INTDIR)\RTCORBA\RT_Invocation_Endpoint_Selectors.obj" \
	"$(INTDIR)\RTCORBA\Direct_Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\RTCORBA.obj"

"..\..\lib\TAO_RTCORBA.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\TAO_RTCORBA.dll.manifest" mt.exe -manifest "..\..\lib\TAO_RTCORBA.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\RTCORBA\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_RTCORBAsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.RTCORBA.dep" "RTCORBA\RTCORBA_includeC.cpp" "RTCORBA\RTCORBAC.cpp" "RTCORBA\Priority_Mapping.cpp" "RTCORBA\Multi_Priority_Mapping.cpp" "RTCORBA\RT_Endpoint_Selector_Factory.cpp" "RTCORBA\Continuous_Priority_Mapping.cpp" "RTCORBA\Linear_Network_Priority_Mapping.cpp" "RTCORBA\RT_ORBInitializer.cpp" "RTCORBA\RT_Transport_Descriptor.cpp" "RTCORBA\RT_Protocols_Hooks.cpp" "RTCORBA\RT_Policy_i.cpp" "RTCORBA\RT_Endpoint_Utils.cpp" "RTCORBA\RT_ProtocolPropertiesA.cpp" "RTCORBA\RT_ProtocolPropertiesC.cpp" "RTCORBA\RT_Transport_Descriptor_Property.cpp" "RTCORBA\Priority_Mapping_Manager.cpp" "RTCORBA\RT_Stub.cpp" "RTCORBA\RT_Thread_Lane_Resources_Manager.cpp" "RTCORBA\Network_Priority_Mapping_Manager.cpp" "RTCORBA\RT_Mutex.cpp" "RTCORBA\RT_Current.cpp" "RTCORBA\RT_Stub_Factory.cpp" "RTCORBA\Network_Priority_Mapping.cpp" "RTCORBA\Linear_Priority_Mapping.cpp" "RTCORBA\RT_PolicyFactory.cpp" "RTCORBA\RT_ORB_Loader.cpp" "RTCORBA\RT_ORB.cpp" "RTCORBA\Thread_Pool.cpp" "RTCORBA\RT_Invocation_Endpoint_Selectors.cpp" "RTCORBA\Direct_Priority_Mapping.cpp" "RTCORBA\RTCORBA.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_RTCORBAsd.lib"
	-@del /f/q "$(OUTDIR)\TAO_RTCORBAsd.exp"
	-@del /f/q "$(OUTDIR)\TAO_RTCORBAsd.ilk"
	-@del /f/q "..\..\lib\TAO_RTCORBAsd.pdb"
	-@del /f/q "RTCORBA\RTCORBA_includeC.h"
	-@del /f/q "RTCORBA\RTCORBA_includeS.h"
	-@del /f/q "RTCORBA\RTCORBA_includeA.h"
	-@del /f/q "RTCORBA\RTCORBA_includeC.cpp"
	-@del /f/q "RTCORBA\RTCORBAC.h"
	-@del /f/q "RTCORBA\RTCORBAS.h"
	-@del /f/q "RTCORBA\RTCORBAA.h"
	-@del /f/q "RTCORBA\RTCORBAC.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\RTCORBA\$(NULL)" mkdir "Static_Debug\RTCORBA"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"..\..\lib\TAO_RTCORBAsd.pdb" /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_RTCORBAsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\RTCORBA\RTCORBA_includeC.obj" \
	"$(INTDIR)\RTCORBA\RTCORBAC.obj" \
	"$(INTDIR)\RTCORBA\Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\Multi_Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\RT_Endpoint_Selector_Factory.obj" \
	"$(INTDIR)\RTCORBA\Continuous_Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\Linear_Network_Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\RT_ORBInitializer.obj" \
	"$(INTDIR)\RTCORBA\RT_Transport_Descriptor.obj" \
	"$(INTDIR)\RTCORBA\RT_Protocols_Hooks.obj" \
	"$(INTDIR)\RTCORBA\RT_Policy_i.obj" \
	"$(INTDIR)\RTCORBA\RT_Endpoint_Utils.obj" \
	"$(INTDIR)\RTCORBA\RT_ProtocolPropertiesA.obj" \
	"$(INTDIR)\RTCORBA\RT_ProtocolPropertiesC.obj" \
	"$(INTDIR)\RTCORBA\RT_Transport_Descriptor_Property.obj" \
	"$(INTDIR)\RTCORBA\Priority_Mapping_Manager.obj" \
	"$(INTDIR)\RTCORBA\RT_Stub.obj" \
	"$(INTDIR)\RTCORBA\RT_Thread_Lane_Resources_Manager.obj" \
	"$(INTDIR)\RTCORBA\Network_Priority_Mapping_Manager.obj" \
	"$(INTDIR)\RTCORBA\RT_Mutex.obj" \
	"$(INTDIR)\RTCORBA\RT_Current.obj" \
	"$(INTDIR)\RTCORBA\RT_Stub_Factory.obj" \
	"$(INTDIR)\RTCORBA\Network_Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\Linear_Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\RT_PolicyFactory.obj" \
	"$(INTDIR)\RTCORBA\RT_ORB_Loader.obj" \
	"$(INTDIR)\RTCORBA\RT_ORB.obj" \
	"$(INTDIR)\RTCORBA\Thread_Pool.obj" \
	"$(INTDIR)\RTCORBA\RT_Invocation_Endpoint_Selectors.obj" \
	"$(INTDIR)\RTCORBA\Direct_Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\RTCORBA.obj"

"$(OUTDIR)\TAO_RTCORBAsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_RTCORBAsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_RTCORBAsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\RTCORBA\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_RTCORBAs.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.RTCORBA.dep" "RTCORBA\RTCORBA_includeC.cpp" "RTCORBA\RTCORBAC.cpp" "RTCORBA\Priority_Mapping.cpp" "RTCORBA\Multi_Priority_Mapping.cpp" "RTCORBA\RT_Endpoint_Selector_Factory.cpp" "RTCORBA\Continuous_Priority_Mapping.cpp" "RTCORBA\Linear_Network_Priority_Mapping.cpp" "RTCORBA\RT_ORBInitializer.cpp" "RTCORBA\RT_Transport_Descriptor.cpp" "RTCORBA\RT_Protocols_Hooks.cpp" "RTCORBA\RT_Policy_i.cpp" "RTCORBA\RT_Endpoint_Utils.cpp" "RTCORBA\RT_ProtocolPropertiesA.cpp" "RTCORBA\RT_ProtocolPropertiesC.cpp" "RTCORBA\RT_Transport_Descriptor_Property.cpp" "RTCORBA\Priority_Mapping_Manager.cpp" "RTCORBA\RT_Stub.cpp" "RTCORBA\RT_Thread_Lane_Resources_Manager.cpp" "RTCORBA\Network_Priority_Mapping_Manager.cpp" "RTCORBA\RT_Mutex.cpp" "RTCORBA\RT_Current.cpp" "RTCORBA\RT_Stub_Factory.cpp" "RTCORBA\Network_Priority_Mapping.cpp" "RTCORBA\Linear_Priority_Mapping.cpp" "RTCORBA\RT_PolicyFactory.cpp" "RTCORBA\RT_ORB_Loader.cpp" "RTCORBA\RT_ORB.cpp" "RTCORBA\Thread_Pool.cpp" "RTCORBA\RT_Invocation_Endpoint_Selectors.cpp" "RTCORBA\Direct_Priority_Mapping.cpp" "RTCORBA\RTCORBA.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_RTCORBAs.lib"
	-@del /f/q "$(OUTDIR)\TAO_RTCORBAs.exp"
	-@del /f/q "$(OUTDIR)\TAO_RTCORBAs.ilk"
	-@del /f/q "RTCORBA\RTCORBA_includeC.h"
	-@del /f/q "RTCORBA\RTCORBA_includeS.h"
	-@del /f/q "RTCORBA\RTCORBA_includeA.h"
	-@del /f/q "RTCORBA\RTCORBA_includeC.cpp"
	-@del /f/q "RTCORBA\RTCORBAC.h"
	-@del /f/q "RTCORBA\RTCORBAS.h"
	-@del /f/q "RTCORBA\RTCORBAA.h"
	-@del /f/q "RTCORBA\RTCORBAC.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\RTCORBA\$(NULL)" mkdir "Static_Release\RTCORBA"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\TAO_RTCORBAs.lib"
LINK32_OBJS= \
	"$(INTDIR)\RTCORBA\RTCORBA_includeC.obj" \
	"$(INTDIR)\RTCORBA\RTCORBAC.obj" \
	"$(INTDIR)\RTCORBA\Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\Multi_Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\RT_Endpoint_Selector_Factory.obj" \
	"$(INTDIR)\RTCORBA\Continuous_Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\Linear_Network_Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\RT_ORBInitializer.obj" \
	"$(INTDIR)\RTCORBA\RT_Transport_Descriptor.obj" \
	"$(INTDIR)\RTCORBA\RT_Protocols_Hooks.obj" \
	"$(INTDIR)\RTCORBA\RT_Policy_i.obj" \
	"$(INTDIR)\RTCORBA\RT_Endpoint_Utils.obj" \
	"$(INTDIR)\RTCORBA\RT_ProtocolPropertiesA.obj" \
	"$(INTDIR)\RTCORBA\RT_ProtocolPropertiesC.obj" \
	"$(INTDIR)\RTCORBA\RT_Transport_Descriptor_Property.obj" \
	"$(INTDIR)\RTCORBA\Priority_Mapping_Manager.obj" \
	"$(INTDIR)\RTCORBA\RT_Stub.obj" \
	"$(INTDIR)\RTCORBA\RT_Thread_Lane_Resources_Manager.obj" \
	"$(INTDIR)\RTCORBA\Network_Priority_Mapping_Manager.obj" \
	"$(INTDIR)\RTCORBA\RT_Mutex.obj" \
	"$(INTDIR)\RTCORBA\RT_Current.obj" \
	"$(INTDIR)\RTCORBA\RT_Stub_Factory.obj" \
	"$(INTDIR)\RTCORBA\Network_Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\Linear_Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\RT_PolicyFactory.obj" \
	"$(INTDIR)\RTCORBA\RT_ORB_Loader.obj" \
	"$(INTDIR)\RTCORBA\RT_ORB.obj" \
	"$(INTDIR)\RTCORBA\Thread_Pool.obj" \
	"$(INTDIR)\RTCORBA\RT_Invocation_Endpoint_Selectors.obj" \
	"$(INTDIR)\RTCORBA\Direct_Priority_Mapping.obj" \
	"$(INTDIR)\RTCORBA\RTCORBA.obj"

"$(OUTDIR)\TAO_RTCORBAs.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_RTCORBAs.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_RTCORBAs.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.RTCORBA.dep")
!INCLUDE "Makefile.RTCORBA.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="RTCORBA\RTCORBA_includeC.cpp"

"$(INTDIR)\RTCORBA\RTCORBA_includeC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\RTCORBA_includeC.obj" $(SOURCE)

SOURCE="RTCORBA\RTCORBAC.cpp"

"$(INTDIR)\RTCORBA\RTCORBAC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\RTCORBAC.obj" $(SOURCE)

SOURCE="RTCORBA\Priority_Mapping.cpp"

"$(INTDIR)\RTCORBA\Priority_Mapping.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\Priority_Mapping.obj" $(SOURCE)

SOURCE="RTCORBA\Multi_Priority_Mapping.cpp"

"$(INTDIR)\RTCORBA\Multi_Priority_Mapping.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\Multi_Priority_Mapping.obj" $(SOURCE)

SOURCE="RTCORBA\RT_Endpoint_Selector_Factory.cpp"

"$(INTDIR)\RTCORBA\RT_Endpoint_Selector_Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\RT_Endpoint_Selector_Factory.obj" $(SOURCE)

SOURCE="RTCORBA\Continuous_Priority_Mapping.cpp"

"$(INTDIR)\RTCORBA\Continuous_Priority_Mapping.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\Continuous_Priority_Mapping.obj" $(SOURCE)

SOURCE="RTCORBA\Linear_Network_Priority_Mapping.cpp"

"$(INTDIR)\RTCORBA\Linear_Network_Priority_Mapping.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\Linear_Network_Priority_Mapping.obj" $(SOURCE)

SOURCE="RTCORBA\RT_ORBInitializer.cpp"

"$(INTDIR)\RTCORBA\RT_ORBInitializer.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\RT_ORBInitializer.obj" $(SOURCE)

SOURCE="RTCORBA\RT_Transport_Descriptor.cpp"

"$(INTDIR)\RTCORBA\RT_Transport_Descriptor.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\RT_Transport_Descriptor.obj" $(SOURCE)

SOURCE="RTCORBA\RT_Protocols_Hooks.cpp"

"$(INTDIR)\RTCORBA\RT_Protocols_Hooks.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\RT_Protocols_Hooks.obj" $(SOURCE)

SOURCE="RTCORBA\RT_Policy_i.cpp"

"$(INTDIR)\RTCORBA\RT_Policy_i.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\RT_Policy_i.obj" $(SOURCE)

SOURCE="RTCORBA\RT_Endpoint_Utils.cpp"

"$(INTDIR)\RTCORBA\RT_Endpoint_Utils.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\RT_Endpoint_Utils.obj" $(SOURCE)

SOURCE="RTCORBA\RT_ProtocolPropertiesA.cpp"

"$(INTDIR)\RTCORBA\RT_ProtocolPropertiesA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\RT_ProtocolPropertiesA.obj" $(SOURCE)

SOURCE="RTCORBA\RT_ProtocolPropertiesC.cpp"

"$(INTDIR)\RTCORBA\RT_ProtocolPropertiesC.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\RT_ProtocolPropertiesC.obj" $(SOURCE)

SOURCE="RTCORBA\RT_Transport_Descriptor_Property.cpp"

"$(INTDIR)\RTCORBA\RT_Transport_Descriptor_Property.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\RT_Transport_Descriptor_Property.obj" $(SOURCE)

SOURCE="RTCORBA\Priority_Mapping_Manager.cpp"

"$(INTDIR)\RTCORBA\Priority_Mapping_Manager.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\Priority_Mapping_Manager.obj" $(SOURCE)

SOURCE="RTCORBA\RT_Stub.cpp"

"$(INTDIR)\RTCORBA\RT_Stub.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\RT_Stub.obj" $(SOURCE)

SOURCE="RTCORBA\RT_Thread_Lane_Resources_Manager.cpp"

"$(INTDIR)\RTCORBA\RT_Thread_Lane_Resources_Manager.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\RT_Thread_Lane_Resources_Manager.obj" $(SOURCE)

SOURCE="RTCORBA\Network_Priority_Mapping_Manager.cpp"

"$(INTDIR)\RTCORBA\Network_Priority_Mapping_Manager.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\Network_Priority_Mapping_Manager.obj" $(SOURCE)

SOURCE="RTCORBA\RT_Mutex.cpp"

"$(INTDIR)\RTCORBA\RT_Mutex.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\RT_Mutex.obj" $(SOURCE)

SOURCE="RTCORBA\RT_Current.cpp"

"$(INTDIR)\RTCORBA\RT_Current.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\RT_Current.obj" $(SOURCE)

SOURCE="RTCORBA\RT_Stub_Factory.cpp"

"$(INTDIR)\RTCORBA\RT_Stub_Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\RT_Stub_Factory.obj" $(SOURCE)

SOURCE="RTCORBA\Network_Priority_Mapping.cpp"

"$(INTDIR)\RTCORBA\Network_Priority_Mapping.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\Network_Priority_Mapping.obj" $(SOURCE)

SOURCE="RTCORBA\Linear_Priority_Mapping.cpp"

"$(INTDIR)\RTCORBA\Linear_Priority_Mapping.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\Linear_Priority_Mapping.obj" $(SOURCE)

SOURCE="RTCORBA\RT_PolicyFactory.cpp"

"$(INTDIR)\RTCORBA\RT_PolicyFactory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\RT_PolicyFactory.obj" $(SOURCE)

SOURCE="RTCORBA\RT_ORB_Loader.cpp"

"$(INTDIR)\RTCORBA\RT_ORB_Loader.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\RT_ORB_Loader.obj" $(SOURCE)

SOURCE="RTCORBA\RT_ORB.cpp"

"$(INTDIR)\RTCORBA\RT_ORB.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\RT_ORB.obj" $(SOURCE)

SOURCE="RTCORBA\Thread_Pool.cpp"

"$(INTDIR)\RTCORBA\Thread_Pool.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\Thread_Pool.obj" $(SOURCE)

SOURCE="RTCORBA\RT_Invocation_Endpoint_Selectors.cpp"

"$(INTDIR)\RTCORBA\RT_Invocation_Endpoint_Selectors.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\RT_Invocation_Endpoint_Selectors.obj" $(SOURCE)

SOURCE="RTCORBA\Direct_Priority_Mapping.cpp"

"$(INTDIR)\RTCORBA\Direct_Priority_Mapping.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\Direct_Priority_Mapping.obj" $(SOURCE)

SOURCE="RTCORBA\RTCORBA.cpp"

"$(INTDIR)\RTCORBA\RTCORBA.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RTCORBA\RTCORBA.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="RTCORBA\RTCORBA_include.pidl"

InputPath=RTCORBA\RTCORBA_include.pidl

"RTCORBA\RTCORBA_includeC.h" "RTCORBA\RTCORBA_includeS.h" "RTCORBA\RTCORBA_includeA.h" "RTCORBA\RTCORBA_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-RTCORBA_RTCORBA_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Sorb -GX -Wb,export_macro=TAO_RTCORBA_Export -Wb,export_include=tao/RTCORBA/rtcorba_export.h -o RTCORBA -Sa -Wb,unique_include=tao/RTCORBA/RTCORBA.h "$(InputPath)"
<<

SOURCE="RTCORBA\RTCORBA.pidl"

InputPath=RTCORBA\RTCORBA.pidl

"RTCORBA\RTCORBAC.h" "RTCORBA\RTCORBAS.h" "RTCORBA\RTCORBAA.h" "RTCORBA\RTCORBAC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BEd.dll" "..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-RTCORBA_RTCORBA_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Sorb -GX -Wb,export_macro=TAO_RTCORBA_Export -Wb,export_include=tao/RTCORBA/rtcorba_export.h -o RTCORBA -Gp -Gd -Wb,include_guard=TAO_RTCORBA_SAFE_INCLUDE -Wb,safe_include=tao/RTCORBA/RTCORBA.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="RTCORBA\RTCORBA_include.pidl"

InputPath=RTCORBA\RTCORBA_include.pidl

"RTCORBA\RTCORBA_includeC.h" "RTCORBA\RTCORBA_includeS.h" "RTCORBA\RTCORBA_includeA.h" "RTCORBA\RTCORBA_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-RTCORBA_RTCORBA_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Sorb -GX -Wb,export_macro=TAO_RTCORBA_Export -Wb,export_include=tao/RTCORBA/rtcorba_export.h -o RTCORBA -Sa -Wb,unique_include=tao/RTCORBA/RTCORBA.h "$(InputPath)"
<<

SOURCE="RTCORBA\RTCORBA.pidl"

InputPath=RTCORBA\RTCORBA.pidl

"RTCORBA\RTCORBAC.h" "RTCORBA\RTCORBAS.h" "RTCORBA\RTCORBAA.h" "RTCORBA\RTCORBAC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe" "..\..\lib\TAO_IDL_BE.dll" "..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-RTCORBA_RTCORBA_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Sorb -GX -Wb,export_macro=TAO_RTCORBA_Export -Wb,export_include=tao/RTCORBA/rtcorba_export.h -o RTCORBA -Gp -Gd -Wb,include_guard=TAO_RTCORBA_SAFE_INCLUDE -Wb,safe_include=tao/RTCORBA/RTCORBA.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="RTCORBA\RTCORBA_include.pidl"

InputPath=RTCORBA\RTCORBA_include.pidl

"RTCORBA\RTCORBA_includeC.h" "RTCORBA\RTCORBA_includeS.h" "RTCORBA\RTCORBA_includeA.h" "RTCORBA\RTCORBA_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-RTCORBA_RTCORBA_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Sorb -GX -Wb,export_macro=TAO_RTCORBA_Export -Wb,export_include=tao/RTCORBA/rtcorba_export.h -o RTCORBA -Sa -Wb,unique_include=tao/RTCORBA/RTCORBA.h "$(InputPath)"
<<

SOURCE="RTCORBA\RTCORBA.pidl"

InputPath=RTCORBA\RTCORBA.pidl

"RTCORBA\RTCORBAC.h" "RTCORBA\RTCORBAS.h" "RTCORBA\RTCORBAA.h" "RTCORBA\RTCORBAC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-RTCORBA_RTCORBA_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Sorb -GX -Wb,export_macro=TAO_RTCORBA_Export -Wb,export_include=tao/RTCORBA/rtcorba_export.h -o RTCORBA -Gp -Gd -Wb,include_guard=TAO_RTCORBA_SAFE_INCLUDE -Wb,safe_include=tao/RTCORBA/RTCORBA.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="RTCORBA\RTCORBA_include.pidl"

InputPath=RTCORBA\RTCORBA_include.pidl

"RTCORBA\RTCORBA_includeC.h" "RTCORBA\RTCORBA_includeS.h" "RTCORBA\RTCORBA_includeA.h" "RTCORBA\RTCORBA_includeC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-RTCORBA_RTCORBA_include_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Sorb -GX -Wb,export_macro=TAO_RTCORBA_Export -Wb,export_include=tao/RTCORBA/rtcorba_export.h -o RTCORBA -Sa -Wb,unique_include=tao/RTCORBA/RTCORBA.h "$(InputPath)"
<<

SOURCE="RTCORBA\RTCORBA.pidl"

InputPath=RTCORBA\RTCORBA.pidl

"RTCORBA\RTCORBAC.h" "RTCORBA\RTCORBAS.h" "RTCORBA\RTCORBAA.h" "RTCORBA\RTCORBAC.cpp" : $(SOURCE)  "..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-RTCORBA_RTCORBA_pidl.bat
	@echo off
	PATH=%PATH%;..\..\lib
	..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I.. -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL -Sci -SS -Sorb -GX -Wb,export_macro=TAO_RTCORBA_Export -Wb,export_include=tao/RTCORBA/rtcorba_export.h -o RTCORBA -Gp -Gd -Wb,include_guard=TAO_RTCORBA_SAFE_INCLUDE -Wb,safe_include=tao/RTCORBA/RTCORBA.h "$(InputPath)"
<<

!ENDIF

SOURCE="RTCORBA\TAO_RTCORBA.rc"

"$(INTDIR)\RTCORBA\TAO_RTCORBA.res" : $(SOURCE)
	@if not exist "$(INTDIR)\RTCORBA\$(NULL)" mkdir "$(INTDIR)\RTCORBA\"
	$(RSC) /l 0x409 /fo"$(INTDIR)\RTCORBA\TAO_RTCORBA.res" /d NDEBUG /d WIN64 /d _CRT_SECURE_NO_WARNINGS /d _CRT_SECURE_NO_DEPRECATE /d _CRT_NONSTDC_NO_DEPRECATE /i "..\.." /i ".." $(SOURCE)



!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.RTCORBA.dep")
	@echo Using "Makefile.RTCORBA.dep"
!ELSE
	@echo Warning: cannot find "Makefile.RTCORBA.dep"
!ENDIF
!ENDIF

