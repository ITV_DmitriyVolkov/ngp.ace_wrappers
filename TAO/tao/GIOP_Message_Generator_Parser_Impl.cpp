#include "tao/GIOP_Message_Generator_Parser_Impl.h"

#if !defined (__ACE_INLINE__)
# include "tao/GIOP_Message_Generator_Parser_Impl.inl"
#endif /* __ACE_INLINE__ */

ACE_RCSID (tao, 
           GIOP_Message_Generator_Parser_Impl, 
           "$Id: GIOP_Message_Generator_Parser_Impl.cpp 14 2007-02-01 15:49:12Z mitza $")
