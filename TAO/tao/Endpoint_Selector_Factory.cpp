// $Id: Endpoint_Selector_Factory.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/Endpoint_Selector_Factory.h"

ACE_RCSID (tao,
           Endpoint_Selector_Factory,
           "$Id: Endpoint_Selector_Factory.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Endpoint_Selector_Factory::~TAO_Endpoint_Selector_Factory (void)
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
