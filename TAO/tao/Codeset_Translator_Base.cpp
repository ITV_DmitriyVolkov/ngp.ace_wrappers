// -*- C++ -*-

//=============================================================================
/**
 *  @file    Codeset_Translator_Base.cpp
 *
 *  $Id: Codeset_Translator_Base.cpp 935 2008-12-10 21:47:27Z mitza $

 *
 * factories are responsible for supplying the proper translator on
 * demand.
 *
 *
 *  @author   Phil Mesnier <mesnier_p@ociweb.com>
 */
//=============================================================================


#include "tao/Codeset_Translator_Base.h"

ACE_RCSID (tao,
           Codeset_Translator_Base,
           "$Id: Codeset_Translator_Base.cpp 935 2008-12-10 21:47:27Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Codeset_Translator_Base::~TAO_Codeset_Translator_Base (void)
{

}

TAO_END_VERSIONED_NAMESPACE_DECL
