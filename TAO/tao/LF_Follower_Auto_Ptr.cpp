// $Id: LF_Follower_Auto_Ptr.cpp 14 2007-02-01 15:49:12Z mitza $

#include "tao/LF_Follower_Auto_Ptr.h"

#if !defined (__ACE_INLINE__)
# include "tao/LF_Follower_Auto_Ptr.inl"
#endif /* __ACE_INLINE__ */

ACE_RCSID (tao,
           LF_Follower_Auto_Ptr,
           "$Id: LF_Follower_Auto_Ptr.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_LF_Follower_Auto_Ptr::~TAO_LF_Follower_Auto_Ptr (void)
{
  this->leader_follower_.release_follower (this->follower_);
}

TAO_END_VERSIONED_NAMESPACE_DECL
