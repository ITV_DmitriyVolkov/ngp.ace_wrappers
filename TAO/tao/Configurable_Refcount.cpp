#include "Configurable_Refcount.h"

#if !defined (__ACE_INLINE__)
# include "Configurable_Refcount.inl"
#endif /* __ACE_INLINE__ */

ACE_RCSID (tao,
           Configurable_Refcount,
           "$Id: Configurable_Refcount.cpp 14 2007-02-01 15:49:12Z mitza $")
