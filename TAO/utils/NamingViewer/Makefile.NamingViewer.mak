# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.NamingViewer.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Console Application")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INSTALLDIR=..\..\..\bin
INTDIR=Debug\NamingViewer\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" "$(INSTALLDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\NamingViewer.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -I"..\..\orbsvcs" -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -D_AFXDLL -DACE_HAS_MFC=1 -DUSING_PCH -f "Makefile.NamingViewer.dep" "NamingObject.cpp" "NamingViewerDlg.cpp" "BindDialog.cpp" "BindNewContext.cpp" "ViewIORDialog.cpp" "NamingTreeCtrl.cpp" "SelectNSDialog.cpp" "NamingViewer.cpp" "AddNameServerDlg.cpp" "StdAfx.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\NamingViewer.pdb"
	-@del /f/q "$(INSTALLDIR)\NamingViewer.exe"
	-@del /f/q "$(INSTALLDIR)\NamingViewer.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\NamingViewer\$(NULL)" mkdir "Debug\NamingViewer"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\.." /I "..\.." /I "..\..\orbsvcs" /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D _AFXDLL /D ACE_HAS_MFC=1 /D MPC_LIB_MODIFIER=\"d\" /FD /c
CPP_PCH=/D USING_PCH /Yu"StdAfx.h" /Fp"$(INTDIR)\StdAfx.pch"
CPP_PROJ=$(CPP_COMMON) $(CPP_PCH) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_CosNamingd.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /debug /pdb:"$(INSTALLDIR)\NamingViewer.pdb" /machine:IA64 /out:"$(INSTALLDIR)\NamingViewer.exe"
LINK32_OBJS= \
	"$(INTDIR)\NamingViewer.res" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\NamingObject.obj" \
	"$(INTDIR)\NamingViewerDlg.obj" \
	"$(INTDIR)\BindDialog.obj" \
	"$(INTDIR)\BindNewContext.obj" \
	"$(INTDIR)\ViewIORDialog.obj" \
	"$(INTDIR)\NamingTreeCtrl.obj" \
	"$(INTDIR)\SelectNSDialog.obj" \
	"$(INTDIR)\NamingViewer.obj" \
	"$(INTDIR)\AddNameServerDlg.obj"

"$(INSTALLDIR)\NamingViewer.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\NamingViewer.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\NamingViewer.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=Release
INSTALLDIR=..\..\..\bin
INTDIR=Release\NamingViewer\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" "$(INSTALLDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\NamingViewer.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -I"..\..\orbsvcs" -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -D_AFXDLL -DACE_HAS_MFC=1 -DUSING_PCH -f "Makefile.NamingViewer.dep" "NamingObject.cpp" "NamingViewerDlg.cpp" "BindDialog.cpp" "BindNewContext.cpp" "ViewIORDialog.cpp" "NamingTreeCtrl.cpp" "SelectNSDialog.cpp" "NamingViewer.cpp" "AddNameServerDlg.cpp" "StdAfx.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\NamingViewer.exe"
	-@del /f/q "$(INSTALLDIR)\NamingViewer.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\NamingViewer\$(NULL)" mkdir "Release\NamingViewer"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\.." /I "..\.." /I "..\..\orbsvcs" /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D _AFXDLL /D ACE_HAS_MFC=1  /FD /c
CPP_PCH=/D USING_PCH /Yu"StdAfx.h" /Fp"$(INTDIR)\StdAfx.pch"
CPP_PROJ=$(CPP_COMMON) $(CPP_PCH) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_CosNaming.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows  /machine:IA64 /out:"$(INSTALLDIR)\NamingViewer.exe"
LINK32_OBJS= \
	"$(INTDIR)\NamingViewer.res" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\NamingObject.obj" \
	"$(INTDIR)\NamingViewerDlg.obj" \
	"$(INTDIR)\BindDialog.obj" \
	"$(INTDIR)\BindNewContext.obj" \
	"$(INTDIR)\ViewIORDialog.obj" \
	"$(INTDIR)\NamingTreeCtrl.obj" \
	"$(INTDIR)\SelectNSDialog.obj" \
	"$(INTDIR)\NamingViewer.obj" \
	"$(INTDIR)\AddNameServerDlg.obj"

"$(INSTALLDIR)\NamingViewer.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\NamingViewer.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\NamingViewer.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=Static_Debug
INSTALLDIR=..\..\..\bin
INTDIR=Static_Debug\NamingViewer\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" "$(INSTALLDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\NamingViewer.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -I"..\..\orbsvcs" -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -D_AFXDLL -DACE_HAS_MFC=1 -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -DUSING_PCH -f "Makefile.NamingViewer.dep" "NamingObject.cpp" "NamingViewerDlg.cpp" "BindDialog.cpp" "BindNewContext.cpp" "ViewIORDialog.cpp" "NamingTreeCtrl.cpp" "SelectNSDialog.cpp" "NamingViewer.cpp" "AddNameServerDlg.cpp" "StdAfx.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\NamingViewer.pdb"
	-@del /f/q "$(INSTALLDIR)\NamingViewer.exe"
	-@del /f/q "$(INSTALLDIR)\NamingViewer.ilk"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\NamingViewer\$(NULL)" mkdir "Static_Debug\NamingViewer"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\.." /I "..\.." /I "..\..\orbsvcs" /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D _AFXDLL /D ACE_HAS_MFC=1 /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c
CPP_PCH=/D USING_PCH /Yu"StdAfx.h" /Fp"$(INTDIR)\StdAfx.pch"
CPP_PROJ=$(CPP_COMMON) $(CPP_PCH) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEsd.lib TAOsd.lib TAO_AnyTypeCodesd.lib TAO_CosNamingsd.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /debug /pdb:"$(INSTALLDIR)\NamingViewer.pdb" /machine:IA64 /out:"$(INSTALLDIR)\NamingViewer.exe"
LINK32_OBJS= \
	"$(INTDIR)\NamingViewer.res" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\NamingObject.obj" \
	"$(INTDIR)\NamingViewerDlg.obj" \
	"$(INTDIR)\BindDialog.obj" \
	"$(INTDIR)\BindNewContext.obj" \
	"$(INTDIR)\ViewIORDialog.obj" \
	"$(INTDIR)\NamingTreeCtrl.obj" \
	"$(INTDIR)\SelectNSDialog.obj" \
	"$(INTDIR)\NamingViewer.obj" \
	"$(INTDIR)\AddNameServerDlg.obj"

"$(INSTALLDIR)\NamingViewer.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\NamingViewer.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\NamingViewer.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=Static_Release
INSTALLDIR=..\..\..\bin
INTDIR=Static_Release\NamingViewer\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" "$(INSTALLDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\NamingViewer.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -I"..\..\orbsvcs" -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -D_AFXDLL -DACE_HAS_MFC=1 -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -DUSING_PCH -f "Makefile.NamingViewer.dep" "NamingObject.cpp" "NamingViewerDlg.cpp" "BindDialog.cpp" "BindNewContext.cpp" "ViewIORDialog.cpp" "NamingTreeCtrl.cpp" "SelectNSDialog.cpp" "NamingViewer.cpp" "AddNameServerDlg.cpp" "StdAfx.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\NamingViewer.exe"
	-@del /f/q "$(INSTALLDIR)\NamingViewer.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\NamingViewer\$(NULL)" mkdir "Static_Release\NamingViewer"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\.." /I "..\.." /I "..\..\orbsvcs" /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D _AFXDLL /D ACE_HAS_MFC=1 /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c
CPP_PCH=/D USING_PCH /Yu"StdAfx.h" /Fp"$(INTDIR)\StdAfx.pch"
CPP_PROJ=$(CPP_COMMON) $(CPP_PCH) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEs.lib TAOs.lib TAO_AnyTypeCodes.lib TAO_CosNamings.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows  /machine:IA64 /out:"$(INSTALLDIR)\NamingViewer.exe"
LINK32_OBJS= \
	"$(INTDIR)\NamingViewer.res" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\NamingObject.obj" \
	"$(INTDIR)\NamingViewerDlg.obj" \
	"$(INTDIR)\BindDialog.obj" \
	"$(INTDIR)\BindNewContext.obj" \
	"$(INTDIR)\ViewIORDialog.obj" \
	"$(INTDIR)\NamingTreeCtrl.obj" \
	"$(INTDIR)\SelectNSDialog.obj" \
	"$(INTDIR)\NamingViewer.obj" \
	"$(INTDIR)\AddNameServerDlg.obj"

"$(INSTALLDIR)\NamingViewer.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\NamingViewer.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\NamingViewer.exe.manifest" -outputresource:$@;1

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(INSTALLDIR)" :
	if not exist "$(INSTALLDIR)\$(NULL)" mkdir "$(INSTALLDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.NamingViewer.dep")
!INCLUDE "Makefile.NamingViewer.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="StdAfx.cpp"

!IF  "$(CFG)" == "Win64 Debug"

CPP_SWITCHES=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\.." /I "..\.." /I "..\..\orbsvcs" /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D _AFXDLL /D ACE_HAS_MFC=1 /D USING_PCH /Fp"$(INTDIR)\StdAfx.pch" /Yc"StdAfx.h" /FD /c

"$(INTDIR)\StdAfx.obj" "$(INTDIR)\StdAfx.pch" : $(SOURCE)
	$(CPP) @<<
  $(CPP_SWITCHES) /Fo"$(INTDIR)\StdAfx.obj" $(SOURCE)
<<

!ELSEIF  "$(CFG)" == "Win64 Release"

CPP_SWITCHES=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\.." /I "..\.." /I "..\..\orbsvcs" /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D _AFXDLL /D ACE_HAS_MFC=1 /D USING_PCH /Fp"$(INTDIR)\StdAfx.pch" /Yc"StdAfx.h" /FD /c

"$(INTDIR)\StdAfx.obj" "$(INTDIR)\StdAfx.pch" : $(SOURCE)
	$(CPP) @<<
  $(CPP_SWITCHES) /Fo"$(INTDIR)\StdAfx.obj" $(SOURCE)
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

CPP_SWITCHES=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\.." /I "..\.." /I "..\..\orbsvcs" /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D _AFXDLL /D ACE_HAS_MFC=1 /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D USING_PCH /Fp"$(INTDIR)\StdAfx.pch" /Yc"StdAfx.h" /FD /c

"$(INTDIR)\StdAfx.obj" "$(INTDIR)\StdAfx.pch" : $(SOURCE)
	$(CPP) @<<
  $(CPP_SWITCHES) /Fo"$(INTDIR)\StdAfx.obj" $(SOURCE)
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"

CPP_SWITCHES=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\.." /I "..\.." /I "..\..\orbsvcs" /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D _AFXDLL /D ACE_HAS_MFC=1 /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D USING_PCH /Fp"$(INTDIR)\StdAfx.pch" /Yc"StdAfx.h" /FD /c

"$(INTDIR)\StdAfx.obj" "$(INTDIR)\StdAfx.pch" : $(SOURCE)
	$(CPP) @<<
  $(CPP_SWITCHES) /Fo"$(INTDIR)\StdAfx.obj" $(SOURCE)
<<

!ENDIF

SOURCE="NamingObject.cpp"

"$(INTDIR)\NamingObject.obj" : $(SOURCE)
	$(CPP) $(CPP_PCH) $(CPP_COMMON) /Fo"$(INTDIR)\NamingObject.obj" $(SOURCE)

SOURCE="NamingViewerDlg.cpp"

"$(INTDIR)\NamingViewerDlg.obj" : $(SOURCE)
	$(CPP) $(CPP_PCH) $(CPP_COMMON) /Fo"$(INTDIR)\NamingViewerDlg.obj" $(SOURCE)

SOURCE="BindDialog.cpp"

"$(INTDIR)\BindDialog.obj" : $(SOURCE)
	$(CPP) $(CPP_PCH) $(CPP_COMMON) /Fo"$(INTDIR)\BindDialog.obj" $(SOURCE)

SOURCE="BindNewContext.cpp"

"$(INTDIR)\BindNewContext.obj" : $(SOURCE)
	$(CPP) $(CPP_PCH) $(CPP_COMMON) /Fo"$(INTDIR)\BindNewContext.obj" $(SOURCE)

SOURCE="ViewIORDialog.cpp"

"$(INTDIR)\ViewIORDialog.obj" : $(SOURCE)
	$(CPP) $(CPP_PCH) $(CPP_COMMON) /Fo"$(INTDIR)\ViewIORDialog.obj" $(SOURCE)

SOURCE="NamingTreeCtrl.cpp"

"$(INTDIR)\NamingTreeCtrl.obj" : $(SOURCE)
	$(CPP) $(CPP_PCH) $(CPP_COMMON) /Fo"$(INTDIR)\NamingTreeCtrl.obj" $(SOURCE)

SOURCE="SelectNSDialog.cpp"

"$(INTDIR)\SelectNSDialog.obj" : $(SOURCE)
	$(CPP) $(CPP_PCH) $(CPP_COMMON) /Fo"$(INTDIR)\SelectNSDialog.obj" $(SOURCE)

SOURCE="NamingViewer.cpp"

"$(INTDIR)\NamingViewer.obj" : $(SOURCE)
	$(CPP) $(CPP_PCH) $(CPP_COMMON) /Fo"$(INTDIR)\NamingViewer.obj" $(SOURCE)

SOURCE="AddNameServerDlg.cpp"

"$(INTDIR)\AddNameServerDlg.obj" : $(SOURCE)
	$(CPP) $(CPP_PCH) $(CPP_COMMON) /Fo"$(INTDIR)\AddNameServerDlg.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF

SOURCE="NamingViewer.rc"

"$(INTDIR)\NamingViewer.res" : $(SOURCE)
	$(RSC) /l 0x409 /fo"$(INTDIR)\NamingViewer.res" /d NDEBUG /d WIN64 /d _CRT_SECURE_NO_WARNINGS /d _CRT_SECURE_NO_DEPRECATE /d _CRT_NONSTDC_NO_DEPRECATE /d _AFXDLL /d ACE_HAS_MFC=1 /i "..\..\.." /i "..\.." /i "..\..\orbsvcs" $(SOURCE)



!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.NamingViewer.dep")
	@echo Using "Makefile.NamingViewer.dep"
!ELSE
	@echo Warning: cannot find "Makefile.NamingViewer.dep"
!ENDIF
!ENDIF

