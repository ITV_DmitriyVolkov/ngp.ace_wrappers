eval '(exit $?0)' && eval 'exec perl -S $0 ${1+"$@"}'
    & eval 'exec perl -S $0 $argv:q'
    if 0;

# $Id: run_test.pl 935 2008-12-10 21:47:27Z mitza $
# -*- perl -*-

use lib "$ENV{ACE_ROOT}/bin";
use PerlACE::Run_Test;

$status = 0;
$iorfile = PerlACE::LocalFile ("test.ior");

print STDERR "================ Thread_Pool Latency Test\n";

unlink $iorfile;

my $type = 'octet';

for ($i = 0; $i <= $#ARGV; $i++) {
    if ($ARGV[$i] eq "-h" || $ARGV[$i] eq "-?") {
        print "Run_Test Perl script for Performance Test\n\n";
        print "run_test  [-t type] \n";
        print "\n";
        print "-t type             -- runs only one type of param test\n";
        exit 0;
    }
    elsif ($ARGV[$i] eq "-t") {
        $type = $ARGV[$i + 1];
        $i++;
    }
}

$SV = new PerlACE::Process ("server",
                            "-o $iorfile");

$CL = new PerlACE::Process ("client",
                            "-t $type -k file://$iorfile "
                            . " -i 150000");

print STDERR $CL->CommandLine () ;
$SV->Spawn ();

if (PerlACE::waitforfile_timed ($iorfile, $PerlACE::wait_interval_for_process_creation) == -1) {
    print STDERR "ERROR: cannot find file <$iorfile>\n";
    $SV->Kill ();
    exit 1;
}

$client = $CL->SpawnWaitKill (420);
$server = $SV->WaitKill (40);

unlink $iorfile;


if ($client != 0) {
    print STDERR "ERROR: client returned $client\n";
    $status = 1;
}

if ($server != 0) {
    print STDERR "ERROR: server returned $server\n";
    $status = 1;
}


exit $status;
