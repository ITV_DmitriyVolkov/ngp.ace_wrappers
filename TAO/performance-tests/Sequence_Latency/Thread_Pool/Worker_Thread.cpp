//
// $Id: Worker_Thread.cpp 14 2007-02-01 15:49:12Z mitza $
//
#include "Worker_Thread.h"

ACE_RCSID(Thread_Pool_Latency, Worker_Thread, "$Id: Worker_Thread.cpp 14 2007-02-01 15:49:12Z mitza $")

Worker_Thread::Worker_Thread (CORBA::ORB_ptr orb)
  : orb_ (CORBA::ORB::_duplicate (orb))
{
}

int
Worker_Thread::svc (void)
{
  try
    {
      this->orb_->run ();
    }
  catch (const CORBA::Exception&){}
  return 0;
}
