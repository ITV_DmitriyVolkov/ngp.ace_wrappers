// $Id: RTI_IO.h 14 2007-02-01 15:49:12Z mitza $

// ============================================================================
//
// = LIBRARY
//    TAO/tests/IDL_Cubit
//
// = FILENAME
//    RTI_IO.h
//
// = AUTHOR
//    Dave Meyer <dmeyer@std.saic.com>
//
// ============================================================================

#ifndef _RTIFUNCS_H
#define _RTIFUNCS_H

#include "cubitC.h"

void print_RtiPacket (Cubit::RtiPacket const &arg);

#endif /* _RTIFUNCS_H */
