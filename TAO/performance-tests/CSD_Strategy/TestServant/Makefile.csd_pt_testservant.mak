# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.csd_pt_testservant.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "FooC.inl" "FooS.inl" "FooC.h" "FooS.h" "FooC.cpp" "FooS.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INTDIR=Debug\csd_pt_testservant\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) ".\CSD_PT_TestServantd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\CSD_Strategy" -I"..\..\..\tao" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DCSD_PT_TESTSERVANT_BUILD_DLL -f "Makefile.csd_pt_testservant.dep" "FooC.cpp" "FooS.cpp" "Foo_ClientEngine.cpp" "Foo_i.cpp" "Foo_Statistics.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CSD_PT_TestServantd.pdb"
	-@del /f/q ".\CSD_PT_TestServantd.dll"
	-@del /f/q "$(OUTDIR)\CSD_PT_TestServantd.lib"
	-@del /f/q "$(OUTDIR)\CSD_PT_TestServantd.exp"
	-@del /f/q "$(OUTDIR)\CSD_PT_TestServantd.ilk"
	-@del /f/q "FooC.inl"
	-@del /f/q "FooS.inl"
	-@del /f/q "FooC.h"
	-@del /f/q "FooS.h"
	-@del /f/q "FooC.cpp"
	-@del /f/q "FooS.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\csd_pt_testservant\$(NULL)" mkdir "Debug\csd_pt_testservant"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /I "..\..\CSD_Strategy" /I "..\..\..\tao" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D CSD_PT_TESTSERVANT_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_PortableServerd.lib TAO_CodecFactoryd.lib TAO_PId.lib TAO_CSD_Frameworkd.lib TAO_CSD_ThreadPoold.lib CSD_PT_TestInfd.lib /libpath:"." /libpath:"..\..\..\..\lib" /libpath:"..\..\CSD_Strategy\TestInf" /nologo /subsystem:windows /dll /debug /pdb:".\CSD_PT_TestServantd.pdb" /machine:IA64 /out:".\CSD_PT_TestServantd.dll" /implib:"$(OUTDIR)\CSD_PT_TestServantd.lib"
LINK32_OBJS= \
	"$(INTDIR)\FooC.obj" \
	"$(INTDIR)\FooS.obj" \
	"$(INTDIR)\Foo_ClientEngine.obj" \
	"$(INTDIR)\Foo_i.obj" \
	"$(INTDIR)\Foo_Statistics.obj"

".\CSD_PT_TestServantd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist ".\CSD_PT_TestServantd.dll.manifest" mt.exe -manifest ".\CSD_PT_TestServantd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=.
INTDIR=Release\csd_pt_testservant\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) ".\CSD_PT_TestServant.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\CSD_Strategy" -I"..\..\..\tao" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DCSD_PT_TESTSERVANT_BUILD_DLL -f "Makefile.csd_pt_testservant.dep" "FooC.cpp" "FooS.cpp" "Foo_ClientEngine.cpp" "Foo_i.cpp" "Foo_Statistics.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q ".\CSD_PT_TestServant.dll"
	-@del /f/q "$(OUTDIR)\CSD_PT_TestServant.lib"
	-@del /f/q "$(OUTDIR)\CSD_PT_TestServant.exp"
	-@del /f/q "$(OUTDIR)\CSD_PT_TestServant.ilk"
	-@del /f/q "FooC.inl"
	-@del /f/q "FooS.inl"
	-@del /f/q "FooC.h"
	-@del /f/q "FooS.h"
	-@del /f/q "FooC.cpp"
	-@del /f/q "FooS.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\csd_pt_testservant\$(NULL)" mkdir "Release\csd_pt_testservant"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\.." /I "..\..\.." /I "..\..\CSD_Strategy" /I "..\..\..\tao" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D CSD_PT_TESTSERVANT_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_PortableServer.lib TAO_CodecFactory.lib TAO_PI.lib TAO_CSD_Framework.lib TAO_CSD_ThreadPool.lib CSD_PT_TestInf.lib /libpath:"." /libpath:"..\..\..\..\lib" /libpath:"..\..\CSD_Strategy\TestInf" /nologo /subsystem:windows /dll  /machine:IA64 /out:".\CSD_PT_TestServant.dll" /implib:"$(OUTDIR)\CSD_PT_TestServant.lib"
LINK32_OBJS= \
	"$(INTDIR)\FooC.obj" \
	"$(INTDIR)\FooS.obj" \
	"$(INTDIR)\Foo_ClientEngine.obj" \
	"$(INTDIR)\Foo_i.obj" \
	"$(INTDIR)\Foo_Statistics.obj"

".\CSD_PT_TestServant.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist ".\CSD_PT_TestServant.dll.manifest" mt.exe -manifest ".\CSD_PT_TestServant.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=.
INTDIR=Static_Debug\csd_pt_testservant\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CSD_PT_TestServantsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\CSD_Strategy" -I"..\..\..\tao" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.csd_pt_testservant.dep" "FooC.cpp" "FooS.cpp" "Foo_ClientEngine.cpp" "Foo_i.cpp" "Foo_Statistics.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CSD_PT_TestServantsd.lib"
	-@del /f/q "$(OUTDIR)\CSD_PT_TestServantsd.exp"
	-@del /f/q "$(OUTDIR)\CSD_PT_TestServantsd.ilk"
	-@del /f/q ".\CSD_PT_TestServantsd.pdb"
	-@del /f/q "FooC.inl"
	-@del /f/q "FooS.inl"
	-@del /f/q "FooC.h"
	-@del /f/q "FooS.h"
	-@del /f/q "FooC.cpp"
	-@del /f/q "FooS.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\csd_pt_testservant\$(NULL)" mkdir "Static_Debug\csd_pt_testservant"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd".\CSD_PT_TestServantsd.pdb" /I "..\..\..\.." /I "..\..\.." /I "..\..\CSD_Strategy" /I "..\..\..\tao" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:".\CSD_PT_TestServantsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\FooC.obj" \
	"$(INTDIR)\FooS.obj" \
	"$(INTDIR)\Foo_ClientEngine.obj" \
	"$(INTDIR)\Foo_i.obj" \
	"$(INTDIR)\Foo_Statistics.obj"

"$(OUTDIR)\CSD_PT_TestServantsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CSD_PT_TestServantsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\CSD_PT_TestServantsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=.
INTDIR=Static_Release\csd_pt_testservant\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CSD_PT_TestServants.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\CSD_Strategy" -I"..\..\..\tao" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.csd_pt_testservant.dep" "FooC.cpp" "FooS.cpp" "Foo_ClientEngine.cpp" "Foo_i.cpp" "Foo_Statistics.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CSD_PT_TestServants.lib"
	-@del /f/q "$(OUTDIR)\CSD_PT_TestServants.exp"
	-@del /f/q "$(OUTDIR)\CSD_PT_TestServants.ilk"
	-@del /f/q "FooC.inl"
	-@del /f/q "FooS.inl"
	-@del /f/q "FooC.h"
	-@del /f/q "FooS.h"
	-@del /f/q "FooC.cpp"
	-@del /f/q "FooS.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\csd_pt_testservant\$(NULL)" mkdir "Static_Release\csd_pt_testservant"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\.." /I "..\..\.." /I "..\..\CSD_Strategy" /I "..\..\..\tao" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:".\CSD_PT_TestServants.lib"
LINK32_OBJS= \
	"$(INTDIR)\FooC.obj" \
	"$(INTDIR)\FooS.obj" \
	"$(INTDIR)\Foo_ClientEngine.obj" \
	"$(INTDIR)\Foo_i.obj" \
	"$(INTDIR)\Foo_Statistics.obj"

"$(OUTDIR)\CSD_PT_TestServants.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CSD_PT_TestServants.lib.manifest" mt.exe -manifest "$(OUTDIR)\CSD_PT_TestServants.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.csd_pt_testservant.dep")
!INCLUDE "Makefile.csd_pt_testservant.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="FooC.cpp"

"$(INTDIR)\FooC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\FooC.obj" $(SOURCE)

SOURCE="FooS.cpp"

"$(INTDIR)\FooS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\FooS.obj" $(SOURCE)

SOURCE="Foo_ClientEngine.cpp"

"$(INTDIR)\Foo_ClientEngine.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Foo_ClientEngine.obj" $(SOURCE)

SOURCE="Foo_i.cpp"

"$(INTDIR)\Foo_i.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Foo_i.obj" $(SOURCE)

SOURCE="Foo_Statistics.cpp"

"$(INTDIR)\Foo_Statistics.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Foo_Statistics.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="Foo.idl"

InputPath=Foo.idl

"FooC.inl" "FooS.inl" "FooC.h" "FooS.h" "FooC.cpp" "FooS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Foo_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I../../CSD_Strategy -Wb,export_macro=CSD_PT_TestServant_Export -Wb,export_include=TestServant/CSD_PT_TestServant_Export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="Foo.idl"

InputPath=Foo.idl

"FooC.inl" "FooS.inl" "FooC.h" "FooS.h" "FooC.cpp" "FooS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Foo_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I../../CSD_Strategy -Wb,export_macro=CSD_PT_TestServant_Export -Wb,export_include=TestServant/CSD_PT_TestServant_Export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="Foo.idl"

InputPath=Foo.idl

"FooC.inl" "FooS.inl" "FooC.h" "FooS.h" "FooC.cpp" "FooS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Foo_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I../../CSD_Strategy -Wb,export_macro=CSD_PT_TestServant_Export -Wb,export_include=TestServant/CSD_PT_TestServant_Export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="Foo.idl"

InputPath=Foo.idl

"FooC.inl" "FooS.inl" "FooC.h" "FooS.h" "FooC.cpp" "FooS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Foo_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I../../CSD_Strategy -Wb,export_macro=CSD_PT_TestServant_Export -Wb,export_include=TestServant/CSD_PT_TestServant_Export.h "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.csd_pt_testservant.dep")
	@echo Using "Makefile.csd_pt_testservant.dep"
!ELSE
	@echo Warning: cannot find "Makefile.csd_pt_testservant.dep"
!ENDIF
!ENDIF

