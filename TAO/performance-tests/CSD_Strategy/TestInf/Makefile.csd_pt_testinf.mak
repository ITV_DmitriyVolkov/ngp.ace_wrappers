# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.csd_pt_testinf.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "FooExceptionC.inl" "FooExceptionS.inl" "FooExceptionC.h" "FooExceptionS.h" "FooExceptionC.cpp" "FooExceptionS.cpp" "TestAppExceptionC.inl" "TestAppExceptionS.inl" "TestAppExceptionC.h" "TestAppExceptionS.h" "TestAppExceptionC.cpp" "TestAppExceptionS.cpp" "CancelledExceptionC.inl" "CancelledExceptionS.inl" "CancelledExceptionC.h" "CancelledExceptionS.h" "CancelledExceptionC.cpp" "CancelledExceptionS.cpp" "CustomExceptionC.inl" "CustomExceptionS.inl" "CustomExceptionC.h" "CustomExceptionS.h" "CustomExceptionC.cpp" "CustomExceptionS.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INTDIR=Debug\csd_pt_testinf\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) ".\CSD_PT_TestInfd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DCSD_PT_TESTINF_BUILD_DLL -f "Makefile.csd_pt_testinf.dep" "AppHelper.cpp" "AppShutdown.cpp" "ClientEngine.cpp" "ClientTask.cpp" "OrbRunner.cpp" "OrbShutdownTask.cpp" "OrbTask.cpp" "TestAppBase.cpp" "FooExceptionC.cpp" "FooExceptionS.cpp" "TestAppExceptionC.cpp" "TestAppExceptionS.cpp" "CancelledExceptionC.cpp" "CancelledExceptionS.cpp" "CustomExceptionC.cpp" "CustomExceptionS.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CSD_PT_TestInfd.pdb"
	-@del /f/q ".\CSD_PT_TestInfd.dll"
	-@del /f/q "$(OUTDIR)\CSD_PT_TestInfd.lib"
	-@del /f/q "$(OUTDIR)\CSD_PT_TestInfd.exp"
	-@del /f/q "$(OUTDIR)\CSD_PT_TestInfd.ilk"
	-@del /f/q "FooExceptionC.inl"
	-@del /f/q "FooExceptionS.inl"
	-@del /f/q "FooExceptionC.h"
	-@del /f/q "FooExceptionS.h"
	-@del /f/q "FooExceptionC.cpp"
	-@del /f/q "FooExceptionS.cpp"
	-@del /f/q "TestAppExceptionC.inl"
	-@del /f/q "TestAppExceptionS.inl"
	-@del /f/q "TestAppExceptionC.h"
	-@del /f/q "TestAppExceptionS.h"
	-@del /f/q "TestAppExceptionC.cpp"
	-@del /f/q "TestAppExceptionS.cpp"
	-@del /f/q "CancelledExceptionC.inl"
	-@del /f/q "CancelledExceptionS.inl"
	-@del /f/q "CancelledExceptionC.h"
	-@del /f/q "CancelledExceptionS.h"
	-@del /f/q "CancelledExceptionC.cpp"
	-@del /f/q "CancelledExceptionS.cpp"
	-@del /f/q "CustomExceptionC.inl"
	-@del /f/q "CustomExceptionS.inl"
	-@del /f/q "CustomExceptionC.h"
	-@del /f/q "CustomExceptionS.h"
	-@del /f/q "CustomExceptionC.cpp"
	-@del /f/q "CustomExceptionS.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\csd_pt_testinf\$(NULL)" mkdir "Debug\csd_pt_testinf"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D CSD_PT_TESTINF_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_PortableServerd.lib TAO_CodecFactoryd.lib TAO_PId.lib TAO_CSD_Frameworkd.lib TAO_CSD_ThreadPoold.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:".\CSD_PT_TestInfd.pdb" /machine:IA64 /out:".\CSD_PT_TestInfd.dll" /implib:"$(OUTDIR)\CSD_PT_TestInfd.lib"
LINK32_OBJS= \
	"$(INTDIR)\AppHelper.obj" \
	"$(INTDIR)\AppShutdown.obj" \
	"$(INTDIR)\ClientEngine.obj" \
	"$(INTDIR)\ClientTask.obj" \
	"$(INTDIR)\OrbRunner.obj" \
	"$(INTDIR)\OrbShutdownTask.obj" \
	"$(INTDIR)\OrbTask.obj" \
	"$(INTDIR)\TestAppBase.obj" \
	"$(INTDIR)\FooExceptionC.obj" \
	"$(INTDIR)\FooExceptionS.obj" \
	"$(INTDIR)\TestAppExceptionC.obj" \
	"$(INTDIR)\TestAppExceptionS.obj" \
	"$(INTDIR)\CancelledExceptionC.obj" \
	"$(INTDIR)\CancelledExceptionS.obj" \
	"$(INTDIR)\CustomExceptionC.obj" \
	"$(INTDIR)\CustomExceptionS.obj"

".\CSD_PT_TestInfd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist ".\CSD_PT_TestInfd.dll.manifest" mt.exe -manifest ".\CSD_PT_TestInfd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=.
INTDIR=Release\csd_pt_testinf\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) ".\CSD_PT_TestInf.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DCSD_PT_TESTINF_BUILD_DLL -f "Makefile.csd_pt_testinf.dep" "AppHelper.cpp" "AppShutdown.cpp" "ClientEngine.cpp" "ClientTask.cpp" "OrbRunner.cpp" "OrbShutdownTask.cpp" "OrbTask.cpp" "TestAppBase.cpp" "FooExceptionC.cpp" "FooExceptionS.cpp" "TestAppExceptionC.cpp" "TestAppExceptionS.cpp" "CancelledExceptionC.cpp" "CancelledExceptionS.cpp" "CustomExceptionC.cpp" "CustomExceptionS.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q ".\CSD_PT_TestInf.dll"
	-@del /f/q "$(OUTDIR)\CSD_PT_TestInf.lib"
	-@del /f/q "$(OUTDIR)\CSD_PT_TestInf.exp"
	-@del /f/q "$(OUTDIR)\CSD_PT_TestInf.ilk"
	-@del /f/q "FooExceptionC.inl"
	-@del /f/q "FooExceptionS.inl"
	-@del /f/q "FooExceptionC.h"
	-@del /f/q "FooExceptionS.h"
	-@del /f/q "FooExceptionC.cpp"
	-@del /f/q "FooExceptionS.cpp"
	-@del /f/q "TestAppExceptionC.inl"
	-@del /f/q "TestAppExceptionS.inl"
	-@del /f/q "TestAppExceptionC.h"
	-@del /f/q "TestAppExceptionS.h"
	-@del /f/q "TestAppExceptionC.cpp"
	-@del /f/q "TestAppExceptionS.cpp"
	-@del /f/q "CancelledExceptionC.inl"
	-@del /f/q "CancelledExceptionS.inl"
	-@del /f/q "CancelledExceptionC.h"
	-@del /f/q "CancelledExceptionS.h"
	-@del /f/q "CancelledExceptionC.cpp"
	-@del /f/q "CancelledExceptionS.cpp"
	-@del /f/q "CustomExceptionC.inl"
	-@del /f/q "CustomExceptionS.inl"
	-@del /f/q "CustomExceptionC.h"
	-@del /f/q "CustomExceptionS.h"
	-@del /f/q "CustomExceptionC.cpp"
	-@del /f/q "CustomExceptionS.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\csd_pt_testinf\$(NULL)" mkdir "Release\csd_pt_testinf"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\.." /I "..\..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D CSD_PT_TESTINF_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_PortableServer.lib TAO_CodecFactory.lib TAO_PI.lib TAO_CSD_Framework.lib TAO_CSD_ThreadPool.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:".\CSD_PT_TestInf.dll" /implib:"$(OUTDIR)\CSD_PT_TestInf.lib"
LINK32_OBJS= \
	"$(INTDIR)\AppHelper.obj" \
	"$(INTDIR)\AppShutdown.obj" \
	"$(INTDIR)\ClientEngine.obj" \
	"$(INTDIR)\ClientTask.obj" \
	"$(INTDIR)\OrbRunner.obj" \
	"$(INTDIR)\OrbShutdownTask.obj" \
	"$(INTDIR)\OrbTask.obj" \
	"$(INTDIR)\TestAppBase.obj" \
	"$(INTDIR)\FooExceptionC.obj" \
	"$(INTDIR)\FooExceptionS.obj" \
	"$(INTDIR)\TestAppExceptionC.obj" \
	"$(INTDIR)\TestAppExceptionS.obj" \
	"$(INTDIR)\CancelledExceptionC.obj" \
	"$(INTDIR)\CancelledExceptionS.obj" \
	"$(INTDIR)\CustomExceptionC.obj" \
	"$(INTDIR)\CustomExceptionS.obj"

".\CSD_PT_TestInf.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist ".\CSD_PT_TestInf.dll.manifest" mt.exe -manifest ".\CSD_PT_TestInf.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=.
INTDIR=Static_Debug\csd_pt_testinf\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CSD_PT_TestInfsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.csd_pt_testinf.dep" "AppHelper.cpp" "AppShutdown.cpp" "ClientEngine.cpp" "ClientTask.cpp" "OrbRunner.cpp" "OrbShutdownTask.cpp" "OrbTask.cpp" "TestAppBase.cpp" "FooExceptionC.cpp" "FooExceptionS.cpp" "TestAppExceptionC.cpp" "TestAppExceptionS.cpp" "CancelledExceptionC.cpp" "CancelledExceptionS.cpp" "CustomExceptionC.cpp" "CustomExceptionS.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CSD_PT_TestInfsd.lib"
	-@del /f/q "$(OUTDIR)\CSD_PT_TestInfsd.exp"
	-@del /f/q "$(OUTDIR)\CSD_PT_TestInfsd.ilk"
	-@del /f/q ".\CSD_PT_TestInfsd.pdb"
	-@del /f/q "FooExceptionC.inl"
	-@del /f/q "FooExceptionS.inl"
	-@del /f/q "FooExceptionC.h"
	-@del /f/q "FooExceptionS.h"
	-@del /f/q "FooExceptionC.cpp"
	-@del /f/q "FooExceptionS.cpp"
	-@del /f/q "TestAppExceptionC.inl"
	-@del /f/q "TestAppExceptionS.inl"
	-@del /f/q "TestAppExceptionC.h"
	-@del /f/q "TestAppExceptionS.h"
	-@del /f/q "TestAppExceptionC.cpp"
	-@del /f/q "TestAppExceptionS.cpp"
	-@del /f/q "CancelledExceptionC.inl"
	-@del /f/q "CancelledExceptionS.inl"
	-@del /f/q "CancelledExceptionC.h"
	-@del /f/q "CancelledExceptionS.h"
	-@del /f/q "CancelledExceptionC.cpp"
	-@del /f/q "CancelledExceptionS.cpp"
	-@del /f/q "CustomExceptionC.inl"
	-@del /f/q "CustomExceptionS.inl"
	-@del /f/q "CustomExceptionC.h"
	-@del /f/q "CustomExceptionS.h"
	-@del /f/q "CustomExceptionC.cpp"
	-@del /f/q "CustomExceptionS.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\csd_pt_testinf\$(NULL)" mkdir "Static_Debug\csd_pt_testinf"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd".\CSD_PT_TestInfsd.pdb" /I "..\..\..\.." /I "..\..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:".\CSD_PT_TestInfsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\AppHelper.obj" \
	"$(INTDIR)\AppShutdown.obj" \
	"$(INTDIR)\ClientEngine.obj" \
	"$(INTDIR)\ClientTask.obj" \
	"$(INTDIR)\OrbRunner.obj" \
	"$(INTDIR)\OrbShutdownTask.obj" \
	"$(INTDIR)\OrbTask.obj" \
	"$(INTDIR)\TestAppBase.obj" \
	"$(INTDIR)\FooExceptionC.obj" \
	"$(INTDIR)\FooExceptionS.obj" \
	"$(INTDIR)\TestAppExceptionC.obj" \
	"$(INTDIR)\TestAppExceptionS.obj" \
	"$(INTDIR)\CancelledExceptionC.obj" \
	"$(INTDIR)\CancelledExceptionS.obj" \
	"$(INTDIR)\CustomExceptionC.obj" \
	"$(INTDIR)\CustomExceptionS.obj"

"$(OUTDIR)\CSD_PT_TestInfsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CSD_PT_TestInfsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\CSD_PT_TestInfsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=.
INTDIR=Static_Release\csd_pt_testinf\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CSD_PT_TestInfs.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.csd_pt_testinf.dep" "AppHelper.cpp" "AppShutdown.cpp" "ClientEngine.cpp" "ClientTask.cpp" "OrbRunner.cpp" "OrbShutdownTask.cpp" "OrbTask.cpp" "TestAppBase.cpp" "FooExceptionC.cpp" "FooExceptionS.cpp" "TestAppExceptionC.cpp" "TestAppExceptionS.cpp" "CancelledExceptionC.cpp" "CancelledExceptionS.cpp" "CustomExceptionC.cpp" "CustomExceptionS.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CSD_PT_TestInfs.lib"
	-@del /f/q "$(OUTDIR)\CSD_PT_TestInfs.exp"
	-@del /f/q "$(OUTDIR)\CSD_PT_TestInfs.ilk"
	-@del /f/q "FooExceptionC.inl"
	-@del /f/q "FooExceptionS.inl"
	-@del /f/q "FooExceptionC.h"
	-@del /f/q "FooExceptionS.h"
	-@del /f/q "FooExceptionC.cpp"
	-@del /f/q "FooExceptionS.cpp"
	-@del /f/q "TestAppExceptionC.inl"
	-@del /f/q "TestAppExceptionS.inl"
	-@del /f/q "TestAppExceptionC.h"
	-@del /f/q "TestAppExceptionS.h"
	-@del /f/q "TestAppExceptionC.cpp"
	-@del /f/q "TestAppExceptionS.cpp"
	-@del /f/q "CancelledExceptionC.inl"
	-@del /f/q "CancelledExceptionS.inl"
	-@del /f/q "CancelledExceptionC.h"
	-@del /f/q "CancelledExceptionS.h"
	-@del /f/q "CancelledExceptionC.cpp"
	-@del /f/q "CancelledExceptionS.cpp"
	-@del /f/q "CustomExceptionC.inl"
	-@del /f/q "CustomExceptionS.inl"
	-@del /f/q "CustomExceptionC.h"
	-@del /f/q "CustomExceptionS.h"
	-@del /f/q "CustomExceptionC.cpp"
	-@del /f/q "CustomExceptionS.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\csd_pt_testinf\$(NULL)" mkdir "Static_Release\csd_pt_testinf"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\.." /I "..\..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:".\CSD_PT_TestInfs.lib"
LINK32_OBJS= \
	"$(INTDIR)\AppHelper.obj" \
	"$(INTDIR)\AppShutdown.obj" \
	"$(INTDIR)\ClientEngine.obj" \
	"$(INTDIR)\ClientTask.obj" \
	"$(INTDIR)\OrbRunner.obj" \
	"$(INTDIR)\OrbShutdownTask.obj" \
	"$(INTDIR)\OrbTask.obj" \
	"$(INTDIR)\TestAppBase.obj" \
	"$(INTDIR)\FooExceptionC.obj" \
	"$(INTDIR)\FooExceptionS.obj" \
	"$(INTDIR)\TestAppExceptionC.obj" \
	"$(INTDIR)\TestAppExceptionS.obj" \
	"$(INTDIR)\CancelledExceptionC.obj" \
	"$(INTDIR)\CancelledExceptionS.obj" \
	"$(INTDIR)\CustomExceptionC.obj" \
	"$(INTDIR)\CustomExceptionS.obj"

"$(OUTDIR)\CSD_PT_TestInfs.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CSD_PT_TestInfs.lib.manifest" mt.exe -manifest "$(OUTDIR)\CSD_PT_TestInfs.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.csd_pt_testinf.dep")
!INCLUDE "Makefile.csd_pt_testinf.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="AppHelper.cpp"

"$(INTDIR)\AppHelper.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AppHelper.obj" $(SOURCE)

SOURCE="AppShutdown.cpp"

"$(INTDIR)\AppShutdown.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AppShutdown.obj" $(SOURCE)

SOURCE="ClientEngine.cpp"

"$(INTDIR)\ClientEngine.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ClientEngine.obj" $(SOURCE)

SOURCE="ClientTask.cpp"

"$(INTDIR)\ClientTask.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ClientTask.obj" $(SOURCE)

SOURCE="OrbRunner.cpp"

"$(INTDIR)\OrbRunner.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\OrbRunner.obj" $(SOURCE)

SOURCE="OrbShutdownTask.cpp"

"$(INTDIR)\OrbShutdownTask.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\OrbShutdownTask.obj" $(SOURCE)

SOURCE="OrbTask.cpp"

"$(INTDIR)\OrbTask.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\OrbTask.obj" $(SOURCE)

SOURCE="TestAppBase.cpp"

"$(INTDIR)\TestAppBase.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TestAppBase.obj" $(SOURCE)

SOURCE="FooExceptionC.cpp"

"$(INTDIR)\FooExceptionC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\FooExceptionC.obj" $(SOURCE)

SOURCE="FooExceptionS.cpp"

"$(INTDIR)\FooExceptionS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\FooExceptionS.obj" $(SOURCE)

SOURCE="TestAppExceptionC.cpp"

"$(INTDIR)\TestAppExceptionC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TestAppExceptionC.obj" $(SOURCE)

SOURCE="TestAppExceptionS.cpp"

"$(INTDIR)\TestAppExceptionS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TestAppExceptionS.obj" $(SOURCE)

SOURCE="CancelledExceptionC.cpp"

"$(INTDIR)\CancelledExceptionC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CancelledExceptionC.obj" $(SOURCE)

SOURCE="CancelledExceptionS.cpp"

"$(INTDIR)\CancelledExceptionS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CancelledExceptionS.obj" $(SOURCE)

SOURCE="CustomExceptionC.cpp"

"$(INTDIR)\CustomExceptionC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CustomExceptionC.obj" $(SOURCE)

SOURCE="CustomExceptionS.cpp"

"$(INTDIR)\CustomExceptionS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CustomExceptionS.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="FooException.idl"

InputPath=FooException.idl

"FooExceptionC.inl" "FooExceptionS.inl" "FooExceptionC.h" "FooExceptionS.h" "FooExceptionC.cpp" "FooExceptionS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-FooException_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Wb,export_macro=CSD_PT_TestInf_Export -Wb,export_include=TestInf/CSD_PT_TestInf_Export.h "$(InputPath)"
<<

SOURCE="TestAppException.idl"

InputPath=TestAppException.idl

"TestAppExceptionC.inl" "TestAppExceptionS.inl" "TestAppExceptionC.h" "TestAppExceptionS.h" "TestAppExceptionC.cpp" "TestAppExceptionS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-TestAppException_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Wb,export_macro=CSD_PT_TestInf_Export -Wb,export_include=TestInf/CSD_PT_TestInf_Export.h "$(InputPath)"
<<

SOURCE="CancelledException.idl"

InputPath=CancelledException.idl

"CancelledExceptionC.inl" "CancelledExceptionS.inl" "CancelledExceptionC.h" "CancelledExceptionS.h" "CancelledExceptionC.cpp" "CancelledExceptionS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CancelledException_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Wb,export_macro=CSD_PT_TestInf_Export -Wb,export_include=TestInf/CSD_PT_TestInf_Export.h "$(InputPath)"
<<

SOURCE="CustomException.idl"

InputPath=CustomException.idl

"CustomExceptionC.inl" "CustomExceptionS.inl" "CustomExceptionC.h" "CustomExceptionS.h" "CustomExceptionC.cpp" "CustomExceptionS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CustomException_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Wb,export_macro=CSD_PT_TestInf_Export -Wb,export_include=TestInf/CSD_PT_TestInf_Export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="FooException.idl"

InputPath=FooException.idl

"FooExceptionC.inl" "FooExceptionS.inl" "FooExceptionC.h" "FooExceptionS.h" "FooExceptionC.cpp" "FooExceptionS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-FooException_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Wb,export_macro=CSD_PT_TestInf_Export -Wb,export_include=TestInf/CSD_PT_TestInf_Export.h "$(InputPath)"
<<

SOURCE="TestAppException.idl"

InputPath=TestAppException.idl

"TestAppExceptionC.inl" "TestAppExceptionS.inl" "TestAppExceptionC.h" "TestAppExceptionS.h" "TestAppExceptionC.cpp" "TestAppExceptionS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-TestAppException_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Wb,export_macro=CSD_PT_TestInf_Export -Wb,export_include=TestInf/CSD_PT_TestInf_Export.h "$(InputPath)"
<<

SOURCE="CancelledException.idl"

InputPath=CancelledException.idl

"CancelledExceptionC.inl" "CancelledExceptionS.inl" "CancelledExceptionC.h" "CancelledExceptionS.h" "CancelledExceptionC.cpp" "CancelledExceptionS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CancelledException_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Wb,export_macro=CSD_PT_TestInf_Export -Wb,export_include=TestInf/CSD_PT_TestInf_Export.h "$(InputPath)"
<<

SOURCE="CustomException.idl"

InputPath=CustomException.idl

"CustomExceptionC.inl" "CustomExceptionS.inl" "CustomExceptionC.h" "CustomExceptionS.h" "CustomExceptionC.cpp" "CustomExceptionS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CustomException_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Wb,export_macro=CSD_PT_TestInf_Export -Wb,export_include=TestInf/CSD_PT_TestInf_Export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="FooException.idl"

InputPath=FooException.idl

"FooExceptionC.inl" "FooExceptionS.inl" "FooExceptionC.h" "FooExceptionS.h" "FooExceptionC.cpp" "FooExceptionS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-FooException_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Wb,export_macro=CSD_PT_TestInf_Export -Wb,export_include=TestInf/CSD_PT_TestInf_Export.h "$(InputPath)"
<<

SOURCE="TestAppException.idl"

InputPath=TestAppException.idl

"TestAppExceptionC.inl" "TestAppExceptionS.inl" "TestAppExceptionC.h" "TestAppExceptionS.h" "TestAppExceptionC.cpp" "TestAppExceptionS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-TestAppException_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Wb,export_macro=CSD_PT_TestInf_Export -Wb,export_include=TestInf/CSD_PT_TestInf_Export.h "$(InputPath)"
<<

SOURCE="CancelledException.idl"

InputPath=CancelledException.idl

"CancelledExceptionC.inl" "CancelledExceptionS.inl" "CancelledExceptionC.h" "CancelledExceptionS.h" "CancelledExceptionC.cpp" "CancelledExceptionS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CancelledException_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Wb,export_macro=CSD_PT_TestInf_Export -Wb,export_include=TestInf/CSD_PT_TestInf_Export.h "$(InputPath)"
<<

SOURCE="CustomException.idl"

InputPath=CustomException.idl

"CustomExceptionC.inl" "CustomExceptionS.inl" "CustomExceptionC.h" "CustomExceptionS.h" "CustomExceptionC.cpp" "CustomExceptionS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CustomException_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Wb,export_macro=CSD_PT_TestInf_Export -Wb,export_include=TestInf/CSD_PT_TestInf_Export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="FooException.idl"

InputPath=FooException.idl

"FooExceptionC.inl" "FooExceptionS.inl" "FooExceptionC.h" "FooExceptionS.h" "FooExceptionC.cpp" "FooExceptionS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-FooException_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Wb,export_macro=CSD_PT_TestInf_Export -Wb,export_include=TestInf/CSD_PT_TestInf_Export.h "$(InputPath)"
<<

SOURCE="TestAppException.idl"

InputPath=TestAppException.idl

"TestAppExceptionC.inl" "TestAppExceptionS.inl" "TestAppExceptionC.h" "TestAppExceptionS.h" "TestAppExceptionC.cpp" "TestAppExceptionS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-TestAppException_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Wb,export_macro=CSD_PT_TestInf_Export -Wb,export_include=TestInf/CSD_PT_TestInf_Export.h "$(InputPath)"
<<

SOURCE="CancelledException.idl"

InputPath=CancelledException.idl

"CancelledExceptionC.inl" "CancelledExceptionS.inl" "CancelledExceptionC.h" "CancelledExceptionS.h" "CancelledExceptionC.cpp" "CancelledExceptionS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CancelledException_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Wb,export_macro=CSD_PT_TestInf_Export -Wb,export_include=TestInf/CSD_PT_TestInf_Export.h "$(InputPath)"
<<

SOURCE="CustomException.idl"

InputPath=CustomException.idl

"CustomExceptionC.inl" "CustomExceptionS.inl" "CustomExceptionC.h" "CustomExceptionS.h" "CustomExceptionC.cpp" "CustomExceptionS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CustomException_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Wb,export_macro=CSD_PT_TestInf_Export -Wb,export_include=TestInf/CSD_PT_TestInf_Export.h "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.csd_pt_testinf.dep")
	@echo Using "Makefile.csd_pt_testinf.dep"
!ELSE
	@echo Warning: cannot find "Makefile.csd_pt_testinf.dep"
!ENDIF
!ENDIF

