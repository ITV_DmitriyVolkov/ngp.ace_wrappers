//
// $Id: Roundtrip.cpp 935 2008-12-10 21:47:27Z mitza $
//
#include "Roundtrip.h"

ACE_RCSID(Thread_Pool_Latency, Roundtrip, "$Id: Roundtrip.cpp 935 2008-12-10 21:47:27Z mitza $")

Roundtrip::Roundtrip (CORBA::ORB_ptr orb)
  : orb_ (CORBA::ORB::_duplicate (orb))
{
}

Test::Timestamp
Roundtrip::test_method (Test::Timestamp send_time)
{
  return send_time;
}

void
Roundtrip::shutdown (void)
{
  this->orb_->shutdown (0);
}
