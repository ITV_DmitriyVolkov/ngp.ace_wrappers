#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake -value_template platforms=Win64 -make_coexistence -recurse -exclude TAO/CIAO -features qos=1,mfc=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: DII_Latency_Idl DII_Latency_Client DII_Latency_Server

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.DII_Latency_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.DII_Latency_Idl.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.DII_Latency_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.DII_Latency_Client.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.DII_Latency_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.DII_Latency_Server.mak CFG="$(CFG)" $(@)

DII_Latency_Idl:
	@echo Project: Makefile.DII_Latency_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.DII_Latency_Idl.mak CFG="$(CFG)" all

DII_Latency_Client: DII_Latency_Idl
	@echo Project: Makefile.DII_Latency_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.DII_Latency_Client.mak CFG="$(CFG)" all

DII_Latency_Server: DII_Latency_Idl
	@echo Project: Makefile.DII_Latency_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.DII_Latency_Server.mak CFG="$(CFG)" all

project_name_list:
	@echo DII_Latency_Client
	@echo DII_Latency_Idl
	@echo DII_Latency_Server
