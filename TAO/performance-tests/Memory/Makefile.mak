#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake -value_template platforms=Win64 -make_coexistence -recurse -exclude TAO/CIAO -features qos=1,mfc=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: Memory_IORsize_Idl Memory_IORsize_Client Memory_IORsize_Server Memory_Growth_Idl Memory_Growth_Client Memory_Growth_Server

clean depend generated realclean $(CUSTOM_TARGETS):
	@cd IORsize
	@echo Directory: IORsize
	@echo Project: Makefile.Memory_IORsize_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Memory_IORsize_Idl.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd IORsize
	@echo Directory: IORsize
	@echo Project: Makefile.Memory_IORsize_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Memory_IORsize_Client.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd IORsize
	@echo Directory: IORsize
	@echo Project: Makefile.Memory_IORsize_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Memory_IORsize_Server.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Single_Threaded
	@echo Directory: Single_Threaded
	@echo Project: Makefile.Memory_Growth_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Memory_Growth_Idl.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Single_Threaded
	@echo Directory: Single_Threaded
	@echo Project: Makefile.Memory_Growth_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Memory_Growth_Client.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Single_Threaded
	@echo Directory: Single_Threaded
	@echo Project: Makefile.Memory_Growth_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Memory_Growth_Server.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)

Memory_IORsize_Idl:
	@cd IORsize
	@echo Directory: IORsize
	@echo Project: Makefile.Memory_IORsize_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Memory_IORsize_Idl.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Memory_IORsize_Client: Memory_IORsize_Idl
	@cd IORsize
	@echo Directory: IORsize
	@echo Project: Makefile.Memory_IORsize_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Memory_IORsize_Client.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Memory_IORsize_Server: Memory_IORsize_Idl
	@cd IORsize
	@echo Directory: IORsize
	@echo Project: Makefile.Memory_IORsize_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Memory_IORsize_Server.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Memory_Growth_Idl:
	@cd Single_Threaded
	@echo Directory: Single_Threaded
	@echo Project: Makefile.Memory_Growth_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Memory_Growth_Idl.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Memory_Growth_Client: Memory_Growth_Idl
	@cd Single_Threaded
	@echo Directory: Single_Threaded
	@echo Project: Makefile.Memory_Growth_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Memory_Growth_Client.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Memory_Growth_Server: Memory_Growth_Idl
	@cd Single_Threaded
	@echo Directory: Single_Threaded
	@echo Project: Makefile.Memory_Growth_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Memory_Growth_Server.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

project_name_list:
	@echo Memory_IORsize_Client
	@echo Memory_IORsize_Idl
	@echo Memory_IORsize_Server
	@echo Memory_Growth_Client
	@echo Memory_Growth_Idl
	@echo Memory_Growth_Server
