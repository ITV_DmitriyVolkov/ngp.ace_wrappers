# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.CIAO_RTEvent.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "CIAO_RTEventC.inl" "CIAO_RTEventS.inl" "CIAO_RTEventC.h" "CIAO_RTEventS.h" "CIAO_RTEventC.cpp" "CIAO_RTEventS.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\..\..\..\lib
INTDIR=Debug\CIAO_RTEvent\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\..\lib\CIAO_RTEventd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -I"..\..\..\..\orbsvcs" -I"..\..\.." -I"..\..\..\ciao" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DCIAO_RTEVENT_BUILD_DLL -f "Makefile.CIAO_RTEvent.dep" "CIAO_RTEvent.cpp" "CIAO_RTEventC.cpp" "CIAO_RTEventS.cpp" "SimpleAddressServer.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_RTEventd.pdb"
	-@del /f/q "..\..\..\..\..\lib\CIAO_RTEventd.dll"
	-@del /f/q "$(OUTDIR)\CIAO_RTEventd.lib"
	-@del /f/q "$(OUTDIR)\CIAO_RTEventd.exp"
	-@del /f/q "$(OUTDIR)\CIAO_RTEventd.ilk"
	-@del /f/q "CIAO_RTEventC.inl"
	-@del /f/q "CIAO_RTEventS.inl"
	-@del /f/q "CIAO_RTEventC.h"
	-@del /f/q "CIAO_RTEventS.h"
	-@del /f/q "CIAO_RTEventC.cpp"
	-@del /f/q "CIAO_RTEventS.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\CIAO_RTEvent\$(NULL)" mkdir "Debug\CIAO_RTEvent"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"$(INTDIR)/" /I "..\..\..\..\.." /I "..\..\..\.." /I "..\..\..\..\orbsvcs" /I "..\..\.." /I "..\..\..\ciao" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D CIAO_RTEVENT_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_CosNamingd.lib TAO_Valuetyped.lib TAO_PortableServerd.lib TAO_CodecFactoryd.lib TAO_PId.lib TAO_Messagingd.lib TAO_Svc_Utilsd.lib TAO_RTEventd.lib TAO_RTEvent_Skeld.lib TAO_RTEvent_Servd.lib TAO_IFR_Clientd.lib CIAO_Clientd.lib CIAO_Containerd.lib CIAO_Events_Based.lib /libpath:"." /libpath:"..\..\..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\..\..\..\lib\CIAO_RTEventd.pdb" /machine:IA64 /out:"..\..\..\..\..\lib\CIAO_RTEventd.dll" /implib:"$(OUTDIR)\CIAO_RTEventd.lib"
LINK32_OBJS= \
	"$(INTDIR)\CIAO_RTEvent.obj" \
	"$(INTDIR)\CIAO_RTEventC.obj" \
	"$(INTDIR)\CIAO_RTEventS.obj" \
	"$(INTDIR)\SimpleAddressServer.obj"

"..\..\..\..\..\lib\CIAO_RTEventd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\..\lib\CIAO_RTEventd.dll.manifest" mt.exe -manifest "..\..\..\..\..\lib\CIAO_RTEventd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\..\..\..\lib
INTDIR=Release\CIAO_RTEvent\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\..\lib\CIAO_RTEvent.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -I"..\..\..\..\orbsvcs" -I"..\..\.." -I"..\..\..\ciao" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DCIAO_RTEVENT_BUILD_DLL -f "Makefile.CIAO_RTEvent.dep" "CIAO_RTEvent.cpp" "CIAO_RTEventC.cpp" "CIAO_RTEventS.cpp" "SimpleAddressServer.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\..\..\..\lib\CIAO_RTEvent.dll"
	-@del /f/q "$(OUTDIR)\CIAO_RTEvent.lib"
	-@del /f/q "$(OUTDIR)\CIAO_RTEvent.exp"
	-@del /f/q "$(OUTDIR)\CIAO_RTEvent.ilk"
	-@del /f/q "CIAO_RTEventC.inl"
	-@del /f/q "CIAO_RTEventS.inl"
	-@del /f/q "CIAO_RTEventC.h"
	-@del /f/q "CIAO_RTEventS.h"
	-@del /f/q "CIAO_RTEventC.cpp"
	-@del /f/q "CIAO_RTEventS.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\CIAO_RTEvent\$(NULL)" mkdir "Release\CIAO_RTEvent"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\..\.." /I "..\..\..\.." /I "..\..\..\..\orbsvcs" /I "..\..\.." /I "..\..\..\ciao" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D CIAO_RTEVENT_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_CosNaming.lib TAO_Valuetype.lib TAO_PortableServer.lib TAO_CodecFactory.lib TAO_PI.lib TAO_Messaging.lib TAO_Svc_Utils.lib TAO_RTEvent.lib TAO_RTEvent_Skel.lib TAO_RTEvent_Serv.lib TAO_IFR_Client.lib CIAO_Client.lib CIAO_Container.lib CIAO_Events_Base.lib /libpath:"." /libpath:"..\..\..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\..\..\..\lib\CIAO_RTEvent.dll" /implib:"$(OUTDIR)\CIAO_RTEvent.lib"
LINK32_OBJS= \
	"$(INTDIR)\CIAO_RTEvent.obj" \
	"$(INTDIR)\CIAO_RTEventC.obj" \
	"$(INTDIR)\CIAO_RTEventS.obj" \
	"$(INTDIR)\SimpleAddressServer.obj"

"..\..\..\..\..\lib\CIAO_RTEvent.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\..\lib\CIAO_RTEvent.dll.manifest" mt.exe -manifest "..\..\..\..\..\lib\CIAO_RTEvent.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\..\..\..\lib
INTDIR=Static_Debug\CIAO_RTEvent\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CIAO_RTEventsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -I"..\..\..\..\orbsvcs" -I"..\..\.." -I"..\..\..\ciao" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CIAO_RTEvent.dep" "CIAO_RTEvent.cpp" "CIAO_RTEventC.cpp" "CIAO_RTEventS.cpp" "SimpleAddressServer.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_RTEventsd.lib"
	-@del /f/q "$(OUTDIR)\CIAO_RTEventsd.exp"
	-@del /f/q "$(OUTDIR)\CIAO_RTEventsd.ilk"
	-@del /f/q "..\..\..\..\..\lib\CIAO_RTEventsd.pdb"
	-@del /f/q "CIAO_RTEventC.inl"
	-@del /f/q "CIAO_RTEventS.inl"
	-@del /f/q "CIAO_RTEventC.h"
	-@del /f/q "CIAO_RTEventS.h"
	-@del /f/q "CIAO_RTEventC.cpp"
	-@del /f/q "CIAO_RTEventS.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\CIAO_RTEvent\$(NULL)" mkdir "Static_Debug\CIAO_RTEvent"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"..\..\..\..\..\lib\CIAO_RTEventsd.pdb" /I "..\..\..\..\.." /I "..\..\..\.." /I "..\..\..\..\orbsvcs" /I "..\..\.." /I "..\..\..\ciao" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\..\lib\CIAO_RTEventsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\CIAO_RTEvent.obj" \
	"$(INTDIR)\CIAO_RTEventC.obj" \
	"$(INTDIR)\CIAO_RTEventS.obj" \
	"$(INTDIR)\SimpleAddressServer.obj"

"$(OUTDIR)\CIAO_RTEventsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CIAO_RTEventsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\CIAO_RTEventsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\..\..\..\lib
INTDIR=Static_Release\CIAO_RTEvent\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CIAO_RTEvents.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -I"..\..\..\..\orbsvcs" -I"..\..\.." -I"..\..\..\ciao" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CIAO_RTEvent.dep" "CIAO_RTEvent.cpp" "CIAO_RTEventC.cpp" "CIAO_RTEventS.cpp" "SimpleAddressServer.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_RTEvents.lib"
	-@del /f/q "$(OUTDIR)\CIAO_RTEvents.exp"
	-@del /f/q "$(OUTDIR)\CIAO_RTEvents.ilk"
	-@del /f/q "CIAO_RTEventC.inl"
	-@del /f/q "CIAO_RTEventS.inl"
	-@del /f/q "CIAO_RTEventC.h"
	-@del /f/q "CIAO_RTEventS.h"
	-@del /f/q "CIAO_RTEventC.cpp"
	-@del /f/q "CIAO_RTEventS.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\CIAO_RTEvent\$(NULL)" mkdir "Static_Release\CIAO_RTEvent"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\..\.." /I "..\..\..\.." /I "..\..\..\..\orbsvcs" /I "..\..\.." /I "..\..\..\ciao" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\..\lib\CIAO_RTEvents.lib"
LINK32_OBJS= \
	"$(INTDIR)\CIAO_RTEvent.obj" \
	"$(INTDIR)\CIAO_RTEventC.obj" \
	"$(INTDIR)\CIAO_RTEventS.obj" \
	"$(INTDIR)\SimpleAddressServer.obj"

"$(OUTDIR)\CIAO_RTEvents.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CIAO_RTEvents.lib.manifest" mt.exe -manifest "$(OUTDIR)\CIAO_RTEvents.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CIAO_RTEvent.dep")
!INCLUDE "Makefile.CIAO_RTEvent.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="CIAO_RTEvent.cpp"

"$(INTDIR)\CIAO_RTEvent.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CIAO_RTEvent.obj" $(SOURCE)

SOURCE="CIAO_RTEventC.cpp"

"$(INTDIR)\CIAO_RTEventC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CIAO_RTEventC.obj" $(SOURCE)

SOURCE="CIAO_RTEventS.cpp"

"$(INTDIR)\CIAO_RTEventS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CIAO_RTEventS.obj" $(SOURCE)

SOURCE="SimpleAddressServer.cpp"

"$(INTDIR)\SimpleAddressServer.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\SimpleAddressServer.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="CIAO_RTEvent.idl"

InputPath=CIAO_RTEvent.idl

"CIAO_RTEventC.inl" "CIAO_RTEventS.inl" "CIAO_RTEventC.h" "CIAO_RTEventS.h" "CIAO_RTEventC.cpp" "CIAO_RTEventS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CIAO_RTEvent_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. -I..\..\..\../orbsvcs -I..\..\.. -I..\..\../ciao -Wb,export_include=CIAO_RTEVENT_Export.h -Wb,export_macro=CIAO_RTEVENT_Export "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="CIAO_RTEvent.idl"

InputPath=CIAO_RTEvent.idl

"CIAO_RTEventC.inl" "CIAO_RTEventS.inl" "CIAO_RTEventC.h" "CIAO_RTEventS.h" "CIAO_RTEventC.cpp" "CIAO_RTEventS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CIAO_RTEvent_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. -I..\..\..\../orbsvcs -I..\..\.. -I..\..\../ciao -Wb,export_include=CIAO_RTEVENT_Export.h -Wb,export_macro=CIAO_RTEVENT_Export "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="CIAO_RTEvent.idl"

InputPath=CIAO_RTEvent.idl

"CIAO_RTEventC.inl" "CIAO_RTEventS.inl" "CIAO_RTEventC.h" "CIAO_RTEventS.h" "CIAO_RTEventC.cpp" "CIAO_RTEventS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CIAO_RTEvent_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. -I..\..\..\../orbsvcs -I..\..\.. -I..\..\../ciao -Wb,export_include=CIAO_RTEVENT_Export.h -Wb,export_macro=CIAO_RTEVENT_Export "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="CIAO_RTEvent.idl"

InputPath=CIAO_RTEvent.idl

"CIAO_RTEventC.inl" "CIAO_RTEventS.inl" "CIAO_RTEventC.h" "CIAO_RTEventS.h" "CIAO_RTEventC.cpp" "CIAO_RTEventS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CIAO_RTEvent_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. -I..\..\..\../orbsvcs -I..\..\.. -I..\..\../ciao -Wb,export_include=CIAO_RTEVENT_Export.h -Wb,export_macro=CIAO_RTEVENT_Export "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CIAO_RTEvent.dep")
	@echo Using "Makefile.CIAO_RTEvent.dep"
!ELSE
	@echo Warning: cannot find "Makefile.CIAO_RTEvent.dep"
!ENDIF
!ENDIF

