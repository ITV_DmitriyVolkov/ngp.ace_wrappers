//=============================================================================
/**
 *  @file CIAO_EventServiceBase.cpp
 *
 *  $Id: CIAO_EventServiceBase.cpp 935 2008-12-10 21:47:27Z mitza $
 *
 *  @author Gan Deng <dengg@dre.vanderbilt.edu>
 */
//=============================================================================

#include "CIAO_EventServiceBase.h"

namespace CIAO
{
  EventServiceBase::EventServiceBase (void)
  {
  }

  EventServiceBase::~EventServiceBase (void)
  {
  }

  void
  EventServiceBase::ciao_push_event (Components::EventBase * /* evt */,
                                     const char * /* source_id */,
                                     CORBA::TypeCode_ptr /* tc */)
  {
  }
}
