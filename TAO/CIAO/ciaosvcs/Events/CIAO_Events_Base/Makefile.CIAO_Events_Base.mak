# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.CIAO_Events_Base.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "CIAO_EventsC.inl" "CIAO_EventsS.inl" "CIAO_EventsC.h" "CIAO_EventsS.h" "CIAO_EventsC.cpp" "CIAO_EventsS.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\..\..\..\lib
INTDIR=Debug\CIAO_Events_Base\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\..\lib\CIAO_Events_Based.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -I"..\..\..\..\orbsvcs" -I"..\..\.." -I"..\..\..\ciao" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DCIAO_EVENTS_BUILD_DLL -f "Makefile.CIAO_Events_Base.dep" "CIAO_EventServiceBase.cpp" "CIAO_EventsC.cpp" "CIAO_EventsS.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_Events_Based.pdb"
	-@del /f/q "..\..\..\..\..\lib\CIAO_Events_Based.dll"
	-@del /f/q "$(OUTDIR)\CIAO_Events_Based.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Events_Based.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Events_Based.ilk"
	-@del /f/q "CIAO_EventsC.inl"
	-@del /f/q "CIAO_EventsS.inl"
	-@del /f/q "CIAO_EventsC.h"
	-@del /f/q "CIAO_EventsS.h"
	-@del /f/q "CIAO_EventsC.cpp"
	-@del /f/q "CIAO_EventsS.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\CIAO_Events_Base\$(NULL)" mkdir "Debug\CIAO_Events_Base"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"$(INTDIR)/" /I "..\..\..\..\.." /I "..\..\..\.." /I "..\..\..\..\orbsvcs" /I "..\..\.." /I "..\..\..\ciao" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D CIAO_EVENTS_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_IFR_Clientd.lib TAO_Valuetyped.lib TAO_CodecFactoryd.lib TAO_PId.lib CIAO_Clientd.lib TAO_PortableServerd.lib CIAO_Containerd.lib /libpath:"." /libpath:"..\..\..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\..\..\..\lib\CIAO_Events_Based.pdb" /machine:IA64 /out:"..\..\..\..\..\lib\CIAO_Events_Based.dll" /implib:"$(OUTDIR)\CIAO_Events_Based.lib"
LINK32_OBJS= \
	"$(INTDIR)\CIAO_EventServiceBase.obj" \
	"$(INTDIR)\CIAO_EventsC.obj" \
	"$(INTDIR)\CIAO_EventsS.obj"

"..\..\..\..\..\lib\CIAO_Events_Based.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\..\lib\CIAO_Events_Based.dll.manifest" mt.exe -manifest "..\..\..\..\..\lib\CIAO_Events_Based.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\..\..\..\lib
INTDIR=Release\CIAO_Events_Base\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\..\lib\CIAO_Events_Base.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -I"..\..\..\..\orbsvcs" -I"..\..\.." -I"..\..\..\ciao" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DCIAO_EVENTS_BUILD_DLL -f "Makefile.CIAO_Events_Base.dep" "CIAO_EventServiceBase.cpp" "CIAO_EventsC.cpp" "CIAO_EventsS.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\..\..\..\lib\CIAO_Events_Base.dll"
	-@del /f/q "$(OUTDIR)\CIAO_Events_Base.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Events_Base.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Events_Base.ilk"
	-@del /f/q "CIAO_EventsC.inl"
	-@del /f/q "CIAO_EventsS.inl"
	-@del /f/q "CIAO_EventsC.h"
	-@del /f/q "CIAO_EventsS.h"
	-@del /f/q "CIAO_EventsC.cpp"
	-@del /f/q "CIAO_EventsS.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\CIAO_Events_Base\$(NULL)" mkdir "Release\CIAO_Events_Base"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\..\.." /I "..\..\..\.." /I "..\..\..\..\orbsvcs" /I "..\..\.." /I "..\..\..\ciao" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D CIAO_EVENTS_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_IFR_Client.lib TAO_Valuetype.lib TAO_CodecFactory.lib TAO_PI.lib CIAO_Client.lib TAO_PortableServer.lib CIAO_Container.lib /libpath:"." /libpath:"..\..\..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\..\..\..\lib\CIAO_Events_Base.dll" /implib:"$(OUTDIR)\CIAO_Events_Base.lib"
LINK32_OBJS= \
	"$(INTDIR)\CIAO_EventServiceBase.obj" \
	"$(INTDIR)\CIAO_EventsC.obj" \
	"$(INTDIR)\CIAO_EventsS.obj"

"..\..\..\..\..\lib\CIAO_Events_Base.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\..\lib\CIAO_Events_Base.dll.manifest" mt.exe -manifest "..\..\..\..\..\lib\CIAO_Events_Base.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\..\..\..\lib
INTDIR=Static_Debug\CIAO_Events_Base\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CIAO_Events_Basesd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -I"..\..\..\..\orbsvcs" -I"..\..\.." -I"..\..\..\ciao" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CIAO_Events_Base.dep" "CIAO_EventServiceBase.cpp" "CIAO_EventsC.cpp" "CIAO_EventsS.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_Events_Basesd.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Events_Basesd.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Events_Basesd.ilk"
	-@del /f/q "..\..\..\..\..\lib\CIAO_Events_Basesd.pdb"
	-@del /f/q "CIAO_EventsC.inl"
	-@del /f/q "CIAO_EventsS.inl"
	-@del /f/q "CIAO_EventsC.h"
	-@del /f/q "CIAO_EventsS.h"
	-@del /f/q "CIAO_EventsC.cpp"
	-@del /f/q "CIAO_EventsS.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\CIAO_Events_Base\$(NULL)" mkdir "Static_Debug\CIAO_Events_Base"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"..\..\..\..\..\lib\CIAO_Events_Basesd.pdb" /I "..\..\..\..\.." /I "..\..\..\.." /I "..\..\..\..\orbsvcs" /I "..\..\.." /I "..\..\..\ciao" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\..\lib\CIAO_Events_Basesd.lib"
LINK32_OBJS= \
	"$(INTDIR)\CIAO_EventServiceBase.obj" \
	"$(INTDIR)\CIAO_EventsC.obj" \
	"$(INTDIR)\CIAO_EventsS.obj"

"$(OUTDIR)\CIAO_Events_Basesd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CIAO_Events_Basesd.lib.manifest" mt.exe -manifest "$(OUTDIR)\CIAO_Events_Basesd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\..\..\..\lib
INTDIR=Static_Release\CIAO_Events_Base\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CIAO_Events_Bases.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -I"..\..\..\..\orbsvcs" -I"..\..\.." -I"..\..\..\ciao" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CIAO_Events_Base.dep" "CIAO_EventServiceBase.cpp" "CIAO_EventsC.cpp" "CIAO_EventsS.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_Events_Bases.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Events_Bases.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Events_Bases.ilk"
	-@del /f/q "CIAO_EventsC.inl"
	-@del /f/q "CIAO_EventsS.inl"
	-@del /f/q "CIAO_EventsC.h"
	-@del /f/q "CIAO_EventsS.h"
	-@del /f/q "CIAO_EventsC.cpp"
	-@del /f/q "CIAO_EventsS.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\CIAO_Events_Base\$(NULL)" mkdir "Static_Release\CIAO_Events_Base"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\..\.." /I "..\..\..\.." /I "..\..\..\..\orbsvcs" /I "..\..\.." /I "..\..\..\ciao" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\..\lib\CIAO_Events_Bases.lib"
LINK32_OBJS= \
	"$(INTDIR)\CIAO_EventServiceBase.obj" \
	"$(INTDIR)\CIAO_EventsC.obj" \
	"$(INTDIR)\CIAO_EventsS.obj"

"$(OUTDIR)\CIAO_Events_Bases.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CIAO_Events_Bases.lib.manifest" mt.exe -manifest "$(OUTDIR)\CIAO_Events_Bases.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CIAO_Events_Base.dep")
!INCLUDE "Makefile.CIAO_Events_Base.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="CIAO_EventServiceBase.cpp"

"$(INTDIR)\CIAO_EventServiceBase.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CIAO_EventServiceBase.obj" $(SOURCE)

SOURCE="CIAO_EventsC.cpp"

"$(INTDIR)\CIAO_EventsC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CIAO_EventsC.obj" $(SOURCE)

SOURCE="CIAO_EventsS.cpp"

"$(INTDIR)\CIAO_EventsS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CIAO_EventsS.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="CIAO_Events.idl"

InputPath=CIAO_Events.idl

"CIAO_EventsC.inl" "CIAO_EventsS.inl" "CIAO_EventsC.h" "CIAO_EventsS.h" "CIAO_EventsC.cpp" "CIAO_EventsS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CIAO_Events_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. -I..\..\..\../orbsvcs -I..\..\.. -I..\..\../ciao -Wb,export_include=CIAO_Events_Export.h -Wb,export_macro=CIAO_EVENTS_Export "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="CIAO_Events.idl"

InputPath=CIAO_Events.idl

"CIAO_EventsC.inl" "CIAO_EventsS.inl" "CIAO_EventsC.h" "CIAO_EventsS.h" "CIAO_EventsC.cpp" "CIAO_EventsS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CIAO_Events_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. -I..\..\..\../orbsvcs -I..\..\.. -I..\..\../ciao -Wb,export_include=CIAO_Events_Export.h -Wb,export_macro=CIAO_EVENTS_Export "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="CIAO_Events.idl"

InputPath=CIAO_Events.idl

"CIAO_EventsC.inl" "CIAO_EventsS.inl" "CIAO_EventsC.h" "CIAO_EventsS.h" "CIAO_EventsC.cpp" "CIAO_EventsS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CIAO_Events_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. -I..\..\..\../orbsvcs -I..\..\.. -I..\..\../ciao -Wb,export_include=CIAO_Events_Export.h -Wb,export_macro=CIAO_EVENTS_Export "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="CIAO_Events.idl"

InputPath=CIAO_Events.idl

"CIAO_EventsC.inl" "CIAO_EventsS.inl" "CIAO_EventsC.h" "CIAO_EventsS.h" "CIAO_EventsC.cpp" "CIAO_EventsS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CIAO_Events_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. -I..\..\..\../orbsvcs -I..\..\.. -I..\..\../ciao -Wb,export_include=CIAO_Events_Export.h -Wb,export_macro=CIAO_EVENTS_Export "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CIAO_Events_Base.dep")
	@echo Using "Makefile.CIAO_Events_Base.dep"
!ELSE
	@echo Warning: cannot find "Makefile.CIAO_Events_Base.dep"
!ENDIF
!ENDIF

