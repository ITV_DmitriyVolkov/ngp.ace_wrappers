
// -*- C++ -*-
// $Id: StockBroker_exec_export.h 982 2009-01-02 20:12:14Z mitza $
// Definition for Win32 Export directives.
// This file is generated automatically by generate_export_file.pl STOCKBROKER_EXEC
// ------------------------------
#ifndef STOCKBROKER_EXEC_EXPORT_H
#define STOCKBROKER_EXEC_EXPORT_H

#include "ace/config-all.h"

#if defined (ACE_AS_STATIC_LIBS) && !defined (STOCKBROKER_EXEC_HAS_DLL)
#  define STOCKBROKER_EXEC_HAS_DLL 0
#endif /* ACE_AS_STATIC_LIBS && STOCKBROKER_EXEC_HAS_DLL */

#if !defined (STOCKBROKER_EXEC_HAS_DLL)
#  define STOCKBROKER_EXEC_HAS_DLL 1
#endif /* ! STOCKBROKER_EXEC_HAS_DLL */

#if defined (STOCKBROKER_EXEC_HAS_DLL) && (STOCKBROKER_EXEC_HAS_DLL == 1)
#  if defined (STOCKBROKER_EXEC_BUILD_DLL)
#    define STOCKBROKER_EXEC_Export ACE_Proper_Export_Flag
#    define STOCKBROKER_EXEC_SINGLETON_DECLARATION(T) ACE_EXPORT_SINGLETON_DECLARATION (T)
#    define STOCKBROKER_EXEC_SINGLETON_DECLARE(SINGLETON_TYPE, CLASS, LOCK) ACE_EXPORT_SINGLETON_DECLARE(SINGLETON_TYPE, CLASS, LOCK)
#  else /* STOCKBROKER_EXEC_BUILD_DLL */
#    define STOCKBROKER_EXEC_Export ACE_Proper_Import_Flag
#    define STOCKBROKER_EXEC_SINGLETON_DECLARATION(T) ACE_IMPORT_SINGLETON_DECLARATION (T)
#    define STOCKBROKER_EXEC_SINGLETON_DECLARE(SINGLETON_TYPE, CLASS, LOCK) ACE_IMPORT_SINGLETON_DECLARE(SINGLETON_TYPE, CLASS, LOCK)
#  endif /* STOCKBROKER_EXEC_BUILD_DLL */
#else /* STOCKBROKER_EXEC_HAS_DLL == 1 */
#  define STOCKBROKER_EXEC_Export
#  define STOCKBROKER_EXEC_SINGLETON_DECLARATION(T)
#  define STOCKBROKER_EXEC_SINGLETON_DECLARE(SINGLETON_TYPE, CLASS, LOCK)
#endif /* STOCKBROKER_EXEC_HAS_DLL == 1 */

// Set STOCKBROKER_EXEC_NTRACE = 0 to turn on library specific tracing even if
// tracing is turned off for ACE.
#if !defined (STOCKBROKER_EXEC_NTRACE)
#  if (ACE_NTRACE == 1)
#    define STOCKBROKER_EXEC_NTRACE 1
#  else /* (ACE_NTRACE == 1) */
#    define STOCKBROKER_EXEC_NTRACE 0
#  endif /* (ACE_NTRACE == 1) */
#endif /* !STOCKBROKER_EXEC_NTRACE */

#if (STOCKBROKER_EXEC_NTRACE == 1)
#  define STOCKBROKER_EXEC_TRACE(X)
#else /* (STOCKBROKER_EXEC_NTRACE == 1) */
#  if !defined (ACE_HAS_TRACE)
#    define ACE_HAS_TRACE
#  endif /* ACE_HAS_TRACE */
#  define STOCKBROKER_EXEC_TRACE(X) ACE_TRACE_IMPL(X)
#  include "ace/Trace.h"
#endif /* (STOCKBROKER_EXEC_NTRACE == 1) */

#endif /* STOCKBROKER_EXEC_EXPORT_H */

// End of auto generated file.
