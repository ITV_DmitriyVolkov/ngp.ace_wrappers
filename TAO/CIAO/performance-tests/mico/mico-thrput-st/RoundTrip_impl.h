//
//$Id: RoundTrip_impl.h 14 2007-02-01 15:49:12Z mitza $
//
#include "RoundTrip.h"

class RoundTrip_impl : virtual public POA_Roundtrip {
 public:
  CORBA::ULongLong test_method (CORBA::ULongLong send_time);
};
