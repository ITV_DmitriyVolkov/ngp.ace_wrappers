#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake TAO/CIAO -value_template platforms=Win64 -make_coexistence -features qos=1,mfc=1,boost=1,cidl=1,ciao=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: Protocols_idl_gen Protocols_stub Protocols_skel

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.Protocols_idl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Protocols_idl_gen.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Protocols_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Protocols_stub.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Protocols_skel.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Protocols_skel.mak CFG="$(CFG)" $(@)

Protocols_idl_gen:
	@echo Project: Makefile.Protocols_idl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Protocols_idl_gen.mak CFG="$(CFG)" all

Protocols_stub: Protocols_idl_gen
	@echo Project: Makefile.Protocols_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Protocols_stub.mak CFG="$(CFG)" all

Protocols_skel: Protocols_stub
	@echo Project: Makefile.Protocols_skel.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Protocols_skel.mak CFG="$(CFG)" all

project_name_list:
	@echo Protocols_idl_gen
	@echo Protocols_skel
	@echo Protocols_stub
