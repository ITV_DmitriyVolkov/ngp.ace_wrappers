# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.Receptacles_stub.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "ReceptaclesC.inl" "ReceptaclesS.inl" "ReceptaclesC.h" "ReceptaclesS.h" "ReceptaclesC.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\..\..\..\..\..\lib
INTDIR=Debug\Receptacles_stub\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\..\..\..\lib\Receptacles_stubd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\..\..\.." -I"..\..\..\..\..\.." -I"..\..\..\..\.." -I"..\..\..\..\..\ciao" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DRECEPTACLES_STUB_BUILD_DLL -f "Makefile.Receptacles_stub.dep" "ReceptaclesC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\Receptacles_stubd.pdb"
	-@del /f/q "..\..\..\..\..\..\..\lib\Receptacles_stubd.dll"
	-@del /f/q "$(OUTDIR)\Receptacles_stubd.lib"
	-@del /f/q "$(OUTDIR)\Receptacles_stubd.exp"
	-@del /f/q "$(OUTDIR)\Receptacles_stubd.ilk"
	-@del /f/q "ReceptaclesC.inl"
	-@del /f/q "ReceptaclesS.inl"
	-@del /f/q "ReceptaclesC.h"
	-@del /f/q "ReceptaclesS.h"
	-@del /f/q "ReceptaclesC.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\Receptacles_stub\$(NULL)" mkdir "Debug\Receptacles_stub"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"$(INTDIR)/" /I "..\..\..\..\..\..\.." /I "..\..\..\..\..\.." /I "..\..\..\..\.." /I "..\..\..\..\..\ciao" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D RECEPTACLES_STUB_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_IFR_Clientd.lib TAO_Valuetyped.lib TAO_CodecFactoryd.lib TAO_PId.lib CIAO_Clientd.lib /libpath:"." /libpath:"..\..\..\..\..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\..\..\..\..\..\lib\Receptacles_stubd.pdb" /machine:IA64 /out:"..\..\..\..\..\..\..\lib\Receptacles_stubd.dll" /implib:"$(OUTDIR)\Receptacles_stubd.lib"
LINK32_OBJS= \
	"$(INTDIR)\ReceptaclesC.obj"

"..\..\..\..\..\..\..\lib\Receptacles_stubd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\..\..\..\lib\Receptacles_stubd.dll.manifest" mt.exe -manifest "..\..\..\..\..\..\..\lib\Receptacles_stubd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\..\..\..\..\..\lib
INTDIR=Release\Receptacles_stub\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\..\..\..\lib\Receptacles_stub.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\..\..\.." -I"..\..\..\..\..\.." -I"..\..\..\..\.." -I"..\..\..\..\..\ciao" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DRECEPTACLES_STUB_BUILD_DLL -f "Makefile.Receptacles_stub.dep" "ReceptaclesC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\..\..\..\..\..\lib\Receptacles_stub.dll"
	-@del /f/q "$(OUTDIR)\Receptacles_stub.lib"
	-@del /f/q "$(OUTDIR)\Receptacles_stub.exp"
	-@del /f/q "$(OUTDIR)\Receptacles_stub.ilk"
	-@del /f/q "ReceptaclesC.inl"
	-@del /f/q "ReceptaclesS.inl"
	-@del /f/q "ReceptaclesC.h"
	-@del /f/q "ReceptaclesS.h"
	-@del /f/q "ReceptaclesC.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\Receptacles_stub\$(NULL)" mkdir "Release\Receptacles_stub"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\..\..\..\.." /I "..\..\..\..\..\.." /I "..\..\..\..\.." /I "..\..\..\..\..\ciao" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D RECEPTACLES_STUB_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_IFR_Client.lib TAO_Valuetype.lib TAO_CodecFactory.lib TAO_PI.lib CIAO_Client.lib /libpath:"." /libpath:"..\..\..\..\..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\..\..\..\..\..\lib\Receptacles_stub.dll" /implib:"$(OUTDIR)\Receptacles_stub.lib"
LINK32_OBJS= \
	"$(INTDIR)\ReceptaclesC.obj"

"..\..\..\..\..\..\..\lib\Receptacles_stub.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\..\..\..\lib\Receptacles_stub.dll.manifest" mt.exe -manifest "..\..\..\..\..\..\..\lib\Receptacles_stub.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\..\..\..\..\..\lib
INTDIR=Static_Debug\Receptacles_stub\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\Receptacles_stubsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\..\..\.." -I"..\..\..\..\..\.." -I"..\..\..\..\.." -I"..\..\..\..\..\ciao" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Receptacles_stub.dep" "ReceptaclesC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\Receptacles_stubsd.lib"
	-@del /f/q "$(OUTDIR)\Receptacles_stubsd.exp"
	-@del /f/q "$(OUTDIR)\Receptacles_stubsd.ilk"
	-@del /f/q "..\..\..\..\..\..\..\lib\Receptacles_stubsd.pdb"
	-@del /f/q "ReceptaclesC.inl"
	-@del /f/q "ReceptaclesS.inl"
	-@del /f/q "ReceptaclesC.h"
	-@del /f/q "ReceptaclesS.h"
	-@del /f/q "ReceptaclesC.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\Receptacles_stub\$(NULL)" mkdir "Static_Debug\Receptacles_stub"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"..\..\..\..\..\..\..\lib\Receptacles_stubsd.pdb" /I "..\..\..\..\..\..\.." /I "..\..\..\..\..\.." /I "..\..\..\..\.." /I "..\..\..\..\..\ciao" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\..\..\..\lib\Receptacles_stubsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\ReceptaclesC.obj"

"$(OUTDIR)\Receptacles_stubsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\Receptacles_stubsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\Receptacles_stubsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\..\..\..\..\..\lib
INTDIR=Static_Release\Receptacles_stub\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\Receptacles_stubs.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\..\..\.." -I"..\..\..\..\..\.." -I"..\..\..\..\.." -I"..\..\..\..\..\ciao" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Receptacles_stub.dep" "ReceptaclesC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\Receptacles_stubs.lib"
	-@del /f/q "$(OUTDIR)\Receptacles_stubs.exp"
	-@del /f/q "$(OUTDIR)\Receptacles_stubs.ilk"
	-@del /f/q "ReceptaclesC.inl"
	-@del /f/q "ReceptaclesS.inl"
	-@del /f/q "ReceptaclesC.h"
	-@del /f/q "ReceptaclesS.h"
	-@del /f/q "ReceptaclesC.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\Receptacles_stub\$(NULL)" mkdir "Static_Release\Receptacles_stub"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\..\..\..\.." /I "..\..\..\..\..\.." /I "..\..\..\..\.." /I "..\..\..\..\..\ciao" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\..\..\..\lib\Receptacles_stubs.lib"
LINK32_OBJS= \
	"$(INTDIR)\ReceptaclesC.obj"

"$(OUTDIR)\Receptacles_stubs.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\Receptacles_stubs.lib.manifest" mt.exe -manifest "$(OUTDIR)\Receptacles_stubs.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Receptacles_stub.dep")
!INCLUDE "Makefile.Receptacles_stub.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="ReceptaclesC.cpp"

"$(INTDIR)\ReceptaclesC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ReceptaclesC.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="Receptacles.idl"

InputPath=Receptacles.idl

"ReceptaclesC.inl" "ReceptaclesS.inl" "ReceptaclesC.h" "ReceptaclesS.h" "ReceptaclesC.cpp" : $(SOURCE)  "..\..\..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Receptacles_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\..\lib
	..\..\..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\..\..\.. -I..\..\..\..\.. -I..\..\..\..\../ciao -Wb,stub_export_macro=RECEPTACLES_STUB_Export -Wb,stub_export_include=Receptacles_stub_export.h -Wb,skel_export_macro=RECEPTACLES_SVNT_Export -Wb,skel_export_include=Receptacles_svnt_export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="Receptacles.idl"

InputPath=Receptacles.idl

"ReceptaclesC.inl" "ReceptaclesS.inl" "ReceptaclesC.h" "ReceptaclesS.h" "ReceptaclesC.cpp" : $(SOURCE)  "..\..\..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Receptacles_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\..\lib
	..\..\..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\..\..\.. -I..\..\..\..\.. -I..\..\..\..\../ciao -Wb,stub_export_macro=RECEPTACLES_STUB_Export -Wb,stub_export_include=Receptacles_stub_export.h -Wb,skel_export_macro=RECEPTACLES_SVNT_Export -Wb,skel_export_include=Receptacles_svnt_export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="Receptacles.idl"

InputPath=Receptacles.idl

"ReceptaclesC.inl" "ReceptaclesS.inl" "ReceptaclesC.h" "ReceptaclesS.h" "ReceptaclesC.cpp" : $(SOURCE)  "..\..\..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Receptacles_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\..\lib
	..\..\..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\..\..\.. -I..\..\..\..\.. -I..\..\..\..\../ciao -Wb,stub_export_macro=RECEPTACLES_STUB_Export -Wb,stub_export_include=Receptacles_stub_export.h -Wb,skel_export_macro=RECEPTACLES_SVNT_Export -Wb,skel_export_include=Receptacles_svnt_export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="Receptacles.idl"

InputPath=Receptacles.idl

"ReceptaclesC.inl" "ReceptaclesS.inl" "ReceptaclesC.h" "ReceptaclesS.h" "ReceptaclesC.cpp" : $(SOURCE)  "..\..\..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Receptacles_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\..\lib
	..\..\..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\..\..\.. -I..\..\..\..\.. -I..\..\..\..\../ciao -Wb,stub_export_macro=RECEPTACLES_STUB_Export -Wb,stub_export_include=Receptacles_stub_export.h -Wb,skel_export_macro=RECEPTACLES_SVNT_Export -Wb,skel_export_include=Receptacles_svnt_export.h "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Receptacles_stub.dep")
	@echo Using "Makefile.Receptacles_stub.dep"
!ELSE
	@echo Warning: cannot find "Makefile.Receptacles_stub.dep"
!ENDIF
!ENDIF

