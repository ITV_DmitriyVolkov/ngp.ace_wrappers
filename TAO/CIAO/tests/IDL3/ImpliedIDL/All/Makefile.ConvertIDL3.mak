# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.ConvertIDL3.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "convert_IDL2.idl" "include_IDL2.idl" "keyword_clash_IDL2.idl" "pass_through_IDL2.idl" "convert_IDL2C.inl" "convert_IDL2S.inl" "convert_IDL2C.h" "convert_IDL2S.h" "convert_IDL2C.cpp" "convert_IDL2S.cpp" "include_IDL2C.inl" "include_IDL2S.inl" "include_IDL2C.h" "include_IDL2S.h" "include_IDL2C.cpp" "include_IDL2S.cpp" "keyword_clash_IDL2C.inl" "keyword_clash_IDL2S.inl" "keyword_clash_IDL2C.h" "keyword_clash_IDL2S.h" "keyword_clash_IDL2C.cpp" "keyword_clash_IDL2S.cpp" "pass_through_IDL2C.inl" "pass_through_IDL2S.inl" "pass_through_IDL2C.h" "pass_through_IDL2S.h" "pass_through_IDL2C.cpp" "pass_through_IDL2S.cpp" "raw_includeC.inl" "raw_includeS.inl" "raw_includeC.h" "raw_includeS.h" "raw_includeC.cpp" "raw_includeS.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\..\..\..\..\lib
INTDIR=Debug\ConvertIDL3\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\..\..\lib\ConvertIDL3d.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\..\.." -I"..\..\..\..\.." -I"..\..\..\.." -I"..\..\..\..\ciao" -I"..\..\..\..\..\orbsvcs" -I"..\..\..\..\DAnCE" -I"..\..\..\..\ciaosvcs\Events" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -f "Makefile.ConvertIDL3.dep" "convert_IDL2C.cpp" "convert_IDL2S.cpp" "include_IDL2C.cpp" "include_IDL2S.cpp" "keyword_clash_IDL2C.cpp" "keyword_clash_IDL2S.cpp" "pass_through_IDL2C.cpp" "pass_through_IDL2S.cpp" "raw_includeC.cpp" "raw_includeS.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\ConvertIDL3d.pdb"
	-@del /f/q "..\..\..\..\..\..\lib\ConvertIDL3d.dll"
	-@del /f/q "$(OUTDIR)\ConvertIDL3d.lib"
	-@del /f/q "$(OUTDIR)\ConvertIDL3d.exp"
	-@del /f/q "$(OUTDIR)\ConvertIDL3d.ilk"
	-@del /f/q "convert_IDL2.idl"
	-@del /f/q "include_IDL2.idl"
	-@del /f/q "keyword_clash_IDL2.idl"
	-@del /f/q "pass_through_IDL2.idl"
	-@del /f/q "convert_IDL2C.inl"
	-@del /f/q "convert_IDL2S.inl"
	-@del /f/q "convert_IDL2C.h"
	-@del /f/q "convert_IDL2S.h"
	-@del /f/q "convert_IDL2C.cpp"
	-@del /f/q "convert_IDL2S.cpp"
	-@del /f/q "include_IDL2C.inl"
	-@del /f/q "include_IDL2S.inl"
	-@del /f/q "include_IDL2C.h"
	-@del /f/q "include_IDL2S.h"
	-@del /f/q "include_IDL2C.cpp"
	-@del /f/q "include_IDL2S.cpp"
	-@del /f/q "keyword_clash_IDL2C.inl"
	-@del /f/q "keyword_clash_IDL2S.inl"
	-@del /f/q "keyword_clash_IDL2C.h"
	-@del /f/q "keyword_clash_IDL2S.h"
	-@del /f/q "keyword_clash_IDL2C.cpp"
	-@del /f/q "keyword_clash_IDL2S.cpp"
	-@del /f/q "pass_through_IDL2C.inl"
	-@del /f/q "pass_through_IDL2S.inl"
	-@del /f/q "pass_through_IDL2C.h"
	-@del /f/q "pass_through_IDL2S.h"
	-@del /f/q "pass_through_IDL2C.cpp"
	-@del /f/q "pass_through_IDL2S.cpp"
	-@del /f/q "raw_includeC.inl"
	-@del /f/q "raw_includeS.inl"
	-@del /f/q "raw_includeC.h"
	-@del /f/q "raw_includeS.h"
	-@del /f/q "raw_includeC.cpp"
	-@del /f/q "raw_includeS.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\ConvertIDL3\$(NULL)" mkdir "Debug\ConvertIDL3"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"$(INTDIR)/" /I "..\..\..\..\..\.." /I "..\..\..\..\.." /I "..\..\..\.." /I "..\..\..\..\ciao" /I "..\..\..\..\..\orbsvcs" /I "..\..\..\..\DAnCE" /I "..\..\..\..\ciaosvcs\Events" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_PortableServerd.lib TAO_IFR_Clientd.lib TAO_Valuetyped.lib TAO_CodecFactoryd.lib TAO_PId.lib CIAO_Clientd.lib CIAO_Containerd.lib CIAO_Events_Based.lib TAO_Messagingd.lib CIAO_Deployment_stubd.lib TAO_Svc_Utilsd.lib TAO_RTEventd.lib TAO_RTEvent_Skeld.lib TAO_RTEvent_Servd.lib TAO_CosNamingd.lib CIAO_RTEventd.lib CIAO_Eventsd.lib CIAO_Deployment_svntd.lib TAO_Utilsd.lib CIAO_Serverd.lib /libpath:"." /libpath:"..\..\..\..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\..\..\..\..\lib\ConvertIDL3d.pdb" /machine:IA64 /out:"..\..\..\..\..\..\lib\ConvertIDL3d.dll" /implib:"$(OUTDIR)\ConvertIDL3d.lib"
LINK32_OBJS= \
	"$(INTDIR)\convert_IDL2C.obj" \
	"$(INTDIR)\convert_IDL2S.obj" \
	"$(INTDIR)\include_IDL2C.obj" \
	"$(INTDIR)\include_IDL2S.obj" \
	"$(INTDIR)\keyword_clash_IDL2C.obj" \
	"$(INTDIR)\keyword_clash_IDL2S.obj" \
	"$(INTDIR)\pass_through_IDL2C.obj" \
	"$(INTDIR)\pass_through_IDL2S.obj" \
	"$(INTDIR)\raw_includeC.obj" \
	"$(INTDIR)\raw_includeS.obj"

"..\..\..\..\..\..\lib\ConvertIDL3d.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\..\..\lib\ConvertIDL3d.dll.manifest" mt.exe -manifest "..\..\..\..\..\..\lib\ConvertIDL3d.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\..\..\..\..\lib
INTDIR=Release\ConvertIDL3\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\..\..\lib\ConvertIDL3.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\..\.." -I"..\..\..\..\.." -I"..\..\..\.." -I"..\..\..\..\ciao" -I"..\..\..\..\..\orbsvcs" -I"..\..\..\..\DAnCE" -I"..\..\..\..\ciaosvcs\Events" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -f "Makefile.ConvertIDL3.dep" "convert_IDL2C.cpp" "convert_IDL2S.cpp" "include_IDL2C.cpp" "include_IDL2S.cpp" "keyword_clash_IDL2C.cpp" "keyword_clash_IDL2S.cpp" "pass_through_IDL2C.cpp" "pass_through_IDL2S.cpp" "raw_includeC.cpp" "raw_includeS.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\..\..\..\..\lib\ConvertIDL3.dll"
	-@del /f/q "$(OUTDIR)\ConvertIDL3.lib"
	-@del /f/q "$(OUTDIR)\ConvertIDL3.exp"
	-@del /f/q "$(OUTDIR)\ConvertIDL3.ilk"
	-@del /f/q "convert_IDL2.idl"
	-@del /f/q "include_IDL2.idl"
	-@del /f/q "keyword_clash_IDL2.idl"
	-@del /f/q "pass_through_IDL2.idl"
	-@del /f/q "convert_IDL2C.inl"
	-@del /f/q "convert_IDL2S.inl"
	-@del /f/q "convert_IDL2C.h"
	-@del /f/q "convert_IDL2S.h"
	-@del /f/q "convert_IDL2C.cpp"
	-@del /f/q "convert_IDL2S.cpp"
	-@del /f/q "include_IDL2C.inl"
	-@del /f/q "include_IDL2S.inl"
	-@del /f/q "include_IDL2C.h"
	-@del /f/q "include_IDL2S.h"
	-@del /f/q "include_IDL2C.cpp"
	-@del /f/q "include_IDL2S.cpp"
	-@del /f/q "keyword_clash_IDL2C.inl"
	-@del /f/q "keyword_clash_IDL2S.inl"
	-@del /f/q "keyword_clash_IDL2C.h"
	-@del /f/q "keyword_clash_IDL2S.h"
	-@del /f/q "keyword_clash_IDL2C.cpp"
	-@del /f/q "keyword_clash_IDL2S.cpp"
	-@del /f/q "pass_through_IDL2C.inl"
	-@del /f/q "pass_through_IDL2S.inl"
	-@del /f/q "pass_through_IDL2C.h"
	-@del /f/q "pass_through_IDL2S.h"
	-@del /f/q "pass_through_IDL2C.cpp"
	-@del /f/q "pass_through_IDL2S.cpp"
	-@del /f/q "raw_includeC.inl"
	-@del /f/q "raw_includeS.inl"
	-@del /f/q "raw_includeC.h"
	-@del /f/q "raw_includeS.h"
	-@del /f/q "raw_includeC.cpp"
	-@del /f/q "raw_includeS.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\ConvertIDL3\$(NULL)" mkdir "Release\ConvertIDL3"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\..\..\.." /I "..\..\..\..\.." /I "..\..\..\.." /I "..\..\..\..\ciao" /I "..\..\..\..\..\orbsvcs" /I "..\..\..\..\DAnCE" /I "..\..\..\..\ciaosvcs\Events" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_PortableServer.lib TAO_IFR_Client.lib TAO_Valuetype.lib TAO_CodecFactory.lib TAO_PI.lib CIAO_Client.lib CIAO_Container.lib CIAO_Events_Base.lib TAO_Messaging.lib CIAO_Deployment_stub.lib TAO_Svc_Utils.lib TAO_RTEvent.lib TAO_RTEvent_Skel.lib TAO_RTEvent_Serv.lib TAO_CosNaming.lib CIAO_RTEvent.lib CIAO_Events.lib CIAO_Deployment_svnt.lib TAO_Utils.lib CIAO_Server.lib /libpath:"." /libpath:"..\..\..\..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\..\..\..\..\lib\ConvertIDL3.dll" /implib:"$(OUTDIR)\ConvertIDL3.lib"
LINK32_OBJS= \
	"$(INTDIR)\convert_IDL2C.obj" \
	"$(INTDIR)\convert_IDL2S.obj" \
	"$(INTDIR)\include_IDL2C.obj" \
	"$(INTDIR)\include_IDL2S.obj" \
	"$(INTDIR)\keyword_clash_IDL2C.obj" \
	"$(INTDIR)\keyword_clash_IDL2S.obj" \
	"$(INTDIR)\pass_through_IDL2C.obj" \
	"$(INTDIR)\pass_through_IDL2S.obj" \
	"$(INTDIR)\raw_includeC.obj" \
	"$(INTDIR)\raw_includeS.obj"

"..\..\..\..\..\..\lib\ConvertIDL3.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\..\..\lib\ConvertIDL3.dll.manifest" mt.exe -manifest "..\..\..\..\..\..\lib\ConvertIDL3.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\..\..\..\..\lib
INTDIR=Static_Debug\ConvertIDL3\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\ConvertIDL3sd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\..\.." -I"..\..\..\..\.." -I"..\..\..\.." -I"..\..\..\..\ciao" -I"..\..\..\..\..\orbsvcs" -I"..\..\..\..\DAnCE" -I"..\..\..\..\ciaosvcs\Events" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.ConvertIDL3.dep" "convert_IDL2C.cpp" "convert_IDL2S.cpp" "include_IDL2C.cpp" "include_IDL2S.cpp" "keyword_clash_IDL2C.cpp" "keyword_clash_IDL2S.cpp" "pass_through_IDL2C.cpp" "pass_through_IDL2S.cpp" "raw_includeC.cpp" "raw_includeS.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\ConvertIDL3sd.lib"
	-@del /f/q "$(OUTDIR)\ConvertIDL3sd.exp"
	-@del /f/q "$(OUTDIR)\ConvertIDL3sd.ilk"
	-@del /f/q "..\..\..\..\..\..\lib\ConvertIDL3sd.pdb"
	-@del /f/q "convert_IDL2.idl"
	-@del /f/q "include_IDL2.idl"
	-@del /f/q "keyword_clash_IDL2.idl"
	-@del /f/q "pass_through_IDL2.idl"
	-@del /f/q "convert_IDL2C.inl"
	-@del /f/q "convert_IDL2S.inl"
	-@del /f/q "convert_IDL2C.h"
	-@del /f/q "convert_IDL2S.h"
	-@del /f/q "convert_IDL2C.cpp"
	-@del /f/q "convert_IDL2S.cpp"
	-@del /f/q "include_IDL2C.inl"
	-@del /f/q "include_IDL2S.inl"
	-@del /f/q "include_IDL2C.h"
	-@del /f/q "include_IDL2S.h"
	-@del /f/q "include_IDL2C.cpp"
	-@del /f/q "include_IDL2S.cpp"
	-@del /f/q "keyword_clash_IDL2C.inl"
	-@del /f/q "keyword_clash_IDL2S.inl"
	-@del /f/q "keyword_clash_IDL2C.h"
	-@del /f/q "keyword_clash_IDL2S.h"
	-@del /f/q "keyword_clash_IDL2C.cpp"
	-@del /f/q "keyword_clash_IDL2S.cpp"
	-@del /f/q "pass_through_IDL2C.inl"
	-@del /f/q "pass_through_IDL2S.inl"
	-@del /f/q "pass_through_IDL2C.h"
	-@del /f/q "pass_through_IDL2S.h"
	-@del /f/q "pass_through_IDL2C.cpp"
	-@del /f/q "pass_through_IDL2S.cpp"
	-@del /f/q "raw_includeC.inl"
	-@del /f/q "raw_includeS.inl"
	-@del /f/q "raw_includeC.h"
	-@del /f/q "raw_includeS.h"
	-@del /f/q "raw_includeC.cpp"
	-@del /f/q "raw_includeS.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\ConvertIDL3\$(NULL)" mkdir "Static_Debug\ConvertIDL3"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"..\..\..\..\..\..\lib\ConvertIDL3sd.pdb" /I "..\..\..\..\..\.." /I "..\..\..\..\.." /I "..\..\..\.." /I "..\..\..\..\ciao" /I "..\..\..\..\..\orbsvcs" /I "..\..\..\..\DAnCE" /I "..\..\..\..\ciaosvcs\Events" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\..\..\lib\ConvertIDL3sd.lib"
LINK32_OBJS= \
	"$(INTDIR)\convert_IDL2C.obj" \
	"$(INTDIR)\convert_IDL2S.obj" \
	"$(INTDIR)\include_IDL2C.obj" \
	"$(INTDIR)\include_IDL2S.obj" \
	"$(INTDIR)\keyword_clash_IDL2C.obj" \
	"$(INTDIR)\keyword_clash_IDL2S.obj" \
	"$(INTDIR)\pass_through_IDL2C.obj" \
	"$(INTDIR)\pass_through_IDL2S.obj" \
	"$(INTDIR)\raw_includeC.obj" \
	"$(INTDIR)\raw_includeS.obj"

"$(OUTDIR)\ConvertIDL3sd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\ConvertIDL3sd.lib.manifest" mt.exe -manifest "$(OUTDIR)\ConvertIDL3sd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\..\..\..\..\lib
INTDIR=Static_Release\ConvertIDL3\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\ConvertIDL3s.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\..\.." -I"..\..\..\..\.." -I"..\..\..\.." -I"..\..\..\..\ciao" -I"..\..\..\..\..\orbsvcs" -I"..\..\..\..\DAnCE" -I"..\..\..\..\ciaosvcs\Events" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.ConvertIDL3.dep" "convert_IDL2C.cpp" "convert_IDL2S.cpp" "include_IDL2C.cpp" "include_IDL2S.cpp" "keyword_clash_IDL2C.cpp" "keyword_clash_IDL2S.cpp" "pass_through_IDL2C.cpp" "pass_through_IDL2S.cpp" "raw_includeC.cpp" "raw_includeS.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\ConvertIDL3s.lib"
	-@del /f/q "$(OUTDIR)\ConvertIDL3s.exp"
	-@del /f/q "$(OUTDIR)\ConvertIDL3s.ilk"
	-@del /f/q "convert_IDL2.idl"
	-@del /f/q "include_IDL2.idl"
	-@del /f/q "keyword_clash_IDL2.idl"
	-@del /f/q "pass_through_IDL2.idl"
	-@del /f/q "convert_IDL2C.inl"
	-@del /f/q "convert_IDL2S.inl"
	-@del /f/q "convert_IDL2C.h"
	-@del /f/q "convert_IDL2S.h"
	-@del /f/q "convert_IDL2C.cpp"
	-@del /f/q "convert_IDL2S.cpp"
	-@del /f/q "include_IDL2C.inl"
	-@del /f/q "include_IDL2S.inl"
	-@del /f/q "include_IDL2C.h"
	-@del /f/q "include_IDL2S.h"
	-@del /f/q "include_IDL2C.cpp"
	-@del /f/q "include_IDL2S.cpp"
	-@del /f/q "keyword_clash_IDL2C.inl"
	-@del /f/q "keyword_clash_IDL2S.inl"
	-@del /f/q "keyword_clash_IDL2C.h"
	-@del /f/q "keyword_clash_IDL2S.h"
	-@del /f/q "keyword_clash_IDL2C.cpp"
	-@del /f/q "keyword_clash_IDL2S.cpp"
	-@del /f/q "pass_through_IDL2C.inl"
	-@del /f/q "pass_through_IDL2S.inl"
	-@del /f/q "pass_through_IDL2C.h"
	-@del /f/q "pass_through_IDL2S.h"
	-@del /f/q "pass_through_IDL2C.cpp"
	-@del /f/q "pass_through_IDL2S.cpp"
	-@del /f/q "raw_includeC.inl"
	-@del /f/q "raw_includeS.inl"
	-@del /f/q "raw_includeC.h"
	-@del /f/q "raw_includeS.h"
	-@del /f/q "raw_includeC.cpp"
	-@del /f/q "raw_includeS.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\ConvertIDL3\$(NULL)" mkdir "Static_Release\ConvertIDL3"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\..\..\.." /I "..\..\..\..\.." /I "..\..\..\.." /I "..\..\..\..\ciao" /I "..\..\..\..\..\orbsvcs" /I "..\..\..\..\DAnCE" /I "..\..\..\..\ciaosvcs\Events" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\..\..\lib\ConvertIDL3s.lib"
LINK32_OBJS= \
	"$(INTDIR)\convert_IDL2C.obj" \
	"$(INTDIR)\convert_IDL2S.obj" \
	"$(INTDIR)\include_IDL2C.obj" \
	"$(INTDIR)\include_IDL2S.obj" \
	"$(INTDIR)\keyword_clash_IDL2C.obj" \
	"$(INTDIR)\keyword_clash_IDL2S.obj" \
	"$(INTDIR)\pass_through_IDL2C.obj" \
	"$(INTDIR)\pass_through_IDL2S.obj" \
	"$(INTDIR)\raw_includeC.obj" \
	"$(INTDIR)\raw_includeS.obj"

"$(OUTDIR)\ConvertIDL3s.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\ConvertIDL3s.lib.manifest" mt.exe -manifest "$(OUTDIR)\ConvertIDL3s.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.ConvertIDL3.dep")
!INCLUDE "Makefile.ConvertIDL3.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="convert_IDL2C.cpp"

"$(INTDIR)\convert_IDL2C.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\convert_IDL2C.obj" $(SOURCE)

SOURCE="convert_IDL2S.cpp"

"$(INTDIR)\convert_IDL2S.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\convert_IDL2S.obj" $(SOURCE)

SOURCE="include_IDL2C.cpp"

"$(INTDIR)\include_IDL2C.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\include_IDL2C.obj" $(SOURCE)

SOURCE="include_IDL2S.cpp"

"$(INTDIR)\include_IDL2S.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\include_IDL2S.obj" $(SOURCE)

SOURCE="keyword_clash_IDL2C.cpp"

"$(INTDIR)\keyword_clash_IDL2C.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\keyword_clash_IDL2C.obj" $(SOURCE)

SOURCE="keyword_clash_IDL2S.cpp"

"$(INTDIR)\keyword_clash_IDL2S.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\keyword_clash_IDL2S.obj" $(SOURCE)

SOURCE="pass_through_IDL2C.cpp"

"$(INTDIR)\pass_through_IDL2C.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\pass_through_IDL2C.obj" $(SOURCE)

SOURCE="pass_through_IDL2S.cpp"

"$(INTDIR)\pass_through_IDL2S.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\pass_through_IDL2S.obj" $(SOURCE)

SOURCE="raw_includeC.cpp"

"$(INTDIR)\raw_includeC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\raw_includeC.obj" $(SOURCE)

SOURCE="raw_includeS.cpp"

"$(INTDIR)\raw_includeS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\raw_includeS.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="convert.idl"

InputPath=convert.idl

"convert_IDL2.idl" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Debug-idl3toidl2_files-convert_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl3_to_idl2 -I..\..\..\../ciao -I..\..\..\..\.. -I..\..\..\..\../orbsvcs "$(InputPath)"
<<

SOURCE="include.idl"

InputPath=include.idl

"include_IDL2.idl" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Debug-idl3toidl2_files-include_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl3_to_idl2 -I..\..\..\../ciao -I..\..\..\..\.. -I..\..\..\..\../orbsvcs "$(InputPath)"
<<

SOURCE="keyword_clash.idl"

InputPath=keyword_clash.idl

"keyword_clash_IDL2.idl" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Debug-idl3toidl2_files-keyword_clash_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl3_to_idl2 -I..\..\..\../ciao -I..\..\..\..\.. -I..\..\..\..\../orbsvcs "$(InputPath)"
<<

SOURCE="pass_through.idl"

InputPath=pass_through.idl

"pass_through_IDL2.idl" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Debug-idl3toidl2_files-pass_through_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl3_to_idl2 -I..\..\..\../ciao -I..\..\..\..\.. -I..\..\..\..\../orbsvcs -x raw_include.idl "$(InputPath)"
<<

SOURCE="convert_IDL2.idl"

InputPath=convert_IDL2.idl

"convert_IDL2C.inl" "convert_IDL2S.inl" "convert_IDL2C.h" "convert_IDL2S.h" "convert_IDL2C.cpp" "convert_IDL2S.cpp" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-convert_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\..\.. -I..\..\..\.. -I..\..\..\../ciao -I..\..\..\..\../orbsvcs -I..\..\..\../DAnCE -I..\..\..\../ciaosvcs/Events -I..\..\..\../ciaosvcs/Events -I..\..\..\../DAnCE -Sm "$(InputPath)"
<<

SOURCE="include_IDL2.idl"

InputPath=include_IDL2.idl

"include_IDL2C.inl" "include_IDL2S.inl" "include_IDL2C.h" "include_IDL2S.h" "include_IDL2C.cpp" "include_IDL2S.cpp" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-include_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\..\.. -I..\..\..\.. -I..\..\..\../ciao -I..\..\..\..\../orbsvcs -I..\..\..\../DAnCE -I..\..\..\../ciaosvcs/Events -I..\..\..\../ciaosvcs/Events -I..\..\..\../DAnCE -Sm "$(InputPath)"
<<

SOURCE="keyword_clash_IDL2.idl"

InputPath=keyword_clash_IDL2.idl

"keyword_clash_IDL2C.inl" "keyword_clash_IDL2S.inl" "keyword_clash_IDL2C.h" "keyword_clash_IDL2S.h" "keyword_clash_IDL2C.cpp" "keyword_clash_IDL2S.cpp" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-keyword_clash_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\..\.. -I..\..\..\.. -I..\..\..\../ciao -I..\..\..\..\../orbsvcs -I..\..\..\../DAnCE -I..\..\..\../ciaosvcs/Events -I..\..\..\../ciaosvcs/Events -I..\..\..\../DAnCE -Sm "$(InputPath)"
<<

SOURCE="pass_through_IDL2.idl"

InputPath=pass_through_IDL2.idl

"pass_through_IDL2C.inl" "pass_through_IDL2S.inl" "pass_through_IDL2C.h" "pass_through_IDL2S.h" "pass_through_IDL2C.cpp" "pass_through_IDL2S.cpp" : $(SOURCE)  "include_IDL2.idl" "..\..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-pass_through_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\..\.. -I..\..\..\.. -I..\..\..\../ciao -I..\..\..\..\../orbsvcs -I..\..\..\../DAnCE -I..\..\..\../ciaosvcs/Events -I..\..\..\../ciaosvcs/Events -I..\..\..\../DAnCE -Sm "$(InputPath)"
<<

SOURCE="raw_include.idl"

InputPath=raw_include.idl

"raw_includeC.inl" "raw_includeS.inl" "raw_includeC.h" "raw_includeS.h" "raw_includeC.cpp" "raw_includeS.cpp" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-raw_include_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\..\.. -I..\..\..\.. -I..\..\..\../ciao -I..\..\..\..\../orbsvcs -I..\..\..\../DAnCE -I..\..\..\../ciaosvcs/Events -I..\..\..\../ciaosvcs/Events -I..\..\..\../DAnCE -Sm "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="convert.idl"

InputPath=convert.idl

"convert_IDL2.idl" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Release-idl3toidl2_files-convert_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl3_to_idl2 -I..\..\..\../ciao -I..\..\..\..\.. -I..\..\..\..\../orbsvcs "$(InputPath)"
<<

SOURCE="include.idl"

InputPath=include.idl

"include_IDL2.idl" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Release-idl3toidl2_files-include_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl3_to_idl2 -I..\..\..\../ciao -I..\..\..\..\.. -I..\..\..\..\../orbsvcs "$(InputPath)"
<<

SOURCE="keyword_clash.idl"

InputPath=keyword_clash.idl

"keyword_clash_IDL2.idl" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Release-idl3toidl2_files-keyword_clash_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl3_to_idl2 -I..\..\..\../ciao -I..\..\..\..\.. -I..\..\..\..\../orbsvcs "$(InputPath)"
<<

SOURCE="pass_through.idl"

InputPath=pass_through.idl

"pass_through_IDL2.idl" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Release-idl3toidl2_files-pass_through_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl3_to_idl2 -I..\..\..\../ciao -I..\..\..\..\.. -I..\..\..\..\../orbsvcs -x raw_include.idl "$(InputPath)"
<<

SOURCE="convert_IDL2.idl"

InputPath=convert_IDL2.idl

"convert_IDL2C.inl" "convert_IDL2S.inl" "convert_IDL2C.h" "convert_IDL2S.h" "convert_IDL2C.cpp" "convert_IDL2S.cpp" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-convert_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\..\.. -I..\..\..\.. -I..\..\..\../ciao -I..\..\..\..\../orbsvcs -I..\..\..\../DAnCE -I..\..\..\../ciaosvcs/Events -I..\..\..\../ciaosvcs/Events -I..\..\..\../DAnCE -Sm "$(InputPath)"
<<

SOURCE="include_IDL2.idl"

InputPath=include_IDL2.idl

"include_IDL2C.inl" "include_IDL2S.inl" "include_IDL2C.h" "include_IDL2S.h" "include_IDL2C.cpp" "include_IDL2S.cpp" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-include_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\..\.. -I..\..\..\.. -I..\..\..\../ciao -I..\..\..\..\../orbsvcs -I..\..\..\../DAnCE -I..\..\..\../ciaosvcs/Events -I..\..\..\../ciaosvcs/Events -I..\..\..\../DAnCE -Sm "$(InputPath)"
<<

SOURCE="keyword_clash_IDL2.idl"

InputPath=keyword_clash_IDL2.idl

"keyword_clash_IDL2C.inl" "keyword_clash_IDL2S.inl" "keyword_clash_IDL2C.h" "keyword_clash_IDL2S.h" "keyword_clash_IDL2C.cpp" "keyword_clash_IDL2S.cpp" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-keyword_clash_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\..\.. -I..\..\..\.. -I..\..\..\../ciao -I..\..\..\..\../orbsvcs -I..\..\..\../DAnCE -I..\..\..\../ciaosvcs/Events -I..\..\..\../ciaosvcs/Events -I..\..\..\../DAnCE -Sm "$(InputPath)"
<<

SOURCE="pass_through_IDL2.idl"

InputPath=pass_through_IDL2.idl

"pass_through_IDL2C.inl" "pass_through_IDL2S.inl" "pass_through_IDL2C.h" "pass_through_IDL2S.h" "pass_through_IDL2C.cpp" "pass_through_IDL2S.cpp" : $(SOURCE)  "include_IDL2.idl" "..\..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-pass_through_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\..\.. -I..\..\..\.. -I..\..\..\../ciao -I..\..\..\..\../orbsvcs -I..\..\..\../DAnCE -I..\..\..\../ciaosvcs/Events -I..\..\..\../ciaosvcs/Events -I..\..\..\../DAnCE -Sm "$(InputPath)"
<<

SOURCE="raw_include.idl"

InputPath=raw_include.idl

"raw_includeC.inl" "raw_includeS.inl" "raw_includeC.h" "raw_includeS.h" "raw_includeC.cpp" "raw_includeS.cpp" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-raw_include_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\..\.. -I..\..\..\.. -I..\..\..\../ciao -I..\..\..\..\../orbsvcs -I..\..\..\../DAnCE -I..\..\..\../ciaosvcs/Events -I..\..\..\../ciaosvcs/Events -I..\..\..\../DAnCE -Sm "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="convert.idl"

InputPath=convert.idl

"convert_IDL2.idl" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Static_Debug-idl3toidl2_files-convert_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl3_to_idl2 -I..\..\..\../ciao -I..\..\..\..\.. -I..\..\..\..\../orbsvcs "$(InputPath)"
<<

SOURCE="include.idl"

InputPath=include.idl

"include_IDL2.idl" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Static_Debug-idl3toidl2_files-include_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl3_to_idl2 -I..\..\..\../ciao -I..\..\..\..\.. -I..\..\..\..\../orbsvcs "$(InputPath)"
<<

SOURCE="keyword_clash.idl"

InputPath=keyword_clash.idl

"keyword_clash_IDL2.idl" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Static_Debug-idl3toidl2_files-keyword_clash_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl3_to_idl2 -I..\..\..\../ciao -I..\..\..\..\.. -I..\..\..\..\../orbsvcs "$(InputPath)"
<<

SOURCE="pass_through.idl"

InputPath=pass_through.idl

"pass_through_IDL2.idl" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Static_Debug-idl3toidl2_files-pass_through_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl3_to_idl2 -I..\..\..\../ciao -I..\..\..\..\.. -I..\..\..\..\../orbsvcs -x raw_include.idl "$(InputPath)"
<<

SOURCE="convert_IDL2.idl"

InputPath=convert_IDL2.idl

"convert_IDL2C.inl" "convert_IDL2S.inl" "convert_IDL2C.h" "convert_IDL2S.h" "convert_IDL2C.cpp" "convert_IDL2S.cpp" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-convert_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\..\.. -I..\..\..\.. -I..\..\..\../ciao -I..\..\..\..\../orbsvcs -I..\..\..\../DAnCE -I..\..\..\../ciaosvcs/Events -I..\..\..\../ciaosvcs/Events -I..\..\..\../DAnCE -Sm "$(InputPath)"
<<

SOURCE="include_IDL2.idl"

InputPath=include_IDL2.idl

"include_IDL2C.inl" "include_IDL2S.inl" "include_IDL2C.h" "include_IDL2S.h" "include_IDL2C.cpp" "include_IDL2S.cpp" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-include_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\..\.. -I..\..\..\.. -I..\..\..\../ciao -I..\..\..\..\../orbsvcs -I..\..\..\../DAnCE -I..\..\..\../ciaosvcs/Events -I..\..\..\../ciaosvcs/Events -I..\..\..\../DAnCE -Sm "$(InputPath)"
<<

SOURCE="keyword_clash_IDL2.idl"

InputPath=keyword_clash_IDL2.idl

"keyword_clash_IDL2C.inl" "keyword_clash_IDL2S.inl" "keyword_clash_IDL2C.h" "keyword_clash_IDL2S.h" "keyword_clash_IDL2C.cpp" "keyword_clash_IDL2S.cpp" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-keyword_clash_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\..\.. -I..\..\..\.. -I..\..\..\../ciao -I..\..\..\..\../orbsvcs -I..\..\..\../DAnCE -I..\..\..\../ciaosvcs/Events -I..\..\..\../ciaosvcs/Events -I..\..\..\../DAnCE -Sm "$(InputPath)"
<<

SOURCE="pass_through_IDL2.idl"

InputPath=pass_through_IDL2.idl

"pass_through_IDL2C.inl" "pass_through_IDL2S.inl" "pass_through_IDL2C.h" "pass_through_IDL2S.h" "pass_through_IDL2C.cpp" "pass_through_IDL2S.cpp" : $(SOURCE)  "include_IDL2.idl" "..\..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-pass_through_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\..\.. -I..\..\..\.. -I..\..\..\../ciao -I..\..\..\..\../orbsvcs -I..\..\..\../DAnCE -I..\..\..\../ciaosvcs/Events -I..\..\..\../ciaosvcs/Events -I..\..\..\../DAnCE -Sm "$(InputPath)"
<<

SOURCE="raw_include.idl"

InputPath=raw_include.idl

"raw_includeC.inl" "raw_includeS.inl" "raw_includeC.h" "raw_includeS.h" "raw_includeC.cpp" "raw_includeS.cpp" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-raw_include_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\..\.. -I..\..\..\.. -I..\..\..\../ciao -I..\..\..\..\../orbsvcs -I..\..\..\../DAnCE -I..\..\..\../ciaosvcs/Events -I..\..\..\../ciaosvcs/Events -I..\..\..\../DAnCE -Sm "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="convert.idl"

InputPath=convert.idl

"convert_IDL2.idl" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Static_Release-idl3toidl2_files-convert_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl3_to_idl2 -I..\..\..\../ciao -I..\..\..\..\.. -I..\..\..\..\../orbsvcs "$(InputPath)"
<<

SOURCE="include.idl"

InputPath=include.idl

"include_IDL2.idl" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Static_Release-idl3toidl2_files-include_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl3_to_idl2 -I..\..\..\../ciao -I..\..\..\..\.. -I..\..\..\..\../orbsvcs "$(InputPath)"
<<

SOURCE="keyword_clash.idl"

InputPath=keyword_clash.idl

"keyword_clash_IDL2.idl" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Static_Release-idl3toidl2_files-keyword_clash_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl3_to_idl2 -I..\..\..\../ciao -I..\..\..\..\.. -I..\..\..\..\../orbsvcs "$(InputPath)"
<<

SOURCE="pass_through.idl"

InputPath=pass_through.idl

"pass_through_IDL2.idl" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Static_Release-idl3toidl2_files-pass_through_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl3_to_idl2 -I..\..\..\../ciao -I..\..\..\..\.. -I..\..\..\..\../orbsvcs -x raw_include.idl "$(InputPath)"
<<

SOURCE="convert_IDL2.idl"

InputPath=convert_IDL2.idl

"convert_IDL2C.inl" "convert_IDL2S.inl" "convert_IDL2C.h" "convert_IDL2S.h" "convert_IDL2C.cpp" "convert_IDL2S.cpp" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-convert_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\..\.. -I..\..\..\.. -I..\..\..\../ciao -I..\..\..\..\../orbsvcs -I..\..\..\../DAnCE -I..\..\..\../ciaosvcs/Events -I..\..\..\../ciaosvcs/Events -I..\..\..\../DAnCE -Sm "$(InputPath)"
<<

SOURCE="include_IDL2.idl"

InputPath=include_IDL2.idl

"include_IDL2C.inl" "include_IDL2S.inl" "include_IDL2C.h" "include_IDL2S.h" "include_IDL2C.cpp" "include_IDL2S.cpp" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-include_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\..\.. -I..\..\..\.. -I..\..\..\../ciao -I..\..\..\..\../orbsvcs -I..\..\..\../DAnCE -I..\..\..\../ciaosvcs/Events -I..\..\..\../ciaosvcs/Events -I..\..\..\../DAnCE -Sm "$(InputPath)"
<<

SOURCE="keyword_clash_IDL2.idl"

InputPath=keyword_clash_IDL2.idl

"keyword_clash_IDL2C.inl" "keyword_clash_IDL2S.inl" "keyword_clash_IDL2C.h" "keyword_clash_IDL2S.h" "keyword_clash_IDL2C.cpp" "keyword_clash_IDL2S.cpp" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-keyword_clash_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\..\.. -I..\..\..\.. -I..\..\..\../ciao -I..\..\..\..\../orbsvcs -I..\..\..\../DAnCE -I..\..\..\../ciaosvcs/Events -I..\..\..\../ciaosvcs/Events -I..\..\..\../DAnCE -Sm "$(InputPath)"
<<

SOURCE="pass_through_IDL2.idl"

InputPath=pass_through_IDL2.idl

"pass_through_IDL2C.inl" "pass_through_IDL2S.inl" "pass_through_IDL2C.h" "pass_through_IDL2S.h" "pass_through_IDL2C.cpp" "pass_through_IDL2S.cpp" : $(SOURCE)  "include_IDL2.idl" "..\..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-pass_through_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\..\.. -I..\..\..\.. -I..\..\..\../ciao -I..\..\..\..\../orbsvcs -I..\..\..\../DAnCE -I..\..\..\../ciaosvcs/Events -I..\..\..\../ciaosvcs/Events -I..\..\..\../DAnCE -Sm "$(InputPath)"
<<

SOURCE="raw_include.idl"

InputPath=raw_include.idl

"raw_includeC.inl" "raw_includeS.inl" "raw_includeC.h" "raw_includeS.h" "raw_includeC.cpp" "raw_includeS.cpp" : $(SOURCE)  "..\..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-raw_include_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\..\lib
	..\..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\..\.. -I..\..\..\.. -I..\..\..\../ciao -I..\..\..\..\../orbsvcs -I..\..\..\../DAnCE -I..\..\..\../ciaosvcs/Events -I..\..\..\../ciaosvcs/Events -I..\..\..\../DAnCE -Sm "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.ConvertIDL3.dep")
	@echo Using "Makefile.ConvertIDL3.dep"
!ELSE
	@echo Warning: cannot find "Makefile.ConvertIDL3.dep"
!ENDIF
!ENDIF

