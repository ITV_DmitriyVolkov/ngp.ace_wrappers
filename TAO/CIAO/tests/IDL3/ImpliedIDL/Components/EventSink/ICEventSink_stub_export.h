
// -*- C++ -*-
// $Id: ICEventSink_stub_export.h 935 2008-12-10 21:47:27Z mitza $
// Definition for Win32 Export directives.
// This file is generated automatically by generate_export_file.pl ICEVENTSINK_STUB
// ------------------------------
#ifndef ICEVENTSINK_STUB_EXPORT_H
#define ICEVENTSINK_STUB_EXPORT_H

#include "ace/config-all.h"

#if defined (ACE_AS_STATIC_LIBS) && !defined (ICEVENTSINK_STUB_HAS_DLL)
#  define ICEVENTSINK_STUB_HAS_DLL 0
#endif /* ACE_AS_STATIC_LIBS && ICEVENTSINK_STUB_HAS_DLL */

#if !defined (ICEVENTSINK_STUB_HAS_DLL)
#  define ICEVENTSINK_STUB_HAS_DLL 1
#endif /* ! ICEVENTSINK_STUB_HAS_DLL */

#if defined (ICEVENTSINK_STUB_HAS_DLL) && (ICEVENTSINK_STUB_HAS_DLL == 1)
#  if defined (ICEVENTSINK_STUB_BUILD_DLL)
#    define ICEVENTSINK_STUB_Export ACE_Proper_Export_Flag
#    define ICEVENTSINK_STUB_SINGLETON_DECLARATION(T) ACE_EXPORT_SINGLETON_DECLARATION (T)
#    define ICEVENTSINK_STUB_SINGLETON_DECLARE(SINGLETON_TYPE, CLASS, LOCK) ACE_EXPORT_SINGLETON_DECLARE(SINGLETON_TYPE, CLASS, LOCK)
#  else /* ICEVENTSINK_STUB_BUILD_DLL */
#    define ICEVENTSINK_STUB_Export ACE_Proper_Import_Flag
#    define ICEVENTSINK_STUB_SINGLETON_DECLARATION(T) ACE_IMPORT_SINGLETON_DECLARATION (T)
#    define ICEVENTSINK_STUB_SINGLETON_DECLARE(SINGLETON_TYPE, CLASS, LOCK) ACE_IMPORT_SINGLETON_DECLARE(SINGLETON_TYPE, CLASS, LOCK)
#  endif /* ICEVENTSINK_STUB_BUILD_DLL */
#else /* ICEVENTSINK_STUB_HAS_DLL == 1 */
#  define ICEVENTSINK_STUB_Export
#  define ICEVENTSINK_STUB_SINGLETON_DECLARATION(T)
#  define ICEVENTSINK_STUB_SINGLETON_DECLARE(SINGLETON_TYPE, CLASS, LOCK)
#endif /* ICEVENTSINK_STUB_HAS_DLL == 1 */

// Set ICEVENTSINK_STUB_NTRACE = 0 to turn on library specific tracing even if
// tracing is turned off for ACE.
#if !defined (ICEVENTSINK_STUB_NTRACE)
#  if (ACE_NTRACE == 1)
#    define ICEVENTSINK_STUB_NTRACE 1
#  else /* (ACE_NTRACE == 1) */
#    define ICEVENTSINK_STUB_NTRACE 0
#  endif /* (ACE_NTRACE == 1) */
#endif /* !ICEVENTSINK_STUB_NTRACE */

#if (ICEVENTSINK_STUB_NTRACE == 1)
#  define ICEVENTSINK_STUB_TRACE(X)
#else /* (ICEVENTSINK_STUB_NTRACE == 1) */
#  if !defined (ACE_HAS_TRACE)
#    define ACE_HAS_TRACE
#  endif /* ACE_HAS_TRACE */
#  define ICEVENTSINK_STUB_TRACE(X) ACE_TRACE_IMPL(X)
#  include "ace/Trace.h"
#endif /* (ICEVENTSINK_STUB_NTRACE == 1) */

#endif /* ICEVENTSINK_STUB_EXPORT_H */

// End of auto generated file.
