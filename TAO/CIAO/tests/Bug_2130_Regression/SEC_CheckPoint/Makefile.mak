#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake TAO/CIAO -value_template platforms=Win64 -make_coexistence -features qos=1,mfc=1,boost=1,cidl=1,ciao=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: TSEC_CheckPoint_stub TSEC_CheckPoint_controller TSEC_CheckPoint_svnt TSEC_CheckPoint_exec

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.TSEC_CheckPoint_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.TSEC_CheckPoint_stub.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.TSEC_CheckPoint_controller.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.TSEC_CheckPoint_controller.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.TSEC_CheckPoint_svnt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.TSEC_CheckPoint_svnt.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.TSEC_CheckPoint_exec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.TSEC_CheckPoint_exec.mak CFG="$(CFG)" $(@)

TSEC_CheckPoint_stub:
	@echo Project: Makefile.TSEC_CheckPoint_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.TSEC_CheckPoint_stub.mak CFG="$(CFG)" all

TSEC_CheckPoint_controller: TSEC_CheckPoint_stub
	@echo Project: Makefile.TSEC_CheckPoint_controller.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.TSEC_CheckPoint_controller.mak CFG="$(CFG)" all

TSEC_CheckPoint_svnt: TSEC_CheckPoint_stub
	@echo Project: Makefile.TSEC_CheckPoint_svnt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.TSEC_CheckPoint_svnt.mak CFG="$(CFG)" all

TSEC_CheckPoint_exec: TSEC_CheckPoint_svnt
	@echo Project: Makefile.TSEC_CheckPoint_exec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.TSEC_CheckPoint_exec.mak CFG="$(CFG)" all

project_name_list:
	@echo TSEC_CheckPoint_controller
	@echo TSEC_CheckPoint_exec
	@echo TSEC_CheckPoint_stub
	@echo TSEC_CheckPoint_svnt
