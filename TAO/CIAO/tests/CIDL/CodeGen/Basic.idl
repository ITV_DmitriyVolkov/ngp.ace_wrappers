// $Id: Basic.idl 14 2007-02-01 15:49:12Z mitza $
//=============================================================================
/**
 * @file Basic.idl
 *
 * Definition of events, and common interfaces used in the BasicSP module.
 *
 * @author Balachandran Natarajan <bala@dre.vanderbilt.edu>
 */
//=============================================================================

#ifndef CIAO_BASIC_IDL
#define CIAO_BASIC_IDL

#include <Components.idl>

// @@NOTE: Do we need a pragma prefix. Anyway its broken now in TAO..

module Extra
{
  exception NoReason {};
  exception NoRhyme {};

  interface Superfluous
  {
    attribute string useless_attr;
    long superfluous_op (in string empty_arg)
      raises (NoReason);
  };
  
  interface Supernumerary
  {
    void supernumerary_op (out string dummy_arg)
      raises (NoRhyme, NoReason);
  };
};

// #pragma prefix ""

module Basic
{
  interface ReadData : Extra::Superfluous 
  {
    string get_data ();
  };
  
  interface AnalyzeData
  {
    void perform_analysis (inout string data);
    attribute boolean fine_tooth_comb;
  };

  eventtype TimeOut {};
  eventtype DataAvailable {};
};

#endif /*CIAO_BASIC_IDL*/
