#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake TAO/CIAO -value_template platforms=Win64 -make_coexistence -features qos=1,mfc=1,boost=1,cidl=1,ciao=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: Minimum_Base_idl_gen Minimum_Base_stub Minimum_Base_skel Minimum_Base_Receiver_cidl_gen Minimum_Base_Receiver_idl_gen Minimum_Base_Receiver_stub Minimum_Base_Receiver_exec Minimum_Base_Receiver_svnt Minimum_Base_Sender_cidl_gen Minimum_Base_Sender_idl_gen Minimum_Base_Sender_stub Minimum_Base_Sender_exec Minimum_Base_Sender_svnt

clean depend generated realclean $(CUSTOM_TARGETS):
	@cd Minimum_Base
	@echo Directory: Minimum_Base
	@echo Project: Makefile.Minimum_Base_idl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_idl_gen.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Minimum_Base
	@echo Directory: Minimum_Base
	@echo Project: Makefile.Minimum_Base_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_stub.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Minimum_Base
	@echo Directory: Minimum_Base
	@echo Project: Makefile.Minimum_Base_skel.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_skel.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Receiver
	@echo Directory: Receiver
	@echo Project: Makefile.Minimum_Base_Receiver_cidl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_Receiver_cidl_gen.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Receiver
	@echo Directory: Receiver
	@echo Project: Makefile.Minimum_Base_Receiver_idl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_Receiver_idl_gen.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Receiver
	@echo Directory: Receiver
	@echo Project: Makefile.Minimum_Base_Receiver_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_Receiver_stub.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Receiver
	@echo Directory: Receiver
	@echo Project: Makefile.Minimum_Base_Receiver_exec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_Receiver_exec.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Receiver
	@echo Directory: Receiver
	@echo Project: Makefile.Minimum_Base_Receiver_svnt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_Receiver_svnt.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Sender
	@echo Directory: Sender
	@echo Project: Makefile.Minimum_Base_Sender_cidl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_Sender_cidl_gen.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Sender
	@echo Directory: Sender
	@echo Project: Makefile.Minimum_Base_Sender_idl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_Sender_idl_gen.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Sender
	@echo Directory: Sender
	@echo Project: Makefile.Minimum_Base_Sender_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_Sender_stub.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Sender
	@echo Directory: Sender
	@echo Project: Makefile.Minimum_Base_Sender_exec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_Sender_exec.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Sender
	@echo Directory: Sender
	@echo Project: Makefile.Minimum_Base_Sender_svnt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_Sender_svnt.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)

Minimum_Base_idl_gen:
	@cd Minimum_Base
	@echo Directory: Minimum_Base
	@echo Project: Makefile.Minimum_Base_idl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_idl_gen.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Minimum_Base_stub: Minimum_Base_idl_gen
	@cd Minimum_Base
	@echo Directory: Minimum_Base
	@echo Project: Makefile.Minimum_Base_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_stub.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Minimum_Base_skel: Minimum_Base_stub
	@cd Minimum_Base
	@echo Directory: Minimum_Base
	@echo Project: Makefile.Minimum_Base_skel.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_skel.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Minimum_Base_Receiver_cidl_gen:
	@cd Receiver
	@echo Directory: Receiver
	@echo Project: Makefile.Minimum_Base_Receiver_cidl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_Receiver_cidl_gen.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Minimum_Base_Receiver_idl_gen:
	@cd Receiver
	@echo Directory: Receiver
	@echo Project: Makefile.Minimum_Base_Receiver_idl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_Receiver_idl_gen.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Minimum_Base_Receiver_stub: Minimum_Base_stub Minimum_Base_Receiver_idl_gen
	@cd Receiver
	@echo Directory: Receiver
	@echo Project: Makefile.Minimum_Base_Receiver_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_Receiver_stub.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Minimum_Base_Receiver_exec: Minimum_Base_Receiver_cidl_gen Minimum_Base_Receiver_stub
	@cd Receiver
	@echo Directory: Receiver
	@echo Project: Makefile.Minimum_Base_Receiver_exec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_Receiver_exec.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Minimum_Base_Receiver_svnt: Minimum_Base_skel Minimum_Base_Receiver_exec
	@cd Receiver
	@echo Directory: Receiver
	@echo Project: Makefile.Minimum_Base_Receiver_svnt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_Receiver_svnt.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Minimum_Base_Sender_cidl_gen:
	@cd Sender
	@echo Directory: Sender
	@echo Project: Makefile.Minimum_Base_Sender_cidl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_Sender_cidl_gen.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Minimum_Base_Sender_idl_gen:
	@cd Sender
	@echo Directory: Sender
	@echo Project: Makefile.Minimum_Base_Sender_idl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_Sender_idl_gen.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Minimum_Base_Sender_stub: Minimum_Base_stub Minimum_Base_Sender_idl_gen
	@cd Sender
	@echo Directory: Sender
	@echo Project: Makefile.Minimum_Base_Sender_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_Sender_stub.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Minimum_Base_Sender_exec: Minimum_Base_Sender_cidl_gen Minimum_Base_Sender_stub
	@cd Sender
	@echo Directory: Sender
	@echo Project: Makefile.Minimum_Base_Sender_exec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_Sender_exec.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Minimum_Base_Sender_svnt: Minimum_Base_skel Minimum_Base_Sender_exec
	@cd Sender
	@echo Directory: Sender
	@echo Project: Makefile.Minimum_Base_Sender_svnt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Minimum_Base_Sender_svnt.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

project_name_list:
	@echo Minimum_Base_idl_gen
	@echo Minimum_Base_skel
	@echo Minimum_Base_stub
	@echo Minimum_Base_Receiver_cidl_gen
	@echo Minimum_Base_Receiver_exec
	@echo Minimum_Base_Receiver_idl_gen
	@echo Minimum_Base_Receiver_stub
	@echo Minimum_Base_Receiver_svnt
	@echo Minimum_Base_Sender_cidl_gen
	@echo Minimum_Base_Sender_exec
	@echo Minimum_Base_Sender_idl_gen
	@echo Minimum_Base_Sender_stub
	@echo Minimum_Base_Sender_svnt
