# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.TAO_IDL3_TO_IDL2_BE.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\..\..\lib
INTDIR=Debug\TAO_IDL3_TO_IDL2_BE\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\lib\TAO_IDL3_TO_IDL2_BEd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\..\TAO_IDL\include" -I"..\..\..\TAO_IDL\fe" -I"..\.." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_IDL3_TO_IDL2_BE_BUILD_DLL -f "Makefile.TAO_IDL3_TO_IDL2_BE.dep" "basic_visitor.cpp" "be_global.cpp" "be_helper.cpp" "be_init.cpp" "be_produce.cpp" "be_sunsoft.cpp" "checking_visitor.cpp" "identifier_helper.cpp" "idl3_to_idl2_visitor.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_IDL3_TO_IDL2_BEd.pdb"
	-@del /f/q "..\..\..\..\lib\TAO_IDL3_TO_IDL2_BEd.dll"
	-@del /f/q "$(OUTDIR)\TAO_IDL3_TO_IDL2_BEd.lib"
	-@del /f/q "$(OUTDIR)\TAO_IDL3_TO_IDL2_BEd.exp"
	-@del /f/q "$(OUTDIR)\TAO_IDL3_TO_IDL2_BEd.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\TAO_IDL3_TO_IDL2_BE\$(NULL)" mkdir "Debug\TAO_IDL3_TO_IDL2_BE"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4355 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\..\TAO_IDL\include" /I "..\..\..\TAO_IDL\fe" /I "..\.." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_IDL3_TO_IDL2_BE_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAO_IDL_FEd.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\..\..\lib\TAO_IDL3_TO_IDL2_BEd.pdb" /machine:IA64 /out:"..\..\..\..\lib\TAO_IDL3_TO_IDL2_BEd.dll" /implib:"$(OUTDIR)\TAO_IDL3_TO_IDL2_BEd.lib"
LINK32_OBJS= \
	"$(INTDIR)\basic_visitor.obj" \
	"$(INTDIR)\be_global.obj" \
	"$(INTDIR)\be_helper.obj" \
	"$(INTDIR)\be_init.obj" \
	"$(INTDIR)\be_produce.obj" \
	"$(INTDIR)\be_sunsoft.obj" \
	"$(INTDIR)\checking_visitor.obj" \
	"$(INTDIR)\identifier_helper.obj" \
	"$(INTDIR)\idl3_to_idl2_visitor.obj"

"..\..\..\..\lib\TAO_IDL3_TO_IDL2_BEd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\lib\TAO_IDL3_TO_IDL2_BEd.dll.manifest" mt.exe -manifest "..\..\..\..\lib\TAO_IDL3_TO_IDL2_BEd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\..\..\lib
INTDIR=Release\TAO_IDL3_TO_IDL2_BE\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\lib\TAO_IDL3_TO_IDL2_BE.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\..\TAO_IDL\include" -I"..\..\..\TAO_IDL\fe" -I"..\.." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_IDL3_TO_IDL2_BE_BUILD_DLL -f "Makefile.TAO_IDL3_TO_IDL2_BE.dep" "basic_visitor.cpp" "be_global.cpp" "be_helper.cpp" "be_init.cpp" "be_produce.cpp" "be_sunsoft.cpp" "checking_visitor.cpp" "identifier_helper.cpp" "idl3_to_idl2_visitor.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\..\..\lib\TAO_IDL3_TO_IDL2_BE.dll"
	-@del /f/q "$(OUTDIR)\TAO_IDL3_TO_IDL2_BE.lib"
	-@del /f/q "$(OUTDIR)\TAO_IDL3_TO_IDL2_BE.exp"
	-@del /f/q "$(OUTDIR)\TAO_IDL3_TO_IDL2_BE.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\TAO_IDL3_TO_IDL2_BE\$(NULL)" mkdir "Release\TAO_IDL3_TO_IDL2_BE"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\..\..\.." /I "..\..\..\TAO_IDL\include" /I "..\..\..\TAO_IDL\fe" /I "..\.." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_IDL3_TO_IDL2_BE_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO_IDL_FE.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\..\..\lib\TAO_IDL3_TO_IDL2_BE.dll" /implib:"$(OUTDIR)\TAO_IDL3_TO_IDL2_BE.lib"
LINK32_OBJS= \
	"$(INTDIR)\basic_visitor.obj" \
	"$(INTDIR)\be_global.obj" \
	"$(INTDIR)\be_helper.obj" \
	"$(INTDIR)\be_init.obj" \
	"$(INTDIR)\be_produce.obj" \
	"$(INTDIR)\be_sunsoft.obj" \
	"$(INTDIR)\checking_visitor.obj" \
	"$(INTDIR)\identifier_helper.obj" \
	"$(INTDIR)\idl3_to_idl2_visitor.obj"

"..\..\..\..\lib\TAO_IDL3_TO_IDL2_BE.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\lib\TAO_IDL3_TO_IDL2_BE.dll.manifest" mt.exe -manifest "..\..\..\..\lib\TAO_IDL3_TO_IDL2_BE.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\..\..\lib
INTDIR=Static_Debug\TAO_IDL3_TO_IDL2_BE\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_IDL3_TO_IDL2_BEsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\..\TAO_IDL\include" -I"..\..\..\TAO_IDL\fe" -I"..\.." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.TAO_IDL3_TO_IDL2_BE.dep" "basic_visitor.cpp" "be_global.cpp" "be_helper.cpp" "be_init.cpp" "be_produce.cpp" "be_sunsoft.cpp" "checking_visitor.cpp" "identifier_helper.cpp" "idl3_to_idl2_visitor.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_IDL3_TO_IDL2_BEsd.lib"
	-@del /f/q "$(OUTDIR)\TAO_IDL3_TO_IDL2_BEsd.exp"
	-@del /f/q "$(OUTDIR)\TAO_IDL3_TO_IDL2_BEsd.ilk"
	-@del /f/q "..\..\..\..\lib\TAO_IDL3_TO_IDL2_BEsd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\TAO_IDL3_TO_IDL2_BE\$(NULL)" mkdir "Static_Debug\TAO_IDL3_TO_IDL2_BE"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4355 /Fd"..\..\..\..\lib\TAO_IDL3_TO_IDL2_BEsd.pdb" /I "..\..\..\.." /I "..\..\..\TAO_IDL\include" /I "..\..\..\TAO_IDL\fe" /I "..\.." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\lib\TAO_IDL3_TO_IDL2_BEsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\basic_visitor.obj" \
	"$(INTDIR)\be_global.obj" \
	"$(INTDIR)\be_helper.obj" \
	"$(INTDIR)\be_init.obj" \
	"$(INTDIR)\be_produce.obj" \
	"$(INTDIR)\be_sunsoft.obj" \
	"$(INTDIR)\checking_visitor.obj" \
	"$(INTDIR)\identifier_helper.obj" \
	"$(INTDIR)\idl3_to_idl2_visitor.obj"

"$(OUTDIR)\TAO_IDL3_TO_IDL2_BEsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_IDL3_TO_IDL2_BEsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_IDL3_TO_IDL2_BEsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\..\..\lib
INTDIR=Static_Release\TAO_IDL3_TO_IDL2_BE\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_IDL3_TO_IDL2_BEs.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\..\TAO_IDL\include" -I"..\..\..\TAO_IDL\fe" -I"..\.." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.TAO_IDL3_TO_IDL2_BE.dep" "basic_visitor.cpp" "be_global.cpp" "be_helper.cpp" "be_init.cpp" "be_produce.cpp" "be_sunsoft.cpp" "checking_visitor.cpp" "identifier_helper.cpp" "idl3_to_idl2_visitor.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_IDL3_TO_IDL2_BEs.lib"
	-@del /f/q "$(OUTDIR)\TAO_IDL3_TO_IDL2_BEs.exp"
	-@del /f/q "$(OUTDIR)\TAO_IDL3_TO_IDL2_BEs.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\TAO_IDL3_TO_IDL2_BE\$(NULL)" mkdir "Static_Release\TAO_IDL3_TO_IDL2_BE"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\..\..\.." /I "..\..\..\TAO_IDL\include" /I "..\..\..\TAO_IDL\fe" /I "..\.." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\lib\TAO_IDL3_TO_IDL2_BEs.lib"
LINK32_OBJS= \
	"$(INTDIR)\basic_visitor.obj" \
	"$(INTDIR)\be_global.obj" \
	"$(INTDIR)\be_helper.obj" \
	"$(INTDIR)\be_init.obj" \
	"$(INTDIR)\be_produce.obj" \
	"$(INTDIR)\be_sunsoft.obj" \
	"$(INTDIR)\checking_visitor.obj" \
	"$(INTDIR)\identifier_helper.obj" \
	"$(INTDIR)\idl3_to_idl2_visitor.obj"

"$(OUTDIR)\TAO_IDL3_TO_IDL2_BEs.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_IDL3_TO_IDL2_BEs.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_IDL3_TO_IDL2_BEs.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.TAO_IDL3_TO_IDL2_BE.dep")
!INCLUDE "Makefile.TAO_IDL3_TO_IDL2_BE.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="basic_visitor.cpp"

"$(INTDIR)\basic_visitor.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\basic_visitor.obj" $(SOURCE)

SOURCE="be_global.cpp"

"$(INTDIR)\be_global.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be_global.obj" $(SOURCE)

SOURCE="be_helper.cpp"

"$(INTDIR)\be_helper.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be_helper.obj" $(SOURCE)

SOURCE="be_init.cpp"

"$(INTDIR)\be_init.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be_init.obj" $(SOURCE)

SOURCE="be_produce.cpp"

"$(INTDIR)\be_produce.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be_produce.obj" $(SOURCE)

SOURCE="be_sunsoft.cpp"

"$(INTDIR)\be_sunsoft.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\be_sunsoft.obj" $(SOURCE)

SOURCE="checking_visitor.cpp"

"$(INTDIR)\checking_visitor.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\checking_visitor.obj" $(SOURCE)

SOURCE="identifier_helper.cpp"

"$(INTDIR)\identifier_helper.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\identifier_helper.obj" $(SOURCE)

SOURCE="idl3_to_idl2_visitor.cpp"

"$(INTDIR)\idl3_to_idl2_visitor.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\idl3_to_idl2_visitor.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.TAO_IDL3_TO_IDL2_BE.dep")
	@echo Using "Makefile.TAO_IDL3_TO_IDL2_BE.dep"
!ELSE
	@echo Warning: cannot find "Makefile.TAO_IDL3_TO_IDL2_BE.dep"
!ENDIF
!ENDIF

