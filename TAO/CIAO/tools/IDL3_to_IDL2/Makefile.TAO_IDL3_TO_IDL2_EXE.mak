# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.TAO_IDL3_TO_IDL2_EXE.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Console Application")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INSTALLDIR=..\..\..\..\bin
INTDIR=Debug\TAO_IDL3_TO_IDL2_EXE\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" "$(INSTALLDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\tao_idl3_to_idl2.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"." -I"..\..\..\TAO_IDL\include" -I"..\..\..\TAO_IDL\fe" -I"..\..\.." -I"..\.." -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -f "Makefile.TAO_IDL3_TO_IDL2_EXE.dep" "..\..\..\TAO_IDL\driver\drv_preproc.cpp" "..\..\..\TAO_IDL\driver\drv_args.cpp" "..\..\..\TAO_IDL\tao_idl.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\tao_idl3_to_idl2.pdb"
	-@del /f/q "$(INSTALLDIR)\tao_idl3_to_idl2.exe"
	-@del /f/q "$(INSTALLDIR)\tao_idl3_to_idl2.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\TAO_IDL3_TO_IDL2_EXE\$(NULL)" mkdir "Debug\TAO_IDL3_TO_IDL2_EXE"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4355 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "." /I "..\..\..\TAO_IDL\include" /I "..\..\..\TAO_IDL\fe" /I "..\..\.." /I "..\.." /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAO_IDL_FEd.lib TAO_IDL3_TO_IDL2_BEd.lib /libpath:"." /libpath:"..\..\..\..\lib" /libpath:"..\..\..\TAO_IDL" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\tao_idl3_to_idl2.pdb" /machine:IA64 /out:"$(INSTALLDIR)\tao_idl3_to_idl2.exe"
LINK32_OBJS= \
	"$(INTDIR)\dotdot\dotdot\..\TAO_IDL\driver\drv_preproc.obj" \
	"$(INTDIR)\dotdot\dotdot\..\TAO_IDL\driver\drv_args.obj" \
	"$(INTDIR)\dotdot\dotdot\..\TAO_IDL\tao_idl.obj"

"$(INSTALLDIR)\tao_idl3_to_idl2.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\tao_idl3_to_idl2.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\tao_idl3_to_idl2.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=Release
INSTALLDIR=..\..\..\..\bin
INTDIR=Release\TAO_IDL3_TO_IDL2_EXE\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" "$(INSTALLDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\tao_idl3_to_idl2.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"." -I"..\..\..\TAO_IDL\include" -I"..\..\..\TAO_IDL\fe" -I"..\..\.." -I"..\.." -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -f "Makefile.TAO_IDL3_TO_IDL2_EXE.dep" "..\..\..\TAO_IDL\driver\drv_preproc.cpp" "..\..\..\TAO_IDL\driver\drv_args.cpp" "..\..\..\TAO_IDL\tao_idl.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\tao_idl3_to_idl2.exe"
	-@del /f/q "$(INSTALLDIR)\tao_idl3_to_idl2.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\TAO_IDL3_TO_IDL2_EXE\$(NULL)" mkdir "Release\TAO_IDL3_TO_IDL2_EXE"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\..\..\.." /I "." /I "..\..\..\TAO_IDL\include" /I "..\..\..\TAO_IDL\fe" /I "..\..\.." /I "..\.." /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO_IDL_FE.lib TAO_IDL3_TO_IDL2_BE.lib /libpath:"." /libpath:"..\..\..\..\lib" /libpath:"..\..\..\TAO_IDL" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\tao_idl3_to_idl2.exe"
LINK32_OBJS= \
	"$(INTDIR)\dotdot\dotdot\..\TAO_IDL\driver\drv_preproc.obj" \
	"$(INTDIR)\dotdot\dotdot\..\TAO_IDL\driver\drv_args.obj" \
	"$(INTDIR)\dotdot\dotdot\..\TAO_IDL\tao_idl.obj"

"$(INSTALLDIR)\tao_idl3_to_idl2.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\tao_idl3_to_idl2.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\tao_idl3_to_idl2.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=Static_Debug
INSTALLDIR=..\..\..\..\bin
INTDIR=Static_Debug\TAO_IDL3_TO_IDL2_EXE\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" "$(INSTALLDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\tao_idl3_to_idl2.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"." -I"..\..\..\TAO_IDL\include" -I"..\..\..\TAO_IDL\fe" -I"..\..\.." -I"..\.." -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.TAO_IDL3_TO_IDL2_EXE.dep" "..\..\..\TAO_IDL\driver\drv_preproc.cpp" "..\..\..\TAO_IDL\driver\drv_args.cpp" "..\..\..\TAO_IDL\tao_idl.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\tao_idl3_to_idl2.pdb"
	-@del /f/q "$(INSTALLDIR)\tao_idl3_to_idl2.exe"
	-@del /f/q "$(INSTALLDIR)\tao_idl3_to_idl2.ilk"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\TAO_IDL3_TO_IDL2_EXE\$(NULL)" mkdir "Static_Debug\TAO_IDL3_TO_IDL2_EXE"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4355 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "." /I "..\..\..\TAO_IDL\include" /I "..\..\..\TAO_IDL\fe" /I "..\..\.." /I "..\.." /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEsd.lib TAO_IDL_FEsd.lib TAO_IDL3_TO_IDL2_BEsd.lib /libpath:"." /libpath:"..\..\..\..\lib" /libpath:"..\..\..\TAO_IDL" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\tao_idl3_to_idl2.pdb" /machine:IA64 /out:"$(INSTALLDIR)\tao_idl3_to_idl2.exe"
LINK32_OBJS= \
	"$(INTDIR)\dotdot\dotdot\..\TAO_IDL\driver\drv_preproc.obj" \
	"$(INTDIR)\dotdot\dotdot\..\TAO_IDL\driver\drv_args.obj" \
	"$(INTDIR)\dotdot\dotdot\..\TAO_IDL\tao_idl.obj"

"$(INSTALLDIR)\tao_idl3_to_idl2.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\tao_idl3_to_idl2.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\tao_idl3_to_idl2.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=Static_Release
INSTALLDIR=..\..\..\..\bin
INTDIR=Static_Release\TAO_IDL3_TO_IDL2_EXE\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" "$(INSTALLDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\tao_idl3_to_idl2.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"." -I"..\..\..\TAO_IDL\include" -I"..\..\..\TAO_IDL\fe" -I"..\..\.." -I"..\.." -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.TAO_IDL3_TO_IDL2_EXE.dep" "..\..\..\TAO_IDL\driver\drv_preproc.cpp" "..\..\..\TAO_IDL\driver\drv_args.cpp" "..\..\..\TAO_IDL\tao_idl.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\tao_idl3_to_idl2.exe"
	-@del /f/q "$(INSTALLDIR)\tao_idl3_to_idl2.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\TAO_IDL3_TO_IDL2_EXE\$(NULL)" mkdir "Static_Release\TAO_IDL3_TO_IDL2_EXE"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\..\..\.." /I "." /I "..\..\..\TAO_IDL\include" /I "..\..\..\TAO_IDL\fe" /I "..\..\.." /I "..\.." /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEs.lib TAO_IDL_FEs.lib TAO_IDL3_TO_IDL2_BEs.lib /libpath:"." /libpath:"..\..\..\..\lib" /libpath:"..\..\..\TAO_IDL" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\tao_idl3_to_idl2.exe"
LINK32_OBJS= \
	"$(INTDIR)\dotdot\dotdot\..\TAO_IDL\driver\drv_preproc.obj" \
	"$(INTDIR)\dotdot\dotdot\..\TAO_IDL\driver\drv_args.obj" \
	"$(INTDIR)\dotdot\dotdot\..\TAO_IDL\tao_idl.obj"

"$(INSTALLDIR)\tao_idl3_to_idl2.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\tao_idl3_to_idl2.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\tao_idl3_to_idl2.exe.manifest" -outputresource:$@;1

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(INSTALLDIR)" :
	if not exist "$(INSTALLDIR)\$(NULL)" mkdir "$(INSTALLDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.TAO_IDL3_TO_IDL2_EXE.dep")
!INCLUDE "Makefile.TAO_IDL3_TO_IDL2_EXE.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="..\..\..\TAO_IDL\driver\drv_preproc.cpp"

"$(INTDIR)\dotdot\dotdot\..\TAO_IDL\driver\drv_preproc.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\dotdot\dotdot\..\TAO_IDL\driver\$(NULL)" mkdir "$(INTDIR)\dotdot\dotdot\..\TAO_IDL\driver\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\dotdot\dotdot\..\TAO_IDL\driver\drv_preproc.obj" $(SOURCE)

SOURCE="..\..\..\TAO_IDL\driver\drv_args.cpp"

"$(INTDIR)\dotdot\dotdot\..\TAO_IDL\driver\drv_args.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\dotdot\dotdot\..\TAO_IDL\driver\$(NULL)" mkdir "$(INTDIR)\dotdot\dotdot\..\TAO_IDL\driver\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\dotdot\dotdot\..\TAO_IDL\driver\drv_args.obj" $(SOURCE)

SOURCE="..\..\..\TAO_IDL\tao_idl.cpp"

"$(INTDIR)\dotdot\dotdot\..\TAO_IDL\tao_idl.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\dotdot\dotdot\..\TAO_IDL\$(NULL)" mkdir "$(INTDIR)\dotdot\dotdot\..\TAO_IDL\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\dotdot\dotdot\..\TAO_IDL\tao_idl.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.TAO_IDL3_TO_IDL2_EXE.dep")
	@echo Using "Makefile.TAO_IDL3_TO_IDL2_EXE.dep"
!ELSE
	@echo Warning: cannot find "Makefile.TAO_IDL3_TO_IDL2_EXE.dep"
!ENDIF
!ENDIF

