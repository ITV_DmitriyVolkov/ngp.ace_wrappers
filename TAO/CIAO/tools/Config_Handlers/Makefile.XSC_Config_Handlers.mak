# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.XSC_Config_Handlers.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\..\..\lib
INTDIR=Debug\XSC_Config_Handlers\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\lib\XSC_Config_Handlersd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\ciaosvcs\Events" -I"..\..\tools" -I"..\..\tools\Config_Handlers" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DXML_USE_PTHREADS -DCONFIG_HANDLERS_BUILD_DLL -f "Makefile.XSC_Config_Handlers.dep" "DP_Handler.cpp" "CCD_Handler.cpp" "ComponentPropertyDescription_Handler.cpp" "MDD_Handler.cpp" "PSPE_Handler.cpp" "CRDD_Handler.cpp" "IDD_Handler.cpp" "ADD_Handler.cpp" "RDD_Handler.cpp" "ID_Handler.cpp" "XML_File_Intf.cpp" "IDREF_Base.cpp" "DnC_Dump.cpp" "Dump_Obj.cpp" "PCD_Handler.cpp" "IRDD_Handler.cpp" "DD_Handler.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlersd.pdb"
	-@del /f/q "..\..\..\..\lib\XSC_Config_Handlersd.dll"
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlersd.lib"
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlersd.exp"
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlersd.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\XSC_Config_Handlers\$(NULL)" mkdir "Debug\XSC_Config_Handlers"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\ciaosvcs\Events" /I "..\..\tools" /I "..\..\tools\Config_Handlers" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D XML_USE_PTHREADS /D CONFIG_HANDLERS_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_IFR_Clientd.lib TAO_Valuetyped.lib TAO_CodecFactoryd.lib TAO_PId.lib CIAO_Clientd.lib TAO_PortableServerd.lib CIAO_Containerd.lib CIAO_Events_Based.lib TAO_Messagingd.lib CIAO_Deployment_stubd.lib TAO_Svc_Utilsd.lib TAO_RTEventd.lib TAO_RTEvent_Skeld.lib TAO_RTEvent_Servd.lib TAO_CosNamingd.lib CIAO_RTEventd.lib CIAO_Eventsd.lib CIAO_XML_Utilsd.lib TAO_DynamicAnyd.lib TAO_TypeCodeFactoryd.lib XSC_XML_Handlersd.lib XSC_Config_Handlers_Commond.lib XSC_DynAny_Handlerd.lib RT_CCM_Config_Handlersd.lib CIAO_Events_Handlersd.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\..\..\lib\XSC_Config_Handlersd.pdb" /machine:IA64 /out:"..\..\..\..\lib\XSC_Config_Handlersd.dll" /implib:"$(OUTDIR)\XSC_Config_Handlersd.lib"
LINK32_OBJS= \
	"$(INTDIR)\DP_Handler.obj" \
	"$(INTDIR)\CCD_Handler.obj" \
	"$(INTDIR)\ComponentPropertyDescription_Handler.obj" \
	"$(INTDIR)\MDD_Handler.obj" \
	"$(INTDIR)\PSPE_Handler.obj" \
	"$(INTDIR)\CRDD_Handler.obj" \
	"$(INTDIR)\IDD_Handler.obj" \
	"$(INTDIR)\ADD_Handler.obj" \
	"$(INTDIR)\RDD_Handler.obj" \
	"$(INTDIR)\ID_Handler.obj" \
	"$(INTDIR)\XML_File_Intf.obj" \
	"$(INTDIR)\IDREF_Base.obj" \
	"$(INTDIR)\DnC_Dump.obj" \
	"$(INTDIR)\Dump_Obj.obj" \
	"$(INTDIR)\PCD_Handler.obj" \
	"$(INTDIR)\IRDD_Handler.obj" \
	"$(INTDIR)\DD_Handler.obj"

"..\..\..\..\lib\XSC_Config_Handlersd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\lib\XSC_Config_Handlersd.dll.manifest" mt.exe -manifest "..\..\..\..\lib\XSC_Config_Handlersd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\..\..\lib
INTDIR=Release\XSC_Config_Handlers\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\lib\XSC_Config_Handlers.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\ciaosvcs\Events" -I"..\..\tools" -I"..\..\tools\Config_Handlers" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DXML_USE_PTHREADS -DCONFIG_HANDLERS_BUILD_DLL -f "Makefile.XSC_Config_Handlers.dep" "DP_Handler.cpp" "CCD_Handler.cpp" "ComponentPropertyDescription_Handler.cpp" "MDD_Handler.cpp" "PSPE_Handler.cpp" "CRDD_Handler.cpp" "IDD_Handler.cpp" "ADD_Handler.cpp" "RDD_Handler.cpp" "ID_Handler.cpp" "XML_File_Intf.cpp" "IDREF_Base.cpp" "DnC_Dump.cpp" "Dump_Obj.cpp" "PCD_Handler.cpp" "IRDD_Handler.cpp" "DD_Handler.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\..\..\lib\XSC_Config_Handlers.dll"
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlers.lib"
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlers.exp"
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlers.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\XSC_Config_Handlers\$(NULL)" mkdir "Release\XSC_Config_Handlers"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\ciaosvcs\Events" /I "..\..\tools" /I "..\..\tools\Config_Handlers" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D XML_USE_PTHREADS /D CONFIG_HANDLERS_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_IFR_Client.lib TAO_Valuetype.lib TAO_CodecFactory.lib TAO_PI.lib CIAO_Client.lib TAO_PortableServer.lib CIAO_Container.lib CIAO_Events_Base.lib TAO_Messaging.lib CIAO_Deployment_stub.lib TAO_Svc_Utils.lib TAO_RTEvent.lib TAO_RTEvent_Skel.lib TAO_RTEvent_Serv.lib TAO_CosNaming.lib CIAO_RTEvent.lib CIAO_Events.lib CIAO_XML_Utils.lib TAO_DynamicAny.lib TAO_TypeCodeFactory.lib XSC_XML_Handlers.lib XSC_Config_Handlers_Common.lib XSC_DynAny_Handler.lib RT_CCM_Config_Handlers.lib CIAO_Events_Handlers.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\..\..\lib\XSC_Config_Handlers.dll" /implib:"$(OUTDIR)\XSC_Config_Handlers.lib"
LINK32_OBJS= \
	"$(INTDIR)\DP_Handler.obj" \
	"$(INTDIR)\CCD_Handler.obj" \
	"$(INTDIR)\ComponentPropertyDescription_Handler.obj" \
	"$(INTDIR)\MDD_Handler.obj" \
	"$(INTDIR)\PSPE_Handler.obj" \
	"$(INTDIR)\CRDD_Handler.obj" \
	"$(INTDIR)\IDD_Handler.obj" \
	"$(INTDIR)\ADD_Handler.obj" \
	"$(INTDIR)\RDD_Handler.obj" \
	"$(INTDIR)\ID_Handler.obj" \
	"$(INTDIR)\XML_File_Intf.obj" \
	"$(INTDIR)\IDREF_Base.obj" \
	"$(INTDIR)\DnC_Dump.obj" \
	"$(INTDIR)\Dump_Obj.obj" \
	"$(INTDIR)\PCD_Handler.obj" \
	"$(INTDIR)\IRDD_Handler.obj" \
	"$(INTDIR)\DD_Handler.obj"

"..\..\..\..\lib\XSC_Config_Handlers.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\lib\XSC_Config_Handlers.dll.manifest" mt.exe -manifest "..\..\..\..\lib\XSC_Config_Handlers.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\..\..\lib
INTDIR=Static_Debug\XSC_Config_Handlers\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\XSC_Config_Handlerssd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\ciaosvcs\Events" -I"..\..\tools" -I"..\..\tools\Config_Handlers" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DXML_USE_PTHREADS -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.XSC_Config_Handlers.dep" "DP_Handler.cpp" "CCD_Handler.cpp" "ComponentPropertyDescription_Handler.cpp" "MDD_Handler.cpp" "PSPE_Handler.cpp" "CRDD_Handler.cpp" "IDD_Handler.cpp" "ADD_Handler.cpp" "RDD_Handler.cpp" "ID_Handler.cpp" "XML_File_Intf.cpp" "IDREF_Base.cpp" "DnC_Dump.cpp" "Dump_Obj.cpp" "PCD_Handler.cpp" "IRDD_Handler.cpp" "DD_Handler.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlerssd.lib"
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlerssd.exp"
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlerssd.ilk"
	-@del /f/q "..\..\..\..\lib\XSC_Config_Handlerssd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\XSC_Config_Handlers\$(NULL)" mkdir "Static_Debug\XSC_Config_Handlers"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"..\..\..\..\lib\XSC_Config_Handlerssd.pdb" /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\ciaosvcs\Events" /I "..\..\tools" /I "..\..\tools\Config_Handlers" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D XML_USE_PTHREADS /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\lib\XSC_Config_Handlerssd.lib"
LINK32_OBJS= \
	"$(INTDIR)\DP_Handler.obj" \
	"$(INTDIR)\CCD_Handler.obj" \
	"$(INTDIR)\ComponentPropertyDescription_Handler.obj" \
	"$(INTDIR)\MDD_Handler.obj" \
	"$(INTDIR)\PSPE_Handler.obj" \
	"$(INTDIR)\CRDD_Handler.obj" \
	"$(INTDIR)\IDD_Handler.obj" \
	"$(INTDIR)\ADD_Handler.obj" \
	"$(INTDIR)\RDD_Handler.obj" \
	"$(INTDIR)\ID_Handler.obj" \
	"$(INTDIR)\XML_File_Intf.obj" \
	"$(INTDIR)\IDREF_Base.obj" \
	"$(INTDIR)\DnC_Dump.obj" \
	"$(INTDIR)\Dump_Obj.obj" \
	"$(INTDIR)\PCD_Handler.obj" \
	"$(INTDIR)\IRDD_Handler.obj" \
	"$(INTDIR)\DD_Handler.obj"

"$(OUTDIR)\XSC_Config_Handlerssd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\XSC_Config_Handlerssd.lib.manifest" mt.exe -manifest "$(OUTDIR)\XSC_Config_Handlerssd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\..\..\lib
INTDIR=Static_Release\XSC_Config_Handlers\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\XSC_Config_Handlerss.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\ciaosvcs\Events" -I"..\..\tools" -I"..\..\tools\Config_Handlers" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DXML_USE_PTHREADS -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.XSC_Config_Handlers.dep" "DP_Handler.cpp" "CCD_Handler.cpp" "ComponentPropertyDescription_Handler.cpp" "MDD_Handler.cpp" "PSPE_Handler.cpp" "CRDD_Handler.cpp" "IDD_Handler.cpp" "ADD_Handler.cpp" "RDD_Handler.cpp" "ID_Handler.cpp" "XML_File_Intf.cpp" "IDREF_Base.cpp" "DnC_Dump.cpp" "Dump_Obj.cpp" "PCD_Handler.cpp" "IRDD_Handler.cpp" "DD_Handler.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlerss.lib"
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlerss.exp"
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlerss.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\XSC_Config_Handlers\$(NULL)" mkdir "Static_Release\XSC_Config_Handlers"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\ciaosvcs\Events" /I "..\..\tools" /I "..\..\tools\Config_Handlers" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D XML_USE_PTHREADS /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\lib\XSC_Config_Handlerss.lib"
LINK32_OBJS= \
	"$(INTDIR)\DP_Handler.obj" \
	"$(INTDIR)\CCD_Handler.obj" \
	"$(INTDIR)\ComponentPropertyDescription_Handler.obj" \
	"$(INTDIR)\MDD_Handler.obj" \
	"$(INTDIR)\PSPE_Handler.obj" \
	"$(INTDIR)\CRDD_Handler.obj" \
	"$(INTDIR)\IDD_Handler.obj" \
	"$(INTDIR)\ADD_Handler.obj" \
	"$(INTDIR)\RDD_Handler.obj" \
	"$(INTDIR)\ID_Handler.obj" \
	"$(INTDIR)\XML_File_Intf.obj" \
	"$(INTDIR)\IDREF_Base.obj" \
	"$(INTDIR)\DnC_Dump.obj" \
	"$(INTDIR)\Dump_Obj.obj" \
	"$(INTDIR)\PCD_Handler.obj" \
	"$(INTDIR)\IRDD_Handler.obj" \
	"$(INTDIR)\DD_Handler.obj"

"$(OUTDIR)\XSC_Config_Handlerss.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\XSC_Config_Handlerss.lib.manifest" mt.exe -manifest "$(OUTDIR)\XSC_Config_Handlerss.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.XSC_Config_Handlers.dep")
!INCLUDE "Makefile.XSC_Config_Handlers.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="DP_Handler.cpp"

"$(INTDIR)\DP_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DP_Handler.obj" $(SOURCE)

SOURCE="CCD_Handler.cpp"

"$(INTDIR)\CCD_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCD_Handler.obj" $(SOURCE)

SOURCE="ComponentPropertyDescription_Handler.cpp"

"$(INTDIR)\ComponentPropertyDescription_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ComponentPropertyDescription_Handler.obj" $(SOURCE)

SOURCE="MDD_Handler.cpp"

"$(INTDIR)\MDD_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\MDD_Handler.obj" $(SOURCE)

SOURCE="PSPE_Handler.cpp"

"$(INTDIR)\PSPE_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PSPE_Handler.obj" $(SOURCE)

SOURCE="CRDD_Handler.cpp"

"$(INTDIR)\CRDD_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CRDD_Handler.obj" $(SOURCE)

SOURCE="IDD_Handler.cpp"

"$(INTDIR)\IDD_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IDD_Handler.obj" $(SOURCE)

SOURCE="ADD_Handler.cpp"

"$(INTDIR)\ADD_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ADD_Handler.obj" $(SOURCE)

SOURCE="RDD_Handler.cpp"

"$(INTDIR)\RDD_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RDD_Handler.obj" $(SOURCE)

SOURCE="ID_Handler.cpp"

"$(INTDIR)\ID_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ID_Handler.obj" $(SOURCE)

SOURCE="XML_File_Intf.cpp"

"$(INTDIR)\XML_File_Intf.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\XML_File_Intf.obj" $(SOURCE)

SOURCE="IDREF_Base.cpp"

"$(INTDIR)\IDREF_Base.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IDREF_Base.obj" $(SOURCE)

SOURCE="DnC_Dump.cpp"

"$(INTDIR)\DnC_Dump.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DnC_Dump.obj" $(SOURCE)

SOURCE="Dump_Obj.cpp"

"$(INTDIR)\Dump_Obj.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Dump_Obj.obj" $(SOURCE)

SOURCE="PCD_Handler.cpp"

"$(INTDIR)\PCD_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PCD_Handler.obj" $(SOURCE)

SOURCE="IRDD_Handler.cpp"

"$(INTDIR)\IRDD_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IRDD_Handler.obj" $(SOURCE)

SOURCE="DD_Handler.cpp"

"$(INTDIR)\DD_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DD_Handler.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.XSC_Config_Handlers.dep")
	@echo Using "Makefile.XSC_Config_Handlers.dep"
!ELSE
	@echo Warning: cannot find "Makefile.XSC_Config_Handlers.dep"
!ENDIF
!ENDIF

