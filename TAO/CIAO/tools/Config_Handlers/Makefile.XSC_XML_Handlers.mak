# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.XSC_XML_Handlers.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\..\..\lib
INTDIR=Debug\XSC_XML_Handlers\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\lib\XSC_XML_Handlersd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\tools\Config_Handlers" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DXML_USE_PTHREADS -DXSC_XML_HANDLERS_BUILD_DLL -f "Makefile.XSC_XML_Handlers.dep" "Deployment.cpp" "ccd.cpp" "cdd.cpp" "cdp.cpp" "cid.cpp" "cpd.cpp" "iad.cpp" "pcd.cpp" "toplevel.cpp" "Basic_Deployment_Data.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\XSC_XML_Handlersd.pdb"
	-@del /f/q "..\..\..\..\lib\XSC_XML_Handlersd.dll"
	-@del /f/q "$(OUTDIR)\XSC_XML_Handlersd.lib"
	-@del /f/q "$(OUTDIR)\XSC_XML_Handlersd.exp"
	-@del /f/q "$(OUTDIR)\XSC_XML_Handlersd.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\XSC_XML_Handlers\$(NULL)" mkdir "Debug\XSC_XML_Handlers"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4355 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\tools\Config_Handlers" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D XML_USE_PTHREADS /D XSC_XML_HANDLERS_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\..\..\lib\XSC_XML_Handlersd.pdb" /machine:IA64 /out:"..\..\..\..\lib\XSC_XML_Handlersd.dll" /implib:"$(OUTDIR)\XSC_XML_Handlersd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Deployment.obj" \
	"$(INTDIR)\ccd.obj" \
	"$(INTDIR)\cdd.obj" \
	"$(INTDIR)\cdp.obj" \
	"$(INTDIR)\cid.obj" \
	"$(INTDIR)\cpd.obj" \
	"$(INTDIR)\iad.obj" \
	"$(INTDIR)\pcd.obj" \
	"$(INTDIR)\toplevel.obj" \
	"$(INTDIR)\Basic_Deployment_Data.obj"

"..\..\..\..\lib\XSC_XML_Handlersd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\lib\XSC_XML_Handlersd.dll.manifest" mt.exe -manifest "..\..\..\..\lib\XSC_XML_Handlersd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\..\..\lib
INTDIR=Release\XSC_XML_Handlers\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\lib\XSC_XML_Handlers.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\tools\Config_Handlers" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DXML_USE_PTHREADS -DXSC_XML_HANDLERS_BUILD_DLL -f "Makefile.XSC_XML_Handlers.dep" "Deployment.cpp" "ccd.cpp" "cdd.cpp" "cdp.cpp" "cid.cpp" "cpd.cpp" "iad.cpp" "pcd.cpp" "toplevel.cpp" "Basic_Deployment_Data.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\..\..\lib\XSC_XML_Handlers.dll"
	-@del /f/q "$(OUTDIR)\XSC_XML_Handlers.lib"
	-@del /f/q "$(OUTDIR)\XSC_XML_Handlers.exp"
	-@del /f/q "$(OUTDIR)\XSC_XML_Handlers.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\XSC_XML_Handlers\$(NULL)" mkdir "Release\XSC_XML_Handlers"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\..\..\.." /I "..\..\tools\Config_Handlers" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D XML_USE_PTHREADS /D XSC_XML_HANDLERS_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\..\..\lib\XSC_XML_Handlers.dll" /implib:"$(OUTDIR)\XSC_XML_Handlers.lib"
LINK32_OBJS= \
	"$(INTDIR)\Deployment.obj" \
	"$(INTDIR)\ccd.obj" \
	"$(INTDIR)\cdd.obj" \
	"$(INTDIR)\cdp.obj" \
	"$(INTDIR)\cid.obj" \
	"$(INTDIR)\cpd.obj" \
	"$(INTDIR)\iad.obj" \
	"$(INTDIR)\pcd.obj" \
	"$(INTDIR)\toplevel.obj" \
	"$(INTDIR)\Basic_Deployment_Data.obj"

"..\..\..\..\lib\XSC_XML_Handlers.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\lib\XSC_XML_Handlers.dll.manifest" mt.exe -manifest "..\..\..\..\lib\XSC_XML_Handlers.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\..\..\lib
INTDIR=Static_Debug\XSC_XML_Handlers\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\XSC_XML_Handlerssd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\tools\Config_Handlers" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DXML_USE_PTHREADS -DACE_AS_STATIC_LIBS -f "Makefile.XSC_XML_Handlers.dep" "Deployment.cpp" "ccd.cpp" "cdd.cpp" "cdp.cpp" "cid.cpp" "cpd.cpp" "iad.cpp" "pcd.cpp" "toplevel.cpp" "Basic_Deployment_Data.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\XSC_XML_Handlerssd.lib"
	-@del /f/q "$(OUTDIR)\XSC_XML_Handlerssd.exp"
	-@del /f/q "$(OUTDIR)\XSC_XML_Handlerssd.ilk"
	-@del /f/q "..\..\..\..\lib\XSC_XML_Handlerssd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\XSC_XML_Handlers\$(NULL)" mkdir "Static_Debug\XSC_XML_Handlers"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4355 /Fd"..\..\..\..\lib\XSC_XML_Handlerssd.pdb" /I "..\..\..\.." /I "..\..\tools\Config_Handlers" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D XML_USE_PTHREADS /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\lib\XSC_XML_Handlerssd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Deployment.obj" \
	"$(INTDIR)\ccd.obj" \
	"$(INTDIR)\cdd.obj" \
	"$(INTDIR)\cdp.obj" \
	"$(INTDIR)\cid.obj" \
	"$(INTDIR)\cpd.obj" \
	"$(INTDIR)\iad.obj" \
	"$(INTDIR)\pcd.obj" \
	"$(INTDIR)\toplevel.obj" \
	"$(INTDIR)\Basic_Deployment_Data.obj"

"$(OUTDIR)\XSC_XML_Handlerssd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\XSC_XML_Handlerssd.lib.manifest" mt.exe -manifest "$(OUTDIR)\XSC_XML_Handlerssd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\..\..\lib
INTDIR=Static_Release\XSC_XML_Handlers\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\XSC_XML_Handlerss.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\tools\Config_Handlers" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DXML_USE_PTHREADS -DACE_AS_STATIC_LIBS -f "Makefile.XSC_XML_Handlers.dep" "Deployment.cpp" "ccd.cpp" "cdd.cpp" "cdp.cpp" "cid.cpp" "cpd.cpp" "iad.cpp" "pcd.cpp" "toplevel.cpp" "Basic_Deployment_Data.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\XSC_XML_Handlerss.lib"
	-@del /f/q "$(OUTDIR)\XSC_XML_Handlerss.exp"
	-@del /f/q "$(OUTDIR)\XSC_XML_Handlerss.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\XSC_XML_Handlers\$(NULL)" mkdir "Static_Release\XSC_XML_Handlers"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\..\..\.." /I "..\..\tools\Config_Handlers" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D XML_USE_PTHREADS /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\lib\XSC_XML_Handlerss.lib"
LINK32_OBJS= \
	"$(INTDIR)\Deployment.obj" \
	"$(INTDIR)\ccd.obj" \
	"$(INTDIR)\cdd.obj" \
	"$(INTDIR)\cdp.obj" \
	"$(INTDIR)\cid.obj" \
	"$(INTDIR)\cpd.obj" \
	"$(INTDIR)\iad.obj" \
	"$(INTDIR)\pcd.obj" \
	"$(INTDIR)\toplevel.obj" \
	"$(INTDIR)\Basic_Deployment_Data.obj"

"$(OUTDIR)\XSC_XML_Handlerss.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\XSC_XML_Handlerss.lib.manifest" mt.exe -manifest "$(OUTDIR)\XSC_XML_Handlerss.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.XSC_XML_Handlers.dep")
!INCLUDE "Makefile.XSC_XML_Handlers.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Deployment.cpp"

"$(INTDIR)\Deployment.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment.obj" $(SOURCE)

SOURCE="ccd.cpp"

"$(INTDIR)\ccd.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ccd.obj" $(SOURCE)

SOURCE="cdd.cpp"

"$(INTDIR)\cdd.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\cdd.obj" $(SOURCE)

SOURCE="cdp.cpp"

"$(INTDIR)\cdp.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\cdp.obj" $(SOURCE)

SOURCE="cid.cpp"

"$(INTDIR)\cid.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\cid.obj" $(SOURCE)

SOURCE="cpd.cpp"

"$(INTDIR)\cpd.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\cpd.obj" $(SOURCE)

SOURCE="iad.cpp"

"$(INTDIR)\iad.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\iad.obj" $(SOURCE)

SOURCE="pcd.cpp"

"$(INTDIR)\pcd.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\pcd.obj" $(SOURCE)

SOURCE="toplevel.cpp"

"$(INTDIR)\toplevel.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\toplevel.obj" $(SOURCE)

SOURCE="Basic_Deployment_Data.cpp"

"$(INTDIR)\Basic_Deployment_Data.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Basic_Deployment_Data.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.XSC_XML_Handlers.dep")
	@echo Using "Makefile.XSC_XML_Handlers.dep"
!ELSE
	@echo Warning: cannot find "Makefile.XSC_XML_Handlers.dep"
!ENDIF
!ENDIF

