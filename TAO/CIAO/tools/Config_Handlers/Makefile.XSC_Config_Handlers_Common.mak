# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.XSC_Config_Handlers_Common.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\..\..\lib
INTDIR=Debug\XSC_Config_Handlers_Common\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\lib\XSC_Config_Handlers_Commond.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\tools" -I"..\..\ciaosvcs\Events" -I"..\..\tools\Config_Handlers" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DXML_USE_PTHREADS -DCONFIG_HANDLERS_COMMON_BUILD_DLL -f "Makefile.XSC_Config_Handlers_Common.dep" "ERE_Handler.cpp" "Req_Handler.cpp" "DataType_Handler.cpp" "Property_Handler.cpp" "SatisfierProperty_Handler.cpp" "CPD_Handler.cpp" "CEPE_Handler.cpp" "Any_Handler.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlers_Commond.pdb"
	-@del /f/q "..\..\..\..\lib\XSC_Config_Handlers_Commond.dll"
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlers_Commond.lib"
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlers_Commond.exp"
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlers_Commond.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\XSC_Config_Handlers_Common\$(NULL)" mkdir "Debug\XSC_Config_Handlers_Common"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\tools" /I "..\..\ciaosvcs\Events" /I "..\..\tools\Config_Handlers" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D XML_USE_PTHREADS /D CONFIG_HANDLERS_COMMON_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_IFR_Clientd.lib TAO_Valuetyped.lib TAO_CodecFactoryd.lib TAO_PId.lib CIAO_Clientd.lib TAO_PortableServerd.lib CIAO_Containerd.lib CIAO_Events_Based.lib TAO_Messagingd.lib CIAO_Deployment_stubd.lib CIAO_XML_Utilsd.lib TAO_DynamicAnyd.lib TAO_TypeCodeFactoryd.lib TAO_Svc_Utilsd.lib TAO_RTEventd.lib TAO_RTEvent_Skeld.lib TAO_RTEvent_Servd.lib TAO_CosNamingd.lib CIAO_RTEventd.lib CIAO_Eventsd.lib XSC_XML_Handlersd.lib XSC_DynAny_Handlerd.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\..\..\lib\XSC_Config_Handlers_Commond.pdb" /machine:IA64 /out:"..\..\..\..\lib\XSC_Config_Handlers_Commond.dll" /implib:"$(OUTDIR)\XSC_Config_Handlers_Commond.lib"
LINK32_OBJS= \
	"$(INTDIR)\ERE_Handler.obj" \
	"$(INTDIR)\Req_Handler.obj" \
	"$(INTDIR)\DataType_Handler.obj" \
	"$(INTDIR)\Property_Handler.obj" \
	"$(INTDIR)\SatisfierProperty_Handler.obj" \
	"$(INTDIR)\CPD_Handler.obj" \
	"$(INTDIR)\CEPE_Handler.obj" \
	"$(INTDIR)\Any_Handler.obj"

"..\..\..\..\lib\XSC_Config_Handlers_Commond.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\lib\XSC_Config_Handlers_Commond.dll.manifest" mt.exe -manifest "..\..\..\..\lib\XSC_Config_Handlers_Commond.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\..\..\lib
INTDIR=Release\XSC_Config_Handlers_Common\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\lib\XSC_Config_Handlers_Common.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\tools" -I"..\..\ciaosvcs\Events" -I"..\..\tools\Config_Handlers" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DXML_USE_PTHREADS -DCONFIG_HANDLERS_COMMON_BUILD_DLL -f "Makefile.XSC_Config_Handlers_Common.dep" "ERE_Handler.cpp" "Req_Handler.cpp" "DataType_Handler.cpp" "Property_Handler.cpp" "SatisfierProperty_Handler.cpp" "CPD_Handler.cpp" "CEPE_Handler.cpp" "Any_Handler.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\..\..\lib\XSC_Config_Handlers_Common.dll"
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlers_Common.lib"
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlers_Common.exp"
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlers_Common.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\XSC_Config_Handlers_Common\$(NULL)" mkdir "Release\XSC_Config_Handlers_Common"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\tools" /I "..\..\ciaosvcs\Events" /I "..\..\tools\Config_Handlers" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D XML_USE_PTHREADS /D CONFIG_HANDLERS_COMMON_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_IFR_Client.lib TAO_Valuetype.lib TAO_CodecFactory.lib TAO_PI.lib CIAO_Client.lib TAO_PortableServer.lib CIAO_Container.lib CIAO_Events_Base.lib TAO_Messaging.lib CIAO_Deployment_stub.lib CIAO_XML_Utils.lib TAO_DynamicAny.lib TAO_TypeCodeFactory.lib TAO_Svc_Utils.lib TAO_RTEvent.lib TAO_RTEvent_Skel.lib TAO_RTEvent_Serv.lib TAO_CosNaming.lib CIAO_RTEvent.lib CIAO_Events.lib XSC_XML_Handlers.lib XSC_DynAny_Handler.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\..\..\lib\XSC_Config_Handlers_Common.dll" /implib:"$(OUTDIR)\XSC_Config_Handlers_Common.lib"
LINK32_OBJS= \
	"$(INTDIR)\ERE_Handler.obj" \
	"$(INTDIR)\Req_Handler.obj" \
	"$(INTDIR)\DataType_Handler.obj" \
	"$(INTDIR)\Property_Handler.obj" \
	"$(INTDIR)\SatisfierProperty_Handler.obj" \
	"$(INTDIR)\CPD_Handler.obj" \
	"$(INTDIR)\CEPE_Handler.obj" \
	"$(INTDIR)\Any_Handler.obj"

"..\..\..\..\lib\XSC_Config_Handlers_Common.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\lib\XSC_Config_Handlers_Common.dll.manifest" mt.exe -manifest "..\..\..\..\lib\XSC_Config_Handlers_Common.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\..\..\lib
INTDIR=Static_Debug\XSC_Config_Handlers_Common\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\XSC_Config_Handlers_Commonsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\tools" -I"..\..\ciaosvcs\Events" -I"..\..\tools\Config_Handlers" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DXML_USE_PTHREADS -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.XSC_Config_Handlers_Common.dep" "ERE_Handler.cpp" "Req_Handler.cpp" "DataType_Handler.cpp" "Property_Handler.cpp" "SatisfierProperty_Handler.cpp" "CPD_Handler.cpp" "CEPE_Handler.cpp" "Any_Handler.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlers_Commonsd.lib"
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlers_Commonsd.exp"
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlers_Commonsd.ilk"
	-@del /f/q "..\..\..\..\lib\XSC_Config_Handlers_Commonsd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\XSC_Config_Handlers_Common\$(NULL)" mkdir "Static_Debug\XSC_Config_Handlers_Common"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"..\..\..\..\lib\XSC_Config_Handlers_Commonsd.pdb" /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\tools" /I "..\..\ciaosvcs\Events" /I "..\..\tools\Config_Handlers" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D XML_USE_PTHREADS /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\lib\XSC_Config_Handlers_Commonsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\ERE_Handler.obj" \
	"$(INTDIR)\Req_Handler.obj" \
	"$(INTDIR)\DataType_Handler.obj" \
	"$(INTDIR)\Property_Handler.obj" \
	"$(INTDIR)\SatisfierProperty_Handler.obj" \
	"$(INTDIR)\CPD_Handler.obj" \
	"$(INTDIR)\CEPE_Handler.obj" \
	"$(INTDIR)\Any_Handler.obj"

"$(OUTDIR)\XSC_Config_Handlers_Commonsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\XSC_Config_Handlers_Commonsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\XSC_Config_Handlers_Commonsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\..\..\lib
INTDIR=Static_Release\XSC_Config_Handlers_Common\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\XSC_Config_Handlers_Commons.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\tools" -I"..\..\ciaosvcs\Events" -I"..\..\tools\Config_Handlers" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DXML_USE_PTHREADS -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.XSC_Config_Handlers_Common.dep" "ERE_Handler.cpp" "Req_Handler.cpp" "DataType_Handler.cpp" "Property_Handler.cpp" "SatisfierProperty_Handler.cpp" "CPD_Handler.cpp" "CEPE_Handler.cpp" "Any_Handler.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlers_Commons.lib"
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlers_Commons.exp"
	-@del /f/q "$(OUTDIR)\XSC_Config_Handlers_Commons.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\XSC_Config_Handlers_Common\$(NULL)" mkdir "Static_Release\XSC_Config_Handlers_Common"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\tools" /I "..\..\ciaosvcs\Events" /I "..\..\tools\Config_Handlers" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D XML_USE_PTHREADS /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\lib\XSC_Config_Handlers_Commons.lib"
LINK32_OBJS= \
	"$(INTDIR)\ERE_Handler.obj" \
	"$(INTDIR)\Req_Handler.obj" \
	"$(INTDIR)\DataType_Handler.obj" \
	"$(INTDIR)\Property_Handler.obj" \
	"$(INTDIR)\SatisfierProperty_Handler.obj" \
	"$(INTDIR)\CPD_Handler.obj" \
	"$(INTDIR)\CEPE_Handler.obj" \
	"$(INTDIR)\Any_Handler.obj"

"$(OUTDIR)\XSC_Config_Handlers_Commons.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\XSC_Config_Handlers_Commons.lib.manifest" mt.exe -manifest "$(OUTDIR)\XSC_Config_Handlers_Commons.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.XSC_Config_Handlers_Common.dep")
!INCLUDE "Makefile.XSC_Config_Handlers_Common.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="ERE_Handler.cpp"

"$(INTDIR)\ERE_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ERE_Handler.obj" $(SOURCE)

SOURCE="Req_Handler.cpp"

"$(INTDIR)\Req_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Req_Handler.obj" $(SOURCE)

SOURCE="DataType_Handler.cpp"

"$(INTDIR)\DataType_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DataType_Handler.obj" $(SOURCE)

SOURCE="Property_Handler.cpp"

"$(INTDIR)\Property_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Property_Handler.obj" $(SOURCE)

SOURCE="SatisfierProperty_Handler.cpp"

"$(INTDIR)\SatisfierProperty_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\SatisfierProperty_Handler.obj" $(SOURCE)

SOURCE="CPD_Handler.cpp"

"$(INTDIR)\CPD_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CPD_Handler.obj" $(SOURCE)

SOURCE="CEPE_Handler.cpp"

"$(INTDIR)\CEPE_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CEPE_Handler.obj" $(SOURCE)

SOURCE="Any_Handler.cpp"

"$(INTDIR)\Any_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Any_Handler.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.XSC_Config_Handlers_Common.dep")
	@echo Using "Makefile.XSC_Config_Handlers_Common.dep"
!ELSE
	@echo Warning: cannot find "Makefile.XSC_Config_Handlers_Common.dep"
!ENDIF
!ENDIF

