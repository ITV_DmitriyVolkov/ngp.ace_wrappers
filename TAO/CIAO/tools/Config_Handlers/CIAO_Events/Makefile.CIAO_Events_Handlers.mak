# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.CIAO_Events_Handlers.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\..\..\..\lib
INTDIR=Debug\CIAO_Events_Handlers\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\..\lib\CIAO_Events_Handlersd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\tools" -I"..\..\..\.." -I"..\..\.." -I"..\..\..\ciao" -I"..\..\..\..\orbsvcs" -I"..\..\..\ciaosvcs\Events" -I"..\..\..\tools\Config_Handlers" -I"..\..\..\DAnCE" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DXML_USE_PTHREADS -DCIAO_EVENTS_HANDLERS_BUILD_DLL -f "Makefile.CIAO_Events_Handlers.dep" "CIAOEvents.cpp" "CIAOEvents_Handler.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_Events_Handlersd.pdb"
	-@del /f/q "..\..\..\..\..\lib\CIAO_Events_Handlersd.dll"
	-@del /f/q "$(OUTDIR)\CIAO_Events_Handlersd.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Events_Handlersd.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Events_Handlersd.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\CIAO_Events_Handlers\$(NULL)" mkdir "Debug\CIAO_Events_Handlers"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"$(INTDIR)/" /I "..\..\..\..\.." /I "..\..\..\tools" /I "..\..\..\.." /I "..\..\.." /I "..\..\..\ciao" /I "..\..\..\..\orbsvcs" /I "..\..\..\ciaosvcs\Events" /I "..\..\..\tools\Config_Handlers" /I "..\..\..\DAnCE" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D XML_USE_PTHREADS /D CIAO_EVENTS_HANDLERS_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib CIAO_XML_Utilsd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_Valuetyped.lib TAO_DynamicAnyd.lib TAO_IFR_Clientd.lib TAO_TypeCodeFactoryd.lib TAO_CodecFactoryd.lib TAO_PId.lib TAO_PortableServerd.lib CIAO_Clientd.lib CIAO_Containerd.lib TAO_Messagingd.lib TAO_Svc_Utilsd.lib TAO_RTEventd.lib TAO_RTEvent_Skeld.lib TAO_RTEvent_Servd.lib CIAO_Events_Based.lib TAO_CosNamingd.lib CIAO_RTEventd.lib CIAO_Eventsd.lib XSC_XML_Handlersd.lib CIAO_Deployment_stubd.lib /libpath:"." /libpath:"..\..\..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\..\..\..\lib\CIAO_Events_Handlersd.pdb" /machine:IA64 /out:"..\..\..\..\..\lib\CIAO_Events_Handlersd.dll" /implib:"$(OUTDIR)\CIAO_Events_Handlersd.lib"
LINK32_OBJS= \
	"$(INTDIR)\CIAOEvents.obj" \
	"$(INTDIR)\CIAOEvents_Handler.obj"

"..\..\..\..\..\lib\CIAO_Events_Handlersd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\..\lib\CIAO_Events_Handlersd.dll.manifest" mt.exe -manifest "..\..\..\..\..\lib\CIAO_Events_Handlersd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\..\..\..\lib
INTDIR=Release\CIAO_Events_Handlers\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\..\lib\CIAO_Events_Handlers.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\tools" -I"..\..\..\.." -I"..\..\.." -I"..\..\..\ciao" -I"..\..\..\..\orbsvcs" -I"..\..\..\ciaosvcs\Events" -I"..\..\..\tools\Config_Handlers" -I"..\..\..\DAnCE" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DXML_USE_PTHREADS -DCIAO_EVENTS_HANDLERS_BUILD_DLL -f "Makefile.CIAO_Events_Handlers.dep" "CIAOEvents.cpp" "CIAOEvents_Handler.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\..\..\..\lib\CIAO_Events_Handlers.dll"
	-@del /f/q "$(OUTDIR)\CIAO_Events_Handlers.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Events_Handlers.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Events_Handlers.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\CIAO_Events_Handlers\$(NULL)" mkdir "Release\CIAO_Events_Handlers"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\..\.." /I "..\..\..\tools" /I "..\..\..\.." /I "..\..\.." /I "..\..\..\ciao" /I "..\..\..\..\orbsvcs" /I "..\..\..\ciaosvcs\Events" /I "..\..\..\tools\Config_Handlers" /I "..\..\..\DAnCE" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D XML_USE_PTHREADS /D CIAO_EVENTS_HANDLERS_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib CIAO_XML_Utils.lib TAO.lib TAO_AnyTypeCode.lib TAO_Valuetype.lib TAO_DynamicAny.lib TAO_IFR_Client.lib TAO_TypeCodeFactory.lib TAO_CodecFactory.lib TAO_PI.lib TAO_PortableServer.lib CIAO_Client.lib CIAO_Container.lib TAO_Messaging.lib TAO_Svc_Utils.lib TAO_RTEvent.lib TAO_RTEvent_Skel.lib TAO_RTEvent_Serv.lib CIAO_Events_Base.lib TAO_CosNaming.lib CIAO_RTEvent.lib CIAO_Events.lib XSC_XML_Handlers.lib CIAO_Deployment_stub.lib /libpath:"." /libpath:"..\..\..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\..\..\..\lib\CIAO_Events_Handlers.dll" /implib:"$(OUTDIR)\CIAO_Events_Handlers.lib"
LINK32_OBJS= \
	"$(INTDIR)\CIAOEvents.obj" \
	"$(INTDIR)\CIAOEvents_Handler.obj"

"..\..\..\..\..\lib\CIAO_Events_Handlers.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\..\lib\CIAO_Events_Handlers.dll.manifest" mt.exe -manifest "..\..\..\..\..\lib\CIAO_Events_Handlers.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\..\..\..\lib
INTDIR=Static_Debug\CIAO_Events_Handlers\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CIAO_Events_Handlerssd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\tools" -I"..\..\..\.." -I"..\..\.." -I"..\..\..\ciao" -I"..\..\..\..\orbsvcs" -I"..\..\..\ciaosvcs\Events" -I"..\..\..\tools\Config_Handlers" -I"..\..\..\DAnCE" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DXML_USE_PTHREADS -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CIAO_Events_Handlers.dep" "CIAOEvents.cpp" "CIAOEvents_Handler.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_Events_Handlerssd.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Events_Handlerssd.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Events_Handlerssd.ilk"
	-@del /f/q "..\..\..\..\..\lib\CIAO_Events_Handlerssd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\CIAO_Events_Handlers\$(NULL)" mkdir "Static_Debug\CIAO_Events_Handlers"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"..\..\..\..\..\lib\CIAO_Events_Handlerssd.pdb" /I "..\..\..\..\.." /I "..\..\..\tools" /I "..\..\..\.." /I "..\..\.." /I "..\..\..\ciao" /I "..\..\..\..\orbsvcs" /I "..\..\..\ciaosvcs\Events" /I "..\..\..\tools\Config_Handlers" /I "..\..\..\DAnCE" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D XML_USE_PTHREADS /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\..\lib\CIAO_Events_Handlerssd.lib"
LINK32_OBJS= \
	"$(INTDIR)\CIAOEvents.obj" \
	"$(INTDIR)\CIAOEvents_Handler.obj"

"$(OUTDIR)\CIAO_Events_Handlerssd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CIAO_Events_Handlerssd.lib.manifest" mt.exe -manifest "$(OUTDIR)\CIAO_Events_Handlerssd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\..\..\..\lib
INTDIR=Static_Release\CIAO_Events_Handlers\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CIAO_Events_Handlerss.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\tools" -I"..\..\..\.." -I"..\..\.." -I"..\..\..\ciao" -I"..\..\..\..\orbsvcs" -I"..\..\..\ciaosvcs\Events" -I"..\..\..\tools\Config_Handlers" -I"..\..\..\DAnCE" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DXML_USE_PTHREADS -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CIAO_Events_Handlers.dep" "CIAOEvents.cpp" "CIAOEvents_Handler.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_Events_Handlerss.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Events_Handlerss.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Events_Handlerss.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\CIAO_Events_Handlers\$(NULL)" mkdir "Static_Release\CIAO_Events_Handlers"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\..\.." /I "..\..\..\tools" /I "..\..\..\.." /I "..\..\.." /I "..\..\..\ciao" /I "..\..\..\..\orbsvcs" /I "..\..\..\ciaosvcs\Events" /I "..\..\..\tools\Config_Handlers" /I "..\..\..\DAnCE" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D XML_USE_PTHREADS /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\..\lib\CIAO_Events_Handlerss.lib"
LINK32_OBJS= \
	"$(INTDIR)\CIAOEvents.obj" \
	"$(INTDIR)\CIAOEvents_Handler.obj"

"$(OUTDIR)\CIAO_Events_Handlerss.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CIAO_Events_Handlerss.lib.manifest" mt.exe -manifest "$(OUTDIR)\CIAO_Events_Handlerss.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CIAO_Events_Handlers.dep")
!INCLUDE "Makefile.CIAO_Events_Handlers.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="CIAOEvents.cpp"

"$(INTDIR)\CIAOEvents.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CIAOEvents.obj" $(SOURCE)

SOURCE="CIAOEvents_Handler.cpp"

"$(INTDIR)\CIAOEvents_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CIAOEvents_Handler.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CIAO_Events_Handlers.dep")
	@echo Using "Makefile.CIAO_Events_Handlers.dep"
!ELSE
	@echo Warning: cannot find "Makefile.CIAO_Events_Handlers.dep"
!ENDIF
!ENDIF

