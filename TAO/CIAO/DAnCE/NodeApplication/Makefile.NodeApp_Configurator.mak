# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.NodeApp_Configurator.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\..\..\lib
INTDIR=Debug\NodeApp_Configurator\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\lib\CIAO_NodeApp_Configuratord.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DNODEAPP_CONFIGURATOR_BUILD_DLL -f "Makefile.NodeApp_Configurator.dep" "NodeApp_Configurator.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_NodeApp_Configuratord.pdb"
	-@del /f/q "..\..\..\..\lib\CIAO_NodeApp_Configuratord.dll"
	-@del /f/q "$(OUTDIR)\CIAO_NodeApp_Configuratord.lib"
	-@del /f/q "$(OUTDIR)\CIAO_NodeApp_Configuratord.exp"
	-@del /f/q "$(OUTDIR)\CIAO_NodeApp_Configuratord.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\NodeApp_Configurator\$(NULL)" mkdir "Debug\NodeApp_Configurator"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D NODEAPP_CONFIGURATOR_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_IFR_Clientd.lib TAO_Valuetyped.lib TAO_CodecFactoryd.lib TAO_PId.lib CIAO_Clientd.lib TAO_PortableServerd.lib CIAO_Containerd.lib CIAO_Events_Based.lib TAO_Messagingd.lib CIAO_Deployment_stubd.lib CIAO_Config_Managerd.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\..\..\lib\CIAO_NodeApp_Configuratord.pdb" /machine:IA64 /out:"..\..\..\..\lib\CIAO_NodeApp_Configuratord.dll" /implib:"$(OUTDIR)\CIAO_NodeApp_Configuratord.lib"
LINK32_OBJS= \
	"$(INTDIR)\NodeApp_Configurator.obj"

"..\..\..\..\lib\CIAO_NodeApp_Configuratord.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\lib\CIAO_NodeApp_Configuratord.dll.manifest" mt.exe -manifest "..\..\..\..\lib\CIAO_NodeApp_Configuratord.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\..\..\lib
INTDIR=Release\NodeApp_Configurator\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\lib\CIAO_NodeApp_Configurator.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DNODEAPP_CONFIGURATOR_BUILD_DLL -f "Makefile.NodeApp_Configurator.dep" "NodeApp_Configurator.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\..\..\lib\CIAO_NodeApp_Configurator.dll"
	-@del /f/q "$(OUTDIR)\CIAO_NodeApp_Configurator.lib"
	-@del /f/q "$(OUTDIR)\CIAO_NodeApp_Configurator.exp"
	-@del /f/q "$(OUTDIR)\CIAO_NodeApp_Configurator.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\NodeApp_Configurator\$(NULL)" mkdir "Release\NodeApp_Configurator"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D NODEAPP_CONFIGURATOR_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_IFR_Client.lib TAO_Valuetype.lib TAO_CodecFactory.lib TAO_PI.lib CIAO_Client.lib TAO_PortableServer.lib CIAO_Container.lib CIAO_Events_Base.lib TAO_Messaging.lib CIAO_Deployment_stub.lib CIAO_Config_Manager.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\..\..\lib\CIAO_NodeApp_Configurator.dll" /implib:"$(OUTDIR)\CIAO_NodeApp_Configurator.lib"
LINK32_OBJS= \
	"$(INTDIR)\NodeApp_Configurator.obj"

"..\..\..\..\lib\CIAO_NodeApp_Configurator.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\lib\CIAO_NodeApp_Configurator.dll.manifest" mt.exe -manifest "..\..\..\..\lib\CIAO_NodeApp_Configurator.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\..\..\lib
INTDIR=Static_Debug\NodeApp_Configurator\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CIAO_NodeApp_Configuratorsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.NodeApp_Configurator.dep" "NodeApp_Configurator.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_NodeApp_Configuratorsd.lib"
	-@del /f/q "$(OUTDIR)\CIAO_NodeApp_Configuratorsd.exp"
	-@del /f/q "$(OUTDIR)\CIAO_NodeApp_Configuratorsd.ilk"
	-@del /f/q "..\..\..\..\lib\CIAO_NodeApp_Configuratorsd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\NodeApp_Configurator\$(NULL)" mkdir "Static_Debug\NodeApp_Configurator"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"..\..\..\..\lib\CIAO_NodeApp_Configuratorsd.pdb" /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\lib\CIAO_NodeApp_Configuratorsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\NodeApp_Configurator.obj"

"$(OUTDIR)\CIAO_NodeApp_Configuratorsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CIAO_NodeApp_Configuratorsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\CIAO_NodeApp_Configuratorsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\..\..\lib
INTDIR=Static_Release\NodeApp_Configurator\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CIAO_NodeApp_Configurators.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.NodeApp_Configurator.dep" "NodeApp_Configurator.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_NodeApp_Configurators.lib"
	-@del /f/q "$(OUTDIR)\CIAO_NodeApp_Configurators.exp"
	-@del /f/q "$(OUTDIR)\CIAO_NodeApp_Configurators.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\NodeApp_Configurator\$(NULL)" mkdir "Static_Release\NodeApp_Configurator"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\lib\CIAO_NodeApp_Configurators.lib"
LINK32_OBJS= \
	"$(INTDIR)\NodeApp_Configurator.obj"

"$(OUTDIR)\CIAO_NodeApp_Configurators.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CIAO_NodeApp_Configurators.lib.manifest" mt.exe -manifest "$(OUTDIR)\CIAO_NodeApp_Configurators.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.NodeApp_Configurator.dep")
!INCLUDE "Makefile.NodeApp_Configurator.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="NodeApp_Configurator.cpp"

"$(INTDIR)\NodeApp_Configurator.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\NodeApp_Configurator.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.NodeApp_Configurator.dep")
	@echo Using "Makefile.NodeApp_Configurator.dep"
!ELSE
	@echo Warning: cannot find "Makefile.NodeApp_Configurator.dep"
!ENDIF
!ENDIF

