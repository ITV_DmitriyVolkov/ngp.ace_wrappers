#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake TAO/CIAO -value_template platforms=Win64 -make_coexistence -features qos=1,mfc=1,boost=1,cidl=1,ciao=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: DAnCE_Utils

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.DAnCE_Utils.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.DAnCE_Utils.mak CFG="$(CFG)" $(@)

DAnCE_Utils:
	@echo Project: Makefile.DAnCE_Utils.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.DAnCE_Utils.mak CFG="$(CFG)" all

project_name_list:
	@echo DAnCE_Utils
