# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.CIAO_TargetManager_stub.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "TargetManagerImplC.inl" "TargetManagerImplS.inl" "TargetManagerImplC.h" "TargetManagerImplS.h" "TargetManagerImplC.cpp" "TargetManagerExtC.inl" "TargetManagerExtS.inl" "TargetManagerExtC.h" "TargetManagerExtS.h" "TargetManagerExtC.cpp" "DomainEventsC.inl" "DomainEventsS.inl" "DomainEventsC.h" "DomainEventsS.h" "DomainEventsC.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\..\..\lib
INTDIR=Debug\CIAO_TargetManager_stub\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\lib\TargetManager_stubd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\DAnCE\Interfaces" -I"..\..\DAnCE\NodeManager" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTARGETMANAGER_STUB_BUILD_DLL -f "Makefile.CIAO_TargetManager_stub.dep" "TargetManagerImplC.cpp" "TargetManagerExtC.cpp" "DomainEventsC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TargetManager_stubd.pdb"
	-@del /f/q "..\..\..\..\lib\TargetManager_stubd.dll"
	-@del /f/q "$(OUTDIR)\TargetManager_stubd.lib"
	-@del /f/q "$(OUTDIR)\TargetManager_stubd.exp"
	-@del /f/q "$(OUTDIR)\TargetManager_stubd.ilk"
	-@del /f/q "TargetManagerImplC.inl"
	-@del /f/q "TargetManagerImplS.inl"
	-@del /f/q "TargetManagerImplC.h"
	-@del /f/q "TargetManagerImplS.h"
	-@del /f/q "TargetManagerImplC.cpp"
	-@del /f/q "TargetManagerExtC.inl"
	-@del /f/q "TargetManagerExtS.inl"
	-@del /f/q "TargetManagerExtC.h"
	-@del /f/q "TargetManagerExtS.h"
	-@del /f/q "TargetManagerExtC.cpp"
	-@del /f/q "DomainEventsC.inl"
	-@del /f/q "DomainEventsS.inl"
	-@del /f/q "DomainEventsC.h"
	-@del /f/q "DomainEventsS.h"
	-@del /f/q "DomainEventsC.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\CIAO_TargetManager_stub\$(NULL)" mkdir "Debug\CIAO_TargetManager_stub"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\DAnCE\Interfaces" /I "..\..\DAnCE\NodeManager" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TARGETMANAGER_STUB_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_IFR_Clientd.lib TAO_Valuetyped.lib TAO_CodecFactoryd.lib TAO_PId.lib CIAO_Clientd.lib TAO_PortableServerd.lib CIAO_Containerd.lib CIAO_Events_Based.lib TAO_Messagingd.lib CIAO_Deployment_stubd.lib NodeManager_stubd.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\..\..\lib\TargetManager_stubd.pdb" /machine:IA64 /out:"..\..\..\..\lib\TargetManager_stubd.dll" /implib:"$(OUTDIR)\TargetManager_stubd.lib"
LINK32_OBJS= \
	"$(INTDIR)\TargetManagerImplC.obj" \
	"$(INTDIR)\TargetManagerExtC.obj" \
	"$(INTDIR)\DomainEventsC.obj"

"..\..\..\..\lib\TargetManager_stubd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\lib\TargetManager_stubd.dll.manifest" mt.exe -manifest "..\..\..\..\lib\TargetManager_stubd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\..\..\lib
INTDIR=Release\CIAO_TargetManager_stub\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\lib\TargetManager_stub.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\DAnCE\Interfaces" -I"..\..\DAnCE\NodeManager" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTARGETMANAGER_STUB_BUILD_DLL -f "Makefile.CIAO_TargetManager_stub.dep" "TargetManagerImplC.cpp" "TargetManagerExtC.cpp" "DomainEventsC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\..\..\lib\TargetManager_stub.dll"
	-@del /f/q "$(OUTDIR)\TargetManager_stub.lib"
	-@del /f/q "$(OUTDIR)\TargetManager_stub.exp"
	-@del /f/q "$(OUTDIR)\TargetManager_stub.ilk"
	-@del /f/q "TargetManagerImplC.inl"
	-@del /f/q "TargetManagerImplS.inl"
	-@del /f/q "TargetManagerImplC.h"
	-@del /f/q "TargetManagerImplS.h"
	-@del /f/q "TargetManagerImplC.cpp"
	-@del /f/q "TargetManagerExtC.inl"
	-@del /f/q "TargetManagerExtS.inl"
	-@del /f/q "TargetManagerExtC.h"
	-@del /f/q "TargetManagerExtS.h"
	-@del /f/q "TargetManagerExtC.cpp"
	-@del /f/q "DomainEventsC.inl"
	-@del /f/q "DomainEventsS.inl"
	-@del /f/q "DomainEventsC.h"
	-@del /f/q "DomainEventsS.h"
	-@del /f/q "DomainEventsC.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\CIAO_TargetManager_stub\$(NULL)" mkdir "Release\CIAO_TargetManager_stub"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\DAnCE\Interfaces" /I "..\..\DAnCE\NodeManager" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TARGETMANAGER_STUB_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_IFR_Client.lib TAO_Valuetype.lib TAO_CodecFactory.lib TAO_PI.lib CIAO_Client.lib TAO_PortableServer.lib CIAO_Container.lib CIAO_Events_Base.lib TAO_Messaging.lib CIAO_Deployment_stub.lib NodeManager_stub.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\..\..\lib\TargetManager_stub.dll" /implib:"$(OUTDIR)\TargetManager_stub.lib"
LINK32_OBJS= \
	"$(INTDIR)\TargetManagerImplC.obj" \
	"$(INTDIR)\TargetManagerExtC.obj" \
	"$(INTDIR)\DomainEventsC.obj"

"..\..\..\..\lib\TargetManager_stub.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\lib\TargetManager_stub.dll.manifest" mt.exe -manifest "..\..\..\..\lib\TargetManager_stub.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\..\..\lib
INTDIR=Static_Debug\CIAO_TargetManager_stub\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TargetManager_stubsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\DAnCE\Interfaces" -I"..\..\DAnCE\NodeManager" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CIAO_TargetManager_stub.dep" "TargetManagerImplC.cpp" "TargetManagerExtC.cpp" "DomainEventsC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TargetManager_stubsd.lib"
	-@del /f/q "$(OUTDIR)\TargetManager_stubsd.exp"
	-@del /f/q "$(OUTDIR)\TargetManager_stubsd.ilk"
	-@del /f/q "..\..\..\..\lib\TargetManager_stubsd.pdb"
	-@del /f/q "TargetManagerImplC.inl"
	-@del /f/q "TargetManagerImplS.inl"
	-@del /f/q "TargetManagerImplC.h"
	-@del /f/q "TargetManagerImplS.h"
	-@del /f/q "TargetManagerImplC.cpp"
	-@del /f/q "TargetManagerExtC.inl"
	-@del /f/q "TargetManagerExtS.inl"
	-@del /f/q "TargetManagerExtC.h"
	-@del /f/q "TargetManagerExtS.h"
	-@del /f/q "TargetManagerExtC.cpp"
	-@del /f/q "DomainEventsC.inl"
	-@del /f/q "DomainEventsS.inl"
	-@del /f/q "DomainEventsC.h"
	-@del /f/q "DomainEventsS.h"
	-@del /f/q "DomainEventsC.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\CIAO_TargetManager_stub\$(NULL)" mkdir "Static_Debug\CIAO_TargetManager_stub"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"..\..\..\..\lib\TargetManager_stubsd.pdb" /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\DAnCE\Interfaces" /I "..\..\DAnCE\NodeManager" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\lib\TargetManager_stubsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\TargetManagerImplC.obj" \
	"$(INTDIR)\TargetManagerExtC.obj" \
	"$(INTDIR)\DomainEventsC.obj"

"$(OUTDIR)\TargetManager_stubsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TargetManager_stubsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TargetManager_stubsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\..\..\lib
INTDIR=Static_Release\CIAO_TargetManager_stub\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TargetManager_stubs.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\DAnCE\Interfaces" -I"..\..\DAnCE\NodeManager" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CIAO_TargetManager_stub.dep" "TargetManagerImplC.cpp" "TargetManagerExtC.cpp" "DomainEventsC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TargetManager_stubs.lib"
	-@del /f/q "$(OUTDIR)\TargetManager_stubs.exp"
	-@del /f/q "$(OUTDIR)\TargetManager_stubs.ilk"
	-@del /f/q "TargetManagerImplC.inl"
	-@del /f/q "TargetManagerImplS.inl"
	-@del /f/q "TargetManagerImplC.h"
	-@del /f/q "TargetManagerImplS.h"
	-@del /f/q "TargetManagerImplC.cpp"
	-@del /f/q "TargetManagerExtC.inl"
	-@del /f/q "TargetManagerExtS.inl"
	-@del /f/q "TargetManagerExtC.h"
	-@del /f/q "TargetManagerExtS.h"
	-@del /f/q "TargetManagerExtC.cpp"
	-@del /f/q "DomainEventsC.inl"
	-@del /f/q "DomainEventsS.inl"
	-@del /f/q "DomainEventsC.h"
	-@del /f/q "DomainEventsS.h"
	-@del /f/q "DomainEventsC.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\CIAO_TargetManager_stub\$(NULL)" mkdir "Static_Release\CIAO_TargetManager_stub"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\DAnCE\Interfaces" /I "..\..\DAnCE\NodeManager" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\lib\TargetManager_stubs.lib"
LINK32_OBJS= \
	"$(INTDIR)\TargetManagerImplC.obj" \
	"$(INTDIR)\TargetManagerExtC.obj" \
	"$(INTDIR)\DomainEventsC.obj"

"$(OUTDIR)\TargetManager_stubs.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TargetManager_stubs.lib.manifest" mt.exe -manifest "$(OUTDIR)\TargetManager_stubs.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CIAO_TargetManager_stub.dep")
!INCLUDE "Makefile.CIAO_TargetManager_stub.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="TargetManagerImplC.cpp"

"$(INTDIR)\TargetManagerImplC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TargetManagerImplC.obj" $(SOURCE)

SOURCE="TargetManagerExtC.cpp"

"$(INTDIR)\TargetManagerExtC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TargetManagerExtC.obj" $(SOURCE)

SOURCE="DomainEventsC.cpp"

"$(INTDIR)\DomainEventsC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DomainEventsC.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="TargetManagerImpl.idl"

InputPath=TargetManagerImpl.idl

"TargetManagerImplC.inl" "TargetManagerImplS.inl" "TargetManagerImplC.h" "TargetManagerImplS.h" "TargetManagerImplC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-TargetManagerImpl_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -I..\..\../orbsvcs -I..\../DAnCE -Wb,stub_export_macro=TARGETMANAGER_STUB_Export -Wb,stub_export_include=TargetManager_stub_export.h -Wb,skel_export_macro=TARGETMANAGER_SVNT_Export -Wb,skel_export_include=TargetManager_svnt_export.h "$(InputPath)"
<<

SOURCE="TargetManagerExt.idl"

InputPath=TargetManagerExt.idl

"TargetManagerExtC.inl" "TargetManagerExtS.inl" "TargetManagerExtC.h" "TargetManagerExtS.h" "TargetManagerExtC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-TargetManagerExt_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -I..\..\../orbsvcs -I..\../DAnCE -Wb,stub_export_macro=TARGETMANAGER_STUB_Export -Wb,stub_export_include=TargetManager_stub_export.h -Wb,skel_export_macro=TARGETMANAGER_SVNT_Export -Wb,skel_export_include=TargetManager_svnt_export.h "$(InputPath)"
<<

SOURCE="DomainEvents.idl"

InputPath=DomainEvents.idl

"DomainEventsC.inl" "DomainEventsS.inl" "DomainEventsC.h" "DomainEventsS.h" "DomainEventsC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-DomainEvents_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -I..\..\../orbsvcs -I..\../DAnCE -Wb,stub_export_macro=TARGETMANAGER_STUB_Export -Wb,stub_export_include=TargetManager_stub_export.h -Wb,skel_export_macro=TARGETMANAGER_SVNT_Export -Wb,skel_export_include=TargetManager_svnt_export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="TargetManagerImpl.idl"

InputPath=TargetManagerImpl.idl

"TargetManagerImplC.inl" "TargetManagerImplS.inl" "TargetManagerImplC.h" "TargetManagerImplS.h" "TargetManagerImplC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-TargetManagerImpl_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -I..\..\../orbsvcs -I..\../DAnCE -Wb,stub_export_macro=TARGETMANAGER_STUB_Export -Wb,stub_export_include=TargetManager_stub_export.h -Wb,skel_export_macro=TARGETMANAGER_SVNT_Export -Wb,skel_export_include=TargetManager_svnt_export.h "$(InputPath)"
<<

SOURCE="TargetManagerExt.idl"

InputPath=TargetManagerExt.idl

"TargetManagerExtC.inl" "TargetManagerExtS.inl" "TargetManagerExtC.h" "TargetManagerExtS.h" "TargetManagerExtC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-TargetManagerExt_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -I..\..\../orbsvcs -I..\../DAnCE -Wb,stub_export_macro=TARGETMANAGER_STUB_Export -Wb,stub_export_include=TargetManager_stub_export.h -Wb,skel_export_macro=TARGETMANAGER_SVNT_Export -Wb,skel_export_include=TargetManager_svnt_export.h "$(InputPath)"
<<

SOURCE="DomainEvents.idl"

InputPath=DomainEvents.idl

"DomainEventsC.inl" "DomainEventsS.inl" "DomainEventsC.h" "DomainEventsS.h" "DomainEventsC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-DomainEvents_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -I..\..\../orbsvcs -I..\../DAnCE -Wb,stub_export_macro=TARGETMANAGER_STUB_Export -Wb,stub_export_include=TargetManager_stub_export.h -Wb,skel_export_macro=TARGETMANAGER_SVNT_Export -Wb,skel_export_include=TargetManager_svnt_export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="TargetManagerImpl.idl"

InputPath=TargetManagerImpl.idl

"TargetManagerImplC.inl" "TargetManagerImplS.inl" "TargetManagerImplC.h" "TargetManagerImplS.h" "TargetManagerImplC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-TargetManagerImpl_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -I..\..\../orbsvcs -I..\../DAnCE -Wb,stub_export_macro=TARGETMANAGER_STUB_Export -Wb,stub_export_include=TargetManager_stub_export.h -Wb,skel_export_macro=TARGETMANAGER_SVNT_Export -Wb,skel_export_include=TargetManager_svnt_export.h "$(InputPath)"
<<

SOURCE="TargetManagerExt.idl"

InputPath=TargetManagerExt.idl

"TargetManagerExtC.inl" "TargetManagerExtS.inl" "TargetManagerExtC.h" "TargetManagerExtS.h" "TargetManagerExtC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-TargetManagerExt_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -I..\..\../orbsvcs -I..\../DAnCE -Wb,stub_export_macro=TARGETMANAGER_STUB_Export -Wb,stub_export_include=TargetManager_stub_export.h -Wb,skel_export_macro=TARGETMANAGER_SVNT_Export -Wb,skel_export_include=TargetManager_svnt_export.h "$(InputPath)"
<<

SOURCE="DomainEvents.idl"

InputPath=DomainEvents.idl

"DomainEventsC.inl" "DomainEventsS.inl" "DomainEventsC.h" "DomainEventsS.h" "DomainEventsC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-DomainEvents_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -I..\..\../orbsvcs -I..\../DAnCE -Wb,stub_export_macro=TARGETMANAGER_STUB_Export -Wb,stub_export_include=TargetManager_stub_export.h -Wb,skel_export_macro=TARGETMANAGER_SVNT_Export -Wb,skel_export_include=TargetManager_svnt_export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="TargetManagerImpl.idl"

InputPath=TargetManagerImpl.idl

"TargetManagerImplC.inl" "TargetManagerImplS.inl" "TargetManagerImplC.h" "TargetManagerImplS.h" "TargetManagerImplC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-TargetManagerImpl_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -I..\..\../orbsvcs -I..\../DAnCE -Wb,stub_export_macro=TARGETMANAGER_STUB_Export -Wb,stub_export_include=TargetManager_stub_export.h -Wb,skel_export_macro=TARGETMANAGER_SVNT_Export -Wb,skel_export_include=TargetManager_svnt_export.h "$(InputPath)"
<<

SOURCE="TargetManagerExt.idl"

InputPath=TargetManagerExt.idl

"TargetManagerExtC.inl" "TargetManagerExtS.inl" "TargetManagerExtC.h" "TargetManagerExtS.h" "TargetManagerExtC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-TargetManagerExt_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -I..\..\../orbsvcs -I..\../DAnCE -Wb,stub_export_macro=TARGETMANAGER_STUB_Export -Wb,stub_export_include=TargetManager_stub_export.h -Wb,skel_export_macro=TARGETMANAGER_SVNT_Export -Wb,skel_export_include=TargetManager_svnt_export.h "$(InputPath)"
<<

SOURCE="DomainEvents.idl"

InputPath=DomainEvents.idl

"DomainEventsC.inl" "DomainEventsS.inl" "DomainEventsC.h" "DomainEventsS.h" "DomainEventsC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-DomainEvents_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -I..\..\../orbsvcs -I..\../DAnCE -Wb,stub_export_macro=TARGETMANAGER_STUB_Export -Wb,stub_export_include=TargetManager_stub_export.h -Wb,skel_export_macro=TARGETMANAGER_SVNT_Export -Wb,skel_export_include=TargetManager_svnt_export.h "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CIAO_TargetManager_stub.dep")
	@echo Using "Makefile.CIAO_TargetManager_stub.dep"
!ELSE
	@echo Warning: cannot find "Makefile.CIAO_TargetManager_stub.dep"
!ENDIF
!ENDIF

