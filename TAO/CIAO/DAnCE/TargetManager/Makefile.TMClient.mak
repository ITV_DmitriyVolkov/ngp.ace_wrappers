# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.TMClient.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Console Application")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INSTALLDIR=.
INTDIR=Debug\TMClient\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\CmpClient.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\tools" -I"..\..\ciaosvcs\Events" -I"..\..\tools\Config_Handlers" -I"..\..\DAnCE\Interfaces" -I"..\..\DAnCE\NodeManager" -I"..\..\DAnCE\TargetManager" -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DXML_USE_PTHREADS -f "Makefile.TMClient.dep" "CmpClient.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\CmpClient.pdb"
	-@del /f/q "$(INSTALLDIR)\CmpClient.exe"
	-@del /f/q "$(INSTALLDIR)\CmpClient.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\TMClient\$(NULL)" mkdir "Debug\TMClient"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\tools" /I "..\..\ciaosvcs\Events" /I "..\..\tools\Config_Handlers" /I "..\..\DAnCE\Interfaces" /I "..\..\DAnCE\NodeManager" /I "..\..\DAnCE\TargetManager" /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D XML_USE_PTHREADS /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_IFR_Clientd.lib TAO_Valuetyped.lib TAO_CodecFactoryd.lib TAO_PId.lib CIAO_Clientd.lib TAO_PortableServerd.lib CIAO_Containerd.lib CIAO_Events_Based.lib TAO_Messagingd.lib CIAO_Deployment_stubd.lib CIAO_XML_Utilsd.lib TAO_DynamicAnyd.lib TAO_TypeCodeFactoryd.lib TAO_Svc_Utilsd.lib TAO_RTEventd.lib TAO_RTEvent_Skeld.lib TAO_RTEvent_Servd.lib TAO_CosNamingd.lib CIAO_RTEventd.lib CIAO_Eventsd.lib XSC_XML_Handlersd.lib XSC_DynAny_Handlerd.lib XSC_Config_Handlers_Commond.lib Package_Config_Handlersd.lib CIAO_Events_Handlersd.lib RT_CCM_Config_Handlersd.lib XSC_Config_Handlersd.lib NodeManager_stubd.lib TargetManager_stubd.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\CmpClient.pdb" /machine:IA64 /out:"$(INSTALLDIR)\CmpClient.exe"
LINK32_OBJS= \
	"$(INTDIR)\CmpClient.obj"

"$(INSTALLDIR)\CmpClient.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\CmpClient.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\CmpClient.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=Release
INSTALLDIR=Release
INTDIR=Release\TMClient\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\CmpClient.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\tools" -I"..\..\ciaosvcs\Events" -I"..\..\tools\Config_Handlers" -I"..\..\DAnCE\Interfaces" -I"..\..\DAnCE\NodeManager" -I"..\..\DAnCE\TargetManager" -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DXML_USE_PTHREADS -f "Makefile.TMClient.dep" "CmpClient.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\CmpClient.exe"
	-@del /f/q "$(INSTALLDIR)\CmpClient.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\TMClient\$(NULL)" mkdir "Release\TMClient"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\tools" /I "..\..\ciaosvcs\Events" /I "..\..\tools\Config_Handlers" /I "..\..\DAnCE\Interfaces" /I "..\..\DAnCE\NodeManager" /I "..\..\DAnCE\TargetManager" /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D XML_USE_PTHREADS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_IFR_Client.lib TAO_Valuetype.lib TAO_CodecFactory.lib TAO_PI.lib CIAO_Client.lib TAO_PortableServer.lib CIAO_Container.lib CIAO_Events_Base.lib TAO_Messaging.lib CIAO_Deployment_stub.lib CIAO_XML_Utils.lib TAO_DynamicAny.lib TAO_TypeCodeFactory.lib TAO_Svc_Utils.lib TAO_RTEvent.lib TAO_RTEvent_Skel.lib TAO_RTEvent_Serv.lib TAO_CosNaming.lib CIAO_RTEvent.lib CIAO_Events.lib XSC_XML_Handlers.lib XSC_DynAny_Handler.lib XSC_Config_Handlers_Common.lib Package_Config_Handlers.lib CIAO_Events_Handlers.lib RT_CCM_Config_Handlers.lib XSC_Config_Handlers.lib NodeManager_stub.lib TargetManager_stub.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\CmpClient.exe"
LINK32_OBJS= \
	"$(INTDIR)\CmpClient.obj"

"$(INSTALLDIR)\CmpClient.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\CmpClient.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\CmpClient.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=Static_Debug
INSTALLDIR=Static_Debug
INTDIR=Static_Debug\TMClient\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\CmpClient.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\tools" -I"..\..\ciaosvcs\Events" -I"..\..\tools\Config_Handlers" -I"..\..\DAnCE\Interfaces" -I"..\..\DAnCE\NodeManager" -I"..\..\DAnCE\TargetManager" -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DXML_USE_PTHREADS -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.TMClient.dep" "CmpClient.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\CmpClient.pdb"
	-@del /f/q "$(INSTALLDIR)\CmpClient.exe"
	-@del /f/q "$(INSTALLDIR)\CmpClient.ilk"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\TMClient\$(NULL)" mkdir "Static_Debug\TMClient"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\tools" /I "..\..\ciaosvcs\Events" /I "..\..\tools\Config_Handlers" /I "..\..\DAnCE\Interfaces" /I "..\..\DAnCE\NodeManager" /I "..\..\DAnCE\TargetManager" /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D XML_USE_PTHREADS /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEsd.lib TAOsd.lib TAO_AnyTypeCodesd.lib TAO_IFR_Clientsd.lib TAO_Valuetypesd.lib TAO_CodecFactorysd.lib TAO_PIsd.lib CIAO_Clientsd.lib TAO_PortableServersd.lib CIAO_Containersd.lib CIAO_Events_Basesd.lib TAO_Messagingsd.lib CIAO_Deployment_stubsd.lib CIAO_XML_Utilssd.lib TAO_DynamicAnysd.lib TAO_TypeCodeFactorysd.lib TAO_Svc_Utilssd.lib TAO_RTEventsd.lib TAO_RTEvent_Skelsd.lib TAO_RTEvent_Servsd.lib TAO_CosNamingsd.lib CIAO_RTEventsd.lib CIAO_Eventssd.lib XSC_XML_Handlerssd.lib XSC_DynAny_Handlersd.lib XSC_Config_Handlers_Commonsd.lib Package_Config_Handlerssd.lib CIAO_Events_Handlerssd.lib RT_CCM_Config_Handlerssd.lib XSC_Config_Handlerssd.lib NodeManager_stubsd.lib TargetManager_stubsd.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\CmpClient.pdb" /machine:IA64 /out:"$(INSTALLDIR)\CmpClient.exe"
LINK32_OBJS= \
	"$(INTDIR)\CmpClient.obj"

"$(INSTALLDIR)\CmpClient.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\CmpClient.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\CmpClient.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=Static_Release
INSTALLDIR=Static_Release
INTDIR=Static_Release\TMClient\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\CmpClient.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\tools" -I"..\..\ciaosvcs\Events" -I"..\..\tools\Config_Handlers" -I"..\..\DAnCE\Interfaces" -I"..\..\DAnCE\NodeManager" -I"..\..\DAnCE\TargetManager" -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DXML_USE_PTHREADS -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.TMClient.dep" "CmpClient.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\CmpClient.exe"
	-@del /f/q "$(INSTALLDIR)\CmpClient.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\TMClient\$(NULL)" mkdir "Static_Release\TMClient"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\tools" /I "..\..\ciaosvcs\Events" /I "..\..\tools\Config_Handlers" /I "..\..\DAnCE\Interfaces" /I "..\..\DAnCE\NodeManager" /I "..\..\DAnCE\TargetManager" /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D XML_USE_PTHREADS /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEs.lib TAOs.lib TAO_AnyTypeCodes.lib TAO_IFR_Clients.lib TAO_Valuetypes.lib TAO_CodecFactorys.lib TAO_PIs.lib CIAO_Clients.lib TAO_PortableServers.lib CIAO_Containers.lib CIAO_Events_Bases.lib TAO_Messagings.lib CIAO_Deployment_stubs.lib CIAO_XML_Utilss.lib TAO_DynamicAnys.lib TAO_TypeCodeFactorys.lib TAO_Svc_Utilss.lib TAO_RTEvents.lib TAO_RTEvent_Skels.lib TAO_RTEvent_Servs.lib TAO_CosNamings.lib CIAO_RTEvents.lib CIAO_Eventss.lib XSC_XML_Handlerss.lib XSC_DynAny_Handlers.lib XSC_Config_Handlers_Commons.lib Package_Config_Handlerss.lib CIAO_Events_Handlerss.lib RT_CCM_Config_Handlerss.lib XSC_Config_Handlerss.lib NodeManager_stubs.lib TargetManager_stubs.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\CmpClient.exe"
LINK32_OBJS= \
	"$(INTDIR)\CmpClient.obj"

"$(INSTALLDIR)\CmpClient.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\CmpClient.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\CmpClient.exe.manifest" -outputresource:$@;1

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.TMClient.dep")
!INCLUDE "Makefile.TMClient.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="CmpClient.cpp"

"$(INTDIR)\CmpClient.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CmpClient.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.TMClient.dep")
	@echo Using "Makefile.TMClient.dep"
!ELSE
	@echo Warning: cannot find "Makefile.TMClient.dep"
!ENDIF
!ENDIF

