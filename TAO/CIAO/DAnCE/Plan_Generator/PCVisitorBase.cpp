/* -*- C++ -*- */

//========================================================================
/*
 *  file  PCVisitorBase.cpp
 *
 * $Id: PCVisitorBase.cpp 14 2007-02-01 15:49:12Z mitza $
 *
 *  This file is a dummy which either includes the PCVisitorBase.inl or
 *  is ignored.
 *
 *  author Stoyan Paunov <spaunov@isis.vanderbilt.edu>
 */
//========================================================================

#include "PCVisitorBase.h"

PCVisitorBase::PCVisitorBase (void)
{
}

PCVisitorBase::~PCVisitorBase (void)
{
}

#if !defined (__ACE_INLINE__)
#include "PCVisitorBase.inl"
#endif /* __ACE_INLINE__ */
