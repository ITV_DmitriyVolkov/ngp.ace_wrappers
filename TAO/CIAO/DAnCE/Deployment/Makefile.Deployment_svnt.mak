# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.Deployment_svnt.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\..\..\lib
INTDIR=Debug\Deployment_svnt\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\lib\CIAO_Deployment_svntd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\ciaosvcs\Events" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DDEPLOYMENT_SVNT_BUILD_DLL -f "Makefile.Deployment_svnt.dep" "Deployment_CoreS.cpp" "Deployment_ApplicationS.cpp" "Deployment_NodeApplicationS.cpp" "Deployment_ContainerS.cpp" "DeploymentS.cpp" "Deployment_RepositoryManagerS.cpp" "Deployment_NodeManagerS.cpp" "Deployment_NodeApplicationManagerS.cpp" "Deployment_ApplicationManagerS.cpp" "Deployment_DomainApplicationManagerS.cpp" "Deployment_DomainApplicationS.cpp" "Deployment_ExecutionManagerS.cpp" "NodeApp_CB_Impl.cpp" "Deployment_TargetManagerS.cpp" "CIAO_NodeApplication_CallBackS.cpp" "Deployment_ResourceCommitmentManagerS.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_svntd.pdb"
	-@del /f/q "..\..\..\..\lib\CIAO_Deployment_svntd.dll"
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_svntd.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_svntd.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_svntd.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\Deployment_svnt\$(NULL)" mkdir "Debug\Deployment_svnt"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\ciaosvcs\Events" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D DEPLOYMENT_SVNT_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_PortableServerd.lib TAO_Valuetyped.lib TAO_IFR_Clientd.lib TAO_CodecFactoryd.lib TAO_PId.lib CIAO_Clientd.lib CIAO_Containerd.lib CIAO_Events_Based.lib TAO_Messagingd.lib CIAO_Deployment_stubd.lib TAO_Svc_Utilsd.lib TAO_RTEventd.lib TAO_RTEvent_Skeld.lib TAO_RTEvent_Servd.lib TAO_CosNamingd.lib CIAO_RTEventd.lib CIAO_Eventsd.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\..\..\lib\CIAO_Deployment_svntd.pdb" /machine:IA64 /out:"..\..\..\..\lib\CIAO_Deployment_svntd.dll" /implib:"$(OUTDIR)\CIAO_Deployment_svntd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Deployment_CoreS.obj" \
	"$(INTDIR)\Deployment_ApplicationS.obj" \
	"$(INTDIR)\Deployment_NodeApplicationS.obj" \
	"$(INTDIR)\Deployment_ContainerS.obj" \
	"$(INTDIR)\DeploymentS.obj" \
	"$(INTDIR)\Deployment_RepositoryManagerS.obj" \
	"$(INTDIR)\Deployment_NodeManagerS.obj" \
	"$(INTDIR)\Deployment_NodeApplicationManagerS.obj" \
	"$(INTDIR)\Deployment_ApplicationManagerS.obj" \
	"$(INTDIR)\Deployment_DomainApplicationManagerS.obj" \
	"$(INTDIR)\Deployment_DomainApplicationS.obj" \
	"$(INTDIR)\Deployment_ExecutionManagerS.obj" \
	"$(INTDIR)\NodeApp_CB_Impl.obj" \
	"$(INTDIR)\Deployment_TargetManagerS.obj" \
	"$(INTDIR)\CIAO_NodeApplication_CallBackS.obj" \
	"$(INTDIR)\Deployment_ResourceCommitmentManagerS.obj"

"..\..\..\..\lib\CIAO_Deployment_svntd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\lib\CIAO_Deployment_svntd.dll.manifest" mt.exe -manifest "..\..\..\..\lib\CIAO_Deployment_svntd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\..\..\lib
INTDIR=Release\Deployment_svnt\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\lib\CIAO_Deployment_svnt.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\ciaosvcs\Events" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DDEPLOYMENT_SVNT_BUILD_DLL -f "Makefile.Deployment_svnt.dep" "Deployment_CoreS.cpp" "Deployment_ApplicationS.cpp" "Deployment_NodeApplicationS.cpp" "Deployment_ContainerS.cpp" "DeploymentS.cpp" "Deployment_RepositoryManagerS.cpp" "Deployment_NodeManagerS.cpp" "Deployment_NodeApplicationManagerS.cpp" "Deployment_ApplicationManagerS.cpp" "Deployment_DomainApplicationManagerS.cpp" "Deployment_DomainApplicationS.cpp" "Deployment_ExecutionManagerS.cpp" "NodeApp_CB_Impl.cpp" "Deployment_TargetManagerS.cpp" "CIAO_NodeApplication_CallBackS.cpp" "Deployment_ResourceCommitmentManagerS.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\..\..\lib\CIAO_Deployment_svnt.dll"
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_svnt.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_svnt.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_svnt.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\Deployment_svnt\$(NULL)" mkdir "Release\Deployment_svnt"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\ciaosvcs\Events" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D DEPLOYMENT_SVNT_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_PortableServer.lib TAO_Valuetype.lib TAO_IFR_Client.lib TAO_CodecFactory.lib TAO_PI.lib CIAO_Client.lib CIAO_Container.lib CIAO_Events_Base.lib TAO_Messaging.lib CIAO_Deployment_stub.lib TAO_Svc_Utils.lib TAO_RTEvent.lib TAO_RTEvent_Skel.lib TAO_RTEvent_Serv.lib TAO_CosNaming.lib CIAO_RTEvent.lib CIAO_Events.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\..\..\lib\CIAO_Deployment_svnt.dll" /implib:"$(OUTDIR)\CIAO_Deployment_svnt.lib"
LINK32_OBJS= \
	"$(INTDIR)\Deployment_CoreS.obj" \
	"$(INTDIR)\Deployment_ApplicationS.obj" \
	"$(INTDIR)\Deployment_NodeApplicationS.obj" \
	"$(INTDIR)\Deployment_ContainerS.obj" \
	"$(INTDIR)\DeploymentS.obj" \
	"$(INTDIR)\Deployment_RepositoryManagerS.obj" \
	"$(INTDIR)\Deployment_NodeManagerS.obj" \
	"$(INTDIR)\Deployment_NodeApplicationManagerS.obj" \
	"$(INTDIR)\Deployment_ApplicationManagerS.obj" \
	"$(INTDIR)\Deployment_DomainApplicationManagerS.obj" \
	"$(INTDIR)\Deployment_DomainApplicationS.obj" \
	"$(INTDIR)\Deployment_ExecutionManagerS.obj" \
	"$(INTDIR)\NodeApp_CB_Impl.obj" \
	"$(INTDIR)\Deployment_TargetManagerS.obj" \
	"$(INTDIR)\CIAO_NodeApplication_CallBackS.obj" \
	"$(INTDIR)\Deployment_ResourceCommitmentManagerS.obj"

"..\..\..\..\lib\CIAO_Deployment_svnt.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\lib\CIAO_Deployment_svnt.dll.manifest" mt.exe -manifest "..\..\..\..\lib\CIAO_Deployment_svnt.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\..\..\lib
INTDIR=Static_Debug\Deployment_svnt\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CIAO_Deployment_svntsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\ciaosvcs\Events" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Deployment_svnt.dep" "Deployment_CoreS.cpp" "Deployment_ApplicationS.cpp" "Deployment_NodeApplicationS.cpp" "Deployment_ContainerS.cpp" "DeploymentS.cpp" "Deployment_RepositoryManagerS.cpp" "Deployment_NodeManagerS.cpp" "Deployment_NodeApplicationManagerS.cpp" "Deployment_ApplicationManagerS.cpp" "Deployment_DomainApplicationManagerS.cpp" "Deployment_DomainApplicationS.cpp" "Deployment_ExecutionManagerS.cpp" "NodeApp_CB_Impl.cpp" "Deployment_TargetManagerS.cpp" "CIAO_NodeApplication_CallBackS.cpp" "Deployment_ResourceCommitmentManagerS.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_svntsd.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_svntsd.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_svntsd.ilk"
	-@del /f/q "..\..\..\..\lib\CIAO_Deployment_svntsd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\Deployment_svnt\$(NULL)" mkdir "Static_Debug\Deployment_svnt"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"..\..\..\..\lib\CIAO_Deployment_svntsd.pdb" /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\ciaosvcs\Events" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\lib\CIAO_Deployment_svntsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Deployment_CoreS.obj" \
	"$(INTDIR)\Deployment_ApplicationS.obj" \
	"$(INTDIR)\Deployment_NodeApplicationS.obj" \
	"$(INTDIR)\Deployment_ContainerS.obj" \
	"$(INTDIR)\DeploymentS.obj" \
	"$(INTDIR)\Deployment_RepositoryManagerS.obj" \
	"$(INTDIR)\Deployment_NodeManagerS.obj" \
	"$(INTDIR)\Deployment_NodeApplicationManagerS.obj" \
	"$(INTDIR)\Deployment_ApplicationManagerS.obj" \
	"$(INTDIR)\Deployment_DomainApplicationManagerS.obj" \
	"$(INTDIR)\Deployment_DomainApplicationS.obj" \
	"$(INTDIR)\Deployment_ExecutionManagerS.obj" \
	"$(INTDIR)\NodeApp_CB_Impl.obj" \
	"$(INTDIR)\Deployment_TargetManagerS.obj" \
	"$(INTDIR)\CIAO_NodeApplication_CallBackS.obj" \
	"$(INTDIR)\Deployment_ResourceCommitmentManagerS.obj"

"$(OUTDIR)\CIAO_Deployment_svntsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CIAO_Deployment_svntsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\CIAO_Deployment_svntsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\..\..\lib
INTDIR=Static_Release\Deployment_svnt\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CIAO_Deployment_svnts.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\ciaosvcs\Events" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Deployment_svnt.dep" "Deployment_CoreS.cpp" "Deployment_ApplicationS.cpp" "Deployment_NodeApplicationS.cpp" "Deployment_ContainerS.cpp" "DeploymentS.cpp" "Deployment_RepositoryManagerS.cpp" "Deployment_NodeManagerS.cpp" "Deployment_NodeApplicationManagerS.cpp" "Deployment_ApplicationManagerS.cpp" "Deployment_DomainApplicationManagerS.cpp" "Deployment_DomainApplicationS.cpp" "Deployment_ExecutionManagerS.cpp" "NodeApp_CB_Impl.cpp" "Deployment_TargetManagerS.cpp" "CIAO_NodeApplication_CallBackS.cpp" "Deployment_ResourceCommitmentManagerS.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_svnts.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_svnts.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_svnts.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\Deployment_svnt\$(NULL)" mkdir "Static_Release\Deployment_svnt"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\ciaosvcs\Events" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\lib\CIAO_Deployment_svnts.lib"
LINK32_OBJS= \
	"$(INTDIR)\Deployment_CoreS.obj" \
	"$(INTDIR)\Deployment_ApplicationS.obj" \
	"$(INTDIR)\Deployment_NodeApplicationS.obj" \
	"$(INTDIR)\Deployment_ContainerS.obj" \
	"$(INTDIR)\DeploymentS.obj" \
	"$(INTDIR)\Deployment_RepositoryManagerS.obj" \
	"$(INTDIR)\Deployment_NodeManagerS.obj" \
	"$(INTDIR)\Deployment_NodeApplicationManagerS.obj" \
	"$(INTDIR)\Deployment_ApplicationManagerS.obj" \
	"$(INTDIR)\Deployment_DomainApplicationManagerS.obj" \
	"$(INTDIR)\Deployment_DomainApplicationS.obj" \
	"$(INTDIR)\Deployment_ExecutionManagerS.obj" \
	"$(INTDIR)\NodeApp_CB_Impl.obj" \
	"$(INTDIR)\Deployment_TargetManagerS.obj" \
	"$(INTDIR)\CIAO_NodeApplication_CallBackS.obj" \
	"$(INTDIR)\Deployment_ResourceCommitmentManagerS.obj"

"$(OUTDIR)\CIAO_Deployment_svnts.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CIAO_Deployment_svnts.lib.manifest" mt.exe -manifest "$(OUTDIR)\CIAO_Deployment_svnts.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Deployment_svnt.dep")
!INCLUDE "Makefile.Deployment_svnt.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Deployment_CoreS.cpp"

"$(INTDIR)\Deployment_CoreS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_CoreS.obj" $(SOURCE)

SOURCE="Deployment_ApplicationS.cpp"

"$(INTDIR)\Deployment_ApplicationS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_ApplicationS.obj" $(SOURCE)

SOURCE="Deployment_NodeApplicationS.cpp"

"$(INTDIR)\Deployment_NodeApplicationS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_NodeApplicationS.obj" $(SOURCE)

SOURCE="Deployment_ContainerS.cpp"

"$(INTDIR)\Deployment_ContainerS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_ContainerS.obj" $(SOURCE)

SOURCE="DeploymentS.cpp"

"$(INTDIR)\DeploymentS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DeploymentS.obj" $(SOURCE)

SOURCE="Deployment_RepositoryManagerS.cpp"

"$(INTDIR)\Deployment_RepositoryManagerS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_RepositoryManagerS.obj" $(SOURCE)

SOURCE="Deployment_NodeManagerS.cpp"

"$(INTDIR)\Deployment_NodeManagerS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_NodeManagerS.obj" $(SOURCE)

SOURCE="Deployment_NodeApplicationManagerS.cpp"

"$(INTDIR)\Deployment_NodeApplicationManagerS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_NodeApplicationManagerS.obj" $(SOURCE)

SOURCE="Deployment_ApplicationManagerS.cpp"

"$(INTDIR)\Deployment_ApplicationManagerS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_ApplicationManagerS.obj" $(SOURCE)

SOURCE="Deployment_DomainApplicationManagerS.cpp"

"$(INTDIR)\Deployment_DomainApplicationManagerS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_DomainApplicationManagerS.obj" $(SOURCE)

SOURCE="Deployment_DomainApplicationS.cpp"

"$(INTDIR)\Deployment_DomainApplicationS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_DomainApplicationS.obj" $(SOURCE)

SOURCE="Deployment_ExecutionManagerS.cpp"

"$(INTDIR)\Deployment_ExecutionManagerS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_ExecutionManagerS.obj" $(SOURCE)

SOURCE="NodeApp_CB_Impl.cpp"

"$(INTDIR)\NodeApp_CB_Impl.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\NodeApp_CB_Impl.obj" $(SOURCE)

SOURCE="Deployment_TargetManagerS.cpp"

"$(INTDIR)\Deployment_TargetManagerS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_TargetManagerS.obj" $(SOURCE)

SOURCE="CIAO_NodeApplication_CallBackS.cpp"

"$(INTDIR)\CIAO_NodeApplication_CallBackS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CIAO_NodeApplication_CallBackS.obj" $(SOURCE)

SOURCE="Deployment_ResourceCommitmentManagerS.cpp"

"$(INTDIR)\Deployment_ResourceCommitmentManagerS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_ResourceCommitmentManagerS.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Deployment_svnt.dep")
	@echo Using "Makefile.Deployment_svnt.dep"
!ELSE
	@echo Warning: cannot find "Makefile.Deployment_svnt.dep"
!ENDIF
!ENDIF

