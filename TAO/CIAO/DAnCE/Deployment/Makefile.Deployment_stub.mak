# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.Deployment_stub.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "Deployment_Packaging_DataC.h" "Deployment_Packaging_DataS.h" "Deployment_Packaging_DataC.cpp" "Deployment_PlanErrorC.h" "Deployment_PlanErrorS.h" "Deployment_PlanErrorC.cpp" "Deployment_BaseC.inl" "Deployment_BaseC.h" "Deployment_BaseS.h" "Deployment_BaseC.cpp" "Deployment_ConnectionC.inl" "Deployment_ConnectionC.h" "Deployment_ConnectionS.h" "Deployment_ConnectionC.cpp" "Deployment_DataC.inl" "Deployment_DataC.h" "Deployment_DataS.h" "Deployment_DataC.cpp" "Deployment_DeploymentPlanC.inl" "Deployment_DeploymentPlanC.h" "Deployment_DeploymentPlanS.h" "Deployment_DeploymentPlanC.cpp" "Deployment_TargetDataC.inl" "Deployment_TargetDataC.h" "Deployment_TargetDataS.h" "Deployment_TargetDataC.cpp" "Deployment_EventsC.inl" "Deployment_EventsC.h" "Deployment_EventsS.h" "Deployment_EventsC.cpp" "CIAO_ServerResourcesC.inl" "CIAO_ServerResourcesC.h" "CIAO_ServerResourcesS.h" "CIAO_ServerResourcesC.cpp" "Deployment_ApplicationManagerC.inl" "Deployment_ApplicationManagerS.inl" "Deployment_ApplicationManagerC.h" "Deployment_ApplicationManagerS.h" "Deployment_ApplicationManagerC.cpp" "Deployment_NodeApplicationManagerC.inl" "Deployment_NodeApplicationManagerS.inl" "Deployment_NodeApplicationManagerC.h" "Deployment_NodeApplicationManagerS.h" "Deployment_NodeApplicationManagerC.cpp" "Deployment_DomainApplicationManagerC.inl" "Deployment_DomainApplicationManagerS.inl" "Deployment_DomainApplicationManagerC.h" "Deployment_DomainApplicationManagerS.h" "Deployment_DomainApplicationManagerC.cpp" "Deployment_ResourceCommitmentManagerC.inl" "Deployment_ResourceCommitmentManagerS.inl" "Deployment_ResourceCommitmentManagerC.h" "Deployment_ResourceCommitmentManagerS.h" "Deployment_ResourceCommitmentManagerC.cpp" "Deployment_TargetManagerC.inl" "Deployment_TargetManagerS.inl" "Deployment_TargetManagerC.h" "Deployment_TargetManagerS.h" "Deployment_TargetManagerC.cpp" "CIAO_NodeApplication_CallBackC.inl" "CIAO_NodeApplication_CallBackS.inl" "CIAO_NodeApplication_CallBackC.h" "CIAO_NodeApplication_CallBackS.h" "CIAO_NodeApplication_CallBackC.cpp" "Deployment_CoreC.inl" "Deployment_CoreS.inl" "Deployment_CoreC.h" "Deployment_CoreS.h" "Deployment_CoreC.cpp" "Deployment_NodeApplicationC.inl" "Deployment_NodeApplicationS.inl" "Deployment_NodeApplicationC.h" "Deployment_NodeApplicationS.h" "Deployment_NodeApplicationC.cpp" "Deployment_ApplicationC.inl" "Deployment_ApplicationS.inl" "Deployment_ApplicationC.h" "Deployment_ApplicationS.h" "Deployment_ApplicationC.cpp" "Deployment_ContainerC.inl" "Deployment_ContainerS.inl" "Deployment_ContainerC.h" "Deployment_ContainerS.h" "Deployment_ContainerC.cpp" "DeploymentC.inl" "DeploymentS.inl" "DeploymentC.h" "DeploymentS.h" "DeploymentC.cpp" "Deployment_NodeManagerC.inl" "Deployment_NodeManagerS.inl" "Deployment_NodeManagerC.h" "Deployment_NodeManagerS.h" "Deployment_NodeManagerC.cpp" "Deployment_DomainApplicationC.inl" "Deployment_DomainApplicationS.inl" "Deployment_DomainApplicationC.h" "Deployment_DomainApplicationS.h" "Deployment_DomainApplicationC.cpp" "Deployment_ExecutionManagerC.inl" "Deployment_ExecutionManagerS.inl" "Deployment_ExecutionManagerC.h" "Deployment_ExecutionManagerS.h" "Deployment_ExecutionManagerC.cpp" "Deployment_RepositoryManagerC.inl" "Deployment_RepositoryManagerS.inl" "Deployment_RepositoryManagerC.h" "Deployment_RepositoryManagerS.h" "Deployment_RepositoryManagerC.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\..\..\lib
INTDIR=Debug\Deployment_stub\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\lib\CIAO_Deployment_stubd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\..\orbsvcs" -I"..\.." -I"..\..\ciao" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DDEPLOYMENT_STUB_BUILD_DLL -f "Makefile.Deployment_stub.dep" "Deployment_CoreC.cpp" "Deployment_BaseC.cpp" "Deployment_ConnectionC.cpp" "Deployment_DataC.cpp" "Deployment_DeploymentPlanC.cpp" "Deployment_TargetDataC.cpp" "Deployment_NodeApplicationC.cpp" "Deployment_ApplicationC.cpp" "Deployment_ContainerC.cpp" "Deployment_Packaging_DataC.cpp" "Deployment_PlanErrorC.cpp" "DeploymentC.cpp" "Deployment_TargetManagerC.cpp" "Deployment_NodeManagerC.cpp" "Deployment_NodeApplicationManagerC.cpp" "Deployment_ApplicationManagerC.cpp" "Deployment_DomainApplicationManagerC.cpp" "Deployment_DomainApplicationC.cpp" "Deployment_ExecutionManagerC.cpp" "Deployment_RepositoryManagerC.cpp" "Deployment_ResourceCommitmentManagerC.cpp" "CIAO_NodeApplication_CallBackC.cpp" "CIAO_ServerResourcesC.cpp" "Deployment_EventsC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_stubd.pdb"
	-@del /f/q "..\..\..\..\lib\CIAO_Deployment_stubd.dll"
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_stubd.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_stubd.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_stubd.ilk"
	-@del /f/q "Deployment_Packaging_DataC.h"
	-@del /f/q "Deployment_Packaging_DataS.h"
	-@del /f/q "Deployment_Packaging_DataC.cpp"
	-@del /f/q "Deployment_PlanErrorC.h"
	-@del /f/q "Deployment_PlanErrorS.h"
	-@del /f/q "Deployment_PlanErrorC.cpp"
	-@del /f/q "Deployment_BaseC.inl"
	-@del /f/q "Deployment_BaseC.h"
	-@del /f/q "Deployment_BaseS.h"
	-@del /f/q "Deployment_BaseC.cpp"
	-@del /f/q "Deployment_ConnectionC.inl"
	-@del /f/q "Deployment_ConnectionC.h"
	-@del /f/q "Deployment_ConnectionS.h"
	-@del /f/q "Deployment_ConnectionC.cpp"
	-@del /f/q "Deployment_DataC.inl"
	-@del /f/q "Deployment_DataC.h"
	-@del /f/q "Deployment_DataS.h"
	-@del /f/q "Deployment_DataC.cpp"
	-@del /f/q "Deployment_DeploymentPlanC.inl"
	-@del /f/q "Deployment_DeploymentPlanC.h"
	-@del /f/q "Deployment_DeploymentPlanS.h"
	-@del /f/q "Deployment_DeploymentPlanC.cpp"
	-@del /f/q "Deployment_TargetDataC.inl"
	-@del /f/q "Deployment_TargetDataC.h"
	-@del /f/q "Deployment_TargetDataS.h"
	-@del /f/q "Deployment_TargetDataC.cpp"
	-@del /f/q "Deployment_EventsC.inl"
	-@del /f/q "Deployment_EventsC.h"
	-@del /f/q "Deployment_EventsS.h"
	-@del /f/q "Deployment_EventsC.cpp"
	-@del /f/q "CIAO_ServerResourcesC.inl"
	-@del /f/q "CIAO_ServerResourcesC.h"
	-@del /f/q "CIAO_ServerResourcesS.h"
	-@del /f/q "CIAO_ServerResourcesC.cpp"
	-@del /f/q "Deployment_ApplicationManagerC.inl"
	-@del /f/q "Deployment_ApplicationManagerS.inl"
	-@del /f/q "Deployment_ApplicationManagerC.h"
	-@del /f/q "Deployment_ApplicationManagerS.h"
	-@del /f/q "Deployment_ApplicationManagerC.cpp"
	-@del /f/q "Deployment_NodeApplicationManagerC.inl"
	-@del /f/q "Deployment_NodeApplicationManagerS.inl"
	-@del /f/q "Deployment_NodeApplicationManagerC.h"
	-@del /f/q "Deployment_NodeApplicationManagerS.h"
	-@del /f/q "Deployment_NodeApplicationManagerC.cpp"
	-@del /f/q "Deployment_DomainApplicationManagerC.inl"
	-@del /f/q "Deployment_DomainApplicationManagerS.inl"
	-@del /f/q "Deployment_DomainApplicationManagerC.h"
	-@del /f/q "Deployment_DomainApplicationManagerS.h"
	-@del /f/q "Deployment_DomainApplicationManagerC.cpp"
	-@del /f/q "Deployment_ResourceCommitmentManagerC.inl"
	-@del /f/q "Deployment_ResourceCommitmentManagerS.inl"
	-@del /f/q "Deployment_ResourceCommitmentManagerC.h"
	-@del /f/q "Deployment_ResourceCommitmentManagerS.h"
	-@del /f/q "Deployment_ResourceCommitmentManagerC.cpp"
	-@del /f/q "Deployment_TargetManagerC.inl"
	-@del /f/q "Deployment_TargetManagerS.inl"
	-@del /f/q "Deployment_TargetManagerC.h"
	-@del /f/q "Deployment_TargetManagerS.h"
	-@del /f/q "Deployment_TargetManagerC.cpp"
	-@del /f/q "CIAO_NodeApplication_CallBackC.inl"
	-@del /f/q "CIAO_NodeApplication_CallBackS.inl"
	-@del /f/q "CIAO_NodeApplication_CallBackC.h"
	-@del /f/q "CIAO_NodeApplication_CallBackS.h"
	-@del /f/q "CIAO_NodeApplication_CallBackC.cpp"
	-@del /f/q "Deployment_CoreC.inl"
	-@del /f/q "Deployment_CoreS.inl"
	-@del /f/q "Deployment_CoreC.h"
	-@del /f/q "Deployment_CoreS.h"
	-@del /f/q "Deployment_CoreC.cpp"
	-@del /f/q "Deployment_NodeApplicationC.inl"
	-@del /f/q "Deployment_NodeApplicationS.inl"
	-@del /f/q "Deployment_NodeApplicationC.h"
	-@del /f/q "Deployment_NodeApplicationS.h"
	-@del /f/q "Deployment_NodeApplicationC.cpp"
	-@del /f/q "Deployment_ApplicationC.inl"
	-@del /f/q "Deployment_ApplicationS.inl"
	-@del /f/q "Deployment_ApplicationC.h"
	-@del /f/q "Deployment_ApplicationS.h"
	-@del /f/q "Deployment_ApplicationC.cpp"
	-@del /f/q "Deployment_ContainerC.inl"
	-@del /f/q "Deployment_ContainerS.inl"
	-@del /f/q "Deployment_ContainerC.h"
	-@del /f/q "Deployment_ContainerS.h"
	-@del /f/q "Deployment_ContainerC.cpp"
	-@del /f/q "DeploymentC.inl"
	-@del /f/q "DeploymentS.inl"
	-@del /f/q "DeploymentC.h"
	-@del /f/q "DeploymentS.h"
	-@del /f/q "DeploymentC.cpp"
	-@del /f/q "Deployment_NodeManagerC.inl"
	-@del /f/q "Deployment_NodeManagerS.inl"
	-@del /f/q "Deployment_NodeManagerC.h"
	-@del /f/q "Deployment_NodeManagerS.h"
	-@del /f/q "Deployment_NodeManagerC.cpp"
	-@del /f/q "Deployment_DomainApplicationC.inl"
	-@del /f/q "Deployment_DomainApplicationS.inl"
	-@del /f/q "Deployment_DomainApplicationC.h"
	-@del /f/q "Deployment_DomainApplicationS.h"
	-@del /f/q "Deployment_DomainApplicationC.cpp"
	-@del /f/q "Deployment_ExecutionManagerC.inl"
	-@del /f/q "Deployment_ExecutionManagerS.inl"
	-@del /f/q "Deployment_ExecutionManagerC.h"
	-@del /f/q "Deployment_ExecutionManagerS.h"
	-@del /f/q "Deployment_ExecutionManagerC.cpp"
	-@del /f/q "Deployment_RepositoryManagerC.inl"
	-@del /f/q "Deployment_RepositoryManagerS.inl"
	-@del /f/q "Deployment_RepositoryManagerC.h"
	-@del /f/q "Deployment_RepositoryManagerS.h"
	-@del /f/q "Deployment_RepositoryManagerC.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\Deployment_stub\$(NULL)" mkdir "Debug\Deployment_stub"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /I "..\..\..\orbsvcs" /I "..\.." /I "..\..\ciao" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D DEPLOYMENT_STUB_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_Valuetyped.lib TAO_IFR_Clientd.lib TAO_PortableServerd.lib TAO_CodecFactoryd.lib TAO_PId.lib CIAO_Clientd.lib CIAO_Containerd.lib CIAO_Events_Based.lib TAO_Messagingd.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\..\..\lib\CIAO_Deployment_stubd.pdb" /machine:IA64 /out:"..\..\..\..\lib\CIAO_Deployment_stubd.dll" /implib:"$(OUTDIR)\CIAO_Deployment_stubd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Deployment_CoreC.obj" \
	"$(INTDIR)\Deployment_BaseC.obj" \
	"$(INTDIR)\Deployment_ConnectionC.obj" \
	"$(INTDIR)\Deployment_DataC.obj" \
	"$(INTDIR)\Deployment_DeploymentPlanC.obj" \
	"$(INTDIR)\Deployment_TargetDataC.obj" \
	"$(INTDIR)\Deployment_NodeApplicationC.obj" \
	"$(INTDIR)\Deployment_ApplicationC.obj" \
	"$(INTDIR)\Deployment_ContainerC.obj" \
	"$(INTDIR)\Deployment_Packaging_DataC.obj" \
	"$(INTDIR)\Deployment_PlanErrorC.obj" \
	"$(INTDIR)\DeploymentC.obj" \
	"$(INTDIR)\Deployment_TargetManagerC.obj" \
	"$(INTDIR)\Deployment_NodeManagerC.obj" \
	"$(INTDIR)\Deployment_NodeApplicationManagerC.obj" \
	"$(INTDIR)\Deployment_ApplicationManagerC.obj" \
	"$(INTDIR)\Deployment_DomainApplicationManagerC.obj" \
	"$(INTDIR)\Deployment_DomainApplicationC.obj" \
	"$(INTDIR)\Deployment_ExecutionManagerC.obj" \
	"$(INTDIR)\Deployment_RepositoryManagerC.obj" \
	"$(INTDIR)\Deployment_ResourceCommitmentManagerC.obj" \
	"$(INTDIR)\CIAO_NodeApplication_CallBackC.obj" \
	"$(INTDIR)\CIAO_ServerResourcesC.obj" \
	"$(INTDIR)\Deployment_EventsC.obj"

"..\..\..\..\lib\CIAO_Deployment_stubd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\lib\CIAO_Deployment_stubd.dll.manifest" mt.exe -manifest "..\..\..\..\lib\CIAO_Deployment_stubd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\..\..\lib
INTDIR=Release\Deployment_stub\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\lib\CIAO_Deployment_stub.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\..\orbsvcs" -I"..\.." -I"..\..\ciao" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DDEPLOYMENT_STUB_BUILD_DLL -f "Makefile.Deployment_stub.dep" "Deployment_CoreC.cpp" "Deployment_BaseC.cpp" "Deployment_ConnectionC.cpp" "Deployment_DataC.cpp" "Deployment_DeploymentPlanC.cpp" "Deployment_TargetDataC.cpp" "Deployment_NodeApplicationC.cpp" "Deployment_ApplicationC.cpp" "Deployment_ContainerC.cpp" "Deployment_Packaging_DataC.cpp" "Deployment_PlanErrorC.cpp" "DeploymentC.cpp" "Deployment_TargetManagerC.cpp" "Deployment_NodeManagerC.cpp" "Deployment_NodeApplicationManagerC.cpp" "Deployment_ApplicationManagerC.cpp" "Deployment_DomainApplicationManagerC.cpp" "Deployment_DomainApplicationC.cpp" "Deployment_ExecutionManagerC.cpp" "Deployment_RepositoryManagerC.cpp" "Deployment_ResourceCommitmentManagerC.cpp" "CIAO_NodeApplication_CallBackC.cpp" "CIAO_ServerResourcesC.cpp" "Deployment_EventsC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\..\..\lib\CIAO_Deployment_stub.dll"
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_stub.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_stub.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_stub.ilk"
	-@del /f/q "Deployment_Packaging_DataC.h"
	-@del /f/q "Deployment_Packaging_DataS.h"
	-@del /f/q "Deployment_Packaging_DataC.cpp"
	-@del /f/q "Deployment_PlanErrorC.h"
	-@del /f/q "Deployment_PlanErrorS.h"
	-@del /f/q "Deployment_PlanErrorC.cpp"
	-@del /f/q "Deployment_BaseC.inl"
	-@del /f/q "Deployment_BaseC.h"
	-@del /f/q "Deployment_BaseS.h"
	-@del /f/q "Deployment_BaseC.cpp"
	-@del /f/q "Deployment_ConnectionC.inl"
	-@del /f/q "Deployment_ConnectionC.h"
	-@del /f/q "Deployment_ConnectionS.h"
	-@del /f/q "Deployment_ConnectionC.cpp"
	-@del /f/q "Deployment_DataC.inl"
	-@del /f/q "Deployment_DataC.h"
	-@del /f/q "Deployment_DataS.h"
	-@del /f/q "Deployment_DataC.cpp"
	-@del /f/q "Deployment_DeploymentPlanC.inl"
	-@del /f/q "Deployment_DeploymentPlanC.h"
	-@del /f/q "Deployment_DeploymentPlanS.h"
	-@del /f/q "Deployment_DeploymentPlanC.cpp"
	-@del /f/q "Deployment_TargetDataC.inl"
	-@del /f/q "Deployment_TargetDataC.h"
	-@del /f/q "Deployment_TargetDataS.h"
	-@del /f/q "Deployment_TargetDataC.cpp"
	-@del /f/q "Deployment_EventsC.inl"
	-@del /f/q "Deployment_EventsC.h"
	-@del /f/q "Deployment_EventsS.h"
	-@del /f/q "Deployment_EventsC.cpp"
	-@del /f/q "CIAO_ServerResourcesC.inl"
	-@del /f/q "CIAO_ServerResourcesC.h"
	-@del /f/q "CIAO_ServerResourcesS.h"
	-@del /f/q "CIAO_ServerResourcesC.cpp"
	-@del /f/q "Deployment_ApplicationManagerC.inl"
	-@del /f/q "Deployment_ApplicationManagerS.inl"
	-@del /f/q "Deployment_ApplicationManagerC.h"
	-@del /f/q "Deployment_ApplicationManagerS.h"
	-@del /f/q "Deployment_ApplicationManagerC.cpp"
	-@del /f/q "Deployment_NodeApplicationManagerC.inl"
	-@del /f/q "Deployment_NodeApplicationManagerS.inl"
	-@del /f/q "Deployment_NodeApplicationManagerC.h"
	-@del /f/q "Deployment_NodeApplicationManagerS.h"
	-@del /f/q "Deployment_NodeApplicationManagerC.cpp"
	-@del /f/q "Deployment_DomainApplicationManagerC.inl"
	-@del /f/q "Deployment_DomainApplicationManagerS.inl"
	-@del /f/q "Deployment_DomainApplicationManagerC.h"
	-@del /f/q "Deployment_DomainApplicationManagerS.h"
	-@del /f/q "Deployment_DomainApplicationManagerC.cpp"
	-@del /f/q "Deployment_ResourceCommitmentManagerC.inl"
	-@del /f/q "Deployment_ResourceCommitmentManagerS.inl"
	-@del /f/q "Deployment_ResourceCommitmentManagerC.h"
	-@del /f/q "Deployment_ResourceCommitmentManagerS.h"
	-@del /f/q "Deployment_ResourceCommitmentManagerC.cpp"
	-@del /f/q "Deployment_TargetManagerC.inl"
	-@del /f/q "Deployment_TargetManagerS.inl"
	-@del /f/q "Deployment_TargetManagerC.h"
	-@del /f/q "Deployment_TargetManagerS.h"
	-@del /f/q "Deployment_TargetManagerC.cpp"
	-@del /f/q "CIAO_NodeApplication_CallBackC.inl"
	-@del /f/q "CIAO_NodeApplication_CallBackS.inl"
	-@del /f/q "CIAO_NodeApplication_CallBackC.h"
	-@del /f/q "CIAO_NodeApplication_CallBackS.h"
	-@del /f/q "CIAO_NodeApplication_CallBackC.cpp"
	-@del /f/q "Deployment_CoreC.inl"
	-@del /f/q "Deployment_CoreS.inl"
	-@del /f/q "Deployment_CoreC.h"
	-@del /f/q "Deployment_CoreS.h"
	-@del /f/q "Deployment_CoreC.cpp"
	-@del /f/q "Deployment_NodeApplicationC.inl"
	-@del /f/q "Deployment_NodeApplicationS.inl"
	-@del /f/q "Deployment_NodeApplicationC.h"
	-@del /f/q "Deployment_NodeApplicationS.h"
	-@del /f/q "Deployment_NodeApplicationC.cpp"
	-@del /f/q "Deployment_ApplicationC.inl"
	-@del /f/q "Deployment_ApplicationS.inl"
	-@del /f/q "Deployment_ApplicationC.h"
	-@del /f/q "Deployment_ApplicationS.h"
	-@del /f/q "Deployment_ApplicationC.cpp"
	-@del /f/q "Deployment_ContainerC.inl"
	-@del /f/q "Deployment_ContainerS.inl"
	-@del /f/q "Deployment_ContainerC.h"
	-@del /f/q "Deployment_ContainerS.h"
	-@del /f/q "Deployment_ContainerC.cpp"
	-@del /f/q "DeploymentC.inl"
	-@del /f/q "DeploymentS.inl"
	-@del /f/q "DeploymentC.h"
	-@del /f/q "DeploymentS.h"
	-@del /f/q "DeploymentC.cpp"
	-@del /f/q "Deployment_NodeManagerC.inl"
	-@del /f/q "Deployment_NodeManagerS.inl"
	-@del /f/q "Deployment_NodeManagerC.h"
	-@del /f/q "Deployment_NodeManagerS.h"
	-@del /f/q "Deployment_NodeManagerC.cpp"
	-@del /f/q "Deployment_DomainApplicationC.inl"
	-@del /f/q "Deployment_DomainApplicationS.inl"
	-@del /f/q "Deployment_DomainApplicationC.h"
	-@del /f/q "Deployment_DomainApplicationS.h"
	-@del /f/q "Deployment_DomainApplicationC.cpp"
	-@del /f/q "Deployment_ExecutionManagerC.inl"
	-@del /f/q "Deployment_ExecutionManagerS.inl"
	-@del /f/q "Deployment_ExecutionManagerC.h"
	-@del /f/q "Deployment_ExecutionManagerS.h"
	-@del /f/q "Deployment_ExecutionManagerC.cpp"
	-@del /f/q "Deployment_RepositoryManagerC.inl"
	-@del /f/q "Deployment_RepositoryManagerS.inl"
	-@del /f/q "Deployment_RepositoryManagerC.h"
	-@del /f/q "Deployment_RepositoryManagerS.h"
	-@del /f/q "Deployment_RepositoryManagerC.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\Deployment_stub\$(NULL)" mkdir "Release\Deployment_stub"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\.." /I "..\..\.." /I "..\..\..\orbsvcs" /I "..\.." /I "..\..\ciao" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D DEPLOYMENT_STUB_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_Valuetype.lib TAO_IFR_Client.lib TAO_PortableServer.lib TAO_CodecFactory.lib TAO_PI.lib CIAO_Client.lib CIAO_Container.lib CIAO_Events_Base.lib TAO_Messaging.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\..\..\lib\CIAO_Deployment_stub.dll" /implib:"$(OUTDIR)\CIAO_Deployment_stub.lib"
LINK32_OBJS= \
	"$(INTDIR)\Deployment_CoreC.obj" \
	"$(INTDIR)\Deployment_BaseC.obj" \
	"$(INTDIR)\Deployment_ConnectionC.obj" \
	"$(INTDIR)\Deployment_DataC.obj" \
	"$(INTDIR)\Deployment_DeploymentPlanC.obj" \
	"$(INTDIR)\Deployment_TargetDataC.obj" \
	"$(INTDIR)\Deployment_NodeApplicationC.obj" \
	"$(INTDIR)\Deployment_ApplicationC.obj" \
	"$(INTDIR)\Deployment_ContainerC.obj" \
	"$(INTDIR)\Deployment_Packaging_DataC.obj" \
	"$(INTDIR)\Deployment_PlanErrorC.obj" \
	"$(INTDIR)\DeploymentC.obj" \
	"$(INTDIR)\Deployment_TargetManagerC.obj" \
	"$(INTDIR)\Deployment_NodeManagerC.obj" \
	"$(INTDIR)\Deployment_NodeApplicationManagerC.obj" \
	"$(INTDIR)\Deployment_ApplicationManagerC.obj" \
	"$(INTDIR)\Deployment_DomainApplicationManagerC.obj" \
	"$(INTDIR)\Deployment_DomainApplicationC.obj" \
	"$(INTDIR)\Deployment_ExecutionManagerC.obj" \
	"$(INTDIR)\Deployment_RepositoryManagerC.obj" \
	"$(INTDIR)\Deployment_ResourceCommitmentManagerC.obj" \
	"$(INTDIR)\CIAO_NodeApplication_CallBackC.obj" \
	"$(INTDIR)\CIAO_ServerResourcesC.obj" \
	"$(INTDIR)\Deployment_EventsC.obj"

"..\..\..\..\lib\CIAO_Deployment_stub.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\lib\CIAO_Deployment_stub.dll.manifest" mt.exe -manifest "..\..\..\..\lib\CIAO_Deployment_stub.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\..\..\lib
INTDIR=Static_Debug\Deployment_stub\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CIAO_Deployment_stubsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\..\orbsvcs" -I"..\.." -I"..\..\ciao" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Deployment_stub.dep" "Deployment_CoreC.cpp" "Deployment_BaseC.cpp" "Deployment_ConnectionC.cpp" "Deployment_DataC.cpp" "Deployment_DeploymentPlanC.cpp" "Deployment_TargetDataC.cpp" "Deployment_NodeApplicationC.cpp" "Deployment_ApplicationC.cpp" "Deployment_ContainerC.cpp" "Deployment_Packaging_DataC.cpp" "Deployment_PlanErrorC.cpp" "DeploymentC.cpp" "Deployment_TargetManagerC.cpp" "Deployment_NodeManagerC.cpp" "Deployment_NodeApplicationManagerC.cpp" "Deployment_ApplicationManagerC.cpp" "Deployment_DomainApplicationManagerC.cpp" "Deployment_DomainApplicationC.cpp" "Deployment_ExecutionManagerC.cpp" "Deployment_RepositoryManagerC.cpp" "Deployment_ResourceCommitmentManagerC.cpp" "CIAO_NodeApplication_CallBackC.cpp" "CIAO_ServerResourcesC.cpp" "Deployment_EventsC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_stubsd.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_stubsd.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_stubsd.ilk"
	-@del /f/q "..\..\..\..\lib\CIAO_Deployment_stubsd.pdb"
	-@del /f/q "Deployment_Packaging_DataC.h"
	-@del /f/q "Deployment_Packaging_DataS.h"
	-@del /f/q "Deployment_Packaging_DataC.cpp"
	-@del /f/q "Deployment_PlanErrorC.h"
	-@del /f/q "Deployment_PlanErrorS.h"
	-@del /f/q "Deployment_PlanErrorC.cpp"
	-@del /f/q "Deployment_BaseC.inl"
	-@del /f/q "Deployment_BaseC.h"
	-@del /f/q "Deployment_BaseS.h"
	-@del /f/q "Deployment_BaseC.cpp"
	-@del /f/q "Deployment_ConnectionC.inl"
	-@del /f/q "Deployment_ConnectionC.h"
	-@del /f/q "Deployment_ConnectionS.h"
	-@del /f/q "Deployment_ConnectionC.cpp"
	-@del /f/q "Deployment_DataC.inl"
	-@del /f/q "Deployment_DataC.h"
	-@del /f/q "Deployment_DataS.h"
	-@del /f/q "Deployment_DataC.cpp"
	-@del /f/q "Deployment_DeploymentPlanC.inl"
	-@del /f/q "Deployment_DeploymentPlanC.h"
	-@del /f/q "Deployment_DeploymentPlanS.h"
	-@del /f/q "Deployment_DeploymentPlanC.cpp"
	-@del /f/q "Deployment_TargetDataC.inl"
	-@del /f/q "Deployment_TargetDataC.h"
	-@del /f/q "Deployment_TargetDataS.h"
	-@del /f/q "Deployment_TargetDataC.cpp"
	-@del /f/q "Deployment_EventsC.inl"
	-@del /f/q "Deployment_EventsC.h"
	-@del /f/q "Deployment_EventsS.h"
	-@del /f/q "Deployment_EventsC.cpp"
	-@del /f/q "CIAO_ServerResourcesC.inl"
	-@del /f/q "CIAO_ServerResourcesC.h"
	-@del /f/q "CIAO_ServerResourcesS.h"
	-@del /f/q "CIAO_ServerResourcesC.cpp"
	-@del /f/q "Deployment_ApplicationManagerC.inl"
	-@del /f/q "Deployment_ApplicationManagerS.inl"
	-@del /f/q "Deployment_ApplicationManagerC.h"
	-@del /f/q "Deployment_ApplicationManagerS.h"
	-@del /f/q "Deployment_ApplicationManagerC.cpp"
	-@del /f/q "Deployment_NodeApplicationManagerC.inl"
	-@del /f/q "Deployment_NodeApplicationManagerS.inl"
	-@del /f/q "Deployment_NodeApplicationManagerC.h"
	-@del /f/q "Deployment_NodeApplicationManagerS.h"
	-@del /f/q "Deployment_NodeApplicationManagerC.cpp"
	-@del /f/q "Deployment_DomainApplicationManagerC.inl"
	-@del /f/q "Deployment_DomainApplicationManagerS.inl"
	-@del /f/q "Deployment_DomainApplicationManagerC.h"
	-@del /f/q "Deployment_DomainApplicationManagerS.h"
	-@del /f/q "Deployment_DomainApplicationManagerC.cpp"
	-@del /f/q "Deployment_ResourceCommitmentManagerC.inl"
	-@del /f/q "Deployment_ResourceCommitmentManagerS.inl"
	-@del /f/q "Deployment_ResourceCommitmentManagerC.h"
	-@del /f/q "Deployment_ResourceCommitmentManagerS.h"
	-@del /f/q "Deployment_ResourceCommitmentManagerC.cpp"
	-@del /f/q "Deployment_TargetManagerC.inl"
	-@del /f/q "Deployment_TargetManagerS.inl"
	-@del /f/q "Deployment_TargetManagerC.h"
	-@del /f/q "Deployment_TargetManagerS.h"
	-@del /f/q "Deployment_TargetManagerC.cpp"
	-@del /f/q "CIAO_NodeApplication_CallBackC.inl"
	-@del /f/q "CIAO_NodeApplication_CallBackS.inl"
	-@del /f/q "CIAO_NodeApplication_CallBackC.h"
	-@del /f/q "CIAO_NodeApplication_CallBackS.h"
	-@del /f/q "CIAO_NodeApplication_CallBackC.cpp"
	-@del /f/q "Deployment_CoreC.inl"
	-@del /f/q "Deployment_CoreS.inl"
	-@del /f/q "Deployment_CoreC.h"
	-@del /f/q "Deployment_CoreS.h"
	-@del /f/q "Deployment_CoreC.cpp"
	-@del /f/q "Deployment_NodeApplicationC.inl"
	-@del /f/q "Deployment_NodeApplicationS.inl"
	-@del /f/q "Deployment_NodeApplicationC.h"
	-@del /f/q "Deployment_NodeApplicationS.h"
	-@del /f/q "Deployment_NodeApplicationC.cpp"
	-@del /f/q "Deployment_ApplicationC.inl"
	-@del /f/q "Deployment_ApplicationS.inl"
	-@del /f/q "Deployment_ApplicationC.h"
	-@del /f/q "Deployment_ApplicationS.h"
	-@del /f/q "Deployment_ApplicationC.cpp"
	-@del /f/q "Deployment_ContainerC.inl"
	-@del /f/q "Deployment_ContainerS.inl"
	-@del /f/q "Deployment_ContainerC.h"
	-@del /f/q "Deployment_ContainerS.h"
	-@del /f/q "Deployment_ContainerC.cpp"
	-@del /f/q "DeploymentC.inl"
	-@del /f/q "DeploymentS.inl"
	-@del /f/q "DeploymentC.h"
	-@del /f/q "DeploymentS.h"
	-@del /f/q "DeploymentC.cpp"
	-@del /f/q "Deployment_NodeManagerC.inl"
	-@del /f/q "Deployment_NodeManagerS.inl"
	-@del /f/q "Deployment_NodeManagerC.h"
	-@del /f/q "Deployment_NodeManagerS.h"
	-@del /f/q "Deployment_NodeManagerC.cpp"
	-@del /f/q "Deployment_DomainApplicationC.inl"
	-@del /f/q "Deployment_DomainApplicationS.inl"
	-@del /f/q "Deployment_DomainApplicationC.h"
	-@del /f/q "Deployment_DomainApplicationS.h"
	-@del /f/q "Deployment_DomainApplicationC.cpp"
	-@del /f/q "Deployment_ExecutionManagerC.inl"
	-@del /f/q "Deployment_ExecutionManagerS.inl"
	-@del /f/q "Deployment_ExecutionManagerC.h"
	-@del /f/q "Deployment_ExecutionManagerS.h"
	-@del /f/q "Deployment_ExecutionManagerC.cpp"
	-@del /f/q "Deployment_RepositoryManagerC.inl"
	-@del /f/q "Deployment_RepositoryManagerS.inl"
	-@del /f/q "Deployment_RepositoryManagerC.h"
	-@del /f/q "Deployment_RepositoryManagerS.h"
	-@del /f/q "Deployment_RepositoryManagerC.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\Deployment_stub\$(NULL)" mkdir "Static_Debug\Deployment_stub"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"..\..\..\..\lib\CIAO_Deployment_stubsd.pdb" /I "..\..\..\.." /I "..\..\.." /I "..\..\..\orbsvcs" /I "..\.." /I "..\..\ciao" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\lib\CIAO_Deployment_stubsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Deployment_CoreC.obj" \
	"$(INTDIR)\Deployment_BaseC.obj" \
	"$(INTDIR)\Deployment_ConnectionC.obj" \
	"$(INTDIR)\Deployment_DataC.obj" \
	"$(INTDIR)\Deployment_DeploymentPlanC.obj" \
	"$(INTDIR)\Deployment_TargetDataC.obj" \
	"$(INTDIR)\Deployment_NodeApplicationC.obj" \
	"$(INTDIR)\Deployment_ApplicationC.obj" \
	"$(INTDIR)\Deployment_ContainerC.obj" \
	"$(INTDIR)\Deployment_Packaging_DataC.obj" \
	"$(INTDIR)\Deployment_PlanErrorC.obj" \
	"$(INTDIR)\DeploymentC.obj" \
	"$(INTDIR)\Deployment_TargetManagerC.obj" \
	"$(INTDIR)\Deployment_NodeManagerC.obj" \
	"$(INTDIR)\Deployment_NodeApplicationManagerC.obj" \
	"$(INTDIR)\Deployment_ApplicationManagerC.obj" \
	"$(INTDIR)\Deployment_DomainApplicationManagerC.obj" \
	"$(INTDIR)\Deployment_DomainApplicationC.obj" \
	"$(INTDIR)\Deployment_ExecutionManagerC.obj" \
	"$(INTDIR)\Deployment_RepositoryManagerC.obj" \
	"$(INTDIR)\Deployment_ResourceCommitmentManagerC.obj" \
	"$(INTDIR)\CIAO_NodeApplication_CallBackC.obj" \
	"$(INTDIR)\CIAO_ServerResourcesC.obj" \
	"$(INTDIR)\Deployment_EventsC.obj"

"$(OUTDIR)\CIAO_Deployment_stubsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CIAO_Deployment_stubsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\CIAO_Deployment_stubsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\..\..\lib
INTDIR=Static_Release\Deployment_stub\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CIAO_Deployment_stubs.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\..\orbsvcs" -I"..\.." -I"..\..\ciao" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Deployment_stub.dep" "Deployment_CoreC.cpp" "Deployment_BaseC.cpp" "Deployment_ConnectionC.cpp" "Deployment_DataC.cpp" "Deployment_DeploymentPlanC.cpp" "Deployment_TargetDataC.cpp" "Deployment_NodeApplicationC.cpp" "Deployment_ApplicationC.cpp" "Deployment_ContainerC.cpp" "Deployment_Packaging_DataC.cpp" "Deployment_PlanErrorC.cpp" "DeploymentC.cpp" "Deployment_TargetManagerC.cpp" "Deployment_NodeManagerC.cpp" "Deployment_NodeApplicationManagerC.cpp" "Deployment_ApplicationManagerC.cpp" "Deployment_DomainApplicationManagerC.cpp" "Deployment_DomainApplicationC.cpp" "Deployment_ExecutionManagerC.cpp" "Deployment_RepositoryManagerC.cpp" "Deployment_ResourceCommitmentManagerC.cpp" "CIAO_NodeApplication_CallBackC.cpp" "CIAO_ServerResourcesC.cpp" "Deployment_EventsC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_stubs.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_stubs.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Deployment_stubs.ilk"
	-@del /f/q "Deployment_Packaging_DataC.h"
	-@del /f/q "Deployment_Packaging_DataS.h"
	-@del /f/q "Deployment_Packaging_DataC.cpp"
	-@del /f/q "Deployment_PlanErrorC.h"
	-@del /f/q "Deployment_PlanErrorS.h"
	-@del /f/q "Deployment_PlanErrorC.cpp"
	-@del /f/q "Deployment_BaseC.inl"
	-@del /f/q "Deployment_BaseC.h"
	-@del /f/q "Deployment_BaseS.h"
	-@del /f/q "Deployment_BaseC.cpp"
	-@del /f/q "Deployment_ConnectionC.inl"
	-@del /f/q "Deployment_ConnectionC.h"
	-@del /f/q "Deployment_ConnectionS.h"
	-@del /f/q "Deployment_ConnectionC.cpp"
	-@del /f/q "Deployment_DataC.inl"
	-@del /f/q "Deployment_DataC.h"
	-@del /f/q "Deployment_DataS.h"
	-@del /f/q "Deployment_DataC.cpp"
	-@del /f/q "Deployment_DeploymentPlanC.inl"
	-@del /f/q "Deployment_DeploymentPlanC.h"
	-@del /f/q "Deployment_DeploymentPlanS.h"
	-@del /f/q "Deployment_DeploymentPlanC.cpp"
	-@del /f/q "Deployment_TargetDataC.inl"
	-@del /f/q "Deployment_TargetDataC.h"
	-@del /f/q "Deployment_TargetDataS.h"
	-@del /f/q "Deployment_TargetDataC.cpp"
	-@del /f/q "Deployment_EventsC.inl"
	-@del /f/q "Deployment_EventsC.h"
	-@del /f/q "Deployment_EventsS.h"
	-@del /f/q "Deployment_EventsC.cpp"
	-@del /f/q "CIAO_ServerResourcesC.inl"
	-@del /f/q "CIAO_ServerResourcesC.h"
	-@del /f/q "CIAO_ServerResourcesS.h"
	-@del /f/q "CIAO_ServerResourcesC.cpp"
	-@del /f/q "Deployment_ApplicationManagerC.inl"
	-@del /f/q "Deployment_ApplicationManagerS.inl"
	-@del /f/q "Deployment_ApplicationManagerC.h"
	-@del /f/q "Deployment_ApplicationManagerS.h"
	-@del /f/q "Deployment_ApplicationManagerC.cpp"
	-@del /f/q "Deployment_NodeApplicationManagerC.inl"
	-@del /f/q "Deployment_NodeApplicationManagerS.inl"
	-@del /f/q "Deployment_NodeApplicationManagerC.h"
	-@del /f/q "Deployment_NodeApplicationManagerS.h"
	-@del /f/q "Deployment_NodeApplicationManagerC.cpp"
	-@del /f/q "Deployment_DomainApplicationManagerC.inl"
	-@del /f/q "Deployment_DomainApplicationManagerS.inl"
	-@del /f/q "Deployment_DomainApplicationManagerC.h"
	-@del /f/q "Deployment_DomainApplicationManagerS.h"
	-@del /f/q "Deployment_DomainApplicationManagerC.cpp"
	-@del /f/q "Deployment_ResourceCommitmentManagerC.inl"
	-@del /f/q "Deployment_ResourceCommitmentManagerS.inl"
	-@del /f/q "Deployment_ResourceCommitmentManagerC.h"
	-@del /f/q "Deployment_ResourceCommitmentManagerS.h"
	-@del /f/q "Deployment_ResourceCommitmentManagerC.cpp"
	-@del /f/q "Deployment_TargetManagerC.inl"
	-@del /f/q "Deployment_TargetManagerS.inl"
	-@del /f/q "Deployment_TargetManagerC.h"
	-@del /f/q "Deployment_TargetManagerS.h"
	-@del /f/q "Deployment_TargetManagerC.cpp"
	-@del /f/q "CIAO_NodeApplication_CallBackC.inl"
	-@del /f/q "CIAO_NodeApplication_CallBackS.inl"
	-@del /f/q "CIAO_NodeApplication_CallBackC.h"
	-@del /f/q "CIAO_NodeApplication_CallBackS.h"
	-@del /f/q "CIAO_NodeApplication_CallBackC.cpp"
	-@del /f/q "Deployment_CoreC.inl"
	-@del /f/q "Deployment_CoreS.inl"
	-@del /f/q "Deployment_CoreC.h"
	-@del /f/q "Deployment_CoreS.h"
	-@del /f/q "Deployment_CoreC.cpp"
	-@del /f/q "Deployment_NodeApplicationC.inl"
	-@del /f/q "Deployment_NodeApplicationS.inl"
	-@del /f/q "Deployment_NodeApplicationC.h"
	-@del /f/q "Deployment_NodeApplicationS.h"
	-@del /f/q "Deployment_NodeApplicationC.cpp"
	-@del /f/q "Deployment_ApplicationC.inl"
	-@del /f/q "Deployment_ApplicationS.inl"
	-@del /f/q "Deployment_ApplicationC.h"
	-@del /f/q "Deployment_ApplicationS.h"
	-@del /f/q "Deployment_ApplicationC.cpp"
	-@del /f/q "Deployment_ContainerC.inl"
	-@del /f/q "Deployment_ContainerS.inl"
	-@del /f/q "Deployment_ContainerC.h"
	-@del /f/q "Deployment_ContainerS.h"
	-@del /f/q "Deployment_ContainerC.cpp"
	-@del /f/q "DeploymentC.inl"
	-@del /f/q "DeploymentS.inl"
	-@del /f/q "DeploymentC.h"
	-@del /f/q "DeploymentS.h"
	-@del /f/q "DeploymentC.cpp"
	-@del /f/q "Deployment_NodeManagerC.inl"
	-@del /f/q "Deployment_NodeManagerS.inl"
	-@del /f/q "Deployment_NodeManagerC.h"
	-@del /f/q "Deployment_NodeManagerS.h"
	-@del /f/q "Deployment_NodeManagerC.cpp"
	-@del /f/q "Deployment_DomainApplicationC.inl"
	-@del /f/q "Deployment_DomainApplicationS.inl"
	-@del /f/q "Deployment_DomainApplicationC.h"
	-@del /f/q "Deployment_DomainApplicationS.h"
	-@del /f/q "Deployment_DomainApplicationC.cpp"
	-@del /f/q "Deployment_ExecutionManagerC.inl"
	-@del /f/q "Deployment_ExecutionManagerS.inl"
	-@del /f/q "Deployment_ExecutionManagerC.h"
	-@del /f/q "Deployment_ExecutionManagerS.h"
	-@del /f/q "Deployment_ExecutionManagerC.cpp"
	-@del /f/q "Deployment_RepositoryManagerC.inl"
	-@del /f/q "Deployment_RepositoryManagerS.inl"
	-@del /f/q "Deployment_RepositoryManagerC.h"
	-@del /f/q "Deployment_RepositoryManagerS.h"
	-@del /f/q "Deployment_RepositoryManagerC.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\Deployment_stub\$(NULL)" mkdir "Static_Release\Deployment_stub"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\.." /I "..\..\.." /I "..\..\..\orbsvcs" /I "..\.." /I "..\..\ciao" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\lib\CIAO_Deployment_stubs.lib"
LINK32_OBJS= \
	"$(INTDIR)\Deployment_CoreC.obj" \
	"$(INTDIR)\Deployment_BaseC.obj" \
	"$(INTDIR)\Deployment_ConnectionC.obj" \
	"$(INTDIR)\Deployment_DataC.obj" \
	"$(INTDIR)\Deployment_DeploymentPlanC.obj" \
	"$(INTDIR)\Deployment_TargetDataC.obj" \
	"$(INTDIR)\Deployment_NodeApplicationC.obj" \
	"$(INTDIR)\Deployment_ApplicationC.obj" \
	"$(INTDIR)\Deployment_ContainerC.obj" \
	"$(INTDIR)\Deployment_Packaging_DataC.obj" \
	"$(INTDIR)\Deployment_PlanErrorC.obj" \
	"$(INTDIR)\DeploymentC.obj" \
	"$(INTDIR)\Deployment_TargetManagerC.obj" \
	"$(INTDIR)\Deployment_NodeManagerC.obj" \
	"$(INTDIR)\Deployment_NodeApplicationManagerC.obj" \
	"$(INTDIR)\Deployment_ApplicationManagerC.obj" \
	"$(INTDIR)\Deployment_DomainApplicationManagerC.obj" \
	"$(INTDIR)\Deployment_DomainApplicationC.obj" \
	"$(INTDIR)\Deployment_ExecutionManagerC.obj" \
	"$(INTDIR)\Deployment_RepositoryManagerC.obj" \
	"$(INTDIR)\Deployment_ResourceCommitmentManagerC.obj" \
	"$(INTDIR)\CIAO_NodeApplication_CallBackC.obj" \
	"$(INTDIR)\CIAO_ServerResourcesC.obj" \
	"$(INTDIR)\Deployment_EventsC.obj"

"$(OUTDIR)\CIAO_Deployment_stubs.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CIAO_Deployment_stubs.lib.manifest" mt.exe -manifest "$(OUTDIR)\CIAO_Deployment_stubs.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Deployment_stub.dep")
!INCLUDE "Makefile.Deployment_stub.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Deployment_CoreC.cpp"

"$(INTDIR)\Deployment_CoreC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_CoreC.obj" $(SOURCE)

SOURCE="Deployment_BaseC.cpp"

"$(INTDIR)\Deployment_BaseC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_BaseC.obj" $(SOURCE)

SOURCE="Deployment_ConnectionC.cpp"

"$(INTDIR)\Deployment_ConnectionC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_ConnectionC.obj" $(SOURCE)

SOURCE="Deployment_DataC.cpp"

"$(INTDIR)\Deployment_DataC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_DataC.obj" $(SOURCE)

SOURCE="Deployment_DeploymentPlanC.cpp"

"$(INTDIR)\Deployment_DeploymentPlanC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_DeploymentPlanC.obj" $(SOURCE)

SOURCE="Deployment_TargetDataC.cpp"

"$(INTDIR)\Deployment_TargetDataC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_TargetDataC.obj" $(SOURCE)

SOURCE="Deployment_NodeApplicationC.cpp"

"$(INTDIR)\Deployment_NodeApplicationC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_NodeApplicationC.obj" $(SOURCE)

SOURCE="Deployment_ApplicationC.cpp"

"$(INTDIR)\Deployment_ApplicationC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_ApplicationC.obj" $(SOURCE)

SOURCE="Deployment_ContainerC.cpp"

"$(INTDIR)\Deployment_ContainerC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_ContainerC.obj" $(SOURCE)

SOURCE="Deployment_Packaging_DataC.cpp"

"$(INTDIR)\Deployment_Packaging_DataC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_Packaging_DataC.obj" $(SOURCE)

SOURCE="Deployment_PlanErrorC.cpp"

"$(INTDIR)\Deployment_PlanErrorC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_PlanErrorC.obj" $(SOURCE)

SOURCE="DeploymentC.cpp"

"$(INTDIR)\DeploymentC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DeploymentC.obj" $(SOURCE)

SOURCE="Deployment_TargetManagerC.cpp"

"$(INTDIR)\Deployment_TargetManagerC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_TargetManagerC.obj" $(SOURCE)

SOURCE="Deployment_NodeManagerC.cpp"

"$(INTDIR)\Deployment_NodeManagerC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_NodeManagerC.obj" $(SOURCE)

SOURCE="Deployment_NodeApplicationManagerC.cpp"

"$(INTDIR)\Deployment_NodeApplicationManagerC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_NodeApplicationManagerC.obj" $(SOURCE)

SOURCE="Deployment_ApplicationManagerC.cpp"

"$(INTDIR)\Deployment_ApplicationManagerC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_ApplicationManagerC.obj" $(SOURCE)

SOURCE="Deployment_DomainApplicationManagerC.cpp"

"$(INTDIR)\Deployment_DomainApplicationManagerC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_DomainApplicationManagerC.obj" $(SOURCE)

SOURCE="Deployment_DomainApplicationC.cpp"

"$(INTDIR)\Deployment_DomainApplicationC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_DomainApplicationC.obj" $(SOURCE)

SOURCE="Deployment_ExecutionManagerC.cpp"

"$(INTDIR)\Deployment_ExecutionManagerC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_ExecutionManagerC.obj" $(SOURCE)

SOURCE="Deployment_RepositoryManagerC.cpp"

"$(INTDIR)\Deployment_RepositoryManagerC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_RepositoryManagerC.obj" $(SOURCE)

SOURCE="Deployment_ResourceCommitmentManagerC.cpp"

"$(INTDIR)\Deployment_ResourceCommitmentManagerC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_ResourceCommitmentManagerC.obj" $(SOURCE)

SOURCE="CIAO_NodeApplication_CallBackC.cpp"

"$(INTDIR)\CIAO_NodeApplication_CallBackC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CIAO_NodeApplication_CallBackC.obj" $(SOURCE)

SOURCE="CIAO_ServerResourcesC.cpp"

"$(INTDIR)\CIAO_ServerResourcesC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CIAO_ServerResourcesC.obj" $(SOURCE)

SOURCE="Deployment_EventsC.cpp"

"$(INTDIR)\Deployment_EventsC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_EventsC.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="Deployment_Packaging_Data.idl"

InputPath=Deployment_Packaging_Data.idl

"Deployment_Packaging_DataC.h" "Deployment_Packaging_DataS.h" "Deployment_Packaging_DataC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Deployment_Packaging_Data_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS -Sci "$(InputPath)"
<<

SOURCE="Deployment_PlanError.idl"

InputPath=Deployment_PlanError.idl

"Deployment_PlanErrorC.h" "Deployment_PlanErrorS.h" "Deployment_PlanErrorC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Deployment_PlanError_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS -Sci "$(InputPath)"
<<

SOURCE="Deployment_Base.idl"

InputPath=Deployment_Base.idl

"Deployment_BaseC.inl" "Deployment_BaseC.h" "Deployment_BaseS.h" "Deployment_BaseC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Deployment_Base_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="Deployment_Connection.idl"

InputPath=Deployment_Connection.idl

"Deployment_ConnectionC.inl" "Deployment_ConnectionC.h" "Deployment_ConnectionS.h" "Deployment_ConnectionC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Deployment_Connection_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="Deployment_Data.idl"

InputPath=Deployment_Data.idl

"Deployment_DataC.inl" "Deployment_DataC.h" "Deployment_DataS.h" "Deployment_DataC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Deployment_Data_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="Deployment_DeploymentPlan.idl"

InputPath=Deployment_DeploymentPlan.idl

"Deployment_DeploymentPlanC.inl" "Deployment_DeploymentPlanC.h" "Deployment_DeploymentPlanS.h" "Deployment_DeploymentPlanC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Deployment_DeploymentPlan_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="Deployment_TargetData.idl"

InputPath=Deployment_TargetData.idl

"Deployment_TargetDataC.inl" "Deployment_TargetDataC.h" "Deployment_TargetDataS.h" "Deployment_TargetDataC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Deployment_TargetData_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="Deployment_Events.idl"

InputPath=Deployment_Events.idl

"Deployment_EventsC.inl" "Deployment_EventsC.h" "Deployment_EventsS.h" "Deployment_EventsC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Deployment_Events_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="CIAO_ServerResources.idl"

InputPath=CIAO_ServerResources.idl

"CIAO_ServerResourcesC.inl" "CIAO_ServerResourcesC.h" "CIAO_ServerResourcesS.h" "CIAO_ServerResourcesC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CIAO_ServerResources_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="Deployment_ApplicationManager.idl"

InputPath=Deployment_ApplicationManager.idl

"Deployment_ApplicationManagerC.inl" "Deployment_ApplicationManagerS.inl" "Deployment_ApplicationManagerC.h" "Deployment_ApplicationManagerS.h" "Deployment_ApplicationManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Deployment_ApplicationManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -GC "$(InputPath)"
<<

SOURCE="Deployment_NodeApplicationManager.idl"

InputPath=Deployment_NodeApplicationManager.idl

"Deployment_NodeApplicationManagerC.inl" "Deployment_NodeApplicationManagerS.inl" "Deployment_NodeApplicationManagerC.h" "Deployment_NodeApplicationManagerS.h" "Deployment_NodeApplicationManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Deployment_NodeApplicationManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -GC "$(InputPath)"
<<

SOURCE="Deployment_DomainApplicationManager.idl"

InputPath=Deployment_DomainApplicationManager.idl

"Deployment_DomainApplicationManagerC.inl" "Deployment_DomainApplicationManagerS.inl" "Deployment_DomainApplicationManagerC.h" "Deployment_DomainApplicationManagerS.h" "Deployment_DomainApplicationManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Deployment_DomainApplicationManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -GH "$(InputPath)"
<<

SOURCE="Deployment_ResourceCommitmentManager.idl"

InputPath=Deployment_ResourceCommitmentManager.idl

"Deployment_ResourceCommitmentManagerC.inl" "Deployment_ResourceCommitmentManagerS.inl" "Deployment_ResourceCommitmentManagerC.h" "Deployment_ResourceCommitmentManagerS.h" "Deployment_ResourceCommitmentManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Deployment_ResourceCommitmentManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_TargetManager.idl"

InputPath=Deployment_TargetManager.idl

"Deployment_TargetManagerC.inl" "Deployment_TargetManagerS.inl" "Deployment_TargetManagerC.h" "Deployment_TargetManagerS.h" "Deployment_TargetManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Deployment_TargetManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="CIAO_NodeApplication_CallBack.idl"

InputPath=CIAO_NodeApplication_CallBack.idl

"CIAO_NodeApplication_CallBackC.inl" "CIAO_NodeApplication_CallBackS.inl" "CIAO_NodeApplication_CallBackC.h" "CIAO_NodeApplication_CallBackS.h" "CIAO_NodeApplication_CallBackC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CIAO_NodeApplication_CallBack_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_Core.idl"

InputPath=Deployment_Core.idl

"Deployment_CoreC.inl" "Deployment_CoreS.inl" "Deployment_CoreC.h" "Deployment_CoreS.h" "Deployment_CoreC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Deployment_Core_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_NodeApplication.idl"

InputPath=Deployment_NodeApplication.idl

"Deployment_NodeApplicationC.inl" "Deployment_NodeApplicationS.inl" "Deployment_NodeApplicationC.h" "Deployment_NodeApplicationS.h" "Deployment_NodeApplicationC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Deployment_NodeApplication_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_Application.idl"

InputPath=Deployment_Application.idl

"Deployment_ApplicationC.inl" "Deployment_ApplicationS.inl" "Deployment_ApplicationC.h" "Deployment_ApplicationS.h" "Deployment_ApplicationC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Deployment_Application_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_Container.idl"

InputPath=Deployment_Container.idl

"Deployment_ContainerC.inl" "Deployment_ContainerS.inl" "Deployment_ContainerC.h" "Deployment_ContainerS.h" "Deployment_ContainerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Deployment_Container_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment.idl"

InputPath=Deployment.idl

"DeploymentC.inl" "DeploymentS.inl" "DeploymentC.h" "DeploymentS.h" "DeploymentC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Deployment_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_NodeManager.idl"

InputPath=Deployment_NodeManager.idl

"Deployment_NodeManagerC.inl" "Deployment_NodeManagerS.inl" "Deployment_NodeManagerC.h" "Deployment_NodeManagerS.h" "Deployment_NodeManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Deployment_NodeManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_DomainApplication.idl"

InputPath=Deployment_DomainApplication.idl

"Deployment_DomainApplicationC.inl" "Deployment_DomainApplicationS.inl" "Deployment_DomainApplicationC.h" "Deployment_DomainApplicationS.h" "Deployment_DomainApplicationC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Deployment_DomainApplication_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_ExecutionManager.idl"

InputPath=Deployment_ExecutionManager.idl

"Deployment_ExecutionManagerC.inl" "Deployment_ExecutionManagerS.inl" "Deployment_ExecutionManagerC.h" "Deployment_ExecutionManagerS.h" "Deployment_ExecutionManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Deployment_ExecutionManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_RepositoryManager.idl"

InputPath=Deployment_RepositoryManager.idl

"Deployment_RepositoryManagerC.inl" "Deployment_RepositoryManagerS.inl" "Deployment_RepositoryManagerC.h" "Deployment_RepositoryManagerS.h" "Deployment_RepositoryManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Deployment_RepositoryManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="Deployment_Packaging_Data.idl"

InputPath=Deployment_Packaging_Data.idl

"Deployment_Packaging_DataC.h" "Deployment_Packaging_DataS.h" "Deployment_Packaging_DataC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Deployment_Packaging_Data_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS -Sci "$(InputPath)"
<<

SOURCE="Deployment_PlanError.idl"

InputPath=Deployment_PlanError.idl

"Deployment_PlanErrorC.h" "Deployment_PlanErrorS.h" "Deployment_PlanErrorC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Deployment_PlanError_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS -Sci "$(InputPath)"
<<

SOURCE="Deployment_Base.idl"

InputPath=Deployment_Base.idl

"Deployment_BaseC.inl" "Deployment_BaseC.h" "Deployment_BaseS.h" "Deployment_BaseC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Deployment_Base_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="Deployment_Connection.idl"

InputPath=Deployment_Connection.idl

"Deployment_ConnectionC.inl" "Deployment_ConnectionC.h" "Deployment_ConnectionS.h" "Deployment_ConnectionC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Deployment_Connection_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="Deployment_Data.idl"

InputPath=Deployment_Data.idl

"Deployment_DataC.inl" "Deployment_DataC.h" "Deployment_DataS.h" "Deployment_DataC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Deployment_Data_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="Deployment_DeploymentPlan.idl"

InputPath=Deployment_DeploymentPlan.idl

"Deployment_DeploymentPlanC.inl" "Deployment_DeploymentPlanC.h" "Deployment_DeploymentPlanS.h" "Deployment_DeploymentPlanC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Deployment_DeploymentPlan_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="Deployment_TargetData.idl"

InputPath=Deployment_TargetData.idl

"Deployment_TargetDataC.inl" "Deployment_TargetDataC.h" "Deployment_TargetDataS.h" "Deployment_TargetDataC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Deployment_TargetData_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="Deployment_Events.idl"

InputPath=Deployment_Events.idl

"Deployment_EventsC.inl" "Deployment_EventsC.h" "Deployment_EventsS.h" "Deployment_EventsC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Deployment_Events_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="CIAO_ServerResources.idl"

InputPath=CIAO_ServerResources.idl

"CIAO_ServerResourcesC.inl" "CIAO_ServerResourcesC.h" "CIAO_ServerResourcesS.h" "CIAO_ServerResourcesC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CIAO_ServerResources_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="Deployment_ApplicationManager.idl"

InputPath=Deployment_ApplicationManager.idl

"Deployment_ApplicationManagerC.inl" "Deployment_ApplicationManagerS.inl" "Deployment_ApplicationManagerC.h" "Deployment_ApplicationManagerS.h" "Deployment_ApplicationManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Deployment_ApplicationManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -GC "$(InputPath)"
<<

SOURCE="Deployment_NodeApplicationManager.idl"

InputPath=Deployment_NodeApplicationManager.idl

"Deployment_NodeApplicationManagerC.inl" "Deployment_NodeApplicationManagerS.inl" "Deployment_NodeApplicationManagerC.h" "Deployment_NodeApplicationManagerS.h" "Deployment_NodeApplicationManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Deployment_NodeApplicationManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -GC "$(InputPath)"
<<

SOURCE="Deployment_DomainApplicationManager.idl"

InputPath=Deployment_DomainApplicationManager.idl

"Deployment_DomainApplicationManagerC.inl" "Deployment_DomainApplicationManagerS.inl" "Deployment_DomainApplicationManagerC.h" "Deployment_DomainApplicationManagerS.h" "Deployment_DomainApplicationManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Deployment_DomainApplicationManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -GH "$(InputPath)"
<<

SOURCE="Deployment_ResourceCommitmentManager.idl"

InputPath=Deployment_ResourceCommitmentManager.idl

"Deployment_ResourceCommitmentManagerC.inl" "Deployment_ResourceCommitmentManagerS.inl" "Deployment_ResourceCommitmentManagerC.h" "Deployment_ResourceCommitmentManagerS.h" "Deployment_ResourceCommitmentManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Deployment_ResourceCommitmentManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_TargetManager.idl"

InputPath=Deployment_TargetManager.idl

"Deployment_TargetManagerC.inl" "Deployment_TargetManagerS.inl" "Deployment_TargetManagerC.h" "Deployment_TargetManagerS.h" "Deployment_TargetManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Deployment_TargetManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="CIAO_NodeApplication_CallBack.idl"

InputPath=CIAO_NodeApplication_CallBack.idl

"CIAO_NodeApplication_CallBackC.inl" "CIAO_NodeApplication_CallBackS.inl" "CIAO_NodeApplication_CallBackC.h" "CIAO_NodeApplication_CallBackS.h" "CIAO_NodeApplication_CallBackC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CIAO_NodeApplication_CallBack_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_Core.idl"

InputPath=Deployment_Core.idl

"Deployment_CoreC.inl" "Deployment_CoreS.inl" "Deployment_CoreC.h" "Deployment_CoreS.h" "Deployment_CoreC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Deployment_Core_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_NodeApplication.idl"

InputPath=Deployment_NodeApplication.idl

"Deployment_NodeApplicationC.inl" "Deployment_NodeApplicationS.inl" "Deployment_NodeApplicationC.h" "Deployment_NodeApplicationS.h" "Deployment_NodeApplicationC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Deployment_NodeApplication_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_Application.idl"

InputPath=Deployment_Application.idl

"Deployment_ApplicationC.inl" "Deployment_ApplicationS.inl" "Deployment_ApplicationC.h" "Deployment_ApplicationS.h" "Deployment_ApplicationC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Deployment_Application_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_Container.idl"

InputPath=Deployment_Container.idl

"Deployment_ContainerC.inl" "Deployment_ContainerS.inl" "Deployment_ContainerC.h" "Deployment_ContainerS.h" "Deployment_ContainerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Deployment_Container_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment.idl"

InputPath=Deployment.idl

"DeploymentC.inl" "DeploymentS.inl" "DeploymentC.h" "DeploymentS.h" "DeploymentC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Deployment_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_NodeManager.idl"

InputPath=Deployment_NodeManager.idl

"Deployment_NodeManagerC.inl" "Deployment_NodeManagerS.inl" "Deployment_NodeManagerC.h" "Deployment_NodeManagerS.h" "Deployment_NodeManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Deployment_NodeManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_DomainApplication.idl"

InputPath=Deployment_DomainApplication.idl

"Deployment_DomainApplicationC.inl" "Deployment_DomainApplicationS.inl" "Deployment_DomainApplicationC.h" "Deployment_DomainApplicationS.h" "Deployment_DomainApplicationC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Deployment_DomainApplication_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_ExecutionManager.idl"

InputPath=Deployment_ExecutionManager.idl

"Deployment_ExecutionManagerC.inl" "Deployment_ExecutionManagerS.inl" "Deployment_ExecutionManagerC.h" "Deployment_ExecutionManagerS.h" "Deployment_ExecutionManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Deployment_ExecutionManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_RepositoryManager.idl"

InputPath=Deployment_RepositoryManager.idl

"Deployment_RepositoryManagerC.inl" "Deployment_RepositoryManagerS.inl" "Deployment_RepositoryManagerC.h" "Deployment_RepositoryManagerS.h" "Deployment_RepositoryManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Deployment_RepositoryManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="Deployment_Packaging_Data.idl"

InputPath=Deployment_Packaging_Data.idl

"Deployment_Packaging_DataC.h" "Deployment_Packaging_DataS.h" "Deployment_Packaging_DataC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Deployment_Packaging_Data_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS -Sci "$(InputPath)"
<<

SOURCE="Deployment_PlanError.idl"

InputPath=Deployment_PlanError.idl

"Deployment_PlanErrorC.h" "Deployment_PlanErrorS.h" "Deployment_PlanErrorC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Deployment_PlanError_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS -Sci "$(InputPath)"
<<

SOURCE="Deployment_Base.idl"

InputPath=Deployment_Base.idl

"Deployment_BaseC.inl" "Deployment_BaseC.h" "Deployment_BaseS.h" "Deployment_BaseC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Deployment_Base_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="Deployment_Connection.idl"

InputPath=Deployment_Connection.idl

"Deployment_ConnectionC.inl" "Deployment_ConnectionC.h" "Deployment_ConnectionS.h" "Deployment_ConnectionC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Deployment_Connection_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="Deployment_Data.idl"

InputPath=Deployment_Data.idl

"Deployment_DataC.inl" "Deployment_DataC.h" "Deployment_DataS.h" "Deployment_DataC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Deployment_Data_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="Deployment_DeploymentPlan.idl"

InputPath=Deployment_DeploymentPlan.idl

"Deployment_DeploymentPlanC.inl" "Deployment_DeploymentPlanC.h" "Deployment_DeploymentPlanS.h" "Deployment_DeploymentPlanC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Deployment_DeploymentPlan_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="Deployment_TargetData.idl"

InputPath=Deployment_TargetData.idl

"Deployment_TargetDataC.inl" "Deployment_TargetDataC.h" "Deployment_TargetDataS.h" "Deployment_TargetDataC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Deployment_TargetData_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="Deployment_Events.idl"

InputPath=Deployment_Events.idl

"Deployment_EventsC.inl" "Deployment_EventsC.h" "Deployment_EventsS.h" "Deployment_EventsC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Deployment_Events_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="CIAO_ServerResources.idl"

InputPath=CIAO_ServerResources.idl

"CIAO_ServerResourcesC.inl" "CIAO_ServerResourcesC.h" "CIAO_ServerResourcesS.h" "CIAO_ServerResourcesC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CIAO_ServerResources_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="Deployment_ApplicationManager.idl"

InputPath=Deployment_ApplicationManager.idl

"Deployment_ApplicationManagerC.inl" "Deployment_ApplicationManagerS.inl" "Deployment_ApplicationManagerC.h" "Deployment_ApplicationManagerS.h" "Deployment_ApplicationManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Deployment_ApplicationManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -GC "$(InputPath)"
<<

SOURCE="Deployment_NodeApplicationManager.idl"

InputPath=Deployment_NodeApplicationManager.idl

"Deployment_NodeApplicationManagerC.inl" "Deployment_NodeApplicationManagerS.inl" "Deployment_NodeApplicationManagerC.h" "Deployment_NodeApplicationManagerS.h" "Deployment_NodeApplicationManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Deployment_NodeApplicationManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -GC "$(InputPath)"
<<

SOURCE="Deployment_DomainApplicationManager.idl"

InputPath=Deployment_DomainApplicationManager.idl

"Deployment_DomainApplicationManagerC.inl" "Deployment_DomainApplicationManagerS.inl" "Deployment_DomainApplicationManagerC.h" "Deployment_DomainApplicationManagerS.h" "Deployment_DomainApplicationManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Deployment_DomainApplicationManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -GH "$(InputPath)"
<<

SOURCE="Deployment_ResourceCommitmentManager.idl"

InputPath=Deployment_ResourceCommitmentManager.idl

"Deployment_ResourceCommitmentManagerC.inl" "Deployment_ResourceCommitmentManagerS.inl" "Deployment_ResourceCommitmentManagerC.h" "Deployment_ResourceCommitmentManagerS.h" "Deployment_ResourceCommitmentManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Deployment_ResourceCommitmentManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_TargetManager.idl"

InputPath=Deployment_TargetManager.idl

"Deployment_TargetManagerC.inl" "Deployment_TargetManagerS.inl" "Deployment_TargetManagerC.h" "Deployment_TargetManagerS.h" "Deployment_TargetManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Deployment_TargetManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="CIAO_NodeApplication_CallBack.idl"

InputPath=CIAO_NodeApplication_CallBack.idl

"CIAO_NodeApplication_CallBackC.inl" "CIAO_NodeApplication_CallBackS.inl" "CIAO_NodeApplication_CallBackC.h" "CIAO_NodeApplication_CallBackS.h" "CIAO_NodeApplication_CallBackC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CIAO_NodeApplication_CallBack_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_Core.idl"

InputPath=Deployment_Core.idl

"Deployment_CoreC.inl" "Deployment_CoreS.inl" "Deployment_CoreC.h" "Deployment_CoreS.h" "Deployment_CoreC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Deployment_Core_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_NodeApplication.idl"

InputPath=Deployment_NodeApplication.idl

"Deployment_NodeApplicationC.inl" "Deployment_NodeApplicationS.inl" "Deployment_NodeApplicationC.h" "Deployment_NodeApplicationS.h" "Deployment_NodeApplicationC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Deployment_NodeApplication_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_Application.idl"

InputPath=Deployment_Application.idl

"Deployment_ApplicationC.inl" "Deployment_ApplicationS.inl" "Deployment_ApplicationC.h" "Deployment_ApplicationS.h" "Deployment_ApplicationC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Deployment_Application_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_Container.idl"

InputPath=Deployment_Container.idl

"Deployment_ContainerC.inl" "Deployment_ContainerS.inl" "Deployment_ContainerC.h" "Deployment_ContainerS.h" "Deployment_ContainerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Deployment_Container_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment.idl"

InputPath=Deployment.idl

"DeploymentC.inl" "DeploymentS.inl" "DeploymentC.h" "DeploymentS.h" "DeploymentC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Deployment_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_NodeManager.idl"

InputPath=Deployment_NodeManager.idl

"Deployment_NodeManagerC.inl" "Deployment_NodeManagerS.inl" "Deployment_NodeManagerC.h" "Deployment_NodeManagerS.h" "Deployment_NodeManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Deployment_NodeManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_DomainApplication.idl"

InputPath=Deployment_DomainApplication.idl

"Deployment_DomainApplicationC.inl" "Deployment_DomainApplicationS.inl" "Deployment_DomainApplicationC.h" "Deployment_DomainApplicationS.h" "Deployment_DomainApplicationC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Deployment_DomainApplication_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_ExecutionManager.idl"

InputPath=Deployment_ExecutionManager.idl

"Deployment_ExecutionManagerC.inl" "Deployment_ExecutionManagerS.inl" "Deployment_ExecutionManagerC.h" "Deployment_ExecutionManagerS.h" "Deployment_ExecutionManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Deployment_ExecutionManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_RepositoryManager.idl"

InputPath=Deployment_RepositoryManager.idl

"Deployment_RepositoryManagerC.inl" "Deployment_RepositoryManagerS.inl" "Deployment_RepositoryManagerC.h" "Deployment_RepositoryManagerS.h" "Deployment_RepositoryManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Deployment_RepositoryManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="Deployment_Packaging_Data.idl"

InputPath=Deployment_Packaging_Data.idl

"Deployment_Packaging_DataC.h" "Deployment_Packaging_DataS.h" "Deployment_Packaging_DataC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Deployment_Packaging_Data_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS -Sci "$(InputPath)"
<<

SOURCE="Deployment_PlanError.idl"

InputPath=Deployment_PlanError.idl

"Deployment_PlanErrorC.h" "Deployment_PlanErrorS.h" "Deployment_PlanErrorC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Deployment_PlanError_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS -Sci "$(InputPath)"
<<

SOURCE="Deployment_Base.idl"

InputPath=Deployment_Base.idl

"Deployment_BaseC.inl" "Deployment_BaseC.h" "Deployment_BaseS.h" "Deployment_BaseC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Deployment_Base_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="Deployment_Connection.idl"

InputPath=Deployment_Connection.idl

"Deployment_ConnectionC.inl" "Deployment_ConnectionC.h" "Deployment_ConnectionS.h" "Deployment_ConnectionC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Deployment_Connection_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="Deployment_Data.idl"

InputPath=Deployment_Data.idl

"Deployment_DataC.inl" "Deployment_DataC.h" "Deployment_DataS.h" "Deployment_DataC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Deployment_Data_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="Deployment_DeploymentPlan.idl"

InputPath=Deployment_DeploymentPlan.idl

"Deployment_DeploymentPlanC.inl" "Deployment_DeploymentPlanC.h" "Deployment_DeploymentPlanS.h" "Deployment_DeploymentPlanC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Deployment_DeploymentPlan_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="Deployment_TargetData.idl"

InputPath=Deployment_TargetData.idl

"Deployment_TargetDataC.inl" "Deployment_TargetDataC.h" "Deployment_TargetDataS.h" "Deployment_TargetDataC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Deployment_TargetData_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="Deployment_Events.idl"

InputPath=Deployment_Events.idl

"Deployment_EventsC.inl" "Deployment_EventsC.h" "Deployment_EventsS.h" "Deployment_EventsC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Deployment_Events_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="CIAO_ServerResources.idl"

InputPath=CIAO_ServerResources.idl

"CIAO_ServerResourcesC.inl" "CIAO_ServerResourcesC.h" "CIAO_ServerResourcesS.h" "CIAO_ServerResourcesC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CIAO_ServerResources_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -SS "$(InputPath)"
<<

SOURCE="Deployment_ApplicationManager.idl"

InputPath=Deployment_ApplicationManager.idl

"Deployment_ApplicationManagerC.inl" "Deployment_ApplicationManagerS.inl" "Deployment_ApplicationManagerC.h" "Deployment_ApplicationManagerS.h" "Deployment_ApplicationManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Deployment_ApplicationManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -GC "$(InputPath)"
<<

SOURCE="Deployment_NodeApplicationManager.idl"

InputPath=Deployment_NodeApplicationManager.idl

"Deployment_NodeApplicationManagerC.inl" "Deployment_NodeApplicationManagerS.inl" "Deployment_NodeApplicationManagerC.h" "Deployment_NodeApplicationManagerS.h" "Deployment_NodeApplicationManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Deployment_NodeApplicationManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -GC "$(InputPath)"
<<

SOURCE="Deployment_DomainApplicationManager.idl"

InputPath=Deployment_DomainApplicationManager.idl

"Deployment_DomainApplicationManagerC.inl" "Deployment_DomainApplicationManagerS.inl" "Deployment_DomainApplicationManagerC.h" "Deployment_DomainApplicationManagerS.h" "Deployment_DomainApplicationManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Deployment_DomainApplicationManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h -GH "$(InputPath)"
<<

SOURCE="Deployment_ResourceCommitmentManager.idl"

InputPath=Deployment_ResourceCommitmentManager.idl

"Deployment_ResourceCommitmentManagerC.inl" "Deployment_ResourceCommitmentManagerS.inl" "Deployment_ResourceCommitmentManagerC.h" "Deployment_ResourceCommitmentManagerS.h" "Deployment_ResourceCommitmentManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Deployment_ResourceCommitmentManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_TargetManager.idl"

InputPath=Deployment_TargetManager.idl

"Deployment_TargetManagerC.inl" "Deployment_TargetManagerS.inl" "Deployment_TargetManagerC.h" "Deployment_TargetManagerS.h" "Deployment_TargetManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Deployment_TargetManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="CIAO_NodeApplication_CallBack.idl"

InputPath=CIAO_NodeApplication_CallBack.idl

"CIAO_NodeApplication_CallBackC.inl" "CIAO_NodeApplication_CallBackS.inl" "CIAO_NodeApplication_CallBackC.h" "CIAO_NodeApplication_CallBackS.h" "CIAO_NodeApplication_CallBackC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CIAO_NodeApplication_CallBack_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_Core.idl"

InputPath=Deployment_Core.idl

"Deployment_CoreC.inl" "Deployment_CoreS.inl" "Deployment_CoreC.h" "Deployment_CoreS.h" "Deployment_CoreC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Deployment_Core_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_NodeApplication.idl"

InputPath=Deployment_NodeApplication.idl

"Deployment_NodeApplicationC.inl" "Deployment_NodeApplicationS.inl" "Deployment_NodeApplicationC.h" "Deployment_NodeApplicationS.h" "Deployment_NodeApplicationC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Deployment_NodeApplication_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_Application.idl"

InputPath=Deployment_Application.idl

"Deployment_ApplicationC.inl" "Deployment_ApplicationS.inl" "Deployment_ApplicationC.h" "Deployment_ApplicationS.h" "Deployment_ApplicationC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Deployment_Application_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_Container.idl"

InputPath=Deployment_Container.idl

"Deployment_ContainerC.inl" "Deployment_ContainerS.inl" "Deployment_ContainerC.h" "Deployment_ContainerS.h" "Deployment_ContainerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Deployment_Container_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment.idl"

InputPath=Deployment.idl

"DeploymentC.inl" "DeploymentS.inl" "DeploymentC.h" "DeploymentS.h" "DeploymentC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Deployment_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_NodeManager.idl"

InputPath=Deployment_NodeManager.idl

"Deployment_NodeManagerC.inl" "Deployment_NodeManagerS.inl" "Deployment_NodeManagerC.h" "Deployment_NodeManagerS.h" "Deployment_NodeManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Deployment_NodeManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_DomainApplication.idl"

InputPath=Deployment_DomainApplication.idl

"Deployment_DomainApplicationC.inl" "Deployment_DomainApplicationS.inl" "Deployment_DomainApplicationC.h" "Deployment_DomainApplicationS.h" "Deployment_DomainApplicationC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Deployment_DomainApplication_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_ExecutionManager.idl"

InputPath=Deployment_ExecutionManager.idl

"Deployment_ExecutionManagerC.inl" "Deployment_ExecutionManagerS.inl" "Deployment_ExecutionManagerC.h" "Deployment_ExecutionManagerS.h" "Deployment_ExecutionManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Deployment_ExecutionManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

SOURCE="Deployment_RepositoryManager.idl"

InputPath=Deployment_RepositoryManager.idl

"Deployment_RepositoryManagerC.inl" "Deployment_RepositoryManagerS.inl" "Deployment_RepositoryManagerC.h" "Deployment_RepositoryManagerS.h" "Deployment_RepositoryManagerC.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Deployment_RepositoryManager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\.. -I..\../ciao -Wb,stub_export_macro=Deployment_stub_Export -Wb,stub_export_include=Deployment_stub_export.h -Wb,skel_export_macro=Deployment_svnt_Export -Wb,skel_export_include=Deployment_svnt_export.h "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Deployment_stub.dep")
	@echo Using "Makefile.Deployment_stub.dep"
!ELSE
	@echo Warning: cannot find "Makefile.Deployment_stub.dep"
!ENDIF
!ENDIF

