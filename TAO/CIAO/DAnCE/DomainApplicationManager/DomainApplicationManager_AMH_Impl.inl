// -*- C++ -*-
//
// $Id: DomainApplicationManager_AMH_Impl.inl 982 2009-01-02 20:12:14Z mitza $

ACE_INLINE const char *
CIAO::DomainApplicationManager_AMH_Impl::get_uuid () const
{
  return this->uuid_;
}

ACE_INLINE void
CIAO::DomainApplicationManager_AMH_Impl::set_uuid (const char * uuid)
{
  // Copy this uuid reference
  this->uuid_ = CORBA::string_dup (uuid);
}
