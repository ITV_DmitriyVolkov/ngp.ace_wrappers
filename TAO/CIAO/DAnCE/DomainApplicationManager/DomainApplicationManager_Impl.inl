// -*- C++ -*-
//
// $Id: DomainApplicationManager_Impl.inl 14 2007-02-01 15:49:12Z mitza $

ACE_INLINE const char *
CIAO::DomainApplicationManager_Impl::get_uuid () const
{
  return this->uuid_;
}

ACE_INLINE void
CIAO::DomainApplicationManager_Impl::set_uuid (const char * uuid)
{
  // Copy this uuid reference
  this->uuid_ = CORBA::string_dup (uuid);
}
