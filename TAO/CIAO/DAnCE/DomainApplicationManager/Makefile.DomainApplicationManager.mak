# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.DomainApplicationManager.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\..\..\lib
INTDIR=Debug\DomainApplicationManager\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\lib\DomainApplicationManagerd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\ciaosvcs\Events" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DDOMAINAPPLICATIONMANAGER_BUILD_DLL -f "Makefile.DomainApplicationManager.dep" "DomainApplicationManager_Impl.cpp" "DomainApplicationManager_AMI_Impl.cpp" "DomainApplicationManager_AMH_Impl.cpp" "DomainApplicationManager_ActiveObject_Impl.cpp" "Deployment_Configuration.cpp" "Reply_Handler_i.cpp" "Task_StartLaunch.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\DomainApplicationManagerd.pdb"
	-@del /f/q "..\..\..\..\lib\DomainApplicationManagerd.dll"
	-@del /f/q "$(OUTDIR)\DomainApplicationManagerd.lib"
	-@del /f/q "$(OUTDIR)\DomainApplicationManagerd.exp"
	-@del /f/q "$(OUTDIR)\DomainApplicationManagerd.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\DomainApplicationManager\$(NULL)" mkdir "Debug\DomainApplicationManager"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\ciaosvcs\Events" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D DOMAINAPPLICATIONMANAGER_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_IFR_Clientd.lib TAO_Valuetyped.lib TAO_CodecFactoryd.lib TAO_PId.lib CIAO_Clientd.lib TAO_PortableServerd.lib CIAO_Containerd.lib CIAO_Events_Based.lib TAO_Messagingd.lib CIAO_Deployment_stubd.lib TAO_Svc_Utilsd.lib TAO_RTEventd.lib TAO_RTEvent_Skeld.lib TAO_RTEvent_Servd.lib TAO_CosNamingd.lib CIAO_RTEventd.lib CIAO_Eventsd.lib CIAO_Deployment_svntd.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\..\..\lib\DomainApplicationManagerd.pdb" /machine:IA64 /out:"..\..\..\..\lib\DomainApplicationManagerd.dll" /implib:"$(OUTDIR)\DomainApplicationManagerd.lib"
LINK32_OBJS= \
	"$(INTDIR)\DomainApplicationManager_Impl.obj" \
	"$(INTDIR)\DomainApplicationManager_AMI_Impl.obj" \
	"$(INTDIR)\DomainApplicationManager_AMH_Impl.obj" \
	"$(INTDIR)\DomainApplicationManager_ActiveObject_Impl.obj" \
	"$(INTDIR)\Deployment_Configuration.obj" \
	"$(INTDIR)\Reply_Handler_i.obj" \
	"$(INTDIR)\Task_StartLaunch.obj"

"..\..\..\..\lib\DomainApplicationManagerd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\lib\DomainApplicationManagerd.dll.manifest" mt.exe -manifest "..\..\..\..\lib\DomainApplicationManagerd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\..\..\lib
INTDIR=Release\DomainApplicationManager\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\lib\DomainApplicationManager.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\ciaosvcs\Events" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DDOMAINAPPLICATIONMANAGER_BUILD_DLL -f "Makefile.DomainApplicationManager.dep" "DomainApplicationManager_Impl.cpp" "DomainApplicationManager_AMI_Impl.cpp" "DomainApplicationManager_AMH_Impl.cpp" "DomainApplicationManager_ActiveObject_Impl.cpp" "Deployment_Configuration.cpp" "Reply_Handler_i.cpp" "Task_StartLaunch.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\..\..\lib\DomainApplicationManager.dll"
	-@del /f/q "$(OUTDIR)\DomainApplicationManager.lib"
	-@del /f/q "$(OUTDIR)\DomainApplicationManager.exp"
	-@del /f/q "$(OUTDIR)\DomainApplicationManager.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\DomainApplicationManager\$(NULL)" mkdir "Release\DomainApplicationManager"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\ciaosvcs\Events" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D DOMAINAPPLICATIONMANAGER_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_IFR_Client.lib TAO_Valuetype.lib TAO_CodecFactory.lib TAO_PI.lib CIAO_Client.lib TAO_PortableServer.lib CIAO_Container.lib CIAO_Events_Base.lib TAO_Messaging.lib CIAO_Deployment_stub.lib TAO_Svc_Utils.lib TAO_RTEvent.lib TAO_RTEvent_Skel.lib TAO_RTEvent_Serv.lib TAO_CosNaming.lib CIAO_RTEvent.lib CIAO_Events.lib CIAO_Deployment_svnt.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\..\..\lib\DomainApplicationManager.dll" /implib:"$(OUTDIR)\DomainApplicationManager.lib"
LINK32_OBJS= \
	"$(INTDIR)\DomainApplicationManager_Impl.obj" \
	"$(INTDIR)\DomainApplicationManager_AMI_Impl.obj" \
	"$(INTDIR)\DomainApplicationManager_AMH_Impl.obj" \
	"$(INTDIR)\DomainApplicationManager_ActiveObject_Impl.obj" \
	"$(INTDIR)\Deployment_Configuration.obj" \
	"$(INTDIR)\Reply_Handler_i.obj" \
	"$(INTDIR)\Task_StartLaunch.obj"

"..\..\..\..\lib\DomainApplicationManager.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\lib\DomainApplicationManager.dll.manifest" mt.exe -manifest "..\..\..\..\lib\DomainApplicationManager.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\..\..\lib
INTDIR=Static_Debug\DomainApplicationManager\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\DomainApplicationManagersd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\ciaosvcs\Events" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.DomainApplicationManager.dep" "DomainApplicationManager_Impl.cpp" "DomainApplicationManager_AMI_Impl.cpp" "DomainApplicationManager_AMH_Impl.cpp" "DomainApplicationManager_ActiveObject_Impl.cpp" "Deployment_Configuration.cpp" "Reply_Handler_i.cpp" "Task_StartLaunch.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\DomainApplicationManagersd.lib"
	-@del /f/q "$(OUTDIR)\DomainApplicationManagersd.exp"
	-@del /f/q "$(OUTDIR)\DomainApplicationManagersd.ilk"
	-@del /f/q "..\..\..\..\lib\DomainApplicationManagersd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\DomainApplicationManager\$(NULL)" mkdir "Static_Debug\DomainApplicationManager"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"..\..\..\..\lib\DomainApplicationManagersd.pdb" /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\ciaosvcs\Events" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\lib\DomainApplicationManagersd.lib"
LINK32_OBJS= \
	"$(INTDIR)\DomainApplicationManager_Impl.obj" \
	"$(INTDIR)\DomainApplicationManager_AMI_Impl.obj" \
	"$(INTDIR)\DomainApplicationManager_AMH_Impl.obj" \
	"$(INTDIR)\DomainApplicationManager_ActiveObject_Impl.obj" \
	"$(INTDIR)\Deployment_Configuration.obj" \
	"$(INTDIR)\Reply_Handler_i.obj" \
	"$(INTDIR)\Task_StartLaunch.obj"

"$(OUTDIR)\DomainApplicationManagersd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\DomainApplicationManagersd.lib.manifest" mt.exe -manifest "$(OUTDIR)\DomainApplicationManagersd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\..\..\lib
INTDIR=Static_Release\DomainApplicationManager\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\DomainApplicationManagers.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\ciaosvcs\Events" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.DomainApplicationManager.dep" "DomainApplicationManager_Impl.cpp" "DomainApplicationManager_AMI_Impl.cpp" "DomainApplicationManager_AMH_Impl.cpp" "DomainApplicationManager_ActiveObject_Impl.cpp" "Deployment_Configuration.cpp" "Reply_Handler_i.cpp" "Task_StartLaunch.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\DomainApplicationManagers.lib"
	-@del /f/q "$(OUTDIR)\DomainApplicationManagers.exp"
	-@del /f/q "$(OUTDIR)\DomainApplicationManagers.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\DomainApplicationManager\$(NULL)" mkdir "Static_Release\DomainApplicationManager"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\ciaosvcs\Events" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\lib\DomainApplicationManagers.lib"
LINK32_OBJS= \
	"$(INTDIR)\DomainApplicationManager_Impl.obj" \
	"$(INTDIR)\DomainApplicationManager_AMI_Impl.obj" \
	"$(INTDIR)\DomainApplicationManager_AMH_Impl.obj" \
	"$(INTDIR)\DomainApplicationManager_ActiveObject_Impl.obj" \
	"$(INTDIR)\Deployment_Configuration.obj" \
	"$(INTDIR)\Reply_Handler_i.obj" \
	"$(INTDIR)\Task_StartLaunch.obj"

"$(OUTDIR)\DomainApplicationManagers.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\DomainApplicationManagers.lib.manifest" mt.exe -manifest "$(OUTDIR)\DomainApplicationManagers.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.DomainApplicationManager.dep")
!INCLUDE "Makefile.DomainApplicationManager.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="DomainApplicationManager_Impl.cpp"

"$(INTDIR)\DomainApplicationManager_Impl.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DomainApplicationManager_Impl.obj" $(SOURCE)

SOURCE="DomainApplicationManager_AMI_Impl.cpp"

"$(INTDIR)\DomainApplicationManager_AMI_Impl.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DomainApplicationManager_AMI_Impl.obj" $(SOURCE)

SOURCE="DomainApplicationManager_AMH_Impl.cpp"

"$(INTDIR)\DomainApplicationManager_AMH_Impl.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DomainApplicationManager_AMH_Impl.obj" $(SOURCE)

SOURCE="DomainApplicationManager_ActiveObject_Impl.cpp"

"$(INTDIR)\DomainApplicationManager_ActiveObject_Impl.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DomainApplicationManager_ActiveObject_Impl.obj" $(SOURCE)

SOURCE="Deployment_Configuration.cpp"

"$(INTDIR)\Deployment_Configuration.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Deployment_Configuration.obj" $(SOURCE)

SOURCE="Reply_Handler_i.cpp"

"$(INTDIR)\Reply_Handler_i.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Reply_Handler_i.obj" $(SOURCE)

SOURCE="Task_StartLaunch.cpp"

"$(INTDIR)\Task_StartLaunch.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Task_StartLaunch.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.DomainApplicationManager.dep")
	@echo Using "Makefile.DomainApplicationManager.dep"
!ELSE
	@echo Warning: cannot find "Makefile.DomainApplicationManager.dep"
!ENDIF
!ENDIF

