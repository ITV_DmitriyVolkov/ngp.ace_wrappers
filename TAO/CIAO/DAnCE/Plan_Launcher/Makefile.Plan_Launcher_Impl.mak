# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.Plan_Launcher_Impl.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\..\..\lib
INTDIR=Debug\Plan_Launcher_Impl\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\lib\Plan_Launcher_Impld.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\ciaosvcs\Events" -I"..\..\tools" -I"..\..\tools\Config_Handlers" -I"..\..\DAnCE\ExecutionManager" -I"..\..\DAnCE\Plan_Generator" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DXML_USE_PTHREADS -DPLAN_LAUNCHER_IMPL_BUILD_DLL -f "Makefile.Plan_Launcher_Impl.dep" "..\ExecutionManager\DAM_Map.cpp" "Plan_Launcher_Impl.cpp" "Plan_Launcher_Benchmark_Impl.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\Plan_Launcher_Impld.pdb"
	-@del /f/q "..\..\..\..\lib\Plan_Launcher_Impld.dll"
	-@del /f/q "$(OUTDIR)\Plan_Launcher_Impld.lib"
	-@del /f/q "$(OUTDIR)\Plan_Launcher_Impld.exp"
	-@del /f/q "$(OUTDIR)\Plan_Launcher_Impld.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\Plan_Launcher_Impl\$(NULL)" mkdir "Debug\Plan_Launcher_Impl"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\ciaosvcs\Events" /I "..\..\tools" /I "..\..\tools\Config_Handlers" /I "..\..\DAnCE\ExecutionManager" /I "..\..\DAnCE\Plan_Generator" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D XML_USE_PTHREADS /D PLAN_LAUNCHER_IMPL_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_PortableServerd.lib TAO_IFR_Clientd.lib TAO_Valuetyped.lib TAO_CodecFactoryd.lib TAO_PId.lib CIAO_Clientd.lib CIAO_Containerd.lib CIAO_Events_Based.lib TAO_Messagingd.lib CIAO_Deployment_stubd.lib TAO_Svc_Utilsd.lib TAO_RTEventd.lib TAO_RTEvent_Skeld.lib TAO_RTEvent_Servd.lib TAO_CosNamingd.lib CIAO_RTEventd.lib CIAO_Eventsd.lib CIAO_Deployment_svntd.lib TAO_Utilsd.lib CIAO_Serverd.lib CIAO_XML_Utilsd.lib TAO_DynamicAnyd.lib TAO_TypeCodeFactoryd.lib XSC_XML_Handlersd.lib XSC_DynAny_Handlerd.lib XSC_Config_Handlers_Commond.lib Package_Config_Handlersd.lib CIAO_Events_Handlersd.lib RT_CCM_Config_Handlersd.lib XSC_Config_Handlersd.lib ExecutionManager_stubd.lib Plan_Generatord.lib TAO_RTCORBAd.lib DAnCE_Utilsd.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\..\..\lib\Plan_Launcher_Impld.pdb" /machine:IA64 /out:"..\..\..\..\lib\Plan_Launcher_Impld.dll" /implib:"$(OUTDIR)\Plan_Launcher_Impld.lib"
LINK32_OBJS= \
	"$(INTDIR)\dotdot\ExecutionManager\DAM_Map.obj" \
	"$(INTDIR)\Plan_Launcher_Impl.obj" \
	"$(INTDIR)\Plan_Launcher_Benchmark_Impl.obj"

"..\..\..\..\lib\Plan_Launcher_Impld.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\lib\Plan_Launcher_Impld.dll.manifest" mt.exe -manifest "..\..\..\..\lib\Plan_Launcher_Impld.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\..\..\lib
INTDIR=Release\Plan_Launcher_Impl\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\lib\Plan_Launcher_Impl.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\ciaosvcs\Events" -I"..\..\tools" -I"..\..\tools\Config_Handlers" -I"..\..\DAnCE\ExecutionManager" -I"..\..\DAnCE\Plan_Generator" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DXML_USE_PTHREADS -DPLAN_LAUNCHER_IMPL_BUILD_DLL -f "Makefile.Plan_Launcher_Impl.dep" "..\ExecutionManager\DAM_Map.cpp" "Plan_Launcher_Impl.cpp" "Plan_Launcher_Benchmark_Impl.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\..\..\lib\Plan_Launcher_Impl.dll"
	-@del /f/q "$(OUTDIR)\Plan_Launcher_Impl.lib"
	-@del /f/q "$(OUTDIR)\Plan_Launcher_Impl.exp"
	-@del /f/q "$(OUTDIR)\Plan_Launcher_Impl.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\Plan_Launcher_Impl\$(NULL)" mkdir "Release\Plan_Launcher_Impl"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\ciaosvcs\Events" /I "..\..\tools" /I "..\..\tools\Config_Handlers" /I "..\..\DAnCE\ExecutionManager" /I "..\..\DAnCE\Plan_Generator" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D XML_USE_PTHREADS /D PLAN_LAUNCHER_IMPL_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_PortableServer.lib TAO_IFR_Client.lib TAO_Valuetype.lib TAO_CodecFactory.lib TAO_PI.lib CIAO_Client.lib CIAO_Container.lib CIAO_Events_Base.lib TAO_Messaging.lib CIAO_Deployment_stub.lib TAO_Svc_Utils.lib TAO_RTEvent.lib TAO_RTEvent_Skel.lib TAO_RTEvent_Serv.lib TAO_CosNaming.lib CIAO_RTEvent.lib CIAO_Events.lib CIAO_Deployment_svnt.lib TAO_Utils.lib CIAO_Server.lib CIAO_XML_Utils.lib TAO_DynamicAny.lib TAO_TypeCodeFactory.lib XSC_XML_Handlers.lib XSC_DynAny_Handler.lib XSC_Config_Handlers_Common.lib Package_Config_Handlers.lib CIAO_Events_Handlers.lib RT_CCM_Config_Handlers.lib XSC_Config_Handlers.lib ExecutionManager_stub.lib Plan_Generator.lib TAO_RTCORBA.lib DAnCE_Utils.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\..\..\lib\Plan_Launcher_Impl.dll" /implib:"$(OUTDIR)\Plan_Launcher_Impl.lib"
LINK32_OBJS= \
	"$(INTDIR)\dotdot\ExecutionManager\DAM_Map.obj" \
	"$(INTDIR)\Plan_Launcher_Impl.obj" \
	"$(INTDIR)\Plan_Launcher_Benchmark_Impl.obj"

"..\..\..\..\lib\Plan_Launcher_Impl.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\lib\Plan_Launcher_Impl.dll.manifest" mt.exe -manifest "..\..\..\..\lib\Plan_Launcher_Impl.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\..\..\lib
INTDIR=Static_Debug\Plan_Launcher_Impl\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\Plan_Launcher_Implsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\ciaosvcs\Events" -I"..\..\tools" -I"..\..\tools\Config_Handlers" -I"..\..\DAnCE\ExecutionManager" -I"..\..\DAnCE\Plan_Generator" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DXML_USE_PTHREADS -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Plan_Launcher_Impl.dep" "..\ExecutionManager\DAM_Map.cpp" "Plan_Launcher_Impl.cpp" "Plan_Launcher_Benchmark_Impl.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\Plan_Launcher_Implsd.lib"
	-@del /f/q "$(OUTDIR)\Plan_Launcher_Implsd.exp"
	-@del /f/q "$(OUTDIR)\Plan_Launcher_Implsd.ilk"
	-@del /f/q "..\..\..\..\lib\Plan_Launcher_Implsd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\Plan_Launcher_Impl\$(NULL)" mkdir "Static_Debug\Plan_Launcher_Impl"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"..\..\..\..\lib\Plan_Launcher_Implsd.pdb" /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\ciaosvcs\Events" /I "..\..\tools" /I "..\..\tools\Config_Handlers" /I "..\..\DAnCE\ExecutionManager" /I "..\..\DAnCE\Plan_Generator" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D XML_USE_PTHREADS /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\lib\Plan_Launcher_Implsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\dotdot\ExecutionManager\DAM_Map.obj" \
	"$(INTDIR)\Plan_Launcher_Impl.obj" \
	"$(INTDIR)\Plan_Launcher_Benchmark_Impl.obj"

"$(OUTDIR)\Plan_Launcher_Implsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\Plan_Launcher_Implsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\Plan_Launcher_Implsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\..\..\lib
INTDIR=Static_Release\Plan_Launcher_Impl\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\Plan_Launcher_Impls.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -I"..\..\..\orbsvcs" -I"..\..\DAnCE" -I"..\..\ciaosvcs\Events" -I"..\..\tools" -I"..\..\tools\Config_Handlers" -I"..\..\DAnCE\ExecutionManager" -I"..\..\DAnCE\Plan_Generator" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DXML_USE_PTHREADS -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Plan_Launcher_Impl.dep" "..\ExecutionManager\DAM_Map.cpp" "Plan_Launcher_Impl.cpp" "Plan_Launcher_Benchmark_Impl.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\Plan_Launcher_Impls.lib"
	-@del /f/q "$(OUTDIR)\Plan_Launcher_Impls.exp"
	-@del /f/q "$(OUTDIR)\Plan_Launcher_Impls.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\Plan_Launcher_Impl\$(NULL)" mkdir "Static_Release\Plan_Launcher_Impl"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /I "..\..\..\orbsvcs" /I "..\..\DAnCE" /I "..\..\ciaosvcs\Events" /I "..\..\tools" /I "..\..\tools\Config_Handlers" /I "..\..\DAnCE\ExecutionManager" /I "..\..\DAnCE\Plan_Generator" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D XML_USE_PTHREADS /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\lib\Plan_Launcher_Impls.lib"
LINK32_OBJS= \
	"$(INTDIR)\dotdot\ExecutionManager\DAM_Map.obj" \
	"$(INTDIR)\Plan_Launcher_Impl.obj" \
	"$(INTDIR)\Plan_Launcher_Benchmark_Impl.obj"

"$(OUTDIR)\Plan_Launcher_Impls.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\Plan_Launcher_Impls.lib.manifest" mt.exe -manifest "$(OUTDIR)\Plan_Launcher_Impls.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Plan_Launcher_Impl.dep")
!INCLUDE "Makefile.Plan_Launcher_Impl.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="..\ExecutionManager\DAM_Map.cpp"

"$(INTDIR)\dotdot\ExecutionManager\DAM_Map.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\dotdot\ExecutionManager\$(NULL)" mkdir "$(INTDIR)\dotdot\ExecutionManager\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\dotdot\ExecutionManager\DAM_Map.obj" $(SOURCE)

SOURCE="Plan_Launcher_Impl.cpp"

"$(INTDIR)\Plan_Launcher_Impl.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Plan_Launcher_Impl.obj" $(SOURCE)

SOURCE="Plan_Launcher_Benchmark_Impl.cpp"

"$(INTDIR)\Plan_Launcher_Benchmark_Impl.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Plan_Launcher_Benchmark_Impl.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Plan_Launcher_Impl.dep")
	@echo Using "Makefile.Plan_Launcher_Impl.dep"
!ELSE
	@echo Warning: cannot find "Makefile.Plan_Launcher_Impl.dep"
!ENDIF
!ENDIF

