// $Id: Swapping_Container.inl 935 2008-12-10 21:47:27Z mitza $

ACE_INLINE CORBA::Object_ptr
CIAO::Swapping_Container::get_objref (PortableServer::Servant p)
{
  return this->the_POA ()->servant_to_reference (p);
}
