#include "Port_Activator.h"

ACE_RCSID (ciao,
           Servant_Activator,
           "$Id: Port_Activator.cpp 935 2008-12-10 21:47:27Z mitza $")

#if !defined (__ACE_INLINE__)
# include "Port_Activator.inl"
#endif /* __ACE_INLINE__ */

namespace CIAO
{
  Port_Activator::Port_Activator (const char *oid,
                                  const char *name,
                                  Type t)
    : oid_ (oid),
      name_ (name),
      t_ (t)
  {
  }

  Port_Activator::~Port_Activator (void)
  {
  }
}
