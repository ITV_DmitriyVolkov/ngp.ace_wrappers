# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.CIAO_Container.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "SecurityC.h" "SecurityC.cpp" "CCM_ContextC.h" "CCM_ContextC.cpp" "CCM_CCMExceptionC.h" "CCM_CCMExceptionC.cpp" "CCM_EntityComponentC.h" "CCM_EntityComponentC.cpp" "CCM_EntityContextC.h" "CCM_EntityContextC.cpp" "CIAO_SwapExecC.h" "CIAO_SwapExecC.cpp" "CIAO_UpgradeableContextC.h" "CIAO_UpgradeableContextC.cpp" "CCM_CCM2ContextC.inl" "CCM_CCM2ContextC.h" "CCM_CCM2ContextC.cpp" "CCM_ProxyHomeRegistrationC.inl" "CCM_ProxyHomeRegistrationC.h" "CCM_ProxyHomeRegistrationC.cpp" "CCM_Session2ContextC.inl" "CCM_Session2ContextC.h" "CCM_Session2ContextC.cpp" "CCM_TransactionC.inl" "CCM_TransactionC.h" "CCM_TransactionC.cpp" "CosPersistentStateC.inl" "CosPersistentStateC.h" "CosPersistentStateC.cpp" "CCM_ContainerC.inl" "CCM_ContainerC.h" "CCM_ContainerC.cpp" "CCM_SessionContextC.inl" "CCM_SessionContextC.h" "CCM_SessionContextC.cpp" "CCM_SessionComponentC.inl" "CCM_SessionComponentC.h" "CCM_SessionComponentC.cpp" "CCM_Container_ExC.inl" "CCM_Container_ExC.h" "CCM_Container_ExC.cpp" "CCM_StateIdFactoryC.inl" "CCM_StateIdFactoryC.h" "CCM_StateIdFactoryC.cpp" "CCM_Entity2ContextC.inl" "CCM_Entity2ContextC.h" "CCM_Entity2ContextC.cpp" "CCM_ComponentIdC.inl" "CCM_ComponentIdC.h" "CCM_ComponentIdC.cpp" "CCM_ExecutorLocatorC.inl" "CCM_ExecutorLocatorC.h" "CCM_ExecutorLocatorC.cpp" "CCM_EnterpriseComponentC.inl" "CCM_EnterpriseComponentC.h" "CCM_EnterpriseComponentC.cpp" "CCM_SessionSynchronizationC.inl" "CCM_SessionSynchronizationC.h" "CCM_SessionSynchronizationC.cpp" "CCM_HomeRegistrationC.inl" "CCM_HomeRegistrationC.h" "CCM_HomeRegistrationC.cpp" "CCM_HomeExecutorBaseC.inl" "CCM_HomeExecutorBaseC.h" "CCM_HomeExecutorBaseC.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\..\lib
INTDIR=Debug\CIAO_Container\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\lib\CIAO_Containerd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -I"..\..\orbsvcs" -I".." -I"..\ciao" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DCIAO_CONTAINER_BUILD_DLL -f "Makefile.CIAO_Container.dep" "CCM_TransactionC.cpp" "CosPersistentStateC.cpp" "CCM_ContainerC.cpp" "CCM_SessionContextC.cpp" "CCM_SessionComponentC.cpp" "CCM_EnterpriseComponentC.cpp" "CCM_SessionSynchronizationC.cpp" "CCM_CCMExceptionC.cpp" "CCM_ContextC.cpp" "SecurityC.cpp" "CCM_Container_ExC.cpp" "CCM_StateIdFactoryC.cpp" "CCM_ComponentIdC.cpp" "CCM_Entity2ContextC.cpp" "CCM_ExecutorLocatorC.cpp" "CCM_ProxyHomeRegistrationC.cpp" "CCM_Session2ContextC.cpp" "CCM_CCM2ContextC.cpp" "CCM_HomeRegistrationC.cpp" "CCM_HomeExecutorBaseC.cpp" "CCM_EntityContextC.cpp" "CCM_EntityComponentC.cpp" "CCM_EventsS.cpp" "CCM_EventConsumerBaseS.cpp" "CCM_EventBaseS.cpp" "CCM_ConfiguratorS.cpp" "CCM_HomeConfigurationS.cpp" "CCM_KeylessCCMHomeS.cpp" "CCM_StandardConfiguratorS.cpp" "CCM_HomeS.cpp" "CCM_ObjectS.cpp" "CCM_PrimaryKeyBaseS.cpp" "CCM_HomeFinderS.cpp" "CCM_NavigationS.cpp" "CCM_ReceptacleS.cpp" "CIAO_SwapExecC.cpp" "CIAO_UpgradeableContextC.cpp" "Cookies.cpp" "ComponentsS.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_Containerd.pdb"
	-@del /f/q "..\..\..\lib\CIAO_Containerd.dll"
	-@del /f/q "$(OUTDIR)\CIAO_Containerd.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Containerd.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Containerd.ilk"
	-@del /f/q "SecurityC.h"
	-@del /f/q "SecurityC.cpp"
	-@del /f/q "CCM_ContextC.h"
	-@del /f/q "CCM_ContextC.cpp"
	-@del /f/q "CCM_CCMExceptionC.h"
	-@del /f/q "CCM_CCMExceptionC.cpp"
	-@del /f/q "CCM_EntityComponentC.h"
	-@del /f/q "CCM_EntityComponentC.cpp"
	-@del /f/q "CCM_EntityContextC.h"
	-@del /f/q "CCM_EntityContextC.cpp"
	-@del /f/q "CIAO_SwapExecC.h"
	-@del /f/q "CIAO_SwapExecC.cpp"
	-@del /f/q "CIAO_UpgradeableContextC.h"
	-@del /f/q "CIAO_UpgradeableContextC.cpp"
	-@del /f/q "CCM_CCM2ContextC.inl"
	-@del /f/q "CCM_CCM2ContextC.h"
	-@del /f/q "CCM_CCM2ContextC.cpp"
	-@del /f/q "CCM_ProxyHomeRegistrationC.inl"
	-@del /f/q "CCM_ProxyHomeRegistrationC.h"
	-@del /f/q "CCM_ProxyHomeRegistrationC.cpp"
	-@del /f/q "CCM_Session2ContextC.inl"
	-@del /f/q "CCM_Session2ContextC.h"
	-@del /f/q "CCM_Session2ContextC.cpp"
	-@del /f/q "CCM_TransactionC.inl"
	-@del /f/q "CCM_TransactionC.h"
	-@del /f/q "CCM_TransactionC.cpp"
	-@del /f/q "CosPersistentStateC.inl"
	-@del /f/q "CosPersistentStateC.h"
	-@del /f/q "CosPersistentStateC.cpp"
	-@del /f/q "CCM_ContainerC.inl"
	-@del /f/q "CCM_ContainerC.h"
	-@del /f/q "CCM_ContainerC.cpp"
	-@del /f/q "CCM_SessionContextC.inl"
	-@del /f/q "CCM_SessionContextC.h"
	-@del /f/q "CCM_SessionContextC.cpp"
	-@del /f/q "CCM_SessionComponentC.inl"
	-@del /f/q "CCM_SessionComponentC.h"
	-@del /f/q "CCM_SessionComponentC.cpp"
	-@del /f/q "CCM_Container_ExC.inl"
	-@del /f/q "CCM_Container_ExC.h"
	-@del /f/q "CCM_Container_ExC.cpp"
	-@del /f/q "CCM_StateIdFactoryC.inl"
	-@del /f/q "CCM_StateIdFactoryC.h"
	-@del /f/q "CCM_StateIdFactoryC.cpp"
	-@del /f/q "CCM_Entity2ContextC.inl"
	-@del /f/q "CCM_Entity2ContextC.h"
	-@del /f/q "CCM_Entity2ContextC.cpp"
	-@del /f/q "CCM_ComponentIdC.inl"
	-@del /f/q "CCM_ComponentIdC.h"
	-@del /f/q "CCM_ComponentIdC.cpp"
	-@del /f/q "CCM_ExecutorLocatorC.inl"
	-@del /f/q "CCM_ExecutorLocatorC.h"
	-@del /f/q "CCM_ExecutorLocatorC.cpp"
	-@del /f/q "CCM_EnterpriseComponentC.inl"
	-@del /f/q "CCM_EnterpriseComponentC.h"
	-@del /f/q "CCM_EnterpriseComponentC.cpp"
	-@del /f/q "CCM_SessionSynchronizationC.inl"
	-@del /f/q "CCM_SessionSynchronizationC.h"
	-@del /f/q "CCM_SessionSynchronizationC.cpp"
	-@del /f/q "CCM_HomeRegistrationC.inl"
	-@del /f/q "CCM_HomeRegistrationC.h"
	-@del /f/q "CCM_HomeRegistrationC.cpp"
	-@del /f/q "CCM_HomeExecutorBaseC.inl"
	-@del /f/q "CCM_HomeExecutorBaseC.h"
	-@del /f/q "CCM_HomeExecutorBaseC.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\CIAO_Container\$(NULL)" mkdir "Debug\CIAO_Container"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"$(INTDIR)/" /I "..\..\.." /I "..\.." /I "..\..\orbsvcs" /I ".." /I "..\ciao" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D CIAO_CONTAINER_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_PortableServerd.lib TAO_Valuetyped.lib TAO_IFR_Clientd.lib TAO_CodecFactoryd.lib TAO_PId.lib CIAO_Clientd.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\..\lib\CIAO_Containerd.pdb" /machine:IA64 /out:"..\..\..\lib\CIAO_Containerd.dll" /implib:"$(OUTDIR)\CIAO_Containerd.lib"
LINK32_OBJS= \
	"$(INTDIR)\CCM_TransactionC.obj" \
	"$(INTDIR)\CosPersistentStateC.obj" \
	"$(INTDIR)\CCM_ContainerC.obj" \
	"$(INTDIR)\CCM_SessionContextC.obj" \
	"$(INTDIR)\CCM_SessionComponentC.obj" \
	"$(INTDIR)\CCM_EnterpriseComponentC.obj" \
	"$(INTDIR)\CCM_SessionSynchronizationC.obj" \
	"$(INTDIR)\CCM_CCMExceptionC.obj" \
	"$(INTDIR)\CCM_ContextC.obj" \
	"$(INTDIR)\SecurityC.obj" \
	"$(INTDIR)\CCM_Container_ExC.obj" \
	"$(INTDIR)\CCM_StateIdFactoryC.obj" \
	"$(INTDIR)\CCM_ComponentIdC.obj" \
	"$(INTDIR)\CCM_Entity2ContextC.obj" \
	"$(INTDIR)\CCM_ExecutorLocatorC.obj" \
	"$(INTDIR)\CCM_ProxyHomeRegistrationC.obj" \
	"$(INTDIR)\CCM_Session2ContextC.obj" \
	"$(INTDIR)\CCM_CCM2ContextC.obj" \
	"$(INTDIR)\CCM_HomeRegistrationC.obj" \
	"$(INTDIR)\CCM_HomeExecutorBaseC.obj" \
	"$(INTDIR)\CCM_EntityContextC.obj" \
	"$(INTDIR)\CCM_EntityComponentC.obj" \
	"$(INTDIR)\CCM_EventsS.obj" \
	"$(INTDIR)\CCM_EventConsumerBaseS.obj" \
	"$(INTDIR)\CCM_EventBaseS.obj" \
	"$(INTDIR)\CCM_ConfiguratorS.obj" \
	"$(INTDIR)\CCM_HomeConfigurationS.obj" \
	"$(INTDIR)\CCM_KeylessCCMHomeS.obj" \
	"$(INTDIR)\CCM_StandardConfiguratorS.obj" \
	"$(INTDIR)\CCM_HomeS.obj" \
	"$(INTDIR)\CCM_ObjectS.obj" \
	"$(INTDIR)\CCM_PrimaryKeyBaseS.obj" \
	"$(INTDIR)\CCM_HomeFinderS.obj" \
	"$(INTDIR)\CCM_NavigationS.obj" \
	"$(INTDIR)\CCM_ReceptacleS.obj" \
	"$(INTDIR)\CIAO_SwapExecC.obj" \
	"$(INTDIR)\CIAO_UpgradeableContextC.obj" \
	"$(INTDIR)\Cookies.obj" \
	"$(INTDIR)\ComponentsS.obj"

"..\..\..\lib\CIAO_Containerd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\lib\CIAO_Containerd.dll.manifest" mt.exe -manifest "..\..\..\lib\CIAO_Containerd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\..\lib
INTDIR=Release\CIAO_Container\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\lib\CIAO_Container.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -I"..\..\orbsvcs" -I".." -I"..\ciao" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DCIAO_CONTAINER_BUILD_DLL -f "Makefile.CIAO_Container.dep" "CCM_TransactionC.cpp" "CosPersistentStateC.cpp" "CCM_ContainerC.cpp" "CCM_SessionContextC.cpp" "CCM_SessionComponentC.cpp" "CCM_EnterpriseComponentC.cpp" "CCM_SessionSynchronizationC.cpp" "CCM_CCMExceptionC.cpp" "CCM_ContextC.cpp" "SecurityC.cpp" "CCM_Container_ExC.cpp" "CCM_StateIdFactoryC.cpp" "CCM_ComponentIdC.cpp" "CCM_Entity2ContextC.cpp" "CCM_ExecutorLocatorC.cpp" "CCM_ProxyHomeRegistrationC.cpp" "CCM_Session2ContextC.cpp" "CCM_CCM2ContextC.cpp" "CCM_HomeRegistrationC.cpp" "CCM_HomeExecutorBaseC.cpp" "CCM_EntityContextC.cpp" "CCM_EntityComponentC.cpp" "CCM_EventsS.cpp" "CCM_EventConsumerBaseS.cpp" "CCM_EventBaseS.cpp" "CCM_ConfiguratorS.cpp" "CCM_HomeConfigurationS.cpp" "CCM_KeylessCCMHomeS.cpp" "CCM_StandardConfiguratorS.cpp" "CCM_HomeS.cpp" "CCM_ObjectS.cpp" "CCM_PrimaryKeyBaseS.cpp" "CCM_HomeFinderS.cpp" "CCM_NavigationS.cpp" "CCM_ReceptacleS.cpp" "CIAO_SwapExecC.cpp" "CIAO_UpgradeableContextC.cpp" "Cookies.cpp" "ComponentsS.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\..\lib\CIAO_Container.dll"
	-@del /f/q "$(OUTDIR)\CIAO_Container.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Container.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Container.ilk"
	-@del /f/q "SecurityC.h"
	-@del /f/q "SecurityC.cpp"
	-@del /f/q "CCM_ContextC.h"
	-@del /f/q "CCM_ContextC.cpp"
	-@del /f/q "CCM_CCMExceptionC.h"
	-@del /f/q "CCM_CCMExceptionC.cpp"
	-@del /f/q "CCM_EntityComponentC.h"
	-@del /f/q "CCM_EntityComponentC.cpp"
	-@del /f/q "CCM_EntityContextC.h"
	-@del /f/q "CCM_EntityContextC.cpp"
	-@del /f/q "CIAO_SwapExecC.h"
	-@del /f/q "CIAO_SwapExecC.cpp"
	-@del /f/q "CIAO_UpgradeableContextC.h"
	-@del /f/q "CIAO_UpgradeableContextC.cpp"
	-@del /f/q "CCM_CCM2ContextC.inl"
	-@del /f/q "CCM_CCM2ContextC.h"
	-@del /f/q "CCM_CCM2ContextC.cpp"
	-@del /f/q "CCM_ProxyHomeRegistrationC.inl"
	-@del /f/q "CCM_ProxyHomeRegistrationC.h"
	-@del /f/q "CCM_ProxyHomeRegistrationC.cpp"
	-@del /f/q "CCM_Session2ContextC.inl"
	-@del /f/q "CCM_Session2ContextC.h"
	-@del /f/q "CCM_Session2ContextC.cpp"
	-@del /f/q "CCM_TransactionC.inl"
	-@del /f/q "CCM_TransactionC.h"
	-@del /f/q "CCM_TransactionC.cpp"
	-@del /f/q "CosPersistentStateC.inl"
	-@del /f/q "CosPersistentStateC.h"
	-@del /f/q "CosPersistentStateC.cpp"
	-@del /f/q "CCM_ContainerC.inl"
	-@del /f/q "CCM_ContainerC.h"
	-@del /f/q "CCM_ContainerC.cpp"
	-@del /f/q "CCM_SessionContextC.inl"
	-@del /f/q "CCM_SessionContextC.h"
	-@del /f/q "CCM_SessionContextC.cpp"
	-@del /f/q "CCM_SessionComponentC.inl"
	-@del /f/q "CCM_SessionComponentC.h"
	-@del /f/q "CCM_SessionComponentC.cpp"
	-@del /f/q "CCM_Container_ExC.inl"
	-@del /f/q "CCM_Container_ExC.h"
	-@del /f/q "CCM_Container_ExC.cpp"
	-@del /f/q "CCM_StateIdFactoryC.inl"
	-@del /f/q "CCM_StateIdFactoryC.h"
	-@del /f/q "CCM_StateIdFactoryC.cpp"
	-@del /f/q "CCM_Entity2ContextC.inl"
	-@del /f/q "CCM_Entity2ContextC.h"
	-@del /f/q "CCM_Entity2ContextC.cpp"
	-@del /f/q "CCM_ComponentIdC.inl"
	-@del /f/q "CCM_ComponentIdC.h"
	-@del /f/q "CCM_ComponentIdC.cpp"
	-@del /f/q "CCM_ExecutorLocatorC.inl"
	-@del /f/q "CCM_ExecutorLocatorC.h"
	-@del /f/q "CCM_ExecutorLocatorC.cpp"
	-@del /f/q "CCM_EnterpriseComponentC.inl"
	-@del /f/q "CCM_EnterpriseComponentC.h"
	-@del /f/q "CCM_EnterpriseComponentC.cpp"
	-@del /f/q "CCM_SessionSynchronizationC.inl"
	-@del /f/q "CCM_SessionSynchronizationC.h"
	-@del /f/q "CCM_SessionSynchronizationC.cpp"
	-@del /f/q "CCM_HomeRegistrationC.inl"
	-@del /f/q "CCM_HomeRegistrationC.h"
	-@del /f/q "CCM_HomeRegistrationC.cpp"
	-@del /f/q "CCM_HomeExecutorBaseC.inl"
	-@del /f/q "CCM_HomeExecutorBaseC.h"
	-@del /f/q "CCM_HomeExecutorBaseC.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\CIAO_Container\$(NULL)" mkdir "Release\CIAO_Container"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\.." /I "..\.." /I "..\..\orbsvcs" /I ".." /I "..\ciao" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D CIAO_CONTAINER_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_PortableServer.lib TAO_Valuetype.lib TAO_IFR_Client.lib TAO_CodecFactory.lib TAO_PI.lib CIAO_Client.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\..\lib\CIAO_Container.dll" /implib:"$(OUTDIR)\CIAO_Container.lib"
LINK32_OBJS= \
	"$(INTDIR)\CCM_TransactionC.obj" \
	"$(INTDIR)\CosPersistentStateC.obj" \
	"$(INTDIR)\CCM_ContainerC.obj" \
	"$(INTDIR)\CCM_SessionContextC.obj" \
	"$(INTDIR)\CCM_SessionComponentC.obj" \
	"$(INTDIR)\CCM_EnterpriseComponentC.obj" \
	"$(INTDIR)\CCM_SessionSynchronizationC.obj" \
	"$(INTDIR)\CCM_CCMExceptionC.obj" \
	"$(INTDIR)\CCM_ContextC.obj" \
	"$(INTDIR)\SecurityC.obj" \
	"$(INTDIR)\CCM_Container_ExC.obj" \
	"$(INTDIR)\CCM_StateIdFactoryC.obj" \
	"$(INTDIR)\CCM_ComponentIdC.obj" \
	"$(INTDIR)\CCM_Entity2ContextC.obj" \
	"$(INTDIR)\CCM_ExecutorLocatorC.obj" \
	"$(INTDIR)\CCM_ProxyHomeRegistrationC.obj" \
	"$(INTDIR)\CCM_Session2ContextC.obj" \
	"$(INTDIR)\CCM_CCM2ContextC.obj" \
	"$(INTDIR)\CCM_HomeRegistrationC.obj" \
	"$(INTDIR)\CCM_HomeExecutorBaseC.obj" \
	"$(INTDIR)\CCM_EntityContextC.obj" \
	"$(INTDIR)\CCM_EntityComponentC.obj" \
	"$(INTDIR)\CCM_EventsS.obj" \
	"$(INTDIR)\CCM_EventConsumerBaseS.obj" \
	"$(INTDIR)\CCM_EventBaseS.obj" \
	"$(INTDIR)\CCM_ConfiguratorS.obj" \
	"$(INTDIR)\CCM_HomeConfigurationS.obj" \
	"$(INTDIR)\CCM_KeylessCCMHomeS.obj" \
	"$(INTDIR)\CCM_StandardConfiguratorS.obj" \
	"$(INTDIR)\CCM_HomeS.obj" \
	"$(INTDIR)\CCM_ObjectS.obj" \
	"$(INTDIR)\CCM_PrimaryKeyBaseS.obj" \
	"$(INTDIR)\CCM_HomeFinderS.obj" \
	"$(INTDIR)\CCM_NavigationS.obj" \
	"$(INTDIR)\CCM_ReceptacleS.obj" \
	"$(INTDIR)\CIAO_SwapExecC.obj" \
	"$(INTDIR)\CIAO_UpgradeableContextC.obj" \
	"$(INTDIR)\Cookies.obj" \
	"$(INTDIR)\ComponentsS.obj"

"..\..\..\lib\CIAO_Container.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\lib\CIAO_Container.dll.manifest" mt.exe -manifest "..\..\..\lib\CIAO_Container.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\..\lib
INTDIR=Static_Debug\CIAO_Container\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CIAO_Containersd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -I"..\..\orbsvcs" -I".." -I"..\ciao" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CIAO_Container.dep" "CCM_TransactionC.cpp" "CosPersistentStateC.cpp" "CCM_ContainerC.cpp" "CCM_SessionContextC.cpp" "CCM_SessionComponentC.cpp" "CCM_EnterpriseComponentC.cpp" "CCM_SessionSynchronizationC.cpp" "CCM_CCMExceptionC.cpp" "CCM_ContextC.cpp" "SecurityC.cpp" "CCM_Container_ExC.cpp" "CCM_StateIdFactoryC.cpp" "CCM_ComponentIdC.cpp" "CCM_Entity2ContextC.cpp" "CCM_ExecutorLocatorC.cpp" "CCM_ProxyHomeRegistrationC.cpp" "CCM_Session2ContextC.cpp" "CCM_CCM2ContextC.cpp" "CCM_HomeRegistrationC.cpp" "CCM_HomeExecutorBaseC.cpp" "CCM_EntityContextC.cpp" "CCM_EntityComponentC.cpp" "CCM_EventsS.cpp" "CCM_EventConsumerBaseS.cpp" "CCM_EventBaseS.cpp" "CCM_ConfiguratorS.cpp" "CCM_HomeConfigurationS.cpp" "CCM_KeylessCCMHomeS.cpp" "CCM_StandardConfiguratorS.cpp" "CCM_HomeS.cpp" "CCM_ObjectS.cpp" "CCM_PrimaryKeyBaseS.cpp" "CCM_HomeFinderS.cpp" "CCM_NavigationS.cpp" "CCM_ReceptacleS.cpp" "CIAO_SwapExecC.cpp" "CIAO_UpgradeableContextC.cpp" "Cookies.cpp" "ComponentsS.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_Containersd.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Containersd.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Containersd.ilk"
	-@del /f/q "..\..\..\lib\CIAO_Containersd.pdb"
	-@del /f/q "SecurityC.h"
	-@del /f/q "SecurityC.cpp"
	-@del /f/q "CCM_ContextC.h"
	-@del /f/q "CCM_ContextC.cpp"
	-@del /f/q "CCM_CCMExceptionC.h"
	-@del /f/q "CCM_CCMExceptionC.cpp"
	-@del /f/q "CCM_EntityComponentC.h"
	-@del /f/q "CCM_EntityComponentC.cpp"
	-@del /f/q "CCM_EntityContextC.h"
	-@del /f/q "CCM_EntityContextC.cpp"
	-@del /f/q "CIAO_SwapExecC.h"
	-@del /f/q "CIAO_SwapExecC.cpp"
	-@del /f/q "CIAO_UpgradeableContextC.h"
	-@del /f/q "CIAO_UpgradeableContextC.cpp"
	-@del /f/q "CCM_CCM2ContextC.inl"
	-@del /f/q "CCM_CCM2ContextC.h"
	-@del /f/q "CCM_CCM2ContextC.cpp"
	-@del /f/q "CCM_ProxyHomeRegistrationC.inl"
	-@del /f/q "CCM_ProxyHomeRegistrationC.h"
	-@del /f/q "CCM_ProxyHomeRegistrationC.cpp"
	-@del /f/q "CCM_Session2ContextC.inl"
	-@del /f/q "CCM_Session2ContextC.h"
	-@del /f/q "CCM_Session2ContextC.cpp"
	-@del /f/q "CCM_TransactionC.inl"
	-@del /f/q "CCM_TransactionC.h"
	-@del /f/q "CCM_TransactionC.cpp"
	-@del /f/q "CosPersistentStateC.inl"
	-@del /f/q "CosPersistentStateC.h"
	-@del /f/q "CosPersistentStateC.cpp"
	-@del /f/q "CCM_ContainerC.inl"
	-@del /f/q "CCM_ContainerC.h"
	-@del /f/q "CCM_ContainerC.cpp"
	-@del /f/q "CCM_SessionContextC.inl"
	-@del /f/q "CCM_SessionContextC.h"
	-@del /f/q "CCM_SessionContextC.cpp"
	-@del /f/q "CCM_SessionComponentC.inl"
	-@del /f/q "CCM_SessionComponentC.h"
	-@del /f/q "CCM_SessionComponentC.cpp"
	-@del /f/q "CCM_Container_ExC.inl"
	-@del /f/q "CCM_Container_ExC.h"
	-@del /f/q "CCM_Container_ExC.cpp"
	-@del /f/q "CCM_StateIdFactoryC.inl"
	-@del /f/q "CCM_StateIdFactoryC.h"
	-@del /f/q "CCM_StateIdFactoryC.cpp"
	-@del /f/q "CCM_Entity2ContextC.inl"
	-@del /f/q "CCM_Entity2ContextC.h"
	-@del /f/q "CCM_Entity2ContextC.cpp"
	-@del /f/q "CCM_ComponentIdC.inl"
	-@del /f/q "CCM_ComponentIdC.h"
	-@del /f/q "CCM_ComponentIdC.cpp"
	-@del /f/q "CCM_ExecutorLocatorC.inl"
	-@del /f/q "CCM_ExecutorLocatorC.h"
	-@del /f/q "CCM_ExecutorLocatorC.cpp"
	-@del /f/q "CCM_EnterpriseComponentC.inl"
	-@del /f/q "CCM_EnterpriseComponentC.h"
	-@del /f/q "CCM_EnterpriseComponentC.cpp"
	-@del /f/q "CCM_SessionSynchronizationC.inl"
	-@del /f/q "CCM_SessionSynchronizationC.h"
	-@del /f/q "CCM_SessionSynchronizationC.cpp"
	-@del /f/q "CCM_HomeRegistrationC.inl"
	-@del /f/q "CCM_HomeRegistrationC.h"
	-@del /f/q "CCM_HomeRegistrationC.cpp"
	-@del /f/q "CCM_HomeExecutorBaseC.inl"
	-@del /f/q "CCM_HomeExecutorBaseC.h"
	-@del /f/q "CCM_HomeExecutorBaseC.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\CIAO_Container\$(NULL)" mkdir "Static_Debug\CIAO_Container"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"..\..\..\lib\CIAO_Containersd.pdb" /I "..\..\.." /I "..\.." /I "..\..\orbsvcs" /I ".." /I "..\ciao" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\lib\CIAO_Containersd.lib"
LINK32_OBJS= \
	"$(INTDIR)\CCM_TransactionC.obj" \
	"$(INTDIR)\CosPersistentStateC.obj" \
	"$(INTDIR)\CCM_ContainerC.obj" \
	"$(INTDIR)\CCM_SessionContextC.obj" \
	"$(INTDIR)\CCM_SessionComponentC.obj" \
	"$(INTDIR)\CCM_EnterpriseComponentC.obj" \
	"$(INTDIR)\CCM_SessionSynchronizationC.obj" \
	"$(INTDIR)\CCM_CCMExceptionC.obj" \
	"$(INTDIR)\CCM_ContextC.obj" \
	"$(INTDIR)\SecurityC.obj" \
	"$(INTDIR)\CCM_Container_ExC.obj" \
	"$(INTDIR)\CCM_StateIdFactoryC.obj" \
	"$(INTDIR)\CCM_ComponentIdC.obj" \
	"$(INTDIR)\CCM_Entity2ContextC.obj" \
	"$(INTDIR)\CCM_ExecutorLocatorC.obj" \
	"$(INTDIR)\CCM_ProxyHomeRegistrationC.obj" \
	"$(INTDIR)\CCM_Session2ContextC.obj" \
	"$(INTDIR)\CCM_CCM2ContextC.obj" \
	"$(INTDIR)\CCM_HomeRegistrationC.obj" \
	"$(INTDIR)\CCM_HomeExecutorBaseC.obj" \
	"$(INTDIR)\CCM_EntityContextC.obj" \
	"$(INTDIR)\CCM_EntityComponentC.obj" \
	"$(INTDIR)\CCM_EventsS.obj" \
	"$(INTDIR)\CCM_EventConsumerBaseS.obj" \
	"$(INTDIR)\CCM_EventBaseS.obj" \
	"$(INTDIR)\CCM_ConfiguratorS.obj" \
	"$(INTDIR)\CCM_HomeConfigurationS.obj" \
	"$(INTDIR)\CCM_KeylessCCMHomeS.obj" \
	"$(INTDIR)\CCM_StandardConfiguratorS.obj" \
	"$(INTDIR)\CCM_HomeS.obj" \
	"$(INTDIR)\CCM_ObjectS.obj" \
	"$(INTDIR)\CCM_PrimaryKeyBaseS.obj" \
	"$(INTDIR)\CCM_HomeFinderS.obj" \
	"$(INTDIR)\CCM_NavigationS.obj" \
	"$(INTDIR)\CCM_ReceptacleS.obj" \
	"$(INTDIR)\CIAO_SwapExecC.obj" \
	"$(INTDIR)\CIAO_UpgradeableContextC.obj" \
	"$(INTDIR)\Cookies.obj" \
	"$(INTDIR)\ComponentsS.obj"

"$(OUTDIR)\CIAO_Containersd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CIAO_Containersd.lib.manifest" mt.exe -manifest "$(OUTDIR)\CIAO_Containersd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\..\lib
INTDIR=Static_Release\CIAO_Container\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CIAO_Containers.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -I"..\..\orbsvcs" -I".." -I"..\ciao" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CIAO_Container.dep" "CCM_TransactionC.cpp" "CosPersistentStateC.cpp" "CCM_ContainerC.cpp" "CCM_SessionContextC.cpp" "CCM_SessionComponentC.cpp" "CCM_EnterpriseComponentC.cpp" "CCM_SessionSynchronizationC.cpp" "CCM_CCMExceptionC.cpp" "CCM_ContextC.cpp" "SecurityC.cpp" "CCM_Container_ExC.cpp" "CCM_StateIdFactoryC.cpp" "CCM_ComponentIdC.cpp" "CCM_Entity2ContextC.cpp" "CCM_ExecutorLocatorC.cpp" "CCM_ProxyHomeRegistrationC.cpp" "CCM_Session2ContextC.cpp" "CCM_CCM2ContextC.cpp" "CCM_HomeRegistrationC.cpp" "CCM_HomeExecutorBaseC.cpp" "CCM_EntityContextC.cpp" "CCM_EntityComponentC.cpp" "CCM_EventsS.cpp" "CCM_EventConsumerBaseS.cpp" "CCM_EventBaseS.cpp" "CCM_ConfiguratorS.cpp" "CCM_HomeConfigurationS.cpp" "CCM_KeylessCCMHomeS.cpp" "CCM_StandardConfiguratorS.cpp" "CCM_HomeS.cpp" "CCM_ObjectS.cpp" "CCM_PrimaryKeyBaseS.cpp" "CCM_HomeFinderS.cpp" "CCM_NavigationS.cpp" "CCM_ReceptacleS.cpp" "CIAO_SwapExecC.cpp" "CIAO_UpgradeableContextC.cpp" "Cookies.cpp" "ComponentsS.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_Containers.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Containers.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Containers.ilk"
	-@del /f/q "SecurityC.h"
	-@del /f/q "SecurityC.cpp"
	-@del /f/q "CCM_ContextC.h"
	-@del /f/q "CCM_ContextC.cpp"
	-@del /f/q "CCM_CCMExceptionC.h"
	-@del /f/q "CCM_CCMExceptionC.cpp"
	-@del /f/q "CCM_EntityComponentC.h"
	-@del /f/q "CCM_EntityComponentC.cpp"
	-@del /f/q "CCM_EntityContextC.h"
	-@del /f/q "CCM_EntityContextC.cpp"
	-@del /f/q "CIAO_SwapExecC.h"
	-@del /f/q "CIAO_SwapExecC.cpp"
	-@del /f/q "CIAO_UpgradeableContextC.h"
	-@del /f/q "CIAO_UpgradeableContextC.cpp"
	-@del /f/q "CCM_CCM2ContextC.inl"
	-@del /f/q "CCM_CCM2ContextC.h"
	-@del /f/q "CCM_CCM2ContextC.cpp"
	-@del /f/q "CCM_ProxyHomeRegistrationC.inl"
	-@del /f/q "CCM_ProxyHomeRegistrationC.h"
	-@del /f/q "CCM_ProxyHomeRegistrationC.cpp"
	-@del /f/q "CCM_Session2ContextC.inl"
	-@del /f/q "CCM_Session2ContextC.h"
	-@del /f/q "CCM_Session2ContextC.cpp"
	-@del /f/q "CCM_TransactionC.inl"
	-@del /f/q "CCM_TransactionC.h"
	-@del /f/q "CCM_TransactionC.cpp"
	-@del /f/q "CosPersistentStateC.inl"
	-@del /f/q "CosPersistentStateC.h"
	-@del /f/q "CosPersistentStateC.cpp"
	-@del /f/q "CCM_ContainerC.inl"
	-@del /f/q "CCM_ContainerC.h"
	-@del /f/q "CCM_ContainerC.cpp"
	-@del /f/q "CCM_SessionContextC.inl"
	-@del /f/q "CCM_SessionContextC.h"
	-@del /f/q "CCM_SessionContextC.cpp"
	-@del /f/q "CCM_SessionComponentC.inl"
	-@del /f/q "CCM_SessionComponentC.h"
	-@del /f/q "CCM_SessionComponentC.cpp"
	-@del /f/q "CCM_Container_ExC.inl"
	-@del /f/q "CCM_Container_ExC.h"
	-@del /f/q "CCM_Container_ExC.cpp"
	-@del /f/q "CCM_StateIdFactoryC.inl"
	-@del /f/q "CCM_StateIdFactoryC.h"
	-@del /f/q "CCM_StateIdFactoryC.cpp"
	-@del /f/q "CCM_Entity2ContextC.inl"
	-@del /f/q "CCM_Entity2ContextC.h"
	-@del /f/q "CCM_Entity2ContextC.cpp"
	-@del /f/q "CCM_ComponentIdC.inl"
	-@del /f/q "CCM_ComponentIdC.h"
	-@del /f/q "CCM_ComponentIdC.cpp"
	-@del /f/q "CCM_ExecutorLocatorC.inl"
	-@del /f/q "CCM_ExecutorLocatorC.h"
	-@del /f/q "CCM_ExecutorLocatorC.cpp"
	-@del /f/q "CCM_EnterpriseComponentC.inl"
	-@del /f/q "CCM_EnterpriseComponentC.h"
	-@del /f/q "CCM_EnterpriseComponentC.cpp"
	-@del /f/q "CCM_SessionSynchronizationC.inl"
	-@del /f/q "CCM_SessionSynchronizationC.h"
	-@del /f/q "CCM_SessionSynchronizationC.cpp"
	-@del /f/q "CCM_HomeRegistrationC.inl"
	-@del /f/q "CCM_HomeRegistrationC.h"
	-@del /f/q "CCM_HomeRegistrationC.cpp"
	-@del /f/q "CCM_HomeExecutorBaseC.inl"
	-@del /f/q "CCM_HomeExecutorBaseC.h"
	-@del /f/q "CCM_HomeExecutorBaseC.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\CIAO_Container\$(NULL)" mkdir "Static_Release\CIAO_Container"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\.." /I "..\.." /I "..\..\orbsvcs" /I ".." /I "..\ciao" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\lib\CIAO_Containers.lib"
LINK32_OBJS= \
	"$(INTDIR)\CCM_TransactionC.obj" \
	"$(INTDIR)\CosPersistentStateC.obj" \
	"$(INTDIR)\CCM_ContainerC.obj" \
	"$(INTDIR)\CCM_SessionContextC.obj" \
	"$(INTDIR)\CCM_SessionComponentC.obj" \
	"$(INTDIR)\CCM_EnterpriseComponentC.obj" \
	"$(INTDIR)\CCM_SessionSynchronizationC.obj" \
	"$(INTDIR)\CCM_CCMExceptionC.obj" \
	"$(INTDIR)\CCM_ContextC.obj" \
	"$(INTDIR)\SecurityC.obj" \
	"$(INTDIR)\CCM_Container_ExC.obj" \
	"$(INTDIR)\CCM_StateIdFactoryC.obj" \
	"$(INTDIR)\CCM_ComponentIdC.obj" \
	"$(INTDIR)\CCM_Entity2ContextC.obj" \
	"$(INTDIR)\CCM_ExecutorLocatorC.obj" \
	"$(INTDIR)\CCM_ProxyHomeRegistrationC.obj" \
	"$(INTDIR)\CCM_Session2ContextC.obj" \
	"$(INTDIR)\CCM_CCM2ContextC.obj" \
	"$(INTDIR)\CCM_HomeRegistrationC.obj" \
	"$(INTDIR)\CCM_HomeExecutorBaseC.obj" \
	"$(INTDIR)\CCM_EntityContextC.obj" \
	"$(INTDIR)\CCM_EntityComponentC.obj" \
	"$(INTDIR)\CCM_EventsS.obj" \
	"$(INTDIR)\CCM_EventConsumerBaseS.obj" \
	"$(INTDIR)\CCM_EventBaseS.obj" \
	"$(INTDIR)\CCM_ConfiguratorS.obj" \
	"$(INTDIR)\CCM_HomeConfigurationS.obj" \
	"$(INTDIR)\CCM_KeylessCCMHomeS.obj" \
	"$(INTDIR)\CCM_StandardConfiguratorS.obj" \
	"$(INTDIR)\CCM_HomeS.obj" \
	"$(INTDIR)\CCM_ObjectS.obj" \
	"$(INTDIR)\CCM_PrimaryKeyBaseS.obj" \
	"$(INTDIR)\CCM_HomeFinderS.obj" \
	"$(INTDIR)\CCM_NavigationS.obj" \
	"$(INTDIR)\CCM_ReceptacleS.obj" \
	"$(INTDIR)\CIAO_SwapExecC.obj" \
	"$(INTDIR)\CIAO_UpgradeableContextC.obj" \
	"$(INTDIR)\Cookies.obj" \
	"$(INTDIR)\ComponentsS.obj"

"$(OUTDIR)\CIAO_Containers.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CIAO_Containers.lib.manifest" mt.exe -manifest "$(OUTDIR)\CIAO_Containers.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CIAO_Container.dep")
!INCLUDE "Makefile.CIAO_Container.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="CCM_TransactionC.cpp"

"$(INTDIR)\CCM_TransactionC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_TransactionC.obj" $(SOURCE)

SOURCE="CosPersistentStateC.cpp"

"$(INTDIR)\CosPersistentStateC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CosPersistentStateC.obj" $(SOURCE)

SOURCE="CCM_ContainerC.cpp"

"$(INTDIR)\CCM_ContainerC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_ContainerC.obj" $(SOURCE)

SOURCE="CCM_SessionContextC.cpp"

"$(INTDIR)\CCM_SessionContextC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_SessionContextC.obj" $(SOURCE)

SOURCE="CCM_SessionComponentC.cpp"

"$(INTDIR)\CCM_SessionComponentC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_SessionComponentC.obj" $(SOURCE)

SOURCE="CCM_EnterpriseComponentC.cpp"

"$(INTDIR)\CCM_EnterpriseComponentC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_EnterpriseComponentC.obj" $(SOURCE)

SOURCE="CCM_SessionSynchronizationC.cpp"

"$(INTDIR)\CCM_SessionSynchronizationC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_SessionSynchronizationC.obj" $(SOURCE)

SOURCE="CCM_CCMExceptionC.cpp"

"$(INTDIR)\CCM_CCMExceptionC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_CCMExceptionC.obj" $(SOURCE)

SOURCE="CCM_ContextC.cpp"

"$(INTDIR)\CCM_ContextC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_ContextC.obj" $(SOURCE)

SOURCE="SecurityC.cpp"

"$(INTDIR)\SecurityC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\SecurityC.obj" $(SOURCE)

SOURCE="CCM_Container_ExC.cpp"

"$(INTDIR)\CCM_Container_ExC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_Container_ExC.obj" $(SOURCE)

SOURCE="CCM_StateIdFactoryC.cpp"

"$(INTDIR)\CCM_StateIdFactoryC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_StateIdFactoryC.obj" $(SOURCE)

SOURCE="CCM_ComponentIdC.cpp"

"$(INTDIR)\CCM_ComponentIdC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_ComponentIdC.obj" $(SOURCE)

SOURCE="CCM_Entity2ContextC.cpp"

"$(INTDIR)\CCM_Entity2ContextC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_Entity2ContextC.obj" $(SOURCE)

SOURCE="CCM_ExecutorLocatorC.cpp"

"$(INTDIR)\CCM_ExecutorLocatorC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_ExecutorLocatorC.obj" $(SOURCE)

SOURCE="CCM_ProxyHomeRegistrationC.cpp"

"$(INTDIR)\CCM_ProxyHomeRegistrationC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_ProxyHomeRegistrationC.obj" $(SOURCE)

SOURCE="CCM_Session2ContextC.cpp"

"$(INTDIR)\CCM_Session2ContextC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_Session2ContextC.obj" $(SOURCE)

SOURCE="CCM_CCM2ContextC.cpp"

"$(INTDIR)\CCM_CCM2ContextC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_CCM2ContextC.obj" $(SOURCE)

SOURCE="CCM_HomeRegistrationC.cpp"

"$(INTDIR)\CCM_HomeRegistrationC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_HomeRegistrationC.obj" $(SOURCE)

SOURCE="CCM_HomeExecutorBaseC.cpp"

"$(INTDIR)\CCM_HomeExecutorBaseC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_HomeExecutorBaseC.obj" $(SOURCE)

SOURCE="CCM_EntityContextC.cpp"

"$(INTDIR)\CCM_EntityContextC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_EntityContextC.obj" $(SOURCE)

SOURCE="CCM_EntityComponentC.cpp"

"$(INTDIR)\CCM_EntityComponentC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_EntityComponentC.obj" $(SOURCE)

SOURCE="CCM_EventsS.cpp"

"$(INTDIR)\CCM_EventsS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_EventsS.obj" $(SOURCE)

SOURCE="CCM_EventConsumerBaseS.cpp"

"$(INTDIR)\CCM_EventConsumerBaseS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_EventConsumerBaseS.obj" $(SOURCE)

SOURCE="CCM_EventBaseS.cpp"

"$(INTDIR)\CCM_EventBaseS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_EventBaseS.obj" $(SOURCE)

SOURCE="CCM_ConfiguratorS.cpp"

"$(INTDIR)\CCM_ConfiguratorS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_ConfiguratorS.obj" $(SOURCE)

SOURCE="CCM_HomeConfigurationS.cpp"

"$(INTDIR)\CCM_HomeConfigurationS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_HomeConfigurationS.obj" $(SOURCE)

SOURCE="CCM_KeylessCCMHomeS.cpp"

"$(INTDIR)\CCM_KeylessCCMHomeS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_KeylessCCMHomeS.obj" $(SOURCE)

SOURCE="CCM_StandardConfiguratorS.cpp"

"$(INTDIR)\CCM_StandardConfiguratorS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_StandardConfiguratorS.obj" $(SOURCE)

SOURCE="CCM_HomeS.cpp"

"$(INTDIR)\CCM_HomeS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_HomeS.obj" $(SOURCE)

SOURCE="CCM_ObjectS.cpp"

"$(INTDIR)\CCM_ObjectS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_ObjectS.obj" $(SOURCE)

SOURCE="CCM_PrimaryKeyBaseS.cpp"

"$(INTDIR)\CCM_PrimaryKeyBaseS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_PrimaryKeyBaseS.obj" $(SOURCE)

SOURCE="CCM_HomeFinderS.cpp"

"$(INTDIR)\CCM_HomeFinderS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_HomeFinderS.obj" $(SOURCE)

SOURCE="CCM_NavigationS.cpp"

"$(INTDIR)\CCM_NavigationS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_NavigationS.obj" $(SOURCE)

SOURCE="CCM_ReceptacleS.cpp"

"$(INTDIR)\CCM_ReceptacleS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_ReceptacleS.obj" $(SOURCE)

SOURCE="CIAO_SwapExecC.cpp"

"$(INTDIR)\CIAO_SwapExecC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CIAO_SwapExecC.obj" $(SOURCE)

SOURCE="CIAO_UpgradeableContextC.cpp"

"$(INTDIR)\CIAO_UpgradeableContextC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CIAO_UpgradeableContextC.obj" $(SOURCE)

SOURCE="Cookies.cpp"

"$(INTDIR)\Cookies.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Cookies.obj" $(SOURCE)

SOURCE="ComponentsS.cpp"

"$(INTDIR)\ComponentsS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ComponentsS.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="Security.idl"

InputPath=Security.idl

"SecurityC.h" "SecurityC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Security_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL "$(InputPath)"
<<

SOURCE="CCM_Context.idl"

InputPath=CCM_Context.idl

"CCM_ContextC.h" "CCM_ContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_Context_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CCM_CCMException.idl"

InputPath=CCM_CCMException.idl

"CCM_CCMExceptionC.h" "CCM_CCMExceptionC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_CCMException_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CCM_EntityComponent.idl"

InputPath=CCM_EntityComponent.idl

"CCM_EntityComponentC.h" "CCM_EntityComponentC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_EntityComponent_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CCM_EntityContext.idl"

InputPath=CCM_EntityContext.idl

"CCM_EntityContextC.h" "CCM_EntityContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_EntityContext_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CIAO_SwapExec.idl"

InputPath=CIAO_SwapExec.idl

"CIAO_SwapExecC.h" "CIAO_SwapExecC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CIAO_SwapExec_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CIAO_UpgradeableContext.idl"

InputPath=CIAO_UpgradeableContext.idl

"CIAO_UpgradeableContextC.h" "CIAO_UpgradeableContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CIAO_UpgradeableContext_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CCM_CCM2Context.idl"

InputPath=CCM_CCM2Context.idl

"CCM_CCM2ContextC.inl" "CCM_CCM2ContextC.h" "CCM_CCM2ContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_CCM2Context_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_ProxyHomeRegistration.idl"

InputPath=CCM_ProxyHomeRegistration.idl

"CCM_ProxyHomeRegistrationC.inl" "CCM_ProxyHomeRegistrationC.h" "CCM_ProxyHomeRegistrationC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_ProxyHomeRegistration_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_Session2Context.idl"

InputPath=CCM_Session2Context.idl

"CCM_Session2ContextC.inl" "CCM_Session2ContextC.h" "CCM_Session2ContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_Session2Context_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_Transaction.idl"

InputPath=CCM_Transaction.idl

"CCM_TransactionC.inl" "CCM_TransactionC.h" "CCM_TransactionC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_Transaction_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CosPersistentState.idl"

InputPath=CosPersistentState.idl

"CosPersistentStateC.inl" "CosPersistentStateC.h" "CosPersistentStateC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CosPersistentState_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_Container.idl"

InputPath=CCM_Container.idl

"CCM_ContainerC.inl" "CCM_ContainerC.h" "CCM_ContainerC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_Container_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_SessionContext.idl"

InputPath=CCM_SessionContext.idl

"CCM_SessionContextC.inl" "CCM_SessionContextC.h" "CCM_SessionContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_SessionContext_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_SessionComponent.idl"

InputPath=CCM_SessionComponent.idl

"CCM_SessionComponentC.inl" "CCM_SessionComponentC.h" "CCM_SessionComponentC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_SessionComponent_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_Container_Ex.idl"

InputPath=CCM_Container_Ex.idl

"CCM_Container_ExC.inl" "CCM_Container_ExC.h" "CCM_Container_ExC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_Container_Ex_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_StateIdFactory.idl"

InputPath=CCM_StateIdFactory.idl

"CCM_StateIdFactoryC.inl" "CCM_StateIdFactoryC.h" "CCM_StateIdFactoryC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_StateIdFactory_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_Entity2Context.idl"

InputPath=CCM_Entity2Context.idl

"CCM_Entity2ContextC.inl" "CCM_Entity2ContextC.h" "CCM_Entity2ContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_Entity2Context_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_ComponentId.idl"

InputPath=CCM_ComponentId.idl

"CCM_ComponentIdC.inl" "CCM_ComponentIdC.h" "CCM_ComponentIdC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_ComponentId_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_ExecutorLocator.idl"

InputPath=CCM_ExecutorLocator.idl

"CCM_ExecutorLocatorC.inl" "CCM_ExecutorLocatorC.h" "CCM_ExecutorLocatorC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_ExecutorLocator_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_EnterpriseComponent.idl"

InputPath=CCM_EnterpriseComponent.idl

"CCM_EnterpriseComponentC.inl" "CCM_EnterpriseComponentC.h" "CCM_EnterpriseComponentC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_EnterpriseComponent_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_SessionSynchronization.idl"

InputPath=CCM_SessionSynchronization.idl

"CCM_SessionSynchronizationC.inl" "CCM_SessionSynchronizationC.h" "CCM_SessionSynchronizationC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_SessionSynchronization_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_HomeRegistration.idl"

InputPath=CCM_HomeRegistration.idl

"CCM_HomeRegistrationC.inl" "CCM_HomeRegistrationC.h" "CCM_HomeRegistrationC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_HomeRegistration_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_HomeExecutorBase.idl"

InputPath=CCM_HomeExecutorBase.idl

"CCM_HomeExecutorBaseC.inl" "CCM_HomeExecutorBaseC.h" "CCM_HomeExecutorBaseC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_HomeExecutorBase_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="Security.idl"

InputPath=Security.idl

"SecurityC.h" "SecurityC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Security_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL "$(InputPath)"
<<

SOURCE="CCM_Context.idl"

InputPath=CCM_Context.idl

"CCM_ContextC.h" "CCM_ContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_Context_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CCM_CCMException.idl"

InputPath=CCM_CCMException.idl

"CCM_CCMExceptionC.h" "CCM_CCMExceptionC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_CCMException_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CCM_EntityComponent.idl"

InputPath=CCM_EntityComponent.idl

"CCM_EntityComponentC.h" "CCM_EntityComponentC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_EntityComponent_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CCM_EntityContext.idl"

InputPath=CCM_EntityContext.idl

"CCM_EntityContextC.h" "CCM_EntityContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_EntityContext_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CIAO_SwapExec.idl"

InputPath=CIAO_SwapExec.idl

"CIAO_SwapExecC.h" "CIAO_SwapExecC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CIAO_SwapExec_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CIAO_UpgradeableContext.idl"

InputPath=CIAO_UpgradeableContext.idl

"CIAO_UpgradeableContextC.h" "CIAO_UpgradeableContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CIAO_UpgradeableContext_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CCM_CCM2Context.idl"

InputPath=CCM_CCM2Context.idl

"CCM_CCM2ContextC.inl" "CCM_CCM2ContextC.h" "CCM_CCM2ContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_CCM2Context_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_ProxyHomeRegistration.idl"

InputPath=CCM_ProxyHomeRegistration.idl

"CCM_ProxyHomeRegistrationC.inl" "CCM_ProxyHomeRegistrationC.h" "CCM_ProxyHomeRegistrationC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_ProxyHomeRegistration_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_Session2Context.idl"

InputPath=CCM_Session2Context.idl

"CCM_Session2ContextC.inl" "CCM_Session2ContextC.h" "CCM_Session2ContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_Session2Context_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_Transaction.idl"

InputPath=CCM_Transaction.idl

"CCM_TransactionC.inl" "CCM_TransactionC.h" "CCM_TransactionC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_Transaction_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CosPersistentState.idl"

InputPath=CosPersistentState.idl

"CosPersistentStateC.inl" "CosPersistentStateC.h" "CosPersistentStateC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CosPersistentState_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_Container.idl"

InputPath=CCM_Container.idl

"CCM_ContainerC.inl" "CCM_ContainerC.h" "CCM_ContainerC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_Container_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_SessionContext.idl"

InputPath=CCM_SessionContext.idl

"CCM_SessionContextC.inl" "CCM_SessionContextC.h" "CCM_SessionContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_SessionContext_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_SessionComponent.idl"

InputPath=CCM_SessionComponent.idl

"CCM_SessionComponentC.inl" "CCM_SessionComponentC.h" "CCM_SessionComponentC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_SessionComponent_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_Container_Ex.idl"

InputPath=CCM_Container_Ex.idl

"CCM_Container_ExC.inl" "CCM_Container_ExC.h" "CCM_Container_ExC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_Container_Ex_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_StateIdFactory.idl"

InputPath=CCM_StateIdFactory.idl

"CCM_StateIdFactoryC.inl" "CCM_StateIdFactoryC.h" "CCM_StateIdFactoryC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_StateIdFactory_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_Entity2Context.idl"

InputPath=CCM_Entity2Context.idl

"CCM_Entity2ContextC.inl" "CCM_Entity2ContextC.h" "CCM_Entity2ContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_Entity2Context_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_ComponentId.idl"

InputPath=CCM_ComponentId.idl

"CCM_ComponentIdC.inl" "CCM_ComponentIdC.h" "CCM_ComponentIdC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_ComponentId_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_ExecutorLocator.idl"

InputPath=CCM_ExecutorLocator.idl

"CCM_ExecutorLocatorC.inl" "CCM_ExecutorLocatorC.h" "CCM_ExecutorLocatorC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_ExecutorLocator_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_EnterpriseComponent.idl"

InputPath=CCM_EnterpriseComponent.idl

"CCM_EnterpriseComponentC.inl" "CCM_EnterpriseComponentC.h" "CCM_EnterpriseComponentC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_EnterpriseComponent_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_SessionSynchronization.idl"

InputPath=CCM_SessionSynchronization.idl

"CCM_SessionSynchronizationC.inl" "CCM_SessionSynchronizationC.h" "CCM_SessionSynchronizationC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_SessionSynchronization_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_HomeRegistration.idl"

InputPath=CCM_HomeRegistration.idl

"CCM_HomeRegistrationC.inl" "CCM_HomeRegistrationC.h" "CCM_HomeRegistrationC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_HomeRegistration_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_HomeExecutorBase.idl"

InputPath=CCM_HomeExecutorBase.idl

"CCM_HomeExecutorBaseC.inl" "CCM_HomeExecutorBaseC.h" "CCM_HomeExecutorBaseC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_HomeExecutorBase_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="Security.idl"

InputPath=Security.idl

"SecurityC.h" "SecurityC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Security_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL "$(InputPath)"
<<

SOURCE="CCM_Context.idl"

InputPath=CCM_Context.idl

"CCM_ContextC.h" "CCM_ContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_Context_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CCM_CCMException.idl"

InputPath=CCM_CCMException.idl

"CCM_CCMExceptionC.h" "CCM_CCMExceptionC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_CCMException_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CCM_EntityComponent.idl"

InputPath=CCM_EntityComponent.idl

"CCM_EntityComponentC.h" "CCM_EntityComponentC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_EntityComponent_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CCM_EntityContext.idl"

InputPath=CCM_EntityContext.idl

"CCM_EntityContextC.h" "CCM_EntityContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_EntityContext_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CIAO_SwapExec.idl"

InputPath=CIAO_SwapExec.idl

"CIAO_SwapExecC.h" "CIAO_SwapExecC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CIAO_SwapExec_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CIAO_UpgradeableContext.idl"

InputPath=CIAO_UpgradeableContext.idl

"CIAO_UpgradeableContextC.h" "CIAO_UpgradeableContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CIAO_UpgradeableContext_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CCM_CCM2Context.idl"

InputPath=CCM_CCM2Context.idl

"CCM_CCM2ContextC.inl" "CCM_CCM2ContextC.h" "CCM_CCM2ContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_CCM2Context_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_ProxyHomeRegistration.idl"

InputPath=CCM_ProxyHomeRegistration.idl

"CCM_ProxyHomeRegistrationC.inl" "CCM_ProxyHomeRegistrationC.h" "CCM_ProxyHomeRegistrationC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_ProxyHomeRegistration_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_Session2Context.idl"

InputPath=CCM_Session2Context.idl

"CCM_Session2ContextC.inl" "CCM_Session2ContextC.h" "CCM_Session2ContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_Session2Context_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_Transaction.idl"

InputPath=CCM_Transaction.idl

"CCM_TransactionC.inl" "CCM_TransactionC.h" "CCM_TransactionC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_Transaction_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CosPersistentState.idl"

InputPath=CosPersistentState.idl

"CosPersistentStateC.inl" "CosPersistentStateC.h" "CosPersistentStateC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CosPersistentState_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_Container.idl"

InputPath=CCM_Container.idl

"CCM_ContainerC.inl" "CCM_ContainerC.h" "CCM_ContainerC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_Container_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_SessionContext.idl"

InputPath=CCM_SessionContext.idl

"CCM_SessionContextC.inl" "CCM_SessionContextC.h" "CCM_SessionContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_SessionContext_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_SessionComponent.idl"

InputPath=CCM_SessionComponent.idl

"CCM_SessionComponentC.inl" "CCM_SessionComponentC.h" "CCM_SessionComponentC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_SessionComponent_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_Container_Ex.idl"

InputPath=CCM_Container_Ex.idl

"CCM_Container_ExC.inl" "CCM_Container_ExC.h" "CCM_Container_ExC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_Container_Ex_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_StateIdFactory.idl"

InputPath=CCM_StateIdFactory.idl

"CCM_StateIdFactoryC.inl" "CCM_StateIdFactoryC.h" "CCM_StateIdFactoryC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_StateIdFactory_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_Entity2Context.idl"

InputPath=CCM_Entity2Context.idl

"CCM_Entity2ContextC.inl" "CCM_Entity2ContextC.h" "CCM_Entity2ContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_Entity2Context_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_ComponentId.idl"

InputPath=CCM_ComponentId.idl

"CCM_ComponentIdC.inl" "CCM_ComponentIdC.h" "CCM_ComponentIdC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_ComponentId_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_ExecutorLocator.idl"

InputPath=CCM_ExecutorLocator.idl

"CCM_ExecutorLocatorC.inl" "CCM_ExecutorLocatorC.h" "CCM_ExecutorLocatorC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_ExecutorLocator_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_EnterpriseComponent.idl"

InputPath=CCM_EnterpriseComponent.idl

"CCM_EnterpriseComponentC.inl" "CCM_EnterpriseComponentC.h" "CCM_EnterpriseComponentC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_EnterpriseComponent_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_SessionSynchronization.idl"

InputPath=CCM_SessionSynchronization.idl

"CCM_SessionSynchronizationC.inl" "CCM_SessionSynchronizationC.h" "CCM_SessionSynchronizationC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_SessionSynchronization_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_HomeRegistration.idl"

InputPath=CCM_HomeRegistration.idl

"CCM_HomeRegistrationC.inl" "CCM_HomeRegistrationC.h" "CCM_HomeRegistrationC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_HomeRegistration_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_HomeExecutorBase.idl"

InputPath=CCM_HomeExecutorBase.idl

"CCM_HomeExecutorBaseC.inl" "CCM_HomeExecutorBaseC.h" "CCM_HomeExecutorBaseC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_HomeExecutorBase_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="Security.idl"

InputPath=Security.idl

"SecurityC.h" "SecurityC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Security_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci -Wb,versioning_begin=TAO_BEGIN_VERSIONED_NAMESPACE_DECL -Wb,versioning_end=TAO_END_VERSIONED_NAMESPACE_DECL "$(InputPath)"
<<

SOURCE="CCM_Context.idl"

InputPath=CCM_Context.idl

"CCM_ContextC.h" "CCM_ContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_Context_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CCM_CCMException.idl"

InputPath=CCM_CCMException.idl

"CCM_CCMExceptionC.h" "CCM_CCMExceptionC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_CCMException_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CCM_EntityComponent.idl"

InputPath=CCM_EntityComponent.idl

"CCM_EntityComponentC.h" "CCM_EntityComponentC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_EntityComponent_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CCM_EntityContext.idl"

InputPath=CCM_EntityContext.idl

"CCM_EntityContextC.h" "CCM_EntityContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_EntityContext_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CIAO_SwapExec.idl"

InputPath=CIAO_SwapExec.idl

"CIAO_SwapExecC.h" "CIAO_SwapExecC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CIAO_SwapExec_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CIAO_UpgradeableContext.idl"

InputPath=CIAO_UpgradeableContext.idl

"CIAO_UpgradeableContextC.h" "CIAO_UpgradeableContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CIAO_UpgradeableContext_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CCM_CCM2Context.idl"

InputPath=CCM_CCM2Context.idl

"CCM_CCM2ContextC.inl" "CCM_CCM2ContextC.h" "CCM_CCM2ContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_CCM2Context_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_ProxyHomeRegistration.idl"

InputPath=CCM_ProxyHomeRegistration.idl

"CCM_ProxyHomeRegistrationC.inl" "CCM_ProxyHomeRegistrationC.h" "CCM_ProxyHomeRegistrationC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_ProxyHomeRegistration_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_Session2Context.idl"

InputPath=CCM_Session2Context.idl

"CCM_Session2ContextC.inl" "CCM_Session2ContextC.h" "CCM_Session2ContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_Session2Context_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_Transaction.idl"

InputPath=CCM_Transaction.idl

"CCM_TransactionC.inl" "CCM_TransactionC.h" "CCM_TransactionC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_Transaction_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CosPersistentState.idl"

InputPath=CosPersistentState.idl

"CosPersistentStateC.inl" "CosPersistentStateC.h" "CosPersistentStateC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CosPersistentState_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_Container.idl"

InputPath=CCM_Container.idl

"CCM_ContainerC.inl" "CCM_ContainerC.h" "CCM_ContainerC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_Container_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_SessionContext.idl"

InputPath=CCM_SessionContext.idl

"CCM_SessionContextC.inl" "CCM_SessionContextC.h" "CCM_SessionContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_SessionContext_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_SessionComponent.idl"

InputPath=CCM_SessionComponent.idl

"CCM_SessionComponentC.inl" "CCM_SessionComponentC.h" "CCM_SessionComponentC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_SessionComponent_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_Container_Ex.idl"

InputPath=CCM_Container_Ex.idl

"CCM_Container_ExC.inl" "CCM_Container_ExC.h" "CCM_Container_ExC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_Container_Ex_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_StateIdFactory.idl"

InputPath=CCM_StateIdFactory.idl

"CCM_StateIdFactoryC.inl" "CCM_StateIdFactoryC.h" "CCM_StateIdFactoryC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_StateIdFactory_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_Entity2Context.idl"

InputPath=CCM_Entity2Context.idl

"CCM_Entity2ContextC.inl" "CCM_Entity2ContextC.h" "CCM_Entity2ContextC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_Entity2Context_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_ComponentId.idl"

InputPath=CCM_ComponentId.idl

"CCM_ComponentIdC.inl" "CCM_ComponentIdC.h" "CCM_ComponentIdC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_ComponentId_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_ExecutorLocator.idl"

InputPath=CCM_ExecutorLocator.idl

"CCM_ExecutorLocatorC.inl" "CCM_ExecutorLocatorC.h" "CCM_ExecutorLocatorC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_ExecutorLocator_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_EnterpriseComponent.idl"

InputPath=CCM_EnterpriseComponent.idl

"CCM_EnterpriseComponentC.inl" "CCM_EnterpriseComponentC.h" "CCM_EnterpriseComponentC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_EnterpriseComponent_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_SessionSynchronization.idl"

InputPath=CCM_SessionSynchronization.idl

"CCM_SessionSynchronizationC.inl" "CCM_SessionSynchronizationC.h" "CCM_SessionSynchronizationC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_SessionSynchronization_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_HomeRegistration.idl"

InputPath=CCM_HomeRegistration.idl

"CCM_HomeRegistrationC.inl" "CCM_HomeRegistrationC.h" "CCM_HomeRegistrationC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_HomeRegistration_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_HomeExecutorBase.idl"

InputPath=CCM_HomeExecutorBase.idl

"CCM_HomeExecutorBaseC.inl" "CCM_HomeExecutorBaseC.h" "CCM_HomeExecutorBaseC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_HomeExecutorBase_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -I..\../orbsvcs -I.. -I../ciao -Wb,export_include=CIAO_Container_Export.h -Wb,export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CIAO_Container.dep")
	@echo Using "Makefile.CIAO_Container.dep"
!ELSE
	@echo Warning: cannot find "Makefile.CIAO_Container.dep"
!ENDIF
!ENDIF

