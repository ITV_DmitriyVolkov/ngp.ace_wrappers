// -*- C++ -*-
//=============================================================================
/**
 *  @file  CIAO_Config.h
 *
 *  $Id: CIAO_Config.h 14 2007-02-01 15:49:12Z mitza $
 *
 *  CIAO compile time configuration file.
 *
 *  @author  Bala Natarajan <bala @ dre.vanderbilt.edu>
 */
//=============================================================================
#ifndef CIAO_CONFIG_H
#define CIAO_CONFIG_H

#if !defined (CIAO_DEFAULT_MAP_SIZE)
# define CIAO_DEFAULT_MAP_SIZE 64
#endif /* CIAO_DEFAULT_MAP_SIZE */

#endif /*CIAO_CONFIG_H*/
