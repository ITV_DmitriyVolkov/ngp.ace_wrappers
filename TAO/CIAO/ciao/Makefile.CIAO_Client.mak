# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.CIAO_Client.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "CCM_BaseC.h" "CCM_BaseC.cpp" "CCM_ComponentC.h" "CCM_ComponentC.cpp" "CCM_CookieC.inl" "CCM_CookieC.h" "CCM_CookieC.cpp" "CCM_EnumerationC.inl" "CCM_EnumerationC.h" "CCM_EnumerationC.cpp" "ComponentsC.h" "ComponentsC.cpp" "CCM_HomeConfigurationC.inl" "CCM_HomeConfigurationS.inl" "CCM_HomeConfigurationC.h" "CCM_HomeConfigurationC.cpp" "CCM_StandardConfiguratorC.inl" "CCM_StandardConfiguratorS.inl" "CCM_StandardConfiguratorC.h" "CCM_StandardConfiguratorC.cpp" "CCM_HomeC.inl" "CCM_HomeS.inl" "CCM_HomeC.h" "CCM_HomeC.cpp" "CCM_ObjectC.inl" "CCM_ObjectS.inl" "CCM_ObjectC.h" "CCM_ObjectC.cpp" "CCM_PrimaryKeyBaseC.inl" "CCM_PrimaryKeyBaseS.inl" "CCM_PrimaryKeyBaseC.h" "CCM_PrimaryKeyBaseC.cpp" "CCM_HomeFinderC.inl" "CCM_HomeFinderC.h" "CCM_HomeFinderC.cpp" "CCM_ConfiguratorC.inl" "CCM_ConfiguratorC.h" "CCM_ConfiguratorC.cpp" "CCM_KeylessCCMHomeC.inl" "CCM_KeylessCCMHomeC.h" "CCM_KeylessCCMHomeC.cpp" "CCM_NavigationC.inl" "CCM_NavigationC.h" "CCM_NavigationC.cpp" "CCM_ReceptacleC.inl" "CCM_ReceptacleC.h" "CCM_ReceptacleC.cpp" "CCM_EventsC.inl" "CCM_EventsS.inl" "CCM_EventsC.h" "CCM_EventsC.cpp" "CCM_EventBaseC.inl" "CCM_EventBaseS.inl" "CCM_EventBaseC.h" "CCM_EventBaseC.cpp" "CCM_EventConsumerBaseC.inl" "CCM_EventConsumerBaseS.inl" "CCM_EventConsumerBaseC.h" "CCM_EventConsumerBaseC.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\..\lib
INTDIR=Debug\CIAO_Client\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\lib\CIAO_Clientd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DCIAO_CLIENT_BUILD_DLL -f "Makefile.CIAO_Client.dep" "CCM_BaseC.cpp" "CCM_EventsC.cpp" "CCM_EventBaseC.cpp" "CCM_EventConsumerBaseC.cpp" "CCM_ComponentC.cpp" "CCM_EnumerationC.cpp" "CCM_HomeC.cpp" "CCM_ConfiguratorC.cpp" "CCM_HomeConfigurationC.cpp" "CCM_KeylessCCMHomeC.cpp" "CCM_StandardConfiguratorC.cpp" "CCM_HomeFinderC.cpp" "CCM_NavigationC.cpp" "CCM_ReceptacleC.cpp" "CCM_CookieC.cpp" "CCM_ObjectC.cpp" "CCM_PrimaryKeyBaseC.cpp" "Client_init.cpp" "ComponentsC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_Clientd.pdb"
	-@del /f/q "..\..\..\lib\CIAO_Clientd.dll"
	-@del /f/q "$(OUTDIR)\CIAO_Clientd.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Clientd.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Clientd.ilk"
	-@del /f/q "CCM_BaseC.h"
	-@del /f/q "CCM_BaseC.cpp"
	-@del /f/q "CCM_ComponentC.h"
	-@del /f/q "CCM_ComponentC.cpp"
	-@del /f/q "CCM_CookieC.inl"
	-@del /f/q "CCM_CookieC.h"
	-@del /f/q "CCM_CookieC.cpp"
	-@del /f/q "CCM_EnumerationC.inl"
	-@del /f/q "CCM_EnumerationC.h"
	-@del /f/q "CCM_EnumerationC.cpp"
	-@del /f/q "ComponentsC.h"
	-@del /f/q "ComponentsC.cpp"
	-@del /f/q "CCM_HomeConfigurationC.inl"
	-@del /f/q "CCM_HomeConfigurationS.inl"
	-@del /f/q "CCM_HomeConfigurationC.h"
	-@del /f/q "CCM_HomeConfigurationC.cpp"
	-@del /f/q "CCM_StandardConfiguratorC.inl"
	-@del /f/q "CCM_StandardConfiguratorS.inl"
	-@del /f/q "CCM_StandardConfiguratorC.h"
	-@del /f/q "CCM_StandardConfiguratorC.cpp"
	-@del /f/q "CCM_HomeC.inl"
	-@del /f/q "CCM_HomeS.inl"
	-@del /f/q "CCM_HomeC.h"
	-@del /f/q "CCM_HomeC.cpp"
	-@del /f/q "CCM_ObjectC.inl"
	-@del /f/q "CCM_ObjectS.inl"
	-@del /f/q "CCM_ObjectC.h"
	-@del /f/q "CCM_ObjectC.cpp"
	-@del /f/q "CCM_PrimaryKeyBaseC.inl"
	-@del /f/q "CCM_PrimaryKeyBaseS.inl"
	-@del /f/q "CCM_PrimaryKeyBaseC.h"
	-@del /f/q "CCM_PrimaryKeyBaseC.cpp"
	-@del /f/q "CCM_HomeFinderC.inl"
	-@del /f/q "CCM_HomeFinderC.h"
	-@del /f/q "CCM_HomeFinderC.cpp"
	-@del /f/q "CCM_ConfiguratorC.inl"
	-@del /f/q "CCM_ConfiguratorC.h"
	-@del /f/q "CCM_ConfiguratorC.cpp"
	-@del /f/q "CCM_KeylessCCMHomeC.inl"
	-@del /f/q "CCM_KeylessCCMHomeC.h"
	-@del /f/q "CCM_KeylessCCMHomeC.cpp"
	-@del /f/q "CCM_NavigationC.inl"
	-@del /f/q "CCM_NavigationC.h"
	-@del /f/q "CCM_NavigationC.cpp"
	-@del /f/q "CCM_ReceptacleC.inl"
	-@del /f/q "CCM_ReceptacleC.h"
	-@del /f/q "CCM_ReceptacleC.cpp"
	-@del /f/q "CCM_EventsC.inl"
	-@del /f/q "CCM_EventsS.inl"
	-@del /f/q "CCM_EventsC.h"
	-@del /f/q "CCM_EventsC.cpp"
	-@del /f/q "CCM_EventBaseC.inl"
	-@del /f/q "CCM_EventBaseS.inl"
	-@del /f/q "CCM_EventBaseC.h"
	-@del /f/q "CCM_EventBaseC.cpp"
	-@del /f/q "CCM_EventConsumerBaseC.inl"
	-@del /f/q "CCM_EventConsumerBaseS.inl"
	-@del /f/q "CCM_EventConsumerBaseC.h"
	-@del /f/q "CCM_EventConsumerBaseC.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\CIAO_Client\$(NULL)" mkdir "Debug\CIAO_Client"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\.." /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D CIAO_CLIENT_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_Valuetyped.lib TAO_IFR_Clientd.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\..\lib\CIAO_Clientd.pdb" /machine:IA64 /out:"..\..\..\lib\CIAO_Clientd.dll" /implib:"$(OUTDIR)\CIAO_Clientd.lib"
LINK32_OBJS= \
	"$(INTDIR)\CCM_BaseC.obj" \
	"$(INTDIR)\CCM_EventsC.obj" \
	"$(INTDIR)\CCM_EventBaseC.obj" \
	"$(INTDIR)\CCM_EventConsumerBaseC.obj" \
	"$(INTDIR)\CCM_ComponentC.obj" \
	"$(INTDIR)\CCM_EnumerationC.obj" \
	"$(INTDIR)\CCM_HomeC.obj" \
	"$(INTDIR)\CCM_ConfiguratorC.obj" \
	"$(INTDIR)\CCM_HomeConfigurationC.obj" \
	"$(INTDIR)\CCM_KeylessCCMHomeC.obj" \
	"$(INTDIR)\CCM_StandardConfiguratorC.obj" \
	"$(INTDIR)\CCM_HomeFinderC.obj" \
	"$(INTDIR)\CCM_NavigationC.obj" \
	"$(INTDIR)\CCM_ReceptacleC.obj" \
	"$(INTDIR)\CCM_CookieC.obj" \
	"$(INTDIR)\CCM_ObjectC.obj" \
	"$(INTDIR)\CCM_PrimaryKeyBaseC.obj" \
	"$(INTDIR)\Client_init.obj" \
	"$(INTDIR)\ComponentsC.obj"

"..\..\..\lib\CIAO_Clientd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\lib\CIAO_Clientd.dll.manifest" mt.exe -manifest "..\..\..\lib\CIAO_Clientd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\..\lib
INTDIR=Release\CIAO_Client\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\lib\CIAO_Client.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DCIAO_CLIENT_BUILD_DLL -f "Makefile.CIAO_Client.dep" "CCM_BaseC.cpp" "CCM_EventsC.cpp" "CCM_EventBaseC.cpp" "CCM_EventConsumerBaseC.cpp" "CCM_ComponentC.cpp" "CCM_EnumerationC.cpp" "CCM_HomeC.cpp" "CCM_ConfiguratorC.cpp" "CCM_HomeConfigurationC.cpp" "CCM_KeylessCCMHomeC.cpp" "CCM_StandardConfiguratorC.cpp" "CCM_HomeFinderC.cpp" "CCM_NavigationC.cpp" "CCM_ReceptacleC.cpp" "CCM_CookieC.cpp" "CCM_ObjectC.cpp" "CCM_PrimaryKeyBaseC.cpp" "Client_init.cpp" "ComponentsC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\..\lib\CIAO_Client.dll"
	-@del /f/q "$(OUTDIR)\CIAO_Client.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Client.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Client.ilk"
	-@del /f/q "CCM_BaseC.h"
	-@del /f/q "CCM_BaseC.cpp"
	-@del /f/q "CCM_ComponentC.h"
	-@del /f/q "CCM_ComponentC.cpp"
	-@del /f/q "CCM_CookieC.inl"
	-@del /f/q "CCM_CookieC.h"
	-@del /f/q "CCM_CookieC.cpp"
	-@del /f/q "CCM_EnumerationC.inl"
	-@del /f/q "CCM_EnumerationC.h"
	-@del /f/q "CCM_EnumerationC.cpp"
	-@del /f/q "ComponentsC.h"
	-@del /f/q "ComponentsC.cpp"
	-@del /f/q "CCM_HomeConfigurationC.inl"
	-@del /f/q "CCM_HomeConfigurationS.inl"
	-@del /f/q "CCM_HomeConfigurationC.h"
	-@del /f/q "CCM_HomeConfigurationC.cpp"
	-@del /f/q "CCM_StandardConfiguratorC.inl"
	-@del /f/q "CCM_StandardConfiguratorS.inl"
	-@del /f/q "CCM_StandardConfiguratorC.h"
	-@del /f/q "CCM_StandardConfiguratorC.cpp"
	-@del /f/q "CCM_HomeC.inl"
	-@del /f/q "CCM_HomeS.inl"
	-@del /f/q "CCM_HomeC.h"
	-@del /f/q "CCM_HomeC.cpp"
	-@del /f/q "CCM_ObjectC.inl"
	-@del /f/q "CCM_ObjectS.inl"
	-@del /f/q "CCM_ObjectC.h"
	-@del /f/q "CCM_ObjectC.cpp"
	-@del /f/q "CCM_PrimaryKeyBaseC.inl"
	-@del /f/q "CCM_PrimaryKeyBaseS.inl"
	-@del /f/q "CCM_PrimaryKeyBaseC.h"
	-@del /f/q "CCM_PrimaryKeyBaseC.cpp"
	-@del /f/q "CCM_HomeFinderC.inl"
	-@del /f/q "CCM_HomeFinderC.h"
	-@del /f/q "CCM_HomeFinderC.cpp"
	-@del /f/q "CCM_ConfiguratorC.inl"
	-@del /f/q "CCM_ConfiguratorC.h"
	-@del /f/q "CCM_ConfiguratorC.cpp"
	-@del /f/q "CCM_KeylessCCMHomeC.inl"
	-@del /f/q "CCM_KeylessCCMHomeC.h"
	-@del /f/q "CCM_KeylessCCMHomeC.cpp"
	-@del /f/q "CCM_NavigationC.inl"
	-@del /f/q "CCM_NavigationC.h"
	-@del /f/q "CCM_NavigationC.cpp"
	-@del /f/q "CCM_ReceptacleC.inl"
	-@del /f/q "CCM_ReceptacleC.h"
	-@del /f/q "CCM_ReceptacleC.cpp"
	-@del /f/q "CCM_EventsC.inl"
	-@del /f/q "CCM_EventsS.inl"
	-@del /f/q "CCM_EventsC.h"
	-@del /f/q "CCM_EventsC.cpp"
	-@del /f/q "CCM_EventBaseC.inl"
	-@del /f/q "CCM_EventBaseS.inl"
	-@del /f/q "CCM_EventBaseC.h"
	-@del /f/q "CCM_EventBaseC.cpp"
	-@del /f/q "CCM_EventConsumerBaseC.inl"
	-@del /f/q "CCM_EventConsumerBaseS.inl"
	-@del /f/q "CCM_EventConsumerBaseC.h"
	-@del /f/q "CCM_EventConsumerBaseC.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\CIAO_Client\$(NULL)" mkdir "Release\CIAO_Client"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\.." /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D CIAO_CLIENT_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_Valuetype.lib TAO_IFR_Client.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\..\lib\CIAO_Client.dll" /implib:"$(OUTDIR)\CIAO_Client.lib"
LINK32_OBJS= \
	"$(INTDIR)\CCM_BaseC.obj" \
	"$(INTDIR)\CCM_EventsC.obj" \
	"$(INTDIR)\CCM_EventBaseC.obj" \
	"$(INTDIR)\CCM_EventConsumerBaseC.obj" \
	"$(INTDIR)\CCM_ComponentC.obj" \
	"$(INTDIR)\CCM_EnumerationC.obj" \
	"$(INTDIR)\CCM_HomeC.obj" \
	"$(INTDIR)\CCM_ConfiguratorC.obj" \
	"$(INTDIR)\CCM_HomeConfigurationC.obj" \
	"$(INTDIR)\CCM_KeylessCCMHomeC.obj" \
	"$(INTDIR)\CCM_StandardConfiguratorC.obj" \
	"$(INTDIR)\CCM_HomeFinderC.obj" \
	"$(INTDIR)\CCM_NavigationC.obj" \
	"$(INTDIR)\CCM_ReceptacleC.obj" \
	"$(INTDIR)\CCM_CookieC.obj" \
	"$(INTDIR)\CCM_ObjectC.obj" \
	"$(INTDIR)\CCM_PrimaryKeyBaseC.obj" \
	"$(INTDIR)\Client_init.obj" \
	"$(INTDIR)\ComponentsC.obj"

"..\..\..\lib\CIAO_Client.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\lib\CIAO_Client.dll.manifest" mt.exe -manifest "..\..\..\lib\CIAO_Client.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\..\lib
INTDIR=Static_Debug\CIAO_Client\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CIAO_Clientsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CIAO_Client.dep" "CCM_BaseC.cpp" "CCM_EventsC.cpp" "CCM_EventBaseC.cpp" "CCM_EventConsumerBaseC.cpp" "CCM_ComponentC.cpp" "CCM_EnumerationC.cpp" "CCM_HomeC.cpp" "CCM_ConfiguratorC.cpp" "CCM_HomeConfigurationC.cpp" "CCM_KeylessCCMHomeC.cpp" "CCM_StandardConfiguratorC.cpp" "CCM_HomeFinderC.cpp" "CCM_NavigationC.cpp" "CCM_ReceptacleC.cpp" "CCM_CookieC.cpp" "CCM_ObjectC.cpp" "CCM_PrimaryKeyBaseC.cpp" "Client_init.cpp" "ComponentsC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_Clientsd.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Clientsd.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Clientsd.ilk"
	-@del /f/q "..\..\..\lib\CIAO_Clientsd.pdb"
	-@del /f/q "CCM_BaseC.h"
	-@del /f/q "CCM_BaseC.cpp"
	-@del /f/q "CCM_ComponentC.h"
	-@del /f/q "CCM_ComponentC.cpp"
	-@del /f/q "CCM_CookieC.inl"
	-@del /f/q "CCM_CookieC.h"
	-@del /f/q "CCM_CookieC.cpp"
	-@del /f/q "CCM_EnumerationC.inl"
	-@del /f/q "CCM_EnumerationC.h"
	-@del /f/q "CCM_EnumerationC.cpp"
	-@del /f/q "ComponentsC.h"
	-@del /f/q "ComponentsC.cpp"
	-@del /f/q "CCM_HomeConfigurationC.inl"
	-@del /f/q "CCM_HomeConfigurationS.inl"
	-@del /f/q "CCM_HomeConfigurationC.h"
	-@del /f/q "CCM_HomeConfigurationC.cpp"
	-@del /f/q "CCM_StandardConfiguratorC.inl"
	-@del /f/q "CCM_StandardConfiguratorS.inl"
	-@del /f/q "CCM_StandardConfiguratorC.h"
	-@del /f/q "CCM_StandardConfiguratorC.cpp"
	-@del /f/q "CCM_HomeC.inl"
	-@del /f/q "CCM_HomeS.inl"
	-@del /f/q "CCM_HomeC.h"
	-@del /f/q "CCM_HomeC.cpp"
	-@del /f/q "CCM_ObjectC.inl"
	-@del /f/q "CCM_ObjectS.inl"
	-@del /f/q "CCM_ObjectC.h"
	-@del /f/q "CCM_ObjectC.cpp"
	-@del /f/q "CCM_PrimaryKeyBaseC.inl"
	-@del /f/q "CCM_PrimaryKeyBaseS.inl"
	-@del /f/q "CCM_PrimaryKeyBaseC.h"
	-@del /f/q "CCM_PrimaryKeyBaseC.cpp"
	-@del /f/q "CCM_HomeFinderC.inl"
	-@del /f/q "CCM_HomeFinderC.h"
	-@del /f/q "CCM_HomeFinderC.cpp"
	-@del /f/q "CCM_ConfiguratorC.inl"
	-@del /f/q "CCM_ConfiguratorC.h"
	-@del /f/q "CCM_ConfiguratorC.cpp"
	-@del /f/q "CCM_KeylessCCMHomeC.inl"
	-@del /f/q "CCM_KeylessCCMHomeC.h"
	-@del /f/q "CCM_KeylessCCMHomeC.cpp"
	-@del /f/q "CCM_NavigationC.inl"
	-@del /f/q "CCM_NavigationC.h"
	-@del /f/q "CCM_NavigationC.cpp"
	-@del /f/q "CCM_ReceptacleC.inl"
	-@del /f/q "CCM_ReceptacleC.h"
	-@del /f/q "CCM_ReceptacleC.cpp"
	-@del /f/q "CCM_EventsC.inl"
	-@del /f/q "CCM_EventsS.inl"
	-@del /f/q "CCM_EventsC.h"
	-@del /f/q "CCM_EventsC.cpp"
	-@del /f/q "CCM_EventBaseC.inl"
	-@del /f/q "CCM_EventBaseS.inl"
	-@del /f/q "CCM_EventBaseC.h"
	-@del /f/q "CCM_EventBaseC.cpp"
	-@del /f/q "CCM_EventConsumerBaseC.inl"
	-@del /f/q "CCM_EventConsumerBaseS.inl"
	-@del /f/q "CCM_EventConsumerBaseC.h"
	-@del /f/q "CCM_EventConsumerBaseC.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\CIAO_Client\$(NULL)" mkdir "Static_Debug\CIAO_Client"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"..\..\..\lib\CIAO_Clientsd.pdb" /I "..\..\.." /I "..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\lib\CIAO_Clientsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\CCM_BaseC.obj" \
	"$(INTDIR)\CCM_EventsC.obj" \
	"$(INTDIR)\CCM_EventBaseC.obj" \
	"$(INTDIR)\CCM_EventConsumerBaseC.obj" \
	"$(INTDIR)\CCM_ComponentC.obj" \
	"$(INTDIR)\CCM_EnumerationC.obj" \
	"$(INTDIR)\CCM_HomeC.obj" \
	"$(INTDIR)\CCM_ConfiguratorC.obj" \
	"$(INTDIR)\CCM_HomeConfigurationC.obj" \
	"$(INTDIR)\CCM_KeylessCCMHomeC.obj" \
	"$(INTDIR)\CCM_StandardConfiguratorC.obj" \
	"$(INTDIR)\CCM_HomeFinderC.obj" \
	"$(INTDIR)\CCM_NavigationC.obj" \
	"$(INTDIR)\CCM_ReceptacleC.obj" \
	"$(INTDIR)\CCM_CookieC.obj" \
	"$(INTDIR)\CCM_ObjectC.obj" \
	"$(INTDIR)\CCM_PrimaryKeyBaseC.obj" \
	"$(INTDIR)\Client_init.obj" \
	"$(INTDIR)\ComponentsC.obj"

"$(OUTDIR)\CIAO_Clientsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CIAO_Clientsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\CIAO_Clientsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\..\lib
INTDIR=Static_Release\CIAO_Client\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CIAO_Clients.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CIAO_Client.dep" "CCM_BaseC.cpp" "CCM_EventsC.cpp" "CCM_EventBaseC.cpp" "CCM_EventConsumerBaseC.cpp" "CCM_ComponentC.cpp" "CCM_EnumerationC.cpp" "CCM_HomeC.cpp" "CCM_ConfiguratorC.cpp" "CCM_HomeConfigurationC.cpp" "CCM_KeylessCCMHomeC.cpp" "CCM_StandardConfiguratorC.cpp" "CCM_HomeFinderC.cpp" "CCM_NavigationC.cpp" "CCM_ReceptacleC.cpp" "CCM_CookieC.cpp" "CCM_ObjectC.cpp" "CCM_PrimaryKeyBaseC.cpp" "Client_init.cpp" "ComponentsC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_Clients.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Clients.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Clients.ilk"
	-@del /f/q "CCM_BaseC.h"
	-@del /f/q "CCM_BaseC.cpp"
	-@del /f/q "CCM_ComponentC.h"
	-@del /f/q "CCM_ComponentC.cpp"
	-@del /f/q "CCM_CookieC.inl"
	-@del /f/q "CCM_CookieC.h"
	-@del /f/q "CCM_CookieC.cpp"
	-@del /f/q "CCM_EnumerationC.inl"
	-@del /f/q "CCM_EnumerationC.h"
	-@del /f/q "CCM_EnumerationC.cpp"
	-@del /f/q "ComponentsC.h"
	-@del /f/q "ComponentsC.cpp"
	-@del /f/q "CCM_HomeConfigurationC.inl"
	-@del /f/q "CCM_HomeConfigurationS.inl"
	-@del /f/q "CCM_HomeConfigurationC.h"
	-@del /f/q "CCM_HomeConfigurationC.cpp"
	-@del /f/q "CCM_StandardConfiguratorC.inl"
	-@del /f/q "CCM_StandardConfiguratorS.inl"
	-@del /f/q "CCM_StandardConfiguratorC.h"
	-@del /f/q "CCM_StandardConfiguratorC.cpp"
	-@del /f/q "CCM_HomeC.inl"
	-@del /f/q "CCM_HomeS.inl"
	-@del /f/q "CCM_HomeC.h"
	-@del /f/q "CCM_HomeC.cpp"
	-@del /f/q "CCM_ObjectC.inl"
	-@del /f/q "CCM_ObjectS.inl"
	-@del /f/q "CCM_ObjectC.h"
	-@del /f/q "CCM_ObjectC.cpp"
	-@del /f/q "CCM_PrimaryKeyBaseC.inl"
	-@del /f/q "CCM_PrimaryKeyBaseS.inl"
	-@del /f/q "CCM_PrimaryKeyBaseC.h"
	-@del /f/q "CCM_PrimaryKeyBaseC.cpp"
	-@del /f/q "CCM_HomeFinderC.inl"
	-@del /f/q "CCM_HomeFinderC.h"
	-@del /f/q "CCM_HomeFinderC.cpp"
	-@del /f/q "CCM_ConfiguratorC.inl"
	-@del /f/q "CCM_ConfiguratorC.h"
	-@del /f/q "CCM_ConfiguratorC.cpp"
	-@del /f/q "CCM_KeylessCCMHomeC.inl"
	-@del /f/q "CCM_KeylessCCMHomeC.h"
	-@del /f/q "CCM_KeylessCCMHomeC.cpp"
	-@del /f/q "CCM_NavigationC.inl"
	-@del /f/q "CCM_NavigationC.h"
	-@del /f/q "CCM_NavigationC.cpp"
	-@del /f/q "CCM_ReceptacleC.inl"
	-@del /f/q "CCM_ReceptacleC.h"
	-@del /f/q "CCM_ReceptacleC.cpp"
	-@del /f/q "CCM_EventsC.inl"
	-@del /f/q "CCM_EventsS.inl"
	-@del /f/q "CCM_EventsC.h"
	-@del /f/q "CCM_EventsC.cpp"
	-@del /f/q "CCM_EventBaseC.inl"
	-@del /f/q "CCM_EventBaseS.inl"
	-@del /f/q "CCM_EventBaseC.h"
	-@del /f/q "CCM_EventBaseC.cpp"
	-@del /f/q "CCM_EventConsumerBaseC.inl"
	-@del /f/q "CCM_EventConsumerBaseS.inl"
	-@del /f/q "CCM_EventConsumerBaseC.h"
	-@del /f/q "CCM_EventConsumerBaseC.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\CIAO_Client\$(NULL)" mkdir "Static_Release\CIAO_Client"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\.." /I "..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\lib\CIAO_Clients.lib"
LINK32_OBJS= \
	"$(INTDIR)\CCM_BaseC.obj" \
	"$(INTDIR)\CCM_EventsC.obj" \
	"$(INTDIR)\CCM_EventBaseC.obj" \
	"$(INTDIR)\CCM_EventConsumerBaseC.obj" \
	"$(INTDIR)\CCM_ComponentC.obj" \
	"$(INTDIR)\CCM_EnumerationC.obj" \
	"$(INTDIR)\CCM_HomeC.obj" \
	"$(INTDIR)\CCM_ConfiguratorC.obj" \
	"$(INTDIR)\CCM_HomeConfigurationC.obj" \
	"$(INTDIR)\CCM_KeylessCCMHomeC.obj" \
	"$(INTDIR)\CCM_StandardConfiguratorC.obj" \
	"$(INTDIR)\CCM_HomeFinderC.obj" \
	"$(INTDIR)\CCM_NavigationC.obj" \
	"$(INTDIR)\CCM_ReceptacleC.obj" \
	"$(INTDIR)\CCM_CookieC.obj" \
	"$(INTDIR)\CCM_ObjectC.obj" \
	"$(INTDIR)\CCM_PrimaryKeyBaseC.obj" \
	"$(INTDIR)\Client_init.obj" \
	"$(INTDIR)\ComponentsC.obj"

"$(OUTDIR)\CIAO_Clients.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CIAO_Clients.lib.manifest" mt.exe -manifest "$(OUTDIR)\CIAO_Clients.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CIAO_Client.dep")
!INCLUDE "Makefile.CIAO_Client.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="CCM_BaseC.cpp"

"$(INTDIR)\CCM_BaseC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_BaseC.obj" $(SOURCE)

SOURCE="CCM_EventsC.cpp"

"$(INTDIR)\CCM_EventsC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_EventsC.obj" $(SOURCE)

SOURCE="CCM_EventBaseC.cpp"

"$(INTDIR)\CCM_EventBaseC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_EventBaseC.obj" $(SOURCE)

SOURCE="CCM_EventConsumerBaseC.cpp"

"$(INTDIR)\CCM_EventConsumerBaseC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_EventConsumerBaseC.obj" $(SOURCE)

SOURCE="CCM_ComponentC.cpp"

"$(INTDIR)\CCM_ComponentC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_ComponentC.obj" $(SOURCE)

SOURCE="CCM_EnumerationC.cpp"

"$(INTDIR)\CCM_EnumerationC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_EnumerationC.obj" $(SOURCE)

SOURCE="CCM_HomeC.cpp"

"$(INTDIR)\CCM_HomeC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_HomeC.obj" $(SOURCE)

SOURCE="CCM_ConfiguratorC.cpp"

"$(INTDIR)\CCM_ConfiguratorC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_ConfiguratorC.obj" $(SOURCE)

SOURCE="CCM_HomeConfigurationC.cpp"

"$(INTDIR)\CCM_HomeConfigurationC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_HomeConfigurationC.obj" $(SOURCE)

SOURCE="CCM_KeylessCCMHomeC.cpp"

"$(INTDIR)\CCM_KeylessCCMHomeC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_KeylessCCMHomeC.obj" $(SOURCE)

SOURCE="CCM_StandardConfiguratorC.cpp"

"$(INTDIR)\CCM_StandardConfiguratorC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_StandardConfiguratorC.obj" $(SOURCE)

SOURCE="CCM_HomeFinderC.cpp"

"$(INTDIR)\CCM_HomeFinderC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_HomeFinderC.obj" $(SOURCE)

SOURCE="CCM_NavigationC.cpp"

"$(INTDIR)\CCM_NavigationC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_NavigationC.obj" $(SOURCE)

SOURCE="CCM_ReceptacleC.cpp"

"$(INTDIR)\CCM_ReceptacleC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_ReceptacleC.obj" $(SOURCE)

SOURCE="CCM_CookieC.cpp"

"$(INTDIR)\CCM_CookieC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_CookieC.obj" $(SOURCE)

SOURCE="CCM_ObjectC.cpp"

"$(INTDIR)\CCM_ObjectC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_ObjectC.obj" $(SOURCE)

SOURCE="CCM_PrimaryKeyBaseC.cpp"

"$(INTDIR)\CCM_PrimaryKeyBaseC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CCM_PrimaryKeyBaseC.obj" $(SOURCE)

SOURCE="Client_init.cpp"

"$(INTDIR)\Client_init.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Client_init.obj" $(SOURCE)

SOURCE="ComponentsC.cpp"

"$(INTDIR)\ComponentsC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ComponentsC.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="CCM_Base.idl"

InputPath=CCM_Base.idl

"CCM_BaseC.h" "CCM_BaseC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_Base_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CCM_Component.idl"

InputPath=CCM_Component.idl

"CCM_ComponentC.h" "CCM_ComponentC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_Component_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CCM_Cookie.idl"

InputPath=CCM_Cookie.idl

"CCM_CookieC.inl" "CCM_CookieC.h" "CCM_CookieC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_Cookie_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_Enumeration.idl"

InputPath=CCM_Enumeration.idl

"CCM_EnumerationC.inl" "CCM_EnumerationC.h" "CCM_EnumerationC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_Enumeration_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="Components.idl"

InputPath=Components.idl

"ComponentsC.h" "ComponentsC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Components_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -Ssi -Sci "$(InputPath)"
<<

SOURCE="CCM_HomeConfiguration.idl"

InputPath=CCM_HomeConfiguration.idl

"CCM_HomeConfigurationC.inl" "CCM_HomeConfigurationS.inl" "CCM_HomeConfigurationC.h" "CCM_HomeConfigurationC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_HomeConfiguration_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_StandardConfigurator.idl"

InputPath=CCM_StandardConfigurator.idl

"CCM_StandardConfiguratorC.inl" "CCM_StandardConfiguratorS.inl" "CCM_StandardConfiguratorC.h" "CCM_StandardConfiguratorC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_StandardConfigurator_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_Home.idl"

InputPath=CCM_Home.idl

"CCM_HomeC.inl" "CCM_HomeS.inl" "CCM_HomeC.h" "CCM_HomeC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_Home_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_Object.idl"

InputPath=CCM_Object.idl

"CCM_ObjectC.inl" "CCM_ObjectS.inl" "CCM_ObjectC.h" "CCM_ObjectC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_Object_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_PrimaryKeyBase.idl"

InputPath=CCM_PrimaryKeyBase.idl

"CCM_PrimaryKeyBaseC.inl" "CCM_PrimaryKeyBaseS.inl" "CCM_PrimaryKeyBaseC.h" "CCM_PrimaryKeyBaseC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_PrimaryKeyBase_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_HomeFinder.idl"

InputPath=CCM_HomeFinder.idl

"CCM_HomeFinderC.inl" "CCM_HomeFinderC.h" "CCM_HomeFinderC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_HomeFinder_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -Ssi "$(InputPath)"
<<

SOURCE="CCM_Configurator.idl"

InputPath=CCM_Configurator.idl

"CCM_ConfiguratorC.inl" "CCM_ConfiguratorC.h" "CCM_ConfiguratorC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_Configurator_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -Ssi "$(InputPath)"
<<

SOURCE="CCM_KeylessCCMHome.idl"

InputPath=CCM_KeylessCCMHome.idl

"CCM_KeylessCCMHomeC.inl" "CCM_KeylessCCMHomeC.h" "CCM_KeylessCCMHomeC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_KeylessCCMHome_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -Ssi "$(InputPath)"
<<

SOURCE="CCM_Navigation.idl"

InputPath=CCM_Navigation.idl

"CCM_NavigationC.inl" "CCM_NavigationC.h" "CCM_NavigationC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_Navigation_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -Ssi "$(InputPath)"
<<

SOURCE="CCM_Receptacle.idl"

InputPath=CCM_Receptacle.idl

"CCM_ReceptacleC.inl" "CCM_ReceptacleC.h" "CCM_ReceptacleC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_Receptacle_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -Ssi "$(InputPath)"
<<

SOURCE="CCM_Events.idl"

InputPath=CCM_Events.idl

"CCM_EventsC.inl" "CCM_EventsS.inl" "CCM_EventsC.h" "CCM_EventsC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_Events_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_EventBase.idl"

InputPath=CCM_EventBase.idl

"CCM_EventBaseC.inl" "CCM_EventBaseS.inl" "CCM_EventBaseC.h" "CCM_EventBaseC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_EventBase_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_EventConsumerBase.idl"

InputPath=CCM_EventConsumerBase.idl

"CCM_EventConsumerBaseC.inl" "CCM_EventConsumerBaseS.inl" "CCM_EventConsumerBaseC.h" "CCM_EventConsumerBaseC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-CCM_EventConsumerBase_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="CCM_Base.idl"

InputPath=CCM_Base.idl

"CCM_BaseC.h" "CCM_BaseC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_Base_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CCM_Component.idl"

InputPath=CCM_Component.idl

"CCM_ComponentC.h" "CCM_ComponentC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_Component_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CCM_Cookie.idl"

InputPath=CCM_Cookie.idl

"CCM_CookieC.inl" "CCM_CookieC.h" "CCM_CookieC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_Cookie_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_Enumeration.idl"

InputPath=CCM_Enumeration.idl

"CCM_EnumerationC.inl" "CCM_EnumerationC.h" "CCM_EnumerationC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_Enumeration_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="Components.idl"

InputPath=Components.idl

"ComponentsC.h" "ComponentsC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Components_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -Ssi -Sci "$(InputPath)"
<<

SOURCE="CCM_HomeConfiguration.idl"

InputPath=CCM_HomeConfiguration.idl

"CCM_HomeConfigurationC.inl" "CCM_HomeConfigurationS.inl" "CCM_HomeConfigurationC.h" "CCM_HomeConfigurationC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_HomeConfiguration_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_StandardConfigurator.idl"

InputPath=CCM_StandardConfigurator.idl

"CCM_StandardConfiguratorC.inl" "CCM_StandardConfiguratorS.inl" "CCM_StandardConfiguratorC.h" "CCM_StandardConfiguratorC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_StandardConfigurator_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_Home.idl"

InputPath=CCM_Home.idl

"CCM_HomeC.inl" "CCM_HomeS.inl" "CCM_HomeC.h" "CCM_HomeC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_Home_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_Object.idl"

InputPath=CCM_Object.idl

"CCM_ObjectC.inl" "CCM_ObjectS.inl" "CCM_ObjectC.h" "CCM_ObjectC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_Object_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_PrimaryKeyBase.idl"

InputPath=CCM_PrimaryKeyBase.idl

"CCM_PrimaryKeyBaseC.inl" "CCM_PrimaryKeyBaseS.inl" "CCM_PrimaryKeyBaseC.h" "CCM_PrimaryKeyBaseC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_PrimaryKeyBase_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_HomeFinder.idl"

InputPath=CCM_HomeFinder.idl

"CCM_HomeFinderC.inl" "CCM_HomeFinderC.h" "CCM_HomeFinderC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_HomeFinder_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -Ssi "$(InputPath)"
<<

SOURCE="CCM_Configurator.idl"

InputPath=CCM_Configurator.idl

"CCM_ConfiguratorC.inl" "CCM_ConfiguratorC.h" "CCM_ConfiguratorC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_Configurator_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -Ssi "$(InputPath)"
<<

SOURCE="CCM_KeylessCCMHome.idl"

InputPath=CCM_KeylessCCMHome.idl

"CCM_KeylessCCMHomeC.inl" "CCM_KeylessCCMHomeC.h" "CCM_KeylessCCMHomeC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_KeylessCCMHome_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -Ssi "$(InputPath)"
<<

SOURCE="CCM_Navigation.idl"

InputPath=CCM_Navigation.idl

"CCM_NavigationC.inl" "CCM_NavigationC.h" "CCM_NavigationC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_Navigation_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -Ssi "$(InputPath)"
<<

SOURCE="CCM_Receptacle.idl"

InputPath=CCM_Receptacle.idl

"CCM_ReceptacleC.inl" "CCM_ReceptacleC.h" "CCM_ReceptacleC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_Receptacle_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -Ssi "$(InputPath)"
<<

SOURCE="CCM_Events.idl"

InputPath=CCM_Events.idl

"CCM_EventsC.inl" "CCM_EventsS.inl" "CCM_EventsC.h" "CCM_EventsC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_Events_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_EventBase.idl"

InputPath=CCM_EventBase.idl

"CCM_EventBaseC.inl" "CCM_EventBaseS.inl" "CCM_EventBaseC.h" "CCM_EventBaseC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_EventBase_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_EventConsumerBase.idl"

InputPath=CCM_EventConsumerBase.idl

"CCM_EventConsumerBaseC.inl" "CCM_EventConsumerBaseS.inl" "CCM_EventConsumerBaseC.h" "CCM_EventConsumerBaseC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-CCM_EventConsumerBase_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="CCM_Base.idl"

InputPath=CCM_Base.idl

"CCM_BaseC.h" "CCM_BaseC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_Base_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CCM_Component.idl"

InputPath=CCM_Component.idl

"CCM_ComponentC.h" "CCM_ComponentC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_Component_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CCM_Cookie.idl"

InputPath=CCM_Cookie.idl

"CCM_CookieC.inl" "CCM_CookieC.h" "CCM_CookieC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_Cookie_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_Enumeration.idl"

InputPath=CCM_Enumeration.idl

"CCM_EnumerationC.inl" "CCM_EnumerationC.h" "CCM_EnumerationC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_Enumeration_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="Components.idl"

InputPath=Components.idl

"ComponentsC.h" "ComponentsC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Components_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -Ssi -Sci "$(InputPath)"
<<

SOURCE="CCM_HomeConfiguration.idl"

InputPath=CCM_HomeConfiguration.idl

"CCM_HomeConfigurationC.inl" "CCM_HomeConfigurationS.inl" "CCM_HomeConfigurationC.h" "CCM_HomeConfigurationC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_HomeConfiguration_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_StandardConfigurator.idl"

InputPath=CCM_StandardConfigurator.idl

"CCM_StandardConfiguratorC.inl" "CCM_StandardConfiguratorS.inl" "CCM_StandardConfiguratorC.h" "CCM_StandardConfiguratorC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_StandardConfigurator_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_Home.idl"

InputPath=CCM_Home.idl

"CCM_HomeC.inl" "CCM_HomeS.inl" "CCM_HomeC.h" "CCM_HomeC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_Home_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_Object.idl"

InputPath=CCM_Object.idl

"CCM_ObjectC.inl" "CCM_ObjectS.inl" "CCM_ObjectC.h" "CCM_ObjectC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_Object_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_PrimaryKeyBase.idl"

InputPath=CCM_PrimaryKeyBase.idl

"CCM_PrimaryKeyBaseC.inl" "CCM_PrimaryKeyBaseS.inl" "CCM_PrimaryKeyBaseC.h" "CCM_PrimaryKeyBaseC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_PrimaryKeyBase_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_HomeFinder.idl"

InputPath=CCM_HomeFinder.idl

"CCM_HomeFinderC.inl" "CCM_HomeFinderC.h" "CCM_HomeFinderC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_HomeFinder_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -Ssi "$(InputPath)"
<<

SOURCE="CCM_Configurator.idl"

InputPath=CCM_Configurator.idl

"CCM_ConfiguratorC.inl" "CCM_ConfiguratorC.h" "CCM_ConfiguratorC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_Configurator_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -Ssi "$(InputPath)"
<<

SOURCE="CCM_KeylessCCMHome.idl"

InputPath=CCM_KeylessCCMHome.idl

"CCM_KeylessCCMHomeC.inl" "CCM_KeylessCCMHomeC.h" "CCM_KeylessCCMHomeC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_KeylessCCMHome_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -Ssi "$(InputPath)"
<<

SOURCE="CCM_Navigation.idl"

InputPath=CCM_Navigation.idl

"CCM_NavigationC.inl" "CCM_NavigationC.h" "CCM_NavigationC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_Navigation_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -Ssi "$(InputPath)"
<<

SOURCE="CCM_Receptacle.idl"

InputPath=CCM_Receptacle.idl

"CCM_ReceptacleC.inl" "CCM_ReceptacleC.h" "CCM_ReceptacleC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_Receptacle_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -Ssi "$(InputPath)"
<<

SOURCE="CCM_Events.idl"

InputPath=CCM_Events.idl

"CCM_EventsC.inl" "CCM_EventsS.inl" "CCM_EventsC.h" "CCM_EventsC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_Events_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_EventBase.idl"

InputPath=CCM_EventBase.idl

"CCM_EventBaseC.inl" "CCM_EventBaseS.inl" "CCM_EventBaseC.h" "CCM_EventBaseC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_EventBase_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_EventConsumerBase.idl"

InputPath=CCM_EventConsumerBase.idl

"CCM_EventConsumerBaseC.inl" "CCM_EventConsumerBaseS.inl" "CCM_EventConsumerBaseC.h" "CCM_EventConsumerBaseC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-CCM_EventConsumerBase_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="CCM_Base.idl"

InputPath=CCM_Base.idl

"CCM_BaseC.h" "CCM_BaseC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_Base_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CCM_Component.idl"

InputPath=CCM_Component.idl

"CCM_ComponentC.h" "CCM_ComponentC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_Component_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -SS -Sci "$(InputPath)"
<<

SOURCE="CCM_Cookie.idl"

InputPath=CCM_Cookie.idl

"CCM_CookieC.inl" "CCM_CookieC.h" "CCM_CookieC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_Cookie_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="CCM_Enumeration.idl"

InputPath=CCM_Enumeration.idl

"CCM_EnumerationC.inl" "CCM_EnumerationC.h" "CCM_EnumerationC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_Enumeration_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -SS "$(InputPath)"
<<

SOURCE="Components.idl"

InputPath=Components.idl

"ComponentsC.h" "ComponentsC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Components_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -Ssi -Sci "$(InputPath)"
<<

SOURCE="CCM_HomeConfiguration.idl"

InputPath=CCM_HomeConfiguration.idl

"CCM_HomeConfigurationC.inl" "CCM_HomeConfigurationS.inl" "CCM_HomeConfigurationC.h" "CCM_HomeConfigurationC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_HomeConfiguration_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_StandardConfigurator.idl"

InputPath=CCM_StandardConfigurator.idl

"CCM_StandardConfiguratorC.inl" "CCM_StandardConfiguratorS.inl" "CCM_StandardConfiguratorC.h" "CCM_StandardConfiguratorC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_StandardConfigurator_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_Home.idl"

InputPath=CCM_Home.idl

"CCM_HomeC.inl" "CCM_HomeS.inl" "CCM_HomeC.h" "CCM_HomeC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_Home_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_Object.idl"

InputPath=CCM_Object.idl

"CCM_ObjectC.inl" "CCM_ObjectS.inl" "CCM_ObjectC.h" "CCM_ObjectC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_Object_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_PrimaryKeyBase.idl"

InputPath=CCM_PrimaryKeyBase.idl

"CCM_PrimaryKeyBaseC.inl" "CCM_PrimaryKeyBaseS.inl" "CCM_PrimaryKeyBaseC.h" "CCM_PrimaryKeyBaseC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_PrimaryKeyBase_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_HomeFinder.idl"

InputPath=CCM_HomeFinder.idl

"CCM_HomeFinderC.inl" "CCM_HomeFinderC.h" "CCM_HomeFinderC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_HomeFinder_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -Ssi "$(InputPath)"
<<

SOURCE="CCM_Configurator.idl"

InputPath=CCM_Configurator.idl

"CCM_ConfiguratorC.inl" "CCM_ConfiguratorC.h" "CCM_ConfiguratorC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_Configurator_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -Ssi "$(InputPath)"
<<

SOURCE="CCM_KeylessCCMHome.idl"

InputPath=CCM_KeylessCCMHome.idl

"CCM_KeylessCCMHomeC.inl" "CCM_KeylessCCMHomeC.h" "CCM_KeylessCCMHomeC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_KeylessCCMHome_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -Ssi "$(InputPath)"
<<

SOURCE="CCM_Navigation.idl"

InputPath=CCM_Navigation.idl

"CCM_NavigationC.inl" "CCM_NavigationC.h" "CCM_NavigationC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_Navigation_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -Ssi "$(InputPath)"
<<

SOURCE="CCM_Receptacle.idl"

InputPath=CCM_Receptacle.idl

"CCM_ReceptacleC.inl" "CCM_ReceptacleC.h" "CCM_ReceptacleC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_Receptacle_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export -Ssi "$(InputPath)"
<<

SOURCE="CCM_Events.idl"

InputPath=CCM_Events.idl

"CCM_EventsC.inl" "CCM_EventsS.inl" "CCM_EventsC.h" "CCM_EventsC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_Events_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_EventBase.idl"

InputPath=CCM_EventBase.idl

"CCM_EventBaseC.inl" "CCM_EventBaseS.inl" "CCM_EventBaseC.h" "CCM_EventBaseC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_EventBase_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

SOURCE="CCM_EventConsumerBase.idl"

InputPath=CCM_EventConsumerBase.idl

"CCM_EventConsumerBaseC.inl" "CCM_EventConsumerBaseS.inl" "CCM_EventConsumerBaseC.h" "CCM_EventConsumerBaseC.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-CCM_EventConsumerBase_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Wb,stub_export_include=CIAO_Client_Export.h -Wb,stub_export_macro=CIAO_CLIENT_Export -Wb,skel_export_include=CIAO_Container_Export.h -Wb,skel_export_macro=CIAO_CONTAINER_Export "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CIAO_Client.dep")
	@echo Using "Makefile.CIAO_Client.dep"
!ELSE
	@echo Warning: cannot find "Makefile.CIAO_Client.dep"
!ENDIF
!ENDIF

