# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.CIAO_Server.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\..\lib
INTDIR=Debug\CIAO_Server\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\lib\CIAO_Serverd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -I"..\..\orbsvcs" -I".." -I"..\ciao" -I"..\DAnCE" -I"..\ciaosvcs\Events" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DCIAO_SERVER_BUILD_DLL -f "Makefile.CIAO_Server.dep" "Container_Base.cpp" "Session_Container.cpp" "Swapping_Container.cpp" "Context_Impl_Base.cpp" "Home_Servant_Impl_Base.cpp" "Servant_Impl_Base.cpp" "Swapping_Servant_Home_Impl_Base.cpp" "Server_init.cpp" "Servant_Activator.cpp" "Dynamic_Component_Activator.cpp" "Dynamic_Component_Servant_Base.cpp" "Port_Activator.cpp" "StandardConfigurator_Impl.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_Serverd.pdb"
	-@del /f/q "..\..\..\lib\CIAO_Serverd.dll"
	-@del /f/q "$(OUTDIR)\CIAO_Serverd.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Serverd.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Serverd.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\CIAO_Server\$(NULL)" mkdir "Debug\CIAO_Server"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"$(INTDIR)/" /I "..\..\.." /I "..\.." /I "..\..\orbsvcs" /I ".." /I "..\ciao" /I "..\DAnCE" /I "..\ciaosvcs\Events" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D CIAO_SERVER_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_PortableServerd.lib TAO_Valuetyped.lib TAO_CosNamingd.lib TAO_IFR_Clientd.lib TAO_CodecFactoryd.lib TAO_PId.lib TAO_Utilsd.lib CIAO_Clientd.lib CIAO_Containerd.lib CIAO_Events_Based.lib TAO_Messagingd.lib CIAO_Deployment_stubd.lib TAO_Svc_Utilsd.lib TAO_RTEventd.lib TAO_RTEvent_Skeld.lib TAO_RTEvent_Servd.lib CIAO_RTEventd.lib CIAO_Eventsd.lib CIAO_Deployment_svntd.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\..\lib\CIAO_Serverd.pdb" /machine:IA64 /out:"..\..\..\lib\CIAO_Serverd.dll" /implib:"$(OUTDIR)\CIAO_Serverd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Container_Base.obj" \
	"$(INTDIR)\Session_Container.obj" \
	"$(INTDIR)\Swapping_Container.obj" \
	"$(INTDIR)\Context_Impl_Base.obj" \
	"$(INTDIR)\Home_Servant_Impl_Base.obj" \
	"$(INTDIR)\Servant_Impl_Base.obj" \
	"$(INTDIR)\Swapping_Servant_Home_Impl_Base.obj" \
	"$(INTDIR)\Server_init.obj" \
	"$(INTDIR)\Servant_Activator.obj" \
	"$(INTDIR)\Dynamic_Component_Activator.obj" \
	"$(INTDIR)\Dynamic_Component_Servant_Base.obj" \
	"$(INTDIR)\Port_Activator.obj" \
	"$(INTDIR)\StandardConfigurator_Impl.obj"

"..\..\..\lib\CIAO_Serverd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\lib\CIAO_Serverd.dll.manifest" mt.exe -manifest "..\..\..\lib\CIAO_Serverd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\..\lib
INTDIR=Release\CIAO_Server\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\lib\CIAO_Server.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -I"..\..\orbsvcs" -I".." -I"..\ciao" -I"..\DAnCE" -I"..\ciaosvcs\Events" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DCIAO_SERVER_BUILD_DLL -f "Makefile.CIAO_Server.dep" "Container_Base.cpp" "Session_Container.cpp" "Swapping_Container.cpp" "Context_Impl_Base.cpp" "Home_Servant_Impl_Base.cpp" "Servant_Impl_Base.cpp" "Swapping_Servant_Home_Impl_Base.cpp" "Server_init.cpp" "Servant_Activator.cpp" "Dynamic_Component_Activator.cpp" "Dynamic_Component_Servant_Base.cpp" "Port_Activator.cpp" "StandardConfigurator_Impl.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\..\lib\CIAO_Server.dll"
	-@del /f/q "$(OUTDIR)\CIAO_Server.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Server.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Server.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\CIAO_Server\$(NULL)" mkdir "Release\CIAO_Server"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\.." /I "..\.." /I "..\..\orbsvcs" /I ".." /I "..\ciao" /I "..\DAnCE" /I "..\ciaosvcs\Events" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D CIAO_SERVER_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_PortableServer.lib TAO_Valuetype.lib TAO_CosNaming.lib TAO_IFR_Client.lib TAO_CodecFactory.lib TAO_PI.lib TAO_Utils.lib CIAO_Client.lib CIAO_Container.lib CIAO_Events_Base.lib TAO_Messaging.lib CIAO_Deployment_stub.lib TAO_Svc_Utils.lib TAO_RTEvent.lib TAO_RTEvent_Skel.lib TAO_RTEvent_Serv.lib CIAO_RTEvent.lib CIAO_Events.lib CIAO_Deployment_svnt.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\..\lib\CIAO_Server.dll" /implib:"$(OUTDIR)\CIAO_Server.lib"
LINK32_OBJS= \
	"$(INTDIR)\Container_Base.obj" \
	"$(INTDIR)\Session_Container.obj" \
	"$(INTDIR)\Swapping_Container.obj" \
	"$(INTDIR)\Context_Impl_Base.obj" \
	"$(INTDIR)\Home_Servant_Impl_Base.obj" \
	"$(INTDIR)\Servant_Impl_Base.obj" \
	"$(INTDIR)\Swapping_Servant_Home_Impl_Base.obj" \
	"$(INTDIR)\Server_init.obj" \
	"$(INTDIR)\Servant_Activator.obj" \
	"$(INTDIR)\Dynamic_Component_Activator.obj" \
	"$(INTDIR)\Dynamic_Component_Servant_Base.obj" \
	"$(INTDIR)\Port_Activator.obj" \
	"$(INTDIR)\StandardConfigurator_Impl.obj"

"..\..\..\lib\CIAO_Server.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\lib\CIAO_Server.dll.manifest" mt.exe -manifest "..\..\..\lib\CIAO_Server.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\..\lib
INTDIR=Static_Debug\CIAO_Server\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CIAO_Serversd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -I"..\..\orbsvcs" -I".." -I"..\ciao" -I"..\DAnCE" -I"..\ciaosvcs\Events" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CIAO_Server.dep" "Container_Base.cpp" "Session_Container.cpp" "Swapping_Container.cpp" "Context_Impl_Base.cpp" "Home_Servant_Impl_Base.cpp" "Servant_Impl_Base.cpp" "Swapping_Servant_Home_Impl_Base.cpp" "Server_init.cpp" "Servant_Activator.cpp" "Dynamic_Component_Activator.cpp" "Dynamic_Component_Servant_Base.cpp" "Port_Activator.cpp" "StandardConfigurator_Impl.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_Serversd.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Serversd.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Serversd.ilk"
	-@del /f/q "..\..\..\lib\CIAO_Serversd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\CIAO_Server\$(NULL)" mkdir "Static_Debug\CIAO_Server"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"..\..\..\lib\CIAO_Serversd.pdb" /I "..\..\.." /I "..\.." /I "..\..\orbsvcs" /I ".." /I "..\ciao" /I "..\DAnCE" /I "..\ciaosvcs\Events" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\lib\CIAO_Serversd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Container_Base.obj" \
	"$(INTDIR)\Session_Container.obj" \
	"$(INTDIR)\Swapping_Container.obj" \
	"$(INTDIR)\Context_Impl_Base.obj" \
	"$(INTDIR)\Home_Servant_Impl_Base.obj" \
	"$(INTDIR)\Servant_Impl_Base.obj" \
	"$(INTDIR)\Swapping_Servant_Home_Impl_Base.obj" \
	"$(INTDIR)\Server_init.obj" \
	"$(INTDIR)\Servant_Activator.obj" \
	"$(INTDIR)\Dynamic_Component_Activator.obj" \
	"$(INTDIR)\Dynamic_Component_Servant_Base.obj" \
	"$(INTDIR)\Port_Activator.obj" \
	"$(INTDIR)\StandardConfigurator_Impl.obj"

"$(OUTDIR)\CIAO_Serversd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CIAO_Serversd.lib.manifest" mt.exe -manifest "$(OUTDIR)\CIAO_Serversd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\..\lib
INTDIR=Static_Release\CIAO_Server\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CIAO_Servers.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -I"..\..\orbsvcs" -I".." -I"..\ciao" -I"..\DAnCE" -I"..\ciaosvcs\Events" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CIAO_Server.dep" "Container_Base.cpp" "Session_Container.cpp" "Swapping_Container.cpp" "Context_Impl_Base.cpp" "Home_Servant_Impl_Base.cpp" "Servant_Impl_Base.cpp" "Swapping_Servant_Home_Impl_Base.cpp" "Server_init.cpp" "Servant_Activator.cpp" "Dynamic_Component_Activator.cpp" "Dynamic_Component_Servant_Base.cpp" "Port_Activator.cpp" "StandardConfigurator_Impl.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIAO_Servers.lib"
	-@del /f/q "$(OUTDIR)\CIAO_Servers.exp"
	-@del /f/q "$(OUTDIR)\CIAO_Servers.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\CIAO_Server\$(NULL)" mkdir "Static_Release\CIAO_Server"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\.." /I "..\.." /I "..\..\orbsvcs" /I ".." /I "..\ciao" /I "..\DAnCE" /I "..\ciaosvcs\Events" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\lib\CIAO_Servers.lib"
LINK32_OBJS= \
	"$(INTDIR)\Container_Base.obj" \
	"$(INTDIR)\Session_Container.obj" \
	"$(INTDIR)\Swapping_Container.obj" \
	"$(INTDIR)\Context_Impl_Base.obj" \
	"$(INTDIR)\Home_Servant_Impl_Base.obj" \
	"$(INTDIR)\Servant_Impl_Base.obj" \
	"$(INTDIR)\Swapping_Servant_Home_Impl_Base.obj" \
	"$(INTDIR)\Server_init.obj" \
	"$(INTDIR)\Servant_Activator.obj" \
	"$(INTDIR)\Dynamic_Component_Activator.obj" \
	"$(INTDIR)\Dynamic_Component_Servant_Base.obj" \
	"$(INTDIR)\Port_Activator.obj" \
	"$(INTDIR)\StandardConfigurator_Impl.obj"

"$(OUTDIR)\CIAO_Servers.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CIAO_Servers.lib.manifest" mt.exe -manifest "$(OUTDIR)\CIAO_Servers.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CIAO_Server.dep")
!INCLUDE "Makefile.CIAO_Server.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Container_Base.cpp"

"$(INTDIR)\Container_Base.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Container_Base.obj" $(SOURCE)

SOURCE="Session_Container.cpp"

"$(INTDIR)\Session_Container.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Session_Container.obj" $(SOURCE)

SOURCE="Swapping_Container.cpp"

"$(INTDIR)\Swapping_Container.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Swapping_Container.obj" $(SOURCE)

SOURCE="Context_Impl_Base.cpp"

"$(INTDIR)\Context_Impl_Base.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Context_Impl_Base.obj" $(SOURCE)

SOURCE="Home_Servant_Impl_Base.cpp"

"$(INTDIR)\Home_Servant_Impl_Base.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Home_Servant_Impl_Base.obj" $(SOURCE)

SOURCE="Servant_Impl_Base.cpp"

"$(INTDIR)\Servant_Impl_Base.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Servant_Impl_Base.obj" $(SOURCE)

SOURCE="Swapping_Servant_Home_Impl_Base.cpp"

"$(INTDIR)\Swapping_Servant_Home_Impl_Base.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Swapping_Servant_Home_Impl_Base.obj" $(SOURCE)

SOURCE="Server_init.cpp"

"$(INTDIR)\Server_init.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Server_init.obj" $(SOURCE)

SOURCE="Servant_Activator.cpp"

"$(INTDIR)\Servant_Activator.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Servant_Activator.obj" $(SOURCE)

SOURCE="Dynamic_Component_Activator.cpp"

"$(INTDIR)\Dynamic_Component_Activator.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Dynamic_Component_Activator.obj" $(SOURCE)

SOURCE="Dynamic_Component_Servant_Base.cpp"

"$(INTDIR)\Dynamic_Component_Servant_Base.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Dynamic_Component_Servant_Base.obj" $(SOURCE)

SOURCE="Port_Activator.cpp"

"$(INTDIR)\Port_Activator.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Port_Activator.obj" $(SOURCE)

SOURCE="StandardConfigurator_Impl.cpp"

"$(INTDIR)\StandardConfigurator_Impl.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\StandardConfigurator_Impl.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CIAO_Server.dep")
	@echo Using "Makefile.CIAO_Server.dep"
!ELSE
	@echo Warning: cannot find "Makefile.CIAO_Server.dep"
!ENDIF
!ENDIF

