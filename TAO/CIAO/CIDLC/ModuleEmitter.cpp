// file      : CIDLC/ModuleEmitter.cpp
// author    : Jeff Parsons <j.parsons@vanderbilt.edu>
// cvs-id    : $Id: ModuleEmitter.cpp 14 2007-02-01 15:49:12Z mitza $

#include "ModuleEmitter.hpp"

ModuleEmitter::ModuleEmitter (Context& c)
  : EmitterBase (c)
{
}

void
ModuleEmitter::pre (Type& t)
{
  os << "namespace " << t.name () << "{";
}

void
ModuleEmitter::post (Type&)
{
  os << "}";
}
