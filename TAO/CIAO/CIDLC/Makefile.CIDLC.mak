# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.CIDLC.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Console Application")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INSTALLDIR=..\bin
INTDIR=Debug\CIDLC\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" "$(INSTALLDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\cidlc.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"$(BOOST_ROOT)\include\$(BOOST_VERSION)" -I"$(BOOST_ROOT)\." -I".." -I"..\CCF" -I"..\..\..\contrib\utility" -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -f "Makefile.CIDLC.dep" "AttributeHeaderEmitters.cpp" "cidlc.cpp" "CompositionEmitter.cpp" "CorbaTypeNameEmitters.cpp" "CxxNamePrinter.cpp" "DescriptorGenerator.cpp" "EmitterBase.cpp" "EmitterContext.cpp" "ExecImplGenerator.cpp" "ExecImplHeaderGenerator.cpp" "ExecImplSourceGenerator.cpp" "ExecutorMappingGenerator.cpp" "InterfaceEmitter.cpp" "Literals.cpp" "ModuleEmitter.cpp" "OperationHeaderEmitters.cpp" "RepositoryIdGenerator.cpp" "ServantGenerator.cpp" "ServantHeaderGenerator.cpp" "ServantSourceGenerator.cpp" "SizeTypeCalculator.cpp" "UtilityTypeNameEmitters.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\cidlc.pdb"
	-@del /f/q "$(INSTALLDIR)\cidlc.exe"
	-@del /f/q "$(INSTALLDIR)\cidlc.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\CIDLC\$(NULL)" mkdir "Debug\CIDLC"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4290 /wd4800 /wd4250 /wd4355 /wd4996 /Fd"$(INTDIR)/" /I "$(BOOST_ROOT)\include\$(BOOST_VERSION)" /I "$(BOOST_ROOT)\." /I ".." /I "..\CCF" /I "..\..\..\contrib\utility" /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO CompilerElementsd.lib CodeGenerationKitd.lib IDL2d.lib IDL3d.lib CIDLd.lib /libpath:"." /libpath:"$(BOOST_ROOT)\lib" /libpath:"..\CCF\CCF" /libpath:"..\..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\cidlc.pdb" /machine:IA64 /out:"$(INSTALLDIR)\cidlc.exe"
LINK32_OBJS= \
	"$(INTDIR)\AttributeHeaderEmitters.obj" \
	"$(INTDIR)\cidlc.obj" \
	"$(INTDIR)\CompositionEmitter.obj" \
	"$(INTDIR)\CorbaTypeNameEmitters.obj" \
	"$(INTDIR)\CxxNamePrinter.obj" \
	"$(INTDIR)\DescriptorGenerator.obj" \
	"$(INTDIR)\EmitterBase.obj" \
	"$(INTDIR)\EmitterContext.obj" \
	"$(INTDIR)\ExecImplGenerator.obj" \
	"$(INTDIR)\ExecImplHeaderGenerator.obj" \
	"$(INTDIR)\ExecImplSourceGenerator.obj" \
	"$(INTDIR)\ExecutorMappingGenerator.obj" \
	"$(INTDIR)\InterfaceEmitter.obj" \
	"$(INTDIR)\Literals.obj" \
	"$(INTDIR)\ModuleEmitter.obj" \
	"$(INTDIR)\OperationHeaderEmitters.obj" \
	"$(INTDIR)\RepositoryIdGenerator.obj" \
	"$(INTDIR)\ServantGenerator.obj" \
	"$(INTDIR)\ServantHeaderGenerator.obj" \
	"$(INTDIR)\ServantSourceGenerator.obj" \
	"$(INTDIR)\SizeTypeCalculator.obj" \
	"$(INTDIR)\UtilityTypeNameEmitters.obj"

"$(INSTALLDIR)\cidlc.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\cidlc.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\cidlc.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=Release
INSTALLDIR=..\bin
INTDIR=Release\CIDLC\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" "$(INSTALLDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\cidlc.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"$(BOOST_ROOT)\include\$(BOOST_VERSION)" -I"$(BOOST_ROOT)\." -I".." -I"..\CCF" -I"..\..\..\contrib\utility" -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -f "Makefile.CIDLC.dep" "AttributeHeaderEmitters.cpp" "cidlc.cpp" "CompositionEmitter.cpp" "CorbaTypeNameEmitters.cpp" "CxxNamePrinter.cpp" "DescriptorGenerator.cpp" "EmitterBase.cpp" "EmitterContext.cpp" "ExecImplGenerator.cpp" "ExecImplHeaderGenerator.cpp" "ExecImplSourceGenerator.cpp" "ExecutorMappingGenerator.cpp" "InterfaceEmitter.cpp" "Literals.cpp" "ModuleEmitter.cpp" "OperationHeaderEmitters.cpp" "RepositoryIdGenerator.cpp" "ServantGenerator.cpp" "ServantHeaderGenerator.cpp" "ServantSourceGenerator.cpp" "SizeTypeCalculator.cpp" "UtilityTypeNameEmitters.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\cidlc.exe"
	-@del /f/q "$(INSTALLDIR)\cidlc.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\CIDLC\$(NULL)" mkdir "Release\CIDLC"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4290 /wd4800 /wd4250 /wd4355 /wd4996 /I "$(BOOST_ROOT)\include\$(BOOST_VERSION)" /I "$(BOOST_ROOT)\." /I ".." /I "..\CCF" /I "..\..\..\contrib\utility" /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO CompilerElements.lib CodeGenerationKit.lib IDL2.lib IDL3.lib CIDL.lib /libpath:"." /libpath:"$(BOOST_ROOT)\lib" /libpath:"..\CCF\CCF" /libpath:"..\..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\cidlc.exe"
LINK32_OBJS= \
	"$(INTDIR)\AttributeHeaderEmitters.obj" \
	"$(INTDIR)\cidlc.obj" \
	"$(INTDIR)\CompositionEmitter.obj" \
	"$(INTDIR)\CorbaTypeNameEmitters.obj" \
	"$(INTDIR)\CxxNamePrinter.obj" \
	"$(INTDIR)\DescriptorGenerator.obj" \
	"$(INTDIR)\EmitterBase.obj" \
	"$(INTDIR)\EmitterContext.obj" \
	"$(INTDIR)\ExecImplGenerator.obj" \
	"$(INTDIR)\ExecImplHeaderGenerator.obj" \
	"$(INTDIR)\ExecImplSourceGenerator.obj" \
	"$(INTDIR)\ExecutorMappingGenerator.obj" \
	"$(INTDIR)\InterfaceEmitter.obj" \
	"$(INTDIR)\Literals.obj" \
	"$(INTDIR)\ModuleEmitter.obj" \
	"$(INTDIR)\OperationHeaderEmitters.obj" \
	"$(INTDIR)\RepositoryIdGenerator.obj" \
	"$(INTDIR)\ServantGenerator.obj" \
	"$(INTDIR)\ServantHeaderGenerator.obj" \
	"$(INTDIR)\ServantSourceGenerator.obj" \
	"$(INTDIR)\SizeTypeCalculator.obj" \
	"$(INTDIR)\UtilityTypeNameEmitters.obj"

"$(INSTALLDIR)\cidlc.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\cidlc.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\cidlc.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=Static_Debug
INSTALLDIR=..\bin
INTDIR=Static_Debug\CIDLC\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" "$(INSTALLDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\cidlc.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"$(BOOST_ROOT)\include\$(BOOST_VERSION)" -I"$(BOOST_ROOT)\." -I".." -I"..\CCF" -I"..\..\..\contrib\utility" -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -f "Makefile.CIDLC.dep" "AttributeHeaderEmitters.cpp" "cidlc.cpp" "CompositionEmitter.cpp" "CorbaTypeNameEmitters.cpp" "CxxNamePrinter.cpp" "DescriptorGenerator.cpp" "EmitterBase.cpp" "EmitterContext.cpp" "ExecImplGenerator.cpp" "ExecImplHeaderGenerator.cpp" "ExecImplSourceGenerator.cpp" "ExecutorMappingGenerator.cpp" "InterfaceEmitter.cpp" "Literals.cpp" "ModuleEmitter.cpp" "OperationHeaderEmitters.cpp" "RepositoryIdGenerator.cpp" "ServantGenerator.cpp" "ServantHeaderGenerator.cpp" "ServantSourceGenerator.cpp" "SizeTypeCalculator.cpp" "UtilityTypeNameEmitters.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\cidlc.pdb"
	-@del /f/q "$(INSTALLDIR)\cidlc.exe"
	-@del /f/q "$(INSTALLDIR)\cidlc.ilk"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\CIDLC\$(NULL)" mkdir "Static_Debug\CIDLC"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4290 /wd4800 /wd4250 /wd4355 /wd4996 /Fd"$(INTDIR)/" /I "$(BOOST_ROOT)\include\$(BOOST_VERSION)" /I "$(BOOST_ROOT)\." /I ".." /I "..\CCF" /I "..\..\..\contrib\utility" /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO CompilerElementssd.lib CodeGenerationKitsd.lib IDL2sd.lib IDL3sd.lib CIDLsd.lib /libpath:"." /libpath:"$(BOOST_ROOT)\lib" /libpath:"..\CCF\CCF" /libpath:"..\..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\cidlc.pdb" /machine:IA64 /out:"$(INSTALLDIR)\cidlc.exe"
LINK32_OBJS= \
	"$(INTDIR)\AttributeHeaderEmitters.obj" \
	"$(INTDIR)\cidlc.obj" \
	"$(INTDIR)\CompositionEmitter.obj" \
	"$(INTDIR)\CorbaTypeNameEmitters.obj" \
	"$(INTDIR)\CxxNamePrinter.obj" \
	"$(INTDIR)\DescriptorGenerator.obj" \
	"$(INTDIR)\EmitterBase.obj" \
	"$(INTDIR)\EmitterContext.obj" \
	"$(INTDIR)\ExecImplGenerator.obj" \
	"$(INTDIR)\ExecImplHeaderGenerator.obj" \
	"$(INTDIR)\ExecImplSourceGenerator.obj" \
	"$(INTDIR)\ExecutorMappingGenerator.obj" \
	"$(INTDIR)\InterfaceEmitter.obj" \
	"$(INTDIR)\Literals.obj" \
	"$(INTDIR)\ModuleEmitter.obj" \
	"$(INTDIR)\OperationHeaderEmitters.obj" \
	"$(INTDIR)\RepositoryIdGenerator.obj" \
	"$(INTDIR)\ServantGenerator.obj" \
	"$(INTDIR)\ServantHeaderGenerator.obj" \
	"$(INTDIR)\ServantSourceGenerator.obj" \
	"$(INTDIR)\SizeTypeCalculator.obj" \
	"$(INTDIR)\UtilityTypeNameEmitters.obj"

"$(INSTALLDIR)\cidlc.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\cidlc.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\cidlc.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=Static_Release
INSTALLDIR=..\bin
INTDIR=Static_Release\CIDLC\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" "$(INSTALLDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\cidlc.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"$(BOOST_ROOT)\include\$(BOOST_VERSION)" -I"$(BOOST_ROOT)\." -I".." -I"..\CCF" -I"..\..\..\contrib\utility" -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -f "Makefile.CIDLC.dep" "AttributeHeaderEmitters.cpp" "cidlc.cpp" "CompositionEmitter.cpp" "CorbaTypeNameEmitters.cpp" "CxxNamePrinter.cpp" "DescriptorGenerator.cpp" "EmitterBase.cpp" "EmitterContext.cpp" "ExecImplGenerator.cpp" "ExecImplHeaderGenerator.cpp" "ExecImplSourceGenerator.cpp" "ExecutorMappingGenerator.cpp" "InterfaceEmitter.cpp" "Literals.cpp" "ModuleEmitter.cpp" "OperationHeaderEmitters.cpp" "RepositoryIdGenerator.cpp" "ServantGenerator.cpp" "ServantHeaderGenerator.cpp" "ServantSourceGenerator.cpp" "SizeTypeCalculator.cpp" "UtilityTypeNameEmitters.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\cidlc.exe"
	-@del /f/q "$(INSTALLDIR)\cidlc.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\CIDLC\$(NULL)" mkdir "Static_Release\CIDLC"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4290 /wd4800 /wd4250 /wd4355 /wd4996 /I "$(BOOST_ROOT)\include\$(BOOST_VERSION)" /I "$(BOOST_ROOT)\." /I ".." /I "..\CCF" /I "..\..\..\contrib\utility" /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO CompilerElementss.lib CodeGenerationKits.lib IDL2s.lib IDL3s.lib CIDLs.lib /libpath:"." /libpath:"$(BOOST_ROOT)\lib" /libpath:"..\CCF\CCF" /libpath:"..\..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\cidlc.exe"
LINK32_OBJS= \
	"$(INTDIR)\AttributeHeaderEmitters.obj" \
	"$(INTDIR)\cidlc.obj" \
	"$(INTDIR)\CompositionEmitter.obj" \
	"$(INTDIR)\CorbaTypeNameEmitters.obj" \
	"$(INTDIR)\CxxNamePrinter.obj" \
	"$(INTDIR)\DescriptorGenerator.obj" \
	"$(INTDIR)\EmitterBase.obj" \
	"$(INTDIR)\EmitterContext.obj" \
	"$(INTDIR)\ExecImplGenerator.obj" \
	"$(INTDIR)\ExecImplHeaderGenerator.obj" \
	"$(INTDIR)\ExecImplSourceGenerator.obj" \
	"$(INTDIR)\ExecutorMappingGenerator.obj" \
	"$(INTDIR)\InterfaceEmitter.obj" \
	"$(INTDIR)\Literals.obj" \
	"$(INTDIR)\ModuleEmitter.obj" \
	"$(INTDIR)\OperationHeaderEmitters.obj" \
	"$(INTDIR)\RepositoryIdGenerator.obj" \
	"$(INTDIR)\ServantGenerator.obj" \
	"$(INTDIR)\ServantHeaderGenerator.obj" \
	"$(INTDIR)\ServantSourceGenerator.obj" \
	"$(INTDIR)\SizeTypeCalculator.obj" \
	"$(INTDIR)\UtilityTypeNameEmitters.obj"

"$(INSTALLDIR)\cidlc.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\cidlc.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\cidlc.exe.manifest" -outputresource:$@;1

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(INSTALLDIR)" :
	if not exist "$(INSTALLDIR)\$(NULL)" mkdir "$(INSTALLDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CIDLC.dep")
!INCLUDE "Makefile.CIDLC.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="AttributeHeaderEmitters.cpp"

"$(INTDIR)\AttributeHeaderEmitters.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AttributeHeaderEmitters.obj" $(SOURCE)

SOURCE="cidlc.cpp"

"$(INTDIR)\cidlc.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\cidlc.obj" $(SOURCE)

SOURCE="CompositionEmitter.cpp"

"$(INTDIR)\CompositionEmitter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CompositionEmitter.obj" $(SOURCE)

SOURCE="CorbaTypeNameEmitters.cpp"

"$(INTDIR)\CorbaTypeNameEmitters.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CorbaTypeNameEmitters.obj" $(SOURCE)

SOURCE="CxxNamePrinter.cpp"

"$(INTDIR)\CxxNamePrinter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CxxNamePrinter.obj" $(SOURCE)

SOURCE="DescriptorGenerator.cpp"

"$(INTDIR)\DescriptorGenerator.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DescriptorGenerator.obj" $(SOURCE)

SOURCE="EmitterBase.cpp"

"$(INTDIR)\EmitterBase.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\EmitterBase.obj" $(SOURCE)

SOURCE="EmitterContext.cpp"

"$(INTDIR)\EmitterContext.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\EmitterContext.obj" $(SOURCE)

SOURCE="ExecImplGenerator.cpp"

"$(INTDIR)\ExecImplGenerator.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ExecImplGenerator.obj" $(SOURCE)

SOURCE="ExecImplHeaderGenerator.cpp"

"$(INTDIR)\ExecImplHeaderGenerator.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ExecImplHeaderGenerator.obj" $(SOURCE)

SOURCE="ExecImplSourceGenerator.cpp"

"$(INTDIR)\ExecImplSourceGenerator.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ExecImplSourceGenerator.obj" $(SOURCE)

SOURCE="ExecutorMappingGenerator.cpp"

"$(INTDIR)\ExecutorMappingGenerator.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ExecutorMappingGenerator.obj" $(SOURCE)

SOURCE="InterfaceEmitter.cpp"

"$(INTDIR)\InterfaceEmitter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\InterfaceEmitter.obj" $(SOURCE)

SOURCE="Literals.cpp"

"$(INTDIR)\Literals.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Literals.obj" $(SOURCE)

SOURCE="ModuleEmitter.cpp"

"$(INTDIR)\ModuleEmitter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ModuleEmitter.obj" $(SOURCE)

SOURCE="OperationHeaderEmitters.cpp"

"$(INTDIR)\OperationHeaderEmitters.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\OperationHeaderEmitters.obj" $(SOURCE)

SOURCE="RepositoryIdGenerator.cpp"

"$(INTDIR)\RepositoryIdGenerator.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\RepositoryIdGenerator.obj" $(SOURCE)

SOURCE="ServantGenerator.cpp"

"$(INTDIR)\ServantGenerator.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ServantGenerator.obj" $(SOURCE)

SOURCE="ServantHeaderGenerator.cpp"

"$(INTDIR)\ServantHeaderGenerator.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ServantHeaderGenerator.obj" $(SOURCE)

SOURCE="ServantSourceGenerator.cpp"

"$(INTDIR)\ServantSourceGenerator.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ServantSourceGenerator.obj" $(SOURCE)

SOURCE="SizeTypeCalculator.cpp"

"$(INTDIR)\SizeTypeCalculator.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\SizeTypeCalculator.obj" $(SOURCE)

SOURCE="UtilityTypeNameEmitters.cpp"

"$(INTDIR)\UtilityTypeNameEmitters.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\UtilityTypeNameEmitters.obj" $(SOURCE)


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CIDLC.dep")
	@echo Using "Makefile.CIDLC.dep"
!ELSE
	@echo Warning: cannot find "Makefile.CIDLC.dep"
!ENDIF
!ENDIF

