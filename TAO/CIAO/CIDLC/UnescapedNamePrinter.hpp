// file      : CIDLC/UnescapedNamePrinter.hpp
// author    : Boris Kolpackov <boris@codesynthesis.com>
// cvs-id    : $Id: UnescapedNamePrinter.hpp 14 2007-02-01 15:49:12Z mitza $

#ifndef UNESCAPED_NAME_PRINTER_HPP
#define UNESCAPED_NAME_PRINTER_HPP

#include "CCF/IDL2/SemanticGraph/Name.hpp"

#include <ostream>

// Prints names in the unescaped form.
//
struct UnescapedNamePrinter: CCF::IDL2::SemanticGraph::NamePrinter
{
  virtual void
  print (std::ostream& os, CCF::IDL2::SemanticGraph::SimpleName const& n)
  {
    os << n.unescaped_str ();
  }
};

#endif // UNESCAPED_NAME_PRINTER_HPP
