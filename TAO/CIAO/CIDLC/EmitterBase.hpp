// file      : CIDLC/EmitterBase.hpp
// author    : Jeff Parsons <j.parsons@vanderbilt.edu>
// cvs-id    : $Id: EmitterBase.hpp 14 2007-02-01 15:49:12Z mitza $

#ifndef EMITTERBASE_HPP
#define EMITTERBASE_HPP

#include "EmitterContext.hpp"

class EmitterBase
{
public:
  EmitterBase (Context&);

protected:
  Context& ctx;
  ostream& os;
};

#endif  // EMITTERBASE_HPP
