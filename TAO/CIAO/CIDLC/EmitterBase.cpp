// file      : CIDLC/EmitterBase.cpp
// author    : Jeff Parsons <j.parsons@vanderbilt.edu>
// cvs-id    : $Id: EmitterBase.cpp 14 2007-02-01 15:49:12Z mitza $

#include "EmitterBase.hpp"

EmitterBase::EmitterBase (Context& c)
  : ctx (c),
    os (ctx.os ())
{
}
