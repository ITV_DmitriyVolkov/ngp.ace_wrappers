// file      : CCF/CompilerElements/ReferenceCounting.hpp
// author    : Boris Kolpackov <boris@dre.vanderbilt.edu>
// cvs-id    : $Id: ReferenceCounting.hpp 14 2007-02-01 15:49:12Z mitza $

#ifndef CCF_RUNTIME_REFERENCE_COUNTING_H
#define CCF_RUNTIME_REFERENCE_COUNTING_H

#include "Utility/ReferenceCounting/ReferenceCounting.hpp"

namespace ReferenceCounting = Utility::ReferenceCounting;

#endif  // CCF_RUNTIME_REFERENCE_COUNTING_H
