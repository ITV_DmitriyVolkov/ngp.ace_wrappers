// file      : CCF/CompilerElements/Introspection.hpp
// author    : Boris Kolpackov <boris@dre.vanderbilt.edu>
// cvs-id    : $Id: Introspection.hpp 14 2007-02-01 15:49:12Z mitza $

#ifndef CCF_RUNTIME_INTROSPECTION_HPP
#define CCF_RUNTIME_INTROSPECTION_HPP

#include "Utility/Introspection/Introspection.hpp"

namespace Introspection = Utility::Introspection;

#endif  // CCF_RUNTIME_INTROSPECTION_HPP
