// file      : CCF/CompilerElements/ExH.hpp
// author    : Boris Kolpackov <boris@dre.vanderbilt.edu>
// cvs-id    : $Id: ExH.hpp 14 2007-02-01 15:49:12Z mitza $

#ifndef CCF_RUNTIME_EX_H_H
#define CCF_RUNTIME_EX_H_H

#include "Utility/ExH/ExH.hpp"

namespace ExH = Utility::ExH;

#endif  // CCF_RUNTIME_EX_H_H
