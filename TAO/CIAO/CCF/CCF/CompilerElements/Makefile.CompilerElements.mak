# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.CompilerElements.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..
INTDIR=Debug\CompilerElements\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CompilerElementsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"$(BOOST_ROOT)\include\$(BOOST_VERSION)" -I"$(BOOST_ROOT)\." -I"..\..\.." -I"..\..\..\CCF" -I"..\..\..\..\..\contrib\utility" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -f "Makefile.CompilerElements.dep" "Preprocessor.cpp" "PreprocessorToken.cpp" "Introspection.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CompilerElementsd.lib"
	-@del /f/q "$(OUTDIR)\CompilerElementsd.exp"
	-@del /f/q "$(OUTDIR)\CompilerElementsd.ilk"
	-@del /f/q "..\CompilerElementsd.pdb"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\CompilerElements\$(NULL)" mkdir "Debug\CompilerElements"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4290 /wd4800 /wd4250 /wd4355 /wd4996 /Fd"..\CompilerElementsd.pdb" /I "$(BOOST_ROOT)\include\$(BOOST_VERSION)" /I "$(BOOST_ROOT)\." /I "..\..\.." /I "..\..\..\CCF" /I "..\..\..\..\..\contrib\utility" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\CompilerElementsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Preprocessor.obj" \
	"$(INTDIR)\PreprocessorToken.obj" \
	"$(INTDIR)\Introspection.obj"

"$(OUTDIR)\CompilerElementsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CompilerElementsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\CompilerElementsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..
INTDIR=Release\CompilerElements\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CompilerElements.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"$(BOOST_ROOT)\include\$(BOOST_VERSION)" -I"$(BOOST_ROOT)\." -I"..\..\.." -I"..\..\..\CCF" -I"..\..\..\..\..\contrib\utility" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -f "Makefile.CompilerElements.dep" "Preprocessor.cpp" "PreprocessorToken.cpp" "Introspection.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CompilerElements.lib"
	-@del /f/q "$(OUTDIR)\CompilerElements.exp"
	-@del /f/q "$(OUTDIR)\CompilerElements.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\CompilerElements\$(NULL)" mkdir "Release\CompilerElements"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4290 /wd4800 /wd4250 /wd4355 /wd4996 /I "$(BOOST_ROOT)\include\$(BOOST_VERSION)" /I "$(BOOST_ROOT)\." /I "..\..\.." /I "..\..\..\CCF" /I "..\..\..\..\..\contrib\utility" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\CompilerElements.lib"
LINK32_OBJS= \
	"$(INTDIR)\Preprocessor.obj" \
	"$(INTDIR)\PreprocessorToken.obj" \
	"$(INTDIR)\Introspection.obj"

"$(OUTDIR)\CompilerElements.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CompilerElements.lib.manifest" mt.exe -manifest "$(OUTDIR)\CompilerElements.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..
INTDIR=Static_Debug\CompilerElements\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CompilerElementssd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"$(BOOST_ROOT)\include\$(BOOST_VERSION)" -I"$(BOOST_ROOT)\." -I"..\..\.." -I"..\..\..\CCF" -I"..\..\..\..\..\contrib\utility" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -f "Makefile.CompilerElements.dep" "Preprocessor.cpp" "PreprocessorToken.cpp" "Introspection.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CompilerElementssd.lib"
	-@del /f/q "$(OUTDIR)\CompilerElementssd.exp"
	-@del /f/q "$(OUTDIR)\CompilerElementssd.ilk"
	-@del /f/q "..\CompilerElementssd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\CompilerElements\$(NULL)" mkdir "Static_Debug\CompilerElements"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4290 /wd4800 /wd4250 /wd4355 /wd4996 /Fd"..\CompilerElementssd.pdb" /I "$(BOOST_ROOT)\include\$(BOOST_VERSION)" /I "$(BOOST_ROOT)\." /I "..\..\.." /I "..\..\..\CCF" /I "..\..\..\..\..\contrib\utility" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\CompilerElementssd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Preprocessor.obj" \
	"$(INTDIR)\PreprocessorToken.obj" \
	"$(INTDIR)\Introspection.obj"

"$(OUTDIR)\CompilerElementssd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CompilerElementssd.lib.manifest" mt.exe -manifest "$(OUTDIR)\CompilerElementssd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..
INTDIR=Static_Release\CompilerElements\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CompilerElementss.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"$(BOOST_ROOT)\include\$(BOOST_VERSION)" -I"$(BOOST_ROOT)\." -I"..\..\.." -I"..\..\..\CCF" -I"..\..\..\..\..\contrib\utility" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -f "Makefile.CompilerElements.dep" "Preprocessor.cpp" "PreprocessorToken.cpp" "Introspection.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CompilerElementss.lib"
	-@del /f/q "$(OUTDIR)\CompilerElementss.exp"
	-@del /f/q "$(OUTDIR)\CompilerElementss.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\CompilerElements\$(NULL)" mkdir "Static_Release\CompilerElements"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4290 /wd4800 /wd4250 /wd4355 /wd4996 /I "$(BOOST_ROOT)\include\$(BOOST_VERSION)" /I "$(BOOST_ROOT)\." /I "..\..\.." /I "..\..\..\CCF" /I "..\..\..\..\..\contrib\utility" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\CompilerElementss.lib"
LINK32_OBJS= \
	"$(INTDIR)\Preprocessor.obj" \
	"$(INTDIR)\PreprocessorToken.obj" \
	"$(INTDIR)\Introspection.obj"

"$(OUTDIR)\CompilerElementss.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CompilerElementss.lib.manifest" mt.exe -manifest "$(OUTDIR)\CompilerElementss.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CompilerElements.dep")
!INCLUDE "Makefile.CompilerElements.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Preprocessor.cpp"

"$(INTDIR)\Preprocessor.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Preprocessor.obj" $(SOURCE)

SOURCE="PreprocessorToken.cpp"

"$(INTDIR)\PreprocessorToken.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PreprocessorToken.obj" $(SOURCE)

SOURCE="Introspection.cpp"

"$(INTDIR)\Introspection.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Introspection.obj" $(SOURCE)


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CompilerElements.dep")
	@echo Using "Makefile.CompilerElements.dep"
!ELSE
	@echo Warning: cannot find "Makefile.CompilerElements.dep"
!ENDIF
!ENDIF

