// file      : CCF/CIDL/SemanticAction/Elements.hpp
// author    : Boris Kolpackov <boris@dre.vanderbilt.edu>
// cvs-id    : $Id: Elements.hpp 14 2007-02-01 15:49:12Z mitza $

#ifndef CCF_CIDL_SEMANTIC_ACTION_ELEMENTS_HPP
#define CCF_CIDL_SEMANTIC_ACTION_ELEMENTS_HPP

#include "CCF/IDL3/SemanticAction/Elements.hpp"

#include "CCF/CIDL/Token.hpp"

namespace CCF
{
  namespace CIDL
  {
    namespace SemanticAction
    {
      using IDL3::SemanticAction::Scope;
    }
  }
}

#endif  // CCF_CIDL_SEMANTIC_ACTION_ELEMENTS_HPP
