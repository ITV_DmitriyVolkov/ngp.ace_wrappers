# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.CIDL.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..
INTDIR=Debug\CIDL\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CIDLd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"$(BOOST_ROOT)\include\$(BOOST_VERSION)" -I"$(BOOST_ROOT)\." -I"..\..\.." -I"..\..\..\CCF" -I"..\..\..\..\..\contrib\utility" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -f "Makefile.CIDL.dep" "Parser.cpp" "LexicalAnalyzer.cpp" ".\SemanticGraph\Elements.cpp" ".\SemanticGraph\Executor.cpp" ".\SemanticGraph\Composition.cpp" ".\Traversal\Executor.cpp" ".\SemanticAction\Composition.cpp" ".\SemanticAction\Impl\Factory.cpp" ".\SemanticAction\Impl\HomeExecutor.cpp" ".\SemanticAction\Impl\Composition.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIDLd.lib"
	-@del /f/q "$(OUTDIR)\CIDLd.exp"
	-@del /f/q "$(OUTDIR)\CIDLd.ilk"
	-@del /f/q "..\CIDLd.pdb"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\CIDL\$(NULL)" mkdir "Debug\CIDL"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4290 /wd4800 /wd4250 /wd4355 /wd4996 /Fd"..\CIDLd.pdb" /I "$(BOOST_ROOT)\include\$(BOOST_VERSION)" /I "$(BOOST_ROOT)\." /I "..\..\.." /I "..\..\..\CCF" /I "..\..\..\..\..\contrib\utility" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\CIDLd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Parser.obj" \
	"$(INTDIR)\LexicalAnalyzer.obj" \
	"$(INTDIR)\.\SemanticGraph\Elements.obj" \
	"$(INTDIR)\.\SemanticGraph\Executor.obj" \
	"$(INTDIR)\.\SemanticGraph\Composition.obj" \
	"$(INTDIR)\.\Traversal\Executor.obj" \
	"$(INTDIR)\.\SemanticAction\Composition.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Factory.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\HomeExecutor.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Composition.obj"

"$(OUTDIR)\CIDLd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CIDLd.lib.manifest" mt.exe -manifest "$(OUTDIR)\CIDLd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..
INTDIR=Release\CIDL\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CIDL.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"$(BOOST_ROOT)\include\$(BOOST_VERSION)" -I"$(BOOST_ROOT)\." -I"..\..\.." -I"..\..\..\CCF" -I"..\..\..\..\..\contrib\utility" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -f "Makefile.CIDL.dep" "Parser.cpp" "LexicalAnalyzer.cpp" ".\SemanticGraph\Elements.cpp" ".\SemanticGraph\Executor.cpp" ".\SemanticGraph\Composition.cpp" ".\Traversal\Executor.cpp" ".\SemanticAction\Composition.cpp" ".\SemanticAction\Impl\Factory.cpp" ".\SemanticAction\Impl\HomeExecutor.cpp" ".\SemanticAction\Impl\Composition.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIDL.lib"
	-@del /f/q "$(OUTDIR)\CIDL.exp"
	-@del /f/q "$(OUTDIR)\CIDL.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\CIDL\$(NULL)" mkdir "Release\CIDL"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4290 /wd4800 /wd4250 /wd4355 /wd4996 /I "$(BOOST_ROOT)\include\$(BOOST_VERSION)" /I "$(BOOST_ROOT)\." /I "..\..\.." /I "..\..\..\CCF" /I "..\..\..\..\..\contrib\utility" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\CIDL.lib"
LINK32_OBJS= \
	"$(INTDIR)\Parser.obj" \
	"$(INTDIR)\LexicalAnalyzer.obj" \
	"$(INTDIR)\.\SemanticGraph\Elements.obj" \
	"$(INTDIR)\.\SemanticGraph\Executor.obj" \
	"$(INTDIR)\.\SemanticGraph\Composition.obj" \
	"$(INTDIR)\.\Traversal\Executor.obj" \
	"$(INTDIR)\.\SemanticAction\Composition.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Factory.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\HomeExecutor.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Composition.obj"

"$(OUTDIR)\CIDL.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CIDL.lib.manifest" mt.exe -manifest "$(OUTDIR)\CIDL.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..
INTDIR=Static_Debug\CIDL\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CIDLsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"$(BOOST_ROOT)\include\$(BOOST_VERSION)" -I"$(BOOST_ROOT)\." -I"..\..\.." -I"..\..\..\CCF" -I"..\..\..\..\..\contrib\utility" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -f "Makefile.CIDL.dep" "Parser.cpp" "LexicalAnalyzer.cpp" ".\SemanticGraph\Elements.cpp" ".\SemanticGraph\Executor.cpp" ".\SemanticGraph\Composition.cpp" ".\Traversal\Executor.cpp" ".\SemanticAction\Composition.cpp" ".\SemanticAction\Impl\Factory.cpp" ".\SemanticAction\Impl\HomeExecutor.cpp" ".\SemanticAction\Impl\Composition.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIDLsd.lib"
	-@del /f/q "$(OUTDIR)\CIDLsd.exp"
	-@del /f/q "$(OUTDIR)\CIDLsd.ilk"
	-@del /f/q "..\CIDLsd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\CIDL\$(NULL)" mkdir "Static_Debug\CIDL"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4290 /wd4800 /wd4250 /wd4355 /wd4996 /Fd"..\CIDLsd.pdb" /I "$(BOOST_ROOT)\include\$(BOOST_VERSION)" /I "$(BOOST_ROOT)\." /I "..\..\.." /I "..\..\..\CCF" /I "..\..\..\..\..\contrib\utility" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\CIDLsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Parser.obj" \
	"$(INTDIR)\LexicalAnalyzer.obj" \
	"$(INTDIR)\.\SemanticGraph\Elements.obj" \
	"$(INTDIR)\.\SemanticGraph\Executor.obj" \
	"$(INTDIR)\.\SemanticGraph\Composition.obj" \
	"$(INTDIR)\.\Traversal\Executor.obj" \
	"$(INTDIR)\.\SemanticAction\Composition.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Factory.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\HomeExecutor.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Composition.obj"

"$(OUTDIR)\CIDLsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CIDLsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\CIDLsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..
INTDIR=Static_Release\CIDL\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CIDLs.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"$(BOOST_ROOT)\include\$(BOOST_VERSION)" -I"$(BOOST_ROOT)\." -I"..\..\.." -I"..\..\..\CCF" -I"..\..\..\..\..\contrib\utility" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -f "Makefile.CIDL.dep" "Parser.cpp" "LexicalAnalyzer.cpp" ".\SemanticGraph\Elements.cpp" ".\SemanticGraph\Executor.cpp" ".\SemanticGraph\Composition.cpp" ".\Traversal\Executor.cpp" ".\SemanticAction\Composition.cpp" ".\SemanticAction\Impl\Factory.cpp" ".\SemanticAction\Impl\HomeExecutor.cpp" ".\SemanticAction\Impl\Composition.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CIDLs.lib"
	-@del /f/q "$(OUTDIR)\CIDLs.exp"
	-@del /f/q "$(OUTDIR)\CIDLs.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\CIDL\$(NULL)" mkdir "Static_Release\CIDL"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4290 /wd4800 /wd4250 /wd4355 /wd4996 /I "$(BOOST_ROOT)\include\$(BOOST_VERSION)" /I "$(BOOST_ROOT)\." /I "..\..\.." /I "..\..\..\CCF" /I "..\..\..\..\..\contrib\utility" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\CIDLs.lib"
LINK32_OBJS= \
	"$(INTDIR)\Parser.obj" \
	"$(INTDIR)\LexicalAnalyzer.obj" \
	"$(INTDIR)\.\SemanticGraph\Elements.obj" \
	"$(INTDIR)\.\SemanticGraph\Executor.obj" \
	"$(INTDIR)\.\SemanticGraph\Composition.obj" \
	"$(INTDIR)\.\Traversal\Executor.obj" \
	"$(INTDIR)\.\SemanticAction\Composition.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Factory.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\HomeExecutor.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Composition.obj"

"$(OUTDIR)\CIDLs.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CIDLs.lib.manifest" mt.exe -manifest "$(OUTDIR)\CIDLs.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CIDL.dep")
!INCLUDE "Makefile.CIDL.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Parser.cpp"

"$(INTDIR)\Parser.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Parser.obj" $(SOURCE)

SOURCE="LexicalAnalyzer.cpp"

"$(INTDIR)\LexicalAnalyzer.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\LexicalAnalyzer.obj" $(SOURCE)

SOURCE=".\SemanticGraph\Elements.cpp"

"$(INTDIR)\.\SemanticGraph\Elements.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\Elements.obj" $(SOURCE)

SOURCE=".\SemanticGraph\Executor.cpp"

"$(INTDIR)\.\SemanticGraph\Executor.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\Executor.obj" $(SOURCE)

SOURCE=".\SemanticGraph\Composition.cpp"

"$(INTDIR)\.\SemanticGraph\Composition.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\Composition.obj" $(SOURCE)

SOURCE=".\Traversal\Executor.cpp"

"$(INTDIR)\.\Traversal\Executor.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\Traversal\$(NULL)" mkdir "$(INTDIR)\.\Traversal\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\Traversal\Executor.obj" $(SOURCE)

SOURCE=".\SemanticAction\Composition.cpp"

"$(INTDIR)\.\SemanticAction\Composition.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Composition.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Factory.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Factory.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\HomeExecutor.cpp"

"$(INTDIR)\.\SemanticAction\Impl\HomeExecutor.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\HomeExecutor.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Composition.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Composition.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Composition.obj" $(SOURCE)


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CIDL.dep")
	@echo Using "Makefile.CIDL.dep"
!ELSE
	@echo Warning: cannot find "Makefile.CIDL.dep"
!ENDIF
!ENDIF

