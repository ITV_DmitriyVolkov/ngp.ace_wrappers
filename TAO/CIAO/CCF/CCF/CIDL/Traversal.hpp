// file      : CCF/CIDL/Traversal.hpp
// author    : Boris Kolpackov <boris@dre.vanderbilt.edu>
// cvs-id    : $Id: Traversal.hpp 14 2007-02-01 15:49:12Z mitza $

#ifndef CCF_CIDL_TRAVERSAL_HPP
#define CCF_CIDL_TRAVERSAL_HPP

#include "CCF/IDL3/Traversal.hpp"

#include "CCF/CIDL/Traversal/Elements.hpp"

#include "CCF/CIDL/Traversal/Composition.hpp"
#include "CCF/CIDL/Traversal/Executor.hpp"

#endif  // CCF_CIDL_TRAVERSAL_HPP
