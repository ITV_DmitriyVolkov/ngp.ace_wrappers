# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.IDL3.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..
INTDIR=Debug\IDL3\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\IDL3d.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"$(BOOST_ROOT)\include\$(BOOST_VERSION)" -I"$(BOOST_ROOT)\." -I"..\..\.." -I"..\..\..\CCF" -I"..\..\..\..\..\contrib\utility" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -f "Makefile.IDL3.dep" "Parser.cpp" "LexicalAnalyzer.cpp" ".\SemanticGraph\Elements.cpp" ".\SemanticGraph\Home.cpp" ".\SemanticGraph\EventType.cpp" ".\SemanticGraph\Component.cpp" ".\Traversal\Home.cpp" ".\Traversal\Component.cpp" ".\SemanticAction\Impl\EventTypeFactory.cpp" ".\SemanticAction\Impl\Uses.cpp" ".\SemanticAction\Impl\Consumes.cpp" ".\SemanticAction\Impl\Factory.cpp" ".\SemanticAction\Impl\Home.cpp" ".\SemanticAction\Impl\EventType.cpp" ".\SemanticAction\Impl\Publishes.cpp" ".\SemanticAction\Impl\Component.cpp" ".\SemanticAction\Impl\HomeFinder.cpp" ".\SemanticAction\Impl\Provides.cpp" ".\SemanticAction\Impl\HomeFactory.cpp" ".\SemanticAction\Impl\Include.cpp" ".\SemanticAction\Impl\Emits.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\IDL3d.lib"
	-@del /f/q "$(OUTDIR)\IDL3d.exp"
	-@del /f/q "$(OUTDIR)\IDL3d.ilk"
	-@del /f/q "..\IDL3d.pdb"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\IDL3\$(NULL)" mkdir "Debug\IDL3"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4290 /wd4800 /wd4250 /wd4355 /wd4996 /Fd"..\IDL3d.pdb" /I "$(BOOST_ROOT)\include\$(BOOST_VERSION)" /I "$(BOOST_ROOT)\." /I "..\..\.." /I "..\..\..\CCF" /I "..\..\..\..\..\contrib\utility" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\IDL3d.lib"
LINK32_OBJS= \
	"$(INTDIR)\Parser.obj" \
	"$(INTDIR)\LexicalAnalyzer.obj" \
	"$(INTDIR)\.\SemanticGraph\Elements.obj" \
	"$(INTDIR)\.\SemanticGraph\Home.obj" \
	"$(INTDIR)\.\SemanticGraph\EventType.obj" \
	"$(INTDIR)\.\SemanticGraph\Component.obj" \
	"$(INTDIR)\.\Traversal\Home.obj" \
	"$(INTDIR)\.\Traversal\Component.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\EventTypeFactory.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Uses.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Consumes.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Factory.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Home.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\EventType.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Publishes.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Component.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\HomeFinder.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Provides.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\HomeFactory.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Include.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Emits.obj"

"$(OUTDIR)\IDL3d.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\IDL3d.lib.manifest" mt.exe -manifest "$(OUTDIR)\IDL3d.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..
INTDIR=Release\IDL3\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\IDL3.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"$(BOOST_ROOT)\include\$(BOOST_VERSION)" -I"$(BOOST_ROOT)\." -I"..\..\.." -I"..\..\..\CCF" -I"..\..\..\..\..\contrib\utility" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -f "Makefile.IDL3.dep" "Parser.cpp" "LexicalAnalyzer.cpp" ".\SemanticGraph\Elements.cpp" ".\SemanticGraph\Home.cpp" ".\SemanticGraph\EventType.cpp" ".\SemanticGraph\Component.cpp" ".\Traversal\Home.cpp" ".\Traversal\Component.cpp" ".\SemanticAction\Impl\EventTypeFactory.cpp" ".\SemanticAction\Impl\Uses.cpp" ".\SemanticAction\Impl\Consumes.cpp" ".\SemanticAction\Impl\Factory.cpp" ".\SemanticAction\Impl\Home.cpp" ".\SemanticAction\Impl\EventType.cpp" ".\SemanticAction\Impl\Publishes.cpp" ".\SemanticAction\Impl\Component.cpp" ".\SemanticAction\Impl\HomeFinder.cpp" ".\SemanticAction\Impl\Provides.cpp" ".\SemanticAction\Impl\HomeFactory.cpp" ".\SemanticAction\Impl\Include.cpp" ".\SemanticAction\Impl\Emits.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\IDL3.lib"
	-@del /f/q "$(OUTDIR)\IDL3.exp"
	-@del /f/q "$(OUTDIR)\IDL3.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\IDL3\$(NULL)" mkdir "Release\IDL3"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4290 /wd4800 /wd4250 /wd4355 /wd4996 /I "$(BOOST_ROOT)\include\$(BOOST_VERSION)" /I "$(BOOST_ROOT)\." /I "..\..\.." /I "..\..\..\CCF" /I "..\..\..\..\..\contrib\utility" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\IDL3.lib"
LINK32_OBJS= \
	"$(INTDIR)\Parser.obj" \
	"$(INTDIR)\LexicalAnalyzer.obj" \
	"$(INTDIR)\.\SemanticGraph\Elements.obj" \
	"$(INTDIR)\.\SemanticGraph\Home.obj" \
	"$(INTDIR)\.\SemanticGraph\EventType.obj" \
	"$(INTDIR)\.\SemanticGraph\Component.obj" \
	"$(INTDIR)\.\Traversal\Home.obj" \
	"$(INTDIR)\.\Traversal\Component.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\EventTypeFactory.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Uses.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Consumes.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Factory.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Home.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\EventType.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Publishes.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Component.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\HomeFinder.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Provides.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\HomeFactory.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Include.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Emits.obj"

"$(OUTDIR)\IDL3.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\IDL3.lib.manifest" mt.exe -manifest "$(OUTDIR)\IDL3.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..
INTDIR=Static_Debug\IDL3\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\IDL3sd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"$(BOOST_ROOT)\include\$(BOOST_VERSION)" -I"$(BOOST_ROOT)\." -I"..\..\.." -I"..\..\..\CCF" -I"..\..\..\..\..\contrib\utility" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -f "Makefile.IDL3.dep" "Parser.cpp" "LexicalAnalyzer.cpp" ".\SemanticGraph\Elements.cpp" ".\SemanticGraph\Home.cpp" ".\SemanticGraph\EventType.cpp" ".\SemanticGraph\Component.cpp" ".\Traversal\Home.cpp" ".\Traversal\Component.cpp" ".\SemanticAction\Impl\EventTypeFactory.cpp" ".\SemanticAction\Impl\Uses.cpp" ".\SemanticAction\Impl\Consumes.cpp" ".\SemanticAction\Impl\Factory.cpp" ".\SemanticAction\Impl\Home.cpp" ".\SemanticAction\Impl\EventType.cpp" ".\SemanticAction\Impl\Publishes.cpp" ".\SemanticAction\Impl\Component.cpp" ".\SemanticAction\Impl\HomeFinder.cpp" ".\SemanticAction\Impl\Provides.cpp" ".\SemanticAction\Impl\HomeFactory.cpp" ".\SemanticAction\Impl\Include.cpp" ".\SemanticAction\Impl\Emits.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\IDL3sd.lib"
	-@del /f/q "$(OUTDIR)\IDL3sd.exp"
	-@del /f/q "$(OUTDIR)\IDL3sd.ilk"
	-@del /f/q "..\IDL3sd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\IDL3\$(NULL)" mkdir "Static_Debug\IDL3"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4290 /wd4800 /wd4250 /wd4355 /wd4996 /Fd"..\IDL3sd.pdb" /I "$(BOOST_ROOT)\include\$(BOOST_VERSION)" /I "$(BOOST_ROOT)\." /I "..\..\.." /I "..\..\..\CCF" /I "..\..\..\..\..\contrib\utility" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\IDL3sd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Parser.obj" \
	"$(INTDIR)\LexicalAnalyzer.obj" \
	"$(INTDIR)\.\SemanticGraph\Elements.obj" \
	"$(INTDIR)\.\SemanticGraph\Home.obj" \
	"$(INTDIR)\.\SemanticGraph\EventType.obj" \
	"$(INTDIR)\.\SemanticGraph\Component.obj" \
	"$(INTDIR)\.\Traversal\Home.obj" \
	"$(INTDIR)\.\Traversal\Component.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\EventTypeFactory.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Uses.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Consumes.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Factory.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Home.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\EventType.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Publishes.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Component.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\HomeFinder.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Provides.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\HomeFactory.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Include.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Emits.obj"

"$(OUTDIR)\IDL3sd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\IDL3sd.lib.manifest" mt.exe -manifest "$(OUTDIR)\IDL3sd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..
INTDIR=Static_Release\IDL3\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\IDL3s.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"$(BOOST_ROOT)\include\$(BOOST_VERSION)" -I"$(BOOST_ROOT)\." -I"..\..\.." -I"..\..\..\CCF" -I"..\..\..\..\..\contrib\utility" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -f "Makefile.IDL3.dep" "Parser.cpp" "LexicalAnalyzer.cpp" ".\SemanticGraph\Elements.cpp" ".\SemanticGraph\Home.cpp" ".\SemanticGraph\EventType.cpp" ".\SemanticGraph\Component.cpp" ".\Traversal\Home.cpp" ".\Traversal\Component.cpp" ".\SemanticAction\Impl\EventTypeFactory.cpp" ".\SemanticAction\Impl\Uses.cpp" ".\SemanticAction\Impl\Consumes.cpp" ".\SemanticAction\Impl\Factory.cpp" ".\SemanticAction\Impl\Home.cpp" ".\SemanticAction\Impl\EventType.cpp" ".\SemanticAction\Impl\Publishes.cpp" ".\SemanticAction\Impl\Component.cpp" ".\SemanticAction\Impl\HomeFinder.cpp" ".\SemanticAction\Impl\Provides.cpp" ".\SemanticAction\Impl\HomeFactory.cpp" ".\SemanticAction\Impl\Include.cpp" ".\SemanticAction\Impl\Emits.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\IDL3s.lib"
	-@del /f/q "$(OUTDIR)\IDL3s.exp"
	-@del /f/q "$(OUTDIR)\IDL3s.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\IDL3\$(NULL)" mkdir "Static_Release\IDL3"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4290 /wd4800 /wd4250 /wd4355 /wd4996 /I "$(BOOST_ROOT)\include\$(BOOST_VERSION)" /I "$(BOOST_ROOT)\." /I "..\..\.." /I "..\..\..\CCF" /I "..\..\..\..\..\contrib\utility" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\IDL3s.lib"
LINK32_OBJS= \
	"$(INTDIR)\Parser.obj" \
	"$(INTDIR)\LexicalAnalyzer.obj" \
	"$(INTDIR)\.\SemanticGraph\Elements.obj" \
	"$(INTDIR)\.\SemanticGraph\Home.obj" \
	"$(INTDIR)\.\SemanticGraph\EventType.obj" \
	"$(INTDIR)\.\SemanticGraph\Component.obj" \
	"$(INTDIR)\.\Traversal\Home.obj" \
	"$(INTDIR)\.\Traversal\Component.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\EventTypeFactory.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Uses.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Consumes.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Factory.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Home.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\EventType.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Publishes.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Component.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\HomeFinder.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Provides.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\HomeFactory.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Include.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Emits.obj"

"$(OUTDIR)\IDL3s.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\IDL3s.lib.manifest" mt.exe -manifest "$(OUTDIR)\IDL3s.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.IDL3.dep")
!INCLUDE "Makefile.IDL3.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Parser.cpp"

"$(INTDIR)\Parser.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Parser.obj" $(SOURCE)

SOURCE="LexicalAnalyzer.cpp"

"$(INTDIR)\LexicalAnalyzer.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\LexicalAnalyzer.obj" $(SOURCE)

SOURCE=".\SemanticGraph\Elements.cpp"

"$(INTDIR)\.\SemanticGraph\Elements.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\Elements.obj" $(SOURCE)

SOURCE=".\SemanticGraph\Home.cpp"

"$(INTDIR)\.\SemanticGraph\Home.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\Home.obj" $(SOURCE)

SOURCE=".\SemanticGraph\EventType.cpp"

"$(INTDIR)\.\SemanticGraph\EventType.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\EventType.obj" $(SOURCE)

SOURCE=".\SemanticGraph\Component.cpp"

"$(INTDIR)\.\SemanticGraph\Component.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\Component.obj" $(SOURCE)

SOURCE=".\Traversal\Home.cpp"

"$(INTDIR)\.\Traversal\Home.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\Traversal\$(NULL)" mkdir "$(INTDIR)\.\Traversal\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\Traversal\Home.obj" $(SOURCE)

SOURCE=".\Traversal\Component.cpp"

"$(INTDIR)\.\Traversal\Component.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\Traversal\$(NULL)" mkdir "$(INTDIR)\.\Traversal\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\Traversal\Component.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\EventTypeFactory.cpp"

"$(INTDIR)\.\SemanticAction\Impl\EventTypeFactory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\EventTypeFactory.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Uses.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Uses.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Uses.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Consumes.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Consumes.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Consumes.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Factory.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Factory.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Home.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Home.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Home.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\EventType.cpp"

"$(INTDIR)\.\SemanticAction\Impl\EventType.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\EventType.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Publishes.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Publishes.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Publishes.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Component.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Component.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Component.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\HomeFinder.cpp"

"$(INTDIR)\.\SemanticAction\Impl\HomeFinder.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\HomeFinder.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Provides.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Provides.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Provides.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\HomeFactory.cpp"

"$(INTDIR)\.\SemanticAction\Impl\HomeFactory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\HomeFactory.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Include.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Include.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Include.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Emits.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Emits.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Emits.obj" $(SOURCE)


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.IDL3.dep")
	@echo Using "Makefile.IDL3.dep"
!ELSE
	@echo Warning: cannot find "Makefile.IDL3.dep"
!ENDIF
!ENDIF

