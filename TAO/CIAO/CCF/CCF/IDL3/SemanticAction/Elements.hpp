// file      : CCF/IDL3/SemanticAction/Elements.hpp
// author    : Boris Kolpackov <boris@dre.vanderbilt.edu>
// cvs-id    : $Id: Elements.hpp 14 2007-02-01 15:49:12Z mitza $

#ifndef CCF_IDL3_SEMANTIC_ACTION_ELEMENTS_HPP
#define CCF_IDL3_SEMANTIC_ACTION_ELEMENTS_HPP

#include "CCF/IDL2/SemanticAction/Elements.hpp"

#include "CCF/IDL3/Token.hpp"

namespace CCF
{
  namespace IDL3
  {
    namespace SemanticAction
    {
      using IDL2::SemanticAction::Scope;
    }
  }
}

#endif  // CCF_IDL3_SEMANTIC_ACTION_ELEMENTS_HPP
