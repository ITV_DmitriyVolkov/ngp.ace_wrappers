# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.IDL2.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..
INTDIR=Debug\IDL2\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\IDL2d.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"$(BOOST_ROOT)\include\$(BOOST_VERSION)" -I"$(BOOST_ROOT)\." -I"..\..\.." -I"..\..\..\CCF" -I"..\..\..\..\..\contrib\utility" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -f "Makefile.IDL2.dep" "Parser.cpp" "LexicalAnalyzer.cpp" "Token.cpp" ".\SemanticGraph\Name.cpp" ".\SemanticGraph\Module.cpp" ".\SemanticGraph\Struct.cpp" ".\SemanticGraph\Operation.cpp" ".\SemanticGraph\Elements.cpp" ".\SemanticGraph\Union.cpp" ".\SemanticGraph\Member.cpp" ".\SemanticGraph\ValueType.cpp" ".\SemanticGraph\Enum.cpp" ".\SemanticGraph\Interface.cpp" ".\SemanticGraph\Attribute.cpp" ".\SemanticGraph\Native.cpp" ".\SemanticGraph\String.cpp" ".\SemanticGraph\Translation.cpp" ".\SemanticGraph\Fundamental.cpp" ".\SemanticGraph\TypeId.cpp" ".\SemanticGraph\Array.cpp" ".\SemanticGraph\Exception.cpp" ".\SemanticGraph\Literals.cpp" ".\SemanticGraph\Sequence.cpp" ".\SemanticGraph\IntExpression.cpp" ".\SemanticGraph\ValueTypeMember.cpp" ".\Traversal\Module.cpp" ".\Traversal\Struct.cpp" ".\Traversal\Operation.cpp" ".\Traversal\Elements.cpp" ".\Traversal\Union.cpp" ".\Traversal\Member.cpp" ".\Traversal\ValueType.cpp" ".\Traversal\Enum.cpp" ".\Traversal\Interface.cpp" ".\Traversal\Attribute.cpp" ".\Traversal\Native.cpp" ".\Traversal\String.cpp" ".\Traversal\Translation.cpp" ".\Traversal\Fundamental.cpp" ".\Traversal\TypeId.cpp" ".\Traversal\Array.cpp" ".\Traversal\Exception.cpp" ".\Traversal\Sequence.cpp" ".\SemanticAction\Operation.cpp" ".\SemanticAction\Impl\Const.cpp" ".\SemanticAction\Impl\Typedef.cpp" ".\SemanticAction\Impl\Module.cpp" ".\SemanticAction\Impl\Struct.cpp" ".\SemanticAction\Impl\NumericExpression.cpp" ".\SemanticAction\Impl\Operation.cpp" ".\SemanticAction\Impl\Elements.cpp" ".\SemanticAction\Impl\Factory.cpp" ".\SemanticAction\Impl\Union.cpp" ".\SemanticAction\Impl\Member.cpp" ".\SemanticAction\Impl\ValueType.cpp" ".\SemanticAction\Impl\Enum.cpp" ".\SemanticAction\Impl\Interface.cpp" ".\SemanticAction\Impl\ValueTypeFactory.cpp" ".\SemanticAction\Impl\Attribute.cpp" ".\SemanticAction\Impl\Native.cpp" ".\SemanticAction\Impl\TypeId.cpp" ".\SemanticAction\Impl\Exception.cpp" ".\SemanticAction\Impl\Include.cpp" ".\SemanticAction\Impl\ValueTypeMember.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\IDL2d.lib"
	-@del /f/q "$(OUTDIR)\IDL2d.exp"
	-@del /f/q "$(OUTDIR)\IDL2d.ilk"
	-@del /f/q "..\IDL2d.pdb"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\IDL2\$(NULL)" mkdir "Debug\IDL2"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4290 /wd4800 /wd4250 /wd4355 /wd4996 /Fd"..\IDL2d.pdb" /I "$(BOOST_ROOT)\include\$(BOOST_VERSION)" /I "$(BOOST_ROOT)\." /I "..\..\.." /I "..\..\..\CCF" /I "..\..\..\..\..\contrib\utility" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\IDL2d.lib"
LINK32_OBJS= \
	"$(INTDIR)\Parser.obj" \
	"$(INTDIR)\LexicalAnalyzer.obj" \
	"$(INTDIR)\Token.obj" \
	"$(INTDIR)\.\SemanticGraph\Name.obj" \
	"$(INTDIR)\.\SemanticGraph\Module.obj" \
	"$(INTDIR)\.\SemanticGraph\Struct.obj" \
	"$(INTDIR)\.\SemanticGraph\Operation.obj" \
	"$(INTDIR)\.\SemanticGraph\Elements.obj" \
	"$(INTDIR)\.\SemanticGraph\Union.obj" \
	"$(INTDIR)\.\SemanticGraph\Member.obj" \
	"$(INTDIR)\.\SemanticGraph\ValueType.obj" \
	"$(INTDIR)\.\SemanticGraph\Enum.obj" \
	"$(INTDIR)\.\SemanticGraph\Interface.obj" \
	"$(INTDIR)\.\SemanticGraph\Attribute.obj" \
	"$(INTDIR)\.\SemanticGraph\Native.obj" \
	"$(INTDIR)\.\SemanticGraph\String.obj" \
	"$(INTDIR)\.\SemanticGraph\Translation.obj" \
	"$(INTDIR)\.\SemanticGraph\Fundamental.obj" \
	"$(INTDIR)\.\SemanticGraph\TypeId.obj" \
	"$(INTDIR)\.\SemanticGraph\Array.obj" \
	"$(INTDIR)\.\SemanticGraph\Exception.obj" \
	"$(INTDIR)\.\SemanticGraph\Literals.obj" \
	"$(INTDIR)\.\SemanticGraph\Sequence.obj" \
	"$(INTDIR)\.\SemanticGraph\IntExpression.obj" \
	"$(INTDIR)\.\SemanticGraph\ValueTypeMember.obj" \
	"$(INTDIR)\.\Traversal\Module.obj" \
	"$(INTDIR)\.\Traversal\Struct.obj" \
	"$(INTDIR)\.\Traversal\Operation.obj" \
	"$(INTDIR)\.\Traversal\Elements.obj" \
	"$(INTDIR)\.\Traversal\Union.obj" \
	"$(INTDIR)\.\Traversal\Member.obj" \
	"$(INTDIR)\.\Traversal\ValueType.obj" \
	"$(INTDIR)\.\Traversal\Enum.obj" \
	"$(INTDIR)\.\Traversal\Interface.obj" \
	"$(INTDIR)\.\Traversal\Attribute.obj" \
	"$(INTDIR)\.\Traversal\Native.obj" \
	"$(INTDIR)\.\Traversal\String.obj" \
	"$(INTDIR)\.\Traversal\Translation.obj" \
	"$(INTDIR)\.\Traversal\Fundamental.obj" \
	"$(INTDIR)\.\Traversal\TypeId.obj" \
	"$(INTDIR)\.\Traversal\Array.obj" \
	"$(INTDIR)\.\Traversal\Exception.obj" \
	"$(INTDIR)\.\Traversal\Sequence.obj" \
	"$(INTDIR)\.\SemanticAction\Operation.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Const.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Typedef.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Module.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Struct.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\NumericExpression.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Operation.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Elements.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Factory.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Union.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Member.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\ValueType.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Enum.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Interface.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\ValueTypeFactory.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Attribute.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Native.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\TypeId.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Exception.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Include.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\ValueTypeMember.obj"

"$(OUTDIR)\IDL2d.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\IDL2d.lib.manifest" mt.exe -manifest "$(OUTDIR)\IDL2d.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..
INTDIR=Release\IDL2\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\IDL2.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"$(BOOST_ROOT)\include\$(BOOST_VERSION)" -I"$(BOOST_ROOT)\." -I"..\..\.." -I"..\..\..\CCF" -I"..\..\..\..\..\contrib\utility" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -f "Makefile.IDL2.dep" "Parser.cpp" "LexicalAnalyzer.cpp" "Token.cpp" ".\SemanticGraph\Name.cpp" ".\SemanticGraph\Module.cpp" ".\SemanticGraph\Struct.cpp" ".\SemanticGraph\Operation.cpp" ".\SemanticGraph\Elements.cpp" ".\SemanticGraph\Union.cpp" ".\SemanticGraph\Member.cpp" ".\SemanticGraph\ValueType.cpp" ".\SemanticGraph\Enum.cpp" ".\SemanticGraph\Interface.cpp" ".\SemanticGraph\Attribute.cpp" ".\SemanticGraph\Native.cpp" ".\SemanticGraph\String.cpp" ".\SemanticGraph\Translation.cpp" ".\SemanticGraph\Fundamental.cpp" ".\SemanticGraph\TypeId.cpp" ".\SemanticGraph\Array.cpp" ".\SemanticGraph\Exception.cpp" ".\SemanticGraph\Literals.cpp" ".\SemanticGraph\Sequence.cpp" ".\SemanticGraph\IntExpression.cpp" ".\SemanticGraph\ValueTypeMember.cpp" ".\Traversal\Module.cpp" ".\Traversal\Struct.cpp" ".\Traversal\Operation.cpp" ".\Traversal\Elements.cpp" ".\Traversal\Union.cpp" ".\Traversal\Member.cpp" ".\Traversal\ValueType.cpp" ".\Traversal\Enum.cpp" ".\Traversal\Interface.cpp" ".\Traversal\Attribute.cpp" ".\Traversal\Native.cpp" ".\Traversal\String.cpp" ".\Traversal\Translation.cpp" ".\Traversal\Fundamental.cpp" ".\Traversal\TypeId.cpp" ".\Traversal\Array.cpp" ".\Traversal\Exception.cpp" ".\Traversal\Sequence.cpp" ".\SemanticAction\Operation.cpp" ".\SemanticAction\Impl\Const.cpp" ".\SemanticAction\Impl\Typedef.cpp" ".\SemanticAction\Impl\Module.cpp" ".\SemanticAction\Impl\Struct.cpp" ".\SemanticAction\Impl\NumericExpression.cpp" ".\SemanticAction\Impl\Operation.cpp" ".\SemanticAction\Impl\Elements.cpp" ".\SemanticAction\Impl\Factory.cpp" ".\SemanticAction\Impl\Union.cpp" ".\SemanticAction\Impl\Member.cpp" ".\SemanticAction\Impl\ValueType.cpp" ".\SemanticAction\Impl\Enum.cpp" ".\SemanticAction\Impl\Interface.cpp" ".\SemanticAction\Impl\ValueTypeFactory.cpp" ".\SemanticAction\Impl\Attribute.cpp" ".\SemanticAction\Impl\Native.cpp" ".\SemanticAction\Impl\TypeId.cpp" ".\SemanticAction\Impl\Exception.cpp" ".\SemanticAction\Impl\Include.cpp" ".\SemanticAction\Impl\ValueTypeMember.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\IDL2.lib"
	-@del /f/q "$(OUTDIR)\IDL2.exp"
	-@del /f/q "$(OUTDIR)\IDL2.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\IDL2\$(NULL)" mkdir "Release\IDL2"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4290 /wd4800 /wd4250 /wd4355 /wd4996 /I "$(BOOST_ROOT)\include\$(BOOST_VERSION)" /I "$(BOOST_ROOT)\." /I "..\..\.." /I "..\..\..\CCF" /I "..\..\..\..\..\contrib\utility" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\IDL2.lib"
LINK32_OBJS= \
	"$(INTDIR)\Parser.obj" \
	"$(INTDIR)\LexicalAnalyzer.obj" \
	"$(INTDIR)\Token.obj" \
	"$(INTDIR)\.\SemanticGraph\Name.obj" \
	"$(INTDIR)\.\SemanticGraph\Module.obj" \
	"$(INTDIR)\.\SemanticGraph\Struct.obj" \
	"$(INTDIR)\.\SemanticGraph\Operation.obj" \
	"$(INTDIR)\.\SemanticGraph\Elements.obj" \
	"$(INTDIR)\.\SemanticGraph\Union.obj" \
	"$(INTDIR)\.\SemanticGraph\Member.obj" \
	"$(INTDIR)\.\SemanticGraph\ValueType.obj" \
	"$(INTDIR)\.\SemanticGraph\Enum.obj" \
	"$(INTDIR)\.\SemanticGraph\Interface.obj" \
	"$(INTDIR)\.\SemanticGraph\Attribute.obj" \
	"$(INTDIR)\.\SemanticGraph\Native.obj" \
	"$(INTDIR)\.\SemanticGraph\String.obj" \
	"$(INTDIR)\.\SemanticGraph\Translation.obj" \
	"$(INTDIR)\.\SemanticGraph\Fundamental.obj" \
	"$(INTDIR)\.\SemanticGraph\TypeId.obj" \
	"$(INTDIR)\.\SemanticGraph\Array.obj" \
	"$(INTDIR)\.\SemanticGraph\Exception.obj" \
	"$(INTDIR)\.\SemanticGraph\Literals.obj" \
	"$(INTDIR)\.\SemanticGraph\Sequence.obj" \
	"$(INTDIR)\.\SemanticGraph\IntExpression.obj" \
	"$(INTDIR)\.\SemanticGraph\ValueTypeMember.obj" \
	"$(INTDIR)\.\Traversal\Module.obj" \
	"$(INTDIR)\.\Traversal\Struct.obj" \
	"$(INTDIR)\.\Traversal\Operation.obj" \
	"$(INTDIR)\.\Traversal\Elements.obj" \
	"$(INTDIR)\.\Traversal\Union.obj" \
	"$(INTDIR)\.\Traversal\Member.obj" \
	"$(INTDIR)\.\Traversal\ValueType.obj" \
	"$(INTDIR)\.\Traversal\Enum.obj" \
	"$(INTDIR)\.\Traversal\Interface.obj" \
	"$(INTDIR)\.\Traversal\Attribute.obj" \
	"$(INTDIR)\.\Traversal\Native.obj" \
	"$(INTDIR)\.\Traversal\String.obj" \
	"$(INTDIR)\.\Traversal\Translation.obj" \
	"$(INTDIR)\.\Traversal\Fundamental.obj" \
	"$(INTDIR)\.\Traversal\TypeId.obj" \
	"$(INTDIR)\.\Traversal\Array.obj" \
	"$(INTDIR)\.\Traversal\Exception.obj" \
	"$(INTDIR)\.\Traversal\Sequence.obj" \
	"$(INTDIR)\.\SemanticAction\Operation.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Const.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Typedef.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Module.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Struct.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\NumericExpression.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Operation.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Elements.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Factory.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Union.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Member.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\ValueType.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Enum.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Interface.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\ValueTypeFactory.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Attribute.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Native.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\TypeId.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Exception.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Include.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\ValueTypeMember.obj"

"$(OUTDIR)\IDL2.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\IDL2.lib.manifest" mt.exe -manifest "$(OUTDIR)\IDL2.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..
INTDIR=Static_Debug\IDL2\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\IDL2sd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"$(BOOST_ROOT)\include\$(BOOST_VERSION)" -I"$(BOOST_ROOT)\." -I"..\..\.." -I"..\..\..\CCF" -I"..\..\..\..\..\contrib\utility" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -f "Makefile.IDL2.dep" "Parser.cpp" "LexicalAnalyzer.cpp" "Token.cpp" ".\SemanticGraph\Name.cpp" ".\SemanticGraph\Module.cpp" ".\SemanticGraph\Struct.cpp" ".\SemanticGraph\Operation.cpp" ".\SemanticGraph\Elements.cpp" ".\SemanticGraph\Union.cpp" ".\SemanticGraph\Member.cpp" ".\SemanticGraph\ValueType.cpp" ".\SemanticGraph\Enum.cpp" ".\SemanticGraph\Interface.cpp" ".\SemanticGraph\Attribute.cpp" ".\SemanticGraph\Native.cpp" ".\SemanticGraph\String.cpp" ".\SemanticGraph\Translation.cpp" ".\SemanticGraph\Fundamental.cpp" ".\SemanticGraph\TypeId.cpp" ".\SemanticGraph\Array.cpp" ".\SemanticGraph\Exception.cpp" ".\SemanticGraph\Literals.cpp" ".\SemanticGraph\Sequence.cpp" ".\SemanticGraph\IntExpression.cpp" ".\SemanticGraph\ValueTypeMember.cpp" ".\Traversal\Module.cpp" ".\Traversal\Struct.cpp" ".\Traversal\Operation.cpp" ".\Traversal\Elements.cpp" ".\Traversal\Union.cpp" ".\Traversal\Member.cpp" ".\Traversal\ValueType.cpp" ".\Traversal\Enum.cpp" ".\Traversal\Interface.cpp" ".\Traversal\Attribute.cpp" ".\Traversal\Native.cpp" ".\Traversal\String.cpp" ".\Traversal\Translation.cpp" ".\Traversal\Fundamental.cpp" ".\Traversal\TypeId.cpp" ".\Traversal\Array.cpp" ".\Traversal\Exception.cpp" ".\Traversal\Sequence.cpp" ".\SemanticAction\Operation.cpp" ".\SemanticAction\Impl\Const.cpp" ".\SemanticAction\Impl\Typedef.cpp" ".\SemanticAction\Impl\Module.cpp" ".\SemanticAction\Impl\Struct.cpp" ".\SemanticAction\Impl\NumericExpression.cpp" ".\SemanticAction\Impl\Operation.cpp" ".\SemanticAction\Impl\Elements.cpp" ".\SemanticAction\Impl\Factory.cpp" ".\SemanticAction\Impl\Union.cpp" ".\SemanticAction\Impl\Member.cpp" ".\SemanticAction\Impl\ValueType.cpp" ".\SemanticAction\Impl\Enum.cpp" ".\SemanticAction\Impl\Interface.cpp" ".\SemanticAction\Impl\ValueTypeFactory.cpp" ".\SemanticAction\Impl\Attribute.cpp" ".\SemanticAction\Impl\Native.cpp" ".\SemanticAction\Impl\TypeId.cpp" ".\SemanticAction\Impl\Exception.cpp" ".\SemanticAction\Impl\Include.cpp" ".\SemanticAction\Impl\ValueTypeMember.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\IDL2sd.lib"
	-@del /f/q "$(OUTDIR)\IDL2sd.exp"
	-@del /f/q "$(OUTDIR)\IDL2sd.ilk"
	-@del /f/q "..\IDL2sd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\IDL2\$(NULL)" mkdir "Static_Debug\IDL2"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4290 /wd4800 /wd4250 /wd4355 /wd4996 /Fd"..\IDL2sd.pdb" /I "$(BOOST_ROOT)\include\$(BOOST_VERSION)" /I "$(BOOST_ROOT)\." /I "..\..\.." /I "..\..\..\CCF" /I "..\..\..\..\..\contrib\utility" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\IDL2sd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Parser.obj" \
	"$(INTDIR)\LexicalAnalyzer.obj" \
	"$(INTDIR)\Token.obj" \
	"$(INTDIR)\.\SemanticGraph\Name.obj" \
	"$(INTDIR)\.\SemanticGraph\Module.obj" \
	"$(INTDIR)\.\SemanticGraph\Struct.obj" \
	"$(INTDIR)\.\SemanticGraph\Operation.obj" \
	"$(INTDIR)\.\SemanticGraph\Elements.obj" \
	"$(INTDIR)\.\SemanticGraph\Union.obj" \
	"$(INTDIR)\.\SemanticGraph\Member.obj" \
	"$(INTDIR)\.\SemanticGraph\ValueType.obj" \
	"$(INTDIR)\.\SemanticGraph\Enum.obj" \
	"$(INTDIR)\.\SemanticGraph\Interface.obj" \
	"$(INTDIR)\.\SemanticGraph\Attribute.obj" \
	"$(INTDIR)\.\SemanticGraph\Native.obj" \
	"$(INTDIR)\.\SemanticGraph\String.obj" \
	"$(INTDIR)\.\SemanticGraph\Translation.obj" \
	"$(INTDIR)\.\SemanticGraph\Fundamental.obj" \
	"$(INTDIR)\.\SemanticGraph\TypeId.obj" \
	"$(INTDIR)\.\SemanticGraph\Array.obj" \
	"$(INTDIR)\.\SemanticGraph\Exception.obj" \
	"$(INTDIR)\.\SemanticGraph\Literals.obj" \
	"$(INTDIR)\.\SemanticGraph\Sequence.obj" \
	"$(INTDIR)\.\SemanticGraph\IntExpression.obj" \
	"$(INTDIR)\.\SemanticGraph\ValueTypeMember.obj" \
	"$(INTDIR)\.\Traversal\Module.obj" \
	"$(INTDIR)\.\Traversal\Struct.obj" \
	"$(INTDIR)\.\Traversal\Operation.obj" \
	"$(INTDIR)\.\Traversal\Elements.obj" \
	"$(INTDIR)\.\Traversal\Union.obj" \
	"$(INTDIR)\.\Traversal\Member.obj" \
	"$(INTDIR)\.\Traversal\ValueType.obj" \
	"$(INTDIR)\.\Traversal\Enum.obj" \
	"$(INTDIR)\.\Traversal\Interface.obj" \
	"$(INTDIR)\.\Traversal\Attribute.obj" \
	"$(INTDIR)\.\Traversal\Native.obj" \
	"$(INTDIR)\.\Traversal\String.obj" \
	"$(INTDIR)\.\Traversal\Translation.obj" \
	"$(INTDIR)\.\Traversal\Fundamental.obj" \
	"$(INTDIR)\.\Traversal\TypeId.obj" \
	"$(INTDIR)\.\Traversal\Array.obj" \
	"$(INTDIR)\.\Traversal\Exception.obj" \
	"$(INTDIR)\.\Traversal\Sequence.obj" \
	"$(INTDIR)\.\SemanticAction\Operation.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Const.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Typedef.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Module.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Struct.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\NumericExpression.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Operation.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Elements.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Factory.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Union.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Member.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\ValueType.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Enum.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Interface.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\ValueTypeFactory.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Attribute.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Native.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\TypeId.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Exception.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Include.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\ValueTypeMember.obj"

"$(OUTDIR)\IDL2sd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\IDL2sd.lib.manifest" mt.exe -manifest "$(OUTDIR)\IDL2sd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..
INTDIR=Static_Release\IDL2\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\IDL2s.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"$(BOOST_ROOT)\include\$(BOOST_VERSION)" -I"$(BOOST_ROOT)\." -I"..\..\.." -I"..\..\..\CCF" -I"..\..\..\..\..\contrib\utility" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -f "Makefile.IDL2.dep" "Parser.cpp" "LexicalAnalyzer.cpp" "Token.cpp" ".\SemanticGraph\Name.cpp" ".\SemanticGraph\Module.cpp" ".\SemanticGraph\Struct.cpp" ".\SemanticGraph\Operation.cpp" ".\SemanticGraph\Elements.cpp" ".\SemanticGraph\Union.cpp" ".\SemanticGraph\Member.cpp" ".\SemanticGraph\ValueType.cpp" ".\SemanticGraph\Enum.cpp" ".\SemanticGraph\Interface.cpp" ".\SemanticGraph\Attribute.cpp" ".\SemanticGraph\Native.cpp" ".\SemanticGraph\String.cpp" ".\SemanticGraph\Translation.cpp" ".\SemanticGraph\Fundamental.cpp" ".\SemanticGraph\TypeId.cpp" ".\SemanticGraph\Array.cpp" ".\SemanticGraph\Exception.cpp" ".\SemanticGraph\Literals.cpp" ".\SemanticGraph\Sequence.cpp" ".\SemanticGraph\IntExpression.cpp" ".\SemanticGraph\ValueTypeMember.cpp" ".\Traversal\Module.cpp" ".\Traversal\Struct.cpp" ".\Traversal\Operation.cpp" ".\Traversal\Elements.cpp" ".\Traversal\Union.cpp" ".\Traversal\Member.cpp" ".\Traversal\ValueType.cpp" ".\Traversal\Enum.cpp" ".\Traversal\Interface.cpp" ".\Traversal\Attribute.cpp" ".\Traversal\Native.cpp" ".\Traversal\String.cpp" ".\Traversal\Translation.cpp" ".\Traversal\Fundamental.cpp" ".\Traversal\TypeId.cpp" ".\Traversal\Array.cpp" ".\Traversal\Exception.cpp" ".\Traversal\Sequence.cpp" ".\SemanticAction\Operation.cpp" ".\SemanticAction\Impl\Const.cpp" ".\SemanticAction\Impl\Typedef.cpp" ".\SemanticAction\Impl\Module.cpp" ".\SemanticAction\Impl\Struct.cpp" ".\SemanticAction\Impl\NumericExpression.cpp" ".\SemanticAction\Impl\Operation.cpp" ".\SemanticAction\Impl\Elements.cpp" ".\SemanticAction\Impl\Factory.cpp" ".\SemanticAction\Impl\Union.cpp" ".\SemanticAction\Impl\Member.cpp" ".\SemanticAction\Impl\ValueType.cpp" ".\SemanticAction\Impl\Enum.cpp" ".\SemanticAction\Impl\Interface.cpp" ".\SemanticAction\Impl\ValueTypeFactory.cpp" ".\SemanticAction\Impl\Attribute.cpp" ".\SemanticAction\Impl\Native.cpp" ".\SemanticAction\Impl\TypeId.cpp" ".\SemanticAction\Impl\Exception.cpp" ".\SemanticAction\Impl\Include.cpp" ".\SemanticAction\Impl\ValueTypeMember.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\IDL2s.lib"
	-@del /f/q "$(OUTDIR)\IDL2s.exp"
	-@del /f/q "$(OUTDIR)\IDL2s.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\IDL2\$(NULL)" mkdir "Static_Release\IDL2"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4290 /wd4800 /wd4250 /wd4355 /wd4996 /I "$(BOOST_ROOT)\include\$(BOOST_VERSION)" /I "$(BOOST_ROOT)\." /I "..\..\.." /I "..\..\..\CCF" /I "..\..\..\..\..\contrib\utility" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\IDL2s.lib"
LINK32_OBJS= \
	"$(INTDIR)\Parser.obj" \
	"$(INTDIR)\LexicalAnalyzer.obj" \
	"$(INTDIR)\Token.obj" \
	"$(INTDIR)\.\SemanticGraph\Name.obj" \
	"$(INTDIR)\.\SemanticGraph\Module.obj" \
	"$(INTDIR)\.\SemanticGraph\Struct.obj" \
	"$(INTDIR)\.\SemanticGraph\Operation.obj" \
	"$(INTDIR)\.\SemanticGraph\Elements.obj" \
	"$(INTDIR)\.\SemanticGraph\Union.obj" \
	"$(INTDIR)\.\SemanticGraph\Member.obj" \
	"$(INTDIR)\.\SemanticGraph\ValueType.obj" \
	"$(INTDIR)\.\SemanticGraph\Enum.obj" \
	"$(INTDIR)\.\SemanticGraph\Interface.obj" \
	"$(INTDIR)\.\SemanticGraph\Attribute.obj" \
	"$(INTDIR)\.\SemanticGraph\Native.obj" \
	"$(INTDIR)\.\SemanticGraph\String.obj" \
	"$(INTDIR)\.\SemanticGraph\Translation.obj" \
	"$(INTDIR)\.\SemanticGraph\Fundamental.obj" \
	"$(INTDIR)\.\SemanticGraph\TypeId.obj" \
	"$(INTDIR)\.\SemanticGraph\Array.obj" \
	"$(INTDIR)\.\SemanticGraph\Exception.obj" \
	"$(INTDIR)\.\SemanticGraph\Literals.obj" \
	"$(INTDIR)\.\SemanticGraph\Sequence.obj" \
	"$(INTDIR)\.\SemanticGraph\IntExpression.obj" \
	"$(INTDIR)\.\SemanticGraph\ValueTypeMember.obj" \
	"$(INTDIR)\.\Traversal\Module.obj" \
	"$(INTDIR)\.\Traversal\Struct.obj" \
	"$(INTDIR)\.\Traversal\Operation.obj" \
	"$(INTDIR)\.\Traversal\Elements.obj" \
	"$(INTDIR)\.\Traversal\Union.obj" \
	"$(INTDIR)\.\Traversal\Member.obj" \
	"$(INTDIR)\.\Traversal\ValueType.obj" \
	"$(INTDIR)\.\Traversal\Enum.obj" \
	"$(INTDIR)\.\Traversal\Interface.obj" \
	"$(INTDIR)\.\Traversal\Attribute.obj" \
	"$(INTDIR)\.\Traversal\Native.obj" \
	"$(INTDIR)\.\Traversal\String.obj" \
	"$(INTDIR)\.\Traversal\Translation.obj" \
	"$(INTDIR)\.\Traversal\Fundamental.obj" \
	"$(INTDIR)\.\Traversal\TypeId.obj" \
	"$(INTDIR)\.\Traversal\Array.obj" \
	"$(INTDIR)\.\Traversal\Exception.obj" \
	"$(INTDIR)\.\Traversal\Sequence.obj" \
	"$(INTDIR)\.\SemanticAction\Operation.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Const.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Typedef.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Module.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Struct.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\NumericExpression.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Operation.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Elements.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Factory.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Union.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Member.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\ValueType.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Enum.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Interface.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\ValueTypeFactory.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Attribute.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Native.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\TypeId.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Exception.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\Include.obj" \
	"$(INTDIR)\.\SemanticAction\Impl\ValueTypeMember.obj"

"$(OUTDIR)\IDL2s.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\IDL2s.lib.manifest" mt.exe -manifest "$(OUTDIR)\IDL2s.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.IDL2.dep")
!INCLUDE "Makefile.IDL2.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Parser.cpp"

"$(INTDIR)\Parser.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Parser.obj" $(SOURCE)

SOURCE="LexicalAnalyzer.cpp"

"$(INTDIR)\LexicalAnalyzer.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\LexicalAnalyzer.obj" $(SOURCE)

SOURCE="Token.cpp"

"$(INTDIR)\Token.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Token.obj" $(SOURCE)

SOURCE=".\SemanticGraph\Name.cpp"

"$(INTDIR)\.\SemanticGraph\Name.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\Name.obj" $(SOURCE)

SOURCE=".\SemanticGraph\Module.cpp"

"$(INTDIR)\.\SemanticGraph\Module.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\Module.obj" $(SOURCE)

SOURCE=".\SemanticGraph\Struct.cpp"

"$(INTDIR)\.\SemanticGraph\Struct.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\Struct.obj" $(SOURCE)

SOURCE=".\SemanticGraph\Operation.cpp"

"$(INTDIR)\.\SemanticGraph\Operation.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\Operation.obj" $(SOURCE)

SOURCE=".\SemanticGraph\Elements.cpp"

"$(INTDIR)\.\SemanticGraph\Elements.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\Elements.obj" $(SOURCE)

SOURCE=".\SemanticGraph\Union.cpp"

"$(INTDIR)\.\SemanticGraph\Union.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\Union.obj" $(SOURCE)

SOURCE=".\SemanticGraph\Member.cpp"

"$(INTDIR)\.\SemanticGraph\Member.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\Member.obj" $(SOURCE)

SOURCE=".\SemanticGraph\ValueType.cpp"

"$(INTDIR)\.\SemanticGraph\ValueType.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\ValueType.obj" $(SOURCE)

SOURCE=".\SemanticGraph\Enum.cpp"

"$(INTDIR)\.\SemanticGraph\Enum.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\Enum.obj" $(SOURCE)

SOURCE=".\SemanticGraph\Interface.cpp"

"$(INTDIR)\.\SemanticGraph\Interface.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\Interface.obj" $(SOURCE)

SOURCE=".\SemanticGraph\Attribute.cpp"

"$(INTDIR)\.\SemanticGraph\Attribute.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\Attribute.obj" $(SOURCE)

SOURCE=".\SemanticGraph\Native.cpp"

"$(INTDIR)\.\SemanticGraph\Native.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\Native.obj" $(SOURCE)

SOURCE=".\SemanticGraph\String.cpp"

"$(INTDIR)\.\SemanticGraph\String.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\String.obj" $(SOURCE)

SOURCE=".\SemanticGraph\Translation.cpp"

"$(INTDIR)\.\SemanticGraph\Translation.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\Translation.obj" $(SOURCE)

SOURCE=".\SemanticGraph\Fundamental.cpp"

"$(INTDIR)\.\SemanticGraph\Fundamental.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\Fundamental.obj" $(SOURCE)

SOURCE=".\SemanticGraph\TypeId.cpp"

"$(INTDIR)\.\SemanticGraph\TypeId.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\TypeId.obj" $(SOURCE)

SOURCE=".\SemanticGraph\Array.cpp"

"$(INTDIR)\.\SemanticGraph\Array.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\Array.obj" $(SOURCE)

SOURCE=".\SemanticGraph\Exception.cpp"

"$(INTDIR)\.\SemanticGraph\Exception.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\Exception.obj" $(SOURCE)

SOURCE=".\SemanticGraph\Literals.cpp"

"$(INTDIR)\.\SemanticGraph\Literals.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\Literals.obj" $(SOURCE)

SOURCE=".\SemanticGraph\Sequence.cpp"

"$(INTDIR)\.\SemanticGraph\Sequence.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\Sequence.obj" $(SOURCE)

SOURCE=".\SemanticGraph\IntExpression.cpp"

"$(INTDIR)\.\SemanticGraph\IntExpression.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\IntExpression.obj" $(SOURCE)

SOURCE=".\SemanticGraph\ValueTypeMember.cpp"

"$(INTDIR)\.\SemanticGraph\ValueTypeMember.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticGraph\$(NULL)" mkdir "$(INTDIR)\.\SemanticGraph\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticGraph\ValueTypeMember.obj" $(SOURCE)

SOURCE=".\Traversal\Module.cpp"

"$(INTDIR)\.\Traversal\Module.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\Traversal\$(NULL)" mkdir "$(INTDIR)\.\Traversal\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\Traversal\Module.obj" $(SOURCE)

SOURCE=".\Traversal\Struct.cpp"

"$(INTDIR)\.\Traversal\Struct.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\Traversal\$(NULL)" mkdir "$(INTDIR)\.\Traversal\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\Traversal\Struct.obj" $(SOURCE)

SOURCE=".\Traversal\Operation.cpp"

"$(INTDIR)\.\Traversal\Operation.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\Traversal\$(NULL)" mkdir "$(INTDIR)\.\Traversal\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\Traversal\Operation.obj" $(SOURCE)

SOURCE=".\Traversal\Elements.cpp"

"$(INTDIR)\.\Traversal\Elements.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\Traversal\$(NULL)" mkdir "$(INTDIR)\.\Traversal\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\Traversal\Elements.obj" $(SOURCE)

SOURCE=".\Traversal\Union.cpp"

"$(INTDIR)\.\Traversal\Union.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\Traversal\$(NULL)" mkdir "$(INTDIR)\.\Traversal\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\Traversal\Union.obj" $(SOURCE)

SOURCE=".\Traversal\Member.cpp"

"$(INTDIR)\.\Traversal\Member.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\Traversal\$(NULL)" mkdir "$(INTDIR)\.\Traversal\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\Traversal\Member.obj" $(SOURCE)

SOURCE=".\Traversal\ValueType.cpp"

"$(INTDIR)\.\Traversal\ValueType.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\Traversal\$(NULL)" mkdir "$(INTDIR)\.\Traversal\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\Traversal\ValueType.obj" $(SOURCE)

SOURCE=".\Traversal\Enum.cpp"

"$(INTDIR)\.\Traversal\Enum.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\Traversal\$(NULL)" mkdir "$(INTDIR)\.\Traversal\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\Traversal\Enum.obj" $(SOURCE)

SOURCE=".\Traversal\Interface.cpp"

"$(INTDIR)\.\Traversal\Interface.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\Traversal\$(NULL)" mkdir "$(INTDIR)\.\Traversal\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\Traversal\Interface.obj" $(SOURCE)

SOURCE=".\Traversal\Attribute.cpp"

"$(INTDIR)\.\Traversal\Attribute.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\Traversal\$(NULL)" mkdir "$(INTDIR)\.\Traversal\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\Traversal\Attribute.obj" $(SOURCE)

SOURCE=".\Traversal\Native.cpp"

"$(INTDIR)\.\Traversal\Native.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\Traversal\$(NULL)" mkdir "$(INTDIR)\.\Traversal\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\Traversal\Native.obj" $(SOURCE)

SOURCE=".\Traversal\String.cpp"

"$(INTDIR)\.\Traversal\String.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\Traversal\$(NULL)" mkdir "$(INTDIR)\.\Traversal\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\Traversal\String.obj" $(SOURCE)

SOURCE=".\Traversal\Translation.cpp"

"$(INTDIR)\.\Traversal\Translation.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\Traversal\$(NULL)" mkdir "$(INTDIR)\.\Traversal\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\Traversal\Translation.obj" $(SOURCE)

SOURCE=".\Traversal\Fundamental.cpp"

"$(INTDIR)\.\Traversal\Fundamental.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\Traversal\$(NULL)" mkdir "$(INTDIR)\.\Traversal\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\Traversal\Fundamental.obj" $(SOURCE)

SOURCE=".\Traversal\TypeId.cpp"

"$(INTDIR)\.\Traversal\TypeId.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\Traversal\$(NULL)" mkdir "$(INTDIR)\.\Traversal\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\Traversal\TypeId.obj" $(SOURCE)

SOURCE=".\Traversal\Array.cpp"

"$(INTDIR)\.\Traversal\Array.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\Traversal\$(NULL)" mkdir "$(INTDIR)\.\Traversal\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\Traversal\Array.obj" $(SOURCE)

SOURCE=".\Traversal\Exception.cpp"

"$(INTDIR)\.\Traversal\Exception.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\Traversal\$(NULL)" mkdir "$(INTDIR)\.\Traversal\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\Traversal\Exception.obj" $(SOURCE)

SOURCE=".\Traversal\Sequence.cpp"

"$(INTDIR)\.\Traversal\Sequence.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\Traversal\$(NULL)" mkdir "$(INTDIR)\.\Traversal\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\Traversal\Sequence.obj" $(SOURCE)

SOURCE=".\SemanticAction\Operation.cpp"

"$(INTDIR)\.\SemanticAction\Operation.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Operation.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Const.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Const.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Const.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Typedef.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Typedef.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Typedef.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Module.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Module.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Module.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Struct.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Struct.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Struct.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\NumericExpression.cpp"

"$(INTDIR)\.\SemanticAction\Impl\NumericExpression.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\NumericExpression.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Operation.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Operation.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Operation.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Elements.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Elements.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Elements.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Factory.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Factory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Factory.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Union.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Union.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Union.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Member.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Member.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Member.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\ValueType.cpp"

"$(INTDIR)\.\SemanticAction\Impl\ValueType.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\ValueType.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Enum.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Enum.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Enum.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Interface.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Interface.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Interface.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\ValueTypeFactory.cpp"

"$(INTDIR)\.\SemanticAction\Impl\ValueTypeFactory.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\ValueTypeFactory.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Attribute.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Attribute.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Attribute.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Native.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Native.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Native.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\TypeId.cpp"

"$(INTDIR)\.\SemanticAction\Impl\TypeId.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\TypeId.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Exception.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Exception.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Exception.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\Include.cpp"

"$(INTDIR)\.\SemanticAction\Impl\Include.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\Include.obj" $(SOURCE)

SOURCE=".\SemanticAction\Impl\ValueTypeMember.cpp"

"$(INTDIR)\.\SemanticAction\Impl\ValueTypeMember.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\.\SemanticAction\Impl\$(NULL)" mkdir "$(INTDIR)\.\SemanticAction\Impl\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\.\SemanticAction\Impl\ValueTypeMember.obj" $(SOURCE)


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.IDL2.dep")
	@echo Using "Makefile.IDL2.dep"
!ELSE
	@echo Warning: cannot find "Makefile.IDL2.dep"
!ENDIF
!ENDIF

