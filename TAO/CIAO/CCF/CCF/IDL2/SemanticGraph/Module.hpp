// file      : CCF/IDL2/SemanticGraph/Module.hpp
// author    : Boris Kolpackov <boris@dre.vanderbilt.edu>
// cvs-id    : $Id: Module.hpp 14 2007-02-01 15:49:12Z mitza $

#ifndef CCF_IDL2_SEMANTIC_GRAPH_MODULE_HPP
#define CCF_IDL2_SEMANTIC_GRAPH_MODULE_HPP

#include "CCF/IDL2/SemanticGraph/Elements.hpp"

namespace CCF
{
  namespace IDL2
  {
    namespace SemanticGraph
    {
      class Module : public virtual Scope
      {
      public:
        static Introspection::TypeInfo const&
        static_type_info ();

      protected:
        friend class Graph<Node, Edge>;

        Module (Path const& path, unsigned long line)
            : Node (path, line)
        {
          type_info (static_type_info ());
        }
      };
    }
  }
}

#endif  // CCF_IDL2_SEMANTIC_GRAPH_MODULE_HPP
