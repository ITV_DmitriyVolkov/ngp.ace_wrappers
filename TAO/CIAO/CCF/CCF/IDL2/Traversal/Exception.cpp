// file      : CCF/IDL2/Traversal/Exception.cpp
// author    : Boris Kolpackov <boris@dre.vanderbilt.edu>
// cvs-id    : $Id: Exception.cpp 14 2007-02-01 15:49:12Z mitza $

#include "CCF/IDL2/Traversal/Exception.hpp"

namespace CCF
{
  namespace IDL2
  {
    namespace Traversal
    {
      void Exception::
      traverse (Type& s)
      {
        pre (s);
        name (s);
        names (s);
        post (s);
      }

      void Exception::
      pre (Type&)
      {
      }

      void Exception::
      name (Type&)
      {
      }

      void Exception::
      post (Type&)
      {
      }
    }
  }
}
