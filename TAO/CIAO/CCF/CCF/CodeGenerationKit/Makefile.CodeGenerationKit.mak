# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.CodeGenerationKit.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..
INTDIR=Debug\CodeGenerationKit\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CodeGenerationKitd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"$(BOOST_ROOT)\include\$(BOOST_VERSION)" -I"$(BOOST_ROOT)\." -I"..\..\.." -I"..\..\..\CCF" -I"..\..\..\..\..\contrib\utility" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -f "Makefile.CodeGenerationKit.dep" "CommandLineParser.cpp" "CommandLine.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CodeGenerationKitd.lib"
	-@del /f/q "$(OUTDIR)\CodeGenerationKitd.exp"
	-@del /f/q "$(OUTDIR)\CodeGenerationKitd.ilk"
	-@del /f/q "..\CodeGenerationKitd.pdb"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\CodeGenerationKit\$(NULL)" mkdir "Debug\CodeGenerationKit"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4290 /wd4800 /wd4250 /wd4355 /wd4996 /Fd"..\CodeGenerationKitd.pdb" /I "$(BOOST_ROOT)\include\$(BOOST_VERSION)" /I "$(BOOST_ROOT)\." /I "..\..\.." /I "..\..\..\CCF" /I "..\..\..\..\..\contrib\utility" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\CodeGenerationKitd.lib"
LINK32_OBJS= \
	"$(INTDIR)\CommandLineParser.obj" \
	"$(INTDIR)\CommandLine.obj"

"$(OUTDIR)\CodeGenerationKitd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CodeGenerationKitd.lib.manifest" mt.exe -manifest "$(OUTDIR)\CodeGenerationKitd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..
INTDIR=Release\CodeGenerationKit\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CodeGenerationKit.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"$(BOOST_ROOT)\include\$(BOOST_VERSION)" -I"$(BOOST_ROOT)\." -I"..\..\.." -I"..\..\..\CCF" -I"..\..\..\..\..\contrib\utility" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -f "Makefile.CodeGenerationKit.dep" "CommandLineParser.cpp" "CommandLine.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CodeGenerationKit.lib"
	-@del /f/q "$(OUTDIR)\CodeGenerationKit.exp"
	-@del /f/q "$(OUTDIR)\CodeGenerationKit.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\CodeGenerationKit\$(NULL)" mkdir "Release\CodeGenerationKit"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4290 /wd4800 /wd4250 /wd4355 /wd4996 /I "$(BOOST_ROOT)\include\$(BOOST_VERSION)" /I "$(BOOST_ROOT)\." /I "..\..\.." /I "..\..\..\CCF" /I "..\..\..\..\..\contrib\utility" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\CodeGenerationKit.lib"
LINK32_OBJS= \
	"$(INTDIR)\CommandLineParser.obj" \
	"$(INTDIR)\CommandLine.obj"

"$(OUTDIR)\CodeGenerationKit.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CodeGenerationKit.lib.manifest" mt.exe -manifest "$(OUTDIR)\CodeGenerationKit.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..
INTDIR=Static_Debug\CodeGenerationKit\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CodeGenerationKitsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"$(BOOST_ROOT)\include\$(BOOST_VERSION)" -I"$(BOOST_ROOT)\." -I"..\..\.." -I"..\..\..\CCF" -I"..\..\..\..\..\contrib\utility" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -f "Makefile.CodeGenerationKit.dep" "CommandLineParser.cpp" "CommandLine.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CodeGenerationKitsd.lib"
	-@del /f/q "$(OUTDIR)\CodeGenerationKitsd.exp"
	-@del /f/q "$(OUTDIR)\CodeGenerationKitsd.ilk"
	-@del /f/q "..\CodeGenerationKitsd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\CodeGenerationKit\$(NULL)" mkdir "Static_Debug\CodeGenerationKit"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4290 /wd4800 /wd4250 /wd4355 /wd4996 /Fd"..\CodeGenerationKitsd.pdb" /I "$(BOOST_ROOT)\include\$(BOOST_VERSION)" /I "$(BOOST_ROOT)\." /I "..\..\.." /I "..\..\..\CCF" /I "..\..\..\..\..\contrib\utility" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\CodeGenerationKitsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\CommandLineParser.obj" \
	"$(INTDIR)\CommandLine.obj"

"$(OUTDIR)\CodeGenerationKitsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CodeGenerationKitsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\CodeGenerationKitsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..
INTDIR=Static_Release\CodeGenerationKit\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CodeGenerationKits.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"$(BOOST_ROOT)\include\$(BOOST_VERSION)" -I"$(BOOST_ROOT)\." -I"..\..\.." -I"..\..\..\CCF" -I"..\..\..\..\..\contrib\utility" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -f "Makefile.CodeGenerationKit.dep" "CommandLineParser.cpp" "CommandLine.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CodeGenerationKits.lib"
	-@del /f/q "$(OUTDIR)\CodeGenerationKits.exp"
	-@del /f/q "$(OUTDIR)\CodeGenerationKits.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\CodeGenerationKit\$(NULL)" mkdir "Static_Release\CodeGenerationKit"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4290 /wd4800 /wd4250 /wd4355 /wd4996 /I "$(BOOST_ROOT)\include\$(BOOST_VERSION)" /I "$(BOOST_ROOT)\." /I "..\..\.." /I "..\..\..\CCF" /I "..\..\..\..\..\contrib\utility" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\CodeGenerationKits.lib"
LINK32_OBJS= \
	"$(INTDIR)\CommandLineParser.obj" \
	"$(INTDIR)\CommandLine.obj"

"$(OUTDIR)\CodeGenerationKits.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CodeGenerationKits.lib.manifest" mt.exe -manifest "$(OUTDIR)\CodeGenerationKits.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CodeGenerationKit.dep")
!INCLUDE "Makefile.CodeGenerationKit.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="CommandLineParser.cpp"

"$(INTDIR)\CommandLineParser.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CommandLineParser.obj" $(SOURCE)

SOURCE="CommandLine.cpp"

"$(INTDIR)\CommandLine.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CommandLine.obj" $(SOURCE)


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CodeGenerationKit.dep")
	@echo Using "Makefile.CodeGenerationKit.dep"
!ELSE
	@echo Warning: cannot find "Makefile.CodeGenerationKit.dep"
!ENDIF
!ENDIF

