# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.Administrator_Client_IDL2_Compile.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "idl2\Runnable_IDL2.idl" "idl2\Publication_IDL2.idl" "idl2\Message_IDL2.idl" "idl2\History_IDL2.idl" "idl2\Administrator_IDL2.idl" "idl2\Messenger_IDL2.idl"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INTDIR=Debug\Administrator_Client_IDL2_Compile\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY)

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	-@rem
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\d.lib"
	-@del /f/q "$(OUTDIR)\d.exp"
	-@del /f/q "$(OUTDIR)\d.ilk"
	-@del /f/q "idl2\Runnable_IDL2.idl"
	-@del /f/q "idl2\Publication_IDL2.idl"
	-@del /f/q "idl2\Message_IDL2.idl"
	-@del /f/q "idl2\History_IDL2.idl"
	-@del /f/q "idl2\Administrator_IDL2.idl"
	-@del /f/q "idl2\Messenger_IDL2.idl"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\Administrator_Client_IDL2_Compile\$(NULL)" mkdir "Debug\Administrator_Client_IDL2_Compile"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /Fd"$(INTDIR)/" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe


!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=.
INTDIR=Release\Administrator_Client_IDL2_Compile\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY)

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	-@rem
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\.lib"
	-@del /f/q "$(OUTDIR)\.exp"
	-@del /f/q "$(OUTDIR)\.ilk"
	-@del /f/q "idl2\Runnable_IDL2.idl"
	-@del /f/q "idl2\Publication_IDL2.idl"
	-@del /f/q "idl2\Message_IDL2.idl"
	-@del /f/q "idl2\History_IDL2.idl"
	-@del /f/q "idl2\Administrator_IDL2.idl"
	-@del /f/q "idl2\Messenger_IDL2.idl"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\Administrator_Client_IDL2_Compile\$(NULL)" mkdir "Release\Administrator_Client_IDL2_Compile"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe


!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=.
INTDIR=Static_Debug\Administrator_Client_IDL2_Compile\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY)

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	-@rem
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q ".\sd.pdb"
	-@del /f/q "idl2\Runnable_IDL2.idl"
	-@del /f/q "idl2\Publication_IDL2.idl"
	-@del /f/q "idl2\Message_IDL2.idl"
	-@del /f/q "idl2\History_IDL2.idl"
	-@del /f/q "idl2\Administrator_IDL2.idl"
	-@del /f/q "idl2\Messenger_IDL2.idl"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\Administrator_Client_IDL2_Compile\$(NULL)" mkdir "Static_Debug\Administrator_Client_IDL2_Compile"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /Fd".\sd.pdb" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"



!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=.
INTDIR=Static_Release\Administrator_Client_IDL2_Compile\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY)

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	-@rem
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "idl2\Runnable_IDL2.idl"
	-@del /f/q "idl2\Publication_IDL2.idl"
	-@del /f/q "idl2\Message_IDL2.idl"
	-@del /f/q "idl2\History_IDL2.idl"
	-@del /f/q "idl2\Administrator_IDL2.idl"
	-@del /f/q "idl2\Messenger_IDL2.idl"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\Administrator_Client_IDL2_Compile\$(NULL)" mkdir "Static_Release\Administrator_Client_IDL2_Compile"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"



!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Administrator_Client_IDL2_Compile.dep")
!INCLUDE "Makefile.Administrator_Client_IDL2_Compile.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
!IF  "$(CFG)" == "Win64 Debug"
SOURCE="Runnable.idl"

InputPath=Runnable.idl

"idl2\Runnable_IDL2.idl" : $(SOURCE)  "..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Debug-idl3toidl2_files-Runnable_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl3_to_idl2 -I..\../ciao -I..\..\.. -I..\..\../orbsvcs -I. -o idl2 "$(InputPath)"
<<

SOURCE="Publication.idl"

InputPath=Publication.idl

"idl2\Publication_IDL2.idl" : $(SOURCE)  "..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Debug-idl3toidl2_files-Publication_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl3_to_idl2 -I..\../ciao -I..\..\.. -I..\..\../orbsvcs -I. -o idl2 "$(InputPath)"
<<

SOURCE="Message.idl"

InputPath=Message.idl

"idl2\Message_IDL2.idl" : $(SOURCE)  "..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Debug-idl3toidl2_files-Message_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl3_to_idl2 -I..\../ciao -I..\..\.. -I..\..\../orbsvcs -I. -o idl2 "$(InputPath)"
<<

SOURCE="History.idl"

InputPath=History.idl

"idl2\History_IDL2.idl" : $(SOURCE)  "..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Debug-idl3toidl2_files-History_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl3_to_idl2 -I..\../ciao -I..\..\.. -I..\..\../orbsvcs -I. -o idl2 "$(InputPath)"
<<

SOURCE="Administrator.idl"

InputPath=Administrator.idl

"idl2\Administrator_IDL2.idl" : $(SOURCE)  "..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Debug-idl3toidl2_files-Administrator_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl3_to_idl2 -I..\../ciao -I..\..\.. -I..\..\../orbsvcs -I. -o idl2 "$(InputPath)"
<<

SOURCE="Messenger.idl"

InputPath=Messenger.idl

"idl2\Messenger_IDL2.idl" : $(SOURCE)  "..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Debug-idl3toidl2_files-Messenger_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl3_to_idl2 -I..\../ciao -I..\..\.. -I..\..\../orbsvcs -I. -o idl2 "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="Runnable.idl"

InputPath=Runnable.idl

"idl2\Runnable_IDL2.idl" : $(SOURCE)  "..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Release-idl3toidl2_files-Runnable_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl3_to_idl2 -I..\../ciao -I..\..\.. -I..\..\../orbsvcs -I. -o idl2 "$(InputPath)"
<<

SOURCE="Publication.idl"

InputPath=Publication.idl

"idl2\Publication_IDL2.idl" : $(SOURCE)  "..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Release-idl3toidl2_files-Publication_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl3_to_idl2 -I..\../ciao -I..\..\.. -I..\..\../orbsvcs -I. -o idl2 "$(InputPath)"
<<

SOURCE="Message.idl"

InputPath=Message.idl

"idl2\Message_IDL2.idl" : $(SOURCE)  "..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Release-idl3toidl2_files-Message_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl3_to_idl2 -I..\../ciao -I..\..\.. -I..\..\../orbsvcs -I. -o idl2 "$(InputPath)"
<<

SOURCE="History.idl"

InputPath=History.idl

"idl2\History_IDL2.idl" : $(SOURCE)  "..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Release-idl3toidl2_files-History_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl3_to_idl2 -I..\../ciao -I..\..\.. -I..\..\../orbsvcs -I. -o idl2 "$(InputPath)"
<<

SOURCE="Administrator.idl"

InputPath=Administrator.idl

"idl2\Administrator_IDL2.idl" : $(SOURCE)  "..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Release-idl3toidl2_files-Administrator_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl3_to_idl2 -I..\../ciao -I..\..\.. -I..\..\../orbsvcs -I. -o idl2 "$(InputPath)"
<<

SOURCE="Messenger.idl"

InputPath=Messenger.idl

"idl2\Messenger_IDL2.idl" : $(SOURCE)  "..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Release-idl3toidl2_files-Messenger_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl3_to_idl2 -I..\../ciao -I..\..\.. -I..\..\../orbsvcs -I. -o idl2 "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="Runnable.idl"

InputPath=Runnable.idl

"idl2\Runnable_IDL2.idl" : $(SOURCE)  "..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Static_Debug-idl3toidl2_files-Runnable_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl3_to_idl2 -I..\../ciao -I..\..\.. -I..\..\../orbsvcs -I. -o idl2 "$(InputPath)"
<<

SOURCE="Publication.idl"

InputPath=Publication.idl

"idl2\Publication_IDL2.idl" : $(SOURCE)  "..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Static_Debug-idl3toidl2_files-Publication_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl3_to_idl2 -I..\../ciao -I..\..\.. -I..\..\../orbsvcs -I. -o idl2 "$(InputPath)"
<<

SOURCE="Message.idl"

InputPath=Message.idl

"idl2\Message_IDL2.idl" : $(SOURCE)  "..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Static_Debug-idl3toidl2_files-Message_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl3_to_idl2 -I..\../ciao -I..\..\.. -I..\..\../orbsvcs -I. -o idl2 "$(InputPath)"
<<

SOURCE="History.idl"

InputPath=History.idl

"idl2\History_IDL2.idl" : $(SOURCE)  "..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Static_Debug-idl3toidl2_files-History_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl3_to_idl2 -I..\../ciao -I..\..\.. -I..\..\../orbsvcs -I. -o idl2 "$(InputPath)"
<<

SOURCE="Administrator.idl"

InputPath=Administrator.idl

"idl2\Administrator_IDL2.idl" : $(SOURCE)  "..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Static_Debug-idl3toidl2_files-Administrator_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl3_to_idl2 -I..\../ciao -I..\..\.. -I..\..\../orbsvcs -I. -o idl2 "$(InputPath)"
<<

SOURCE="Messenger.idl"

InputPath=Messenger.idl

"idl2\Messenger_IDL2.idl" : $(SOURCE)  "..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Static_Debug-idl3toidl2_files-Messenger_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl3_to_idl2 -I..\../ciao -I..\..\.. -I..\..\../orbsvcs -I. -o idl2 "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="Runnable.idl"

InputPath=Runnable.idl

"idl2\Runnable_IDL2.idl" : $(SOURCE)  "..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Static_Release-idl3toidl2_files-Runnable_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl3_to_idl2 -I..\../ciao -I..\..\.. -I..\..\../orbsvcs -I. -o idl2 "$(InputPath)"
<<

SOURCE="Publication.idl"

InputPath=Publication.idl

"idl2\Publication_IDL2.idl" : $(SOURCE)  "..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Static_Release-idl3toidl2_files-Publication_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl3_to_idl2 -I..\../ciao -I..\..\.. -I..\..\../orbsvcs -I. -o idl2 "$(InputPath)"
<<

SOURCE="Message.idl"

InputPath=Message.idl

"idl2\Message_IDL2.idl" : $(SOURCE)  "..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Static_Release-idl3toidl2_files-Message_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl3_to_idl2 -I..\../ciao -I..\..\.. -I..\..\../orbsvcs -I. -o idl2 "$(InputPath)"
<<

SOURCE="History.idl"

InputPath=History.idl

"idl2\History_IDL2.idl" : $(SOURCE)  "..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Static_Release-idl3toidl2_files-History_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl3_to_idl2 -I..\../ciao -I..\..\.. -I..\..\../orbsvcs -I. -o idl2 "$(InputPath)"
<<

SOURCE="Administrator.idl"

InputPath=Administrator.idl

"idl2\Administrator_IDL2.idl" : $(SOURCE)  "..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Static_Release-idl3toidl2_files-Administrator_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl3_to_idl2 -I..\../ciao -I..\..\.. -I..\..\../orbsvcs -I. -o idl2 "$(InputPath)"
<<

SOURCE="Messenger.idl"

InputPath=Messenger.idl

"idl2\Messenger_IDL2.idl" : $(SOURCE)  "..\..\..\..\bin\tao_idl3_to_idl2.exe"
	<<tempfile-Win64-Static_Release-idl3toidl2_files-Messenger_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl3_to_idl2 -I..\../ciao -I..\..\.. -I..\..\../orbsvcs -I. -o idl2 "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Administrator_Client_IDL2_Compile.dep")
	@echo Using "Makefile.Administrator_Client_IDL2_Compile.dep"
!ENDIF
!ENDIF

