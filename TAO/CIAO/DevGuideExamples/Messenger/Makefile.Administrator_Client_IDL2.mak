# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.Administrator_Client_IDL2.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Console Application")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "idl2\Runnable_IDL2C.inl" "idl2\Runnable_IDL2C.h" "idl2\Runnable_IDL2S.h" "idl2\Runnable_IDL2C.cpp" "idl2\Publication_IDL2C.inl" "idl2\Publication_IDL2C.h" "idl2\Publication_IDL2S.h" "idl2\Publication_IDL2C.cpp" "idl2\Message_IDL2C.inl" "idl2\Message_IDL2C.h" "idl2\Message_IDL2S.h" "idl2\Message_IDL2C.cpp" "idl2\History_IDL2C.inl" "idl2\History_IDL2C.h" "idl2\History_IDL2S.h" "idl2\History_IDL2C.cpp" "idl2\Administrator_IDL2C.inl" "idl2\Administrator_IDL2C.h" "idl2\Administrator_IDL2S.h" "idl2\Administrator_IDL2C.cpp" "idl2\Messenger_IDL2C.inl" "idl2\Messenger_IDL2C.h" "idl2\Messenger_IDL2S.h" "idl2\Messenger_IDL2C.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INSTALLDIR=.
INTDIR=Debug\Administrator_Client_IDL2\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\Administrator_Client_IDL2.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -f "Makefile.Administrator_Client_IDL2.dep" "Administrator_Client_IDL2.cpp" "idl2\Administrator_IDL2C.cpp" "idl2\Messenger_IDL2C.cpp" "idl2\Publication_IDL2C.cpp" "idl2\Runnable_IDL2C.cpp" "idl2\History_IDL2C.cpp" "idl2\Message_IDL2C.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\Administrator_Client_IDL2.pdb"
	-@del /f/q "$(INSTALLDIR)\Administrator_Client_IDL2.exe"
	-@del /f/q "$(INSTALLDIR)\Administrator_Client_IDL2.ilk"
	-@del /f/q "idl2\Runnable_IDL2C.inl"
	-@del /f/q "idl2\Runnable_IDL2C.h"
	-@del /f/q "idl2\Runnable_IDL2S.h"
	-@del /f/q "idl2\Runnable_IDL2C.cpp"
	-@del /f/q "idl2\Publication_IDL2C.inl"
	-@del /f/q "idl2\Publication_IDL2C.h"
	-@del /f/q "idl2\Publication_IDL2S.h"
	-@del /f/q "idl2\Publication_IDL2C.cpp"
	-@del /f/q "idl2\Message_IDL2C.inl"
	-@del /f/q "idl2\Message_IDL2C.h"
	-@del /f/q "idl2\Message_IDL2S.h"
	-@del /f/q "idl2\Message_IDL2C.cpp"
	-@del /f/q "idl2\History_IDL2C.inl"
	-@del /f/q "idl2\History_IDL2C.h"
	-@del /f/q "idl2\History_IDL2S.h"
	-@del /f/q "idl2\History_IDL2C.cpp"
	-@del /f/q "idl2\Administrator_IDL2C.inl"
	-@del /f/q "idl2\Administrator_IDL2C.h"
	-@del /f/q "idl2\Administrator_IDL2S.h"
	-@del /f/q "idl2\Administrator_IDL2C.cpp"
	-@del /f/q "idl2\Messenger_IDL2C.inl"
	-@del /f/q "idl2\Messenger_IDL2C.h"
	-@del /f/q "idl2\Messenger_IDL2S.h"
	-@del /f/q "idl2\Messenger_IDL2C.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\Administrator_Client_IDL2\$(NULL)" mkdir "Debug\Administrator_Client_IDL2"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_IFR_Clientd.lib TAO_Valuetyped.lib TAO_CodecFactoryd.lib TAO_PId.lib CIAO_Clientd.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\Administrator_Client_IDL2.pdb" /machine:IA64 /out:"$(INSTALLDIR)\Administrator_Client_IDL2.exe"
LINK32_OBJS= \
	"$(INTDIR)\Administrator_Client_IDL2.obj" \
	"$(INTDIR)\idl2\Administrator_IDL2C.obj" \
	"$(INTDIR)\idl2\Messenger_IDL2C.obj" \
	"$(INTDIR)\idl2\Publication_IDL2C.obj" \
	"$(INTDIR)\idl2\Runnable_IDL2C.obj" \
	"$(INTDIR)\idl2\History_IDL2C.obj" \
	"$(INTDIR)\idl2\Message_IDL2C.obj"

"$(INSTALLDIR)\Administrator_Client_IDL2.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\Administrator_Client_IDL2.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\Administrator_Client_IDL2.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=Release
INSTALLDIR=Release
INTDIR=Release\Administrator_Client_IDL2\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\Administrator_Client_IDL2.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -f "Makefile.Administrator_Client_IDL2.dep" "Administrator_Client_IDL2.cpp" "idl2\Administrator_IDL2C.cpp" "idl2\Messenger_IDL2C.cpp" "idl2\Publication_IDL2C.cpp" "idl2\Runnable_IDL2C.cpp" "idl2\History_IDL2C.cpp" "idl2\Message_IDL2C.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\Administrator_Client_IDL2.exe"
	-@del /f/q "$(INSTALLDIR)\Administrator_Client_IDL2.ilk"
	-@del /f/q "idl2\Runnable_IDL2C.inl"
	-@del /f/q "idl2\Runnable_IDL2C.h"
	-@del /f/q "idl2\Runnable_IDL2S.h"
	-@del /f/q "idl2\Runnable_IDL2C.cpp"
	-@del /f/q "idl2\Publication_IDL2C.inl"
	-@del /f/q "idl2\Publication_IDL2C.h"
	-@del /f/q "idl2\Publication_IDL2S.h"
	-@del /f/q "idl2\Publication_IDL2C.cpp"
	-@del /f/q "idl2\Message_IDL2C.inl"
	-@del /f/q "idl2\Message_IDL2C.h"
	-@del /f/q "idl2\Message_IDL2S.h"
	-@del /f/q "idl2\Message_IDL2C.cpp"
	-@del /f/q "idl2\History_IDL2C.inl"
	-@del /f/q "idl2\History_IDL2C.h"
	-@del /f/q "idl2\History_IDL2S.h"
	-@del /f/q "idl2\History_IDL2C.cpp"
	-@del /f/q "idl2\Administrator_IDL2C.inl"
	-@del /f/q "idl2\Administrator_IDL2C.h"
	-@del /f/q "idl2\Administrator_IDL2S.h"
	-@del /f/q "idl2\Administrator_IDL2C.cpp"
	-@del /f/q "idl2\Messenger_IDL2C.inl"
	-@del /f/q "idl2\Messenger_IDL2C.h"
	-@del /f/q "idl2\Messenger_IDL2S.h"
	-@del /f/q "idl2\Messenger_IDL2C.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\Administrator_Client_IDL2\$(NULL)" mkdir "Release\Administrator_Client_IDL2"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_IFR_Client.lib TAO_Valuetype.lib TAO_CodecFactory.lib TAO_PI.lib CIAO_Client.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\Administrator_Client_IDL2.exe"
LINK32_OBJS= \
	"$(INTDIR)\Administrator_Client_IDL2.obj" \
	"$(INTDIR)\idl2\Administrator_IDL2C.obj" \
	"$(INTDIR)\idl2\Messenger_IDL2C.obj" \
	"$(INTDIR)\idl2\Publication_IDL2C.obj" \
	"$(INTDIR)\idl2\Runnable_IDL2C.obj" \
	"$(INTDIR)\idl2\History_IDL2C.obj" \
	"$(INTDIR)\idl2\Message_IDL2C.obj"

"$(INSTALLDIR)\Administrator_Client_IDL2.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\Administrator_Client_IDL2.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\Administrator_Client_IDL2.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=Static_Debug
INSTALLDIR=Static_Debug
INTDIR=Static_Debug\Administrator_Client_IDL2\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\Administrator_Client_IDL2.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Administrator_Client_IDL2.dep" "Administrator_Client_IDL2.cpp" "idl2\Administrator_IDL2C.cpp" "idl2\Messenger_IDL2C.cpp" "idl2\Publication_IDL2C.cpp" "idl2\Runnable_IDL2C.cpp" "idl2\History_IDL2C.cpp" "idl2\Message_IDL2C.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\Administrator_Client_IDL2.pdb"
	-@del /f/q "$(INSTALLDIR)\Administrator_Client_IDL2.exe"
	-@del /f/q "$(INSTALLDIR)\Administrator_Client_IDL2.ilk"
	-@del /f/q "idl2\Runnable_IDL2C.inl"
	-@del /f/q "idl2\Runnable_IDL2C.h"
	-@del /f/q "idl2\Runnable_IDL2S.h"
	-@del /f/q "idl2\Runnable_IDL2C.cpp"
	-@del /f/q "idl2\Publication_IDL2C.inl"
	-@del /f/q "idl2\Publication_IDL2C.h"
	-@del /f/q "idl2\Publication_IDL2S.h"
	-@del /f/q "idl2\Publication_IDL2C.cpp"
	-@del /f/q "idl2\Message_IDL2C.inl"
	-@del /f/q "idl2\Message_IDL2C.h"
	-@del /f/q "idl2\Message_IDL2S.h"
	-@del /f/q "idl2\Message_IDL2C.cpp"
	-@del /f/q "idl2\History_IDL2C.inl"
	-@del /f/q "idl2\History_IDL2C.h"
	-@del /f/q "idl2\History_IDL2S.h"
	-@del /f/q "idl2\History_IDL2C.cpp"
	-@del /f/q "idl2\Administrator_IDL2C.inl"
	-@del /f/q "idl2\Administrator_IDL2C.h"
	-@del /f/q "idl2\Administrator_IDL2S.h"
	-@del /f/q "idl2\Administrator_IDL2C.cpp"
	-@del /f/q "idl2\Messenger_IDL2C.inl"
	-@del /f/q "idl2\Messenger_IDL2C.h"
	-@del /f/q "idl2\Messenger_IDL2S.h"
	-@del /f/q "idl2\Messenger_IDL2C.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\Administrator_Client_IDL2\$(NULL)" mkdir "Static_Debug\Administrator_Client_IDL2"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEsd.lib TAOsd.lib TAO_AnyTypeCodesd.lib TAO_IFR_Clientsd.lib TAO_Valuetypesd.lib TAO_CodecFactorysd.lib TAO_PIsd.lib CIAO_Clientsd.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\Administrator_Client_IDL2.pdb" /machine:IA64 /out:"$(INSTALLDIR)\Administrator_Client_IDL2.exe"
LINK32_OBJS= \
	"$(INTDIR)\Administrator_Client_IDL2.obj" \
	"$(INTDIR)\idl2\Administrator_IDL2C.obj" \
	"$(INTDIR)\idl2\Messenger_IDL2C.obj" \
	"$(INTDIR)\idl2\Publication_IDL2C.obj" \
	"$(INTDIR)\idl2\Runnable_IDL2C.obj" \
	"$(INTDIR)\idl2\History_IDL2C.obj" \
	"$(INTDIR)\idl2\Message_IDL2C.obj"

"$(INSTALLDIR)\Administrator_Client_IDL2.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\Administrator_Client_IDL2.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\Administrator_Client_IDL2.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=Static_Release
INSTALLDIR=Static_Release
INTDIR=Static_Release\Administrator_Client_IDL2\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\Administrator_Client_IDL2.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\.." -I"..\..\ciao" -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Administrator_Client_IDL2.dep" "Administrator_Client_IDL2.cpp" "idl2\Administrator_IDL2C.cpp" "idl2\Messenger_IDL2C.cpp" "idl2\Publication_IDL2C.cpp" "idl2\Runnable_IDL2C.cpp" "idl2\History_IDL2C.cpp" "idl2\Message_IDL2C.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\Administrator_Client_IDL2.exe"
	-@del /f/q "$(INSTALLDIR)\Administrator_Client_IDL2.ilk"
	-@del /f/q "idl2\Runnable_IDL2C.inl"
	-@del /f/q "idl2\Runnable_IDL2C.h"
	-@del /f/q "idl2\Runnable_IDL2S.h"
	-@del /f/q "idl2\Runnable_IDL2C.cpp"
	-@del /f/q "idl2\Publication_IDL2C.inl"
	-@del /f/q "idl2\Publication_IDL2C.h"
	-@del /f/q "idl2\Publication_IDL2S.h"
	-@del /f/q "idl2\Publication_IDL2C.cpp"
	-@del /f/q "idl2\Message_IDL2C.inl"
	-@del /f/q "idl2\Message_IDL2C.h"
	-@del /f/q "idl2\Message_IDL2S.h"
	-@del /f/q "idl2\Message_IDL2C.cpp"
	-@del /f/q "idl2\History_IDL2C.inl"
	-@del /f/q "idl2\History_IDL2C.h"
	-@del /f/q "idl2\History_IDL2S.h"
	-@del /f/q "idl2\History_IDL2C.cpp"
	-@del /f/q "idl2\Administrator_IDL2C.inl"
	-@del /f/q "idl2\Administrator_IDL2C.h"
	-@del /f/q "idl2\Administrator_IDL2S.h"
	-@del /f/q "idl2\Administrator_IDL2C.cpp"
	-@del /f/q "idl2\Messenger_IDL2C.inl"
	-@del /f/q "idl2\Messenger_IDL2C.h"
	-@del /f/q "idl2\Messenger_IDL2S.h"
	-@del /f/q "idl2\Messenger_IDL2C.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\Administrator_Client_IDL2\$(NULL)" mkdir "Static_Release\Administrator_Client_IDL2"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\.." /I "..\..\.." /I "..\.." /I "..\..\ciao" /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEs.lib TAOs.lib TAO_AnyTypeCodes.lib TAO_IFR_Clients.lib TAO_Valuetypes.lib TAO_CodecFactorys.lib TAO_PIs.lib CIAO_Clients.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\Administrator_Client_IDL2.exe"
LINK32_OBJS= \
	"$(INTDIR)\Administrator_Client_IDL2.obj" \
	"$(INTDIR)\idl2\Administrator_IDL2C.obj" \
	"$(INTDIR)\idl2\Messenger_IDL2C.obj" \
	"$(INTDIR)\idl2\Publication_IDL2C.obj" \
	"$(INTDIR)\idl2\Runnable_IDL2C.obj" \
	"$(INTDIR)\idl2\History_IDL2C.obj" \
	"$(INTDIR)\idl2\Message_IDL2C.obj"

"$(INSTALLDIR)\Administrator_Client_IDL2.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\Administrator_Client_IDL2.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\Administrator_Client_IDL2.exe.manifest" -outputresource:$@;1

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Administrator_Client_IDL2.dep")
!INCLUDE "Makefile.Administrator_Client_IDL2.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Administrator_Client_IDL2.cpp"

"$(INTDIR)\Administrator_Client_IDL2.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Administrator_Client_IDL2.obj" $(SOURCE)

SOURCE="idl2\Administrator_IDL2C.cpp"

"$(INTDIR)\idl2\Administrator_IDL2C.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\idl2\$(NULL)" mkdir "$(INTDIR)\idl2\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\idl2\Administrator_IDL2C.obj" $(SOURCE)

SOURCE="idl2\Messenger_IDL2C.cpp"

"$(INTDIR)\idl2\Messenger_IDL2C.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\idl2\$(NULL)" mkdir "$(INTDIR)\idl2\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\idl2\Messenger_IDL2C.obj" $(SOURCE)

SOURCE="idl2\Publication_IDL2C.cpp"

"$(INTDIR)\idl2\Publication_IDL2C.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\idl2\$(NULL)" mkdir "$(INTDIR)\idl2\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\idl2\Publication_IDL2C.obj" $(SOURCE)

SOURCE="idl2\Runnable_IDL2C.cpp"

"$(INTDIR)\idl2\Runnable_IDL2C.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\idl2\$(NULL)" mkdir "$(INTDIR)\idl2\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\idl2\Runnable_IDL2C.obj" $(SOURCE)

SOURCE="idl2\History_IDL2C.cpp"

"$(INTDIR)\idl2\History_IDL2C.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\idl2\$(NULL)" mkdir "$(INTDIR)\idl2\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\idl2\History_IDL2C.obj" $(SOURCE)

SOURCE="idl2\Message_IDL2C.cpp"

"$(INTDIR)\idl2\Message_IDL2C.obj" : $(SOURCE)
	@if not exist "$(INTDIR)\idl2\$(NULL)" mkdir "$(INTDIR)\idl2\"
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\idl2\Message_IDL2C.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="idl2\Runnable_IDL2.idl"

InputPath=idl2\Runnable_IDL2.idl

"idl2\Runnable_IDL2C.inl" "idl2\Runnable_IDL2C.h" "idl2\Runnable_IDL2S.h" "idl2\Runnable_IDL2C.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-idl2_Runnable_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -Sm -Iidl2 -o idl2 -SS "$(InputPath)"
<<

SOURCE="idl2\Publication_IDL2.idl"

InputPath=idl2\Publication_IDL2.idl

"idl2\Publication_IDL2C.inl" "idl2\Publication_IDL2C.h" "idl2\Publication_IDL2S.h" "idl2\Publication_IDL2C.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-idl2_Publication_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -Sm -Iidl2 -o idl2 -SS "$(InputPath)"
<<

SOURCE="idl2\Message_IDL2.idl"

InputPath=idl2\Message_IDL2.idl

"idl2\Message_IDL2C.inl" "idl2\Message_IDL2C.h" "idl2\Message_IDL2S.h" "idl2\Message_IDL2C.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-idl2_Message_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -Sm -Iidl2 -o idl2 -SS "$(InputPath)"
<<

SOURCE="idl2\History_IDL2.idl"

InputPath=idl2\History_IDL2.idl

"idl2\History_IDL2C.inl" "idl2\History_IDL2C.h" "idl2\History_IDL2S.h" "idl2\History_IDL2C.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-idl2_History_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -Sm -Iidl2 -o idl2 -SS "$(InputPath)"
<<

SOURCE="idl2\Administrator_IDL2.idl"

InputPath=idl2\Administrator_IDL2.idl

"idl2\Administrator_IDL2C.inl" "idl2\Administrator_IDL2C.h" "idl2\Administrator_IDL2S.h" "idl2\Administrator_IDL2C.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-idl2_Administrator_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -Sm -Iidl2 -o idl2 -SS "$(InputPath)"
<<

SOURCE="idl2\Messenger_IDL2.idl"

InputPath=idl2\Messenger_IDL2.idl

"idl2\Messenger_IDL2C.inl" "idl2\Messenger_IDL2C.h" "idl2\Messenger_IDL2S.h" "idl2\Messenger_IDL2C.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-idl2_Messenger_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -Sm -Iidl2 -o idl2 -SS "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="idl2\Runnable_IDL2.idl"

InputPath=idl2\Runnable_IDL2.idl

"idl2\Runnable_IDL2C.inl" "idl2\Runnable_IDL2C.h" "idl2\Runnable_IDL2S.h" "idl2\Runnable_IDL2C.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-idl2_Runnable_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -Sm -Iidl2 -o idl2 -SS "$(InputPath)"
<<

SOURCE="idl2\Publication_IDL2.idl"

InputPath=idl2\Publication_IDL2.idl

"idl2\Publication_IDL2C.inl" "idl2\Publication_IDL2C.h" "idl2\Publication_IDL2S.h" "idl2\Publication_IDL2C.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-idl2_Publication_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -Sm -Iidl2 -o idl2 -SS "$(InputPath)"
<<

SOURCE="idl2\Message_IDL2.idl"

InputPath=idl2\Message_IDL2.idl

"idl2\Message_IDL2C.inl" "idl2\Message_IDL2C.h" "idl2\Message_IDL2S.h" "idl2\Message_IDL2C.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-idl2_Message_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -Sm -Iidl2 -o idl2 -SS "$(InputPath)"
<<

SOURCE="idl2\History_IDL2.idl"

InputPath=idl2\History_IDL2.idl

"idl2\History_IDL2C.inl" "idl2\History_IDL2C.h" "idl2\History_IDL2S.h" "idl2\History_IDL2C.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-idl2_History_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -Sm -Iidl2 -o idl2 -SS "$(InputPath)"
<<

SOURCE="idl2\Administrator_IDL2.idl"

InputPath=idl2\Administrator_IDL2.idl

"idl2\Administrator_IDL2C.inl" "idl2\Administrator_IDL2C.h" "idl2\Administrator_IDL2S.h" "idl2\Administrator_IDL2C.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-idl2_Administrator_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -Sm -Iidl2 -o idl2 -SS "$(InputPath)"
<<

SOURCE="idl2\Messenger_IDL2.idl"

InputPath=idl2\Messenger_IDL2.idl

"idl2\Messenger_IDL2C.inl" "idl2\Messenger_IDL2C.h" "idl2\Messenger_IDL2S.h" "idl2\Messenger_IDL2C.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-idl2_Messenger_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -Sm -Iidl2 -o idl2 -SS "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="idl2\Runnable_IDL2.idl"

InputPath=idl2\Runnable_IDL2.idl

"idl2\Runnable_IDL2C.inl" "idl2\Runnable_IDL2C.h" "idl2\Runnable_IDL2S.h" "idl2\Runnable_IDL2C.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-idl2_Runnable_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -Sm -Iidl2 -o idl2 -SS "$(InputPath)"
<<

SOURCE="idl2\Publication_IDL2.idl"

InputPath=idl2\Publication_IDL2.idl

"idl2\Publication_IDL2C.inl" "idl2\Publication_IDL2C.h" "idl2\Publication_IDL2S.h" "idl2\Publication_IDL2C.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-idl2_Publication_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -Sm -Iidl2 -o idl2 -SS "$(InputPath)"
<<

SOURCE="idl2\Message_IDL2.idl"

InputPath=idl2\Message_IDL2.idl

"idl2\Message_IDL2C.inl" "idl2\Message_IDL2C.h" "idl2\Message_IDL2S.h" "idl2\Message_IDL2C.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-idl2_Message_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -Sm -Iidl2 -o idl2 -SS "$(InputPath)"
<<

SOURCE="idl2\History_IDL2.idl"

InputPath=idl2\History_IDL2.idl

"idl2\History_IDL2C.inl" "idl2\History_IDL2C.h" "idl2\History_IDL2S.h" "idl2\History_IDL2C.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-idl2_History_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -Sm -Iidl2 -o idl2 -SS "$(InputPath)"
<<

SOURCE="idl2\Administrator_IDL2.idl"

InputPath=idl2\Administrator_IDL2.idl

"idl2\Administrator_IDL2C.inl" "idl2\Administrator_IDL2C.h" "idl2\Administrator_IDL2S.h" "idl2\Administrator_IDL2C.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-idl2_Administrator_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -Sm -Iidl2 -o idl2 -SS "$(InputPath)"
<<

SOURCE="idl2\Messenger_IDL2.idl"

InputPath=idl2\Messenger_IDL2.idl

"idl2\Messenger_IDL2C.inl" "idl2\Messenger_IDL2C.h" "idl2\Messenger_IDL2S.h" "idl2\Messenger_IDL2C.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-idl2_Messenger_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -Sm -Iidl2 -o idl2 -SS "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="idl2\Runnable_IDL2.idl"

InputPath=idl2\Runnable_IDL2.idl

"idl2\Runnable_IDL2C.inl" "idl2\Runnable_IDL2C.h" "idl2\Runnable_IDL2S.h" "idl2\Runnable_IDL2C.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-idl2_Runnable_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -Sm -Iidl2 -o idl2 -SS "$(InputPath)"
<<

SOURCE="idl2\Publication_IDL2.idl"

InputPath=idl2\Publication_IDL2.idl

"idl2\Publication_IDL2C.inl" "idl2\Publication_IDL2C.h" "idl2\Publication_IDL2S.h" "idl2\Publication_IDL2C.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-idl2_Publication_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -Sm -Iidl2 -o idl2 -SS "$(InputPath)"
<<

SOURCE="idl2\Message_IDL2.idl"

InputPath=idl2\Message_IDL2.idl

"idl2\Message_IDL2C.inl" "idl2\Message_IDL2C.h" "idl2\Message_IDL2S.h" "idl2\Message_IDL2C.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-idl2_Message_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -Sm -Iidl2 -o idl2 -SS "$(InputPath)"
<<

SOURCE="idl2\History_IDL2.idl"

InputPath=idl2\History_IDL2.idl

"idl2\History_IDL2C.inl" "idl2\History_IDL2C.h" "idl2\History_IDL2S.h" "idl2\History_IDL2C.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-idl2_History_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -Sm -Iidl2 -o idl2 -SS "$(InputPath)"
<<

SOURCE="idl2\Administrator_IDL2.idl"

InputPath=idl2\Administrator_IDL2.idl

"idl2\Administrator_IDL2C.inl" "idl2\Administrator_IDL2C.h" "idl2\Administrator_IDL2S.h" "idl2\Administrator_IDL2C.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-idl2_Administrator_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -Sm -Iidl2 -o idl2 -SS "$(InputPath)"
<<

SOURCE="idl2\Messenger_IDL2.idl"

InputPath=idl2\Messenger_IDL2.idl

"idl2\Messenger_IDL2C.inl" "idl2\Messenger_IDL2C.h" "idl2\Messenger_IDL2S.h" "idl2\Messenger_IDL2C.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-idl2_Messenger_IDL2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
        if not exist idl2 mkdir idl2
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\.. -I..\../ciao -Sm -Iidl2 -o idl2 -SS "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Administrator_Client_IDL2.dep")
	@echo Using "Makefile.Administrator_Client_IDL2.dep"
!ELSE
	@echo Warning: cannot find "Makefile.Administrator_Client_IDL2.dep"
!ENDIF
!ENDIF

