#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake TAO/CIAO -value_template platforms=Win64 -make_coexistence -features qos=1,mfc=1,boost=1,cidl=1,ciao=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: BasicSP_lem_gen BasicSP_stub_gen BasicSP_stub BasicSP_exec BasicSP_svnt BMClosedED_lem_gen BMClosedED_stub_gen BMClosedED_stub BMClosedED_exec BMClosedED_svnt BMDevice_lem_gen BMDevice_stub_gen BMDevice_stub BMDevice_exec BMDevice_svnt BMDisplay_lem_gen BMDisplay_stub_gen BMDisplay_stub BMDisplay_exec BMDisplay_svnt EC_stub_gen EC_stub EC_client EC_controller EC_lem_gen EC_exec EC_svnt

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.BasicSP_lem_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BasicSP_lem_gen.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.BasicSP_stub_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BasicSP_stub_gen.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.BasicSP_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BasicSP_stub.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.BasicSP_exec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BasicSP_exec.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.BasicSP_svnt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BasicSP_svnt.mak CFG="$(CFG)" $(@)
	@cd BMClosedED
	@echo Directory: BMClosedED
	@echo Project: Makefile.BMClosedED_lem_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMClosedED_lem_gen.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd BMClosedED
	@echo Directory: BMClosedED
	@echo Project: Makefile.BMClosedED_stub_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMClosedED_stub_gen.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd BMClosedED
	@echo Directory: BMClosedED
	@echo Project: Makefile.BMClosedED_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMClosedED_stub.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd BMClosedED
	@echo Directory: BMClosedED
	@echo Project: Makefile.BMClosedED_exec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMClosedED_exec.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd BMClosedED
	@echo Directory: BMClosedED
	@echo Project: Makefile.BMClosedED_svnt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMClosedED_svnt.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd BMDevice
	@echo Directory: BMDevice
	@echo Project: Makefile.BMDevice_lem_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMDevice_lem_gen.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd BMDevice
	@echo Directory: BMDevice
	@echo Project: Makefile.BMDevice_stub_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMDevice_stub_gen.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd BMDevice
	@echo Directory: BMDevice
	@echo Project: Makefile.BMDevice_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMDevice_stub.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd BMDevice
	@echo Directory: BMDevice
	@echo Project: Makefile.BMDevice_exec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMDevice_exec.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd BMDevice
	@echo Directory: BMDevice
	@echo Project: Makefile.BMDevice_svnt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMDevice_svnt.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd BMDisplay
	@echo Directory: BMDisplay
	@echo Project: Makefile.BMDisplay_lem_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMDisplay_lem_gen.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd BMDisplay
	@echo Directory: BMDisplay
	@echo Project: Makefile.BMDisplay_stub_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMDisplay_stub_gen.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd BMDisplay
	@echo Directory: BMDisplay
	@echo Project: Makefile.BMDisplay_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMDisplay_stub.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd BMDisplay
	@echo Directory: BMDisplay
	@echo Project: Makefile.BMDisplay_exec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMDisplay_exec.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd BMDisplay
	@echo Directory: BMDisplay
	@echo Project: Makefile.BMDisplay_svnt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMDisplay_svnt.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd EC
	@echo Directory: EC
	@echo Project: Makefile.EC_stub_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.EC_stub_gen.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd EC
	@echo Directory: EC
	@echo Project: Makefile.EC_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.EC_stub.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd EC
	@echo Directory: EC
	@echo Project: Makefile.EC_client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.EC_client.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd EC
	@echo Directory: EC
	@echo Project: Makefile.EC_controller.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.EC_controller.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd EC
	@echo Directory: EC
	@echo Project: Makefile.EC_lem_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.EC_lem_gen.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd EC
	@echo Directory: EC
	@echo Project: Makefile.EC_exec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.EC_exec.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd EC
	@echo Directory: EC
	@echo Project: Makefile.EC_svnt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.EC_svnt.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)

BasicSP_lem_gen:
	@echo Project: Makefile.BasicSP_lem_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BasicSP_lem_gen.mak CFG="$(CFG)" all

BasicSP_stub_gen:
	@echo Project: Makefile.BasicSP_stub_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BasicSP_stub_gen.mak CFG="$(CFG)" all

BasicSP_stub: BasicSP_stub_gen
	@echo Project: Makefile.BasicSP_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BasicSP_stub.mak CFG="$(CFG)" all

BasicSP_exec: BasicSP_lem_gen BasicSP_stub
	@echo Project: Makefile.BasicSP_exec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BasicSP_exec.mak CFG="$(CFG)" all

BasicSP_svnt: BasicSP_exec
	@echo Project: Makefile.BasicSP_svnt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BasicSP_svnt.mak CFG="$(CFG)" all

BMClosedED_lem_gen: BasicSP_lem_gen
	@cd BMClosedED
	@echo Directory: BMClosedED
	@echo Project: Makefile.BMClosedED_lem_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMClosedED_lem_gen.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

BMClosedED_stub_gen:
	@cd BMClosedED
	@echo Directory: BMClosedED
	@echo Project: Makefile.BMClosedED_stub_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMClosedED_stub_gen.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

BMClosedED_stub: BasicSP_stub BMClosedED_stub_gen
	@cd BMClosedED
	@echo Directory: BMClosedED
	@echo Project: Makefile.BMClosedED_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMClosedED_stub.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

BMClosedED_exec: BasicSP_exec BMClosedED_lem_gen BMClosedED_stub
	@cd BMClosedED
	@echo Directory: BMClosedED
	@echo Project: Makefile.BMClosedED_exec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMClosedED_exec.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

BMClosedED_svnt: BasicSP_svnt BMClosedED_exec
	@cd BMClosedED
	@echo Directory: BMClosedED
	@echo Project: Makefile.BMClosedED_svnt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMClosedED_svnt.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

BMDevice_lem_gen: BasicSP_lem_gen
	@cd BMDevice
	@echo Directory: BMDevice
	@echo Project: Makefile.BMDevice_lem_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMDevice_lem_gen.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

BMDevice_stub_gen:
	@cd BMDevice
	@echo Directory: BMDevice
	@echo Project: Makefile.BMDevice_stub_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMDevice_stub_gen.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

BMDevice_stub: BasicSP_stub BMDevice_stub_gen
	@cd BMDevice
	@echo Directory: BMDevice
	@echo Project: Makefile.BMDevice_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMDevice_stub.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

BMDevice_exec: BasicSP_exec BMDevice_lem_gen BMDevice_stub
	@cd BMDevice
	@echo Directory: BMDevice
	@echo Project: Makefile.BMDevice_exec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMDevice_exec.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

BMDevice_svnt: BasicSP_svnt BMDevice_exec
	@cd BMDevice
	@echo Directory: BMDevice
	@echo Project: Makefile.BMDevice_svnt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMDevice_svnt.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

BMDisplay_lem_gen: BasicSP_lem_gen
	@cd BMDisplay
	@echo Directory: BMDisplay
	@echo Project: Makefile.BMDisplay_lem_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMDisplay_lem_gen.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

BMDisplay_stub_gen:
	@cd BMDisplay
	@echo Directory: BMDisplay
	@echo Project: Makefile.BMDisplay_stub_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMDisplay_stub_gen.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

BMDisplay_stub: BasicSP_stub BMDisplay_stub_gen
	@cd BMDisplay
	@echo Directory: BMDisplay
	@echo Project: Makefile.BMDisplay_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMDisplay_stub.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

BMDisplay_exec: BasicSP_exec BMDisplay_lem_gen BMDisplay_stub
	@cd BMDisplay
	@echo Directory: BMDisplay
	@echo Project: Makefile.BMDisplay_exec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMDisplay_exec.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

BMDisplay_svnt: BasicSP_svnt BMDisplay_exec
	@cd BMDisplay
	@echo Directory: BMDisplay
	@echo Project: Makefile.BMDisplay_svnt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.BMDisplay_svnt.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

EC_stub_gen:
	@cd EC
	@echo Directory: EC
	@echo Project: Makefile.EC_stub_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.EC_stub_gen.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

EC_stub: BasicSP_stub EC_stub_gen
	@cd EC
	@echo Directory: EC
	@echo Project: Makefile.EC_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.EC_stub.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

EC_client: EC_stub
	@cd EC
	@echo Directory: EC
	@echo Project: Makefile.EC_client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.EC_client.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

EC_controller: EC_stub
	@cd EC
	@echo Directory: EC
	@echo Project: Makefile.EC_controller.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.EC_controller.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

EC_lem_gen: BasicSP_lem_gen
	@cd EC
	@echo Directory: EC
	@echo Project: Makefile.EC_lem_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.EC_lem_gen.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

EC_exec: BasicSP_exec EC_stub EC_lem_gen
	@cd EC
	@echo Directory: EC
	@echo Project: Makefile.EC_exec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.EC_exec.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

EC_svnt: BasicSP_svnt EC_exec
	@cd EC
	@echo Directory: EC
	@echo Project: Makefile.EC_svnt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.EC_svnt.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

project_name_list:
	@echo BMClosedED_exec
	@echo BMClosedED_lem_gen
	@echo BMClosedED_stub
	@echo BMClosedED_stub_gen
	@echo BMClosedED_svnt
	@echo BMDevice_exec
	@echo BMDevice_lem_gen
	@echo BMDevice_stub
	@echo BMDevice_stub_gen
	@echo BMDevice_svnt
	@echo BMDisplay_exec
	@echo BMDisplay_lem_gen
	@echo BMDisplay_stub
	@echo BMDisplay_stub_gen
	@echo BMDisplay_svnt
	@echo EC_client
	@echo EC_controller
	@echo EC_exec
	@echo EC_lem_gen
	@echo EC_stub
	@echo EC_stub_gen
	@echo EC_svnt
	@echo BasicSP_exec
	@echo BasicSP_lem_gen
	@echo BasicSP_stub
	@echo BasicSP_stub_gen
	@echo BasicSP_svnt
