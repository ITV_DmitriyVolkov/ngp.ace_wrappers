# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.BMDevice_exec.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\..\..\..\lib
INTDIR=Debug\BMDevice_exec\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\..\lib\BMDevice_execd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -I"..\..\.." -I"..\..\..\ciao" -I"..\..\..\..\orbsvcs" -I"..\..\..\DAnCE" -I"..\..\..\ciaosvcs\Events" -I"..\..\..\examples\BasicSP" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DBMDEVICE_EXEC_BUILD_DLL -f "Makefile.BMDevice_exec.dep" "BMDevice_exec.cpp" "BMDeviceEC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\BMDevice_execd.pdb"
	-@del /f/q "..\..\..\..\..\lib\BMDevice_execd.dll"
	-@del /f/q "$(OUTDIR)\BMDevice_execd.lib"
	-@del /f/q "$(OUTDIR)\BMDevice_execd.exp"
	-@del /f/q "$(OUTDIR)\BMDevice_execd.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\BMDevice_exec\$(NULL)" mkdir "Debug\BMDevice_exec"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"$(INTDIR)/" /I "..\..\..\..\.." /I "..\..\..\.." /I "..\..\.." /I "..\..\..\ciao" /I "..\..\..\..\orbsvcs" /I "..\..\..\DAnCE" /I "..\..\..\ciaosvcs\Events" /I "..\..\..\examples\BasicSP" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D BMDEVICE_EXEC_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_PortableServerd.lib TAO_IFR_Clientd.lib TAO_Valuetyped.lib TAO_CodecFactoryd.lib TAO_PId.lib CIAO_Clientd.lib CIAO_Containerd.lib CIAO_Events_Based.lib TAO_Messagingd.lib CIAO_Deployment_stubd.lib TAO_Svc_Utilsd.lib TAO_RTEventd.lib TAO_RTEvent_Skeld.lib TAO_RTEvent_Servd.lib TAO_CosNamingd.lib CIAO_RTEventd.lib CIAO_Eventsd.lib CIAO_Deployment_svntd.lib TAO_Utilsd.lib CIAO_Serverd.lib BasicSP_stubd.lib BasicSP_execd.lib BMDevice_stubd.lib /libpath:"." /libpath:"..\..\..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\..\..\..\lib\BMDevice_execd.pdb" /machine:IA64 /out:"..\..\..\..\..\lib\BMDevice_execd.dll" /implib:"$(OUTDIR)\BMDevice_execd.lib"
LINK32_OBJS= \
	"$(INTDIR)\BMDevice_exec.obj" \
	"$(INTDIR)\BMDeviceEC.obj"

"..\..\..\..\..\lib\BMDevice_execd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\..\lib\BMDevice_execd.dll.manifest" mt.exe -manifest "..\..\..\..\..\lib\BMDevice_execd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\..\..\..\lib
INTDIR=Release\BMDevice_exec\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\..\..\lib\BMDevice_exec.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -I"..\..\.." -I"..\..\..\ciao" -I"..\..\..\..\orbsvcs" -I"..\..\..\DAnCE" -I"..\..\..\ciaosvcs\Events" -I"..\..\..\examples\BasicSP" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DBMDEVICE_EXEC_BUILD_DLL -f "Makefile.BMDevice_exec.dep" "BMDevice_exec.cpp" "BMDeviceEC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\..\..\..\lib\BMDevice_exec.dll"
	-@del /f/q "$(OUTDIR)\BMDevice_exec.lib"
	-@del /f/q "$(OUTDIR)\BMDevice_exec.exp"
	-@del /f/q "$(OUTDIR)\BMDevice_exec.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\BMDevice_exec\$(NULL)" mkdir "Release\BMDevice_exec"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\..\.." /I "..\..\..\.." /I "..\..\.." /I "..\..\..\ciao" /I "..\..\..\..\orbsvcs" /I "..\..\..\DAnCE" /I "..\..\..\ciaosvcs\Events" /I "..\..\..\examples\BasicSP" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D BMDEVICE_EXEC_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_PortableServer.lib TAO_IFR_Client.lib TAO_Valuetype.lib TAO_CodecFactory.lib TAO_PI.lib CIAO_Client.lib CIAO_Container.lib CIAO_Events_Base.lib TAO_Messaging.lib CIAO_Deployment_stub.lib TAO_Svc_Utils.lib TAO_RTEvent.lib TAO_RTEvent_Skel.lib TAO_RTEvent_Serv.lib TAO_CosNaming.lib CIAO_RTEvent.lib CIAO_Events.lib CIAO_Deployment_svnt.lib TAO_Utils.lib CIAO_Server.lib BasicSP_stub.lib BasicSP_exec.lib BMDevice_stub.lib /libpath:"." /libpath:"..\..\..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\..\..\..\lib\BMDevice_exec.dll" /implib:"$(OUTDIR)\BMDevice_exec.lib"
LINK32_OBJS= \
	"$(INTDIR)\BMDevice_exec.obj" \
	"$(INTDIR)\BMDeviceEC.obj"

"..\..\..\..\..\lib\BMDevice_exec.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\..\..\lib\BMDevice_exec.dll.manifest" mt.exe -manifest "..\..\..\..\..\lib\BMDevice_exec.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\..\..\..\lib
INTDIR=Static_Debug\BMDevice_exec\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\BMDevice_execsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -I"..\..\.." -I"..\..\..\ciao" -I"..\..\..\..\orbsvcs" -I"..\..\..\DAnCE" -I"..\..\..\ciaosvcs\Events" -I"..\..\..\examples\BasicSP" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.BMDevice_exec.dep" "BMDevice_exec.cpp" "BMDeviceEC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\BMDevice_execsd.lib"
	-@del /f/q "$(OUTDIR)\BMDevice_execsd.exp"
	-@del /f/q "$(OUTDIR)\BMDevice_execsd.ilk"
	-@del /f/q "..\..\..\..\..\lib\BMDevice_execsd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\BMDevice_exec\$(NULL)" mkdir "Static_Debug\BMDevice_exec"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /Fd"..\..\..\..\..\lib\BMDevice_execsd.pdb" /I "..\..\..\..\.." /I "..\..\..\.." /I "..\..\.." /I "..\..\..\ciao" /I "..\..\..\..\orbsvcs" /I "..\..\..\DAnCE" /I "..\..\..\ciaosvcs\Events" /I "..\..\..\examples\BasicSP" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\..\lib\BMDevice_execsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\BMDevice_exec.obj" \
	"$(INTDIR)\BMDeviceEC.obj"

"$(OUTDIR)\BMDevice_execsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\BMDevice_execsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\BMDevice_execsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\..\..\..\lib
INTDIR=Static_Release\BMDevice_exec\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\BMDevice_execs.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -I"..\..\.." -I"..\..\..\ciao" -I"..\..\..\..\orbsvcs" -I"..\..\..\DAnCE" -I"..\..\..\ciaosvcs\Events" -I"..\..\..\examples\BasicSP" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.BMDevice_exec.dep" "BMDevice_exec.cpp" "BMDeviceEC.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\BMDevice_execs.lib"
	-@del /f/q "$(OUTDIR)\BMDevice_execs.exp"
	-@del /f/q "$(OUTDIR)\BMDevice_execs.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\BMDevice_exec\$(NULL)" mkdir "Static_Release\BMDevice_exec"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4290 /wd4996 /wd4250 /I "..\..\..\..\.." /I "..\..\..\.." /I "..\..\.." /I "..\..\..\ciao" /I "..\..\..\..\orbsvcs" /I "..\..\..\DAnCE" /I "..\..\..\ciaosvcs\Events" /I "..\..\..\examples\BasicSP" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\..\..\lib\BMDevice_execs.lib"
LINK32_OBJS= \
	"$(INTDIR)\BMDevice_exec.obj" \
	"$(INTDIR)\BMDeviceEC.obj"

"$(OUTDIR)\BMDevice_execs.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\BMDevice_execs.lib.manifest" mt.exe -manifest "$(OUTDIR)\BMDevice_execs.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.BMDevice_exec.dep")
!INCLUDE "Makefile.BMDevice_exec.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="BMDevice_exec.cpp"

"$(INTDIR)\BMDevice_exec.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\BMDevice_exec.obj" $(SOURCE)

SOURCE="BMDeviceEC.cpp"

"$(INTDIR)\BMDeviceEC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\BMDeviceEC.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.BMDevice_exec.dep")
	@echo Using "Makefile.BMDevice_exec.dep"
!ELSE
	@echo Warning: cannot find "Makefile.BMDevice_exec.dep"
!ENDIF
!ENDIF

