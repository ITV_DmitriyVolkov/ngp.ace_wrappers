#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake TAO/CIAO -value_template platforms=Win64 -make_coexistence -features qos=1,mfc=1,boost=1,cidl=1,ciao=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: Display_NavDisplay_cidl_gen Display_NavDisplay_idl_gen Display_NavDisplay_stub Display_NavDisplay_exec Display_NavDisplay_svnt

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.Display_NavDisplay_cidl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Display_NavDisplay_cidl_gen.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Display_NavDisplay_idl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Display_NavDisplay_idl_gen.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Display_NavDisplay_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Display_NavDisplay_stub.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Display_NavDisplay_exec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Display_NavDisplay_exec.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Display_NavDisplay_svnt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Display_NavDisplay_svnt.mak CFG="$(CFG)" $(@)

Display_NavDisplay_cidl_gen:
	@echo Project: Makefile.Display_NavDisplay_cidl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Display_NavDisplay_cidl_gen.mak CFG="$(CFG)" all

Display_NavDisplay_idl_gen:
	@echo Project: Makefile.Display_NavDisplay_idl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Display_NavDisplay_idl_gen.mak CFG="$(CFG)" all

Display_NavDisplay_stub: Display_NavDisplay_idl_gen
	@echo Project: Makefile.Display_NavDisplay_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Display_NavDisplay_stub.mak CFG="$(CFG)" all

Display_NavDisplay_exec: Display_NavDisplay_cidl_gen Display_NavDisplay_stub
	@echo Project: Makefile.Display_NavDisplay_exec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Display_NavDisplay_exec.mak CFG="$(CFG)" all

Display_NavDisplay_svnt: Display_NavDisplay_exec
	@echo Project: Makefile.Display_NavDisplay_svnt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Display_NavDisplay_svnt.mak CFG="$(CFG)" all

project_name_list:
	@echo Display_NavDisplay_cidl_gen
	@echo Display_NavDisplay_exec
	@echo Display_NavDisplay_idl_gen
	@echo Display_NavDisplay_stub
	@echo Display_NavDisplay_svnt
