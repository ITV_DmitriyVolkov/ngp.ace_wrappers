#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake TAO/CIAO -value_template platforms=Win64 -make_coexistence -features qos=1,mfc=1,boost=1,cidl=1,ciao=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: Null_Interface_idl_gen Null_Interface_stub Null_Component_idl_gen Null_Component_stub Null_Component_cidl_gen Null_Component_exec Null_Interface_skel Null_Component_svnt Null_Component_StaticDAnCEApp

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.Null_Interface_idl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Null_Interface_idl_gen.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Null_Interface_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Null_Interface_stub.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Null_Component_idl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Null_Component_idl_gen.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Null_Component_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Null_Component_stub.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Null_Component_cidl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Null_Component_cidl_gen.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Null_Component_exec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Null_Component_exec.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Null_Interface_skel.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Null_Interface_skel.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Null_Component_svnt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Null_Component_svnt.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Null_Component_StaticDAnCEApp.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Null_Component_StaticDAnCEApp.mak CFG="$(CFG)" $(@)

Null_Interface_idl_gen:
	@echo Project: Makefile.Null_Interface_idl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Null_Interface_idl_gen.mak CFG="$(CFG)" all

Null_Interface_stub: Null_Interface_idl_gen
	@echo Project: Makefile.Null_Interface_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Null_Interface_stub.mak CFG="$(CFG)" all

Null_Component_idl_gen:
	@echo Project: Makefile.Null_Component_idl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Null_Component_idl_gen.mak CFG="$(CFG)" all

Null_Component_stub: Null_Interface_stub Null_Component_idl_gen
	@echo Project: Makefile.Null_Component_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Null_Component_stub.mak CFG="$(CFG)" all

Null_Component_cidl_gen:
	@echo Project: Makefile.Null_Component_cidl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Null_Component_cidl_gen.mak CFG="$(CFG)" all

Null_Component_exec: Null_Component_stub Null_Component_cidl_gen
	@echo Project: Makefile.Null_Component_exec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Null_Component_exec.mak CFG="$(CFG)" all

Null_Interface_skel: Null_Interface_stub
	@echo Project: Makefile.Null_Interface_skel.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Null_Interface_skel.mak CFG="$(CFG)" all

Null_Component_svnt: Null_Component_exec Null_Interface_skel
	@echo Project: Makefile.Null_Component_svnt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Null_Component_svnt.mak CFG="$(CFG)" all

Null_Component_StaticDAnCEApp: Null_Component_svnt
	@echo Project: Makefile.Null_Component_StaticDAnCEApp.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Null_Component_StaticDAnCEApp.mak CFG="$(CFG)" all

project_name_list:
	@echo Null_Component_StaticDAnCEApp
	@echo Null_Component_cidl_gen
	@echo Null_Component_exec
	@echo Null_Component_idl_gen
	@echo Null_Component_stub
	@echo Null_Component_svnt
	@echo Null_Interface_idl_gen
	@echo Null_Interface_skel
	@echo Null_Interface_stub
