eval '(exit $?0)' && eval 'exec perl -S $0 ${1+"$@"}'
    & eval 'exec perl -S $0 $argv:q'
    if 0;

# $Id: run_test_collocated.pl 1155 2009-05-15 17:17:36Z mitza $
# -*- perl -*-

use RunEventTest;

exit RunEventTest::run_test (
       "deploymentplan_events_collocated.cdp", 
       "NodeManagerMap_collocated.dat");
