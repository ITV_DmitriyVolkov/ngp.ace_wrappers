# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.Hello_Receiver_cidl_gen.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "ReceiverE.idl" "Receiver_svnt.h" "Receiver_svnt.cpp" "ReceiverEC.inl" "ReceiverEC.h" "ReceiverES.h" "ReceiverEC.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INTDIR=Debug\Hello_Receiver_cidl_gen\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY)

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	-@rem
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\d.lib"
	-@del /f/q "$(OUTDIR)\d.exp"
	-@del /f/q "$(OUTDIR)\d.ilk"
	-@del /f/q "ReceiverE.idl"
	-@del /f/q "Receiver_svnt.h"
	-@del /f/q "Receiver_svnt.cpp"
	-@del /f/q "ReceiverEC.inl"
	-@del /f/q "ReceiverEC.h"
	-@del /f/q "ReceiverES.h"
	-@del /f/q "ReceiverEC.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\Hello_Receiver_cidl_gen\$(NULL)" mkdir "Debug\Hello_Receiver_cidl_gen"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /Fd"$(INTDIR)/" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe


!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=.
INTDIR=Release\Hello_Receiver_cidl_gen\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY)

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	-@rem
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\.lib"
	-@del /f/q "$(OUTDIR)\.exp"
	-@del /f/q "$(OUTDIR)\.ilk"
	-@del /f/q "ReceiverE.idl"
	-@del /f/q "Receiver_svnt.h"
	-@del /f/q "Receiver_svnt.cpp"
	-@del /f/q "ReceiverEC.inl"
	-@del /f/q "ReceiverEC.h"
	-@del /f/q "ReceiverES.h"
	-@del /f/q "ReceiverEC.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\Hello_Receiver_cidl_gen\$(NULL)" mkdir "Release\Hello_Receiver_cidl_gen"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe


!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=.
INTDIR=Static_Debug\Hello_Receiver_cidl_gen\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY)

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	-@rem
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q ".\sd.pdb"
	-@del /f/q "ReceiverE.idl"
	-@del /f/q "Receiver_svnt.h"
	-@del /f/q "Receiver_svnt.cpp"
	-@del /f/q "ReceiverEC.inl"
	-@del /f/q "ReceiverEC.h"
	-@del /f/q "ReceiverES.h"
	-@del /f/q "ReceiverEC.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\Hello_Receiver_cidl_gen\$(NULL)" mkdir "Static_Debug\Hello_Receiver_cidl_gen"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /Fd".\sd.pdb" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"



!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=.
INTDIR=Static_Release\Hello_Receiver_cidl_gen\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY)

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	-@rem
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "ReceiverE.idl"
	-@del /f/q "Receiver_svnt.h"
	-@del /f/q "Receiver_svnt.cpp"
	-@del /f/q "ReceiverEC.inl"
	-@del /f/q "ReceiverEC.h"
	-@del /f/q "ReceiverES.h"
	-@del /f/q "ReceiverEC.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\Hello_Receiver_cidl_gen\$(NULL)" mkdir "Static_Release\Hello_Receiver_cidl_gen"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"



!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Hello_Receiver_cidl_gen.dep")
!INCLUDE "Makefile.Hello_Receiver_cidl_gen.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
!IF  "$(CFG)" == "Win64 Debug"
SOURCE="Receiver.cidl"

InputPath=Receiver.cidl

"ReceiverE.idl" "Receiver_svnt.h" "Receiver_svnt.cpp" : $(SOURCE)  "..\..\..\bin\cidlc.exe"
	<<tempfile-Win64-Debug-cidl_files-Receiver_cidl.bat
	@echo off
	..\..\..\bin\cidlc -I ..\..\.. -I..\..\../DAnCE -I..\..\../ciao -I..\..\..\.. -I..\..\..\../tao -I..\..\..\../orbsvcs --svnt-export-macro RECEIVER_SVNT_Export --svnt-export-include Receiver_svnt_export.h "$(InputPath)"
<<

SOURCE="ReceiverE.idl"

InputPath=ReceiverE.idl

"ReceiverEC.inl" "ReceiverEC.h" "ReceiverES.h" "ReceiverEC.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-ReceiverE_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. -Sa -St -Wb,export_macro=RECEIVER_EXEC_Export -Wb,export_include=Receiver_exec_export.h -SS "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="Receiver.cidl"

InputPath=Receiver.cidl

"ReceiverE.idl" "Receiver_svnt.h" "Receiver_svnt.cpp" : $(SOURCE)  "..\..\..\bin\cidlc.exe"
	<<tempfile-Win64-Release-cidl_files-Receiver_cidl.bat
	@echo off
	..\..\..\bin\cidlc -I ..\..\.. -I..\..\../DAnCE -I..\..\../ciao -I..\..\..\.. -I..\..\..\../tao -I..\..\..\../orbsvcs --svnt-export-macro RECEIVER_SVNT_Export --svnt-export-include Receiver_svnt_export.h "$(InputPath)"
<<

SOURCE="ReceiverE.idl"

InputPath=ReceiverE.idl

"ReceiverEC.inl" "ReceiverEC.h" "ReceiverES.h" "ReceiverEC.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-ReceiverE_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. -Sa -St -Wb,export_macro=RECEIVER_EXEC_Export -Wb,export_include=Receiver_exec_export.h -SS "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="Receiver.cidl"

InputPath=Receiver.cidl

"ReceiverE.idl" "Receiver_svnt.h" "Receiver_svnt.cpp" : $(SOURCE)  "..\..\..\bin\cidlc.exe"
	<<tempfile-Win64-Static_Debug-cidl_files-Receiver_cidl.bat
	@echo off
	..\..\..\bin\cidlc -I ..\..\.. -I..\..\../DAnCE -I..\..\../ciao -I..\..\..\.. -I..\..\..\../tao -I..\..\..\../orbsvcs --svnt-export-macro RECEIVER_SVNT_Export --svnt-export-include Receiver_svnt_export.h "$(InputPath)"
<<

SOURCE="ReceiverE.idl"

InputPath=ReceiverE.idl

"ReceiverEC.inl" "ReceiverEC.h" "ReceiverES.h" "ReceiverEC.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-ReceiverE_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. -Sa -St -Wb,export_macro=RECEIVER_EXEC_Export -Wb,export_include=Receiver_exec_export.h -SS "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="Receiver.cidl"

InputPath=Receiver.cidl

"ReceiverE.idl" "Receiver_svnt.h" "Receiver_svnt.cpp" : $(SOURCE)  "..\..\..\bin\cidlc.exe"
	<<tempfile-Win64-Static_Release-cidl_files-Receiver_cidl.bat
	@echo off
	..\..\..\bin\cidlc -I ..\..\.. -I..\..\../DAnCE -I..\..\../ciao -I..\..\..\.. -I..\..\..\../tao -I..\..\..\../orbsvcs --svnt-export-macro RECEIVER_SVNT_Export --svnt-export-include Receiver_svnt_export.h "$(InputPath)"
<<

SOURCE="ReceiverE.idl"

InputPath=ReceiverE.idl

"ReceiverEC.inl" "ReceiverEC.h" "ReceiverES.h" "ReceiverEC.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-ReceiverE_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. -Sa -St -Wb,export_macro=RECEIVER_EXEC_Export -Wb,export_include=Receiver_exec_export.h -SS "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Hello_Receiver_cidl_gen.dep")
	@echo Using "Makefile.Hello_Receiver_cidl_gen.dep"
!ENDIF
!ENDIF

