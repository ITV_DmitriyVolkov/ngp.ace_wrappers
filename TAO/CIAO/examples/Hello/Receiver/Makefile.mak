#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake TAO/CIAO -value_template platforms=Win64 -make_coexistence -features qos=1,mfc=1,boost=1,cidl=1,ciao=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: Hello_Receiver_cidl_gen Hello_Receiver_idl_gen Hello_Receiver_stub Hello_Receiver_exec Hello_Receiver_svnt

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.Hello_Receiver_cidl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Hello_Receiver_cidl_gen.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Hello_Receiver_idl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Hello_Receiver_idl_gen.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Hello_Receiver_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Hello_Receiver_stub.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Hello_Receiver_exec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Hello_Receiver_exec.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Hello_Receiver_svnt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Hello_Receiver_svnt.mak CFG="$(CFG)" $(@)

Hello_Receiver_cidl_gen:
	@echo Project: Makefile.Hello_Receiver_cidl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Hello_Receiver_cidl_gen.mak CFG="$(CFG)" all

Hello_Receiver_idl_gen:
	@echo Project: Makefile.Hello_Receiver_idl_gen.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Hello_Receiver_idl_gen.mak CFG="$(CFG)" all

Hello_Receiver_stub: Hello_Receiver_idl_gen
	@echo Project: Makefile.Hello_Receiver_stub.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Hello_Receiver_stub.mak CFG="$(CFG)" all

Hello_Receiver_exec: Hello_Receiver_cidl_gen Hello_Receiver_stub
	@echo Project: Makefile.Hello_Receiver_exec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Hello_Receiver_exec.mak CFG="$(CFG)" all

Hello_Receiver_svnt: Hello_Receiver_exec
	@echo Project: Makefile.Hello_Receiver_svnt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Hello_Receiver_svnt.mak CFG="$(CFG)" all

project_name_list:
	@echo Hello_Receiver_cidl_gen
	@echo Hello_Receiver_exec
	@echo Hello_Receiver_idl_gen
	@echo Hello_Receiver_stub
	@echo Hello_Receiver_svnt
