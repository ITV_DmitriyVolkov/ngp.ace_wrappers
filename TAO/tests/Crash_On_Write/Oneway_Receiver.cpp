//
// $Id: Oneway_Receiver.cpp 935 2008-12-10 21:47:27Z mitza $
//
#include "Oneway_Receiver.h"

ACE_RCSID(Crash_On_Write, Oneway_Receiver, "$Id: Oneway_Receiver.cpp 935 2008-12-10 21:47:27Z mitza $")

Oneway_Receiver::Oneway_Receiver (void)
{
}

void
Oneway_Receiver::receive_oneway (void)
{
}
