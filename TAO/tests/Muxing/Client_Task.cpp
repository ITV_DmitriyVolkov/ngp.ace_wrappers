//
// $Id: Client_Task.cpp 14 2007-02-01 15:49:12Z mitza $
//

#include "Client_Task.h"

ACE_RCSID(Muxing, Client_Task, "$Id: Client_Task.cpp 14 2007-02-01 15:49:12Z mitza $")

Client_Task::Client_Task (Test::Receiver_ptr receiver,
                          CORBA::Long event_count,
                          CORBA::ULong event_size,
                          ACE_Thread_Manager *thr_mgr)
  : ACE_Task_Base (thr_mgr)
  , receiver_ (Test::Receiver::_duplicate (receiver))
  , event_count_ (event_count)
  , event_size_ (event_size)
{
}

int
Client_Task::svc (void)
{
  ACE_DEBUG ((LM_DEBUG, "(%P|%t) Starting client task\n"));
  Test::Payload payload (this->event_size_);
  payload.length (this->event_size_);

  for (CORBA::ULong j = 0; j != payload.length (); ++j)
    payload[j] = (j % 256);

  try
    {
      for (int i = 0; i != this->event_count_; ++i)
        {
          this->receiver_->receive_data (payload);
        }
    }
  catch (const CORBA::Exception&)
    {
      return -1;
    }
  ACE_DEBUG ((LM_DEBUG, "(%P|%t) Client task finished\n"));
  return 0;
}
