//
// $Id: Callback.cpp 979 2008-12-31 20:22:32Z mitza $
//
#include "Callback.h"

ACE_RCSID(Callback, Callback, "$Id: Callback.cpp 979 2008-12-31 20:22:32Z mitza $")

Callback::Callback (void)
  : received_callback_ (false)
{
}

void
Callback::test_oneway (void)
{
  received_callback_ = true;

  ACE_DEBUG ((LM_DEBUG,
              "(%P|%t) Callback - test_oneway!\n"));
}

bool
Callback::received_callback (void)
{
  return received_callback_;
}
