//
// $Id: Service.cpp 979 2008-12-31 20:22:32Z mitza $
//
#include "Service.h"

ACE_RCSID(Callback, Service, "$Id: Service.cpp 979 2008-12-31 20:22:32Z mitza $")

Service::Service (CORBA::ORB_ptr orb)
  : orb_ (CORBA::ORB::_duplicate (orb))
{
}

void
Service::run_test (Test::Callback_ptr callback)
{
  callback->test_oneway ();
}

void
Service::shutdown (void)
{
  ACE_DEBUG ((LM_DEBUG, "(%P|%t) - Shuting down self (server)\n"));

  this->orb_->shutdown (0);
}
