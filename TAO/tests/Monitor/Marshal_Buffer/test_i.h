// $Id: test_i.h 979 2008-12-31 20:22:32Z mitza $

#ifndef TAO_TEST_I_H
#define TAO_TEST_I_H

#include "testS.h"

class test_impl : public virtual POA_test
{
  virtual char * test_op (const char * in_arg);
};

#endif /* TAO_TEST_I_H */
