// $Id: test_i.cpp 935 2008-12-10 21:47:27Z mitza $

#include "test_i.h"

#if !defined(__ACE_INLINE__)
#include "test_i.inl"
#endif /* __ACE_INLINE__ */

ACE_RCSID(Native_Exceptions, test_i, "$Id: test_i.cpp 935 2008-12-10 21:47:27Z mitza $")

CORBA::Long
Simple_Server_i::test_method (CORBA::Long x)
{
  return x;
}

CORBA::Long
Simple_Server_i::test_raise (CORBA::Long)
{
  throw Simple_Server::Failure ();
}

void
Simple_Server_i::shutdown (void)
{
  this->orb_->shutdown ();
}
