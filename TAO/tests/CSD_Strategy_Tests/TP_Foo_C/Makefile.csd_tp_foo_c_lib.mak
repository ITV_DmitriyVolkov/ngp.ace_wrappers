# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.csd_tp_foo_c_lib.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "Foo_CC.inl" "Foo_CS.inl" "Foo_CC.h" "Foo_CS.h" "Foo_CC.cpp" "Foo_CS.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INTDIR=Debug\csd_tp_foo_c_lib\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) ".\CSD_TP_Foo_Cd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\CSD_Strategy_Tests\TP_Common" -I"..\..\..\tao" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DCSD_TP_FOO_C_BUILD_DLL -f "Makefile.csd_tp_foo_c_lib.dep" "Foo_CC.cpp" "Foo_CS.cpp" "Foo_C_ClientEngine.cpp" "Foo_C_Custom_ClientEngine.cpp" "Foo_C_Custom_Proxy.cpp" "Foo_C_cust_op1.cpp" "Foo_C_cust_op2.cpp" "Foo_C_cust_op3.cpp" "Foo_C_cust_op4.cpp" "Foo_C_cust_op5.cpp" "Foo_C_i.cpp" "Foo_C_Statistics.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_Cd.pdb"
	-@del /f/q ".\CSD_TP_Foo_Cd.dll"
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_Cd.lib"
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_Cd.exp"
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_Cd.ilk"
	-@del /f/q "Foo_CC.inl"
	-@del /f/q "Foo_CS.inl"
	-@del /f/q "Foo_CC.h"
	-@del /f/q "Foo_CS.h"
	-@del /f/q "Foo_CC.cpp"
	-@del /f/q "Foo_CS.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\csd_tp_foo_c_lib\$(NULL)" mkdir "Debug\csd_tp_foo_c_lib"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /I "..\..\CSD_Strategy_Tests\TP_Common" /I "..\..\..\tao" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D CSD_TP_FOO_C_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_PortableServerd.lib TAO_CodecFactoryd.lib TAO_PId.lib TAO_CSD_Frameworkd.lib TAO_CSD_ThreadPoold.lib CSD_TP_Testd.lib /libpath:"." /libpath:"..\..\..\..\lib" /libpath:"..\..\CSD_Strategy_Tests\TP_Common" /nologo /subsystem:windows /dll /debug /pdb:".\CSD_TP_Foo_Cd.pdb" /machine:IA64 /out:".\CSD_TP_Foo_Cd.dll" /implib:"$(OUTDIR)\CSD_TP_Foo_Cd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Foo_CC.obj" \
	"$(INTDIR)\Foo_CS.obj" \
	"$(INTDIR)\Foo_C_ClientEngine.obj" \
	"$(INTDIR)\Foo_C_Custom_ClientEngine.obj" \
	"$(INTDIR)\Foo_C_Custom_Proxy.obj" \
	"$(INTDIR)\Foo_C_cust_op1.obj" \
	"$(INTDIR)\Foo_C_cust_op2.obj" \
	"$(INTDIR)\Foo_C_cust_op3.obj" \
	"$(INTDIR)\Foo_C_cust_op4.obj" \
	"$(INTDIR)\Foo_C_cust_op5.obj" \
	"$(INTDIR)\Foo_C_i.obj" \
	"$(INTDIR)\Foo_C_Statistics.obj"

".\CSD_TP_Foo_Cd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist ".\CSD_TP_Foo_Cd.dll.manifest" mt.exe -manifest ".\CSD_TP_Foo_Cd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=.
INTDIR=Release\csd_tp_foo_c_lib\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) ".\CSD_TP_Foo_C.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\CSD_Strategy_Tests\TP_Common" -I"..\..\..\tao" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DCSD_TP_FOO_C_BUILD_DLL -f "Makefile.csd_tp_foo_c_lib.dep" "Foo_CC.cpp" "Foo_CS.cpp" "Foo_C_ClientEngine.cpp" "Foo_C_Custom_ClientEngine.cpp" "Foo_C_Custom_Proxy.cpp" "Foo_C_cust_op1.cpp" "Foo_C_cust_op2.cpp" "Foo_C_cust_op3.cpp" "Foo_C_cust_op4.cpp" "Foo_C_cust_op5.cpp" "Foo_C_i.cpp" "Foo_C_Statistics.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q ".\CSD_TP_Foo_C.dll"
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_C.lib"
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_C.exp"
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_C.ilk"
	-@del /f/q "Foo_CC.inl"
	-@del /f/q "Foo_CS.inl"
	-@del /f/q "Foo_CC.h"
	-@del /f/q "Foo_CS.h"
	-@del /f/q "Foo_CC.cpp"
	-@del /f/q "Foo_CS.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\csd_tp_foo_c_lib\$(NULL)" mkdir "Release\csd_tp_foo_c_lib"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\.." /I "..\..\.." /I "..\..\CSD_Strategy_Tests\TP_Common" /I "..\..\..\tao" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D CSD_TP_FOO_C_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_PortableServer.lib TAO_CodecFactory.lib TAO_PI.lib TAO_CSD_Framework.lib TAO_CSD_ThreadPool.lib CSD_TP_Test.lib /libpath:"." /libpath:"..\..\..\..\lib" /libpath:"..\..\CSD_Strategy_Tests\TP_Common" /nologo /subsystem:windows /dll  /machine:IA64 /out:".\CSD_TP_Foo_C.dll" /implib:"$(OUTDIR)\CSD_TP_Foo_C.lib"
LINK32_OBJS= \
	"$(INTDIR)\Foo_CC.obj" \
	"$(INTDIR)\Foo_CS.obj" \
	"$(INTDIR)\Foo_C_ClientEngine.obj" \
	"$(INTDIR)\Foo_C_Custom_ClientEngine.obj" \
	"$(INTDIR)\Foo_C_Custom_Proxy.obj" \
	"$(INTDIR)\Foo_C_cust_op1.obj" \
	"$(INTDIR)\Foo_C_cust_op2.obj" \
	"$(INTDIR)\Foo_C_cust_op3.obj" \
	"$(INTDIR)\Foo_C_cust_op4.obj" \
	"$(INTDIR)\Foo_C_cust_op5.obj" \
	"$(INTDIR)\Foo_C_i.obj" \
	"$(INTDIR)\Foo_C_Statistics.obj"

".\CSD_TP_Foo_C.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist ".\CSD_TP_Foo_C.dll.manifest" mt.exe -manifest ".\CSD_TP_Foo_C.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=.
INTDIR=Static_Debug\csd_tp_foo_c_lib\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CSD_TP_Foo_Csd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\CSD_Strategy_Tests\TP_Common" -I"..\..\..\tao" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.csd_tp_foo_c_lib.dep" "Foo_CC.cpp" "Foo_CS.cpp" "Foo_C_ClientEngine.cpp" "Foo_C_Custom_ClientEngine.cpp" "Foo_C_Custom_Proxy.cpp" "Foo_C_cust_op1.cpp" "Foo_C_cust_op2.cpp" "Foo_C_cust_op3.cpp" "Foo_C_cust_op4.cpp" "Foo_C_cust_op5.cpp" "Foo_C_i.cpp" "Foo_C_Statistics.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_Csd.lib"
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_Csd.exp"
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_Csd.ilk"
	-@del /f/q ".\CSD_TP_Foo_Csd.pdb"
	-@del /f/q "Foo_CC.inl"
	-@del /f/q "Foo_CS.inl"
	-@del /f/q "Foo_CC.h"
	-@del /f/q "Foo_CS.h"
	-@del /f/q "Foo_CC.cpp"
	-@del /f/q "Foo_CS.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\csd_tp_foo_c_lib\$(NULL)" mkdir "Static_Debug\csd_tp_foo_c_lib"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd".\CSD_TP_Foo_Csd.pdb" /I "..\..\..\.." /I "..\..\.." /I "..\..\CSD_Strategy_Tests\TP_Common" /I "..\..\..\tao" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:".\CSD_TP_Foo_Csd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Foo_CC.obj" \
	"$(INTDIR)\Foo_CS.obj" \
	"$(INTDIR)\Foo_C_ClientEngine.obj" \
	"$(INTDIR)\Foo_C_Custom_ClientEngine.obj" \
	"$(INTDIR)\Foo_C_Custom_Proxy.obj" \
	"$(INTDIR)\Foo_C_cust_op1.obj" \
	"$(INTDIR)\Foo_C_cust_op2.obj" \
	"$(INTDIR)\Foo_C_cust_op3.obj" \
	"$(INTDIR)\Foo_C_cust_op4.obj" \
	"$(INTDIR)\Foo_C_cust_op5.obj" \
	"$(INTDIR)\Foo_C_i.obj" \
	"$(INTDIR)\Foo_C_Statistics.obj"

"$(OUTDIR)\CSD_TP_Foo_Csd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CSD_TP_Foo_Csd.lib.manifest" mt.exe -manifest "$(OUTDIR)\CSD_TP_Foo_Csd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=.
INTDIR=Static_Release\csd_tp_foo_c_lib\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CSD_TP_Foo_Cs.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\CSD_Strategy_Tests\TP_Common" -I"..\..\..\tao" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.csd_tp_foo_c_lib.dep" "Foo_CC.cpp" "Foo_CS.cpp" "Foo_C_ClientEngine.cpp" "Foo_C_Custom_ClientEngine.cpp" "Foo_C_Custom_Proxy.cpp" "Foo_C_cust_op1.cpp" "Foo_C_cust_op2.cpp" "Foo_C_cust_op3.cpp" "Foo_C_cust_op4.cpp" "Foo_C_cust_op5.cpp" "Foo_C_i.cpp" "Foo_C_Statistics.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_Cs.lib"
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_Cs.exp"
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_Cs.ilk"
	-@del /f/q "Foo_CC.inl"
	-@del /f/q "Foo_CS.inl"
	-@del /f/q "Foo_CC.h"
	-@del /f/q "Foo_CS.h"
	-@del /f/q "Foo_CC.cpp"
	-@del /f/q "Foo_CS.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\csd_tp_foo_c_lib\$(NULL)" mkdir "Static_Release\csd_tp_foo_c_lib"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\.." /I "..\..\.." /I "..\..\CSD_Strategy_Tests\TP_Common" /I "..\..\..\tao" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:".\CSD_TP_Foo_Cs.lib"
LINK32_OBJS= \
	"$(INTDIR)\Foo_CC.obj" \
	"$(INTDIR)\Foo_CS.obj" \
	"$(INTDIR)\Foo_C_ClientEngine.obj" \
	"$(INTDIR)\Foo_C_Custom_ClientEngine.obj" \
	"$(INTDIR)\Foo_C_Custom_Proxy.obj" \
	"$(INTDIR)\Foo_C_cust_op1.obj" \
	"$(INTDIR)\Foo_C_cust_op2.obj" \
	"$(INTDIR)\Foo_C_cust_op3.obj" \
	"$(INTDIR)\Foo_C_cust_op4.obj" \
	"$(INTDIR)\Foo_C_cust_op5.obj" \
	"$(INTDIR)\Foo_C_i.obj" \
	"$(INTDIR)\Foo_C_Statistics.obj"

"$(OUTDIR)\CSD_TP_Foo_Cs.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CSD_TP_Foo_Cs.lib.manifest" mt.exe -manifest "$(OUTDIR)\CSD_TP_Foo_Cs.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.csd_tp_foo_c_lib.dep")
!INCLUDE "Makefile.csd_tp_foo_c_lib.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Foo_CC.cpp"

"$(INTDIR)\Foo_CC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Foo_CC.obj" $(SOURCE)

SOURCE="Foo_CS.cpp"

"$(INTDIR)\Foo_CS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Foo_CS.obj" $(SOURCE)

SOURCE="Foo_C_ClientEngine.cpp"

"$(INTDIR)\Foo_C_ClientEngine.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Foo_C_ClientEngine.obj" $(SOURCE)

SOURCE="Foo_C_Custom_ClientEngine.cpp"

"$(INTDIR)\Foo_C_Custom_ClientEngine.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Foo_C_Custom_ClientEngine.obj" $(SOURCE)

SOURCE="Foo_C_Custom_Proxy.cpp"

"$(INTDIR)\Foo_C_Custom_Proxy.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Foo_C_Custom_Proxy.obj" $(SOURCE)

SOURCE="Foo_C_cust_op1.cpp"

"$(INTDIR)\Foo_C_cust_op1.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Foo_C_cust_op1.obj" $(SOURCE)

SOURCE="Foo_C_cust_op2.cpp"

"$(INTDIR)\Foo_C_cust_op2.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Foo_C_cust_op2.obj" $(SOURCE)

SOURCE="Foo_C_cust_op3.cpp"

"$(INTDIR)\Foo_C_cust_op3.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Foo_C_cust_op3.obj" $(SOURCE)

SOURCE="Foo_C_cust_op4.cpp"

"$(INTDIR)\Foo_C_cust_op4.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Foo_C_cust_op4.obj" $(SOURCE)

SOURCE="Foo_C_cust_op5.cpp"

"$(INTDIR)\Foo_C_cust_op5.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Foo_C_cust_op5.obj" $(SOURCE)

SOURCE="Foo_C_i.cpp"

"$(INTDIR)\Foo_C_i.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Foo_C_i.obj" $(SOURCE)

SOURCE="Foo_C_Statistics.cpp"

"$(INTDIR)\Foo_C_Statistics.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Foo_C_Statistics.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="Foo_C.idl"

InputPath=Foo_C.idl

"Foo_CC.inl" "Foo_CS.inl" "Foo_CC.h" "Foo_CS.h" "Foo_CC.cpp" "Foo_CS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Foo_C_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I../../CSD_Strategy_Tests/TP_Common -b -Wb,export_macro=CSD_TP_Foo_C_Export -Wb,export_include=CSD_TP_Foo_C_Export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="Foo_C.idl"

InputPath=Foo_C.idl

"Foo_CC.inl" "Foo_CS.inl" "Foo_CC.h" "Foo_CS.h" "Foo_CC.cpp" "Foo_CS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Foo_C_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I../../CSD_Strategy_Tests/TP_Common -b -Wb,export_macro=CSD_TP_Foo_C_Export -Wb,export_include=CSD_TP_Foo_C_Export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="Foo_C.idl"

InputPath=Foo_C.idl

"Foo_CC.inl" "Foo_CS.inl" "Foo_CC.h" "Foo_CS.h" "Foo_CC.cpp" "Foo_CS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Foo_C_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I../../CSD_Strategy_Tests/TP_Common -b -Wb,export_macro=CSD_TP_Foo_C_Export -Wb,export_include=CSD_TP_Foo_C_Export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="Foo_C.idl"

InputPath=Foo_C.idl

"Foo_CC.inl" "Foo_CS.inl" "Foo_CC.h" "Foo_CS.h" "Foo_CC.cpp" "Foo_CS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Foo_C_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I../../CSD_Strategy_Tests/TP_Common -b -Wb,export_macro=CSD_TP_Foo_C_Export -Wb,export_include=CSD_TP_Foo_C_Export.h "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.csd_tp_foo_c_lib.dep")
	@echo Using "Makefile.csd_tp_foo_c_lib.dep"
!ELSE
	@echo Warning: cannot find "Makefile.csd_tp_foo_c_lib.dep"
!ENDIF
!ENDIF

