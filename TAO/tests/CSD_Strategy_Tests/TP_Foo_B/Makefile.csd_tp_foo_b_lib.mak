# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.csd_tp_foo_b_lib.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "Foo_BC.inl" "Foo_BS.inl" "Foo_BC.h" "Foo_BS.h" "Foo_BC.cpp" "Foo_BS.cpp" "CallbackC.inl" "CallbackS.inl" "CallbackC.h" "CallbackS.h" "CallbackC.cpp" "CallbackS.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INTDIR=Debug\csd_tp_foo_b_lib\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) ".\CSD_TP_Foo_Bd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\CSD_Strategy_Tests\TP_Common" -I"..\..\..\tao" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DCSD_TP_FOO_B_BUILD_DLL -f "Makefile.csd_tp_foo_b_lib.dep" "Foo_BC.cpp" "Foo_BS.cpp" "Foo_B_ClientEngine.cpp" "Foo_B_SimpleClientEngine.cpp" "Foo_B_i.cpp" "Foo_B_Statistics.cpp" "Callback_i.cpp" "CallbackC.cpp" "CallbackS.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_Bd.pdb"
	-@del /f/q ".\CSD_TP_Foo_Bd.dll"
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_Bd.lib"
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_Bd.exp"
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_Bd.ilk"
	-@del /f/q "Foo_BC.inl"
	-@del /f/q "Foo_BS.inl"
	-@del /f/q "Foo_BC.h"
	-@del /f/q "Foo_BS.h"
	-@del /f/q "Foo_BC.cpp"
	-@del /f/q "Foo_BS.cpp"
	-@del /f/q "CallbackC.inl"
	-@del /f/q "CallbackS.inl"
	-@del /f/q "CallbackC.h"
	-@del /f/q "CallbackS.h"
	-@del /f/q "CallbackC.cpp"
	-@del /f/q "CallbackS.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\csd_tp_foo_b_lib\$(NULL)" mkdir "Debug\csd_tp_foo_b_lib"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /I "..\..\CSD_Strategy_Tests\TP_Common" /I "..\..\..\tao" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D CSD_TP_FOO_B_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_PortableServerd.lib TAO_CodecFactoryd.lib TAO_PId.lib TAO_CSD_Frameworkd.lib TAO_CSD_ThreadPoold.lib CSD_TP_Testd.lib /libpath:"." /libpath:"..\..\..\..\lib" /libpath:"..\..\CSD_Strategy_Tests\TP_Common" /nologo /subsystem:windows /dll /debug /pdb:".\CSD_TP_Foo_Bd.pdb" /machine:IA64 /out:".\CSD_TP_Foo_Bd.dll" /implib:"$(OUTDIR)\CSD_TP_Foo_Bd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Foo_BC.obj" \
	"$(INTDIR)\Foo_BS.obj" \
	"$(INTDIR)\Foo_B_ClientEngine.obj" \
	"$(INTDIR)\Foo_B_SimpleClientEngine.obj" \
	"$(INTDIR)\Foo_B_i.obj" \
	"$(INTDIR)\Foo_B_Statistics.obj" \
	"$(INTDIR)\Callback_i.obj" \
	"$(INTDIR)\CallbackC.obj" \
	"$(INTDIR)\CallbackS.obj"

".\CSD_TP_Foo_Bd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist ".\CSD_TP_Foo_Bd.dll.manifest" mt.exe -manifest ".\CSD_TP_Foo_Bd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=.
INTDIR=Release\csd_tp_foo_b_lib\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) ".\CSD_TP_Foo_B.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\CSD_Strategy_Tests\TP_Common" -I"..\..\..\tao" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DCSD_TP_FOO_B_BUILD_DLL -f "Makefile.csd_tp_foo_b_lib.dep" "Foo_BC.cpp" "Foo_BS.cpp" "Foo_B_ClientEngine.cpp" "Foo_B_SimpleClientEngine.cpp" "Foo_B_i.cpp" "Foo_B_Statistics.cpp" "Callback_i.cpp" "CallbackC.cpp" "CallbackS.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q ".\CSD_TP_Foo_B.dll"
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_B.lib"
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_B.exp"
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_B.ilk"
	-@del /f/q "Foo_BC.inl"
	-@del /f/q "Foo_BS.inl"
	-@del /f/q "Foo_BC.h"
	-@del /f/q "Foo_BS.h"
	-@del /f/q "Foo_BC.cpp"
	-@del /f/q "Foo_BS.cpp"
	-@del /f/q "CallbackC.inl"
	-@del /f/q "CallbackS.inl"
	-@del /f/q "CallbackC.h"
	-@del /f/q "CallbackS.h"
	-@del /f/q "CallbackC.cpp"
	-@del /f/q "CallbackS.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\csd_tp_foo_b_lib\$(NULL)" mkdir "Release\csd_tp_foo_b_lib"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\.." /I "..\..\.." /I "..\..\CSD_Strategy_Tests\TP_Common" /I "..\..\..\tao" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D CSD_TP_FOO_B_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_PortableServer.lib TAO_CodecFactory.lib TAO_PI.lib TAO_CSD_Framework.lib TAO_CSD_ThreadPool.lib CSD_TP_Test.lib /libpath:"." /libpath:"..\..\..\..\lib" /libpath:"..\..\CSD_Strategy_Tests\TP_Common" /nologo /subsystem:windows /dll  /machine:IA64 /out:".\CSD_TP_Foo_B.dll" /implib:"$(OUTDIR)\CSD_TP_Foo_B.lib"
LINK32_OBJS= \
	"$(INTDIR)\Foo_BC.obj" \
	"$(INTDIR)\Foo_BS.obj" \
	"$(INTDIR)\Foo_B_ClientEngine.obj" \
	"$(INTDIR)\Foo_B_SimpleClientEngine.obj" \
	"$(INTDIR)\Foo_B_i.obj" \
	"$(INTDIR)\Foo_B_Statistics.obj" \
	"$(INTDIR)\Callback_i.obj" \
	"$(INTDIR)\CallbackC.obj" \
	"$(INTDIR)\CallbackS.obj"

".\CSD_TP_Foo_B.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist ".\CSD_TP_Foo_B.dll.manifest" mt.exe -manifest ".\CSD_TP_Foo_B.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=.
INTDIR=Static_Debug\csd_tp_foo_b_lib\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CSD_TP_Foo_Bsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\CSD_Strategy_Tests\TP_Common" -I"..\..\..\tao" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.csd_tp_foo_b_lib.dep" "Foo_BC.cpp" "Foo_BS.cpp" "Foo_B_ClientEngine.cpp" "Foo_B_SimpleClientEngine.cpp" "Foo_B_i.cpp" "Foo_B_Statistics.cpp" "Callback_i.cpp" "CallbackC.cpp" "CallbackS.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_Bsd.lib"
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_Bsd.exp"
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_Bsd.ilk"
	-@del /f/q ".\CSD_TP_Foo_Bsd.pdb"
	-@del /f/q "Foo_BC.inl"
	-@del /f/q "Foo_BS.inl"
	-@del /f/q "Foo_BC.h"
	-@del /f/q "Foo_BS.h"
	-@del /f/q "Foo_BC.cpp"
	-@del /f/q "Foo_BS.cpp"
	-@del /f/q "CallbackC.inl"
	-@del /f/q "CallbackS.inl"
	-@del /f/q "CallbackC.h"
	-@del /f/q "CallbackS.h"
	-@del /f/q "CallbackC.cpp"
	-@del /f/q "CallbackS.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\csd_tp_foo_b_lib\$(NULL)" mkdir "Static_Debug\csd_tp_foo_b_lib"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd".\CSD_TP_Foo_Bsd.pdb" /I "..\..\..\.." /I "..\..\.." /I "..\..\CSD_Strategy_Tests\TP_Common" /I "..\..\..\tao" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:".\CSD_TP_Foo_Bsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Foo_BC.obj" \
	"$(INTDIR)\Foo_BS.obj" \
	"$(INTDIR)\Foo_B_ClientEngine.obj" \
	"$(INTDIR)\Foo_B_SimpleClientEngine.obj" \
	"$(INTDIR)\Foo_B_i.obj" \
	"$(INTDIR)\Foo_B_Statistics.obj" \
	"$(INTDIR)\Callback_i.obj" \
	"$(INTDIR)\CallbackC.obj" \
	"$(INTDIR)\CallbackS.obj"

"$(OUTDIR)\CSD_TP_Foo_Bsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CSD_TP_Foo_Bsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\CSD_TP_Foo_Bsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=.
INTDIR=Static_Release\csd_tp_foo_b_lib\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CSD_TP_Foo_Bs.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\CSD_Strategy_Tests\TP_Common" -I"..\..\..\tao" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.csd_tp_foo_b_lib.dep" "Foo_BC.cpp" "Foo_BS.cpp" "Foo_B_ClientEngine.cpp" "Foo_B_SimpleClientEngine.cpp" "Foo_B_i.cpp" "Foo_B_Statistics.cpp" "Callback_i.cpp" "CallbackC.cpp" "CallbackS.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_Bs.lib"
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_Bs.exp"
	-@del /f/q "$(OUTDIR)\CSD_TP_Foo_Bs.ilk"
	-@del /f/q "Foo_BC.inl"
	-@del /f/q "Foo_BS.inl"
	-@del /f/q "Foo_BC.h"
	-@del /f/q "Foo_BS.h"
	-@del /f/q "Foo_BC.cpp"
	-@del /f/q "Foo_BS.cpp"
	-@del /f/q "CallbackC.inl"
	-@del /f/q "CallbackS.inl"
	-@del /f/q "CallbackC.h"
	-@del /f/q "CallbackS.h"
	-@del /f/q "CallbackC.cpp"
	-@del /f/q "CallbackS.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\csd_tp_foo_b_lib\$(NULL)" mkdir "Static_Release\csd_tp_foo_b_lib"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\.." /I "..\..\.." /I "..\..\CSD_Strategy_Tests\TP_Common" /I "..\..\..\tao" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:".\CSD_TP_Foo_Bs.lib"
LINK32_OBJS= \
	"$(INTDIR)\Foo_BC.obj" \
	"$(INTDIR)\Foo_BS.obj" \
	"$(INTDIR)\Foo_B_ClientEngine.obj" \
	"$(INTDIR)\Foo_B_SimpleClientEngine.obj" \
	"$(INTDIR)\Foo_B_i.obj" \
	"$(INTDIR)\Foo_B_Statistics.obj" \
	"$(INTDIR)\Callback_i.obj" \
	"$(INTDIR)\CallbackC.obj" \
	"$(INTDIR)\CallbackS.obj"

"$(OUTDIR)\CSD_TP_Foo_Bs.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CSD_TP_Foo_Bs.lib.manifest" mt.exe -manifest "$(OUTDIR)\CSD_TP_Foo_Bs.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.csd_tp_foo_b_lib.dep")
!INCLUDE "Makefile.csd_tp_foo_b_lib.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Foo_BC.cpp"

"$(INTDIR)\Foo_BC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Foo_BC.obj" $(SOURCE)

SOURCE="Foo_BS.cpp"

"$(INTDIR)\Foo_BS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Foo_BS.obj" $(SOURCE)

SOURCE="Foo_B_ClientEngine.cpp"

"$(INTDIR)\Foo_B_ClientEngine.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Foo_B_ClientEngine.obj" $(SOURCE)

SOURCE="Foo_B_SimpleClientEngine.cpp"

"$(INTDIR)\Foo_B_SimpleClientEngine.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Foo_B_SimpleClientEngine.obj" $(SOURCE)

SOURCE="Foo_B_i.cpp"

"$(INTDIR)\Foo_B_i.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Foo_B_i.obj" $(SOURCE)

SOURCE="Foo_B_Statistics.cpp"

"$(INTDIR)\Foo_B_Statistics.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Foo_B_Statistics.obj" $(SOURCE)

SOURCE="Callback_i.cpp"

"$(INTDIR)\Callback_i.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Callback_i.obj" $(SOURCE)

SOURCE="CallbackC.cpp"

"$(INTDIR)\CallbackC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CallbackC.obj" $(SOURCE)

SOURCE="CallbackS.cpp"

"$(INTDIR)\CallbackS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CallbackS.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="Foo_B.idl"

InputPath=Foo_B.idl

"Foo_BC.inl" "Foo_BS.inl" "Foo_BC.h" "Foo_BS.h" "Foo_BC.cpp" "Foo_BS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Foo_B_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I../../CSD_Strategy_Tests/TP_Common -b -Wb,export_macro=CSD_TP_Foo_B_Export -Wb,export_include=CSD_TP_Foo_B_Export.h "$(InputPath)"
<<

SOURCE="Callback.idl"

InputPath=Callback.idl

"CallbackC.inl" "CallbackS.inl" "CallbackC.h" "CallbackS.h" "CallbackC.cpp" "CallbackS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Callback_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I../../CSD_Strategy_Tests/TP_Common -b -Wb,export_macro=CSD_TP_Foo_B_Export -Wb,export_include=CSD_TP_Foo_B_Export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="Foo_B.idl"

InputPath=Foo_B.idl

"Foo_BC.inl" "Foo_BS.inl" "Foo_BC.h" "Foo_BS.h" "Foo_BC.cpp" "Foo_BS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Foo_B_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I../../CSD_Strategy_Tests/TP_Common -b -Wb,export_macro=CSD_TP_Foo_B_Export -Wb,export_include=CSD_TP_Foo_B_Export.h "$(InputPath)"
<<

SOURCE="Callback.idl"

InputPath=Callback.idl

"CallbackC.inl" "CallbackS.inl" "CallbackC.h" "CallbackS.h" "CallbackC.cpp" "CallbackS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Callback_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I../../CSD_Strategy_Tests/TP_Common -b -Wb,export_macro=CSD_TP_Foo_B_Export -Wb,export_include=CSD_TP_Foo_B_Export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="Foo_B.idl"

InputPath=Foo_B.idl

"Foo_BC.inl" "Foo_BS.inl" "Foo_BC.h" "Foo_BS.h" "Foo_BC.cpp" "Foo_BS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Foo_B_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I../../CSD_Strategy_Tests/TP_Common -b -Wb,export_macro=CSD_TP_Foo_B_Export -Wb,export_include=CSD_TP_Foo_B_Export.h "$(InputPath)"
<<

SOURCE="Callback.idl"

InputPath=Callback.idl

"CallbackC.inl" "CallbackS.inl" "CallbackC.h" "CallbackS.h" "CallbackC.cpp" "CallbackS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Callback_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I../../CSD_Strategy_Tests/TP_Common -b -Wb,export_macro=CSD_TP_Foo_B_Export -Wb,export_include=CSD_TP_Foo_B_Export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="Foo_B.idl"

InputPath=Foo_B.idl

"Foo_BC.inl" "Foo_BS.inl" "Foo_BC.h" "Foo_BS.h" "Foo_BC.cpp" "Foo_BS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Foo_B_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I../../CSD_Strategy_Tests/TP_Common -b -Wb,export_macro=CSD_TP_Foo_B_Export -Wb,export_include=CSD_TP_Foo_B_Export.h "$(InputPath)"
<<

SOURCE="Callback.idl"

InputPath=Callback.idl

"CallbackC.inl" "CallbackS.inl" "CallbackC.h" "CallbackS.h" "CallbackC.cpp" "CallbackS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Callback_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I../../CSD_Strategy_Tests/TP_Common -b -Wb,export_macro=CSD_TP_Foo_B_Export -Wb,export_include=CSD_TP_Foo_B_Export.h "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.csd_tp_foo_b_lib.dep")
	@echo Using "Makefile.csd_tp_foo_b_lib.dep"
!ELSE
	@echo Warning: cannot find "Makefile.csd_tp_foo_b_lib.dep"
!ENDIF
!ENDIF

