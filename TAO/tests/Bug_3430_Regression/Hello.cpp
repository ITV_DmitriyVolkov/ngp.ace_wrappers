//
// $Id: Hello.cpp 1271 2009-08-25 18:33:17Z calabrep $
//
#include "Hello.h"

ACE_RCSID(Hello, Hello, "$Id: Hello.cpp 1271 2009-08-25 18:33:17Z calabrep $")

Hello::Hello (CORBA::ORB_ptr orb)
  : orb_ (CORBA::ORB::_duplicate (orb))
{
}

void
Hello::method(Test::Hello_out r)
{
  Test::Hello_var v;
  r = v;
}

void
Hello::shutdown (void)
{
  this->orb_->shutdown (0);
}
