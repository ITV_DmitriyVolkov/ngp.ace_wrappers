#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake -value_template platforms=Win64 -make_coexistence -recurse -exclude TAO/CIAO -features qos=1,mfc=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: Object_Loader_Idl Object_Loader_Test Object_Loader_Driver

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.Object_Loader_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Object_Loader_Idl.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Object_Loader_Test.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Object_Loader_Test.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Object_Loader_Driver.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Object_Loader_Driver.mak CFG="$(CFG)" $(@)

Object_Loader_Idl:
	@echo Project: Makefile.Object_Loader_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Object_Loader_Idl.mak CFG="$(CFG)" all

Object_Loader_Test: Object_Loader_Idl
	@echo Project: Makefile.Object_Loader_Test.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Object_Loader_Test.mak CFG="$(CFG)" all

Object_Loader_Driver: Object_Loader_Idl Object_Loader_Test
	@echo Project: Makefile.Object_Loader_Driver.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Object_Loader_Driver.mak CFG="$(CFG)" all

project_name_list:
	@echo Object_Loader_Driver
	@echo Object_Loader_Idl
	@echo Object_Loader_Test
