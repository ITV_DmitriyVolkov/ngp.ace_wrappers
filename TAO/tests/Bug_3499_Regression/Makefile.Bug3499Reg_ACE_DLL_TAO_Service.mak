# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.Bug3499Reg_ACE_DLL_TAO_Service.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INTDIR=Debug\Bug3499Reg_ACE_DLL_TAO_Service\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) ".\Bug_3499_Regression_ACE_DLL_TAO_Serviced.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_DLL_TAO_SERVICE_BUILD_DLL -f "Makefile.Bug3499Reg_ACE_DLL_TAO_Service.dep" "DLL_TAO_Service.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\Bug_3499_Regression_ACE_DLL_TAO_Serviced.pdb"
	-@del /f/q ".\Bug_3499_Regression_ACE_DLL_TAO_Serviced.dll"
	-@del /f/q "$(OUTDIR)\Bug_3499_Regression_ACE_DLL_TAO_Serviced.lib"
	-@del /f/q "$(OUTDIR)\Bug_3499_Regression_ACE_DLL_TAO_Serviced.exp"
	-@del /f/q "$(OUTDIR)\Bug_3499_Regression_ACE_DLL_TAO_Serviced.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\Bug3499Reg_ACE_DLL_TAO_Service\$(NULL)" mkdir "Debug\Bug3499Reg_ACE_DLL_TAO_Service"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\.." /I "..\.." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_DLL_TAO_SERVICE_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_PortableServerd.lib Bug_3499_Regression_ACE_DLL_Serviced.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:".\Bug_3499_Regression_ACE_DLL_TAO_Serviced.pdb" /machine:IA64 /out:".\Bug_3499_Regression_ACE_DLL_TAO_Serviced.dll" /implib:"$(OUTDIR)\Bug_3499_Regression_ACE_DLL_TAO_Serviced.lib"
LINK32_OBJS= \
	"$(INTDIR)\DLL_TAO_Service.obj"

".\Bug_3499_Regression_ACE_DLL_TAO_Serviced.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist ".\Bug_3499_Regression_ACE_DLL_TAO_Serviced.dll.manifest" mt.exe -manifest ".\Bug_3499_Regression_ACE_DLL_TAO_Serviced.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=.
INTDIR=Release\Bug3499Reg_ACE_DLL_TAO_Service\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) ".\Bug_3499_Regression_ACE_DLL_TAO_Service.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_DLL_TAO_SERVICE_BUILD_DLL -f "Makefile.Bug3499Reg_ACE_DLL_TAO_Service.dep" "DLL_TAO_Service.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q ".\Bug_3499_Regression_ACE_DLL_TAO_Service.dll"
	-@del /f/q "$(OUTDIR)\Bug_3499_Regression_ACE_DLL_TAO_Service.lib"
	-@del /f/q "$(OUTDIR)\Bug_3499_Regression_ACE_DLL_TAO_Service.exp"
	-@del /f/q "$(OUTDIR)\Bug_3499_Regression_ACE_DLL_TAO_Service.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\Bug3499Reg_ACE_DLL_TAO_Service\$(NULL)" mkdir "Release\Bug3499Reg_ACE_DLL_TAO_Service"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\.." /I "..\.." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_DLL_TAO_SERVICE_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_PortableServer.lib Bug_3499_Regression_ACE_DLL_Service.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:".\Bug_3499_Regression_ACE_DLL_TAO_Service.dll" /implib:"$(OUTDIR)\Bug_3499_Regression_ACE_DLL_TAO_Service.lib"
LINK32_OBJS= \
	"$(INTDIR)\DLL_TAO_Service.obj"

".\Bug_3499_Regression_ACE_DLL_TAO_Service.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist ".\Bug_3499_Regression_ACE_DLL_TAO_Service.dll.manifest" mt.exe -manifest ".\Bug_3499_Regression_ACE_DLL_TAO_Service.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=.
INTDIR=Static_Debug\Bug3499Reg_ACE_DLL_TAO_Service\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\Bug_3499_Regression_ACE_DLL_TAO_Servicesd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Bug3499Reg_ACE_DLL_TAO_Service.dep" "DLL_TAO_Service.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\Bug_3499_Regression_ACE_DLL_TAO_Servicesd.lib"
	-@del /f/q "$(OUTDIR)\Bug_3499_Regression_ACE_DLL_TAO_Servicesd.exp"
	-@del /f/q "$(OUTDIR)\Bug_3499_Regression_ACE_DLL_TAO_Servicesd.ilk"
	-@del /f/q ".\Bug_3499_Regression_ACE_DLL_TAO_Servicesd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\Bug3499Reg_ACE_DLL_TAO_Service\$(NULL)" mkdir "Static_Debug\Bug3499Reg_ACE_DLL_TAO_Service"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd".\Bug_3499_Regression_ACE_DLL_TAO_Servicesd.pdb" /I "..\..\.." /I "..\.." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:".\Bug_3499_Regression_ACE_DLL_TAO_Servicesd.lib"
LINK32_OBJS= \
	"$(INTDIR)\DLL_TAO_Service.obj"

"$(OUTDIR)\Bug_3499_Regression_ACE_DLL_TAO_Servicesd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\Bug_3499_Regression_ACE_DLL_TAO_Servicesd.lib.manifest" mt.exe -manifest "$(OUTDIR)\Bug_3499_Regression_ACE_DLL_TAO_Servicesd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=.
INTDIR=Static_Release\Bug3499Reg_ACE_DLL_TAO_Service\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\Bug_3499_Regression_ACE_DLL_TAO_Services.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Bug3499Reg_ACE_DLL_TAO_Service.dep" "DLL_TAO_Service.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\Bug_3499_Regression_ACE_DLL_TAO_Services.lib"
	-@del /f/q "$(OUTDIR)\Bug_3499_Regression_ACE_DLL_TAO_Services.exp"
	-@del /f/q "$(OUTDIR)\Bug_3499_Regression_ACE_DLL_TAO_Services.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\Bug3499Reg_ACE_DLL_TAO_Service\$(NULL)" mkdir "Static_Release\Bug3499Reg_ACE_DLL_TAO_Service"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\.." /I "..\.." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:".\Bug_3499_Regression_ACE_DLL_TAO_Services.lib"
LINK32_OBJS= \
	"$(INTDIR)\DLL_TAO_Service.obj"

"$(OUTDIR)\Bug_3499_Regression_ACE_DLL_TAO_Services.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\Bug_3499_Regression_ACE_DLL_TAO_Services.lib.manifest" mt.exe -manifest "$(OUTDIR)\Bug_3499_Regression_ACE_DLL_TAO_Services.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Bug3499Reg_ACE_DLL_TAO_Service.dep")
!INCLUDE "Makefile.Bug3499Reg_ACE_DLL_TAO_Service.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="DLL_TAO_Service.cpp"

"$(INTDIR)\DLL_TAO_Service.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DLL_TAO_Service.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Bug3499Reg_ACE_DLL_TAO_Service.dep")
	@echo Using "Makefile.Bug3499Reg_ACE_DLL_TAO_Service.dep"
!ELSE
	@echo Warning: cannot find "Makefile.Bug3499Reg_ACE_DLL_TAO_Service.dep"
!ENDIF
!ENDIF

