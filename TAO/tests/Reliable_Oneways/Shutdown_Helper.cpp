//
// $Id: Shutdown_Helper.cpp 935 2008-12-10 21:47:27Z mitza $
//
#include "Shutdown_Helper.h"

ACE_RCSID(Reliable_Oneways, Shutdown_Helper, "$Id: Shutdown_Helper.cpp 935 2008-12-10 21:47:27Z mitza $")

Shutdown_Helper::Shutdown_Helper (CORBA::ORB_ptr orb)
  : orb_ (CORBA::ORB::_duplicate (orb))
{
}

void
Shutdown_Helper::shutdown (void)
{
  this->orb_->shutdown (0);
}
