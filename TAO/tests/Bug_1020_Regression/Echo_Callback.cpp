/**
 * @file Echo_Callback.cpp
 *
 * $Id: Echo_Callback.cpp 935 2008-12-10 21:47:27Z mitza $
 *
 * @author Carlos O'Ryan <coryan@atdesk.com>
 *
 */
#include "Echo_Callback.h"

ACE_RCSID(Notify_Crash, Echo_Callback, "$Id: Echo_Callback.cpp 935 2008-12-10 21:47:27Z mitza $")

Echo_Callback::Echo_Callback()
{
}

void
Echo_Callback::echo_payload(Test::Payload const &
                            TAO_ENV_ARG_DECL_NOT_USED)
{
  static int n = 0;
  n++;
  if(n == 30)
    ACE_OS::abort();

  ACE_Time_Value tick(0, 20000);
  ACE_OS::sleep(tick);
}

void
Echo_Callback::echo_payload_excep(Test::AMI_EchoExceptionHolder *
                                  TAO_ENV_ARG_DECL_NOT_USED)
{
}
