/**
 * @file ORB_Task.cpp
 *
 * $Id: ORB_Task.cpp 14 2007-02-01 15:49:12Z mitza $
 *
 * @author Carlos O'Ryan <coryan@atdesk.com>
 *
 */
#include "ORB_Task.h"
#include "tao/Environment.h"

ACE_RCSID(Bug_1230_Regression, ORB_Task, "$Id: ORB_Task.cpp 14 2007-02-01 15:49:12Z mitza $")

ORB_Task::ORB_Task(CORBA::ORB_ptr orb)
  : orb_(CORBA::ORB::_duplicate(orb))
{
}

int
ORB_Task::svc (void)
{
  try
    {
      this->orb_->run();
    }
  catch (const CORBA::Exception& )
    {
    }
  return 0;
}
