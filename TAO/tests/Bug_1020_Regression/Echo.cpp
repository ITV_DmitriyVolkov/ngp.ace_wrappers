/**
 * @file Echo.cpp
 *
 * $Id: Echo.cpp 935 2008-12-10 21:47:27Z mitza $
 *
 * @author Carlos O'Ryan <coryan@atdesk.com>
 *
 */
#include "Echo.h"

ACE_RCSID(Notify_Crash, Echo, "$Id: Echo.cpp 935 2008-12-10 21:47:27Z mitza $")

Echo::Echo(CORBA::ORB_ptr orb)
  : orb_(CORBA::ORB::_duplicate(orb))
{
}

Test::Payload *
Echo::echo_payload(Test::Payload const &)
{
  ACE_Time_Value tick(0, 10000);
  orb_->run(tick);

  Test::Payload_var pload(new Test::Payload);
  pload->length(1024);
  return pload._retn();
}
