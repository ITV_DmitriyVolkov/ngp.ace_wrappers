#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake -value_template platforms=Win64 -make_coexistence -recurse -exclude TAO/CIAO -features qos=1,mfc=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: LongDoubleTest_Idl LongDoubleTest_Client LongDoubleTest_Server

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.LongDoubleTest_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.LongDoubleTest_Idl.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.LongDoubleTest_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.LongDoubleTest_Client.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.LongDoubleTest_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.LongDoubleTest_Server.mak CFG="$(CFG)" $(@)

LongDoubleTest_Idl:
	@echo Project: Makefile.LongDoubleTest_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.LongDoubleTest_Idl.mak CFG="$(CFG)" all

LongDoubleTest_Client: LongDoubleTest_Idl
	@echo Project: Makefile.LongDoubleTest_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.LongDoubleTest_Client.mak CFG="$(CFG)" all

LongDoubleTest_Server: LongDoubleTest_Idl
	@echo Project: Makefile.LongDoubleTest_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.LongDoubleTest_Server.mak CFG="$(CFG)" all

project_name_list:
	@echo LongDoubleTest_Client
	@echo LongDoubleTest_Idl
	@echo LongDoubleTest_Server
