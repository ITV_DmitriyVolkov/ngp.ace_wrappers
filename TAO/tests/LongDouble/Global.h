// $Id: Global.h 979 2008-12-31 20:22:32Z mitza $
#ifndef TAO_TEST_GLOBAL_H
#define TAO_TEST_GLOBAL_H

class Global
{
public:
  static long double get_long_double (void);
};

#endif /* TAO_TEST_GLOBAL_H */

