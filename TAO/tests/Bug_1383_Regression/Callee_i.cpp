// $Id: Callee_i.cpp 979 2008-12-31 20:22:32Z mitza $

#include "Callee_i.h"

void Callee_i::callback (const char * message)
{
  ACE_DEBUG ((LM_DEBUG, "Callee_i::callback: message is %s", message));
}
