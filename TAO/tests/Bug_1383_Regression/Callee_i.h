// $Id: Callee_i.h 979 2008-12-31 20:22:32Z mitza $

#ifndef CALLEE_I_H
#define CALLEE_I_H

#include "calleeS.h"

class Callee_i : public POA_Callee {
  void callback (const char * message);
};

#endif
