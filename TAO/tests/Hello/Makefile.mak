#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake -value_template platforms=Win64 -make_coexistence -recurse -exclude TAO/CIAO -features qos=1,mfc=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: Hello_Idl Hello_Client Hello_Server

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.Hello_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Hello_Idl.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Hello_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Hello_Client.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Hello_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Hello_Server.mak CFG="$(CFG)" $(@)

Hello_Idl:
	@echo Project: Makefile.Hello_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Hello_Idl.mak CFG="$(CFG)" all

Hello_Client: Hello_Idl
	@echo Project: Makefile.Hello_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Hello_Client.mak CFG="$(CFG)" all

Hello_Server: Hello_Idl
	@echo Project: Makefile.Hello_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Hello_Server.mak CFG="$(CFG)" all

project_name_list:
	@echo Hello_Client
	@echo Hello_Idl
	@echo Hello_Server
