# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.DynAny_Test_Basic.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Console Application")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "da_testsC.inl" "da_testsS.inl" "da_testsC.h" "da_testsS.h" "da_testsC.cpp" "da_testsS.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INSTALLDIR=.
INTDIR=Debug\DynAny_Test_Basic\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\basic_test.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -f "Makefile.DynAny_Test_Basic.dep" "da_testsC.cpp" "da_testsS.cpp" "data.cpp" "driver.cpp" "analyzer.cpp" "test_dynany.cpp" "test_dynarray.cpp" "test_dynenum.cpp" "test_dynsequence.cpp" "test_dynstruct.cpp" "test_dynunion.cpp" "test_wrapper.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\basic_test.pdb"
	-@del /f/q "$(INSTALLDIR)\basic_test.exe"
	-@del /f/q "$(INSTALLDIR)\basic_test.ilk"
	-@del /f/q "da_testsC.inl"
	-@del /f/q "da_testsS.inl"
	-@del /f/q "da_testsC.h"
	-@del /f/q "da_testsS.h"
	-@del /f/q "da_testsC.cpp"
	-@del /f/q "da_testsS.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\DynAny_Test_Basic\$(NULL)" mkdir "Debug\DynAny_Test_Basic"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\.." /I "..\.." /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_PortableServerd.lib TAO_Valuetyped.lib TAO_DynamicAnyd.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\basic_test.pdb" /machine:IA64 /out:"$(INSTALLDIR)\basic_test.exe"
LINK32_OBJS= \
	"$(INTDIR)\da_testsC.obj" \
	"$(INTDIR)\da_testsS.obj" \
	"$(INTDIR)\data.obj" \
	"$(INTDIR)\driver.obj" \
	"$(INTDIR)\analyzer.obj" \
	"$(INTDIR)\test_dynany.obj" \
	"$(INTDIR)\test_dynarray.obj" \
	"$(INTDIR)\test_dynenum.obj" \
	"$(INTDIR)\test_dynsequence.obj" \
	"$(INTDIR)\test_dynstruct.obj" \
	"$(INTDIR)\test_dynunion.obj" \
	"$(INTDIR)\test_wrapper.obj"

"$(INSTALLDIR)\basic_test.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\basic_test.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\basic_test.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=Release
INSTALLDIR=Release
INTDIR=Release\DynAny_Test_Basic\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\basic_test.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -f "Makefile.DynAny_Test_Basic.dep" "da_testsC.cpp" "da_testsS.cpp" "data.cpp" "driver.cpp" "analyzer.cpp" "test_dynany.cpp" "test_dynarray.cpp" "test_dynenum.cpp" "test_dynsequence.cpp" "test_dynstruct.cpp" "test_dynunion.cpp" "test_wrapper.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\basic_test.exe"
	-@del /f/q "$(INSTALLDIR)\basic_test.ilk"
	-@del /f/q "da_testsC.inl"
	-@del /f/q "da_testsS.inl"
	-@del /f/q "da_testsC.h"
	-@del /f/q "da_testsS.h"
	-@del /f/q "da_testsC.cpp"
	-@del /f/q "da_testsS.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\DynAny_Test_Basic\$(NULL)" mkdir "Release\DynAny_Test_Basic"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\.." /I "..\.." /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_PortableServer.lib TAO_Valuetype.lib TAO_DynamicAny.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\basic_test.exe"
LINK32_OBJS= \
	"$(INTDIR)\da_testsC.obj" \
	"$(INTDIR)\da_testsS.obj" \
	"$(INTDIR)\data.obj" \
	"$(INTDIR)\driver.obj" \
	"$(INTDIR)\analyzer.obj" \
	"$(INTDIR)\test_dynany.obj" \
	"$(INTDIR)\test_dynarray.obj" \
	"$(INTDIR)\test_dynenum.obj" \
	"$(INTDIR)\test_dynsequence.obj" \
	"$(INTDIR)\test_dynstruct.obj" \
	"$(INTDIR)\test_dynunion.obj" \
	"$(INTDIR)\test_wrapper.obj"

"$(INSTALLDIR)\basic_test.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\basic_test.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\basic_test.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=Static_Debug
INSTALLDIR=Static_Debug
INTDIR=Static_Debug\DynAny_Test_Basic\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\basic_test.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.DynAny_Test_Basic.dep" "da_testsC.cpp" "da_testsS.cpp" "data.cpp" "driver.cpp" "analyzer.cpp" "test_dynany.cpp" "test_dynarray.cpp" "test_dynenum.cpp" "test_dynsequence.cpp" "test_dynstruct.cpp" "test_dynunion.cpp" "test_wrapper.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\basic_test.pdb"
	-@del /f/q "$(INSTALLDIR)\basic_test.exe"
	-@del /f/q "$(INSTALLDIR)\basic_test.ilk"
	-@del /f/q "da_testsC.inl"
	-@del /f/q "da_testsS.inl"
	-@del /f/q "da_testsC.h"
	-@del /f/q "da_testsS.h"
	-@del /f/q "da_testsC.cpp"
	-@del /f/q "da_testsS.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\DynAny_Test_Basic\$(NULL)" mkdir "Static_Debug\DynAny_Test_Basic"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\.." /I "..\.." /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEsd.lib TAOsd.lib TAO_AnyTypeCodesd.lib TAO_PortableServersd.lib TAO_Valuetypesd.lib TAO_DynamicAnysd.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\basic_test.pdb" /machine:IA64 /out:"$(INSTALLDIR)\basic_test.exe"
LINK32_OBJS= \
	"$(INTDIR)\da_testsC.obj" \
	"$(INTDIR)\da_testsS.obj" \
	"$(INTDIR)\data.obj" \
	"$(INTDIR)\driver.obj" \
	"$(INTDIR)\analyzer.obj" \
	"$(INTDIR)\test_dynany.obj" \
	"$(INTDIR)\test_dynarray.obj" \
	"$(INTDIR)\test_dynenum.obj" \
	"$(INTDIR)\test_dynsequence.obj" \
	"$(INTDIR)\test_dynstruct.obj" \
	"$(INTDIR)\test_dynunion.obj" \
	"$(INTDIR)\test_wrapper.obj"

"$(INSTALLDIR)\basic_test.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\basic_test.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\basic_test.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=Static_Release
INSTALLDIR=Static_Release
INTDIR=Static_Release\DynAny_Test_Basic\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\basic_test.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.DynAny_Test_Basic.dep" "da_testsC.cpp" "da_testsS.cpp" "data.cpp" "driver.cpp" "analyzer.cpp" "test_dynany.cpp" "test_dynarray.cpp" "test_dynenum.cpp" "test_dynsequence.cpp" "test_dynstruct.cpp" "test_dynunion.cpp" "test_wrapper.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\basic_test.exe"
	-@del /f/q "$(INSTALLDIR)\basic_test.ilk"
	-@del /f/q "da_testsC.inl"
	-@del /f/q "da_testsS.inl"
	-@del /f/q "da_testsC.h"
	-@del /f/q "da_testsS.h"
	-@del /f/q "da_testsC.cpp"
	-@del /f/q "da_testsS.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\DynAny_Test_Basic\$(NULL)" mkdir "Static_Release\DynAny_Test_Basic"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\.." /I "..\.." /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEs.lib TAOs.lib TAO_AnyTypeCodes.lib TAO_PortableServers.lib TAO_Valuetypes.lib TAO_DynamicAnys.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\basic_test.exe"
LINK32_OBJS= \
	"$(INTDIR)\da_testsC.obj" \
	"$(INTDIR)\da_testsS.obj" \
	"$(INTDIR)\data.obj" \
	"$(INTDIR)\driver.obj" \
	"$(INTDIR)\analyzer.obj" \
	"$(INTDIR)\test_dynany.obj" \
	"$(INTDIR)\test_dynarray.obj" \
	"$(INTDIR)\test_dynenum.obj" \
	"$(INTDIR)\test_dynsequence.obj" \
	"$(INTDIR)\test_dynstruct.obj" \
	"$(INTDIR)\test_dynunion.obj" \
	"$(INTDIR)\test_wrapper.obj"

"$(INSTALLDIR)\basic_test.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\basic_test.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\basic_test.exe.manifest" -outputresource:$@;1

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.DynAny_Test_Basic.dep")
!INCLUDE "Makefile.DynAny_Test_Basic.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="da_testsC.cpp"

"$(INTDIR)\da_testsC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\da_testsC.obj" $(SOURCE)

SOURCE="da_testsS.cpp"

"$(INTDIR)\da_testsS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\da_testsS.obj" $(SOURCE)

SOURCE="data.cpp"

"$(INTDIR)\data.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\data.obj" $(SOURCE)

SOURCE="driver.cpp"

"$(INTDIR)\driver.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\driver.obj" $(SOURCE)

SOURCE="analyzer.cpp"

"$(INTDIR)\analyzer.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\analyzer.obj" $(SOURCE)

SOURCE="test_dynany.cpp"

"$(INTDIR)\test_dynany.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\test_dynany.obj" $(SOURCE)

SOURCE="test_dynarray.cpp"

"$(INTDIR)\test_dynarray.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\test_dynarray.obj" $(SOURCE)

SOURCE="test_dynenum.cpp"

"$(INTDIR)\test_dynenum.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\test_dynenum.obj" $(SOURCE)

SOURCE="test_dynsequence.cpp"

"$(INTDIR)\test_dynsequence.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\test_dynsequence.obj" $(SOURCE)

SOURCE="test_dynstruct.cpp"

"$(INTDIR)\test_dynstruct.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\test_dynstruct.obj" $(SOURCE)

SOURCE="test_dynunion.cpp"

"$(INTDIR)\test_dynunion.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\test_dynunion.obj" $(SOURCE)

SOURCE="test_wrapper.cpp"

"$(INTDIR)\test_wrapper.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\test_wrapper.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="da_tests.idl"

InputPath=da_tests.idl

"da_testsC.inl" "da_testsS.inl" "da_testsC.h" "da_testsS.h" "da_testsC.cpp" "da_testsS.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-da_tests_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="da_tests.idl"

InputPath=da_tests.idl

"da_testsC.inl" "da_testsS.inl" "da_testsC.h" "da_testsS.h" "da_testsC.cpp" "da_testsS.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-da_tests_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="da_tests.idl"

InputPath=da_tests.idl

"da_testsC.inl" "da_testsS.inl" "da_testsC.h" "da_testsS.h" "da_testsC.cpp" "da_testsS.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-da_tests_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="da_tests.idl"

InputPath=da_tests.idl

"da_testsC.inl" "da_testsS.inl" "da_testsC.h" "da_testsS.h" "da_testsC.cpp" "da_testsS.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-da_tests_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.DynAny_Test_Basic.dep")
	@echo Using "Makefile.DynAny_Test_Basic.dep"
!ELSE
	@echo Warning: cannot find "Makefile.DynAny_Test_Basic.dep"
!ENDIF
!ENDIF

