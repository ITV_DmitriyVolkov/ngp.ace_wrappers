// -*- C++ -*-

#include "WUCS4_UTF16_Factory.h"

ACE_RCSID(UCS4_UTF16, WUCS4_UTF16_Factory, "$Id: WUCS4_UTF16_Factory.cpp 14 2007-02-01 15:49:12Z mitza $")

ACE_STATIC_SVC_DEFINE (WUCS4_UTF16_Factory,
                       ACE_TEXT ("WUCS4_UTF16_Factory"),
                       ACE_SVC_OBJ_T,
                       &ACE_SVC_NAME (WUCS4_UTF16_Factory),
                       ACE_Service_Type::DELETE_THIS
                       | ACE_Service_Type::DELETE_OBJ,
                       0)
ACE_FACTORY_DEFINE (UCS4_UTF16, WUCS4_UTF16_Factory)
