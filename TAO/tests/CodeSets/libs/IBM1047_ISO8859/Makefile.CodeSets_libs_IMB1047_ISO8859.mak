# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.CodeSets_libs_IMB1047_ISO8859.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INTDIR=Debug\CodeSets_libs_IMB1047_ISO8859\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) ".\IBM1047_ISO8859d.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DIBM1047_ISO8859_BUILD_DLL -f "Makefile.CodeSets_libs_IMB1047_ISO8859.dep" "Char_IBM1047_ISO8859_Factory.cpp" "Wchar_IBM1047_ISO8859_Factory.cpp" "Wchar_IBM1047_ISO8859_Translator.cpp" "Char_IBM1047_ISO8859_Translator.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\IBM1047_ISO8859d.pdb"
	-@del /f/q ".\IBM1047_ISO8859d.dll"
	-@del /f/q "$(OUTDIR)\IBM1047_ISO8859d.lib"
	-@del /f/q "$(OUTDIR)\IBM1047_ISO8859d.exp"
	-@del /f/q "$(OUTDIR)\IBM1047_ISO8859d.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\CodeSets_libs_IMB1047_ISO8859\$(NULL)" mkdir "Debug\CodeSets_libs_IMB1047_ISO8859"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\..\..\.." /I "..\..\..\.." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D IBM1047_ISO8859_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_Codesetd.lib /libpath:"." /libpath:"..\..\..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:".\IBM1047_ISO8859d.pdb" /machine:IA64 /out:".\IBM1047_ISO8859d.dll" /implib:"$(OUTDIR)\IBM1047_ISO8859d.lib"
LINK32_OBJS= \
	"$(INTDIR)\Char_IBM1047_ISO8859_Factory.obj" \
	"$(INTDIR)\Wchar_IBM1047_ISO8859_Factory.obj" \
	"$(INTDIR)\Wchar_IBM1047_ISO8859_Translator.obj" \
	"$(INTDIR)\Char_IBM1047_ISO8859_Translator.obj"

".\IBM1047_ISO8859d.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist ".\IBM1047_ISO8859d.dll.manifest" mt.exe -manifest ".\IBM1047_ISO8859d.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=.
INTDIR=Release\CodeSets_libs_IMB1047_ISO8859\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) ".\IBM1047_ISO8859.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DIBM1047_ISO8859_BUILD_DLL -f "Makefile.CodeSets_libs_IMB1047_ISO8859.dep" "Char_IBM1047_ISO8859_Factory.cpp" "Wchar_IBM1047_ISO8859_Factory.cpp" "Wchar_IBM1047_ISO8859_Translator.cpp" "Char_IBM1047_ISO8859_Translator.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q ".\IBM1047_ISO8859.dll"
	-@del /f/q "$(OUTDIR)\IBM1047_ISO8859.lib"
	-@del /f/q "$(OUTDIR)\IBM1047_ISO8859.exp"
	-@del /f/q "$(OUTDIR)\IBM1047_ISO8859.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\CodeSets_libs_IMB1047_ISO8859\$(NULL)" mkdir "Release\CodeSets_libs_IMB1047_ISO8859"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\..\.." /I "..\..\..\.." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D IBM1047_ISO8859_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_Codeset.lib /libpath:"." /libpath:"..\..\..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:".\IBM1047_ISO8859.dll" /implib:"$(OUTDIR)\IBM1047_ISO8859.lib"
LINK32_OBJS= \
	"$(INTDIR)\Char_IBM1047_ISO8859_Factory.obj" \
	"$(INTDIR)\Wchar_IBM1047_ISO8859_Factory.obj" \
	"$(INTDIR)\Wchar_IBM1047_ISO8859_Translator.obj" \
	"$(INTDIR)\Char_IBM1047_ISO8859_Translator.obj"

".\IBM1047_ISO8859.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist ".\IBM1047_ISO8859.dll.manifest" mt.exe -manifest ".\IBM1047_ISO8859.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=.
INTDIR=Static_Debug\CodeSets_libs_IMB1047_ISO8859\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\IBM1047_ISO8859sd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CodeSets_libs_IMB1047_ISO8859.dep" "Char_IBM1047_ISO8859_Factory.cpp" "Wchar_IBM1047_ISO8859_Factory.cpp" "Wchar_IBM1047_ISO8859_Translator.cpp" "Char_IBM1047_ISO8859_Translator.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\IBM1047_ISO8859sd.lib"
	-@del /f/q "$(OUTDIR)\IBM1047_ISO8859sd.exp"
	-@del /f/q "$(OUTDIR)\IBM1047_ISO8859sd.ilk"
	-@del /f/q ".\IBM1047_ISO8859sd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\CodeSets_libs_IMB1047_ISO8859\$(NULL)" mkdir "Static_Debug\CodeSets_libs_IMB1047_ISO8859"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd".\IBM1047_ISO8859sd.pdb" /I "..\..\..\..\.." /I "..\..\..\.." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:".\IBM1047_ISO8859sd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Char_IBM1047_ISO8859_Factory.obj" \
	"$(INTDIR)\Wchar_IBM1047_ISO8859_Factory.obj" \
	"$(INTDIR)\Wchar_IBM1047_ISO8859_Translator.obj" \
	"$(INTDIR)\Char_IBM1047_ISO8859_Translator.obj"

"$(OUTDIR)\IBM1047_ISO8859sd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\IBM1047_ISO8859sd.lib.manifest" mt.exe -manifest "$(OUTDIR)\IBM1047_ISO8859sd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=.
INTDIR=Static_Release\CodeSets_libs_IMB1047_ISO8859\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\IBM1047_ISO8859s.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CodeSets_libs_IMB1047_ISO8859.dep" "Char_IBM1047_ISO8859_Factory.cpp" "Wchar_IBM1047_ISO8859_Factory.cpp" "Wchar_IBM1047_ISO8859_Translator.cpp" "Char_IBM1047_ISO8859_Translator.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\IBM1047_ISO8859s.lib"
	-@del /f/q "$(OUTDIR)\IBM1047_ISO8859s.exp"
	-@del /f/q "$(OUTDIR)\IBM1047_ISO8859s.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\CodeSets_libs_IMB1047_ISO8859\$(NULL)" mkdir "Static_Release\CodeSets_libs_IMB1047_ISO8859"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\..\.." /I "..\..\..\.." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:".\IBM1047_ISO8859s.lib"
LINK32_OBJS= \
	"$(INTDIR)\Char_IBM1047_ISO8859_Factory.obj" \
	"$(INTDIR)\Wchar_IBM1047_ISO8859_Factory.obj" \
	"$(INTDIR)\Wchar_IBM1047_ISO8859_Translator.obj" \
	"$(INTDIR)\Char_IBM1047_ISO8859_Translator.obj"

"$(OUTDIR)\IBM1047_ISO8859s.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\IBM1047_ISO8859s.lib.manifest" mt.exe -manifest "$(OUTDIR)\IBM1047_ISO8859s.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CodeSets_libs_IMB1047_ISO8859.dep")
!INCLUDE "Makefile.CodeSets_libs_IMB1047_ISO8859.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Char_IBM1047_ISO8859_Factory.cpp"

"$(INTDIR)\Char_IBM1047_ISO8859_Factory.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Char_IBM1047_ISO8859_Factory.obj" $(SOURCE)

SOURCE="Wchar_IBM1047_ISO8859_Factory.cpp"

"$(INTDIR)\Wchar_IBM1047_ISO8859_Factory.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Wchar_IBM1047_ISO8859_Factory.obj" $(SOURCE)

SOURCE="Wchar_IBM1047_ISO8859_Translator.cpp"

"$(INTDIR)\Wchar_IBM1047_ISO8859_Translator.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Wchar_IBM1047_ISO8859_Translator.obj" $(SOURCE)

SOURCE="Char_IBM1047_ISO8859_Translator.cpp"

"$(INTDIR)\Char_IBM1047_ISO8859_Translator.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Char_IBM1047_ISO8859_Translator.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CodeSets_libs_IMB1047_ISO8859.dep")
	@echo Using "Makefile.CodeSets_libs_IMB1047_ISO8859.dep"
!ELSE
	@echo Warning: cannot find "Makefile.CodeSets_libs_IMB1047_ISO8859.dep"
!ENDIF
!ENDIF

