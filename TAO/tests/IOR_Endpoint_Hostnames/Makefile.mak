#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake -value_template platforms=Win64 -make_coexistence -recurse -exclude TAO/CIAO -features qos=1,mfc=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: generate_ior list_interfaces

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.generate_ior.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.generate_ior.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.list_interfaces.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.list_interfaces.mak CFG="$(CFG)" $(@)

generate_ior:
	@echo Project: Makefile.generate_ior.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.generate_ior.mak CFG="$(CFG)" all

list_interfaces:
	@echo Project: Makefile.list_interfaces.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.list_interfaces.mak CFG="$(CFG)" all

project_name_list:
	@echo generate_ior
	@echo list_interfaces
