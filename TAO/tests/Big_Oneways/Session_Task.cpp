//
// $Id: Session_Task.cpp 14 2007-02-01 15:49:12Z mitza $
//

#include "Session_Task.h"
#include "Session.h"

ACE_RCSID(Big_Oneways, Session_Task, "$Id: Session_Task.cpp 14 2007-02-01 15:49:12Z mitza $")

Session_Task::Session_Task (Session *session)
  : session_ (session)
{
}

int
Session_Task::svc (void)
{
  return this->session_->svc ();
}
