//
// $Id: HelloWorld.cpp 935 2008-12-10 21:47:27Z mitza $
//

#include "HelloWorld.h"


ACE_RCSID (ICMG_Any_Bug, HelloWorld, "$Id: HelloWorld.cpp 935 2008-12-10 21:47:27Z mitza $")


HelloWorld::HelloWorld (void)
{
}

char *
HelloWorld::get_string (void)
{
  return CORBA::string_dup ("TAO Any Implementation Works!");
}
