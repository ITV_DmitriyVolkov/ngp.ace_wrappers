//
// $Id: Client_Task.cpp 14 2007-02-01 15:49:12Z mitza $
//

#include "Client_Task.h"

ACE_RCSID(Muxing, Client_Task, "$Id: Client_Task.cpp 14 2007-02-01 15:49:12Z mitza $")

Client_Task::Client_Task (CORBA::ORB_ptr orb)
  : orb_ (CORBA::ORB::_duplicate (orb))
{
}

int
Client_Task::svc (void)
{

  try
    {
      this->orb_->run ();
    }
  catch (const CORBA::Exception&)
    {
      return -1;
    }

  return 0;
}
