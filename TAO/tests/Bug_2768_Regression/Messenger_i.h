// $Id: Messenger_i.h 979 2008-12-31 20:22:32Z mitza $

#ifndef __MESSENGER_I_H__
#define __MESSENGER_I_H__

#include "MessengerS.h"

class Messenger_i : public POA_SimpleMessenger::Messenger
{
public:
  Messenger_i (CORBA::ORB_ptr orb);
  virtual void send (void);

private:
  /// Use an ORB reference to conver strings to objects and shutdown
  /// the application.
  CORBA::ORB_var orb_;
};

#endif
