#include "test_i.h"


ACE_RCSID (ORB_Shutdown,
           test_i,
           "$Id: test_i.cpp 935 2008-12-10 21:47:27Z mitza $")


test_i::test_i (CORBA::ORB_ptr orb)
  : orb_ (CORBA::ORB::_duplicate (orb))
{
}

test_i::~test_i (void)
{
}

void
test_i::hello (void)
{
  ACE_DEBUG ((LM_DEBUG, "Hello!\n"));
}

void
test_i::shutdown (void)
{
  ACE_DEBUG ((LM_DEBUG,
              "Shutting down ORB.\n"));

  this->orb_->shutdown (0);
}
