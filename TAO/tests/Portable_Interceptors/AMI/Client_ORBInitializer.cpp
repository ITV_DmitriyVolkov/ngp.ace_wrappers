/**
 * @file Client_ORBInitializer.cpp
 *
 * $Id: Client_ORBInitializer.cpp 935 2008-12-10 21:47:27Z mitza $
 *
 * @author Carlos O'Ryan <coryan@atdesk.com>
 */

#include "Client_ORBInitializer.h"
#include "Client_Interceptor.h"

ACE_RCSID(Portable_Interceptor_AMI, Client_ORBInitializer, "$Id: Client_ORBInitializer.cpp 935 2008-12-10 21:47:27Z mitza $")

Client_ORBInitializer::Client_ORBInitializer ()
{
}

void
Client_ORBInitializer::pre_init (
    PortableInterceptor::ORBInitInfo_ptr)
{
}

void
Client_ORBInitializer::post_init (
    PortableInterceptor::ORBInitInfo_ptr info)
{
  PortableInterceptor::ClientRequestInterceptor_var interceptor(
      new Echo_Client_Request_Interceptor);

  info->add_client_request_interceptor (interceptor.in ());
}
