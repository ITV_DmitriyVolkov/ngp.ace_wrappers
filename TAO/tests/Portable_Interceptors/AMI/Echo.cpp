/**
 * @file Echo.cpp
 *
 * $Id: Echo.cpp 935 2008-12-10 21:47:27Z mitza $
 *
 * @author Carlos O'Ryan <coryan@atdesk.com>
 */
#include "Echo.h"

ACE_RCSID(Portable_Interceptors_AMI, Echo, "$Id: Echo.cpp 935 2008-12-10 21:47:27Z mitza $")

Echo::Echo(CORBA::ORB_ptr orb)
  : orb_(CORBA::ORB::_duplicate(orb))
{
}

char *
Echo::echo_operation(char const * the_input)
{
  return CORBA::string_dup(the_input);
}

void
Echo::shutdown(void)
{
  this->orb_->shutdown(0);
}
