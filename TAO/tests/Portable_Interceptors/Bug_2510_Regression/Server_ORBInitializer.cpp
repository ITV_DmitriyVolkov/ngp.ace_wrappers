// -*- C++ -*-
//
// $Id: Server_ORBInitializer.cpp 935 2008-12-10 21:47:27Z mitza $
//

#include "Server_ORBInitializer.h"
extern PortableInterceptor::SlotId slotId;

ACE_RCSID (Bug_2510,
           Server_ORBInitializer,
           "$Id: Server_ORBInitializer.cpp 935 2008-12-10 21:47:27Z mitza $")

Server_ORBInitializer::Server_ORBInitializer (void)
{
}

void
Server_ORBInitializer::pre_init (
    PortableInterceptor::ORBInitInfo_ptr)
{
}

void
Server_ORBInitializer::post_init (
    PortableInterceptor::ORBInitInfo_ptr info)
{
  // allocate a slot
  slotId = info->allocate_slot_id ();
  ACE_DEBUG ((LM_DEBUG, "Server_ORBInitializer::post_init() : allocated slot id: %d\n", slotId));

  // Install the Echo server request interceptor
  ACE_NEW_THROW_EX (this->server_interceptor_,
                    Echo_Server_Request_Interceptor,
                    CORBA::NO_MEMORY ());

  PortableInterceptor::ServerRequestInterceptor_var interceptor =
    this->server_interceptor_;

  info->add_server_request_interceptor (interceptor.in ());
}

Echo_Server_Request_Interceptor *
Server_ORBInitializer::server_interceptor (void)
{
  return this->server_interceptor_;
}

