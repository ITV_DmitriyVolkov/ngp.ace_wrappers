// -*- C++ -*-
//
// $Id: Server_ORBInitializer.cpp 935 2008-12-10 21:47:27Z mitza $
//

#include "Server_ORBInitializer.h"
#include "server_interceptor.h"

ACE_RCSID (Service_Context_Manipulation, Server_ORBInitializer, "$Id: Server_ORBInitializer.cpp 935 2008-12-10 21:47:27Z mitza $")

Server_ORBInitializer::Server_ORBInitializer (void)
{
}

void
Server_ORBInitializer::pre_init (
    PortableInterceptor::ORBInitInfo_ptr)
{
}

void
Server_ORBInitializer::post_init (
    PortableInterceptor::ORBInitInfo_ptr info)
{
  PortableInterceptor::ServerRequestInterceptor_ptr interceptor =
    PortableInterceptor::ServerRequestInterceptor::_nil ();

  // Install the Echo server request interceptor
  ACE_NEW_THROW_EX (interceptor,
                    Echo_Server_Request_Interceptor,
                    CORBA::NO_MEMORY ());

  PortableInterceptor::ServerRequestInterceptor_var
    server_interceptor = interceptor;

  info->add_server_request_interceptor (server_interceptor.in ());
}

