// -*- C++ -*-

#include "test_i.h"

ACE_RCSID (Redirection,
           test_i,
           "$Id: test_i.cpp 979 2008-12-31 20:22:32Z mitza $")

test_i::test_i (CORBA::Short num,
                CORBA::ORB_ptr orb)
  : number_ (num),
    orb_ (CORBA::ORB::_duplicate (orb))
{
}

test_i::~test_i (void)
{
}

CORBA::Short
test_i::number (void)
{
  return this->number_;
}

void
test_i::shutdown (void)
{
  ACE_DEBUG ((LM_DEBUG,
              "Server is shutting down via object %d.\n",
              this->number_));
  this->orb_->shutdown (0);
}
