#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake -value_template platforms=Win64 -make_coexistence -recurse -exclude TAO/CIAO -features qos=1,mfc=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: Muxed_GIOP_Versions_Idl Muxed_GIOP_Versions_Client Muxed_GIOP_Versions_Server

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.Muxed_GIOP_Versions_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Muxed_GIOP_Versions_Idl.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Muxed_GIOP_Versions_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Muxed_GIOP_Versions_Client.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Muxed_GIOP_Versions_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Muxed_GIOP_Versions_Server.mak CFG="$(CFG)" $(@)

Muxed_GIOP_Versions_Idl:
	@echo Project: Makefile.Muxed_GIOP_Versions_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Muxed_GIOP_Versions_Idl.mak CFG="$(CFG)" all

Muxed_GIOP_Versions_Client: Muxed_GIOP_Versions_Idl
	@echo Project: Makefile.Muxed_GIOP_Versions_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Muxed_GIOP_Versions_Client.mak CFG="$(CFG)" all

Muxed_GIOP_Versions_Server: Muxed_GIOP_Versions_Idl
	@echo Project: Makefile.Muxed_GIOP_Versions_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Muxed_GIOP_Versions_Server.mak CFG="$(CFG)" all

project_name_list:
	@echo Muxed_GIOP_Versions_Client
	@echo Muxed_GIOP_Versions_Idl
	@echo Muxed_GIOP_Versions_Server
