// $Id: Client_ORBInitializer.cpp 1805 2011-02-17 14:24:00Z mesnierp $

#include "Client_ORBInitializer.h"
#include "client_interceptor.h"


ACE_RCSID (Service_Context_Manipulation, Client_ORBInitializer, "$Id: Client_ORBInitializer.cpp 1805 2011-02-17 14:24:00Z mesnierp $")

Client_ORBInitializer::Client_ORBInitializer ()
{ 

}

void
Client_ORBInitializer::pre_init (
    PortableInterceptor::ORBInitInfo_ptr )
{
}

void
Client_ORBInitializer::post_init (
    PortableInterceptor::ORBInitInfo_ptr info)
{
  PortableInterceptor::ClientRequestInterceptor_ptr interceptor =
    PortableInterceptor::ClientRequestInterceptor::_nil ();

  // Install the Echo client request interceptor
  ACE_NEW_THROW_EX (interceptor,
                    Echo_Client_Request_Interceptor (),
                    CORBA::NO_MEMORY ());

  PortableInterceptor::ClientRequestInterceptor_var
    client_interceptor = interceptor;

  info->add_client_request_interceptor (client_interceptor.in ());
}

