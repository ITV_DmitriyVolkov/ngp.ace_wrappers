# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.Param_Test_Client.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Console Application")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INSTALLDIR=.
INTDIR=Debug\Param_Test_Client\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\client.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -f "Makefile.Param_Test_Client.dep" "param_testC.cpp" "any.cpp" "bd_array_seq.cpp" "bd_long_seq.cpp" "bd_short_seq.cpp" "bd_string.cpp" "bd_struct_seq.cpp" "bd_str_seq.cpp" "bd_wstring.cpp" "bd_wstr_seq.cpp" "big_union.cpp" "client.cpp" "complex_any.cpp" "driver.cpp" "except.cpp" "fixed_array.cpp" "fixed_struct.cpp" "helper.cpp" "multdim_array.cpp" "nested_struct.cpp" "objref.cpp" "objref_struct.cpp" "options.cpp" "recursive_struct.cpp" "recursive_union.cpp" "results.cpp" "short.cpp" "small_union.cpp" "typecode.cpp" "ub_any_seq.cpp" "ub_array_seq.cpp" "ub_long_seq.cpp" "ub_objref_seq.cpp" "ub_short_seq.cpp" "ub_string.cpp" "ub_struct_seq.cpp" "ub_str_seq.cpp" "ub_wstring.cpp" "ub_wstr_seq.cpp" "ulonglong.cpp" "var_array.cpp" "var_struct.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\client.pdb"
	-@del /f/q "$(INSTALLDIR)\client.exe"
	-@del /f/q "$(INSTALLDIR)\client.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\Param_Test_Client\$(NULL)" mkdir "Debug\Param_Test_Client"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\.." /I "..\.." /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_PortableServerd.lib TAO_Codesetd.lib TAO_Valuetyped.lib TAO_CodecFactoryd.lib TAO_PId.lib TAO_Messagingd.lib TAO_DynamicInterfaced.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\client.pdb" /machine:IA64 /out:"$(INSTALLDIR)\client.exe"
LINK32_OBJS= \
	"$(INTDIR)\param_testC.obj" \
	"$(INTDIR)\any.obj" \
	"$(INTDIR)\bd_array_seq.obj" \
	"$(INTDIR)\bd_long_seq.obj" \
	"$(INTDIR)\bd_short_seq.obj" \
	"$(INTDIR)\bd_string.obj" \
	"$(INTDIR)\bd_struct_seq.obj" \
	"$(INTDIR)\bd_str_seq.obj" \
	"$(INTDIR)\bd_wstring.obj" \
	"$(INTDIR)\bd_wstr_seq.obj" \
	"$(INTDIR)\big_union.obj" \
	"$(INTDIR)\client.obj" \
	"$(INTDIR)\complex_any.obj" \
	"$(INTDIR)\driver.obj" \
	"$(INTDIR)\except.obj" \
	"$(INTDIR)\fixed_array.obj" \
	"$(INTDIR)\fixed_struct.obj" \
	"$(INTDIR)\helper.obj" \
	"$(INTDIR)\multdim_array.obj" \
	"$(INTDIR)\nested_struct.obj" \
	"$(INTDIR)\objref.obj" \
	"$(INTDIR)\objref_struct.obj" \
	"$(INTDIR)\options.obj" \
	"$(INTDIR)\recursive_struct.obj" \
	"$(INTDIR)\recursive_union.obj" \
	"$(INTDIR)\results.obj" \
	"$(INTDIR)\short.obj" \
	"$(INTDIR)\small_union.obj" \
	"$(INTDIR)\typecode.obj" \
	"$(INTDIR)\ub_any_seq.obj" \
	"$(INTDIR)\ub_array_seq.obj" \
	"$(INTDIR)\ub_long_seq.obj" \
	"$(INTDIR)\ub_objref_seq.obj" \
	"$(INTDIR)\ub_short_seq.obj" \
	"$(INTDIR)\ub_string.obj" \
	"$(INTDIR)\ub_struct_seq.obj" \
	"$(INTDIR)\ub_str_seq.obj" \
	"$(INTDIR)\ub_wstring.obj" \
	"$(INTDIR)\ub_wstr_seq.obj" \
	"$(INTDIR)\ulonglong.obj" \
	"$(INTDIR)\var_array.obj" \
	"$(INTDIR)\var_struct.obj"

"$(INSTALLDIR)\client.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\client.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\client.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=Release
INSTALLDIR=Release
INTDIR=Release\Param_Test_Client\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\client.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -f "Makefile.Param_Test_Client.dep" "param_testC.cpp" "any.cpp" "bd_array_seq.cpp" "bd_long_seq.cpp" "bd_short_seq.cpp" "bd_string.cpp" "bd_struct_seq.cpp" "bd_str_seq.cpp" "bd_wstring.cpp" "bd_wstr_seq.cpp" "big_union.cpp" "client.cpp" "complex_any.cpp" "driver.cpp" "except.cpp" "fixed_array.cpp" "fixed_struct.cpp" "helper.cpp" "multdim_array.cpp" "nested_struct.cpp" "objref.cpp" "objref_struct.cpp" "options.cpp" "recursive_struct.cpp" "recursive_union.cpp" "results.cpp" "short.cpp" "small_union.cpp" "typecode.cpp" "ub_any_seq.cpp" "ub_array_seq.cpp" "ub_long_seq.cpp" "ub_objref_seq.cpp" "ub_short_seq.cpp" "ub_string.cpp" "ub_struct_seq.cpp" "ub_str_seq.cpp" "ub_wstring.cpp" "ub_wstr_seq.cpp" "ulonglong.cpp" "var_array.cpp" "var_struct.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\client.exe"
	-@del /f/q "$(INSTALLDIR)\client.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\Param_Test_Client\$(NULL)" mkdir "Release\Param_Test_Client"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\.." /I "..\.." /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_PortableServer.lib TAO_Codeset.lib TAO_Valuetype.lib TAO_CodecFactory.lib TAO_PI.lib TAO_Messaging.lib TAO_DynamicInterface.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\client.exe"
LINK32_OBJS= \
	"$(INTDIR)\param_testC.obj" \
	"$(INTDIR)\any.obj" \
	"$(INTDIR)\bd_array_seq.obj" \
	"$(INTDIR)\bd_long_seq.obj" \
	"$(INTDIR)\bd_short_seq.obj" \
	"$(INTDIR)\bd_string.obj" \
	"$(INTDIR)\bd_struct_seq.obj" \
	"$(INTDIR)\bd_str_seq.obj" \
	"$(INTDIR)\bd_wstring.obj" \
	"$(INTDIR)\bd_wstr_seq.obj" \
	"$(INTDIR)\big_union.obj" \
	"$(INTDIR)\client.obj" \
	"$(INTDIR)\complex_any.obj" \
	"$(INTDIR)\driver.obj" \
	"$(INTDIR)\except.obj" \
	"$(INTDIR)\fixed_array.obj" \
	"$(INTDIR)\fixed_struct.obj" \
	"$(INTDIR)\helper.obj" \
	"$(INTDIR)\multdim_array.obj" \
	"$(INTDIR)\nested_struct.obj" \
	"$(INTDIR)\objref.obj" \
	"$(INTDIR)\objref_struct.obj" \
	"$(INTDIR)\options.obj" \
	"$(INTDIR)\recursive_struct.obj" \
	"$(INTDIR)\recursive_union.obj" \
	"$(INTDIR)\results.obj" \
	"$(INTDIR)\short.obj" \
	"$(INTDIR)\small_union.obj" \
	"$(INTDIR)\typecode.obj" \
	"$(INTDIR)\ub_any_seq.obj" \
	"$(INTDIR)\ub_array_seq.obj" \
	"$(INTDIR)\ub_long_seq.obj" \
	"$(INTDIR)\ub_objref_seq.obj" \
	"$(INTDIR)\ub_short_seq.obj" \
	"$(INTDIR)\ub_string.obj" \
	"$(INTDIR)\ub_struct_seq.obj" \
	"$(INTDIR)\ub_str_seq.obj" \
	"$(INTDIR)\ub_wstring.obj" \
	"$(INTDIR)\ub_wstr_seq.obj" \
	"$(INTDIR)\ulonglong.obj" \
	"$(INTDIR)\var_array.obj" \
	"$(INTDIR)\var_struct.obj"

"$(INSTALLDIR)\client.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\client.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\client.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=Static_Debug
INSTALLDIR=Static_Debug
INTDIR=Static_Debug\Param_Test_Client\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\client.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Param_Test_Client.dep" "param_testC.cpp" "any.cpp" "bd_array_seq.cpp" "bd_long_seq.cpp" "bd_short_seq.cpp" "bd_string.cpp" "bd_struct_seq.cpp" "bd_str_seq.cpp" "bd_wstring.cpp" "bd_wstr_seq.cpp" "big_union.cpp" "client.cpp" "complex_any.cpp" "driver.cpp" "except.cpp" "fixed_array.cpp" "fixed_struct.cpp" "helper.cpp" "multdim_array.cpp" "nested_struct.cpp" "objref.cpp" "objref_struct.cpp" "options.cpp" "recursive_struct.cpp" "recursive_union.cpp" "results.cpp" "short.cpp" "small_union.cpp" "typecode.cpp" "ub_any_seq.cpp" "ub_array_seq.cpp" "ub_long_seq.cpp" "ub_objref_seq.cpp" "ub_short_seq.cpp" "ub_string.cpp" "ub_struct_seq.cpp" "ub_str_seq.cpp" "ub_wstring.cpp" "ub_wstr_seq.cpp" "ulonglong.cpp" "var_array.cpp" "var_struct.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\client.pdb"
	-@del /f/q "$(INSTALLDIR)\client.exe"
	-@del /f/q "$(INSTALLDIR)\client.ilk"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\Param_Test_Client\$(NULL)" mkdir "Static_Debug\Param_Test_Client"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\.." /I "..\.." /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEsd.lib TAOsd.lib TAO_AnyTypeCodesd.lib TAO_PortableServersd.lib TAO_Codesetsd.lib TAO_Valuetypesd.lib TAO_CodecFactorysd.lib TAO_PIsd.lib TAO_Messagingsd.lib TAO_DynamicInterfacesd.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\client.pdb" /machine:IA64 /out:"$(INSTALLDIR)\client.exe"
LINK32_OBJS= \
	"$(INTDIR)\param_testC.obj" \
	"$(INTDIR)\any.obj" \
	"$(INTDIR)\bd_array_seq.obj" \
	"$(INTDIR)\bd_long_seq.obj" \
	"$(INTDIR)\bd_short_seq.obj" \
	"$(INTDIR)\bd_string.obj" \
	"$(INTDIR)\bd_struct_seq.obj" \
	"$(INTDIR)\bd_str_seq.obj" \
	"$(INTDIR)\bd_wstring.obj" \
	"$(INTDIR)\bd_wstr_seq.obj" \
	"$(INTDIR)\big_union.obj" \
	"$(INTDIR)\client.obj" \
	"$(INTDIR)\complex_any.obj" \
	"$(INTDIR)\driver.obj" \
	"$(INTDIR)\except.obj" \
	"$(INTDIR)\fixed_array.obj" \
	"$(INTDIR)\fixed_struct.obj" \
	"$(INTDIR)\helper.obj" \
	"$(INTDIR)\multdim_array.obj" \
	"$(INTDIR)\nested_struct.obj" \
	"$(INTDIR)\objref.obj" \
	"$(INTDIR)\objref_struct.obj" \
	"$(INTDIR)\options.obj" \
	"$(INTDIR)\recursive_struct.obj" \
	"$(INTDIR)\recursive_union.obj" \
	"$(INTDIR)\results.obj" \
	"$(INTDIR)\short.obj" \
	"$(INTDIR)\small_union.obj" \
	"$(INTDIR)\typecode.obj" \
	"$(INTDIR)\ub_any_seq.obj" \
	"$(INTDIR)\ub_array_seq.obj" \
	"$(INTDIR)\ub_long_seq.obj" \
	"$(INTDIR)\ub_objref_seq.obj" \
	"$(INTDIR)\ub_short_seq.obj" \
	"$(INTDIR)\ub_string.obj" \
	"$(INTDIR)\ub_struct_seq.obj" \
	"$(INTDIR)\ub_str_seq.obj" \
	"$(INTDIR)\ub_wstring.obj" \
	"$(INTDIR)\ub_wstr_seq.obj" \
	"$(INTDIR)\ulonglong.obj" \
	"$(INTDIR)\var_array.obj" \
	"$(INTDIR)\var_struct.obj"

"$(INSTALLDIR)\client.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\client.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\client.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=Static_Release
INSTALLDIR=Static_Release
INTDIR=Static_Release\Param_Test_Client\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\client.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Param_Test_Client.dep" "param_testC.cpp" "any.cpp" "bd_array_seq.cpp" "bd_long_seq.cpp" "bd_short_seq.cpp" "bd_string.cpp" "bd_struct_seq.cpp" "bd_str_seq.cpp" "bd_wstring.cpp" "bd_wstr_seq.cpp" "big_union.cpp" "client.cpp" "complex_any.cpp" "driver.cpp" "except.cpp" "fixed_array.cpp" "fixed_struct.cpp" "helper.cpp" "multdim_array.cpp" "nested_struct.cpp" "objref.cpp" "objref_struct.cpp" "options.cpp" "recursive_struct.cpp" "recursive_union.cpp" "results.cpp" "short.cpp" "small_union.cpp" "typecode.cpp" "ub_any_seq.cpp" "ub_array_seq.cpp" "ub_long_seq.cpp" "ub_objref_seq.cpp" "ub_short_seq.cpp" "ub_string.cpp" "ub_struct_seq.cpp" "ub_str_seq.cpp" "ub_wstring.cpp" "ub_wstr_seq.cpp" "ulonglong.cpp" "var_array.cpp" "var_struct.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\client.exe"
	-@del /f/q "$(INSTALLDIR)\client.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\Param_Test_Client\$(NULL)" mkdir "Static_Release\Param_Test_Client"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\.." /I "..\.." /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEs.lib TAOs.lib TAO_AnyTypeCodes.lib TAO_PortableServers.lib TAO_Codesets.lib TAO_Valuetypes.lib TAO_CodecFactorys.lib TAO_PIs.lib TAO_Messagings.lib TAO_DynamicInterfaces.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\client.exe"
LINK32_OBJS= \
	"$(INTDIR)\param_testC.obj" \
	"$(INTDIR)\any.obj" \
	"$(INTDIR)\bd_array_seq.obj" \
	"$(INTDIR)\bd_long_seq.obj" \
	"$(INTDIR)\bd_short_seq.obj" \
	"$(INTDIR)\bd_string.obj" \
	"$(INTDIR)\bd_struct_seq.obj" \
	"$(INTDIR)\bd_str_seq.obj" \
	"$(INTDIR)\bd_wstring.obj" \
	"$(INTDIR)\bd_wstr_seq.obj" \
	"$(INTDIR)\big_union.obj" \
	"$(INTDIR)\client.obj" \
	"$(INTDIR)\complex_any.obj" \
	"$(INTDIR)\driver.obj" \
	"$(INTDIR)\except.obj" \
	"$(INTDIR)\fixed_array.obj" \
	"$(INTDIR)\fixed_struct.obj" \
	"$(INTDIR)\helper.obj" \
	"$(INTDIR)\multdim_array.obj" \
	"$(INTDIR)\nested_struct.obj" \
	"$(INTDIR)\objref.obj" \
	"$(INTDIR)\objref_struct.obj" \
	"$(INTDIR)\options.obj" \
	"$(INTDIR)\recursive_struct.obj" \
	"$(INTDIR)\recursive_union.obj" \
	"$(INTDIR)\results.obj" \
	"$(INTDIR)\short.obj" \
	"$(INTDIR)\small_union.obj" \
	"$(INTDIR)\typecode.obj" \
	"$(INTDIR)\ub_any_seq.obj" \
	"$(INTDIR)\ub_array_seq.obj" \
	"$(INTDIR)\ub_long_seq.obj" \
	"$(INTDIR)\ub_objref_seq.obj" \
	"$(INTDIR)\ub_short_seq.obj" \
	"$(INTDIR)\ub_string.obj" \
	"$(INTDIR)\ub_struct_seq.obj" \
	"$(INTDIR)\ub_str_seq.obj" \
	"$(INTDIR)\ub_wstring.obj" \
	"$(INTDIR)\ub_wstr_seq.obj" \
	"$(INTDIR)\ulonglong.obj" \
	"$(INTDIR)\var_array.obj" \
	"$(INTDIR)\var_struct.obj"

"$(INSTALLDIR)\client.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\client.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\client.exe.manifest" -outputresource:$@;1

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Param_Test_Client.dep")
!INCLUDE "Makefile.Param_Test_Client.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="param_testC.cpp"

"$(INTDIR)\param_testC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\param_testC.obj" $(SOURCE)

SOURCE="any.cpp"

"$(INTDIR)\any.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\any.obj" $(SOURCE)

SOURCE="bd_array_seq.cpp"

"$(INTDIR)\bd_array_seq.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\bd_array_seq.obj" $(SOURCE)

SOURCE="bd_long_seq.cpp"

"$(INTDIR)\bd_long_seq.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\bd_long_seq.obj" $(SOURCE)

SOURCE="bd_short_seq.cpp"

"$(INTDIR)\bd_short_seq.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\bd_short_seq.obj" $(SOURCE)

SOURCE="bd_string.cpp"

"$(INTDIR)\bd_string.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\bd_string.obj" $(SOURCE)

SOURCE="bd_struct_seq.cpp"

"$(INTDIR)\bd_struct_seq.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\bd_struct_seq.obj" $(SOURCE)

SOURCE="bd_str_seq.cpp"

"$(INTDIR)\bd_str_seq.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\bd_str_seq.obj" $(SOURCE)

SOURCE="bd_wstring.cpp"

"$(INTDIR)\bd_wstring.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\bd_wstring.obj" $(SOURCE)

SOURCE="bd_wstr_seq.cpp"

"$(INTDIR)\bd_wstr_seq.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\bd_wstr_seq.obj" $(SOURCE)

SOURCE="big_union.cpp"

"$(INTDIR)\big_union.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\big_union.obj" $(SOURCE)

SOURCE="client.cpp"

"$(INTDIR)\client.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\client.obj" $(SOURCE)

SOURCE="complex_any.cpp"

"$(INTDIR)\complex_any.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\complex_any.obj" $(SOURCE)

SOURCE="driver.cpp"

"$(INTDIR)\driver.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\driver.obj" $(SOURCE)

SOURCE="except.cpp"

"$(INTDIR)\except.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\except.obj" $(SOURCE)

SOURCE="fixed_array.cpp"

"$(INTDIR)\fixed_array.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\fixed_array.obj" $(SOURCE)

SOURCE="fixed_struct.cpp"

"$(INTDIR)\fixed_struct.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\fixed_struct.obj" $(SOURCE)

SOURCE="helper.cpp"

"$(INTDIR)\helper.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\helper.obj" $(SOURCE)

SOURCE="multdim_array.cpp"

"$(INTDIR)\multdim_array.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\multdim_array.obj" $(SOURCE)

SOURCE="nested_struct.cpp"

"$(INTDIR)\nested_struct.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\nested_struct.obj" $(SOURCE)

SOURCE="objref.cpp"

"$(INTDIR)\objref.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\objref.obj" $(SOURCE)

SOURCE="objref_struct.cpp"

"$(INTDIR)\objref_struct.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\objref_struct.obj" $(SOURCE)

SOURCE="options.cpp"

"$(INTDIR)\options.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\options.obj" $(SOURCE)

SOURCE="recursive_struct.cpp"

"$(INTDIR)\recursive_struct.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\recursive_struct.obj" $(SOURCE)

SOURCE="recursive_union.cpp"

"$(INTDIR)\recursive_union.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\recursive_union.obj" $(SOURCE)

SOURCE="results.cpp"

"$(INTDIR)\results.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\results.obj" $(SOURCE)

SOURCE="short.cpp"

"$(INTDIR)\short.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\short.obj" $(SOURCE)

SOURCE="small_union.cpp"

"$(INTDIR)\small_union.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\small_union.obj" $(SOURCE)

SOURCE="typecode.cpp"

"$(INTDIR)\typecode.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\typecode.obj" $(SOURCE)

SOURCE="ub_any_seq.cpp"

"$(INTDIR)\ub_any_seq.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ub_any_seq.obj" $(SOURCE)

SOURCE="ub_array_seq.cpp"

"$(INTDIR)\ub_array_seq.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ub_array_seq.obj" $(SOURCE)

SOURCE="ub_long_seq.cpp"

"$(INTDIR)\ub_long_seq.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ub_long_seq.obj" $(SOURCE)

SOURCE="ub_objref_seq.cpp"

"$(INTDIR)\ub_objref_seq.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ub_objref_seq.obj" $(SOURCE)

SOURCE="ub_short_seq.cpp"

"$(INTDIR)\ub_short_seq.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ub_short_seq.obj" $(SOURCE)

SOURCE="ub_string.cpp"

"$(INTDIR)\ub_string.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ub_string.obj" $(SOURCE)

SOURCE="ub_struct_seq.cpp"

"$(INTDIR)\ub_struct_seq.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ub_struct_seq.obj" $(SOURCE)

SOURCE="ub_str_seq.cpp"

"$(INTDIR)\ub_str_seq.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ub_str_seq.obj" $(SOURCE)

SOURCE="ub_wstring.cpp"

"$(INTDIR)\ub_wstring.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ub_wstring.obj" $(SOURCE)

SOURCE="ub_wstr_seq.cpp"

"$(INTDIR)\ub_wstr_seq.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ub_wstr_seq.obj" $(SOURCE)

SOURCE="ulonglong.cpp"

"$(INTDIR)\ulonglong.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ulonglong.obj" $(SOURCE)

SOURCE="var_array.cpp"

"$(INTDIR)\var_array.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\var_array.obj" $(SOURCE)

SOURCE="var_struct.cpp"

"$(INTDIR)\var_struct.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\var_struct.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Param_Test_Client.dep")
	@echo Using "Makefile.Param_Test_Client.dep"
!ELSE
	@echo Warning: cannot find "Makefile.Param_Test_Client.dep"
!ENDIF
!ENDIF

