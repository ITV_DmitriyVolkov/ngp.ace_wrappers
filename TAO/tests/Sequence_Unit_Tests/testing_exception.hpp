#ifndef guard_testing_exception_hpp
#define guard_testing_exception_hpp
/**
 * @file
 *
 * @brief Simple exception to raise in the tests.
 *
 * $Id: testing_exception.hpp 14 2007-02-01 15:49:12Z mitza $
 *
 * @author Carlos O'Ryan
 */

struct testing_exception {};

#endif // guard_testing_exception_hpp
