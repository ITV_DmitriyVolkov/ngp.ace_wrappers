#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake -value_template platforms=Win64 -make_coexistence -recurse -exclude TAO/CIAO -features qos=1,mfc=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: Sequence_Unit_Tests_B_Obj_Ref_Seq Sequence_Unit_Tests_B_Sequence_CDR Sequence_Unit_Tests_B_Simple_Types Sequence_Unit_Tests_B_String_Seq Sequence_Unit_Tests_B_Value_Sequence Sequence_Unit_Tests_Bounded_String Sequence_Unit_Tests_Obj_Ref_Seq_Elem Sequence_Unit_Tests_String_Seq_Elem Sequence_Unit_Tests_Test_Alloc_Traits Sequence_Unit_Tests_UB_Fwd_Ob_Ref_Seq Sequence_Unit_Tests_UB_Obj_Ref_Seq Sequence_Unit_Tests_UB_Oct_Seq_No_Cpy Sequence_Unit_Tests_UB_Octet_Sequence Sequence_Unit_Tests_UB_Sequence_CDR Sequence_Unit_Tests_UB_Simple_Types Sequence_Unit_Tests_UB_String_Seq Sequence_Unit_Tests_UB_Value_Sequence Sequence_Unit_Tests_Unbounded_Octet Sequence_Unit_Tests_Unbounded_String

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.Sequence_Unit_Tests_B_Obj_Ref_Seq.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_B_Obj_Ref_Seq.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Sequence_Unit_Tests_B_Sequence_CDR.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_B_Sequence_CDR.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Sequence_Unit_Tests_B_Simple_Types.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_B_Simple_Types.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Sequence_Unit_Tests_B_String_Seq.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_B_String_Seq.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Sequence_Unit_Tests_B_Value_Sequence.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_B_Value_Sequence.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Sequence_Unit_Tests_Bounded_String.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_Bounded_String.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Sequence_Unit_Tests_Obj_Ref_Seq_Elem.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_Obj_Ref_Seq_Elem.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Sequence_Unit_Tests_String_Seq_Elem.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_String_Seq_Elem.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Sequence_Unit_Tests_Test_Alloc_Traits.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_Test_Alloc_Traits.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Sequence_Unit_Tests_UB_Fwd_Ob_Ref_Seq.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_UB_Fwd_Ob_Ref_Seq.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Sequence_Unit_Tests_UB_Obj_Ref_Seq.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_UB_Obj_Ref_Seq.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Sequence_Unit_Tests_UB_Oct_Seq_No_Cpy.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_UB_Oct_Seq_No_Cpy.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Sequence_Unit_Tests_UB_Octet_Sequence.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_UB_Octet_Sequence.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Sequence_Unit_Tests_UB_Sequence_CDR.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_UB_Sequence_CDR.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Sequence_Unit_Tests_UB_Simple_Types.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_UB_Simple_Types.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Sequence_Unit_Tests_UB_String_Seq.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_UB_String_Seq.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Sequence_Unit_Tests_UB_Value_Sequence.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_UB_Value_Sequence.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Sequence_Unit_Tests_Unbounded_Octet.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_Unbounded_Octet.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Sequence_Unit_Tests_Unbounded_String.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_Unbounded_String.mak CFG="$(CFG)" $(@)

Sequence_Unit_Tests_B_Obj_Ref_Seq:
	@echo Project: Makefile.Sequence_Unit_Tests_B_Obj_Ref_Seq.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_B_Obj_Ref_Seq.mak CFG="$(CFG)" all

Sequence_Unit_Tests_B_Sequence_CDR:
	@echo Project: Makefile.Sequence_Unit_Tests_B_Sequence_CDR.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_B_Sequence_CDR.mak CFG="$(CFG)" all

Sequence_Unit_Tests_B_Simple_Types:
	@echo Project: Makefile.Sequence_Unit_Tests_B_Simple_Types.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_B_Simple_Types.mak CFG="$(CFG)" all

Sequence_Unit_Tests_B_String_Seq:
	@echo Project: Makefile.Sequence_Unit_Tests_B_String_Seq.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_B_String_Seq.mak CFG="$(CFG)" all

Sequence_Unit_Tests_B_Value_Sequence:
	@echo Project: Makefile.Sequence_Unit_Tests_B_Value_Sequence.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_B_Value_Sequence.mak CFG="$(CFG)" all

Sequence_Unit_Tests_Bounded_String:
	@echo Project: Makefile.Sequence_Unit_Tests_Bounded_String.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_Bounded_String.mak CFG="$(CFG)" all

Sequence_Unit_Tests_Obj_Ref_Seq_Elem:
	@echo Project: Makefile.Sequence_Unit_Tests_Obj_Ref_Seq_Elem.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_Obj_Ref_Seq_Elem.mak CFG="$(CFG)" all

Sequence_Unit_Tests_String_Seq_Elem:
	@echo Project: Makefile.Sequence_Unit_Tests_String_Seq_Elem.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_String_Seq_Elem.mak CFG="$(CFG)" all

Sequence_Unit_Tests_Test_Alloc_Traits:
	@echo Project: Makefile.Sequence_Unit_Tests_Test_Alloc_Traits.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_Test_Alloc_Traits.mak CFG="$(CFG)" all

Sequence_Unit_Tests_UB_Fwd_Ob_Ref_Seq:
	@echo Project: Makefile.Sequence_Unit_Tests_UB_Fwd_Ob_Ref_Seq.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_UB_Fwd_Ob_Ref_Seq.mak CFG="$(CFG)" all

Sequence_Unit_Tests_UB_Obj_Ref_Seq:
	@echo Project: Makefile.Sequence_Unit_Tests_UB_Obj_Ref_Seq.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_UB_Obj_Ref_Seq.mak CFG="$(CFG)" all

Sequence_Unit_Tests_UB_Oct_Seq_No_Cpy:
	@echo Project: Makefile.Sequence_Unit_Tests_UB_Oct_Seq_No_Cpy.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_UB_Oct_Seq_No_Cpy.mak CFG="$(CFG)" all

Sequence_Unit_Tests_UB_Octet_Sequence:
	@echo Project: Makefile.Sequence_Unit_Tests_UB_Octet_Sequence.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_UB_Octet_Sequence.mak CFG="$(CFG)" all

Sequence_Unit_Tests_UB_Sequence_CDR:
	@echo Project: Makefile.Sequence_Unit_Tests_UB_Sequence_CDR.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_UB_Sequence_CDR.mak CFG="$(CFG)" all

Sequence_Unit_Tests_UB_Simple_Types:
	@echo Project: Makefile.Sequence_Unit_Tests_UB_Simple_Types.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_UB_Simple_Types.mak CFG="$(CFG)" all

Sequence_Unit_Tests_UB_String_Seq:
	@echo Project: Makefile.Sequence_Unit_Tests_UB_String_Seq.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_UB_String_Seq.mak CFG="$(CFG)" all

Sequence_Unit_Tests_UB_Value_Sequence:
	@echo Project: Makefile.Sequence_Unit_Tests_UB_Value_Sequence.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_UB_Value_Sequence.mak CFG="$(CFG)" all

Sequence_Unit_Tests_Unbounded_Octet:
	@echo Project: Makefile.Sequence_Unit_Tests_Unbounded_Octet.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_Unbounded_Octet.mak CFG="$(CFG)" all

Sequence_Unit_Tests_Unbounded_String:
	@echo Project: Makefile.Sequence_Unit_Tests_Unbounded_String.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Sequence_Unit_Tests_Unbounded_String.mak CFG="$(CFG)" all

project_name_list:
	@echo Sequence_Unit_Tests_B_Obj_Ref_Seq
	@echo Sequence_Unit_Tests_B_Sequence_CDR
	@echo Sequence_Unit_Tests_B_Simple_Types
	@echo Sequence_Unit_Tests_B_String_Seq
	@echo Sequence_Unit_Tests_B_Value_Sequence
	@echo Sequence_Unit_Tests_Bounded_String
	@echo Sequence_Unit_Tests_Obj_Ref_Seq_Elem
	@echo Sequence_Unit_Tests_String_Seq_Elem
	@echo Sequence_Unit_Tests_Test_Alloc_Traits
	@echo Sequence_Unit_Tests_UB_Fwd_Ob_Ref_Seq
	@echo Sequence_Unit_Tests_UB_Obj_Ref_Seq
	@echo Sequence_Unit_Tests_UB_Oct_Seq_No_Cpy
	@echo Sequence_Unit_Tests_UB_Octet_Sequence
	@echo Sequence_Unit_Tests_UB_Sequence_CDR
	@echo Sequence_Unit_Tests_UB_Simple_Types
	@echo Sequence_Unit_Tests_UB_String_Seq
	@echo Sequence_Unit_Tests_UB_Value_Sequence
	@echo Sequence_Unit_Tests_Unbounded_Octet
	@echo Sequence_Unit_Tests_Unbounded_String
