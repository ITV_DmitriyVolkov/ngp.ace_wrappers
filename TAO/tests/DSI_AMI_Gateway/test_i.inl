// $Id: test_i.inl 979 2008-12-31 20:22:32Z mitza $

ACE_INLINE
Simple_Server_i::Simple_Server_i (CORBA::ORB_ptr orb)
  :  orb_ (CORBA::ORB::_duplicate (orb)),
     vlong_ (0)
{
}
