// $Id: test_dsi.inl 979 2008-12-31 20:22:32Z mitza $

ACE_INLINE
DSI_Simple_Server::DSI_Simple_Server (
    CORBA::ORB_ptr orb,
    CORBA::Object_ptr target,
    PortableServer::POA_ptr poa)
  :  orb_ (CORBA::ORB::_duplicate (orb)),
     target_ (CORBA::Object::_duplicate (target)),
     poa_ (PortableServer::POA::_duplicate (poa))
{
}
