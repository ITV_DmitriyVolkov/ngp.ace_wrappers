// $Id: test_i.h 1201 2009-06-12 20:22:45Z daiy $

// ============================================================================
//
// = LIBRARY
//   TAO/tests/Forwarding
//
// = FILENAME
//   test_i.h
//
// = AUTHOR
//   Carlos O'Ryan
//
// ============================================================================

#ifndef TAO_FORWARDING_TEST_I_H
#define TAO_FORWARDING_TEST_I_H

#include "testS.h"

class Simple_Server_i : public POA_Simple_Server
{
  // = TITLE
  //   Simpler Server implementation
  //
  // = DESCRIPTION
  //   Implements the Simple_Server interface in test.idl
  //
public:
  Simple_Server_i (CORBA::ORB_ptr orb);
  // ctor

  // = The Simple_Server methods.
  CORBA::Boolean test_is_a (const char * type);

  void shutdown (void);

  int ncalls () const;

private:
  CORBA::ORB_var orb_;
  // The ORB
  int ncalls_;

  int raise_exception_;
};

#if defined(__ACE_INLINE__)
#include "test_i.inl"
#endif /* __ACE_INLINE__ */

#endif /* TAO_FORWARDING_TEST_I_H */
