#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake -value_template platforms=Win64 -make_coexistence -recurse -exclude TAO/CIAO -features qos=1,mfc=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: ForwardOnceUponException_Idl ForwardOnceUponException_Client ForwardOnceUponException_Server

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.ForwardOnceUponException_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.ForwardOnceUponException_Idl.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.ForwardOnceUponException_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.ForwardOnceUponException_Client.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.ForwardOnceUponException_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.ForwardOnceUponException_Server.mak CFG="$(CFG)" $(@)

ForwardOnceUponException_Idl:
	@echo Project: Makefile.ForwardOnceUponException_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.ForwardOnceUponException_Idl.mak CFG="$(CFG)" all

ForwardOnceUponException_Client: ForwardOnceUponException_Idl
	@echo Project: Makefile.ForwardOnceUponException_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.ForwardOnceUponException_Client.mak CFG="$(CFG)" all

ForwardOnceUponException_Server: ForwardOnceUponException_Idl
	@echo Project: Makefile.ForwardOnceUponException_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.ForwardOnceUponException_Server.mak CFG="$(CFG)" all

project_name_list:
	@echo ForwardOnceUponException_Client
	@echo ForwardOnceUponException_Idl
	@echo ForwardOnceUponException_Server
