/**
 * @file Echo_Caller.h
 *
 * $Id: Echo_Caller.h 935 2008-12-10 21:47:27Z mitza $
 *
 * @author Carlos O'Ryan <coryan@atdesk.com>
 *
 */
#ifndef Echo_Caller__h_
#define Echo_Caller__h_

#include "TestS.h"

class Thread_Pool;

class Echo_Caller : public POA_Test::Echo_Caller
{
public:
  Echo_Caller(CORBA::ORB_ptr orb, Thread_Pool *pool_);

  virtual void start_task(Test::Echo_ptr client);

  virtual void shutdown(void);

private:
  CORBA::ORB_var orb_;
  Thread_Pool *pool_;
};

#endif /* Echo_Caller__h_ */
