#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake -value_template platforms=Win64 -make_coexistence -recurse -exclude TAO/CIAO -features qos=1,mfc=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: POA_Generic_Servant_Lib POA_Generic_Servant_Client

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.POA_Generic_Servant_Lib.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.POA_Generic_Servant_Lib.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.POA_Generic_Servant_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.POA_Generic_Servant_Client.mak CFG="$(CFG)" $(@)

POA_Generic_Servant_Lib:
	@echo Project: Makefile.POA_Generic_Servant_Lib.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.POA_Generic_Servant_Lib.mak CFG="$(CFG)" all

POA_Generic_Servant_Client: POA_Generic_Servant_Lib
	@echo Project: Makefile.POA_Generic_Servant_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.POA_Generic_Servant_Client.mak CFG="$(CFG)" all

project_name_list:
	@echo POA_Generic_Servant_Client
	@echo POA_Generic_Servant_Lib
