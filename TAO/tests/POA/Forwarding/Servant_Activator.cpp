// $Id: Servant_Activator.cpp 979 2008-12-31 20:22:32Z mitza $

#include "Servant_Activator.h"
#include "test_i.h"

ACE_RCSID(Forwarding, Servant_Activator, "$Id: Servant_Activator.cpp 979 2008-12-31 20:22:32Z mitza $")

  ServantActivator::ServantActivator (CORBA::ORB_ptr orb,
                                      CORBA::Object_ptr forward_to)
    : orb_ (CORBA::ORB::_duplicate (orb)),
      forward_to_ (CORBA::Object::_duplicate (forward_to))
{
}

PortableServer::Servant
ServantActivator::incarnate (const PortableServer::ObjectId &,
                             PortableServer::POA_ptr)
{
  this->orb_->shutdown (0);

  // Throw forward exception
  throw PortableServer::ForwardRequest (this->forward_to_.in ());
}

void
ServantActivator::etherealize (const PortableServer::ObjectId &,
                               PortableServer::POA_ptr,
                               PortableServer::Servant servant,
                               CORBA::Boolean,
                               CORBA::Boolean)
{
  delete servant;
}

void
ServantActivator::forward_requests (void)
{
  if (CORBA::is_nil (this->forward_to_.in ()))
    throw test::Cannot_Forward ();
}
