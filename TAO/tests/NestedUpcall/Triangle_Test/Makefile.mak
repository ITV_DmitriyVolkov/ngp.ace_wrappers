#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake -value_template platforms=Win64 -make_coexistence -recurse -exclude TAO/CIAO -features qos=1,mfc=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: TT_Nested_Upcall_Server_Idl TT_Nested_Upcall_Initiator TT_Nested_Upcall_Server_A TT_Nested_Upcall_Server_B

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.TT_Nested_Upcall_Server_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.TT_Nested_Upcall_Server_Idl.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.TT_Nested_Upcall_Initiator.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.TT_Nested_Upcall_Initiator.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.TT_Nested_Upcall_Server_A.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.TT_Nested_Upcall_Server_A.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.TT_Nested_Upcall_Server_B.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.TT_Nested_Upcall_Server_B.mak CFG="$(CFG)" $(@)

TT_Nested_Upcall_Server_Idl:
	@echo Project: Makefile.TT_Nested_Upcall_Server_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.TT_Nested_Upcall_Server_Idl.mak CFG="$(CFG)" all

TT_Nested_Upcall_Initiator: TT_Nested_Upcall_Server_Idl
	@echo Project: Makefile.TT_Nested_Upcall_Initiator.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.TT_Nested_Upcall_Initiator.mak CFG="$(CFG)" all

TT_Nested_Upcall_Server_A: TT_Nested_Upcall_Server_Idl
	@echo Project: Makefile.TT_Nested_Upcall_Server_A.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.TT_Nested_Upcall_Server_A.mak CFG="$(CFG)" all

TT_Nested_Upcall_Server_B: TT_Nested_Upcall_Server_Idl
	@echo Project: Makefile.TT_Nested_Upcall_Server_B.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.TT_Nested_Upcall_Server_B.mak CFG="$(CFG)" all

project_name_list:
	@echo TT_Nested_Upcall_Initiator
	@echo TT_Nested_Upcall_Server_A
	@echo TT_Nested_Upcall_Server_B
	@echo TT_Nested_Upcall_Server_Idl
