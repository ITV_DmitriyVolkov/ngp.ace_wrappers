// -*- c++ -*-
// $Id: Initiator_i.h 935 2008-12-10 21:47:27Z mitza $

// ============================================================================
//
// = LIBRARY
//    TAO/tests/NestedUpCalls/Triangle_Test
//
// = FILENAME
//    Initiator_i.h
//
// = DESCRIPTION
//    This class implements the Initiator of the
//    Nested Upcalls - Triangle test.
//
// = AUTHORS
//    Michael Kircher
//
// ============================================================================

#ifndef INITIATOR_IMPL_H
#  define INITIATOR_IMPL_H

#include "Triangle_TestS.h"

class Initiator_i : public POA_Initiator
{
  // = TITLE
  //     Implement the <Initiator> IDL interface.
public:
  Initiator_i (Object_A_ptr object_A_ptr,
                  Object_B_ptr object_B_ptr);
  // Constructor.

  virtual ~Initiator_i (void);
  // Destructor.

  virtual void foo_object_B (void);

private:
  Object_A_var object_A_var_;
  // reference to object A

  Object_B_var object_B_var_;
  // reference to object B
};

#endif /* INITIATOR_IMPL_H */
