//
// $Id: Process.cpp 935 2008-12-10 21:47:27Z mitza $
//
#include "Process.h"
#include "ace/OS_NS_unistd.h"

ACE_RCSID(Client_Leaks, Process, "$Id: Process.cpp 935 2008-12-10 21:47:27Z mitza $")

Process::Process (CORBA::ORB_ptr orb)
  : orb_ (CORBA::ORB::_duplicate (orb))
{
}

CORBA::Long
Process::get_process_id (void)
{
  return ACE_OS::getpid ();
}

void
Process::shutdown (void)
{
  this->orb_->shutdown (0);
}
