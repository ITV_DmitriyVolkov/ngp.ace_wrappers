# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.IDL_Test_Main.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Console Application")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "arrayC.inl" "arrayS.inl" "arrayC.h" "arrayS.h" "arrayS_T.h" "arrayC.cpp" "arrayS.cpp" "arrayS_T.cpp" "Bug_2350_RegressionC.inl" "Bug_2350_RegressionS.inl" "Bug_2350_RegressionC.h" "Bug_2350_RegressionS.h" "Bug_2350_RegressionS_T.h" "Bug_2350_RegressionC.cpp" "Bug_2350_RegressionS.cpp" "Bug_2350_RegressionS_T.cpp" "Bug_2577_RegressionC.inl" "Bug_2577_RegressionS.inl" "Bug_2577_RegressionC.h" "Bug_2577_RegressionS.h" "Bug_2577_RegressionS_T.h" "Bug_2577_RegressionC.cpp" "Bug_2577_RegressionS.cpp" "Bug_2577_RegressionS_T.cpp" "Bug_2582_RegressionC.inl" "Bug_2582_RegressionS.inl" "Bug_2582_RegressionC.h" "Bug_2582_RegressionS.h" "Bug_2582_RegressionS_T.h" "Bug_2582_RegressionC.cpp" "Bug_2582_RegressionS.cpp" "Bug_2582_RegressionS_T.cpp" "Bug_2583_RegressionC.inl" "Bug_2583_RegressionS.inl" "Bug_2583_RegressionC.h" "Bug_2583_RegressionS.h" "Bug_2583_RegressionS_T.h" "Bug_2583_RegressionC.cpp" "Bug_2583_RegressionS.cpp" "Bug_2583_RegressionS_T.cpp" "Bug_2616_RegressionC.inl" "Bug_2616_RegressionS.inl" "Bug_2616_RegressionC.h" "Bug_2616_RegressionS.h" "Bug_2616_RegressionS_T.h" "Bug_2616_RegressionC.cpp" "Bug_2616_RegressionS.cpp" "Bug_2616_RegressionS_T.cpp" "Bug_2619_RegressionC.inl" "Bug_2619_RegressionS.inl" "Bug_2619_RegressionC.h" "Bug_2619_RegressionS.h" "Bug_2619_RegressionS_T.h" "Bug_2619_RegressionC.cpp" "Bug_2619_RegressionS.cpp" "Bug_2619_RegressionS_T.cpp" "enum_in_structC.inl" "enum_in_structS.inl" "enum_in_structC.h" "enum_in_structS.h" "enum_in_structS_T.h" "enum_in_structC.cpp" "enum_in_structS.cpp" "enum_in_structS_T.cpp" "fullC.inl" "fullS.inl" "fullC.h" "fullS.h" "fullS_T.h" "fullC.cpp" "fullS.cpp" "fullS_T.cpp" "fwdC.inl" "fwdS.inl" "fwdC.h" "fwdS.h" "fwdS_T.h" "fwdC.cpp" "fwdS.cpp" "fwdS_T.cpp" "gperfC.inl" "gperfS.inl" "gperfC.h" "gperfS.h" "gperfS_T.h" "gperfC.cpp" "gperfS.cpp" "gperfS_T.cpp" "includingC.inl" "includingS.inl" "includingC.h" "includingS.h" "includingS_T.h" "includingC.cpp" "includingS.cpp" "includingS_T.cpp" "interfaceC.inl" "interfaceS.inl" "interfaceC.h" "interfaceS.h" "interfaceS_T.h" "interfaceC.cpp" "interfaceS.cpp" "interfaceS_T.cpp" "includedC.inl" "includedS.inl" "includedC.h" "includedS.h" "includedS_T.h" "includedC.cpp" "includedS.cpp" "includedS_T.cpp" "nested_scopeC.inl" "nested_scopeS.inl" "nested_scopeC.h" "nested_scopeS.h" "nested_scopeS_T.h" "nested_scopeC.cpp" "nested_scopeS.cpp" "nested_scopeS_T.cpp" "old_arrayC.inl" "old_arrayS.inl" "old_arrayC.h" "old_arrayS.h" "old_arrayS_T.h" "old_arrayC.cpp" "old_arrayS.cpp" "old_arrayS_T.cpp" "old_constantsC.inl" "old_constantsS.inl" "old_constantsC.h" "old_constantsS.h" "old_constantsS_T.h" "old_constantsC.cpp" "old_constantsS.cpp" "old_constantsS_T.cpp" "old_sequenceC.inl" "old_sequenceS.inl" "old_sequenceC.h" "old_sequenceS.h" "old_sequenceS_T.h" "old_sequenceC.cpp" "old_sequenceS.cpp" "old_sequenceS_T.cpp" "simpleC.inl" "simpleS.inl" "simpleC.h" "simpleS.h" "simpleS_T.h" "simpleC.cpp" "simpleS.cpp" "simpleS_T.cpp" "simple2C.inl" "simple2S.inl" "simple2C.h" "simple2S.h" "simple2S_T.h" "simple2C.cpp" "simple2S.cpp" "simple2S_T.cpp" "old_structC.inl" "old_structS.inl" "old_structC.h" "old_structS.h" "old_structS_T.h" "old_structC.cpp" "old_structS.cpp" "old_structS_T.cpp" "old_unionC.inl" "old_unionS.inl" "old_unionC.h" "old_unionS.h" "old_unionS_T.h" "old_unionC.cpp" "old_unionS.cpp" "old_unionS_T.cpp" "old_union2C.inl" "old_union2S.inl" "old_union2C.h" "old_union2S.h" "old_union2S_T.h" "old_union2C.cpp" "old_union2S.cpp" "old_union2S_T.cpp" "paramsC.inl" "paramsS.inl" "paramsC.h" "paramsS.h" "paramsS_T.h" "paramsC.cpp" "paramsS.cpp" "paramsS_T.cpp" "reopened_modulesC.inl" "reopened_modulesS.inl" "reopened_modulesC.h" "reopened_modulesS.h" "reopened_modulesS_T.h" "reopened_modulesC.cpp" "reopened_modulesS.cpp" "reopened_modulesS_T.cpp" "sequenceC.inl" "sequenceS.inl" "sequenceC.h" "sequenceS.h" "sequenceS_T.h" "sequenceC.cpp" "sequenceS.cpp" "sequenceS_T.cpp" "structC.inl" "structS.inl" "structC.h" "structS.h" "structS_T.h" "structC.cpp" "structS.cpp" "structS_T.cpp" "reopen_include1C.inl" "reopen_include1S.inl" "reopen_include1C.h" "reopen_include1S.h" "reopen_include1S_T.h" "reopen_include1C.cpp" "reopen_include1S.cpp" "reopen_include1S_T.cpp" "reopen_include2C.inl" "reopen_include2S.inl" "reopen_include2C.h" "reopen_include2S.h" "reopen_include2S_T.h" "reopen_include2C.cpp" "reopen_include2S.cpp" "reopen_include2S_T.cpp" "typeprefixC.inl" "typeprefixS.inl" "typeprefixC.h" "typeprefixS.h" "typeprefixS_T.h" "typeprefixC.cpp" "typeprefixS.cpp" "typeprefixS_T.cpp" "unionC.inl" "unionS.inl" "unionC.h" "unionS.h" "unionS_T.h" "unionC.cpp" "unionS.cpp" "unionS_T.cpp" "anonymousC.inl" "anonymousS.inl" "anonymousC.h" "anonymousS.h" "anonymousA.h" "anonymousS_T.h" "anonymousC.cpp" "anonymousS.cpp" "anonymousA.cpp" "anonymousS_T.cpp" "array_onlyC.inl" "array_onlyS.inl" "array_onlyC.h" "array_onlyS.h" "array_onlyA.h" "array_onlyS_T.h" "array_onlyC.cpp" "array_onlyS.cpp" "array_onlyA.cpp" "array_onlyS_T.cpp" "constantsC.inl" "constantsS.inl" "constantsC.h" "constantsS.h" "constantsA.h" "constantsS_T.h" "constantsC.cpp" "constantsS.cpp" "constantsA.cpp" "constantsS_T.cpp" "generic_objectC.inl" "generic_objectS.inl" "generic_objectC.h" "generic_objectS.h" "generic_objectA.h" "generic_objectS_T.h" "generic_objectC.cpp" "generic_objectS.cpp" "generic_objectA.cpp" "generic_objectS_T.cpp" "keywordsC.inl" "keywordsS.inl" "keywordsC.h" "keywordsS.h" "keywordsA.h" "keywordsS_T.h" "keywordsC.cpp" "keywordsS.cpp" "keywordsA.cpp" "keywordsS_T.cpp" "dif2C.inl" "dif2S.inl" "dif2C.h" "dif2S.h" "dif2A.h" "dif2S_T.h" "dif2C.cpp" "dif2S.cpp" "dif2A.cpp" "dif2S_T.cpp" "inheritC.inl" "inheritS.inl" "inheritC.h" "inheritS.h" "inheritA.h" "inheritS_T.h" "inheritC.cpp" "inheritS.cpp" "inheritA.cpp" "inheritS_T.cpp" "moduleC.inl" "moduleS.inl" "moduleC.h" "moduleS.h" "moduleA.h" "moduleS_T.h" "moduleC.cpp" "moduleS.cpp" "moduleA.cpp" "moduleS_T.cpp" "primtypesC.inl" "primtypesS.inl" "primtypesC.h" "primtypesS.h" "primtypesA.h" "primtypesS_T.h" "primtypesC.cpp" "primtypesS.cpp" "primtypesA.cpp" "primtypesS_T.cpp" "pragmaC.inl" "pragmaS.inl" "pragmaC.h" "pragmaS.h" "pragmaA.h" "pragmaS_T.h" "pragmaC.cpp" "pragmaS.cpp" "pragmaA.cpp" "pragmaS_T.cpp" "repo_id_modC.inl" "repo_id_modS.inl" "repo_id_modC.h" "repo_id_modS.h" "repo_id_modA.h" "repo_id_modS_T.h" "repo_id_modC.cpp" "repo_id_modS.cpp" "repo_id_modA.cpp" "repo_id_modS_T.cpp" "typedefC.inl" "typedefS.inl" "typedefC.h" "typedefS.h" "typedefA.h" "typedefS_T.h" "typedefC.cpp" "typedefS.cpp" "typedefA.cpp" "typedefS_T.cpp" "typecodeC.inl" "typecodeS.inl" "typecodeC.h" "typecodeS.h" "typecodeA.h" "typecodeS_T.h" "typecodeC.cpp" "typecodeS.cpp" "typecodeA.cpp" "typecodeS_T.cpp" "valuetypeC.inl" "valuetypeS.inl" "valuetypeC.h" "valuetypeS.h" "valuetypeA.h" "valuetypeS_T.h" "valuetypeC.cpp" "valuetypeS.cpp" "valuetypeA.cpp" "valuetypeS_T.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INSTALLDIR=.
INTDIR=Debug\IDL_Test_Main\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\main.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -f "Makefile.IDL_Test_Main.dep" "Bug_2577_RegressionC.cpp" "Bug_2577_RegressionS.cpp" "valuetypeC.cpp" "valuetypeS.cpp" "valuetypeA.cpp" "old_unionC.cpp" "old_unionS.cpp" "array_onlyC.cpp" "array_onlyS.cpp" "array_onlyA.cpp" "Bug_2582_RegressionC.cpp" "Bug_2582_RegressionS.cpp" "unionC.cpp" "unionS.cpp" "Bug_2616_RegressionC.cpp" "Bug_2616_RegressionS.cpp" "pragmaC.cpp" "pragmaS.cpp" "pragmaA.cpp" "generic_objectC.cpp" "generic_objectS.cpp" "generic_objectA.cpp" "enum_in_structC.cpp" "enum_in_structS.cpp" "dif2C.cpp" "dif2S.cpp" "dif2A.cpp" "Bug_2619_RegressionC.cpp" "Bug_2619_RegressionS.cpp" "typedefC.cpp" "typedefS.cpp" "typedefA.cpp" "old_structC.cpp" "old_structS.cpp" "simpleC.cpp" "simpleS.cpp" "fwdC.cpp" "fwdS.cpp" "paramsC.cpp" "paramsS.cpp" "typecodeC.cpp" "typecodeS.cpp" "typecodeA.cpp" "old_sequenceC.cpp" "sequenceC.cpp" "old_sequenceS.cpp" "sequenceS.cpp" "includingC.cpp" "includingS.cpp" "Bug_2583_RegressionC.cpp" "Bug_2583_RegressionS.cpp" "typeprefixC.cpp" "typeprefixS.cpp" "repo_id_modC.cpp" "repo_id_modS.cpp" "repo_id_modA.cpp" "old_arrayC.cpp" "old_arrayS.cpp" "inheritC.cpp" "inheritS.cpp" "inheritA.cpp" "includedC.cpp" "includedS.cpp" "reopen_include2C.cpp" "reopen_include2S.cpp" "reopen_include1C.cpp" "reopen_include1S.cpp" "old_constantsC.cpp" "old_constantsS.cpp" "anonymousC.cpp" "anonymousS.cpp" "anonymousA.cpp" "gperfC.cpp" "gperfS.cpp" "primtypesC.cpp" "primtypesS.cpp" "primtypesA.cpp" "simple2C.cpp" "simple2S.cpp" "arrayC.cpp" "arrayS.cpp" "reopened_modulesC.cpp" "reopened_modulesS.cpp" "fullC.cpp" "fullS.cpp" "keywordsC.cpp" "keywordsS.cpp" "keywordsA.cpp" "moduleC.cpp" "moduleS.cpp" "moduleA.cpp" "constantsC.cpp" "constantsS.cpp" "constantsA.cpp" "nested_scopeC.cpp" "nested_scopeS.cpp" "structC.cpp" "structS.cpp" "old_union2C.cpp" "old_union2S.cpp" "interfaceC.cpp" "interfaceS.cpp" "Bug_2350_RegressionC.cpp" "Bug_2350_RegressionS.cpp" "main.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\main.pdb"
	-@del /f/q "$(INSTALLDIR)\main.exe"
	-@del /f/q "$(INSTALLDIR)\main.ilk"
	-@del /f/q "arrayC.inl"
	-@del /f/q "arrayS.inl"
	-@del /f/q "arrayC.h"
	-@del /f/q "arrayS.h"
	-@del /f/q "arrayS_T.h"
	-@del /f/q "arrayC.cpp"
	-@del /f/q "arrayS.cpp"
	-@del /f/q "arrayS_T.cpp"
	-@del /f/q "Bug_2350_RegressionC.inl"
	-@del /f/q "Bug_2350_RegressionS.inl"
	-@del /f/q "Bug_2350_RegressionC.h"
	-@del /f/q "Bug_2350_RegressionS.h"
	-@del /f/q "Bug_2350_RegressionS_T.h"
	-@del /f/q "Bug_2350_RegressionC.cpp"
	-@del /f/q "Bug_2350_RegressionS.cpp"
	-@del /f/q "Bug_2350_RegressionS_T.cpp"
	-@del /f/q "Bug_2577_RegressionC.inl"
	-@del /f/q "Bug_2577_RegressionS.inl"
	-@del /f/q "Bug_2577_RegressionC.h"
	-@del /f/q "Bug_2577_RegressionS.h"
	-@del /f/q "Bug_2577_RegressionS_T.h"
	-@del /f/q "Bug_2577_RegressionC.cpp"
	-@del /f/q "Bug_2577_RegressionS.cpp"
	-@del /f/q "Bug_2577_RegressionS_T.cpp"
	-@del /f/q "Bug_2582_RegressionC.inl"
	-@del /f/q "Bug_2582_RegressionS.inl"
	-@del /f/q "Bug_2582_RegressionC.h"
	-@del /f/q "Bug_2582_RegressionS.h"
	-@del /f/q "Bug_2582_RegressionS_T.h"
	-@del /f/q "Bug_2582_RegressionC.cpp"
	-@del /f/q "Bug_2582_RegressionS.cpp"
	-@del /f/q "Bug_2582_RegressionS_T.cpp"
	-@del /f/q "Bug_2583_RegressionC.inl"
	-@del /f/q "Bug_2583_RegressionS.inl"
	-@del /f/q "Bug_2583_RegressionC.h"
	-@del /f/q "Bug_2583_RegressionS.h"
	-@del /f/q "Bug_2583_RegressionS_T.h"
	-@del /f/q "Bug_2583_RegressionC.cpp"
	-@del /f/q "Bug_2583_RegressionS.cpp"
	-@del /f/q "Bug_2583_RegressionS_T.cpp"
	-@del /f/q "Bug_2616_RegressionC.inl"
	-@del /f/q "Bug_2616_RegressionS.inl"
	-@del /f/q "Bug_2616_RegressionC.h"
	-@del /f/q "Bug_2616_RegressionS.h"
	-@del /f/q "Bug_2616_RegressionS_T.h"
	-@del /f/q "Bug_2616_RegressionC.cpp"
	-@del /f/q "Bug_2616_RegressionS.cpp"
	-@del /f/q "Bug_2616_RegressionS_T.cpp"
	-@del /f/q "Bug_2619_RegressionC.inl"
	-@del /f/q "Bug_2619_RegressionS.inl"
	-@del /f/q "Bug_2619_RegressionC.h"
	-@del /f/q "Bug_2619_RegressionS.h"
	-@del /f/q "Bug_2619_RegressionS_T.h"
	-@del /f/q "Bug_2619_RegressionC.cpp"
	-@del /f/q "Bug_2619_RegressionS.cpp"
	-@del /f/q "Bug_2619_RegressionS_T.cpp"
	-@del /f/q "enum_in_structC.inl"
	-@del /f/q "enum_in_structS.inl"
	-@del /f/q "enum_in_structC.h"
	-@del /f/q "enum_in_structS.h"
	-@del /f/q "enum_in_structS_T.h"
	-@del /f/q "enum_in_structC.cpp"
	-@del /f/q "enum_in_structS.cpp"
	-@del /f/q "enum_in_structS_T.cpp"
	-@del /f/q "fullC.inl"
	-@del /f/q "fullS.inl"
	-@del /f/q "fullC.h"
	-@del /f/q "fullS.h"
	-@del /f/q "fullS_T.h"
	-@del /f/q "fullC.cpp"
	-@del /f/q "fullS.cpp"
	-@del /f/q "fullS_T.cpp"
	-@del /f/q "fwdC.inl"
	-@del /f/q "fwdS.inl"
	-@del /f/q "fwdC.h"
	-@del /f/q "fwdS.h"
	-@del /f/q "fwdS_T.h"
	-@del /f/q "fwdC.cpp"
	-@del /f/q "fwdS.cpp"
	-@del /f/q "fwdS_T.cpp"
	-@del /f/q "gperfC.inl"
	-@del /f/q "gperfS.inl"
	-@del /f/q "gperfC.h"
	-@del /f/q "gperfS.h"
	-@del /f/q "gperfS_T.h"
	-@del /f/q "gperfC.cpp"
	-@del /f/q "gperfS.cpp"
	-@del /f/q "gperfS_T.cpp"
	-@del /f/q "includingC.inl"
	-@del /f/q "includingS.inl"
	-@del /f/q "includingC.h"
	-@del /f/q "includingS.h"
	-@del /f/q "includingS_T.h"
	-@del /f/q "includingC.cpp"
	-@del /f/q "includingS.cpp"
	-@del /f/q "includingS_T.cpp"
	-@del /f/q "interfaceC.inl"
	-@del /f/q "interfaceS.inl"
	-@del /f/q "interfaceC.h"
	-@del /f/q "interfaceS.h"
	-@del /f/q "interfaceS_T.h"
	-@del /f/q "interfaceC.cpp"
	-@del /f/q "interfaceS.cpp"
	-@del /f/q "interfaceS_T.cpp"
	-@del /f/q "includedC.inl"
	-@del /f/q "includedS.inl"
	-@del /f/q "includedC.h"
	-@del /f/q "includedS.h"
	-@del /f/q "includedS_T.h"
	-@del /f/q "includedC.cpp"
	-@del /f/q "includedS.cpp"
	-@del /f/q "includedS_T.cpp"
	-@del /f/q "nested_scopeC.inl"
	-@del /f/q "nested_scopeS.inl"
	-@del /f/q "nested_scopeC.h"
	-@del /f/q "nested_scopeS.h"
	-@del /f/q "nested_scopeS_T.h"
	-@del /f/q "nested_scopeC.cpp"
	-@del /f/q "nested_scopeS.cpp"
	-@del /f/q "nested_scopeS_T.cpp"
	-@del /f/q "old_arrayC.inl"
	-@del /f/q "old_arrayS.inl"
	-@del /f/q "old_arrayC.h"
	-@del /f/q "old_arrayS.h"
	-@del /f/q "old_arrayS_T.h"
	-@del /f/q "old_arrayC.cpp"
	-@del /f/q "old_arrayS.cpp"
	-@del /f/q "old_arrayS_T.cpp"
	-@del /f/q "old_constantsC.inl"
	-@del /f/q "old_constantsS.inl"
	-@del /f/q "old_constantsC.h"
	-@del /f/q "old_constantsS.h"
	-@del /f/q "old_constantsS_T.h"
	-@del /f/q "old_constantsC.cpp"
	-@del /f/q "old_constantsS.cpp"
	-@del /f/q "old_constantsS_T.cpp"
	-@del /f/q "old_sequenceC.inl"
	-@del /f/q "old_sequenceS.inl"
	-@del /f/q "old_sequenceC.h"
	-@del /f/q "old_sequenceS.h"
	-@del /f/q "old_sequenceS_T.h"
	-@del /f/q "old_sequenceC.cpp"
	-@del /f/q "old_sequenceS.cpp"
	-@del /f/q "old_sequenceS_T.cpp"
	-@del /f/q "simpleC.inl"
	-@del /f/q "simpleS.inl"
	-@del /f/q "simpleC.h"
	-@del /f/q "simpleS.h"
	-@del /f/q "simpleS_T.h"
	-@del /f/q "simpleC.cpp"
	-@del /f/q "simpleS.cpp"
	-@del /f/q "simpleS_T.cpp"
	-@del /f/q "simple2C.inl"
	-@del /f/q "simple2S.inl"
	-@del /f/q "simple2C.h"
	-@del /f/q "simple2S.h"
	-@del /f/q "simple2S_T.h"
	-@del /f/q "simple2C.cpp"
	-@del /f/q "simple2S.cpp"
	-@del /f/q "simple2S_T.cpp"
	-@del /f/q "old_structC.inl"
	-@del /f/q "old_structS.inl"
	-@del /f/q "old_structC.h"
	-@del /f/q "old_structS.h"
	-@del /f/q "old_structS_T.h"
	-@del /f/q "old_structC.cpp"
	-@del /f/q "old_structS.cpp"
	-@del /f/q "old_structS_T.cpp"
	-@del /f/q "old_unionC.inl"
	-@del /f/q "old_unionS.inl"
	-@del /f/q "old_unionC.h"
	-@del /f/q "old_unionS.h"
	-@del /f/q "old_unionS_T.h"
	-@del /f/q "old_unionC.cpp"
	-@del /f/q "old_unionS.cpp"
	-@del /f/q "old_unionS_T.cpp"
	-@del /f/q "old_union2C.inl"
	-@del /f/q "old_union2S.inl"
	-@del /f/q "old_union2C.h"
	-@del /f/q "old_union2S.h"
	-@del /f/q "old_union2S_T.h"
	-@del /f/q "old_union2C.cpp"
	-@del /f/q "old_union2S.cpp"
	-@del /f/q "old_union2S_T.cpp"
	-@del /f/q "paramsC.inl"
	-@del /f/q "paramsS.inl"
	-@del /f/q "paramsC.h"
	-@del /f/q "paramsS.h"
	-@del /f/q "paramsS_T.h"
	-@del /f/q "paramsC.cpp"
	-@del /f/q "paramsS.cpp"
	-@del /f/q "paramsS_T.cpp"
	-@del /f/q "reopened_modulesC.inl"
	-@del /f/q "reopened_modulesS.inl"
	-@del /f/q "reopened_modulesC.h"
	-@del /f/q "reopened_modulesS.h"
	-@del /f/q "reopened_modulesS_T.h"
	-@del /f/q "reopened_modulesC.cpp"
	-@del /f/q "reopened_modulesS.cpp"
	-@del /f/q "reopened_modulesS_T.cpp"
	-@del /f/q "sequenceC.inl"
	-@del /f/q "sequenceS.inl"
	-@del /f/q "sequenceC.h"
	-@del /f/q "sequenceS.h"
	-@del /f/q "sequenceS_T.h"
	-@del /f/q "sequenceC.cpp"
	-@del /f/q "sequenceS.cpp"
	-@del /f/q "sequenceS_T.cpp"
	-@del /f/q "structC.inl"
	-@del /f/q "structS.inl"
	-@del /f/q "structC.h"
	-@del /f/q "structS.h"
	-@del /f/q "structS_T.h"
	-@del /f/q "structC.cpp"
	-@del /f/q "structS.cpp"
	-@del /f/q "structS_T.cpp"
	-@del /f/q "reopen_include1C.inl"
	-@del /f/q "reopen_include1S.inl"
	-@del /f/q "reopen_include1C.h"
	-@del /f/q "reopen_include1S.h"
	-@del /f/q "reopen_include1S_T.h"
	-@del /f/q "reopen_include1C.cpp"
	-@del /f/q "reopen_include1S.cpp"
	-@del /f/q "reopen_include1S_T.cpp"
	-@del /f/q "reopen_include2C.inl"
	-@del /f/q "reopen_include2S.inl"
	-@del /f/q "reopen_include2C.h"
	-@del /f/q "reopen_include2S.h"
	-@del /f/q "reopen_include2S_T.h"
	-@del /f/q "reopen_include2C.cpp"
	-@del /f/q "reopen_include2S.cpp"
	-@del /f/q "reopen_include2S_T.cpp"
	-@del /f/q "typeprefixC.inl"
	-@del /f/q "typeprefixS.inl"
	-@del /f/q "typeprefixC.h"
	-@del /f/q "typeprefixS.h"
	-@del /f/q "typeprefixS_T.h"
	-@del /f/q "typeprefixC.cpp"
	-@del /f/q "typeprefixS.cpp"
	-@del /f/q "typeprefixS_T.cpp"
	-@del /f/q "unionC.inl"
	-@del /f/q "unionS.inl"
	-@del /f/q "unionC.h"
	-@del /f/q "unionS.h"
	-@del /f/q "unionS_T.h"
	-@del /f/q "unionC.cpp"
	-@del /f/q "unionS.cpp"
	-@del /f/q "unionS_T.cpp"
	-@del /f/q "anonymousC.inl"
	-@del /f/q "anonymousS.inl"
	-@del /f/q "anonymousC.h"
	-@del /f/q "anonymousS.h"
	-@del /f/q "anonymousA.h"
	-@del /f/q "anonymousS_T.h"
	-@del /f/q "anonymousC.cpp"
	-@del /f/q "anonymousS.cpp"
	-@del /f/q "anonymousA.cpp"
	-@del /f/q "anonymousS_T.cpp"
	-@del /f/q "array_onlyC.inl"
	-@del /f/q "array_onlyS.inl"
	-@del /f/q "array_onlyC.h"
	-@del /f/q "array_onlyS.h"
	-@del /f/q "array_onlyA.h"
	-@del /f/q "array_onlyS_T.h"
	-@del /f/q "array_onlyC.cpp"
	-@del /f/q "array_onlyS.cpp"
	-@del /f/q "array_onlyA.cpp"
	-@del /f/q "array_onlyS_T.cpp"
	-@del /f/q "constantsC.inl"
	-@del /f/q "constantsS.inl"
	-@del /f/q "constantsC.h"
	-@del /f/q "constantsS.h"
	-@del /f/q "constantsA.h"
	-@del /f/q "constantsS_T.h"
	-@del /f/q "constantsC.cpp"
	-@del /f/q "constantsS.cpp"
	-@del /f/q "constantsA.cpp"
	-@del /f/q "constantsS_T.cpp"
	-@del /f/q "generic_objectC.inl"
	-@del /f/q "generic_objectS.inl"
	-@del /f/q "generic_objectC.h"
	-@del /f/q "generic_objectS.h"
	-@del /f/q "generic_objectA.h"
	-@del /f/q "generic_objectS_T.h"
	-@del /f/q "generic_objectC.cpp"
	-@del /f/q "generic_objectS.cpp"
	-@del /f/q "generic_objectA.cpp"
	-@del /f/q "generic_objectS_T.cpp"
	-@del /f/q "keywordsC.inl"
	-@del /f/q "keywordsS.inl"
	-@del /f/q "keywordsC.h"
	-@del /f/q "keywordsS.h"
	-@del /f/q "keywordsA.h"
	-@del /f/q "keywordsS_T.h"
	-@del /f/q "keywordsC.cpp"
	-@del /f/q "keywordsS.cpp"
	-@del /f/q "keywordsA.cpp"
	-@del /f/q "keywordsS_T.cpp"
	-@del /f/q "dif2C.inl"
	-@del /f/q "dif2S.inl"
	-@del /f/q "dif2C.h"
	-@del /f/q "dif2S.h"
	-@del /f/q "dif2A.h"
	-@del /f/q "dif2S_T.h"
	-@del /f/q "dif2C.cpp"
	-@del /f/q "dif2S.cpp"
	-@del /f/q "dif2A.cpp"
	-@del /f/q "dif2S_T.cpp"
	-@del /f/q "inheritC.inl"
	-@del /f/q "inheritS.inl"
	-@del /f/q "inheritC.h"
	-@del /f/q "inheritS.h"
	-@del /f/q "inheritA.h"
	-@del /f/q "inheritS_T.h"
	-@del /f/q "inheritC.cpp"
	-@del /f/q "inheritS.cpp"
	-@del /f/q "inheritA.cpp"
	-@del /f/q "inheritS_T.cpp"
	-@del /f/q "moduleC.inl"
	-@del /f/q "moduleS.inl"
	-@del /f/q "moduleC.h"
	-@del /f/q "moduleS.h"
	-@del /f/q "moduleA.h"
	-@del /f/q "moduleS_T.h"
	-@del /f/q "moduleC.cpp"
	-@del /f/q "moduleS.cpp"
	-@del /f/q "moduleA.cpp"
	-@del /f/q "moduleS_T.cpp"
	-@del /f/q "primtypesC.inl"
	-@del /f/q "primtypesS.inl"
	-@del /f/q "primtypesC.h"
	-@del /f/q "primtypesS.h"
	-@del /f/q "primtypesA.h"
	-@del /f/q "primtypesS_T.h"
	-@del /f/q "primtypesC.cpp"
	-@del /f/q "primtypesS.cpp"
	-@del /f/q "primtypesA.cpp"
	-@del /f/q "primtypesS_T.cpp"
	-@del /f/q "pragmaC.inl"
	-@del /f/q "pragmaS.inl"
	-@del /f/q "pragmaC.h"
	-@del /f/q "pragmaS.h"
	-@del /f/q "pragmaA.h"
	-@del /f/q "pragmaS_T.h"
	-@del /f/q "pragmaC.cpp"
	-@del /f/q "pragmaS.cpp"
	-@del /f/q "pragmaA.cpp"
	-@del /f/q "pragmaS_T.cpp"
	-@del /f/q "repo_id_modC.inl"
	-@del /f/q "repo_id_modS.inl"
	-@del /f/q "repo_id_modC.h"
	-@del /f/q "repo_id_modS.h"
	-@del /f/q "repo_id_modA.h"
	-@del /f/q "repo_id_modS_T.h"
	-@del /f/q "repo_id_modC.cpp"
	-@del /f/q "repo_id_modS.cpp"
	-@del /f/q "repo_id_modA.cpp"
	-@del /f/q "repo_id_modS_T.cpp"
	-@del /f/q "typedefC.inl"
	-@del /f/q "typedefS.inl"
	-@del /f/q "typedefC.h"
	-@del /f/q "typedefS.h"
	-@del /f/q "typedefA.h"
	-@del /f/q "typedefS_T.h"
	-@del /f/q "typedefC.cpp"
	-@del /f/q "typedefS.cpp"
	-@del /f/q "typedefA.cpp"
	-@del /f/q "typedefS_T.cpp"
	-@del /f/q "typecodeC.inl"
	-@del /f/q "typecodeS.inl"
	-@del /f/q "typecodeC.h"
	-@del /f/q "typecodeS.h"
	-@del /f/q "typecodeA.h"
	-@del /f/q "typecodeS_T.h"
	-@del /f/q "typecodeC.cpp"
	-@del /f/q "typecodeS.cpp"
	-@del /f/q "typecodeA.cpp"
	-@del /f/q "typecodeS_T.cpp"
	-@del /f/q "valuetypeC.inl"
	-@del /f/q "valuetypeS.inl"
	-@del /f/q "valuetypeC.h"
	-@del /f/q "valuetypeS.h"
	-@del /f/q "valuetypeA.h"
	-@del /f/q "valuetypeS_T.h"
	-@del /f/q "valuetypeC.cpp"
	-@del /f/q "valuetypeS.cpp"
	-@del /f/q "valuetypeA.cpp"
	-@del /f/q "valuetypeS_T.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\IDL_Test_Main\$(NULL)" mkdir "Debug\IDL_Test_Main"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\.." /I "..\.." /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_PortableServerd.lib TAO_Valuetyped.lib TAO_CodecFactoryd.lib TAO_PId.lib TAO_Messagingd.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\main.pdb" /machine:IA64 /out:"$(INSTALLDIR)\main.exe"
LINK32_OBJS= \
	"$(INTDIR)\Bug_2577_RegressionC.obj" \
	"$(INTDIR)\Bug_2577_RegressionS.obj" \
	"$(INTDIR)\valuetypeC.obj" \
	"$(INTDIR)\valuetypeS.obj" \
	"$(INTDIR)\valuetypeA.obj" \
	"$(INTDIR)\old_unionC.obj" \
	"$(INTDIR)\old_unionS.obj" \
	"$(INTDIR)\array_onlyC.obj" \
	"$(INTDIR)\array_onlyS.obj" \
	"$(INTDIR)\array_onlyA.obj" \
	"$(INTDIR)\Bug_2582_RegressionC.obj" \
	"$(INTDIR)\Bug_2582_RegressionS.obj" \
	"$(INTDIR)\unionC.obj" \
	"$(INTDIR)\unionS.obj" \
	"$(INTDIR)\Bug_2616_RegressionC.obj" \
	"$(INTDIR)\Bug_2616_RegressionS.obj" \
	"$(INTDIR)\pragmaC.obj" \
	"$(INTDIR)\pragmaS.obj" \
	"$(INTDIR)\pragmaA.obj" \
	"$(INTDIR)\generic_objectC.obj" \
	"$(INTDIR)\generic_objectS.obj" \
	"$(INTDIR)\generic_objectA.obj" \
	"$(INTDIR)\enum_in_structC.obj" \
	"$(INTDIR)\enum_in_structS.obj" \
	"$(INTDIR)\dif2C.obj" \
	"$(INTDIR)\dif2S.obj" \
	"$(INTDIR)\dif2A.obj" \
	"$(INTDIR)\Bug_2619_RegressionC.obj" \
	"$(INTDIR)\Bug_2619_RegressionS.obj" \
	"$(INTDIR)\typedefC.obj" \
	"$(INTDIR)\typedefS.obj" \
	"$(INTDIR)\typedefA.obj" \
	"$(INTDIR)\old_structC.obj" \
	"$(INTDIR)\old_structS.obj" \
	"$(INTDIR)\simpleC.obj" \
	"$(INTDIR)\simpleS.obj" \
	"$(INTDIR)\fwdC.obj" \
	"$(INTDIR)\fwdS.obj" \
	"$(INTDIR)\paramsC.obj" \
	"$(INTDIR)\paramsS.obj" \
	"$(INTDIR)\typecodeC.obj" \
	"$(INTDIR)\typecodeS.obj" \
	"$(INTDIR)\typecodeA.obj" \
	"$(INTDIR)\old_sequenceC.obj" \
	"$(INTDIR)\sequenceC.obj" \
	"$(INTDIR)\old_sequenceS.obj" \
	"$(INTDIR)\sequenceS.obj" \
	"$(INTDIR)\includingC.obj" \
	"$(INTDIR)\includingS.obj" \
	"$(INTDIR)\Bug_2583_RegressionC.obj" \
	"$(INTDIR)\Bug_2583_RegressionS.obj" \
	"$(INTDIR)\typeprefixC.obj" \
	"$(INTDIR)\typeprefixS.obj" \
	"$(INTDIR)\repo_id_modC.obj" \
	"$(INTDIR)\repo_id_modS.obj" \
	"$(INTDIR)\repo_id_modA.obj" \
	"$(INTDIR)\old_arrayC.obj" \
	"$(INTDIR)\old_arrayS.obj" \
	"$(INTDIR)\inheritC.obj" \
	"$(INTDIR)\inheritS.obj" \
	"$(INTDIR)\inheritA.obj" \
	"$(INTDIR)\includedC.obj" \
	"$(INTDIR)\includedS.obj" \
	"$(INTDIR)\reopen_include2C.obj" \
	"$(INTDIR)\reopen_include2S.obj" \
	"$(INTDIR)\reopen_include1C.obj" \
	"$(INTDIR)\reopen_include1S.obj" \
	"$(INTDIR)\old_constantsC.obj" \
	"$(INTDIR)\old_constantsS.obj" \
	"$(INTDIR)\anonymousC.obj" \
	"$(INTDIR)\anonymousS.obj" \
	"$(INTDIR)\anonymousA.obj" \
	"$(INTDIR)\gperfC.obj" \
	"$(INTDIR)\gperfS.obj" \
	"$(INTDIR)\primtypesC.obj" \
	"$(INTDIR)\primtypesS.obj" \
	"$(INTDIR)\primtypesA.obj" \
	"$(INTDIR)\simple2C.obj" \
	"$(INTDIR)\simple2S.obj" \
	"$(INTDIR)\arrayC.obj" \
	"$(INTDIR)\arrayS.obj" \
	"$(INTDIR)\reopened_modulesC.obj" \
	"$(INTDIR)\reopened_modulesS.obj" \
	"$(INTDIR)\fullC.obj" \
	"$(INTDIR)\fullS.obj" \
	"$(INTDIR)\keywordsC.obj" \
	"$(INTDIR)\keywordsS.obj" \
	"$(INTDIR)\keywordsA.obj" \
	"$(INTDIR)\moduleC.obj" \
	"$(INTDIR)\moduleS.obj" \
	"$(INTDIR)\moduleA.obj" \
	"$(INTDIR)\constantsC.obj" \
	"$(INTDIR)\constantsS.obj" \
	"$(INTDIR)\constantsA.obj" \
	"$(INTDIR)\nested_scopeC.obj" \
	"$(INTDIR)\nested_scopeS.obj" \
	"$(INTDIR)\structC.obj" \
	"$(INTDIR)\structS.obj" \
	"$(INTDIR)\old_union2C.obj" \
	"$(INTDIR)\old_union2S.obj" \
	"$(INTDIR)\interfaceC.obj" \
	"$(INTDIR)\interfaceS.obj" \
	"$(INTDIR)\Bug_2350_RegressionC.obj" \
	"$(INTDIR)\Bug_2350_RegressionS.obj" \
	"$(INTDIR)\main.obj"

"$(INSTALLDIR)\main.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\main.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\main.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=Release
INSTALLDIR=Release
INTDIR=Release\IDL_Test_Main\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\main.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -f "Makefile.IDL_Test_Main.dep" "Bug_2577_RegressionC.cpp" "Bug_2577_RegressionS.cpp" "valuetypeC.cpp" "valuetypeS.cpp" "valuetypeA.cpp" "old_unionC.cpp" "old_unionS.cpp" "array_onlyC.cpp" "array_onlyS.cpp" "array_onlyA.cpp" "Bug_2582_RegressionC.cpp" "Bug_2582_RegressionS.cpp" "unionC.cpp" "unionS.cpp" "Bug_2616_RegressionC.cpp" "Bug_2616_RegressionS.cpp" "pragmaC.cpp" "pragmaS.cpp" "pragmaA.cpp" "generic_objectC.cpp" "generic_objectS.cpp" "generic_objectA.cpp" "enum_in_structC.cpp" "enum_in_structS.cpp" "dif2C.cpp" "dif2S.cpp" "dif2A.cpp" "Bug_2619_RegressionC.cpp" "Bug_2619_RegressionS.cpp" "typedefC.cpp" "typedefS.cpp" "typedefA.cpp" "old_structC.cpp" "old_structS.cpp" "simpleC.cpp" "simpleS.cpp" "fwdC.cpp" "fwdS.cpp" "paramsC.cpp" "paramsS.cpp" "typecodeC.cpp" "typecodeS.cpp" "typecodeA.cpp" "old_sequenceC.cpp" "sequenceC.cpp" "old_sequenceS.cpp" "sequenceS.cpp" "includingC.cpp" "includingS.cpp" "Bug_2583_RegressionC.cpp" "Bug_2583_RegressionS.cpp" "typeprefixC.cpp" "typeprefixS.cpp" "repo_id_modC.cpp" "repo_id_modS.cpp" "repo_id_modA.cpp" "old_arrayC.cpp" "old_arrayS.cpp" "inheritC.cpp" "inheritS.cpp" "inheritA.cpp" "includedC.cpp" "includedS.cpp" "reopen_include2C.cpp" "reopen_include2S.cpp" "reopen_include1C.cpp" "reopen_include1S.cpp" "old_constantsC.cpp" "old_constantsS.cpp" "anonymousC.cpp" "anonymousS.cpp" "anonymousA.cpp" "gperfC.cpp" "gperfS.cpp" "primtypesC.cpp" "primtypesS.cpp" "primtypesA.cpp" "simple2C.cpp" "simple2S.cpp" "arrayC.cpp" "arrayS.cpp" "reopened_modulesC.cpp" "reopened_modulesS.cpp" "fullC.cpp" "fullS.cpp" "keywordsC.cpp" "keywordsS.cpp" "keywordsA.cpp" "moduleC.cpp" "moduleS.cpp" "moduleA.cpp" "constantsC.cpp" "constantsS.cpp" "constantsA.cpp" "nested_scopeC.cpp" "nested_scopeS.cpp" "structC.cpp" "structS.cpp" "old_union2C.cpp" "old_union2S.cpp" "interfaceC.cpp" "interfaceS.cpp" "Bug_2350_RegressionC.cpp" "Bug_2350_RegressionS.cpp" "main.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\main.exe"
	-@del /f/q "$(INSTALLDIR)\main.ilk"
	-@del /f/q "arrayC.inl"
	-@del /f/q "arrayS.inl"
	-@del /f/q "arrayC.h"
	-@del /f/q "arrayS.h"
	-@del /f/q "arrayS_T.h"
	-@del /f/q "arrayC.cpp"
	-@del /f/q "arrayS.cpp"
	-@del /f/q "arrayS_T.cpp"
	-@del /f/q "Bug_2350_RegressionC.inl"
	-@del /f/q "Bug_2350_RegressionS.inl"
	-@del /f/q "Bug_2350_RegressionC.h"
	-@del /f/q "Bug_2350_RegressionS.h"
	-@del /f/q "Bug_2350_RegressionS_T.h"
	-@del /f/q "Bug_2350_RegressionC.cpp"
	-@del /f/q "Bug_2350_RegressionS.cpp"
	-@del /f/q "Bug_2350_RegressionS_T.cpp"
	-@del /f/q "Bug_2577_RegressionC.inl"
	-@del /f/q "Bug_2577_RegressionS.inl"
	-@del /f/q "Bug_2577_RegressionC.h"
	-@del /f/q "Bug_2577_RegressionS.h"
	-@del /f/q "Bug_2577_RegressionS_T.h"
	-@del /f/q "Bug_2577_RegressionC.cpp"
	-@del /f/q "Bug_2577_RegressionS.cpp"
	-@del /f/q "Bug_2577_RegressionS_T.cpp"
	-@del /f/q "Bug_2582_RegressionC.inl"
	-@del /f/q "Bug_2582_RegressionS.inl"
	-@del /f/q "Bug_2582_RegressionC.h"
	-@del /f/q "Bug_2582_RegressionS.h"
	-@del /f/q "Bug_2582_RegressionS_T.h"
	-@del /f/q "Bug_2582_RegressionC.cpp"
	-@del /f/q "Bug_2582_RegressionS.cpp"
	-@del /f/q "Bug_2582_RegressionS_T.cpp"
	-@del /f/q "Bug_2583_RegressionC.inl"
	-@del /f/q "Bug_2583_RegressionS.inl"
	-@del /f/q "Bug_2583_RegressionC.h"
	-@del /f/q "Bug_2583_RegressionS.h"
	-@del /f/q "Bug_2583_RegressionS_T.h"
	-@del /f/q "Bug_2583_RegressionC.cpp"
	-@del /f/q "Bug_2583_RegressionS.cpp"
	-@del /f/q "Bug_2583_RegressionS_T.cpp"
	-@del /f/q "Bug_2616_RegressionC.inl"
	-@del /f/q "Bug_2616_RegressionS.inl"
	-@del /f/q "Bug_2616_RegressionC.h"
	-@del /f/q "Bug_2616_RegressionS.h"
	-@del /f/q "Bug_2616_RegressionS_T.h"
	-@del /f/q "Bug_2616_RegressionC.cpp"
	-@del /f/q "Bug_2616_RegressionS.cpp"
	-@del /f/q "Bug_2616_RegressionS_T.cpp"
	-@del /f/q "Bug_2619_RegressionC.inl"
	-@del /f/q "Bug_2619_RegressionS.inl"
	-@del /f/q "Bug_2619_RegressionC.h"
	-@del /f/q "Bug_2619_RegressionS.h"
	-@del /f/q "Bug_2619_RegressionS_T.h"
	-@del /f/q "Bug_2619_RegressionC.cpp"
	-@del /f/q "Bug_2619_RegressionS.cpp"
	-@del /f/q "Bug_2619_RegressionS_T.cpp"
	-@del /f/q "enum_in_structC.inl"
	-@del /f/q "enum_in_structS.inl"
	-@del /f/q "enum_in_structC.h"
	-@del /f/q "enum_in_structS.h"
	-@del /f/q "enum_in_structS_T.h"
	-@del /f/q "enum_in_structC.cpp"
	-@del /f/q "enum_in_structS.cpp"
	-@del /f/q "enum_in_structS_T.cpp"
	-@del /f/q "fullC.inl"
	-@del /f/q "fullS.inl"
	-@del /f/q "fullC.h"
	-@del /f/q "fullS.h"
	-@del /f/q "fullS_T.h"
	-@del /f/q "fullC.cpp"
	-@del /f/q "fullS.cpp"
	-@del /f/q "fullS_T.cpp"
	-@del /f/q "fwdC.inl"
	-@del /f/q "fwdS.inl"
	-@del /f/q "fwdC.h"
	-@del /f/q "fwdS.h"
	-@del /f/q "fwdS_T.h"
	-@del /f/q "fwdC.cpp"
	-@del /f/q "fwdS.cpp"
	-@del /f/q "fwdS_T.cpp"
	-@del /f/q "gperfC.inl"
	-@del /f/q "gperfS.inl"
	-@del /f/q "gperfC.h"
	-@del /f/q "gperfS.h"
	-@del /f/q "gperfS_T.h"
	-@del /f/q "gperfC.cpp"
	-@del /f/q "gperfS.cpp"
	-@del /f/q "gperfS_T.cpp"
	-@del /f/q "includingC.inl"
	-@del /f/q "includingS.inl"
	-@del /f/q "includingC.h"
	-@del /f/q "includingS.h"
	-@del /f/q "includingS_T.h"
	-@del /f/q "includingC.cpp"
	-@del /f/q "includingS.cpp"
	-@del /f/q "includingS_T.cpp"
	-@del /f/q "interfaceC.inl"
	-@del /f/q "interfaceS.inl"
	-@del /f/q "interfaceC.h"
	-@del /f/q "interfaceS.h"
	-@del /f/q "interfaceS_T.h"
	-@del /f/q "interfaceC.cpp"
	-@del /f/q "interfaceS.cpp"
	-@del /f/q "interfaceS_T.cpp"
	-@del /f/q "includedC.inl"
	-@del /f/q "includedS.inl"
	-@del /f/q "includedC.h"
	-@del /f/q "includedS.h"
	-@del /f/q "includedS_T.h"
	-@del /f/q "includedC.cpp"
	-@del /f/q "includedS.cpp"
	-@del /f/q "includedS_T.cpp"
	-@del /f/q "nested_scopeC.inl"
	-@del /f/q "nested_scopeS.inl"
	-@del /f/q "nested_scopeC.h"
	-@del /f/q "nested_scopeS.h"
	-@del /f/q "nested_scopeS_T.h"
	-@del /f/q "nested_scopeC.cpp"
	-@del /f/q "nested_scopeS.cpp"
	-@del /f/q "nested_scopeS_T.cpp"
	-@del /f/q "old_arrayC.inl"
	-@del /f/q "old_arrayS.inl"
	-@del /f/q "old_arrayC.h"
	-@del /f/q "old_arrayS.h"
	-@del /f/q "old_arrayS_T.h"
	-@del /f/q "old_arrayC.cpp"
	-@del /f/q "old_arrayS.cpp"
	-@del /f/q "old_arrayS_T.cpp"
	-@del /f/q "old_constantsC.inl"
	-@del /f/q "old_constantsS.inl"
	-@del /f/q "old_constantsC.h"
	-@del /f/q "old_constantsS.h"
	-@del /f/q "old_constantsS_T.h"
	-@del /f/q "old_constantsC.cpp"
	-@del /f/q "old_constantsS.cpp"
	-@del /f/q "old_constantsS_T.cpp"
	-@del /f/q "old_sequenceC.inl"
	-@del /f/q "old_sequenceS.inl"
	-@del /f/q "old_sequenceC.h"
	-@del /f/q "old_sequenceS.h"
	-@del /f/q "old_sequenceS_T.h"
	-@del /f/q "old_sequenceC.cpp"
	-@del /f/q "old_sequenceS.cpp"
	-@del /f/q "old_sequenceS_T.cpp"
	-@del /f/q "simpleC.inl"
	-@del /f/q "simpleS.inl"
	-@del /f/q "simpleC.h"
	-@del /f/q "simpleS.h"
	-@del /f/q "simpleS_T.h"
	-@del /f/q "simpleC.cpp"
	-@del /f/q "simpleS.cpp"
	-@del /f/q "simpleS_T.cpp"
	-@del /f/q "simple2C.inl"
	-@del /f/q "simple2S.inl"
	-@del /f/q "simple2C.h"
	-@del /f/q "simple2S.h"
	-@del /f/q "simple2S_T.h"
	-@del /f/q "simple2C.cpp"
	-@del /f/q "simple2S.cpp"
	-@del /f/q "simple2S_T.cpp"
	-@del /f/q "old_structC.inl"
	-@del /f/q "old_structS.inl"
	-@del /f/q "old_structC.h"
	-@del /f/q "old_structS.h"
	-@del /f/q "old_structS_T.h"
	-@del /f/q "old_structC.cpp"
	-@del /f/q "old_structS.cpp"
	-@del /f/q "old_structS_T.cpp"
	-@del /f/q "old_unionC.inl"
	-@del /f/q "old_unionS.inl"
	-@del /f/q "old_unionC.h"
	-@del /f/q "old_unionS.h"
	-@del /f/q "old_unionS_T.h"
	-@del /f/q "old_unionC.cpp"
	-@del /f/q "old_unionS.cpp"
	-@del /f/q "old_unionS_T.cpp"
	-@del /f/q "old_union2C.inl"
	-@del /f/q "old_union2S.inl"
	-@del /f/q "old_union2C.h"
	-@del /f/q "old_union2S.h"
	-@del /f/q "old_union2S_T.h"
	-@del /f/q "old_union2C.cpp"
	-@del /f/q "old_union2S.cpp"
	-@del /f/q "old_union2S_T.cpp"
	-@del /f/q "paramsC.inl"
	-@del /f/q "paramsS.inl"
	-@del /f/q "paramsC.h"
	-@del /f/q "paramsS.h"
	-@del /f/q "paramsS_T.h"
	-@del /f/q "paramsC.cpp"
	-@del /f/q "paramsS.cpp"
	-@del /f/q "paramsS_T.cpp"
	-@del /f/q "reopened_modulesC.inl"
	-@del /f/q "reopened_modulesS.inl"
	-@del /f/q "reopened_modulesC.h"
	-@del /f/q "reopened_modulesS.h"
	-@del /f/q "reopened_modulesS_T.h"
	-@del /f/q "reopened_modulesC.cpp"
	-@del /f/q "reopened_modulesS.cpp"
	-@del /f/q "reopened_modulesS_T.cpp"
	-@del /f/q "sequenceC.inl"
	-@del /f/q "sequenceS.inl"
	-@del /f/q "sequenceC.h"
	-@del /f/q "sequenceS.h"
	-@del /f/q "sequenceS_T.h"
	-@del /f/q "sequenceC.cpp"
	-@del /f/q "sequenceS.cpp"
	-@del /f/q "sequenceS_T.cpp"
	-@del /f/q "structC.inl"
	-@del /f/q "structS.inl"
	-@del /f/q "structC.h"
	-@del /f/q "structS.h"
	-@del /f/q "structS_T.h"
	-@del /f/q "structC.cpp"
	-@del /f/q "structS.cpp"
	-@del /f/q "structS_T.cpp"
	-@del /f/q "reopen_include1C.inl"
	-@del /f/q "reopen_include1S.inl"
	-@del /f/q "reopen_include1C.h"
	-@del /f/q "reopen_include1S.h"
	-@del /f/q "reopen_include1S_T.h"
	-@del /f/q "reopen_include1C.cpp"
	-@del /f/q "reopen_include1S.cpp"
	-@del /f/q "reopen_include1S_T.cpp"
	-@del /f/q "reopen_include2C.inl"
	-@del /f/q "reopen_include2S.inl"
	-@del /f/q "reopen_include2C.h"
	-@del /f/q "reopen_include2S.h"
	-@del /f/q "reopen_include2S_T.h"
	-@del /f/q "reopen_include2C.cpp"
	-@del /f/q "reopen_include2S.cpp"
	-@del /f/q "reopen_include2S_T.cpp"
	-@del /f/q "typeprefixC.inl"
	-@del /f/q "typeprefixS.inl"
	-@del /f/q "typeprefixC.h"
	-@del /f/q "typeprefixS.h"
	-@del /f/q "typeprefixS_T.h"
	-@del /f/q "typeprefixC.cpp"
	-@del /f/q "typeprefixS.cpp"
	-@del /f/q "typeprefixS_T.cpp"
	-@del /f/q "unionC.inl"
	-@del /f/q "unionS.inl"
	-@del /f/q "unionC.h"
	-@del /f/q "unionS.h"
	-@del /f/q "unionS_T.h"
	-@del /f/q "unionC.cpp"
	-@del /f/q "unionS.cpp"
	-@del /f/q "unionS_T.cpp"
	-@del /f/q "anonymousC.inl"
	-@del /f/q "anonymousS.inl"
	-@del /f/q "anonymousC.h"
	-@del /f/q "anonymousS.h"
	-@del /f/q "anonymousA.h"
	-@del /f/q "anonymousS_T.h"
	-@del /f/q "anonymousC.cpp"
	-@del /f/q "anonymousS.cpp"
	-@del /f/q "anonymousA.cpp"
	-@del /f/q "anonymousS_T.cpp"
	-@del /f/q "array_onlyC.inl"
	-@del /f/q "array_onlyS.inl"
	-@del /f/q "array_onlyC.h"
	-@del /f/q "array_onlyS.h"
	-@del /f/q "array_onlyA.h"
	-@del /f/q "array_onlyS_T.h"
	-@del /f/q "array_onlyC.cpp"
	-@del /f/q "array_onlyS.cpp"
	-@del /f/q "array_onlyA.cpp"
	-@del /f/q "array_onlyS_T.cpp"
	-@del /f/q "constantsC.inl"
	-@del /f/q "constantsS.inl"
	-@del /f/q "constantsC.h"
	-@del /f/q "constantsS.h"
	-@del /f/q "constantsA.h"
	-@del /f/q "constantsS_T.h"
	-@del /f/q "constantsC.cpp"
	-@del /f/q "constantsS.cpp"
	-@del /f/q "constantsA.cpp"
	-@del /f/q "constantsS_T.cpp"
	-@del /f/q "generic_objectC.inl"
	-@del /f/q "generic_objectS.inl"
	-@del /f/q "generic_objectC.h"
	-@del /f/q "generic_objectS.h"
	-@del /f/q "generic_objectA.h"
	-@del /f/q "generic_objectS_T.h"
	-@del /f/q "generic_objectC.cpp"
	-@del /f/q "generic_objectS.cpp"
	-@del /f/q "generic_objectA.cpp"
	-@del /f/q "generic_objectS_T.cpp"
	-@del /f/q "keywordsC.inl"
	-@del /f/q "keywordsS.inl"
	-@del /f/q "keywordsC.h"
	-@del /f/q "keywordsS.h"
	-@del /f/q "keywordsA.h"
	-@del /f/q "keywordsS_T.h"
	-@del /f/q "keywordsC.cpp"
	-@del /f/q "keywordsS.cpp"
	-@del /f/q "keywordsA.cpp"
	-@del /f/q "keywordsS_T.cpp"
	-@del /f/q "dif2C.inl"
	-@del /f/q "dif2S.inl"
	-@del /f/q "dif2C.h"
	-@del /f/q "dif2S.h"
	-@del /f/q "dif2A.h"
	-@del /f/q "dif2S_T.h"
	-@del /f/q "dif2C.cpp"
	-@del /f/q "dif2S.cpp"
	-@del /f/q "dif2A.cpp"
	-@del /f/q "dif2S_T.cpp"
	-@del /f/q "inheritC.inl"
	-@del /f/q "inheritS.inl"
	-@del /f/q "inheritC.h"
	-@del /f/q "inheritS.h"
	-@del /f/q "inheritA.h"
	-@del /f/q "inheritS_T.h"
	-@del /f/q "inheritC.cpp"
	-@del /f/q "inheritS.cpp"
	-@del /f/q "inheritA.cpp"
	-@del /f/q "inheritS_T.cpp"
	-@del /f/q "moduleC.inl"
	-@del /f/q "moduleS.inl"
	-@del /f/q "moduleC.h"
	-@del /f/q "moduleS.h"
	-@del /f/q "moduleA.h"
	-@del /f/q "moduleS_T.h"
	-@del /f/q "moduleC.cpp"
	-@del /f/q "moduleS.cpp"
	-@del /f/q "moduleA.cpp"
	-@del /f/q "moduleS_T.cpp"
	-@del /f/q "primtypesC.inl"
	-@del /f/q "primtypesS.inl"
	-@del /f/q "primtypesC.h"
	-@del /f/q "primtypesS.h"
	-@del /f/q "primtypesA.h"
	-@del /f/q "primtypesS_T.h"
	-@del /f/q "primtypesC.cpp"
	-@del /f/q "primtypesS.cpp"
	-@del /f/q "primtypesA.cpp"
	-@del /f/q "primtypesS_T.cpp"
	-@del /f/q "pragmaC.inl"
	-@del /f/q "pragmaS.inl"
	-@del /f/q "pragmaC.h"
	-@del /f/q "pragmaS.h"
	-@del /f/q "pragmaA.h"
	-@del /f/q "pragmaS_T.h"
	-@del /f/q "pragmaC.cpp"
	-@del /f/q "pragmaS.cpp"
	-@del /f/q "pragmaA.cpp"
	-@del /f/q "pragmaS_T.cpp"
	-@del /f/q "repo_id_modC.inl"
	-@del /f/q "repo_id_modS.inl"
	-@del /f/q "repo_id_modC.h"
	-@del /f/q "repo_id_modS.h"
	-@del /f/q "repo_id_modA.h"
	-@del /f/q "repo_id_modS_T.h"
	-@del /f/q "repo_id_modC.cpp"
	-@del /f/q "repo_id_modS.cpp"
	-@del /f/q "repo_id_modA.cpp"
	-@del /f/q "repo_id_modS_T.cpp"
	-@del /f/q "typedefC.inl"
	-@del /f/q "typedefS.inl"
	-@del /f/q "typedefC.h"
	-@del /f/q "typedefS.h"
	-@del /f/q "typedefA.h"
	-@del /f/q "typedefS_T.h"
	-@del /f/q "typedefC.cpp"
	-@del /f/q "typedefS.cpp"
	-@del /f/q "typedefA.cpp"
	-@del /f/q "typedefS_T.cpp"
	-@del /f/q "typecodeC.inl"
	-@del /f/q "typecodeS.inl"
	-@del /f/q "typecodeC.h"
	-@del /f/q "typecodeS.h"
	-@del /f/q "typecodeA.h"
	-@del /f/q "typecodeS_T.h"
	-@del /f/q "typecodeC.cpp"
	-@del /f/q "typecodeS.cpp"
	-@del /f/q "typecodeA.cpp"
	-@del /f/q "typecodeS_T.cpp"
	-@del /f/q "valuetypeC.inl"
	-@del /f/q "valuetypeS.inl"
	-@del /f/q "valuetypeC.h"
	-@del /f/q "valuetypeS.h"
	-@del /f/q "valuetypeA.h"
	-@del /f/q "valuetypeS_T.h"
	-@del /f/q "valuetypeC.cpp"
	-@del /f/q "valuetypeS.cpp"
	-@del /f/q "valuetypeA.cpp"
	-@del /f/q "valuetypeS_T.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\IDL_Test_Main\$(NULL)" mkdir "Release\IDL_Test_Main"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\.." /I "..\.." /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_PortableServer.lib TAO_Valuetype.lib TAO_CodecFactory.lib TAO_PI.lib TAO_Messaging.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\main.exe"
LINK32_OBJS= \
	"$(INTDIR)\Bug_2577_RegressionC.obj" \
	"$(INTDIR)\Bug_2577_RegressionS.obj" \
	"$(INTDIR)\valuetypeC.obj" \
	"$(INTDIR)\valuetypeS.obj" \
	"$(INTDIR)\valuetypeA.obj" \
	"$(INTDIR)\old_unionC.obj" \
	"$(INTDIR)\old_unionS.obj" \
	"$(INTDIR)\array_onlyC.obj" \
	"$(INTDIR)\array_onlyS.obj" \
	"$(INTDIR)\array_onlyA.obj" \
	"$(INTDIR)\Bug_2582_RegressionC.obj" \
	"$(INTDIR)\Bug_2582_RegressionS.obj" \
	"$(INTDIR)\unionC.obj" \
	"$(INTDIR)\unionS.obj" \
	"$(INTDIR)\Bug_2616_RegressionC.obj" \
	"$(INTDIR)\Bug_2616_RegressionS.obj" \
	"$(INTDIR)\pragmaC.obj" \
	"$(INTDIR)\pragmaS.obj" \
	"$(INTDIR)\pragmaA.obj" \
	"$(INTDIR)\generic_objectC.obj" \
	"$(INTDIR)\generic_objectS.obj" \
	"$(INTDIR)\generic_objectA.obj" \
	"$(INTDIR)\enum_in_structC.obj" \
	"$(INTDIR)\enum_in_structS.obj" \
	"$(INTDIR)\dif2C.obj" \
	"$(INTDIR)\dif2S.obj" \
	"$(INTDIR)\dif2A.obj" \
	"$(INTDIR)\Bug_2619_RegressionC.obj" \
	"$(INTDIR)\Bug_2619_RegressionS.obj" \
	"$(INTDIR)\typedefC.obj" \
	"$(INTDIR)\typedefS.obj" \
	"$(INTDIR)\typedefA.obj" \
	"$(INTDIR)\old_structC.obj" \
	"$(INTDIR)\old_structS.obj" \
	"$(INTDIR)\simpleC.obj" \
	"$(INTDIR)\simpleS.obj" \
	"$(INTDIR)\fwdC.obj" \
	"$(INTDIR)\fwdS.obj" \
	"$(INTDIR)\paramsC.obj" \
	"$(INTDIR)\paramsS.obj" \
	"$(INTDIR)\typecodeC.obj" \
	"$(INTDIR)\typecodeS.obj" \
	"$(INTDIR)\typecodeA.obj" \
	"$(INTDIR)\old_sequenceC.obj" \
	"$(INTDIR)\sequenceC.obj" \
	"$(INTDIR)\old_sequenceS.obj" \
	"$(INTDIR)\sequenceS.obj" \
	"$(INTDIR)\includingC.obj" \
	"$(INTDIR)\includingS.obj" \
	"$(INTDIR)\Bug_2583_RegressionC.obj" \
	"$(INTDIR)\Bug_2583_RegressionS.obj" \
	"$(INTDIR)\typeprefixC.obj" \
	"$(INTDIR)\typeprefixS.obj" \
	"$(INTDIR)\repo_id_modC.obj" \
	"$(INTDIR)\repo_id_modS.obj" \
	"$(INTDIR)\repo_id_modA.obj" \
	"$(INTDIR)\old_arrayC.obj" \
	"$(INTDIR)\old_arrayS.obj" \
	"$(INTDIR)\inheritC.obj" \
	"$(INTDIR)\inheritS.obj" \
	"$(INTDIR)\inheritA.obj" \
	"$(INTDIR)\includedC.obj" \
	"$(INTDIR)\includedS.obj" \
	"$(INTDIR)\reopen_include2C.obj" \
	"$(INTDIR)\reopen_include2S.obj" \
	"$(INTDIR)\reopen_include1C.obj" \
	"$(INTDIR)\reopen_include1S.obj" \
	"$(INTDIR)\old_constantsC.obj" \
	"$(INTDIR)\old_constantsS.obj" \
	"$(INTDIR)\anonymousC.obj" \
	"$(INTDIR)\anonymousS.obj" \
	"$(INTDIR)\anonymousA.obj" \
	"$(INTDIR)\gperfC.obj" \
	"$(INTDIR)\gperfS.obj" \
	"$(INTDIR)\primtypesC.obj" \
	"$(INTDIR)\primtypesS.obj" \
	"$(INTDIR)\primtypesA.obj" \
	"$(INTDIR)\simple2C.obj" \
	"$(INTDIR)\simple2S.obj" \
	"$(INTDIR)\arrayC.obj" \
	"$(INTDIR)\arrayS.obj" \
	"$(INTDIR)\reopened_modulesC.obj" \
	"$(INTDIR)\reopened_modulesS.obj" \
	"$(INTDIR)\fullC.obj" \
	"$(INTDIR)\fullS.obj" \
	"$(INTDIR)\keywordsC.obj" \
	"$(INTDIR)\keywordsS.obj" \
	"$(INTDIR)\keywordsA.obj" \
	"$(INTDIR)\moduleC.obj" \
	"$(INTDIR)\moduleS.obj" \
	"$(INTDIR)\moduleA.obj" \
	"$(INTDIR)\constantsC.obj" \
	"$(INTDIR)\constantsS.obj" \
	"$(INTDIR)\constantsA.obj" \
	"$(INTDIR)\nested_scopeC.obj" \
	"$(INTDIR)\nested_scopeS.obj" \
	"$(INTDIR)\structC.obj" \
	"$(INTDIR)\structS.obj" \
	"$(INTDIR)\old_union2C.obj" \
	"$(INTDIR)\old_union2S.obj" \
	"$(INTDIR)\interfaceC.obj" \
	"$(INTDIR)\interfaceS.obj" \
	"$(INTDIR)\Bug_2350_RegressionC.obj" \
	"$(INTDIR)\Bug_2350_RegressionS.obj" \
	"$(INTDIR)\main.obj"

"$(INSTALLDIR)\main.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\main.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\main.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=Static_Debug
INSTALLDIR=Static_Debug
INTDIR=Static_Debug\IDL_Test_Main\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\main.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.IDL_Test_Main.dep" "Bug_2577_RegressionC.cpp" "Bug_2577_RegressionS.cpp" "valuetypeC.cpp" "valuetypeS.cpp" "valuetypeA.cpp" "old_unionC.cpp" "old_unionS.cpp" "array_onlyC.cpp" "array_onlyS.cpp" "array_onlyA.cpp" "Bug_2582_RegressionC.cpp" "Bug_2582_RegressionS.cpp" "unionC.cpp" "unionS.cpp" "Bug_2616_RegressionC.cpp" "Bug_2616_RegressionS.cpp" "pragmaC.cpp" "pragmaS.cpp" "pragmaA.cpp" "generic_objectC.cpp" "generic_objectS.cpp" "generic_objectA.cpp" "enum_in_structC.cpp" "enum_in_structS.cpp" "dif2C.cpp" "dif2S.cpp" "dif2A.cpp" "Bug_2619_RegressionC.cpp" "Bug_2619_RegressionS.cpp" "typedefC.cpp" "typedefS.cpp" "typedefA.cpp" "old_structC.cpp" "old_structS.cpp" "simpleC.cpp" "simpleS.cpp" "fwdC.cpp" "fwdS.cpp" "paramsC.cpp" "paramsS.cpp" "typecodeC.cpp" "typecodeS.cpp" "typecodeA.cpp" "old_sequenceC.cpp" "sequenceC.cpp" "old_sequenceS.cpp" "sequenceS.cpp" "includingC.cpp" "includingS.cpp" "Bug_2583_RegressionC.cpp" "Bug_2583_RegressionS.cpp" "typeprefixC.cpp" "typeprefixS.cpp" "repo_id_modC.cpp" "repo_id_modS.cpp" "repo_id_modA.cpp" "old_arrayC.cpp" "old_arrayS.cpp" "inheritC.cpp" "inheritS.cpp" "inheritA.cpp" "includedC.cpp" "includedS.cpp" "reopen_include2C.cpp" "reopen_include2S.cpp" "reopen_include1C.cpp" "reopen_include1S.cpp" "old_constantsC.cpp" "old_constantsS.cpp" "anonymousC.cpp" "anonymousS.cpp" "anonymousA.cpp" "gperfC.cpp" "gperfS.cpp" "primtypesC.cpp" "primtypesS.cpp" "primtypesA.cpp" "simple2C.cpp" "simple2S.cpp" "arrayC.cpp" "arrayS.cpp" "reopened_modulesC.cpp" "reopened_modulesS.cpp" "fullC.cpp" "fullS.cpp" "keywordsC.cpp" "keywordsS.cpp" "keywordsA.cpp" "moduleC.cpp" "moduleS.cpp" "moduleA.cpp" "constantsC.cpp" "constantsS.cpp" "constantsA.cpp" "nested_scopeC.cpp" "nested_scopeS.cpp" "structC.cpp" "structS.cpp" "old_union2C.cpp" "old_union2S.cpp" "interfaceC.cpp" "interfaceS.cpp" "Bug_2350_RegressionC.cpp" "Bug_2350_RegressionS.cpp" "main.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\main.pdb"
	-@del /f/q "$(INSTALLDIR)\main.exe"
	-@del /f/q "$(INSTALLDIR)\main.ilk"
	-@del /f/q "arrayC.inl"
	-@del /f/q "arrayS.inl"
	-@del /f/q "arrayC.h"
	-@del /f/q "arrayS.h"
	-@del /f/q "arrayS_T.h"
	-@del /f/q "arrayC.cpp"
	-@del /f/q "arrayS.cpp"
	-@del /f/q "arrayS_T.cpp"
	-@del /f/q "Bug_2350_RegressionC.inl"
	-@del /f/q "Bug_2350_RegressionS.inl"
	-@del /f/q "Bug_2350_RegressionC.h"
	-@del /f/q "Bug_2350_RegressionS.h"
	-@del /f/q "Bug_2350_RegressionS_T.h"
	-@del /f/q "Bug_2350_RegressionC.cpp"
	-@del /f/q "Bug_2350_RegressionS.cpp"
	-@del /f/q "Bug_2350_RegressionS_T.cpp"
	-@del /f/q "Bug_2577_RegressionC.inl"
	-@del /f/q "Bug_2577_RegressionS.inl"
	-@del /f/q "Bug_2577_RegressionC.h"
	-@del /f/q "Bug_2577_RegressionS.h"
	-@del /f/q "Bug_2577_RegressionS_T.h"
	-@del /f/q "Bug_2577_RegressionC.cpp"
	-@del /f/q "Bug_2577_RegressionS.cpp"
	-@del /f/q "Bug_2577_RegressionS_T.cpp"
	-@del /f/q "Bug_2582_RegressionC.inl"
	-@del /f/q "Bug_2582_RegressionS.inl"
	-@del /f/q "Bug_2582_RegressionC.h"
	-@del /f/q "Bug_2582_RegressionS.h"
	-@del /f/q "Bug_2582_RegressionS_T.h"
	-@del /f/q "Bug_2582_RegressionC.cpp"
	-@del /f/q "Bug_2582_RegressionS.cpp"
	-@del /f/q "Bug_2582_RegressionS_T.cpp"
	-@del /f/q "Bug_2583_RegressionC.inl"
	-@del /f/q "Bug_2583_RegressionS.inl"
	-@del /f/q "Bug_2583_RegressionC.h"
	-@del /f/q "Bug_2583_RegressionS.h"
	-@del /f/q "Bug_2583_RegressionS_T.h"
	-@del /f/q "Bug_2583_RegressionC.cpp"
	-@del /f/q "Bug_2583_RegressionS.cpp"
	-@del /f/q "Bug_2583_RegressionS_T.cpp"
	-@del /f/q "Bug_2616_RegressionC.inl"
	-@del /f/q "Bug_2616_RegressionS.inl"
	-@del /f/q "Bug_2616_RegressionC.h"
	-@del /f/q "Bug_2616_RegressionS.h"
	-@del /f/q "Bug_2616_RegressionS_T.h"
	-@del /f/q "Bug_2616_RegressionC.cpp"
	-@del /f/q "Bug_2616_RegressionS.cpp"
	-@del /f/q "Bug_2616_RegressionS_T.cpp"
	-@del /f/q "Bug_2619_RegressionC.inl"
	-@del /f/q "Bug_2619_RegressionS.inl"
	-@del /f/q "Bug_2619_RegressionC.h"
	-@del /f/q "Bug_2619_RegressionS.h"
	-@del /f/q "Bug_2619_RegressionS_T.h"
	-@del /f/q "Bug_2619_RegressionC.cpp"
	-@del /f/q "Bug_2619_RegressionS.cpp"
	-@del /f/q "Bug_2619_RegressionS_T.cpp"
	-@del /f/q "enum_in_structC.inl"
	-@del /f/q "enum_in_structS.inl"
	-@del /f/q "enum_in_structC.h"
	-@del /f/q "enum_in_structS.h"
	-@del /f/q "enum_in_structS_T.h"
	-@del /f/q "enum_in_structC.cpp"
	-@del /f/q "enum_in_structS.cpp"
	-@del /f/q "enum_in_structS_T.cpp"
	-@del /f/q "fullC.inl"
	-@del /f/q "fullS.inl"
	-@del /f/q "fullC.h"
	-@del /f/q "fullS.h"
	-@del /f/q "fullS_T.h"
	-@del /f/q "fullC.cpp"
	-@del /f/q "fullS.cpp"
	-@del /f/q "fullS_T.cpp"
	-@del /f/q "fwdC.inl"
	-@del /f/q "fwdS.inl"
	-@del /f/q "fwdC.h"
	-@del /f/q "fwdS.h"
	-@del /f/q "fwdS_T.h"
	-@del /f/q "fwdC.cpp"
	-@del /f/q "fwdS.cpp"
	-@del /f/q "fwdS_T.cpp"
	-@del /f/q "gperfC.inl"
	-@del /f/q "gperfS.inl"
	-@del /f/q "gperfC.h"
	-@del /f/q "gperfS.h"
	-@del /f/q "gperfS_T.h"
	-@del /f/q "gperfC.cpp"
	-@del /f/q "gperfS.cpp"
	-@del /f/q "gperfS_T.cpp"
	-@del /f/q "includingC.inl"
	-@del /f/q "includingS.inl"
	-@del /f/q "includingC.h"
	-@del /f/q "includingS.h"
	-@del /f/q "includingS_T.h"
	-@del /f/q "includingC.cpp"
	-@del /f/q "includingS.cpp"
	-@del /f/q "includingS_T.cpp"
	-@del /f/q "interfaceC.inl"
	-@del /f/q "interfaceS.inl"
	-@del /f/q "interfaceC.h"
	-@del /f/q "interfaceS.h"
	-@del /f/q "interfaceS_T.h"
	-@del /f/q "interfaceC.cpp"
	-@del /f/q "interfaceS.cpp"
	-@del /f/q "interfaceS_T.cpp"
	-@del /f/q "includedC.inl"
	-@del /f/q "includedS.inl"
	-@del /f/q "includedC.h"
	-@del /f/q "includedS.h"
	-@del /f/q "includedS_T.h"
	-@del /f/q "includedC.cpp"
	-@del /f/q "includedS.cpp"
	-@del /f/q "includedS_T.cpp"
	-@del /f/q "nested_scopeC.inl"
	-@del /f/q "nested_scopeS.inl"
	-@del /f/q "nested_scopeC.h"
	-@del /f/q "nested_scopeS.h"
	-@del /f/q "nested_scopeS_T.h"
	-@del /f/q "nested_scopeC.cpp"
	-@del /f/q "nested_scopeS.cpp"
	-@del /f/q "nested_scopeS_T.cpp"
	-@del /f/q "old_arrayC.inl"
	-@del /f/q "old_arrayS.inl"
	-@del /f/q "old_arrayC.h"
	-@del /f/q "old_arrayS.h"
	-@del /f/q "old_arrayS_T.h"
	-@del /f/q "old_arrayC.cpp"
	-@del /f/q "old_arrayS.cpp"
	-@del /f/q "old_arrayS_T.cpp"
	-@del /f/q "old_constantsC.inl"
	-@del /f/q "old_constantsS.inl"
	-@del /f/q "old_constantsC.h"
	-@del /f/q "old_constantsS.h"
	-@del /f/q "old_constantsS_T.h"
	-@del /f/q "old_constantsC.cpp"
	-@del /f/q "old_constantsS.cpp"
	-@del /f/q "old_constantsS_T.cpp"
	-@del /f/q "old_sequenceC.inl"
	-@del /f/q "old_sequenceS.inl"
	-@del /f/q "old_sequenceC.h"
	-@del /f/q "old_sequenceS.h"
	-@del /f/q "old_sequenceS_T.h"
	-@del /f/q "old_sequenceC.cpp"
	-@del /f/q "old_sequenceS.cpp"
	-@del /f/q "old_sequenceS_T.cpp"
	-@del /f/q "simpleC.inl"
	-@del /f/q "simpleS.inl"
	-@del /f/q "simpleC.h"
	-@del /f/q "simpleS.h"
	-@del /f/q "simpleS_T.h"
	-@del /f/q "simpleC.cpp"
	-@del /f/q "simpleS.cpp"
	-@del /f/q "simpleS_T.cpp"
	-@del /f/q "simple2C.inl"
	-@del /f/q "simple2S.inl"
	-@del /f/q "simple2C.h"
	-@del /f/q "simple2S.h"
	-@del /f/q "simple2S_T.h"
	-@del /f/q "simple2C.cpp"
	-@del /f/q "simple2S.cpp"
	-@del /f/q "simple2S_T.cpp"
	-@del /f/q "old_structC.inl"
	-@del /f/q "old_structS.inl"
	-@del /f/q "old_structC.h"
	-@del /f/q "old_structS.h"
	-@del /f/q "old_structS_T.h"
	-@del /f/q "old_structC.cpp"
	-@del /f/q "old_structS.cpp"
	-@del /f/q "old_structS_T.cpp"
	-@del /f/q "old_unionC.inl"
	-@del /f/q "old_unionS.inl"
	-@del /f/q "old_unionC.h"
	-@del /f/q "old_unionS.h"
	-@del /f/q "old_unionS_T.h"
	-@del /f/q "old_unionC.cpp"
	-@del /f/q "old_unionS.cpp"
	-@del /f/q "old_unionS_T.cpp"
	-@del /f/q "old_union2C.inl"
	-@del /f/q "old_union2S.inl"
	-@del /f/q "old_union2C.h"
	-@del /f/q "old_union2S.h"
	-@del /f/q "old_union2S_T.h"
	-@del /f/q "old_union2C.cpp"
	-@del /f/q "old_union2S.cpp"
	-@del /f/q "old_union2S_T.cpp"
	-@del /f/q "paramsC.inl"
	-@del /f/q "paramsS.inl"
	-@del /f/q "paramsC.h"
	-@del /f/q "paramsS.h"
	-@del /f/q "paramsS_T.h"
	-@del /f/q "paramsC.cpp"
	-@del /f/q "paramsS.cpp"
	-@del /f/q "paramsS_T.cpp"
	-@del /f/q "reopened_modulesC.inl"
	-@del /f/q "reopened_modulesS.inl"
	-@del /f/q "reopened_modulesC.h"
	-@del /f/q "reopened_modulesS.h"
	-@del /f/q "reopened_modulesS_T.h"
	-@del /f/q "reopened_modulesC.cpp"
	-@del /f/q "reopened_modulesS.cpp"
	-@del /f/q "reopened_modulesS_T.cpp"
	-@del /f/q "sequenceC.inl"
	-@del /f/q "sequenceS.inl"
	-@del /f/q "sequenceC.h"
	-@del /f/q "sequenceS.h"
	-@del /f/q "sequenceS_T.h"
	-@del /f/q "sequenceC.cpp"
	-@del /f/q "sequenceS.cpp"
	-@del /f/q "sequenceS_T.cpp"
	-@del /f/q "structC.inl"
	-@del /f/q "structS.inl"
	-@del /f/q "structC.h"
	-@del /f/q "structS.h"
	-@del /f/q "structS_T.h"
	-@del /f/q "structC.cpp"
	-@del /f/q "structS.cpp"
	-@del /f/q "structS_T.cpp"
	-@del /f/q "reopen_include1C.inl"
	-@del /f/q "reopen_include1S.inl"
	-@del /f/q "reopen_include1C.h"
	-@del /f/q "reopen_include1S.h"
	-@del /f/q "reopen_include1S_T.h"
	-@del /f/q "reopen_include1C.cpp"
	-@del /f/q "reopen_include1S.cpp"
	-@del /f/q "reopen_include1S_T.cpp"
	-@del /f/q "reopen_include2C.inl"
	-@del /f/q "reopen_include2S.inl"
	-@del /f/q "reopen_include2C.h"
	-@del /f/q "reopen_include2S.h"
	-@del /f/q "reopen_include2S_T.h"
	-@del /f/q "reopen_include2C.cpp"
	-@del /f/q "reopen_include2S.cpp"
	-@del /f/q "reopen_include2S_T.cpp"
	-@del /f/q "typeprefixC.inl"
	-@del /f/q "typeprefixS.inl"
	-@del /f/q "typeprefixC.h"
	-@del /f/q "typeprefixS.h"
	-@del /f/q "typeprefixS_T.h"
	-@del /f/q "typeprefixC.cpp"
	-@del /f/q "typeprefixS.cpp"
	-@del /f/q "typeprefixS_T.cpp"
	-@del /f/q "unionC.inl"
	-@del /f/q "unionS.inl"
	-@del /f/q "unionC.h"
	-@del /f/q "unionS.h"
	-@del /f/q "unionS_T.h"
	-@del /f/q "unionC.cpp"
	-@del /f/q "unionS.cpp"
	-@del /f/q "unionS_T.cpp"
	-@del /f/q "anonymousC.inl"
	-@del /f/q "anonymousS.inl"
	-@del /f/q "anonymousC.h"
	-@del /f/q "anonymousS.h"
	-@del /f/q "anonymousA.h"
	-@del /f/q "anonymousS_T.h"
	-@del /f/q "anonymousC.cpp"
	-@del /f/q "anonymousS.cpp"
	-@del /f/q "anonymousA.cpp"
	-@del /f/q "anonymousS_T.cpp"
	-@del /f/q "array_onlyC.inl"
	-@del /f/q "array_onlyS.inl"
	-@del /f/q "array_onlyC.h"
	-@del /f/q "array_onlyS.h"
	-@del /f/q "array_onlyA.h"
	-@del /f/q "array_onlyS_T.h"
	-@del /f/q "array_onlyC.cpp"
	-@del /f/q "array_onlyS.cpp"
	-@del /f/q "array_onlyA.cpp"
	-@del /f/q "array_onlyS_T.cpp"
	-@del /f/q "constantsC.inl"
	-@del /f/q "constantsS.inl"
	-@del /f/q "constantsC.h"
	-@del /f/q "constantsS.h"
	-@del /f/q "constantsA.h"
	-@del /f/q "constantsS_T.h"
	-@del /f/q "constantsC.cpp"
	-@del /f/q "constantsS.cpp"
	-@del /f/q "constantsA.cpp"
	-@del /f/q "constantsS_T.cpp"
	-@del /f/q "generic_objectC.inl"
	-@del /f/q "generic_objectS.inl"
	-@del /f/q "generic_objectC.h"
	-@del /f/q "generic_objectS.h"
	-@del /f/q "generic_objectA.h"
	-@del /f/q "generic_objectS_T.h"
	-@del /f/q "generic_objectC.cpp"
	-@del /f/q "generic_objectS.cpp"
	-@del /f/q "generic_objectA.cpp"
	-@del /f/q "generic_objectS_T.cpp"
	-@del /f/q "keywordsC.inl"
	-@del /f/q "keywordsS.inl"
	-@del /f/q "keywordsC.h"
	-@del /f/q "keywordsS.h"
	-@del /f/q "keywordsA.h"
	-@del /f/q "keywordsS_T.h"
	-@del /f/q "keywordsC.cpp"
	-@del /f/q "keywordsS.cpp"
	-@del /f/q "keywordsA.cpp"
	-@del /f/q "keywordsS_T.cpp"
	-@del /f/q "dif2C.inl"
	-@del /f/q "dif2S.inl"
	-@del /f/q "dif2C.h"
	-@del /f/q "dif2S.h"
	-@del /f/q "dif2A.h"
	-@del /f/q "dif2S_T.h"
	-@del /f/q "dif2C.cpp"
	-@del /f/q "dif2S.cpp"
	-@del /f/q "dif2A.cpp"
	-@del /f/q "dif2S_T.cpp"
	-@del /f/q "inheritC.inl"
	-@del /f/q "inheritS.inl"
	-@del /f/q "inheritC.h"
	-@del /f/q "inheritS.h"
	-@del /f/q "inheritA.h"
	-@del /f/q "inheritS_T.h"
	-@del /f/q "inheritC.cpp"
	-@del /f/q "inheritS.cpp"
	-@del /f/q "inheritA.cpp"
	-@del /f/q "inheritS_T.cpp"
	-@del /f/q "moduleC.inl"
	-@del /f/q "moduleS.inl"
	-@del /f/q "moduleC.h"
	-@del /f/q "moduleS.h"
	-@del /f/q "moduleA.h"
	-@del /f/q "moduleS_T.h"
	-@del /f/q "moduleC.cpp"
	-@del /f/q "moduleS.cpp"
	-@del /f/q "moduleA.cpp"
	-@del /f/q "moduleS_T.cpp"
	-@del /f/q "primtypesC.inl"
	-@del /f/q "primtypesS.inl"
	-@del /f/q "primtypesC.h"
	-@del /f/q "primtypesS.h"
	-@del /f/q "primtypesA.h"
	-@del /f/q "primtypesS_T.h"
	-@del /f/q "primtypesC.cpp"
	-@del /f/q "primtypesS.cpp"
	-@del /f/q "primtypesA.cpp"
	-@del /f/q "primtypesS_T.cpp"
	-@del /f/q "pragmaC.inl"
	-@del /f/q "pragmaS.inl"
	-@del /f/q "pragmaC.h"
	-@del /f/q "pragmaS.h"
	-@del /f/q "pragmaA.h"
	-@del /f/q "pragmaS_T.h"
	-@del /f/q "pragmaC.cpp"
	-@del /f/q "pragmaS.cpp"
	-@del /f/q "pragmaA.cpp"
	-@del /f/q "pragmaS_T.cpp"
	-@del /f/q "repo_id_modC.inl"
	-@del /f/q "repo_id_modS.inl"
	-@del /f/q "repo_id_modC.h"
	-@del /f/q "repo_id_modS.h"
	-@del /f/q "repo_id_modA.h"
	-@del /f/q "repo_id_modS_T.h"
	-@del /f/q "repo_id_modC.cpp"
	-@del /f/q "repo_id_modS.cpp"
	-@del /f/q "repo_id_modA.cpp"
	-@del /f/q "repo_id_modS_T.cpp"
	-@del /f/q "typedefC.inl"
	-@del /f/q "typedefS.inl"
	-@del /f/q "typedefC.h"
	-@del /f/q "typedefS.h"
	-@del /f/q "typedefA.h"
	-@del /f/q "typedefS_T.h"
	-@del /f/q "typedefC.cpp"
	-@del /f/q "typedefS.cpp"
	-@del /f/q "typedefA.cpp"
	-@del /f/q "typedefS_T.cpp"
	-@del /f/q "typecodeC.inl"
	-@del /f/q "typecodeS.inl"
	-@del /f/q "typecodeC.h"
	-@del /f/q "typecodeS.h"
	-@del /f/q "typecodeA.h"
	-@del /f/q "typecodeS_T.h"
	-@del /f/q "typecodeC.cpp"
	-@del /f/q "typecodeS.cpp"
	-@del /f/q "typecodeA.cpp"
	-@del /f/q "typecodeS_T.cpp"
	-@del /f/q "valuetypeC.inl"
	-@del /f/q "valuetypeS.inl"
	-@del /f/q "valuetypeC.h"
	-@del /f/q "valuetypeS.h"
	-@del /f/q "valuetypeA.h"
	-@del /f/q "valuetypeS_T.h"
	-@del /f/q "valuetypeC.cpp"
	-@del /f/q "valuetypeS.cpp"
	-@del /f/q "valuetypeA.cpp"
	-@del /f/q "valuetypeS_T.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\IDL_Test_Main\$(NULL)" mkdir "Static_Debug\IDL_Test_Main"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\.." /I "..\.." /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEsd.lib TAOsd.lib TAO_AnyTypeCodesd.lib TAO_PortableServersd.lib TAO_Valuetypesd.lib TAO_CodecFactorysd.lib TAO_PIsd.lib TAO_Messagingsd.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\main.pdb" /machine:IA64 /out:"$(INSTALLDIR)\main.exe"
LINK32_OBJS= \
	"$(INTDIR)\Bug_2577_RegressionC.obj" \
	"$(INTDIR)\Bug_2577_RegressionS.obj" \
	"$(INTDIR)\valuetypeC.obj" \
	"$(INTDIR)\valuetypeS.obj" \
	"$(INTDIR)\valuetypeA.obj" \
	"$(INTDIR)\old_unionC.obj" \
	"$(INTDIR)\old_unionS.obj" \
	"$(INTDIR)\array_onlyC.obj" \
	"$(INTDIR)\array_onlyS.obj" \
	"$(INTDIR)\array_onlyA.obj" \
	"$(INTDIR)\Bug_2582_RegressionC.obj" \
	"$(INTDIR)\Bug_2582_RegressionS.obj" \
	"$(INTDIR)\unionC.obj" \
	"$(INTDIR)\unionS.obj" \
	"$(INTDIR)\Bug_2616_RegressionC.obj" \
	"$(INTDIR)\Bug_2616_RegressionS.obj" \
	"$(INTDIR)\pragmaC.obj" \
	"$(INTDIR)\pragmaS.obj" \
	"$(INTDIR)\pragmaA.obj" \
	"$(INTDIR)\generic_objectC.obj" \
	"$(INTDIR)\generic_objectS.obj" \
	"$(INTDIR)\generic_objectA.obj" \
	"$(INTDIR)\enum_in_structC.obj" \
	"$(INTDIR)\enum_in_structS.obj" \
	"$(INTDIR)\dif2C.obj" \
	"$(INTDIR)\dif2S.obj" \
	"$(INTDIR)\dif2A.obj" \
	"$(INTDIR)\Bug_2619_RegressionC.obj" \
	"$(INTDIR)\Bug_2619_RegressionS.obj" \
	"$(INTDIR)\typedefC.obj" \
	"$(INTDIR)\typedefS.obj" \
	"$(INTDIR)\typedefA.obj" \
	"$(INTDIR)\old_structC.obj" \
	"$(INTDIR)\old_structS.obj" \
	"$(INTDIR)\simpleC.obj" \
	"$(INTDIR)\simpleS.obj" \
	"$(INTDIR)\fwdC.obj" \
	"$(INTDIR)\fwdS.obj" \
	"$(INTDIR)\paramsC.obj" \
	"$(INTDIR)\paramsS.obj" \
	"$(INTDIR)\typecodeC.obj" \
	"$(INTDIR)\typecodeS.obj" \
	"$(INTDIR)\typecodeA.obj" \
	"$(INTDIR)\old_sequenceC.obj" \
	"$(INTDIR)\sequenceC.obj" \
	"$(INTDIR)\old_sequenceS.obj" \
	"$(INTDIR)\sequenceS.obj" \
	"$(INTDIR)\includingC.obj" \
	"$(INTDIR)\includingS.obj" \
	"$(INTDIR)\Bug_2583_RegressionC.obj" \
	"$(INTDIR)\Bug_2583_RegressionS.obj" \
	"$(INTDIR)\typeprefixC.obj" \
	"$(INTDIR)\typeprefixS.obj" \
	"$(INTDIR)\repo_id_modC.obj" \
	"$(INTDIR)\repo_id_modS.obj" \
	"$(INTDIR)\repo_id_modA.obj" \
	"$(INTDIR)\old_arrayC.obj" \
	"$(INTDIR)\old_arrayS.obj" \
	"$(INTDIR)\inheritC.obj" \
	"$(INTDIR)\inheritS.obj" \
	"$(INTDIR)\inheritA.obj" \
	"$(INTDIR)\includedC.obj" \
	"$(INTDIR)\includedS.obj" \
	"$(INTDIR)\reopen_include2C.obj" \
	"$(INTDIR)\reopen_include2S.obj" \
	"$(INTDIR)\reopen_include1C.obj" \
	"$(INTDIR)\reopen_include1S.obj" \
	"$(INTDIR)\old_constantsC.obj" \
	"$(INTDIR)\old_constantsS.obj" \
	"$(INTDIR)\anonymousC.obj" \
	"$(INTDIR)\anonymousS.obj" \
	"$(INTDIR)\anonymousA.obj" \
	"$(INTDIR)\gperfC.obj" \
	"$(INTDIR)\gperfS.obj" \
	"$(INTDIR)\primtypesC.obj" \
	"$(INTDIR)\primtypesS.obj" \
	"$(INTDIR)\primtypesA.obj" \
	"$(INTDIR)\simple2C.obj" \
	"$(INTDIR)\simple2S.obj" \
	"$(INTDIR)\arrayC.obj" \
	"$(INTDIR)\arrayS.obj" \
	"$(INTDIR)\reopened_modulesC.obj" \
	"$(INTDIR)\reopened_modulesS.obj" \
	"$(INTDIR)\fullC.obj" \
	"$(INTDIR)\fullS.obj" \
	"$(INTDIR)\keywordsC.obj" \
	"$(INTDIR)\keywordsS.obj" \
	"$(INTDIR)\keywordsA.obj" \
	"$(INTDIR)\moduleC.obj" \
	"$(INTDIR)\moduleS.obj" \
	"$(INTDIR)\moduleA.obj" \
	"$(INTDIR)\constantsC.obj" \
	"$(INTDIR)\constantsS.obj" \
	"$(INTDIR)\constantsA.obj" \
	"$(INTDIR)\nested_scopeC.obj" \
	"$(INTDIR)\nested_scopeS.obj" \
	"$(INTDIR)\structC.obj" \
	"$(INTDIR)\structS.obj" \
	"$(INTDIR)\old_union2C.obj" \
	"$(INTDIR)\old_union2S.obj" \
	"$(INTDIR)\interfaceC.obj" \
	"$(INTDIR)\interfaceS.obj" \
	"$(INTDIR)\Bug_2350_RegressionC.obj" \
	"$(INTDIR)\Bug_2350_RegressionS.obj" \
	"$(INTDIR)\main.obj"

"$(INSTALLDIR)\main.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\main.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\main.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=Static_Release
INSTALLDIR=Static_Release
INTDIR=Static_Release\IDL_Test_Main\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\main.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.IDL_Test_Main.dep" "Bug_2577_RegressionC.cpp" "Bug_2577_RegressionS.cpp" "valuetypeC.cpp" "valuetypeS.cpp" "valuetypeA.cpp" "old_unionC.cpp" "old_unionS.cpp" "array_onlyC.cpp" "array_onlyS.cpp" "array_onlyA.cpp" "Bug_2582_RegressionC.cpp" "Bug_2582_RegressionS.cpp" "unionC.cpp" "unionS.cpp" "Bug_2616_RegressionC.cpp" "Bug_2616_RegressionS.cpp" "pragmaC.cpp" "pragmaS.cpp" "pragmaA.cpp" "generic_objectC.cpp" "generic_objectS.cpp" "generic_objectA.cpp" "enum_in_structC.cpp" "enum_in_structS.cpp" "dif2C.cpp" "dif2S.cpp" "dif2A.cpp" "Bug_2619_RegressionC.cpp" "Bug_2619_RegressionS.cpp" "typedefC.cpp" "typedefS.cpp" "typedefA.cpp" "old_structC.cpp" "old_structS.cpp" "simpleC.cpp" "simpleS.cpp" "fwdC.cpp" "fwdS.cpp" "paramsC.cpp" "paramsS.cpp" "typecodeC.cpp" "typecodeS.cpp" "typecodeA.cpp" "old_sequenceC.cpp" "sequenceC.cpp" "old_sequenceS.cpp" "sequenceS.cpp" "includingC.cpp" "includingS.cpp" "Bug_2583_RegressionC.cpp" "Bug_2583_RegressionS.cpp" "typeprefixC.cpp" "typeprefixS.cpp" "repo_id_modC.cpp" "repo_id_modS.cpp" "repo_id_modA.cpp" "old_arrayC.cpp" "old_arrayS.cpp" "inheritC.cpp" "inheritS.cpp" "inheritA.cpp" "includedC.cpp" "includedS.cpp" "reopen_include2C.cpp" "reopen_include2S.cpp" "reopen_include1C.cpp" "reopen_include1S.cpp" "old_constantsC.cpp" "old_constantsS.cpp" "anonymousC.cpp" "anonymousS.cpp" "anonymousA.cpp" "gperfC.cpp" "gperfS.cpp" "primtypesC.cpp" "primtypesS.cpp" "primtypesA.cpp" "simple2C.cpp" "simple2S.cpp" "arrayC.cpp" "arrayS.cpp" "reopened_modulesC.cpp" "reopened_modulesS.cpp" "fullC.cpp" "fullS.cpp" "keywordsC.cpp" "keywordsS.cpp" "keywordsA.cpp" "moduleC.cpp" "moduleS.cpp" "moduleA.cpp" "constantsC.cpp" "constantsS.cpp" "constantsA.cpp" "nested_scopeC.cpp" "nested_scopeS.cpp" "structC.cpp" "structS.cpp" "old_union2C.cpp" "old_union2S.cpp" "interfaceC.cpp" "interfaceS.cpp" "Bug_2350_RegressionC.cpp" "Bug_2350_RegressionS.cpp" "main.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\main.exe"
	-@del /f/q "$(INSTALLDIR)\main.ilk"
	-@del /f/q "arrayC.inl"
	-@del /f/q "arrayS.inl"
	-@del /f/q "arrayC.h"
	-@del /f/q "arrayS.h"
	-@del /f/q "arrayS_T.h"
	-@del /f/q "arrayC.cpp"
	-@del /f/q "arrayS.cpp"
	-@del /f/q "arrayS_T.cpp"
	-@del /f/q "Bug_2350_RegressionC.inl"
	-@del /f/q "Bug_2350_RegressionS.inl"
	-@del /f/q "Bug_2350_RegressionC.h"
	-@del /f/q "Bug_2350_RegressionS.h"
	-@del /f/q "Bug_2350_RegressionS_T.h"
	-@del /f/q "Bug_2350_RegressionC.cpp"
	-@del /f/q "Bug_2350_RegressionS.cpp"
	-@del /f/q "Bug_2350_RegressionS_T.cpp"
	-@del /f/q "Bug_2577_RegressionC.inl"
	-@del /f/q "Bug_2577_RegressionS.inl"
	-@del /f/q "Bug_2577_RegressionC.h"
	-@del /f/q "Bug_2577_RegressionS.h"
	-@del /f/q "Bug_2577_RegressionS_T.h"
	-@del /f/q "Bug_2577_RegressionC.cpp"
	-@del /f/q "Bug_2577_RegressionS.cpp"
	-@del /f/q "Bug_2577_RegressionS_T.cpp"
	-@del /f/q "Bug_2582_RegressionC.inl"
	-@del /f/q "Bug_2582_RegressionS.inl"
	-@del /f/q "Bug_2582_RegressionC.h"
	-@del /f/q "Bug_2582_RegressionS.h"
	-@del /f/q "Bug_2582_RegressionS_T.h"
	-@del /f/q "Bug_2582_RegressionC.cpp"
	-@del /f/q "Bug_2582_RegressionS.cpp"
	-@del /f/q "Bug_2582_RegressionS_T.cpp"
	-@del /f/q "Bug_2583_RegressionC.inl"
	-@del /f/q "Bug_2583_RegressionS.inl"
	-@del /f/q "Bug_2583_RegressionC.h"
	-@del /f/q "Bug_2583_RegressionS.h"
	-@del /f/q "Bug_2583_RegressionS_T.h"
	-@del /f/q "Bug_2583_RegressionC.cpp"
	-@del /f/q "Bug_2583_RegressionS.cpp"
	-@del /f/q "Bug_2583_RegressionS_T.cpp"
	-@del /f/q "Bug_2616_RegressionC.inl"
	-@del /f/q "Bug_2616_RegressionS.inl"
	-@del /f/q "Bug_2616_RegressionC.h"
	-@del /f/q "Bug_2616_RegressionS.h"
	-@del /f/q "Bug_2616_RegressionS_T.h"
	-@del /f/q "Bug_2616_RegressionC.cpp"
	-@del /f/q "Bug_2616_RegressionS.cpp"
	-@del /f/q "Bug_2616_RegressionS_T.cpp"
	-@del /f/q "Bug_2619_RegressionC.inl"
	-@del /f/q "Bug_2619_RegressionS.inl"
	-@del /f/q "Bug_2619_RegressionC.h"
	-@del /f/q "Bug_2619_RegressionS.h"
	-@del /f/q "Bug_2619_RegressionS_T.h"
	-@del /f/q "Bug_2619_RegressionC.cpp"
	-@del /f/q "Bug_2619_RegressionS.cpp"
	-@del /f/q "Bug_2619_RegressionS_T.cpp"
	-@del /f/q "enum_in_structC.inl"
	-@del /f/q "enum_in_structS.inl"
	-@del /f/q "enum_in_structC.h"
	-@del /f/q "enum_in_structS.h"
	-@del /f/q "enum_in_structS_T.h"
	-@del /f/q "enum_in_structC.cpp"
	-@del /f/q "enum_in_structS.cpp"
	-@del /f/q "enum_in_structS_T.cpp"
	-@del /f/q "fullC.inl"
	-@del /f/q "fullS.inl"
	-@del /f/q "fullC.h"
	-@del /f/q "fullS.h"
	-@del /f/q "fullS_T.h"
	-@del /f/q "fullC.cpp"
	-@del /f/q "fullS.cpp"
	-@del /f/q "fullS_T.cpp"
	-@del /f/q "fwdC.inl"
	-@del /f/q "fwdS.inl"
	-@del /f/q "fwdC.h"
	-@del /f/q "fwdS.h"
	-@del /f/q "fwdS_T.h"
	-@del /f/q "fwdC.cpp"
	-@del /f/q "fwdS.cpp"
	-@del /f/q "fwdS_T.cpp"
	-@del /f/q "gperfC.inl"
	-@del /f/q "gperfS.inl"
	-@del /f/q "gperfC.h"
	-@del /f/q "gperfS.h"
	-@del /f/q "gperfS_T.h"
	-@del /f/q "gperfC.cpp"
	-@del /f/q "gperfS.cpp"
	-@del /f/q "gperfS_T.cpp"
	-@del /f/q "includingC.inl"
	-@del /f/q "includingS.inl"
	-@del /f/q "includingC.h"
	-@del /f/q "includingS.h"
	-@del /f/q "includingS_T.h"
	-@del /f/q "includingC.cpp"
	-@del /f/q "includingS.cpp"
	-@del /f/q "includingS_T.cpp"
	-@del /f/q "interfaceC.inl"
	-@del /f/q "interfaceS.inl"
	-@del /f/q "interfaceC.h"
	-@del /f/q "interfaceS.h"
	-@del /f/q "interfaceS_T.h"
	-@del /f/q "interfaceC.cpp"
	-@del /f/q "interfaceS.cpp"
	-@del /f/q "interfaceS_T.cpp"
	-@del /f/q "includedC.inl"
	-@del /f/q "includedS.inl"
	-@del /f/q "includedC.h"
	-@del /f/q "includedS.h"
	-@del /f/q "includedS_T.h"
	-@del /f/q "includedC.cpp"
	-@del /f/q "includedS.cpp"
	-@del /f/q "includedS_T.cpp"
	-@del /f/q "nested_scopeC.inl"
	-@del /f/q "nested_scopeS.inl"
	-@del /f/q "nested_scopeC.h"
	-@del /f/q "nested_scopeS.h"
	-@del /f/q "nested_scopeS_T.h"
	-@del /f/q "nested_scopeC.cpp"
	-@del /f/q "nested_scopeS.cpp"
	-@del /f/q "nested_scopeS_T.cpp"
	-@del /f/q "old_arrayC.inl"
	-@del /f/q "old_arrayS.inl"
	-@del /f/q "old_arrayC.h"
	-@del /f/q "old_arrayS.h"
	-@del /f/q "old_arrayS_T.h"
	-@del /f/q "old_arrayC.cpp"
	-@del /f/q "old_arrayS.cpp"
	-@del /f/q "old_arrayS_T.cpp"
	-@del /f/q "old_constantsC.inl"
	-@del /f/q "old_constantsS.inl"
	-@del /f/q "old_constantsC.h"
	-@del /f/q "old_constantsS.h"
	-@del /f/q "old_constantsS_T.h"
	-@del /f/q "old_constantsC.cpp"
	-@del /f/q "old_constantsS.cpp"
	-@del /f/q "old_constantsS_T.cpp"
	-@del /f/q "old_sequenceC.inl"
	-@del /f/q "old_sequenceS.inl"
	-@del /f/q "old_sequenceC.h"
	-@del /f/q "old_sequenceS.h"
	-@del /f/q "old_sequenceS_T.h"
	-@del /f/q "old_sequenceC.cpp"
	-@del /f/q "old_sequenceS.cpp"
	-@del /f/q "old_sequenceS_T.cpp"
	-@del /f/q "simpleC.inl"
	-@del /f/q "simpleS.inl"
	-@del /f/q "simpleC.h"
	-@del /f/q "simpleS.h"
	-@del /f/q "simpleS_T.h"
	-@del /f/q "simpleC.cpp"
	-@del /f/q "simpleS.cpp"
	-@del /f/q "simpleS_T.cpp"
	-@del /f/q "simple2C.inl"
	-@del /f/q "simple2S.inl"
	-@del /f/q "simple2C.h"
	-@del /f/q "simple2S.h"
	-@del /f/q "simple2S_T.h"
	-@del /f/q "simple2C.cpp"
	-@del /f/q "simple2S.cpp"
	-@del /f/q "simple2S_T.cpp"
	-@del /f/q "old_structC.inl"
	-@del /f/q "old_structS.inl"
	-@del /f/q "old_structC.h"
	-@del /f/q "old_structS.h"
	-@del /f/q "old_structS_T.h"
	-@del /f/q "old_structC.cpp"
	-@del /f/q "old_structS.cpp"
	-@del /f/q "old_structS_T.cpp"
	-@del /f/q "old_unionC.inl"
	-@del /f/q "old_unionS.inl"
	-@del /f/q "old_unionC.h"
	-@del /f/q "old_unionS.h"
	-@del /f/q "old_unionS_T.h"
	-@del /f/q "old_unionC.cpp"
	-@del /f/q "old_unionS.cpp"
	-@del /f/q "old_unionS_T.cpp"
	-@del /f/q "old_union2C.inl"
	-@del /f/q "old_union2S.inl"
	-@del /f/q "old_union2C.h"
	-@del /f/q "old_union2S.h"
	-@del /f/q "old_union2S_T.h"
	-@del /f/q "old_union2C.cpp"
	-@del /f/q "old_union2S.cpp"
	-@del /f/q "old_union2S_T.cpp"
	-@del /f/q "paramsC.inl"
	-@del /f/q "paramsS.inl"
	-@del /f/q "paramsC.h"
	-@del /f/q "paramsS.h"
	-@del /f/q "paramsS_T.h"
	-@del /f/q "paramsC.cpp"
	-@del /f/q "paramsS.cpp"
	-@del /f/q "paramsS_T.cpp"
	-@del /f/q "reopened_modulesC.inl"
	-@del /f/q "reopened_modulesS.inl"
	-@del /f/q "reopened_modulesC.h"
	-@del /f/q "reopened_modulesS.h"
	-@del /f/q "reopened_modulesS_T.h"
	-@del /f/q "reopened_modulesC.cpp"
	-@del /f/q "reopened_modulesS.cpp"
	-@del /f/q "reopened_modulesS_T.cpp"
	-@del /f/q "sequenceC.inl"
	-@del /f/q "sequenceS.inl"
	-@del /f/q "sequenceC.h"
	-@del /f/q "sequenceS.h"
	-@del /f/q "sequenceS_T.h"
	-@del /f/q "sequenceC.cpp"
	-@del /f/q "sequenceS.cpp"
	-@del /f/q "sequenceS_T.cpp"
	-@del /f/q "structC.inl"
	-@del /f/q "structS.inl"
	-@del /f/q "structC.h"
	-@del /f/q "structS.h"
	-@del /f/q "structS_T.h"
	-@del /f/q "structC.cpp"
	-@del /f/q "structS.cpp"
	-@del /f/q "structS_T.cpp"
	-@del /f/q "reopen_include1C.inl"
	-@del /f/q "reopen_include1S.inl"
	-@del /f/q "reopen_include1C.h"
	-@del /f/q "reopen_include1S.h"
	-@del /f/q "reopen_include1S_T.h"
	-@del /f/q "reopen_include1C.cpp"
	-@del /f/q "reopen_include1S.cpp"
	-@del /f/q "reopen_include1S_T.cpp"
	-@del /f/q "reopen_include2C.inl"
	-@del /f/q "reopen_include2S.inl"
	-@del /f/q "reopen_include2C.h"
	-@del /f/q "reopen_include2S.h"
	-@del /f/q "reopen_include2S_T.h"
	-@del /f/q "reopen_include2C.cpp"
	-@del /f/q "reopen_include2S.cpp"
	-@del /f/q "reopen_include2S_T.cpp"
	-@del /f/q "typeprefixC.inl"
	-@del /f/q "typeprefixS.inl"
	-@del /f/q "typeprefixC.h"
	-@del /f/q "typeprefixS.h"
	-@del /f/q "typeprefixS_T.h"
	-@del /f/q "typeprefixC.cpp"
	-@del /f/q "typeprefixS.cpp"
	-@del /f/q "typeprefixS_T.cpp"
	-@del /f/q "unionC.inl"
	-@del /f/q "unionS.inl"
	-@del /f/q "unionC.h"
	-@del /f/q "unionS.h"
	-@del /f/q "unionS_T.h"
	-@del /f/q "unionC.cpp"
	-@del /f/q "unionS.cpp"
	-@del /f/q "unionS_T.cpp"
	-@del /f/q "anonymousC.inl"
	-@del /f/q "anonymousS.inl"
	-@del /f/q "anonymousC.h"
	-@del /f/q "anonymousS.h"
	-@del /f/q "anonymousA.h"
	-@del /f/q "anonymousS_T.h"
	-@del /f/q "anonymousC.cpp"
	-@del /f/q "anonymousS.cpp"
	-@del /f/q "anonymousA.cpp"
	-@del /f/q "anonymousS_T.cpp"
	-@del /f/q "array_onlyC.inl"
	-@del /f/q "array_onlyS.inl"
	-@del /f/q "array_onlyC.h"
	-@del /f/q "array_onlyS.h"
	-@del /f/q "array_onlyA.h"
	-@del /f/q "array_onlyS_T.h"
	-@del /f/q "array_onlyC.cpp"
	-@del /f/q "array_onlyS.cpp"
	-@del /f/q "array_onlyA.cpp"
	-@del /f/q "array_onlyS_T.cpp"
	-@del /f/q "constantsC.inl"
	-@del /f/q "constantsS.inl"
	-@del /f/q "constantsC.h"
	-@del /f/q "constantsS.h"
	-@del /f/q "constantsA.h"
	-@del /f/q "constantsS_T.h"
	-@del /f/q "constantsC.cpp"
	-@del /f/q "constantsS.cpp"
	-@del /f/q "constantsA.cpp"
	-@del /f/q "constantsS_T.cpp"
	-@del /f/q "generic_objectC.inl"
	-@del /f/q "generic_objectS.inl"
	-@del /f/q "generic_objectC.h"
	-@del /f/q "generic_objectS.h"
	-@del /f/q "generic_objectA.h"
	-@del /f/q "generic_objectS_T.h"
	-@del /f/q "generic_objectC.cpp"
	-@del /f/q "generic_objectS.cpp"
	-@del /f/q "generic_objectA.cpp"
	-@del /f/q "generic_objectS_T.cpp"
	-@del /f/q "keywordsC.inl"
	-@del /f/q "keywordsS.inl"
	-@del /f/q "keywordsC.h"
	-@del /f/q "keywordsS.h"
	-@del /f/q "keywordsA.h"
	-@del /f/q "keywordsS_T.h"
	-@del /f/q "keywordsC.cpp"
	-@del /f/q "keywordsS.cpp"
	-@del /f/q "keywordsA.cpp"
	-@del /f/q "keywordsS_T.cpp"
	-@del /f/q "dif2C.inl"
	-@del /f/q "dif2S.inl"
	-@del /f/q "dif2C.h"
	-@del /f/q "dif2S.h"
	-@del /f/q "dif2A.h"
	-@del /f/q "dif2S_T.h"
	-@del /f/q "dif2C.cpp"
	-@del /f/q "dif2S.cpp"
	-@del /f/q "dif2A.cpp"
	-@del /f/q "dif2S_T.cpp"
	-@del /f/q "inheritC.inl"
	-@del /f/q "inheritS.inl"
	-@del /f/q "inheritC.h"
	-@del /f/q "inheritS.h"
	-@del /f/q "inheritA.h"
	-@del /f/q "inheritS_T.h"
	-@del /f/q "inheritC.cpp"
	-@del /f/q "inheritS.cpp"
	-@del /f/q "inheritA.cpp"
	-@del /f/q "inheritS_T.cpp"
	-@del /f/q "moduleC.inl"
	-@del /f/q "moduleS.inl"
	-@del /f/q "moduleC.h"
	-@del /f/q "moduleS.h"
	-@del /f/q "moduleA.h"
	-@del /f/q "moduleS_T.h"
	-@del /f/q "moduleC.cpp"
	-@del /f/q "moduleS.cpp"
	-@del /f/q "moduleA.cpp"
	-@del /f/q "moduleS_T.cpp"
	-@del /f/q "primtypesC.inl"
	-@del /f/q "primtypesS.inl"
	-@del /f/q "primtypesC.h"
	-@del /f/q "primtypesS.h"
	-@del /f/q "primtypesA.h"
	-@del /f/q "primtypesS_T.h"
	-@del /f/q "primtypesC.cpp"
	-@del /f/q "primtypesS.cpp"
	-@del /f/q "primtypesA.cpp"
	-@del /f/q "primtypesS_T.cpp"
	-@del /f/q "pragmaC.inl"
	-@del /f/q "pragmaS.inl"
	-@del /f/q "pragmaC.h"
	-@del /f/q "pragmaS.h"
	-@del /f/q "pragmaA.h"
	-@del /f/q "pragmaS_T.h"
	-@del /f/q "pragmaC.cpp"
	-@del /f/q "pragmaS.cpp"
	-@del /f/q "pragmaA.cpp"
	-@del /f/q "pragmaS_T.cpp"
	-@del /f/q "repo_id_modC.inl"
	-@del /f/q "repo_id_modS.inl"
	-@del /f/q "repo_id_modC.h"
	-@del /f/q "repo_id_modS.h"
	-@del /f/q "repo_id_modA.h"
	-@del /f/q "repo_id_modS_T.h"
	-@del /f/q "repo_id_modC.cpp"
	-@del /f/q "repo_id_modS.cpp"
	-@del /f/q "repo_id_modA.cpp"
	-@del /f/q "repo_id_modS_T.cpp"
	-@del /f/q "typedefC.inl"
	-@del /f/q "typedefS.inl"
	-@del /f/q "typedefC.h"
	-@del /f/q "typedefS.h"
	-@del /f/q "typedefA.h"
	-@del /f/q "typedefS_T.h"
	-@del /f/q "typedefC.cpp"
	-@del /f/q "typedefS.cpp"
	-@del /f/q "typedefA.cpp"
	-@del /f/q "typedefS_T.cpp"
	-@del /f/q "typecodeC.inl"
	-@del /f/q "typecodeS.inl"
	-@del /f/q "typecodeC.h"
	-@del /f/q "typecodeS.h"
	-@del /f/q "typecodeA.h"
	-@del /f/q "typecodeS_T.h"
	-@del /f/q "typecodeC.cpp"
	-@del /f/q "typecodeS.cpp"
	-@del /f/q "typecodeA.cpp"
	-@del /f/q "typecodeS_T.cpp"
	-@del /f/q "valuetypeC.inl"
	-@del /f/q "valuetypeS.inl"
	-@del /f/q "valuetypeC.h"
	-@del /f/q "valuetypeS.h"
	-@del /f/q "valuetypeA.h"
	-@del /f/q "valuetypeS_T.h"
	-@del /f/q "valuetypeC.cpp"
	-@del /f/q "valuetypeS.cpp"
	-@del /f/q "valuetypeA.cpp"
	-@del /f/q "valuetypeS_T.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\IDL_Test_Main\$(NULL)" mkdir "Static_Release\IDL_Test_Main"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\.." /I "..\.." /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEs.lib TAOs.lib TAO_AnyTypeCodes.lib TAO_PortableServers.lib TAO_Valuetypes.lib TAO_CodecFactorys.lib TAO_PIs.lib TAO_Messagings.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\main.exe"
LINK32_OBJS= \
	"$(INTDIR)\Bug_2577_RegressionC.obj" \
	"$(INTDIR)\Bug_2577_RegressionS.obj" \
	"$(INTDIR)\valuetypeC.obj" \
	"$(INTDIR)\valuetypeS.obj" \
	"$(INTDIR)\valuetypeA.obj" \
	"$(INTDIR)\old_unionC.obj" \
	"$(INTDIR)\old_unionS.obj" \
	"$(INTDIR)\array_onlyC.obj" \
	"$(INTDIR)\array_onlyS.obj" \
	"$(INTDIR)\array_onlyA.obj" \
	"$(INTDIR)\Bug_2582_RegressionC.obj" \
	"$(INTDIR)\Bug_2582_RegressionS.obj" \
	"$(INTDIR)\unionC.obj" \
	"$(INTDIR)\unionS.obj" \
	"$(INTDIR)\Bug_2616_RegressionC.obj" \
	"$(INTDIR)\Bug_2616_RegressionS.obj" \
	"$(INTDIR)\pragmaC.obj" \
	"$(INTDIR)\pragmaS.obj" \
	"$(INTDIR)\pragmaA.obj" \
	"$(INTDIR)\generic_objectC.obj" \
	"$(INTDIR)\generic_objectS.obj" \
	"$(INTDIR)\generic_objectA.obj" \
	"$(INTDIR)\enum_in_structC.obj" \
	"$(INTDIR)\enum_in_structS.obj" \
	"$(INTDIR)\dif2C.obj" \
	"$(INTDIR)\dif2S.obj" \
	"$(INTDIR)\dif2A.obj" \
	"$(INTDIR)\Bug_2619_RegressionC.obj" \
	"$(INTDIR)\Bug_2619_RegressionS.obj" \
	"$(INTDIR)\typedefC.obj" \
	"$(INTDIR)\typedefS.obj" \
	"$(INTDIR)\typedefA.obj" \
	"$(INTDIR)\old_structC.obj" \
	"$(INTDIR)\old_structS.obj" \
	"$(INTDIR)\simpleC.obj" \
	"$(INTDIR)\simpleS.obj" \
	"$(INTDIR)\fwdC.obj" \
	"$(INTDIR)\fwdS.obj" \
	"$(INTDIR)\paramsC.obj" \
	"$(INTDIR)\paramsS.obj" \
	"$(INTDIR)\typecodeC.obj" \
	"$(INTDIR)\typecodeS.obj" \
	"$(INTDIR)\typecodeA.obj" \
	"$(INTDIR)\old_sequenceC.obj" \
	"$(INTDIR)\sequenceC.obj" \
	"$(INTDIR)\old_sequenceS.obj" \
	"$(INTDIR)\sequenceS.obj" \
	"$(INTDIR)\includingC.obj" \
	"$(INTDIR)\includingS.obj" \
	"$(INTDIR)\Bug_2583_RegressionC.obj" \
	"$(INTDIR)\Bug_2583_RegressionS.obj" \
	"$(INTDIR)\typeprefixC.obj" \
	"$(INTDIR)\typeprefixS.obj" \
	"$(INTDIR)\repo_id_modC.obj" \
	"$(INTDIR)\repo_id_modS.obj" \
	"$(INTDIR)\repo_id_modA.obj" \
	"$(INTDIR)\old_arrayC.obj" \
	"$(INTDIR)\old_arrayS.obj" \
	"$(INTDIR)\inheritC.obj" \
	"$(INTDIR)\inheritS.obj" \
	"$(INTDIR)\inheritA.obj" \
	"$(INTDIR)\includedC.obj" \
	"$(INTDIR)\includedS.obj" \
	"$(INTDIR)\reopen_include2C.obj" \
	"$(INTDIR)\reopen_include2S.obj" \
	"$(INTDIR)\reopen_include1C.obj" \
	"$(INTDIR)\reopen_include1S.obj" \
	"$(INTDIR)\old_constantsC.obj" \
	"$(INTDIR)\old_constantsS.obj" \
	"$(INTDIR)\anonymousC.obj" \
	"$(INTDIR)\anonymousS.obj" \
	"$(INTDIR)\anonymousA.obj" \
	"$(INTDIR)\gperfC.obj" \
	"$(INTDIR)\gperfS.obj" \
	"$(INTDIR)\primtypesC.obj" \
	"$(INTDIR)\primtypesS.obj" \
	"$(INTDIR)\primtypesA.obj" \
	"$(INTDIR)\simple2C.obj" \
	"$(INTDIR)\simple2S.obj" \
	"$(INTDIR)\arrayC.obj" \
	"$(INTDIR)\arrayS.obj" \
	"$(INTDIR)\reopened_modulesC.obj" \
	"$(INTDIR)\reopened_modulesS.obj" \
	"$(INTDIR)\fullC.obj" \
	"$(INTDIR)\fullS.obj" \
	"$(INTDIR)\keywordsC.obj" \
	"$(INTDIR)\keywordsS.obj" \
	"$(INTDIR)\keywordsA.obj" \
	"$(INTDIR)\moduleC.obj" \
	"$(INTDIR)\moduleS.obj" \
	"$(INTDIR)\moduleA.obj" \
	"$(INTDIR)\constantsC.obj" \
	"$(INTDIR)\constantsS.obj" \
	"$(INTDIR)\constantsA.obj" \
	"$(INTDIR)\nested_scopeC.obj" \
	"$(INTDIR)\nested_scopeS.obj" \
	"$(INTDIR)\structC.obj" \
	"$(INTDIR)\structS.obj" \
	"$(INTDIR)\old_union2C.obj" \
	"$(INTDIR)\old_union2S.obj" \
	"$(INTDIR)\interfaceC.obj" \
	"$(INTDIR)\interfaceS.obj" \
	"$(INTDIR)\Bug_2350_RegressionC.obj" \
	"$(INTDIR)\Bug_2350_RegressionS.obj" \
	"$(INTDIR)\main.obj"

"$(INSTALLDIR)\main.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\main.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\main.exe.manifest" -outputresource:$@;1

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.IDL_Test_Main.dep")
!INCLUDE "Makefile.IDL_Test_Main.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Bug_2577_RegressionC.cpp"

"$(INTDIR)\Bug_2577_RegressionC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Bug_2577_RegressionC.obj" $(SOURCE)

SOURCE="Bug_2577_RegressionS.cpp"

"$(INTDIR)\Bug_2577_RegressionS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Bug_2577_RegressionS.obj" $(SOURCE)

SOURCE="valuetypeC.cpp"

"$(INTDIR)\valuetypeC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\valuetypeC.obj" $(SOURCE)

SOURCE="valuetypeS.cpp"

"$(INTDIR)\valuetypeS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\valuetypeS.obj" $(SOURCE)

SOURCE="valuetypeA.cpp"

"$(INTDIR)\valuetypeA.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\valuetypeA.obj" $(SOURCE)

SOURCE="old_unionC.cpp"

"$(INTDIR)\old_unionC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\old_unionC.obj" $(SOURCE)

SOURCE="old_unionS.cpp"

"$(INTDIR)\old_unionS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\old_unionS.obj" $(SOURCE)

SOURCE="array_onlyC.cpp"

"$(INTDIR)\array_onlyC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\array_onlyC.obj" $(SOURCE)

SOURCE="array_onlyS.cpp"

"$(INTDIR)\array_onlyS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\array_onlyS.obj" $(SOURCE)

SOURCE="array_onlyA.cpp"

"$(INTDIR)\array_onlyA.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\array_onlyA.obj" $(SOURCE)

SOURCE="Bug_2582_RegressionC.cpp"

"$(INTDIR)\Bug_2582_RegressionC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Bug_2582_RegressionC.obj" $(SOURCE)

SOURCE="Bug_2582_RegressionS.cpp"

"$(INTDIR)\Bug_2582_RegressionS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Bug_2582_RegressionS.obj" $(SOURCE)

SOURCE="unionC.cpp"

"$(INTDIR)\unionC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\unionC.obj" $(SOURCE)

SOURCE="unionS.cpp"

"$(INTDIR)\unionS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\unionS.obj" $(SOURCE)

SOURCE="Bug_2616_RegressionC.cpp"

"$(INTDIR)\Bug_2616_RegressionC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Bug_2616_RegressionC.obj" $(SOURCE)

SOURCE="Bug_2616_RegressionS.cpp"

"$(INTDIR)\Bug_2616_RegressionS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Bug_2616_RegressionS.obj" $(SOURCE)

SOURCE="pragmaC.cpp"

"$(INTDIR)\pragmaC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\pragmaC.obj" $(SOURCE)

SOURCE="pragmaS.cpp"

"$(INTDIR)\pragmaS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\pragmaS.obj" $(SOURCE)

SOURCE="pragmaA.cpp"

"$(INTDIR)\pragmaA.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\pragmaA.obj" $(SOURCE)

SOURCE="generic_objectC.cpp"

"$(INTDIR)\generic_objectC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\generic_objectC.obj" $(SOURCE)

SOURCE="generic_objectS.cpp"

"$(INTDIR)\generic_objectS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\generic_objectS.obj" $(SOURCE)

SOURCE="generic_objectA.cpp"

"$(INTDIR)\generic_objectA.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\generic_objectA.obj" $(SOURCE)

SOURCE="enum_in_structC.cpp"

"$(INTDIR)\enum_in_structC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\enum_in_structC.obj" $(SOURCE)

SOURCE="enum_in_structS.cpp"

"$(INTDIR)\enum_in_structS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\enum_in_structS.obj" $(SOURCE)

SOURCE="dif2C.cpp"

"$(INTDIR)\dif2C.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\dif2C.obj" $(SOURCE)

SOURCE="dif2S.cpp"

"$(INTDIR)\dif2S.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\dif2S.obj" $(SOURCE)

SOURCE="dif2A.cpp"

"$(INTDIR)\dif2A.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\dif2A.obj" $(SOURCE)

SOURCE="Bug_2619_RegressionC.cpp"

"$(INTDIR)\Bug_2619_RegressionC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Bug_2619_RegressionC.obj" $(SOURCE)

SOURCE="Bug_2619_RegressionS.cpp"

"$(INTDIR)\Bug_2619_RegressionS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Bug_2619_RegressionS.obj" $(SOURCE)

SOURCE="typedefC.cpp"

"$(INTDIR)\typedefC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\typedefC.obj" $(SOURCE)

SOURCE="typedefS.cpp"

"$(INTDIR)\typedefS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\typedefS.obj" $(SOURCE)

SOURCE="typedefA.cpp"

"$(INTDIR)\typedefA.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\typedefA.obj" $(SOURCE)

SOURCE="old_structC.cpp"

"$(INTDIR)\old_structC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\old_structC.obj" $(SOURCE)

SOURCE="old_structS.cpp"

"$(INTDIR)\old_structS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\old_structS.obj" $(SOURCE)

SOURCE="simpleC.cpp"

"$(INTDIR)\simpleC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\simpleC.obj" $(SOURCE)

SOURCE="simpleS.cpp"

"$(INTDIR)\simpleS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\simpleS.obj" $(SOURCE)

SOURCE="fwdC.cpp"

"$(INTDIR)\fwdC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\fwdC.obj" $(SOURCE)

SOURCE="fwdS.cpp"

"$(INTDIR)\fwdS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\fwdS.obj" $(SOURCE)

SOURCE="paramsC.cpp"

"$(INTDIR)\paramsC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\paramsC.obj" $(SOURCE)

SOURCE="paramsS.cpp"

"$(INTDIR)\paramsS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\paramsS.obj" $(SOURCE)

SOURCE="typecodeC.cpp"

"$(INTDIR)\typecodeC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\typecodeC.obj" $(SOURCE)

SOURCE="typecodeS.cpp"

"$(INTDIR)\typecodeS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\typecodeS.obj" $(SOURCE)

SOURCE="typecodeA.cpp"

"$(INTDIR)\typecodeA.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\typecodeA.obj" $(SOURCE)

SOURCE="old_sequenceC.cpp"

"$(INTDIR)\old_sequenceC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\old_sequenceC.obj" $(SOURCE)

SOURCE="sequenceC.cpp"

"$(INTDIR)\sequenceC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\sequenceC.obj" $(SOURCE)

SOURCE="old_sequenceS.cpp"

"$(INTDIR)\old_sequenceS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\old_sequenceS.obj" $(SOURCE)

SOURCE="sequenceS.cpp"

"$(INTDIR)\sequenceS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\sequenceS.obj" $(SOURCE)

SOURCE="includingC.cpp"

"$(INTDIR)\includingC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\includingC.obj" $(SOURCE)

SOURCE="includingS.cpp"

"$(INTDIR)\includingS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\includingS.obj" $(SOURCE)

SOURCE="Bug_2583_RegressionC.cpp"

"$(INTDIR)\Bug_2583_RegressionC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Bug_2583_RegressionC.obj" $(SOURCE)

SOURCE="Bug_2583_RegressionS.cpp"

"$(INTDIR)\Bug_2583_RegressionS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Bug_2583_RegressionS.obj" $(SOURCE)

SOURCE="typeprefixC.cpp"

"$(INTDIR)\typeprefixC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\typeprefixC.obj" $(SOURCE)

SOURCE="typeprefixS.cpp"

"$(INTDIR)\typeprefixS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\typeprefixS.obj" $(SOURCE)

SOURCE="repo_id_modC.cpp"

"$(INTDIR)\repo_id_modC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\repo_id_modC.obj" $(SOURCE)

SOURCE="repo_id_modS.cpp"

"$(INTDIR)\repo_id_modS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\repo_id_modS.obj" $(SOURCE)

SOURCE="repo_id_modA.cpp"

"$(INTDIR)\repo_id_modA.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\repo_id_modA.obj" $(SOURCE)

SOURCE="old_arrayC.cpp"

"$(INTDIR)\old_arrayC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\old_arrayC.obj" $(SOURCE)

SOURCE="old_arrayS.cpp"

"$(INTDIR)\old_arrayS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\old_arrayS.obj" $(SOURCE)

SOURCE="inheritC.cpp"

"$(INTDIR)\inheritC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\inheritC.obj" $(SOURCE)

SOURCE="inheritS.cpp"

"$(INTDIR)\inheritS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\inheritS.obj" $(SOURCE)

SOURCE="inheritA.cpp"

"$(INTDIR)\inheritA.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\inheritA.obj" $(SOURCE)

SOURCE="includedC.cpp"

"$(INTDIR)\includedC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\includedC.obj" $(SOURCE)

SOURCE="includedS.cpp"

"$(INTDIR)\includedS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\includedS.obj" $(SOURCE)

SOURCE="reopen_include2C.cpp"

"$(INTDIR)\reopen_include2C.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\reopen_include2C.obj" $(SOURCE)

SOURCE="reopen_include2S.cpp"

"$(INTDIR)\reopen_include2S.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\reopen_include2S.obj" $(SOURCE)

SOURCE="reopen_include1C.cpp"

"$(INTDIR)\reopen_include1C.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\reopen_include1C.obj" $(SOURCE)

SOURCE="reopen_include1S.cpp"

"$(INTDIR)\reopen_include1S.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\reopen_include1S.obj" $(SOURCE)

SOURCE="old_constantsC.cpp"

"$(INTDIR)\old_constantsC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\old_constantsC.obj" $(SOURCE)

SOURCE="old_constantsS.cpp"

"$(INTDIR)\old_constantsS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\old_constantsS.obj" $(SOURCE)

SOURCE="anonymousC.cpp"

"$(INTDIR)\anonymousC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\anonymousC.obj" $(SOURCE)

SOURCE="anonymousS.cpp"

"$(INTDIR)\anonymousS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\anonymousS.obj" $(SOURCE)

SOURCE="anonymousA.cpp"

"$(INTDIR)\anonymousA.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\anonymousA.obj" $(SOURCE)

SOURCE="gperfC.cpp"

"$(INTDIR)\gperfC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\gperfC.obj" $(SOURCE)

SOURCE="gperfS.cpp"

"$(INTDIR)\gperfS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\gperfS.obj" $(SOURCE)

SOURCE="primtypesC.cpp"

"$(INTDIR)\primtypesC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\primtypesC.obj" $(SOURCE)

SOURCE="primtypesS.cpp"

"$(INTDIR)\primtypesS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\primtypesS.obj" $(SOURCE)

SOURCE="primtypesA.cpp"

"$(INTDIR)\primtypesA.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\primtypesA.obj" $(SOURCE)

SOURCE="simple2C.cpp"

"$(INTDIR)\simple2C.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\simple2C.obj" $(SOURCE)

SOURCE="simple2S.cpp"

"$(INTDIR)\simple2S.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\simple2S.obj" $(SOURCE)

SOURCE="arrayC.cpp"

"$(INTDIR)\arrayC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\arrayC.obj" $(SOURCE)

SOURCE="arrayS.cpp"

"$(INTDIR)\arrayS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\arrayS.obj" $(SOURCE)

SOURCE="reopened_modulesC.cpp"

"$(INTDIR)\reopened_modulesC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\reopened_modulesC.obj" $(SOURCE)

SOURCE="reopened_modulesS.cpp"

"$(INTDIR)\reopened_modulesS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\reopened_modulesS.obj" $(SOURCE)

SOURCE="fullC.cpp"

"$(INTDIR)\fullC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\fullC.obj" $(SOURCE)

SOURCE="fullS.cpp"

"$(INTDIR)\fullS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\fullS.obj" $(SOURCE)

SOURCE="keywordsC.cpp"

"$(INTDIR)\keywordsC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\keywordsC.obj" $(SOURCE)

SOURCE="keywordsS.cpp"

"$(INTDIR)\keywordsS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\keywordsS.obj" $(SOURCE)

SOURCE="keywordsA.cpp"

"$(INTDIR)\keywordsA.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\keywordsA.obj" $(SOURCE)

SOURCE="moduleC.cpp"

"$(INTDIR)\moduleC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\moduleC.obj" $(SOURCE)

SOURCE="moduleS.cpp"

"$(INTDIR)\moduleS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\moduleS.obj" $(SOURCE)

SOURCE="moduleA.cpp"

"$(INTDIR)\moduleA.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\moduleA.obj" $(SOURCE)

SOURCE="constantsC.cpp"

"$(INTDIR)\constantsC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\constantsC.obj" $(SOURCE)

SOURCE="constantsS.cpp"

"$(INTDIR)\constantsS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\constantsS.obj" $(SOURCE)

SOURCE="constantsA.cpp"

"$(INTDIR)\constantsA.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\constantsA.obj" $(SOURCE)

SOURCE="nested_scopeC.cpp"

"$(INTDIR)\nested_scopeC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\nested_scopeC.obj" $(SOURCE)

SOURCE="nested_scopeS.cpp"

"$(INTDIR)\nested_scopeS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\nested_scopeS.obj" $(SOURCE)

SOURCE="structC.cpp"

"$(INTDIR)\structC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\structC.obj" $(SOURCE)

SOURCE="structS.cpp"

"$(INTDIR)\structS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\structS.obj" $(SOURCE)

SOURCE="old_union2C.cpp"

"$(INTDIR)\old_union2C.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\old_union2C.obj" $(SOURCE)

SOURCE="old_union2S.cpp"

"$(INTDIR)\old_union2S.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\old_union2S.obj" $(SOURCE)

SOURCE="interfaceC.cpp"

"$(INTDIR)\interfaceC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\interfaceC.obj" $(SOURCE)

SOURCE="interfaceS.cpp"

"$(INTDIR)\interfaceS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\interfaceS.obj" $(SOURCE)

SOURCE="Bug_2350_RegressionC.cpp"

"$(INTDIR)\Bug_2350_RegressionC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Bug_2350_RegressionC.obj" $(SOURCE)

SOURCE="Bug_2350_RegressionS.cpp"

"$(INTDIR)\Bug_2350_RegressionS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Bug_2350_RegressionS.obj" $(SOURCE)

SOURCE="main.cpp"

"$(INTDIR)\main.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\main.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="array.idl"

InputPath=array.idl

"arrayC.inl" "arrayS.inl" "arrayC.h" "arrayS.h" "arrayS_T.h" "arrayC.cpp" "arrayS.cpp" "arrayS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-array_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="Bug_2350_Regression.idl"

InputPath=Bug_2350_Regression.idl

"Bug_2350_RegressionC.inl" "Bug_2350_RegressionS.inl" "Bug_2350_RegressionC.h" "Bug_2350_RegressionS.h" "Bug_2350_RegressionS_T.h" "Bug_2350_RegressionC.cpp" "Bug_2350_RegressionS.cpp" "Bug_2350_RegressionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Bug_2350_Regression_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="Bug_2577_Regression.idl"

InputPath=Bug_2577_Regression.idl

"Bug_2577_RegressionC.inl" "Bug_2577_RegressionS.inl" "Bug_2577_RegressionC.h" "Bug_2577_RegressionS.h" "Bug_2577_RegressionS_T.h" "Bug_2577_RegressionC.cpp" "Bug_2577_RegressionS.cpp" "Bug_2577_RegressionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Bug_2577_Regression_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="Bug_2582_Regression.idl"

InputPath=Bug_2582_Regression.idl

"Bug_2582_RegressionC.inl" "Bug_2582_RegressionS.inl" "Bug_2582_RegressionC.h" "Bug_2582_RegressionS.h" "Bug_2582_RegressionS_T.h" "Bug_2582_RegressionC.cpp" "Bug_2582_RegressionS.cpp" "Bug_2582_RegressionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Bug_2582_Regression_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="Bug_2583_Regression.idl"

InputPath=Bug_2583_Regression.idl

"Bug_2583_RegressionC.inl" "Bug_2583_RegressionS.inl" "Bug_2583_RegressionC.h" "Bug_2583_RegressionS.h" "Bug_2583_RegressionS_T.h" "Bug_2583_RegressionC.cpp" "Bug_2583_RegressionS.cpp" "Bug_2583_RegressionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Bug_2583_Regression_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="Bug_2616_Regression.idl"

InputPath=Bug_2616_Regression.idl

"Bug_2616_RegressionC.inl" "Bug_2616_RegressionS.inl" "Bug_2616_RegressionC.h" "Bug_2616_RegressionS.h" "Bug_2616_RegressionS_T.h" "Bug_2616_RegressionC.cpp" "Bug_2616_RegressionS.cpp" "Bug_2616_RegressionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Bug_2616_Regression_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="Bug_2619_Regression.idl"

InputPath=Bug_2619_Regression.idl

"Bug_2619_RegressionC.inl" "Bug_2619_RegressionS.inl" "Bug_2619_RegressionC.h" "Bug_2619_RegressionS.h" "Bug_2619_RegressionS_T.h" "Bug_2619_RegressionC.cpp" "Bug_2619_RegressionS.cpp" "Bug_2619_RegressionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Bug_2619_Regression_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="enum_in_struct.idl"

InputPath=enum_in_struct.idl

"enum_in_structC.inl" "enum_in_structS.inl" "enum_in_structC.h" "enum_in_structS.h" "enum_in_structS_T.h" "enum_in_structC.cpp" "enum_in_structS.cpp" "enum_in_structS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-enum_in_struct_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="full.idl"

InputPath=full.idl

"fullC.inl" "fullS.inl" "fullC.h" "fullS.h" "fullS_T.h" "fullC.cpp" "fullS.cpp" "fullS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-full_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="fwd.idl"

InputPath=fwd.idl

"fwdC.inl" "fwdS.inl" "fwdC.h" "fwdS.h" "fwdS_T.h" "fwdC.cpp" "fwdS.cpp" "fwdS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-fwd_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="gperf.idl"

InputPath=gperf.idl

"gperfC.inl" "gperfS.inl" "gperfC.h" "gperfS.h" "gperfS_T.h" "gperfC.cpp" "gperfS.cpp" "gperfS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-gperf_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="including.idl"

InputPath=including.idl

"includingC.inl" "includingS.inl" "includingC.h" "includingS.h" "includingS_T.h" "includingC.cpp" "includingS.cpp" "includingS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-including_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="interface.idl"

InputPath=interface.idl

"interfaceC.inl" "interfaceS.inl" "interfaceC.h" "interfaceS.h" "interfaceS_T.h" "interfaceC.cpp" "interfaceS.cpp" "interfaceS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-interface_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="included.idl"

InputPath=included.idl

"includedC.inl" "includedS.inl" "includedC.h" "includedS.h" "includedS_T.h" "includedC.cpp" "includedS.cpp" "includedS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-included_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="nested_scope.idl"

InputPath=nested_scope.idl

"nested_scopeC.inl" "nested_scopeS.inl" "nested_scopeC.h" "nested_scopeS.h" "nested_scopeS_T.h" "nested_scopeC.cpp" "nested_scopeS.cpp" "nested_scopeS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-nested_scope_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="old_array.idl"

InputPath=old_array.idl

"old_arrayC.inl" "old_arrayS.inl" "old_arrayC.h" "old_arrayS.h" "old_arrayS_T.h" "old_arrayC.cpp" "old_arrayS.cpp" "old_arrayS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-old_array_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="old_constants.idl"

InputPath=old_constants.idl

"old_constantsC.inl" "old_constantsS.inl" "old_constantsC.h" "old_constantsS.h" "old_constantsS_T.h" "old_constantsC.cpp" "old_constantsS.cpp" "old_constantsS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-old_constants_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="old_sequence.idl"

InputPath=old_sequence.idl

"old_sequenceC.inl" "old_sequenceS.inl" "old_sequenceC.h" "old_sequenceS.h" "old_sequenceS_T.h" "old_sequenceC.cpp" "old_sequenceS.cpp" "old_sequenceS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-old_sequence_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="simple.idl"

InputPath=simple.idl

"simpleC.inl" "simpleS.inl" "simpleC.h" "simpleS.h" "simpleS_T.h" "simpleC.cpp" "simpleS.cpp" "simpleS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-simple_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="simple2.idl"

InputPath=simple2.idl

"simple2C.inl" "simple2S.inl" "simple2C.h" "simple2S.h" "simple2S_T.h" "simple2C.cpp" "simple2S.cpp" "simple2S_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-simple2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="old_struct.idl"

InputPath=old_struct.idl

"old_structC.inl" "old_structS.inl" "old_structC.h" "old_structS.h" "old_structS_T.h" "old_structC.cpp" "old_structS.cpp" "old_structS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-old_struct_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="old_union.idl"

InputPath=old_union.idl

"old_unionC.inl" "old_unionS.inl" "old_unionC.h" "old_unionS.h" "old_unionS_T.h" "old_unionC.cpp" "old_unionS.cpp" "old_unionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-old_union_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="old_union2.idl"

InputPath=old_union2.idl

"old_union2C.inl" "old_union2S.inl" "old_union2C.h" "old_union2S.h" "old_union2S_T.h" "old_union2C.cpp" "old_union2S.cpp" "old_union2S_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-old_union2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="params.idl"

InputPath=params.idl

"paramsC.inl" "paramsS.inl" "paramsC.h" "paramsS.h" "paramsS_T.h" "paramsC.cpp" "paramsS.cpp" "paramsS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-params_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="reopened_modules.idl"

InputPath=reopened_modules.idl

"reopened_modulesC.inl" "reopened_modulesS.inl" "reopened_modulesC.h" "reopened_modulesS.h" "reopened_modulesS_T.h" "reopened_modulesC.cpp" "reopened_modulesS.cpp" "reopened_modulesS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-reopened_modules_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="sequence.idl"

InputPath=sequence.idl

"sequenceC.inl" "sequenceS.inl" "sequenceC.h" "sequenceS.h" "sequenceS_T.h" "sequenceC.cpp" "sequenceS.cpp" "sequenceS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-sequence_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="struct.idl"

InputPath=struct.idl

"structC.inl" "structS.inl" "structC.h" "structS.h" "structS_T.h" "structC.cpp" "structS.cpp" "structS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-struct_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="reopen_include1.idl"

InputPath=reopen_include1.idl

"reopen_include1C.inl" "reopen_include1S.inl" "reopen_include1C.h" "reopen_include1S.h" "reopen_include1S_T.h" "reopen_include1C.cpp" "reopen_include1S.cpp" "reopen_include1S_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-reopen_include1_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="reopen_include2.idl"

InputPath=reopen_include2.idl

"reopen_include2C.inl" "reopen_include2S.inl" "reopen_include2C.h" "reopen_include2S.h" "reopen_include2S_T.h" "reopen_include2C.cpp" "reopen_include2S.cpp" "reopen_include2S_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-reopen_include2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="typeprefix.idl"

InputPath=typeprefix.idl

"typeprefixC.inl" "typeprefixS.inl" "typeprefixC.h" "typeprefixS.h" "typeprefixS_T.h" "typeprefixC.cpp" "typeprefixS.cpp" "typeprefixS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-typeprefix_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="union.idl"

InputPath=union.idl

"unionC.inl" "unionS.inl" "unionC.h" "unionS.h" "unionS_T.h" "unionC.cpp" "unionS.cpp" "unionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-union_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="anonymous.idl"

InputPath=anonymous.idl

"anonymousC.inl" "anonymousS.inl" "anonymousC.h" "anonymousS.h" "anonymousA.h" "anonymousS_T.h" "anonymousC.cpp" "anonymousS.cpp" "anonymousA.cpp" "anonymousS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-anonymous_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="array_only.idl"

InputPath=array_only.idl

"array_onlyC.inl" "array_onlyS.inl" "array_onlyC.h" "array_onlyS.h" "array_onlyA.h" "array_onlyS_T.h" "array_onlyC.cpp" "array_onlyS.cpp" "array_onlyA.cpp" "array_onlyS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-array_only_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="constants.idl"

InputPath=constants.idl

"constantsC.inl" "constantsS.inl" "constantsC.h" "constantsS.h" "constantsA.h" "constantsS_T.h" "constantsC.cpp" "constantsS.cpp" "constantsA.cpp" "constantsS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-constants_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="generic_object.idl"

InputPath=generic_object.idl

"generic_objectC.inl" "generic_objectS.inl" "generic_objectC.h" "generic_objectS.h" "generic_objectA.h" "generic_objectS_T.h" "generic_objectC.cpp" "generic_objectS.cpp" "generic_objectA.cpp" "generic_objectS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-generic_object_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="keywords.idl"

InputPath=keywords.idl

"keywordsC.inl" "keywordsS.inl" "keywordsC.h" "keywordsS.h" "keywordsA.h" "keywordsS_T.h" "keywordsC.cpp" "keywordsS.cpp" "keywordsA.cpp" "keywordsS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-keywords_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="dif2.idl"

InputPath=dif2.idl

"dif2C.inl" "dif2S.inl" "dif2C.h" "dif2S.h" "dif2A.h" "dif2S_T.h" "dif2C.cpp" "dif2S.cpp" "dif2A.cpp" "dif2S_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-dif2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="inherit.idl"

InputPath=inherit.idl

"inheritC.inl" "inheritS.inl" "inheritC.h" "inheritS.h" "inheritA.h" "inheritS_T.h" "inheritC.cpp" "inheritS.cpp" "inheritA.cpp" "inheritS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-inherit_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="module.idl"

InputPath=module.idl

"moduleC.inl" "moduleS.inl" "moduleC.h" "moduleS.h" "moduleA.h" "moduleS_T.h" "moduleC.cpp" "moduleS.cpp" "moduleA.cpp" "moduleS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-module_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="primtypes.idl"

InputPath=primtypes.idl

"primtypesC.inl" "primtypesS.inl" "primtypesC.h" "primtypesS.h" "primtypesA.h" "primtypesS_T.h" "primtypesC.cpp" "primtypesS.cpp" "primtypesA.cpp" "primtypesS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-primtypes_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="pragma.idl"

InputPath=pragma.idl

"pragmaC.inl" "pragmaS.inl" "pragmaC.h" "pragmaS.h" "pragmaA.h" "pragmaS_T.h" "pragmaC.cpp" "pragmaS.cpp" "pragmaA.cpp" "pragmaS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-pragma_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="repo_id_mod.idl"

InputPath=repo_id_mod.idl

"repo_id_modC.inl" "repo_id_modS.inl" "repo_id_modC.h" "repo_id_modS.h" "repo_id_modA.h" "repo_id_modS_T.h" "repo_id_modC.cpp" "repo_id_modS.cpp" "repo_id_modA.cpp" "repo_id_modS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-repo_id_mod_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="typedef.idl"

InputPath=typedef.idl

"typedefC.inl" "typedefS.inl" "typedefC.h" "typedefS.h" "typedefA.h" "typedefS_T.h" "typedefC.cpp" "typedefS.cpp" "typedefA.cpp" "typedefS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-typedef_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="typecode.idl"

InputPath=typecode.idl

"typecodeC.inl" "typecodeS.inl" "typecodeC.h" "typecodeS.h" "typecodeA.h" "typecodeS_T.h" "typecodeC.cpp" "typecodeS.cpp" "typecodeA.cpp" "typecodeS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-typecode_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="valuetype.idl"

InputPath=valuetype.idl

"valuetypeC.inl" "valuetypeS.inl" "valuetypeC.h" "valuetypeS.h" "valuetypeA.h" "valuetypeS_T.h" "valuetypeC.cpp" "valuetypeS.cpp" "valuetypeA.cpp" "valuetypeS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-valuetype_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="array.idl"

InputPath=array.idl

"arrayC.inl" "arrayS.inl" "arrayC.h" "arrayS.h" "arrayS_T.h" "arrayC.cpp" "arrayS.cpp" "arrayS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-array_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="Bug_2350_Regression.idl"

InputPath=Bug_2350_Regression.idl

"Bug_2350_RegressionC.inl" "Bug_2350_RegressionS.inl" "Bug_2350_RegressionC.h" "Bug_2350_RegressionS.h" "Bug_2350_RegressionS_T.h" "Bug_2350_RegressionC.cpp" "Bug_2350_RegressionS.cpp" "Bug_2350_RegressionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Bug_2350_Regression_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="Bug_2577_Regression.idl"

InputPath=Bug_2577_Regression.idl

"Bug_2577_RegressionC.inl" "Bug_2577_RegressionS.inl" "Bug_2577_RegressionC.h" "Bug_2577_RegressionS.h" "Bug_2577_RegressionS_T.h" "Bug_2577_RegressionC.cpp" "Bug_2577_RegressionS.cpp" "Bug_2577_RegressionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Bug_2577_Regression_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="Bug_2582_Regression.idl"

InputPath=Bug_2582_Regression.idl

"Bug_2582_RegressionC.inl" "Bug_2582_RegressionS.inl" "Bug_2582_RegressionC.h" "Bug_2582_RegressionS.h" "Bug_2582_RegressionS_T.h" "Bug_2582_RegressionC.cpp" "Bug_2582_RegressionS.cpp" "Bug_2582_RegressionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Bug_2582_Regression_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="Bug_2583_Regression.idl"

InputPath=Bug_2583_Regression.idl

"Bug_2583_RegressionC.inl" "Bug_2583_RegressionS.inl" "Bug_2583_RegressionC.h" "Bug_2583_RegressionS.h" "Bug_2583_RegressionS_T.h" "Bug_2583_RegressionC.cpp" "Bug_2583_RegressionS.cpp" "Bug_2583_RegressionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Bug_2583_Regression_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="Bug_2616_Regression.idl"

InputPath=Bug_2616_Regression.idl

"Bug_2616_RegressionC.inl" "Bug_2616_RegressionS.inl" "Bug_2616_RegressionC.h" "Bug_2616_RegressionS.h" "Bug_2616_RegressionS_T.h" "Bug_2616_RegressionC.cpp" "Bug_2616_RegressionS.cpp" "Bug_2616_RegressionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Bug_2616_Regression_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="Bug_2619_Regression.idl"

InputPath=Bug_2619_Regression.idl

"Bug_2619_RegressionC.inl" "Bug_2619_RegressionS.inl" "Bug_2619_RegressionC.h" "Bug_2619_RegressionS.h" "Bug_2619_RegressionS_T.h" "Bug_2619_RegressionC.cpp" "Bug_2619_RegressionS.cpp" "Bug_2619_RegressionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Bug_2619_Regression_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="enum_in_struct.idl"

InputPath=enum_in_struct.idl

"enum_in_structC.inl" "enum_in_structS.inl" "enum_in_structC.h" "enum_in_structS.h" "enum_in_structS_T.h" "enum_in_structC.cpp" "enum_in_structS.cpp" "enum_in_structS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-enum_in_struct_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="full.idl"

InputPath=full.idl

"fullC.inl" "fullS.inl" "fullC.h" "fullS.h" "fullS_T.h" "fullC.cpp" "fullS.cpp" "fullS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-full_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="fwd.idl"

InputPath=fwd.idl

"fwdC.inl" "fwdS.inl" "fwdC.h" "fwdS.h" "fwdS_T.h" "fwdC.cpp" "fwdS.cpp" "fwdS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-fwd_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="gperf.idl"

InputPath=gperf.idl

"gperfC.inl" "gperfS.inl" "gperfC.h" "gperfS.h" "gperfS_T.h" "gperfC.cpp" "gperfS.cpp" "gperfS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-gperf_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="including.idl"

InputPath=including.idl

"includingC.inl" "includingS.inl" "includingC.h" "includingS.h" "includingS_T.h" "includingC.cpp" "includingS.cpp" "includingS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-including_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="interface.idl"

InputPath=interface.idl

"interfaceC.inl" "interfaceS.inl" "interfaceC.h" "interfaceS.h" "interfaceS_T.h" "interfaceC.cpp" "interfaceS.cpp" "interfaceS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-interface_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="included.idl"

InputPath=included.idl

"includedC.inl" "includedS.inl" "includedC.h" "includedS.h" "includedS_T.h" "includedC.cpp" "includedS.cpp" "includedS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-included_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="nested_scope.idl"

InputPath=nested_scope.idl

"nested_scopeC.inl" "nested_scopeS.inl" "nested_scopeC.h" "nested_scopeS.h" "nested_scopeS_T.h" "nested_scopeC.cpp" "nested_scopeS.cpp" "nested_scopeS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-nested_scope_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="old_array.idl"

InputPath=old_array.idl

"old_arrayC.inl" "old_arrayS.inl" "old_arrayC.h" "old_arrayS.h" "old_arrayS_T.h" "old_arrayC.cpp" "old_arrayS.cpp" "old_arrayS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-old_array_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="old_constants.idl"

InputPath=old_constants.idl

"old_constantsC.inl" "old_constantsS.inl" "old_constantsC.h" "old_constantsS.h" "old_constantsS_T.h" "old_constantsC.cpp" "old_constantsS.cpp" "old_constantsS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-old_constants_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="old_sequence.idl"

InputPath=old_sequence.idl

"old_sequenceC.inl" "old_sequenceS.inl" "old_sequenceC.h" "old_sequenceS.h" "old_sequenceS_T.h" "old_sequenceC.cpp" "old_sequenceS.cpp" "old_sequenceS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-old_sequence_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="simple.idl"

InputPath=simple.idl

"simpleC.inl" "simpleS.inl" "simpleC.h" "simpleS.h" "simpleS_T.h" "simpleC.cpp" "simpleS.cpp" "simpleS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-simple_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="simple2.idl"

InputPath=simple2.idl

"simple2C.inl" "simple2S.inl" "simple2C.h" "simple2S.h" "simple2S_T.h" "simple2C.cpp" "simple2S.cpp" "simple2S_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-simple2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="old_struct.idl"

InputPath=old_struct.idl

"old_structC.inl" "old_structS.inl" "old_structC.h" "old_structS.h" "old_structS_T.h" "old_structC.cpp" "old_structS.cpp" "old_structS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-old_struct_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="old_union.idl"

InputPath=old_union.idl

"old_unionC.inl" "old_unionS.inl" "old_unionC.h" "old_unionS.h" "old_unionS_T.h" "old_unionC.cpp" "old_unionS.cpp" "old_unionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-old_union_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="old_union2.idl"

InputPath=old_union2.idl

"old_union2C.inl" "old_union2S.inl" "old_union2C.h" "old_union2S.h" "old_union2S_T.h" "old_union2C.cpp" "old_union2S.cpp" "old_union2S_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-old_union2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="params.idl"

InputPath=params.idl

"paramsC.inl" "paramsS.inl" "paramsC.h" "paramsS.h" "paramsS_T.h" "paramsC.cpp" "paramsS.cpp" "paramsS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-params_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="reopened_modules.idl"

InputPath=reopened_modules.idl

"reopened_modulesC.inl" "reopened_modulesS.inl" "reopened_modulesC.h" "reopened_modulesS.h" "reopened_modulesS_T.h" "reopened_modulesC.cpp" "reopened_modulesS.cpp" "reopened_modulesS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-reopened_modules_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="sequence.idl"

InputPath=sequence.idl

"sequenceC.inl" "sequenceS.inl" "sequenceC.h" "sequenceS.h" "sequenceS_T.h" "sequenceC.cpp" "sequenceS.cpp" "sequenceS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-sequence_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="struct.idl"

InputPath=struct.idl

"structC.inl" "structS.inl" "structC.h" "structS.h" "structS_T.h" "structC.cpp" "structS.cpp" "structS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-struct_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="reopen_include1.idl"

InputPath=reopen_include1.idl

"reopen_include1C.inl" "reopen_include1S.inl" "reopen_include1C.h" "reopen_include1S.h" "reopen_include1S_T.h" "reopen_include1C.cpp" "reopen_include1S.cpp" "reopen_include1S_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-reopen_include1_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="reopen_include2.idl"

InputPath=reopen_include2.idl

"reopen_include2C.inl" "reopen_include2S.inl" "reopen_include2C.h" "reopen_include2S.h" "reopen_include2S_T.h" "reopen_include2C.cpp" "reopen_include2S.cpp" "reopen_include2S_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-reopen_include2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="typeprefix.idl"

InputPath=typeprefix.idl

"typeprefixC.inl" "typeprefixS.inl" "typeprefixC.h" "typeprefixS.h" "typeprefixS_T.h" "typeprefixC.cpp" "typeprefixS.cpp" "typeprefixS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-typeprefix_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="union.idl"

InputPath=union.idl

"unionC.inl" "unionS.inl" "unionC.h" "unionS.h" "unionS_T.h" "unionC.cpp" "unionS.cpp" "unionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-union_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="anonymous.idl"

InputPath=anonymous.idl

"anonymousC.inl" "anonymousS.inl" "anonymousC.h" "anonymousS.h" "anonymousA.h" "anonymousS_T.h" "anonymousC.cpp" "anonymousS.cpp" "anonymousA.cpp" "anonymousS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-anonymous_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="array_only.idl"

InputPath=array_only.idl

"array_onlyC.inl" "array_onlyS.inl" "array_onlyC.h" "array_onlyS.h" "array_onlyA.h" "array_onlyS_T.h" "array_onlyC.cpp" "array_onlyS.cpp" "array_onlyA.cpp" "array_onlyS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-array_only_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="constants.idl"

InputPath=constants.idl

"constantsC.inl" "constantsS.inl" "constantsC.h" "constantsS.h" "constantsA.h" "constantsS_T.h" "constantsC.cpp" "constantsS.cpp" "constantsA.cpp" "constantsS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-constants_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="generic_object.idl"

InputPath=generic_object.idl

"generic_objectC.inl" "generic_objectS.inl" "generic_objectC.h" "generic_objectS.h" "generic_objectA.h" "generic_objectS_T.h" "generic_objectC.cpp" "generic_objectS.cpp" "generic_objectA.cpp" "generic_objectS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-generic_object_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="keywords.idl"

InputPath=keywords.idl

"keywordsC.inl" "keywordsS.inl" "keywordsC.h" "keywordsS.h" "keywordsA.h" "keywordsS_T.h" "keywordsC.cpp" "keywordsS.cpp" "keywordsA.cpp" "keywordsS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-keywords_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="dif2.idl"

InputPath=dif2.idl

"dif2C.inl" "dif2S.inl" "dif2C.h" "dif2S.h" "dif2A.h" "dif2S_T.h" "dif2C.cpp" "dif2S.cpp" "dif2A.cpp" "dif2S_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-dif2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="inherit.idl"

InputPath=inherit.idl

"inheritC.inl" "inheritS.inl" "inheritC.h" "inheritS.h" "inheritA.h" "inheritS_T.h" "inheritC.cpp" "inheritS.cpp" "inheritA.cpp" "inheritS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-inherit_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="module.idl"

InputPath=module.idl

"moduleC.inl" "moduleS.inl" "moduleC.h" "moduleS.h" "moduleA.h" "moduleS_T.h" "moduleC.cpp" "moduleS.cpp" "moduleA.cpp" "moduleS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-module_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="primtypes.idl"

InputPath=primtypes.idl

"primtypesC.inl" "primtypesS.inl" "primtypesC.h" "primtypesS.h" "primtypesA.h" "primtypesS_T.h" "primtypesC.cpp" "primtypesS.cpp" "primtypesA.cpp" "primtypesS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-primtypes_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="pragma.idl"

InputPath=pragma.idl

"pragmaC.inl" "pragmaS.inl" "pragmaC.h" "pragmaS.h" "pragmaA.h" "pragmaS_T.h" "pragmaC.cpp" "pragmaS.cpp" "pragmaA.cpp" "pragmaS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-pragma_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="repo_id_mod.idl"

InputPath=repo_id_mod.idl

"repo_id_modC.inl" "repo_id_modS.inl" "repo_id_modC.h" "repo_id_modS.h" "repo_id_modA.h" "repo_id_modS_T.h" "repo_id_modC.cpp" "repo_id_modS.cpp" "repo_id_modA.cpp" "repo_id_modS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-repo_id_mod_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="typedef.idl"

InputPath=typedef.idl

"typedefC.inl" "typedefS.inl" "typedefC.h" "typedefS.h" "typedefA.h" "typedefS_T.h" "typedefC.cpp" "typedefS.cpp" "typedefA.cpp" "typedefS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-typedef_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="typecode.idl"

InputPath=typecode.idl

"typecodeC.inl" "typecodeS.inl" "typecodeC.h" "typecodeS.h" "typecodeA.h" "typecodeS_T.h" "typecodeC.cpp" "typecodeS.cpp" "typecodeA.cpp" "typecodeS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-typecode_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="valuetype.idl"

InputPath=valuetype.idl

"valuetypeC.inl" "valuetypeS.inl" "valuetypeC.h" "valuetypeS.h" "valuetypeA.h" "valuetypeS_T.h" "valuetypeC.cpp" "valuetypeS.cpp" "valuetypeA.cpp" "valuetypeS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-valuetype_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="array.idl"

InputPath=array.idl

"arrayC.inl" "arrayS.inl" "arrayC.h" "arrayS.h" "arrayS_T.h" "arrayC.cpp" "arrayS.cpp" "arrayS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-array_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="Bug_2350_Regression.idl"

InputPath=Bug_2350_Regression.idl

"Bug_2350_RegressionC.inl" "Bug_2350_RegressionS.inl" "Bug_2350_RegressionC.h" "Bug_2350_RegressionS.h" "Bug_2350_RegressionS_T.h" "Bug_2350_RegressionC.cpp" "Bug_2350_RegressionS.cpp" "Bug_2350_RegressionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Bug_2350_Regression_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="Bug_2577_Regression.idl"

InputPath=Bug_2577_Regression.idl

"Bug_2577_RegressionC.inl" "Bug_2577_RegressionS.inl" "Bug_2577_RegressionC.h" "Bug_2577_RegressionS.h" "Bug_2577_RegressionS_T.h" "Bug_2577_RegressionC.cpp" "Bug_2577_RegressionS.cpp" "Bug_2577_RegressionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Bug_2577_Regression_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="Bug_2582_Regression.idl"

InputPath=Bug_2582_Regression.idl

"Bug_2582_RegressionC.inl" "Bug_2582_RegressionS.inl" "Bug_2582_RegressionC.h" "Bug_2582_RegressionS.h" "Bug_2582_RegressionS_T.h" "Bug_2582_RegressionC.cpp" "Bug_2582_RegressionS.cpp" "Bug_2582_RegressionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Bug_2582_Regression_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="Bug_2583_Regression.idl"

InputPath=Bug_2583_Regression.idl

"Bug_2583_RegressionC.inl" "Bug_2583_RegressionS.inl" "Bug_2583_RegressionC.h" "Bug_2583_RegressionS.h" "Bug_2583_RegressionS_T.h" "Bug_2583_RegressionC.cpp" "Bug_2583_RegressionS.cpp" "Bug_2583_RegressionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Bug_2583_Regression_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="Bug_2616_Regression.idl"

InputPath=Bug_2616_Regression.idl

"Bug_2616_RegressionC.inl" "Bug_2616_RegressionS.inl" "Bug_2616_RegressionC.h" "Bug_2616_RegressionS.h" "Bug_2616_RegressionS_T.h" "Bug_2616_RegressionC.cpp" "Bug_2616_RegressionS.cpp" "Bug_2616_RegressionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Bug_2616_Regression_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="Bug_2619_Regression.idl"

InputPath=Bug_2619_Regression.idl

"Bug_2619_RegressionC.inl" "Bug_2619_RegressionS.inl" "Bug_2619_RegressionC.h" "Bug_2619_RegressionS.h" "Bug_2619_RegressionS_T.h" "Bug_2619_RegressionC.cpp" "Bug_2619_RegressionS.cpp" "Bug_2619_RegressionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Bug_2619_Regression_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="enum_in_struct.idl"

InputPath=enum_in_struct.idl

"enum_in_structC.inl" "enum_in_structS.inl" "enum_in_structC.h" "enum_in_structS.h" "enum_in_structS_T.h" "enum_in_structC.cpp" "enum_in_structS.cpp" "enum_in_structS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-enum_in_struct_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="full.idl"

InputPath=full.idl

"fullC.inl" "fullS.inl" "fullC.h" "fullS.h" "fullS_T.h" "fullC.cpp" "fullS.cpp" "fullS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-full_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="fwd.idl"

InputPath=fwd.idl

"fwdC.inl" "fwdS.inl" "fwdC.h" "fwdS.h" "fwdS_T.h" "fwdC.cpp" "fwdS.cpp" "fwdS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-fwd_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="gperf.idl"

InputPath=gperf.idl

"gperfC.inl" "gperfS.inl" "gperfC.h" "gperfS.h" "gperfS_T.h" "gperfC.cpp" "gperfS.cpp" "gperfS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-gperf_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="including.idl"

InputPath=including.idl

"includingC.inl" "includingS.inl" "includingC.h" "includingS.h" "includingS_T.h" "includingC.cpp" "includingS.cpp" "includingS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-including_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="interface.idl"

InputPath=interface.idl

"interfaceC.inl" "interfaceS.inl" "interfaceC.h" "interfaceS.h" "interfaceS_T.h" "interfaceC.cpp" "interfaceS.cpp" "interfaceS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-interface_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="included.idl"

InputPath=included.idl

"includedC.inl" "includedS.inl" "includedC.h" "includedS.h" "includedS_T.h" "includedC.cpp" "includedS.cpp" "includedS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-included_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="nested_scope.idl"

InputPath=nested_scope.idl

"nested_scopeC.inl" "nested_scopeS.inl" "nested_scopeC.h" "nested_scopeS.h" "nested_scopeS_T.h" "nested_scopeC.cpp" "nested_scopeS.cpp" "nested_scopeS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-nested_scope_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="old_array.idl"

InputPath=old_array.idl

"old_arrayC.inl" "old_arrayS.inl" "old_arrayC.h" "old_arrayS.h" "old_arrayS_T.h" "old_arrayC.cpp" "old_arrayS.cpp" "old_arrayS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-old_array_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="old_constants.idl"

InputPath=old_constants.idl

"old_constantsC.inl" "old_constantsS.inl" "old_constantsC.h" "old_constantsS.h" "old_constantsS_T.h" "old_constantsC.cpp" "old_constantsS.cpp" "old_constantsS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-old_constants_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="old_sequence.idl"

InputPath=old_sequence.idl

"old_sequenceC.inl" "old_sequenceS.inl" "old_sequenceC.h" "old_sequenceS.h" "old_sequenceS_T.h" "old_sequenceC.cpp" "old_sequenceS.cpp" "old_sequenceS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-old_sequence_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="simple.idl"

InputPath=simple.idl

"simpleC.inl" "simpleS.inl" "simpleC.h" "simpleS.h" "simpleS_T.h" "simpleC.cpp" "simpleS.cpp" "simpleS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-simple_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="simple2.idl"

InputPath=simple2.idl

"simple2C.inl" "simple2S.inl" "simple2C.h" "simple2S.h" "simple2S_T.h" "simple2C.cpp" "simple2S.cpp" "simple2S_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-simple2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="old_struct.idl"

InputPath=old_struct.idl

"old_structC.inl" "old_structS.inl" "old_structC.h" "old_structS.h" "old_structS_T.h" "old_structC.cpp" "old_structS.cpp" "old_structS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-old_struct_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="old_union.idl"

InputPath=old_union.idl

"old_unionC.inl" "old_unionS.inl" "old_unionC.h" "old_unionS.h" "old_unionS_T.h" "old_unionC.cpp" "old_unionS.cpp" "old_unionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-old_union_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="old_union2.idl"

InputPath=old_union2.idl

"old_union2C.inl" "old_union2S.inl" "old_union2C.h" "old_union2S.h" "old_union2S_T.h" "old_union2C.cpp" "old_union2S.cpp" "old_union2S_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-old_union2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="params.idl"

InputPath=params.idl

"paramsC.inl" "paramsS.inl" "paramsC.h" "paramsS.h" "paramsS_T.h" "paramsC.cpp" "paramsS.cpp" "paramsS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-params_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="reopened_modules.idl"

InputPath=reopened_modules.idl

"reopened_modulesC.inl" "reopened_modulesS.inl" "reopened_modulesC.h" "reopened_modulesS.h" "reopened_modulesS_T.h" "reopened_modulesC.cpp" "reopened_modulesS.cpp" "reopened_modulesS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-reopened_modules_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="sequence.idl"

InputPath=sequence.idl

"sequenceC.inl" "sequenceS.inl" "sequenceC.h" "sequenceS.h" "sequenceS_T.h" "sequenceC.cpp" "sequenceS.cpp" "sequenceS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-sequence_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="struct.idl"

InputPath=struct.idl

"structC.inl" "structS.inl" "structC.h" "structS.h" "structS_T.h" "structC.cpp" "structS.cpp" "structS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-struct_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="reopen_include1.idl"

InputPath=reopen_include1.idl

"reopen_include1C.inl" "reopen_include1S.inl" "reopen_include1C.h" "reopen_include1S.h" "reopen_include1S_T.h" "reopen_include1C.cpp" "reopen_include1S.cpp" "reopen_include1S_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-reopen_include1_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="reopen_include2.idl"

InputPath=reopen_include2.idl

"reopen_include2C.inl" "reopen_include2S.inl" "reopen_include2C.h" "reopen_include2S.h" "reopen_include2S_T.h" "reopen_include2C.cpp" "reopen_include2S.cpp" "reopen_include2S_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-reopen_include2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="typeprefix.idl"

InputPath=typeprefix.idl

"typeprefixC.inl" "typeprefixS.inl" "typeprefixC.h" "typeprefixS.h" "typeprefixS_T.h" "typeprefixC.cpp" "typeprefixS.cpp" "typeprefixS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-typeprefix_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="union.idl"

InputPath=union.idl

"unionC.inl" "unionS.inl" "unionC.h" "unionS.h" "unionS_T.h" "unionC.cpp" "unionS.cpp" "unionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-union_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="anonymous.idl"

InputPath=anonymous.idl

"anonymousC.inl" "anonymousS.inl" "anonymousC.h" "anonymousS.h" "anonymousA.h" "anonymousS_T.h" "anonymousC.cpp" "anonymousS.cpp" "anonymousA.cpp" "anonymousS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-anonymous_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="array_only.idl"

InputPath=array_only.idl

"array_onlyC.inl" "array_onlyS.inl" "array_onlyC.h" "array_onlyS.h" "array_onlyA.h" "array_onlyS_T.h" "array_onlyC.cpp" "array_onlyS.cpp" "array_onlyA.cpp" "array_onlyS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-array_only_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="constants.idl"

InputPath=constants.idl

"constantsC.inl" "constantsS.inl" "constantsC.h" "constantsS.h" "constantsA.h" "constantsS_T.h" "constantsC.cpp" "constantsS.cpp" "constantsA.cpp" "constantsS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-constants_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="generic_object.idl"

InputPath=generic_object.idl

"generic_objectC.inl" "generic_objectS.inl" "generic_objectC.h" "generic_objectS.h" "generic_objectA.h" "generic_objectS_T.h" "generic_objectC.cpp" "generic_objectS.cpp" "generic_objectA.cpp" "generic_objectS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-generic_object_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="keywords.idl"

InputPath=keywords.idl

"keywordsC.inl" "keywordsS.inl" "keywordsC.h" "keywordsS.h" "keywordsA.h" "keywordsS_T.h" "keywordsC.cpp" "keywordsS.cpp" "keywordsA.cpp" "keywordsS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-keywords_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="dif2.idl"

InputPath=dif2.idl

"dif2C.inl" "dif2S.inl" "dif2C.h" "dif2S.h" "dif2A.h" "dif2S_T.h" "dif2C.cpp" "dif2S.cpp" "dif2A.cpp" "dif2S_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-dif2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="inherit.idl"

InputPath=inherit.idl

"inheritC.inl" "inheritS.inl" "inheritC.h" "inheritS.h" "inheritA.h" "inheritS_T.h" "inheritC.cpp" "inheritS.cpp" "inheritA.cpp" "inheritS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-inherit_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="module.idl"

InputPath=module.idl

"moduleC.inl" "moduleS.inl" "moduleC.h" "moduleS.h" "moduleA.h" "moduleS_T.h" "moduleC.cpp" "moduleS.cpp" "moduleA.cpp" "moduleS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-module_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="primtypes.idl"

InputPath=primtypes.idl

"primtypesC.inl" "primtypesS.inl" "primtypesC.h" "primtypesS.h" "primtypesA.h" "primtypesS_T.h" "primtypesC.cpp" "primtypesS.cpp" "primtypesA.cpp" "primtypesS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-primtypes_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="pragma.idl"

InputPath=pragma.idl

"pragmaC.inl" "pragmaS.inl" "pragmaC.h" "pragmaS.h" "pragmaA.h" "pragmaS_T.h" "pragmaC.cpp" "pragmaS.cpp" "pragmaA.cpp" "pragmaS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-pragma_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="repo_id_mod.idl"

InputPath=repo_id_mod.idl

"repo_id_modC.inl" "repo_id_modS.inl" "repo_id_modC.h" "repo_id_modS.h" "repo_id_modA.h" "repo_id_modS_T.h" "repo_id_modC.cpp" "repo_id_modS.cpp" "repo_id_modA.cpp" "repo_id_modS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-repo_id_mod_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="typedef.idl"

InputPath=typedef.idl

"typedefC.inl" "typedefS.inl" "typedefC.h" "typedefS.h" "typedefA.h" "typedefS_T.h" "typedefC.cpp" "typedefS.cpp" "typedefA.cpp" "typedefS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-typedef_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="typecode.idl"

InputPath=typecode.idl

"typecodeC.inl" "typecodeS.inl" "typecodeC.h" "typecodeS.h" "typecodeA.h" "typecodeS_T.h" "typecodeC.cpp" "typecodeS.cpp" "typecodeA.cpp" "typecodeS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-typecode_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="valuetype.idl"

InputPath=valuetype.idl

"valuetypeC.inl" "valuetypeS.inl" "valuetypeC.h" "valuetypeS.h" "valuetypeA.h" "valuetypeS_T.h" "valuetypeC.cpp" "valuetypeS.cpp" "valuetypeA.cpp" "valuetypeS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-valuetype_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="array.idl"

InputPath=array.idl

"arrayC.inl" "arrayS.inl" "arrayC.h" "arrayS.h" "arrayS_T.h" "arrayC.cpp" "arrayS.cpp" "arrayS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-array_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="Bug_2350_Regression.idl"

InputPath=Bug_2350_Regression.idl

"Bug_2350_RegressionC.inl" "Bug_2350_RegressionS.inl" "Bug_2350_RegressionC.h" "Bug_2350_RegressionS.h" "Bug_2350_RegressionS_T.h" "Bug_2350_RegressionC.cpp" "Bug_2350_RegressionS.cpp" "Bug_2350_RegressionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Bug_2350_Regression_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="Bug_2577_Regression.idl"

InputPath=Bug_2577_Regression.idl

"Bug_2577_RegressionC.inl" "Bug_2577_RegressionS.inl" "Bug_2577_RegressionC.h" "Bug_2577_RegressionS.h" "Bug_2577_RegressionS_T.h" "Bug_2577_RegressionC.cpp" "Bug_2577_RegressionS.cpp" "Bug_2577_RegressionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Bug_2577_Regression_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="Bug_2582_Regression.idl"

InputPath=Bug_2582_Regression.idl

"Bug_2582_RegressionC.inl" "Bug_2582_RegressionS.inl" "Bug_2582_RegressionC.h" "Bug_2582_RegressionS.h" "Bug_2582_RegressionS_T.h" "Bug_2582_RegressionC.cpp" "Bug_2582_RegressionS.cpp" "Bug_2582_RegressionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Bug_2582_Regression_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="Bug_2583_Regression.idl"

InputPath=Bug_2583_Regression.idl

"Bug_2583_RegressionC.inl" "Bug_2583_RegressionS.inl" "Bug_2583_RegressionC.h" "Bug_2583_RegressionS.h" "Bug_2583_RegressionS_T.h" "Bug_2583_RegressionC.cpp" "Bug_2583_RegressionS.cpp" "Bug_2583_RegressionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Bug_2583_Regression_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="Bug_2616_Regression.idl"

InputPath=Bug_2616_Regression.idl

"Bug_2616_RegressionC.inl" "Bug_2616_RegressionS.inl" "Bug_2616_RegressionC.h" "Bug_2616_RegressionS.h" "Bug_2616_RegressionS_T.h" "Bug_2616_RegressionC.cpp" "Bug_2616_RegressionS.cpp" "Bug_2616_RegressionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Bug_2616_Regression_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="Bug_2619_Regression.idl"

InputPath=Bug_2619_Regression.idl

"Bug_2619_RegressionC.inl" "Bug_2619_RegressionS.inl" "Bug_2619_RegressionC.h" "Bug_2619_RegressionS.h" "Bug_2619_RegressionS_T.h" "Bug_2619_RegressionC.cpp" "Bug_2619_RegressionS.cpp" "Bug_2619_RegressionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Bug_2619_Regression_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="enum_in_struct.idl"

InputPath=enum_in_struct.idl

"enum_in_structC.inl" "enum_in_structS.inl" "enum_in_structC.h" "enum_in_structS.h" "enum_in_structS_T.h" "enum_in_structC.cpp" "enum_in_structS.cpp" "enum_in_structS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-enum_in_struct_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="full.idl"

InputPath=full.idl

"fullC.inl" "fullS.inl" "fullC.h" "fullS.h" "fullS_T.h" "fullC.cpp" "fullS.cpp" "fullS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-full_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="fwd.idl"

InputPath=fwd.idl

"fwdC.inl" "fwdS.inl" "fwdC.h" "fwdS.h" "fwdS_T.h" "fwdC.cpp" "fwdS.cpp" "fwdS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-fwd_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="gperf.idl"

InputPath=gperf.idl

"gperfC.inl" "gperfS.inl" "gperfC.h" "gperfS.h" "gperfS_T.h" "gperfC.cpp" "gperfS.cpp" "gperfS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-gperf_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="including.idl"

InputPath=including.idl

"includingC.inl" "includingS.inl" "includingC.h" "includingS.h" "includingS_T.h" "includingC.cpp" "includingS.cpp" "includingS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-including_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="interface.idl"

InputPath=interface.idl

"interfaceC.inl" "interfaceS.inl" "interfaceC.h" "interfaceS.h" "interfaceS_T.h" "interfaceC.cpp" "interfaceS.cpp" "interfaceS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-interface_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="included.idl"

InputPath=included.idl

"includedC.inl" "includedS.inl" "includedC.h" "includedS.h" "includedS_T.h" "includedC.cpp" "includedS.cpp" "includedS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-included_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="nested_scope.idl"

InputPath=nested_scope.idl

"nested_scopeC.inl" "nested_scopeS.inl" "nested_scopeC.h" "nested_scopeS.h" "nested_scopeS_T.h" "nested_scopeC.cpp" "nested_scopeS.cpp" "nested_scopeS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-nested_scope_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="old_array.idl"

InputPath=old_array.idl

"old_arrayC.inl" "old_arrayS.inl" "old_arrayC.h" "old_arrayS.h" "old_arrayS_T.h" "old_arrayC.cpp" "old_arrayS.cpp" "old_arrayS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-old_array_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="old_constants.idl"

InputPath=old_constants.idl

"old_constantsC.inl" "old_constantsS.inl" "old_constantsC.h" "old_constantsS.h" "old_constantsS_T.h" "old_constantsC.cpp" "old_constantsS.cpp" "old_constantsS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-old_constants_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="old_sequence.idl"

InputPath=old_sequence.idl

"old_sequenceC.inl" "old_sequenceS.inl" "old_sequenceC.h" "old_sequenceS.h" "old_sequenceS_T.h" "old_sequenceC.cpp" "old_sequenceS.cpp" "old_sequenceS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-old_sequence_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="simple.idl"

InputPath=simple.idl

"simpleC.inl" "simpleS.inl" "simpleC.h" "simpleS.h" "simpleS_T.h" "simpleC.cpp" "simpleS.cpp" "simpleS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-simple_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="simple2.idl"

InputPath=simple2.idl

"simple2C.inl" "simple2S.inl" "simple2C.h" "simple2S.h" "simple2S_T.h" "simple2C.cpp" "simple2S.cpp" "simple2S_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-simple2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="old_struct.idl"

InputPath=old_struct.idl

"old_structC.inl" "old_structS.inl" "old_structC.h" "old_structS.h" "old_structS_T.h" "old_structC.cpp" "old_structS.cpp" "old_structS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-old_struct_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="old_union.idl"

InputPath=old_union.idl

"old_unionC.inl" "old_unionS.inl" "old_unionC.h" "old_unionS.h" "old_unionS_T.h" "old_unionC.cpp" "old_unionS.cpp" "old_unionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-old_union_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="old_union2.idl"

InputPath=old_union2.idl

"old_union2C.inl" "old_union2S.inl" "old_union2C.h" "old_union2S.h" "old_union2S_T.h" "old_union2C.cpp" "old_union2S.cpp" "old_union2S_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-old_union2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="params.idl"

InputPath=params.idl

"paramsC.inl" "paramsS.inl" "paramsC.h" "paramsS.h" "paramsS_T.h" "paramsC.cpp" "paramsS.cpp" "paramsS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-params_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="reopened_modules.idl"

InputPath=reopened_modules.idl

"reopened_modulesC.inl" "reopened_modulesS.inl" "reopened_modulesC.h" "reopened_modulesS.h" "reopened_modulesS_T.h" "reopened_modulesC.cpp" "reopened_modulesS.cpp" "reopened_modulesS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-reopened_modules_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="sequence.idl"

InputPath=sequence.idl

"sequenceC.inl" "sequenceS.inl" "sequenceC.h" "sequenceS.h" "sequenceS_T.h" "sequenceC.cpp" "sequenceS.cpp" "sequenceS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-sequence_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="struct.idl"

InputPath=struct.idl

"structC.inl" "structS.inl" "structC.h" "structS.h" "structS_T.h" "structC.cpp" "structS.cpp" "structS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-struct_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="reopen_include1.idl"

InputPath=reopen_include1.idl

"reopen_include1C.inl" "reopen_include1S.inl" "reopen_include1C.h" "reopen_include1S.h" "reopen_include1S_T.h" "reopen_include1C.cpp" "reopen_include1S.cpp" "reopen_include1S_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-reopen_include1_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="reopen_include2.idl"

InputPath=reopen_include2.idl

"reopen_include2C.inl" "reopen_include2S.inl" "reopen_include2C.h" "reopen_include2S.h" "reopen_include2S_T.h" "reopen_include2C.cpp" "reopen_include2S.cpp" "reopen_include2S_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-reopen_include2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="typeprefix.idl"

InputPath=typeprefix.idl

"typeprefixC.inl" "typeprefixS.inl" "typeprefixC.h" "typeprefixS.h" "typeprefixS_T.h" "typeprefixC.cpp" "typeprefixS.cpp" "typeprefixS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-typeprefix_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="union.idl"

InputPath=union.idl

"unionC.inl" "unionS.inl" "unionC.h" "unionS.h" "unionS_T.h" "unionC.cpp" "unionS.cpp" "unionS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-union_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT "$(InputPath)"
<<

SOURCE="anonymous.idl"

InputPath=anonymous.idl

"anonymousC.inl" "anonymousS.inl" "anonymousC.h" "anonymousS.h" "anonymousA.h" "anonymousS_T.h" "anonymousC.cpp" "anonymousS.cpp" "anonymousA.cpp" "anonymousS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-anonymous_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="array_only.idl"

InputPath=array_only.idl

"array_onlyC.inl" "array_onlyS.inl" "array_onlyC.h" "array_onlyS.h" "array_onlyA.h" "array_onlyS_T.h" "array_onlyC.cpp" "array_onlyS.cpp" "array_onlyA.cpp" "array_onlyS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-array_only_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="constants.idl"

InputPath=constants.idl

"constantsC.inl" "constantsS.inl" "constantsC.h" "constantsS.h" "constantsA.h" "constantsS_T.h" "constantsC.cpp" "constantsS.cpp" "constantsA.cpp" "constantsS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-constants_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="generic_object.idl"

InputPath=generic_object.idl

"generic_objectC.inl" "generic_objectS.inl" "generic_objectC.h" "generic_objectS.h" "generic_objectA.h" "generic_objectS_T.h" "generic_objectC.cpp" "generic_objectS.cpp" "generic_objectA.cpp" "generic_objectS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-generic_object_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="keywords.idl"

InputPath=keywords.idl

"keywordsC.inl" "keywordsS.inl" "keywordsC.h" "keywordsS.h" "keywordsA.h" "keywordsS_T.h" "keywordsC.cpp" "keywordsS.cpp" "keywordsA.cpp" "keywordsS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-keywords_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="dif2.idl"

InputPath=dif2.idl

"dif2C.inl" "dif2S.inl" "dif2C.h" "dif2S.h" "dif2A.h" "dif2S_T.h" "dif2C.cpp" "dif2S.cpp" "dif2A.cpp" "dif2S_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-dif2_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="inherit.idl"

InputPath=inherit.idl

"inheritC.inl" "inheritS.inl" "inheritC.h" "inheritS.h" "inheritA.h" "inheritS_T.h" "inheritC.cpp" "inheritS.cpp" "inheritA.cpp" "inheritS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-inherit_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="module.idl"

InputPath=module.idl

"moduleC.inl" "moduleS.inl" "moduleC.h" "moduleS.h" "moduleA.h" "moduleS_T.h" "moduleC.cpp" "moduleS.cpp" "moduleA.cpp" "moduleS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-module_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="primtypes.idl"

InputPath=primtypes.idl

"primtypesC.inl" "primtypesS.inl" "primtypesC.h" "primtypesS.h" "primtypesA.h" "primtypesS_T.h" "primtypesC.cpp" "primtypesS.cpp" "primtypesA.cpp" "primtypesS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-primtypes_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="pragma.idl"

InputPath=pragma.idl

"pragmaC.inl" "pragmaS.inl" "pragmaC.h" "pragmaS.h" "pragmaA.h" "pragmaS_T.h" "pragmaC.cpp" "pragmaS.cpp" "pragmaA.cpp" "pragmaS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-pragma_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="repo_id_mod.idl"

InputPath=repo_id_mod.idl

"repo_id_modC.inl" "repo_id_modS.inl" "repo_id_modC.h" "repo_id_modS.h" "repo_id_modA.h" "repo_id_modS_T.h" "repo_id_modC.cpp" "repo_id_modS.cpp" "repo_id_modA.cpp" "repo_id_modS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-repo_id_mod_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="typedef.idl"

InputPath=typedef.idl

"typedefC.inl" "typedefS.inl" "typedefC.h" "typedefS.h" "typedefA.h" "typedefS_T.h" "typedefC.cpp" "typedefS.cpp" "typedefA.cpp" "typedefS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-typedef_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="typecode.idl"

InputPath=typecode.idl

"typecodeC.inl" "typecodeS.inl" "typecodeC.h" "typecodeS.h" "typecodeA.h" "typecodeS_T.h" "typecodeC.cpp" "typecodeS.cpp" "typecodeA.cpp" "typecodeS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-typecode_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

SOURCE="valuetype.idl"

InputPath=valuetype.idl

"valuetypeC.inl" "valuetypeS.inl" "valuetypeC.h" "valuetypeS.h" "valuetypeA.h" "valuetypeS_T.h" "valuetypeC.cpp" "valuetypeS.cpp" "valuetypeA.cpp" "valuetypeS_T.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-valuetype_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -GC -GH -Gd -GT -GA "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.IDL_Test_Main.dep")
	@echo Using "Makefile.IDL_Test_Main.dep"
!ELSE
	@echo Warning: cannot find "Makefile.IDL_Test_Main.dep"
!ENDIF
!ENDIF

