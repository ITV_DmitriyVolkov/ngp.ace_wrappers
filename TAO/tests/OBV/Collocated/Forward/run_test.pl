eval '(exit $?0)' && eval 'exec perl -S $0 ${1+"$@"}'
     & eval 'exec perl -S $0 $argv:q'
     if 0;

# $Id: run_test.pl 14 2007-02-01 15:49:12Z mitza $
# -*- perl -*-

use lib "$ENV{ACE_ROOT}/bin";
use PerlACE::Run_Test;

$iorfile = PerlACE::LocalFile ("test.ior");
unlink $iorfile;

if (PerlACE::is_vxworks_test()) {
    $CO = new PerlACE::ProcessVX ("collocated");
}
else {
    $CO = new PerlACE::Process ("collocated");
}

$CO->Spawn ();

if (PerlACE::waitforfile_timed ($iorfile,
                        $PerlACE::wait_interval_for_process_creation) == -1) {
    print STDERR "ERROR: cannot find file <$iorfile>\n";
    $SV->Kill (); $SV->TimedWait (1);
    exit 1;
} 

$result = $CO->WaitKill (10);

if ($result != 0) {
    print STDERR "ERROR: collocated returned $result\n";
    $result = 1;
}

unlink $iorfile;

exit $status;
