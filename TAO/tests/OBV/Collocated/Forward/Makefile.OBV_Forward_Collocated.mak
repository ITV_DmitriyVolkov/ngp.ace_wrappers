# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.OBV_Forward_Collocated.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Console Application")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "TreeNodeC.inl" "TreeNodeS.inl" "TreeNodeC.h" "TreeNodeS.h" "TreeNodeC.cpp" "TreeNodeS.cpp" "TreeControllerC.inl" "TreeControllerS.inl" "TreeControllerC.h" "TreeControllerS.h" "TreeControllerC.cpp" "TreeControllerS.cpp" "TreeBaseC.inl" "TreeBaseS.inl" "TreeBaseC.h" "TreeBaseS.h" "TreeBaseC.cpp" "TreeBaseS.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INSTALLDIR=.
INTDIR=Debug\OBV_Forward_Collocated\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\collocated.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -f "Makefile.OBV_Forward_Collocated.dep" "Test_impl.cpp" "TreeBaseC.cpp" "TreeControllerC.cpp" "TreeNodeC.cpp" "TreeBaseS.cpp" "TreeControllerS.cpp" "TreeNodeS.cpp" "Collocated_Test.cpp" "Client_Task.cpp" "Server_Task.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\collocated.pdb"
	-@del /f/q "$(INSTALLDIR)\collocated.exe"
	-@del /f/q "$(INSTALLDIR)\collocated.ilk"
	-@del /f/q "TreeNodeC.inl"
	-@del /f/q "TreeNodeS.inl"
	-@del /f/q "TreeNodeC.h"
	-@del /f/q "TreeNodeS.h"
	-@del /f/q "TreeNodeC.cpp"
	-@del /f/q "TreeNodeS.cpp"
	-@del /f/q "TreeControllerC.inl"
	-@del /f/q "TreeControllerS.inl"
	-@del /f/q "TreeControllerC.h"
	-@del /f/q "TreeControllerS.h"
	-@del /f/q "TreeControllerC.cpp"
	-@del /f/q "TreeControllerS.cpp"
	-@del /f/q "TreeBaseC.inl"
	-@del /f/q "TreeBaseS.inl"
	-@del /f/q "TreeBaseC.h"
	-@del /f/q "TreeBaseS.h"
	-@del /f/q "TreeBaseC.cpp"
	-@del /f/q "TreeBaseS.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\OBV_Forward_Collocated\$(NULL)" mkdir "Debug\OBV_Forward_Collocated"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\..\..\.." /I "..\..\..\.." /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_PortableServerd.lib TAO_Valuetyped.lib /libpath:"." /libpath:"..\..\..\..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\collocated.pdb" /machine:IA64 /out:"$(INSTALLDIR)\collocated.exe"
LINK32_OBJS= \
	"$(INTDIR)\Test_impl.obj" \
	"$(INTDIR)\TreeBaseC.obj" \
	"$(INTDIR)\TreeControllerC.obj" \
	"$(INTDIR)\TreeNodeC.obj" \
	"$(INTDIR)\TreeBaseS.obj" \
	"$(INTDIR)\TreeControllerS.obj" \
	"$(INTDIR)\TreeNodeS.obj" \
	"$(INTDIR)\Collocated_Test.obj" \
	"$(INTDIR)\Client_Task.obj" \
	"$(INTDIR)\Server_Task.obj"

"$(INSTALLDIR)\collocated.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\collocated.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\collocated.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=Release
INSTALLDIR=Release
INTDIR=Release\OBV_Forward_Collocated\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\collocated.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -f "Makefile.OBV_Forward_Collocated.dep" "Test_impl.cpp" "TreeBaseC.cpp" "TreeControllerC.cpp" "TreeNodeC.cpp" "TreeBaseS.cpp" "TreeControllerS.cpp" "TreeNodeS.cpp" "Collocated_Test.cpp" "Client_Task.cpp" "Server_Task.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\collocated.exe"
	-@del /f/q "$(INSTALLDIR)\collocated.ilk"
	-@del /f/q "TreeNodeC.inl"
	-@del /f/q "TreeNodeS.inl"
	-@del /f/q "TreeNodeC.h"
	-@del /f/q "TreeNodeS.h"
	-@del /f/q "TreeNodeC.cpp"
	-@del /f/q "TreeNodeS.cpp"
	-@del /f/q "TreeControllerC.inl"
	-@del /f/q "TreeControllerS.inl"
	-@del /f/q "TreeControllerC.h"
	-@del /f/q "TreeControllerS.h"
	-@del /f/q "TreeControllerC.cpp"
	-@del /f/q "TreeControllerS.cpp"
	-@del /f/q "TreeBaseC.inl"
	-@del /f/q "TreeBaseS.inl"
	-@del /f/q "TreeBaseC.h"
	-@del /f/q "TreeBaseS.h"
	-@del /f/q "TreeBaseC.cpp"
	-@del /f/q "TreeBaseS.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\OBV_Forward_Collocated\$(NULL)" mkdir "Release\OBV_Forward_Collocated"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\..\.." /I "..\..\..\.." /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_PortableServer.lib TAO_Valuetype.lib /libpath:"." /libpath:"..\..\..\..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\collocated.exe"
LINK32_OBJS= \
	"$(INTDIR)\Test_impl.obj" \
	"$(INTDIR)\TreeBaseC.obj" \
	"$(INTDIR)\TreeControllerC.obj" \
	"$(INTDIR)\TreeNodeC.obj" \
	"$(INTDIR)\TreeBaseS.obj" \
	"$(INTDIR)\TreeControllerS.obj" \
	"$(INTDIR)\TreeNodeS.obj" \
	"$(INTDIR)\Collocated_Test.obj" \
	"$(INTDIR)\Client_Task.obj" \
	"$(INTDIR)\Server_Task.obj"

"$(INSTALLDIR)\collocated.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\collocated.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\collocated.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=Static_Debug
INSTALLDIR=Static_Debug
INTDIR=Static_Debug\OBV_Forward_Collocated\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\collocated.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.OBV_Forward_Collocated.dep" "Test_impl.cpp" "TreeBaseC.cpp" "TreeControllerC.cpp" "TreeNodeC.cpp" "TreeBaseS.cpp" "TreeControllerS.cpp" "TreeNodeS.cpp" "Collocated_Test.cpp" "Client_Task.cpp" "Server_Task.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\collocated.pdb"
	-@del /f/q "$(INSTALLDIR)\collocated.exe"
	-@del /f/q "$(INSTALLDIR)\collocated.ilk"
	-@del /f/q "TreeNodeC.inl"
	-@del /f/q "TreeNodeS.inl"
	-@del /f/q "TreeNodeC.h"
	-@del /f/q "TreeNodeS.h"
	-@del /f/q "TreeNodeC.cpp"
	-@del /f/q "TreeNodeS.cpp"
	-@del /f/q "TreeControllerC.inl"
	-@del /f/q "TreeControllerS.inl"
	-@del /f/q "TreeControllerC.h"
	-@del /f/q "TreeControllerS.h"
	-@del /f/q "TreeControllerC.cpp"
	-@del /f/q "TreeControllerS.cpp"
	-@del /f/q "TreeBaseC.inl"
	-@del /f/q "TreeBaseS.inl"
	-@del /f/q "TreeBaseC.h"
	-@del /f/q "TreeBaseS.h"
	-@del /f/q "TreeBaseC.cpp"
	-@del /f/q "TreeBaseS.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\OBV_Forward_Collocated\$(NULL)" mkdir "Static_Debug\OBV_Forward_Collocated"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\..\..\.." /I "..\..\..\.." /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEsd.lib TAOsd.lib TAO_AnyTypeCodesd.lib TAO_PortableServersd.lib TAO_Valuetypesd.lib /libpath:"." /libpath:"..\..\..\..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\collocated.pdb" /machine:IA64 /out:"$(INSTALLDIR)\collocated.exe"
LINK32_OBJS= \
	"$(INTDIR)\Test_impl.obj" \
	"$(INTDIR)\TreeBaseC.obj" \
	"$(INTDIR)\TreeControllerC.obj" \
	"$(INTDIR)\TreeNodeC.obj" \
	"$(INTDIR)\TreeBaseS.obj" \
	"$(INTDIR)\TreeControllerS.obj" \
	"$(INTDIR)\TreeNodeS.obj" \
	"$(INTDIR)\Collocated_Test.obj" \
	"$(INTDIR)\Client_Task.obj" \
	"$(INTDIR)\Server_Task.obj"

"$(INSTALLDIR)\collocated.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\collocated.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\collocated.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=Static_Release
INSTALLDIR=Static_Release
INTDIR=Static_Release\OBV_Forward_Collocated\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\collocated.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.OBV_Forward_Collocated.dep" "Test_impl.cpp" "TreeBaseC.cpp" "TreeControllerC.cpp" "TreeNodeC.cpp" "TreeBaseS.cpp" "TreeControllerS.cpp" "TreeNodeS.cpp" "Collocated_Test.cpp" "Client_Task.cpp" "Server_Task.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\collocated.exe"
	-@del /f/q "$(INSTALLDIR)\collocated.ilk"
	-@del /f/q "TreeNodeC.inl"
	-@del /f/q "TreeNodeS.inl"
	-@del /f/q "TreeNodeC.h"
	-@del /f/q "TreeNodeS.h"
	-@del /f/q "TreeNodeC.cpp"
	-@del /f/q "TreeNodeS.cpp"
	-@del /f/q "TreeControllerC.inl"
	-@del /f/q "TreeControllerS.inl"
	-@del /f/q "TreeControllerC.h"
	-@del /f/q "TreeControllerS.h"
	-@del /f/q "TreeControllerC.cpp"
	-@del /f/q "TreeControllerS.cpp"
	-@del /f/q "TreeBaseC.inl"
	-@del /f/q "TreeBaseS.inl"
	-@del /f/q "TreeBaseC.h"
	-@del /f/q "TreeBaseS.h"
	-@del /f/q "TreeBaseC.cpp"
	-@del /f/q "TreeBaseS.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\OBV_Forward_Collocated\$(NULL)" mkdir "Static_Release\OBV_Forward_Collocated"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\..\.." /I "..\..\..\.." /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEs.lib TAOs.lib TAO_AnyTypeCodes.lib TAO_PortableServers.lib TAO_Valuetypes.lib /libpath:"." /libpath:"..\..\..\..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\collocated.exe"
LINK32_OBJS= \
	"$(INTDIR)\Test_impl.obj" \
	"$(INTDIR)\TreeBaseC.obj" \
	"$(INTDIR)\TreeControllerC.obj" \
	"$(INTDIR)\TreeNodeC.obj" \
	"$(INTDIR)\TreeBaseS.obj" \
	"$(INTDIR)\TreeControllerS.obj" \
	"$(INTDIR)\TreeNodeS.obj" \
	"$(INTDIR)\Collocated_Test.obj" \
	"$(INTDIR)\Client_Task.obj" \
	"$(INTDIR)\Server_Task.obj"

"$(INSTALLDIR)\collocated.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\collocated.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\collocated.exe.manifest" -outputresource:$@;1

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.OBV_Forward_Collocated.dep")
!INCLUDE "Makefile.OBV_Forward_Collocated.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Test_impl.cpp"

"$(INTDIR)\Test_impl.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Test_impl.obj" $(SOURCE)

SOURCE="TreeBaseC.cpp"

"$(INTDIR)\TreeBaseC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TreeBaseC.obj" $(SOURCE)

SOURCE="TreeControllerC.cpp"

"$(INTDIR)\TreeControllerC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TreeControllerC.obj" $(SOURCE)

SOURCE="TreeNodeC.cpp"

"$(INTDIR)\TreeNodeC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TreeNodeC.obj" $(SOURCE)

SOURCE="TreeBaseS.cpp"

"$(INTDIR)\TreeBaseS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TreeBaseS.obj" $(SOURCE)

SOURCE="TreeControllerS.cpp"

"$(INTDIR)\TreeControllerS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TreeControllerS.obj" $(SOURCE)

SOURCE="TreeNodeS.cpp"

"$(INTDIR)\TreeNodeS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TreeNodeS.obj" $(SOURCE)

SOURCE="Collocated_Test.cpp"

"$(INTDIR)\Collocated_Test.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Collocated_Test.obj" $(SOURCE)

SOURCE="Client_Task.cpp"

"$(INTDIR)\Client_Task.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Client_Task.obj" $(SOURCE)

SOURCE="Server_Task.cpp"

"$(INTDIR)\Server_Task.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Server_Task.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="TreeNode.idl"

InputPath=TreeNode.idl

"TreeNodeC.inl" "TreeNodeS.inl" "TreeNodeC.h" "TreeNodeS.h" "TreeNodeC.cpp" "TreeNodeS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-TreeNode_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. "$(InputPath)"
<<

SOURCE="TreeController.idl"

InputPath=TreeController.idl

"TreeControllerC.inl" "TreeControllerS.inl" "TreeControllerC.h" "TreeControllerS.h" "TreeControllerC.cpp" "TreeControllerS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-TreeController_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. "$(InputPath)"
<<

SOURCE="TreeBase.idl"

InputPath=TreeBase.idl

"TreeBaseC.inl" "TreeBaseS.inl" "TreeBaseC.h" "TreeBaseS.h" "TreeBaseC.cpp" "TreeBaseS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-TreeBase_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="TreeNode.idl"

InputPath=TreeNode.idl

"TreeNodeC.inl" "TreeNodeS.inl" "TreeNodeC.h" "TreeNodeS.h" "TreeNodeC.cpp" "TreeNodeS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-TreeNode_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. "$(InputPath)"
<<

SOURCE="TreeController.idl"

InputPath=TreeController.idl

"TreeControllerC.inl" "TreeControllerS.inl" "TreeControllerC.h" "TreeControllerS.h" "TreeControllerC.cpp" "TreeControllerS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-TreeController_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. "$(InputPath)"
<<

SOURCE="TreeBase.idl"

InputPath=TreeBase.idl

"TreeBaseC.inl" "TreeBaseS.inl" "TreeBaseC.h" "TreeBaseS.h" "TreeBaseC.cpp" "TreeBaseS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-TreeBase_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="TreeNode.idl"

InputPath=TreeNode.idl

"TreeNodeC.inl" "TreeNodeS.inl" "TreeNodeC.h" "TreeNodeS.h" "TreeNodeC.cpp" "TreeNodeS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-TreeNode_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. "$(InputPath)"
<<

SOURCE="TreeController.idl"

InputPath=TreeController.idl

"TreeControllerC.inl" "TreeControllerS.inl" "TreeControllerC.h" "TreeControllerS.h" "TreeControllerC.cpp" "TreeControllerS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-TreeController_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. "$(InputPath)"
<<

SOURCE="TreeBase.idl"

InputPath=TreeBase.idl

"TreeBaseC.inl" "TreeBaseS.inl" "TreeBaseC.h" "TreeBaseS.h" "TreeBaseC.cpp" "TreeBaseS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-TreeBase_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="TreeNode.idl"

InputPath=TreeNode.idl

"TreeNodeC.inl" "TreeNodeS.inl" "TreeNodeC.h" "TreeNodeS.h" "TreeNodeC.cpp" "TreeNodeS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-TreeNode_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. "$(InputPath)"
<<

SOURCE="TreeController.idl"

InputPath=TreeController.idl

"TreeControllerC.inl" "TreeControllerS.inl" "TreeControllerC.h" "TreeControllerS.h" "TreeControllerC.cpp" "TreeControllerS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-TreeController_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. "$(InputPath)"
<<

SOURCE="TreeBase.idl"

InputPath=TreeBase.idl

"TreeBaseC.inl" "TreeBaseS.inl" "TreeBaseC.h" "TreeBaseS.h" "TreeBaseC.cpp" "TreeBaseS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-TreeBase_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.OBV_Forward_Collocated.dep")
	@echo Using "Makefile.OBV_Forward_Collocated.dep"
!ELSE
	@echo Warning: cannot find "Makefile.OBV_Forward_Collocated.dep"
!ENDIF
!ENDIF

