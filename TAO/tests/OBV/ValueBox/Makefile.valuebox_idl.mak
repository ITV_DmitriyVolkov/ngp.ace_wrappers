# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.valuebox_idl.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "vb_basicC.inl" "vb_basicS.inl" "vb_basicC.h" "vb_basicS.h" "vb_basicC.cpp" "vb_basicS.cpp" "vb_structC.inl" "vb_structS.inl" "vb_structC.h" "vb_structS.h" "vb_structC.cpp" "vb_structS.cpp" "vb_unionC.inl" "vb_unionS.inl" "vb_unionC.h" "vb_unionS.h" "vb_unionC.cpp" "vb_unionS.cpp" "valueboxC.inl" "valueboxS.inl" "valueboxC.h" "valueboxS.h" "valueboxC.cpp" "valueboxS.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INTDIR=Debug\valuebox_idl\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY)

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	-@rem
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\d.lib"
	-@del /f/q "$(OUTDIR)\d.exp"
	-@del /f/q "$(OUTDIR)\d.ilk"
	-@del /f/q "vb_basicC.inl"
	-@del /f/q "vb_basicS.inl"
	-@del /f/q "vb_basicC.h"
	-@del /f/q "vb_basicS.h"
	-@del /f/q "vb_basicC.cpp"
	-@del /f/q "vb_basicS.cpp"
	-@del /f/q "vb_structC.inl"
	-@del /f/q "vb_structS.inl"
	-@del /f/q "vb_structC.h"
	-@del /f/q "vb_structS.h"
	-@del /f/q "vb_structC.cpp"
	-@del /f/q "vb_structS.cpp"
	-@del /f/q "vb_unionC.inl"
	-@del /f/q "vb_unionS.inl"
	-@del /f/q "vb_unionC.h"
	-@del /f/q "vb_unionS.h"
	-@del /f/q "vb_unionC.cpp"
	-@del /f/q "vb_unionS.cpp"
	-@del /f/q "valueboxC.inl"
	-@del /f/q "valueboxS.inl"
	-@del /f/q "valueboxC.h"
	-@del /f/q "valueboxS.h"
	-@del /f/q "valueboxC.cpp"
	-@del /f/q "valueboxS.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\valuebox_idl\$(NULL)" mkdir "Debug\valuebox_idl"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe


!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=.
INTDIR=Release\valuebox_idl\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY)

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	-@rem
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\.lib"
	-@del /f/q "$(OUTDIR)\.exp"
	-@del /f/q "$(OUTDIR)\.ilk"
	-@del /f/q "vb_basicC.inl"
	-@del /f/q "vb_basicS.inl"
	-@del /f/q "vb_basicC.h"
	-@del /f/q "vb_basicS.h"
	-@del /f/q "vb_basicC.cpp"
	-@del /f/q "vb_basicS.cpp"
	-@del /f/q "vb_structC.inl"
	-@del /f/q "vb_structS.inl"
	-@del /f/q "vb_structC.h"
	-@del /f/q "vb_structS.h"
	-@del /f/q "vb_structC.cpp"
	-@del /f/q "vb_structS.cpp"
	-@del /f/q "vb_unionC.inl"
	-@del /f/q "vb_unionS.inl"
	-@del /f/q "vb_unionC.h"
	-@del /f/q "vb_unionS.h"
	-@del /f/q "vb_unionC.cpp"
	-@del /f/q "vb_unionS.cpp"
	-@del /f/q "valueboxC.inl"
	-@del /f/q "valueboxS.inl"
	-@del /f/q "valueboxC.h"
	-@del /f/q "valueboxS.h"
	-@del /f/q "valueboxC.cpp"
	-@del /f/q "valueboxS.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\valuebox_idl\$(NULL)" mkdir "Release\valuebox_idl"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\.." /I "..\..\.." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe


!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=.
INTDIR=Static_Debug\valuebox_idl\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY)

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	-@rem
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q ".\sd.pdb"
	-@del /f/q "vb_basicC.inl"
	-@del /f/q "vb_basicS.inl"
	-@del /f/q "vb_basicC.h"
	-@del /f/q "vb_basicS.h"
	-@del /f/q "vb_basicC.cpp"
	-@del /f/q "vb_basicS.cpp"
	-@del /f/q "vb_structC.inl"
	-@del /f/q "vb_structS.inl"
	-@del /f/q "vb_structC.h"
	-@del /f/q "vb_structS.h"
	-@del /f/q "vb_structC.cpp"
	-@del /f/q "vb_structS.cpp"
	-@del /f/q "vb_unionC.inl"
	-@del /f/q "vb_unionS.inl"
	-@del /f/q "vb_unionC.h"
	-@del /f/q "vb_unionS.h"
	-@del /f/q "vb_unionC.cpp"
	-@del /f/q "vb_unionS.cpp"
	-@del /f/q "valueboxC.inl"
	-@del /f/q "valueboxS.inl"
	-@del /f/q "valueboxC.h"
	-@del /f/q "valueboxS.h"
	-@del /f/q "valueboxC.cpp"
	-@del /f/q "valueboxS.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\valuebox_idl\$(NULL)" mkdir "Static_Debug\valuebox_idl"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd".\sd.pdb" /I "..\..\..\.." /I "..\..\.." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"



!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=.
INTDIR=Static_Release\valuebox_idl\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY)

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	-@rem
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "vb_basicC.inl"
	-@del /f/q "vb_basicS.inl"
	-@del /f/q "vb_basicC.h"
	-@del /f/q "vb_basicS.h"
	-@del /f/q "vb_basicC.cpp"
	-@del /f/q "vb_basicS.cpp"
	-@del /f/q "vb_structC.inl"
	-@del /f/q "vb_structS.inl"
	-@del /f/q "vb_structC.h"
	-@del /f/q "vb_structS.h"
	-@del /f/q "vb_structC.cpp"
	-@del /f/q "vb_structS.cpp"
	-@del /f/q "vb_unionC.inl"
	-@del /f/q "vb_unionS.inl"
	-@del /f/q "vb_unionC.h"
	-@del /f/q "vb_unionS.h"
	-@del /f/q "vb_unionC.cpp"
	-@del /f/q "vb_unionS.cpp"
	-@del /f/q "valueboxC.inl"
	-@del /f/q "valueboxS.inl"
	-@del /f/q "valueboxC.h"
	-@del /f/q "valueboxS.h"
	-@del /f/q "valueboxC.cpp"
	-@del /f/q "valueboxS.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\valuebox_idl\$(NULL)" mkdir "Static_Release\valuebox_idl"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\.." /I "..\..\.." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"



!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.valuebox_idl.dep")
!INCLUDE "Makefile.valuebox_idl.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
!IF  "$(CFG)" == "Win64 Debug"
SOURCE="vb_basic.idl"

InputPath=vb_basic.idl

"vb_basicC.inl" "vb_basicS.inl" "vb_basicC.h" "vb_basicS.h" "vb_basicC.cpp" "vb_basicS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-vb_basic_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. "$(InputPath)"
<<

SOURCE="vb_struct.idl"

InputPath=vb_struct.idl

"vb_structC.inl" "vb_structS.inl" "vb_structC.h" "vb_structS.h" "vb_structC.cpp" "vb_structS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-vb_struct_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. "$(InputPath)"
<<

SOURCE="vb_union.idl"

InputPath=vb_union.idl

"vb_unionC.inl" "vb_unionS.inl" "vb_unionC.h" "vb_unionS.h" "vb_unionC.cpp" "vb_unionS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-vb_union_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. "$(InputPath)"
<<

SOURCE="valuebox.idl"

InputPath=valuebox.idl

"valueboxC.inl" "valueboxS.inl" "valueboxC.h" "valueboxS.h" "valueboxC.cpp" "valueboxS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-valuebox_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="vb_basic.idl"

InputPath=vb_basic.idl

"vb_basicC.inl" "vb_basicS.inl" "vb_basicC.h" "vb_basicS.h" "vb_basicC.cpp" "vb_basicS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-vb_basic_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. "$(InputPath)"
<<

SOURCE="vb_struct.idl"

InputPath=vb_struct.idl

"vb_structC.inl" "vb_structS.inl" "vb_structC.h" "vb_structS.h" "vb_structC.cpp" "vb_structS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-vb_struct_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. "$(InputPath)"
<<

SOURCE="vb_union.idl"

InputPath=vb_union.idl

"vb_unionC.inl" "vb_unionS.inl" "vb_unionC.h" "vb_unionS.h" "vb_unionC.cpp" "vb_unionS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-vb_union_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. "$(InputPath)"
<<

SOURCE="valuebox.idl"

InputPath=valuebox.idl

"valueboxC.inl" "valueboxS.inl" "valueboxC.h" "valueboxS.h" "valueboxC.cpp" "valueboxS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-valuebox_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="vb_basic.idl"

InputPath=vb_basic.idl

"vb_basicC.inl" "vb_basicS.inl" "vb_basicC.h" "vb_basicS.h" "vb_basicC.cpp" "vb_basicS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-vb_basic_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. "$(InputPath)"
<<

SOURCE="vb_struct.idl"

InputPath=vb_struct.idl

"vb_structC.inl" "vb_structS.inl" "vb_structC.h" "vb_structS.h" "vb_structC.cpp" "vb_structS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-vb_struct_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. "$(InputPath)"
<<

SOURCE="vb_union.idl"

InputPath=vb_union.idl

"vb_unionC.inl" "vb_unionS.inl" "vb_unionC.h" "vb_unionS.h" "vb_unionC.cpp" "vb_unionS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-vb_union_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. "$(InputPath)"
<<

SOURCE="valuebox.idl"

InputPath=valuebox.idl

"valueboxC.inl" "valueboxS.inl" "valueboxC.h" "valueboxS.h" "valueboxC.cpp" "valueboxS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-valuebox_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="vb_basic.idl"

InputPath=vb_basic.idl

"vb_basicC.inl" "vb_basicS.inl" "vb_basicC.h" "vb_basicS.h" "vb_basicC.cpp" "vb_basicS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-vb_basic_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. "$(InputPath)"
<<

SOURCE="vb_struct.idl"

InputPath=vb_struct.idl

"vb_structC.inl" "vb_structS.inl" "vb_structC.h" "vb_structS.h" "vb_structC.cpp" "vb_structS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-vb_struct_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. "$(InputPath)"
<<

SOURCE="vb_union.idl"

InputPath=vb_union.idl

"vb_unionC.inl" "vb_unionS.inl" "vb_unionC.h" "vb_unionS.h" "vb_unionC.cpp" "vb_unionS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-vb_union_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. "$(InputPath)"
<<

SOURCE="valuebox.idl"

InputPath=valuebox.idl

"valueboxC.inl" "valueboxS.inl" "valueboxC.h" "valueboxS.h" "valueboxC.cpp" "valueboxS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-valuebox_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.valuebox_idl.dep")
	@echo Using "Makefile.valuebox_idl.dep"
!ENDIF
!ENDIF

