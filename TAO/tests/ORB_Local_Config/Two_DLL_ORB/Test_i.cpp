//
// $Id: Test_i.cpp 935 2008-12-10 21:47:27Z mitza $
//
#include "Test_i.h"

ACE_RCSID(Hello, Hello, "$Id: Test_i.cpp 935 2008-12-10 21:47:27Z mitza $")

Hello::Hello (CORBA::ORB_ptr orb)
  : orb_ (CORBA::ORB::_duplicate (orb))
{
}

char *
Hello::get_string (void)
{
  return CORBA::string_dup ("Hello there!");
}

void
Hello::shutdown (void)
{
  this->orb_->shutdown (0);
}
