#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake -value_template platforms=Win64 -make_coexistence -recurse -exclude TAO/CIAO -features qos=1,mfc=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: LocalObject_Messenger_Idl LocalObject_Messenger_Server

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.LocalObject_Messenger_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.LocalObject_Messenger_Idl.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.LocalObject_Messenger_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.LocalObject_Messenger_Server.mak CFG="$(CFG)" $(@)

LocalObject_Messenger_Idl:
	@echo Project: Makefile.LocalObject_Messenger_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.LocalObject_Messenger_Idl.mak CFG="$(CFG)" all

LocalObject_Messenger_Server: LocalObject_Messenger_Idl
	@echo Project: Makefile.LocalObject_Messenger_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.LocalObject_Messenger_Server.mak CFG="$(CFG)" all

project_name_list:
	@echo LocalObject_Messenger_Idl
	@echo LocalObject_Messenger_Server
