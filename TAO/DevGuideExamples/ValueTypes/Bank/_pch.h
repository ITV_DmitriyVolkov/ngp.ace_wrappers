// $Id: _pch.h 979 2008-12-31 20:22:32Z mitza $

#ifndef PCH_H
#define PCH_H

#ifdef USING_PCH
#include "tao/corba.h"
#include "tao/ORB_Core.h"
#include "tao/Stub.h"
#endif

#endif
