// $Id: Messenger_i.h 979 2008-12-31 20:22:32Z mitza $

#ifndef MESSENGER_I_H_
#define MESSENGER_I_H_

#include "MessengerS.h"

class  Messenger_i : public virtual POA_Messenger
{
public:
  virtual CORBA::Boolean send_message(Message*& msg);
};

#endif
