# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.ValueTypes_DevGuideMsgServer.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Console Application")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INSTALLDIR=.
INTDIR=Debug\ValueTypes_DevGuideMsgServer\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\MessengerServer.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DUSING_PCH -f "Makefile.ValueTypes_DevGuideMsgServer.dep" "Messenger_i.cpp" "Message_i.cpp" "MessengerServer.cpp" "MessengerC.cpp" "MessengerS.cpp" "_pch.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\MessengerServer.pdb"
	-@del /f/q "$(INSTALLDIR)\MessengerServer.exe"
	-@del /f/q "$(INSTALLDIR)\MessengerServer.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\ValueTypes_DevGuideMsgServer\$(NULL)" mkdir "Debug\ValueTypes_DevGuideMsgServer"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D MPC_LIB_MODIFIER=\"d\" /FD /c
CPP_PCH=/D USING_PCH /Yu"_pch.h" /Fp"$(INTDIR)\_pch.pch"
CPP_PROJ=$(CPP_COMMON) $(CPP_PCH) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_PortableServerd.lib TAO_Valuetyped.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\MessengerServer.pdb" /machine:IA64 /out:"$(INSTALLDIR)\MessengerServer.exe"
LINK32_OBJS= \
	"$(INTDIR)\_pch.obj" \
	"$(INTDIR)\Messenger_i.obj" \
	"$(INTDIR)\Message_i.obj" \
	"$(INTDIR)\MessengerServer.obj" \
	"$(INTDIR)\MessengerC.obj" \
	"$(INTDIR)\MessengerS.obj"

"$(INSTALLDIR)\MessengerServer.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\MessengerServer.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\MessengerServer.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=Release
INSTALLDIR=Release
INTDIR=Release\ValueTypes_DevGuideMsgServer\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\MessengerServer.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DUSING_PCH -f "Makefile.ValueTypes_DevGuideMsgServer.dep" "Messenger_i.cpp" "Message_i.cpp" "MessengerServer.cpp" "MessengerC.cpp" "MessengerS.cpp" "_pch.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\MessengerServer.exe"
	-@del /f/q "$(INSTALLDIR)\MessengerServer.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\ValueTypes_DevGuideMsgServer\$(NULL)" mkdir "Release\ValueTypes_DevGuideMsgServer"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\.." /I "..\..\.." /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE  /FD /c
CPP_PCH=/D USING_PCH /Yu"_pch.h" /Fp"$(INTDIR)\_pch.pch"
CPP_PROJ=$(CPP_COMMON) $(CPP_PCH) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_PortableServer.lib TAO_Valuetype.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\MessengerServer.exe"
LINK32_OBJS= \
	"$(INTDIR)\_pch.obj" \
	"$(INTDIR)\Messenger_i.obj" \
	"$(INTDIR)\Message_i.obj" \
	"$(INTDIR)\MessengerServer.obj" \
	"$(INTDIR)\MessengerC.obj" \
	"$(INTDIR)\MessengerS.obj"

"$(INSTALLDIR)\MessengerServer.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\MessengerServer.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\MessengerServer.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=Static_Debug
INSTALLDIR=Static_Debug
INTDIR=Static_Debug\ValueTypes_DevGuideMsgServer\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\MessengerServer.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -DUSING_PCH -f "Makefile.ValueTypes_DevGuideMsgServer.dep" "Messenger_i.cpp" "Message_i.cpp" "MessengerServer.cpp" "MessengerC.cpp" "MessengerS.cpp" "_pch.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\MessengerServer.pdb"
	-@del /f/q "$(INSTALLDIR)\MessengerServer.exe"
	-@del /f/q "$(INSTALLDIR)\MessengerServer.ilk"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\ValueTypes_DevGuideMsgServer\$(NULL)" mkdir "Static_Debug\ValueTypes_DevGuideMsgServer"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c
CPP_PCH=/D USING_PCH /Yu"_pch.h" /Fp"$(INTDIR)\_pch.pch"
CPP_PROJ=$(CPP_COMMON) $(CPP_PCH) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEsd.lib TAOsd.lib TAO_AnyTypeCodesd.lib TAO_PortableServersd.lib TAO_Valuetypesd.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\MessengerServer.pdb" /machine:IA64 /out:"$(INSTALLDIR)\MessengerServer.exe"
LINK32_OBJS= \
	"$(INTDIR)\_pch.obj" \
	"$(INTDIR)\Messenger_i.obj" \
	"$(INTDIR)\Message_i.obj" \
	"$(INTDIR)\MessengerServer.obj" \
	"$(INTDIR)\MessengerC.obj" \
	"$(INTDIR)\MessengerS.obj"

"$(INSTALLDIR)\MessengerServer.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\MessengerServer.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\MessengerServer.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=Static_Release
INSTALLDIR=Static_Release
INTDIR=Static_Release\ValueTypes_DevGuideMsgServer\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\MessengerServer.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -DUSING_PCH -f "Makefile.ValueTypes_DevGuideMsgServer.dep" "Messenger_i.cpp" "Message_i.cpp" "MessengerServer.cpp" "MessengerC.cpp" "MessengerS.cpp" "_pch.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\MessengerServer.exe"
	-@del /f/q "$(INSTALLDIR)\MessengerServer.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\ValueTypes_DevGuideMsgServer\$(NULL)" mkdir "Static_Release\ValueTypes_DevGuideMsgServer"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\.." /I "..\..\.." /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c
CPP_PCH=/D USING_PCH /Yu"_pch.h" /Fp"$(INTDIR)\_pch.pch"
CPP_PROJ=$(CPP_COMMON) $(CPP_PCH) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEs.lib TAOs.lib TAO_AnyTypeCodes.lib TAO_PortableServers.lib TAO_Valuetypes.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\MessengerServer.exe"
LINK32_OBJS= \
	"$(INTDIR)\_pch.obj" \
	"$(INTDIR)\Messenger_i.obj" \
	"$(INTDIR)\Message_i.obj" \
	"$(INTDIR)\MessengerServer.obj" \
	"$(INTDIR)\MessengerC.obj" \
	"$(INTDIR)\MessengerS.obj"

"$(INSTALLDIR)\MessengerServer.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\MessengerServer.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\MessengerServer.exe.manifest" -outputresource:$@;1

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.ValueTypes_DevGuideMsgServer.dep")
!INCLUDE "Makefile.ValueTypes_DevGuideMsgServer.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="_pch.cpp"

!IF  "$(CFG)" == "Win64 Debug"

CPP_SWITCHES=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D USING_PCH /Fp"$(INTDIR)\_pch.pch" /Yc"_pch.h" /FD /c

"$(INTDIR)\_pch.obj" "$(INTDIR)\_pch.pch" : $(SOURCE)
	$(CPP) @<<
  $(CPP_SWITCHES) /Fo"$(INTDIR)\_pch.obj" $(SOURCE)
<<

!ELSEIF  "$(CFG)" == "Win64 Release"

CPP_SWITCHES=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\.." /I "..\..\.." /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D USING_PCH /Fp"$(INTDIR)\_pch.pch" /Yc"_pch.h" /FD /c

"$(INTDIR)\_pch.obj" "$(INTDIR)\_pch.pch" : $(SOURCE)
	$(CPP) @<<
  $(CPP_SWITCHES) /Fo"$(INTDIR)\_pch.obj" $(SOURCE)
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

CPP_SWITCHES=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D USING_PCH /Fp"$(INTDIR)\_pch.pch" /Yc"_pch.h" /FD /c

"$(INTDIR)\_pch.obj" "$(INTDIR)\_pch.pch" : $(SOURCE)
	$(CPP) @<<
  $(CPP_SWITCHES) /Fo"$(INTDIR)\_pch.obj" $(SOURCE)
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"

CPP_SWITCHES=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\.." /I "..\..\.." /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D USING_PCH /Fp"$(INTDIR)\_pch.pch" /Yc"_pch.h" /FD /c

"$(INTDIR)\_pch.obj" "$(INTDIR)\_pch.pch" : $(SOURCE)
	$(CPP) @<<
  $(CPP_SWITCHES) /Fo"$(INTDIR)\_pch.obj" $(SOURCE)
<<

!ENDIF

SOURCE="Messenger_i.cpp"

"$(INTDIR)\Messenger_i.obj" : $(SOURCE)
	$(CPP) $(CPP_PCH) $(CPP_COMMON) /Fo"$(INTDIR)\Messenger_i.obj" $(SOURCE)

SOURCE="Message_i.cpp"

"$(INTDIR)\Message_i.obj" : $(SOURCE)
	$(CPP) $(CPP_PCH) $(CPP_COMMON) /Fo"$(INTDIR)\Message_i.obj" $(SOURCE)

SOURCE="MessengerServer.cpp"

"$(INTDIR)\MessengerServer.obj" : $(SOURCE)
	$(CPP) $(CPP_PCH) $(CPP_COMMON) /Fo"$(INTDIR)\MessengerServer.obj" $(SOURCE)

SOURCE="MessengerC.cpp"

"$(INTDIR)\MessengerC.obj" : $(SOURCE)
	$(CPP) $(CPP_PCH) $(CPP_COMMON) /Fo"$(INTDIR)\MessengerC.obj" $(SOURCE)

SOURCE="MessengerS.cpp"

"$(INTDIR)\MessengerS.obj" : $(SOURCE)
	$(CPP) $(CPP_PCH) $(CPP_COMMON) /Fo"$(INTDIR)\MessengerS.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.ValueTypes_DevGuideMsgServer.dep")
	@echo Using "Makefile.ValueTypes_DevGuideMsgServer.dep"
!ELSE
	@echo Warning: cannot find "Makefile.ValueTypes_DevGuideMsgServer.dep"
!ENDIF
!ENDIF

