// $Id: common.h 979 2008-12-31 20:22:32Z mitza $

#ifndef MESSENGER_COMMON_H
#define MESSENGER_COMMON_H

#include "tao/corba.h"

CORBA::ULong get_total_lanes();
CORBA::Short get_increment();

#endif /* MESSENGER_COMMON_H */
