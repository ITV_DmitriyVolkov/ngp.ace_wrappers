# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.GettingStarted_Idl.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "MessengerC.inl" "MessengerS.inl" "MessengerC.h" "MessengerS.h" "MessengerC.cpp" "MessengerS.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INTDIR=Debug\GettingStarted_Idl\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY)

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -DUSING_PCH -f "Makefile.GettingStarted_Idl.dep" "started_pch.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\d.lib"
	-@del /f/q "$(OUTDIR)\d.exp"
	-@del /f/q "$(OUTDIR)\d.ilk"
	-@del /f/q "MessengerC.inl"
	-@del /f/q "MessengerS.inl"
	-@del /f/q "MessengerC.h"
	-@del /f/q "MessengerS.h"
	-@del /f/q "MessengerC.cpp"
	-@del /f/q "MessengerS.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\GettingStarted_Idl\$(NULL)" mkdir "Debug\GettingStarted_Idl"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /Fd"$(INTDIR)/" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c
CPP_PCH=/D USING_PCH /Yu"started_pch.h" /Fp"$(INTDIR)\started_pch.pch"
CPP_PROJ=$(CPP_COMMON) $(CPP_PCH) /Fo"$(INTDIR)\\"

RSC=rc.exe


!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=.
INTDIR=Release\GettingStarted_Idl\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY)

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -DUSING_PCH -f "Makefile.GettingStarted_Idl.dep" "started_pch.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\.lib"
	-@del /f/q "$(OUTDIR)\.exp"
	-@del /f/q "$(OUTDIR)\.ilk"
	-@del /f/q "MessengerC.inl"
	-@del /f/q "MessengerS.inl"
	-@del /f/q "MessengerC.h"
	-@del /f/q "MessengerS.h"
	-@del /f/q "MessengerC.cpp"
	-@del /f/q "MessengerS.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\GettingStarted_Idl\$(NULL)" mkdir "Release\GettingStarted_Idl"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c
CPP_PCH=/D USING_PCH /Yu"started_pch.h" /Fp"$(INTDIR)\started_pch.pch"
CPP_PROJ=$(CPP_COMMON) $(CPP_PCH) /Fo"$(INTDIR)\\"

RSC=rc.exe


!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=.
INTDIR=Static_Debug\GettingStarted_Idl\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY)

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -DUSING_PCH -f "Makefile.GettingStarted_Idl.dep" "started_pch.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q ".\sd.pdb"
	-@del /f/q "MessengerC.inl"
	-@del /f/q "MessengerS.inl"
	-@del /f/q "MessengerC.h"
	-@del /f/q "MessengerS.h"
	-@del /f/q "MessengerC.cpp"
	-@del /f/q "MessengerS.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\GettingStarted_Idl\$(NULL)" mkdir "Static_Debug\GettingStarted_Idl"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /Fd".\sd.pdb" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c
CPP_PCH=/D USING_PCH /Yu"started_pch.h" /Fp"$(INTDIR)\started_pch.pch"
CPP_PROJ=$(CPP_COMMON) $(CPP_PCH) /Fo"$(INTDIR)\\"



!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=.
INTDIR=Static_Release\GettingStarted_Idl\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY)

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -DUSING_PCH -f "Makefile.GettingStarted_Idl.dep" "started_pch.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "MessengerC.inl"
	-@del /f/q "MessengerS.inl"
	-@del /f/q "MessengerC.h"
	-@del /f/q "MessengerS.h"
	-@del /f/q "MessengerC.cpp"
	-@del /f/q "MessengerS.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\GettingStarted_Idl\$(NULL)" mkdir "Static_Release\GettingStarted_Idl"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c
CPP_PCH=/D USING_PCH /Yu"started_pch.h" /Fp"$(INTDIR)\started_pch.pch"
CPP_PROJ=$(CPP_COMMON) $(CPP_PCH) /Fo"$(INTDIR)\\"



!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.GettingStarted_Idl.dep")
!INCLUDE "Makefile.GettingStarted_Idl.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="started_pch.cpp"

!IF  "$(CFG)" == "Win64 Debug"

CPP_SWITCHES=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /Fd"$(INTDIR)/" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D USING_PCH /Fp"$(INTDIR)\started_pch.pch" /Yc"started_pch.h" /FD /c

"$(INTDIR)\started_pch.obj" "$(INTDIR)\started_pch.pch" : $(SOURCE)
	$(CPP) @<<
  $(CPP_SWITCHES) /Fo"$(INTDIR)\started_pch.obj" $(SOURCE)
<<

!ELSEIF  "$(CFG)" == "Win64 Release"

CPP_SWITCHES=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D USING_PCH /Fp"$(INTDIR)\started_pch.pch" /Yc"started_pch.h" /FD /c

"$(INTDIR)\started_pch.obj" "$(INTDIR)\started_pch.pch" : $(SOURCE)
	$(CPP) @<<
  $(CPP_SWITCHES) /Fo"$(INTDIR)\started_pch.obj" $(SOURCE)
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

CPP_SWITCHES=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /Fd".\sd.pdb" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D USING_PCH /Fp"$(INTDIR)\started_pch.pch" /Yc"started_pch.h" /FD /c

"$(INTDIR)\started_pch.obj" "$(INTDIR)\started_pch.pch" : $(SOURCE)
	$(CPP) @<<
  $(CPP_SWITCHES) /Fo"$(INTDIR)\started_pch.obj" $(SOURCE)
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"

CPP_SWITCHES=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D USING_PCH /Fp"$(INTDIR)\started_pch.pch" /Yc"started_pch.h" /FD /c

"$(INTDIR)\started_pch.obj" "$(INTDIR)\started_pch.pch" : $(SOURCE)
	$(CPP) @<<
  $(CPP_SWITCHES) /Fo"$(INTDIR)\started_pch.obj" $(SOURCE)
<<

!ENDIF

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="Messenger.idl"

InputPath=Messenger.idl

"MessengerC.inl" "MessengerS.inl" "MessengerC.h" "MessengerS.h" "MessengerC.cpp" "MessengerS.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Messenger_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Sa -St -Wb,pch_include=started_pch.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="Messenger.idl"

InputPath=Messenger.idl

"MessengerC.inl" "MessengerS.inl" "MessengerC.h" "MessengerS.h" "MessengerC.cpp" "MessengerS.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe" "..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Messenger_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Sa -St -Wb,pch_include=started_pch.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="Messenger.idl"

InputPath=Messenger.idl

"MessengerC.inl" "MessengerS.inl" "MessengerC.h" "MessengerS.h" "MessengerC.cpp" "MessengerS.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Messenger_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Sa -St -Wb,pch_include=started_pch.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="Messenger.idl"

InputPath=Messenger.idl

"MessengerC.inl" "MessengerS.inl" "MessengerC.h" "MessengerS.h" "MessengerC.cpp" "MessengerS.cpp" : $(SOURCE)  "..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Messenger_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\lib
	..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\.. -Sa -St -Wb,pch_include=started_pch.h "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.GettingStarted_Idl.dep")
	@echo Using "Makefile.GettingStarted_Idl.dep"
!ELSE
	@echo Warning: cannot find "Makefile.GettingStarted_Idl.dep"
!ENDIF
!ENDIF

