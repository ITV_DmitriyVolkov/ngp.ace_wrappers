# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.AMH_AMI_Inner_Server.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Console Application")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INSTALLDIR=.
INTDIR=Debug\AMH_AMI_Inner_Server\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\inner_server.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DUSING_PCH -f "Makefile.AMH_AMI_Inner_Server.dep" "innerS.cpp" "innerC.cpp" "inner_i.cpp" "inner_server.cpp" "amh_ami_pch.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\inner_server.pdb"
	-@del /f/q "$(INSTALLDIR)\inner_server.exe"
	-@del /f/q "$(INSTALLDIR)\inner_server.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\AMH_AMI_Inner_Server\$(NULL)" mkdir "Debug\AMH_AMI_Inner_Server"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\.." /I "..\.." /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D MPC_LIB_MODIFIER=\"d\" /FD /c
CPP_PCH=/D USING_PCH /Yu"amh_ami_pch.h" /Fp"$(INTDIR)\amh_ami_pch.pch"
CPP_PROJ=$(CPP_COMMON) $(CPP_PCH) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_Valuetyped.lib TAO_PortableServerd.lib TAO_CodecFactoryd.lib TAO_PId.lib TAO_Messagingd.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\inner_server.pdb" /machine:IA64 /out:"$(INSTALLDIR)\inner_server.exe"
LINK32_OBJS= \
	"$(INTDIR)\amh_ami_pch.obj" \
	"$(INTDIR)\innerS.obj" \
	"$(INTDIR)\innerC.obj" \
	"$(INTDIR)\inner_i.obj" \
	"$(INTDIR)\inner_server.obj"

"$(INSTALLDIR)\inner_server.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\inner_server.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\inner_server.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=Release
INSTALLDIR=Release
INTDIR=Release\AMH_AMI_Inner_Server\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\inner_server.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DUSING_PCH -f "Makefile.AMH_AMI_Inner_Server.dep" "innerS.cpp" "innerC.cpp" "inner_i.cpp" "inner_server.cpp" "amh_ami_pch.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\inner_server.exe"
	-@del /f/q "$(INSTALLDIR)\inner_server.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\AMH_AMI_Inner_Server\$(NULL)" mkdir "Release\AMH_AMI_Inner_Server"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\.." /I "..\.." /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE  /FD /c
CPP_PCH=/D USING_PCH /Yu"amh_ami_pch.h" /Fp"$(INTDIR)\amh_ami_pch.pch"
CPP_PROJ=$(CPP_COMMON) $(CPP_PCH) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_Valuetype.lib TAO_PortableServer.lib TAO_CodecFactory.lib TAO_PI.lib TAO_Messaging.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\inner_server.exe"
LINK32_OBJS= \
	"$(INTDIR)\amh_ami_pch.obj" \
	"$(INTDIR)\innerS.obj" \
	"$(INTDIR)\innerC.obj" \
	"$(INTDIR)\inner_i.obj" \
	"$(INTDIR)\inner_server.obj"

"$(INSTALLDIR)\inner_server.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\inner_server.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\inner_server.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=Static_Debug
INSTALLDIR=Static_Debug
INTDIR=Static_Debug\AMH_AMI_Inner_Server\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\inner_server.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -DUSING_PCH -f "Makefile.AMH_AMI_Inner_Server.dep" "innerS.cpp" "innerC.cpp" "inner_i.cpp" "inner_server.cpp" "amh_ami_pch.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\inner_server.pdb"
	-@del /f/q "$(INSTALLDIR)\inner_server.exe"
	-@del /f/q "$(INSTALLDIR)\inner_server.ilk"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\AMH_AMI_Inner_Server\$(NULL)" mkdir "Static_Debug\AMH_AMI_Inner_Server"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\.." /I "..\.." /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c
CPP_PCH=/D USING_PCH /Yu"amh_ami_pch.h" /Fp"$(INTDIR)\amh_ami_pch.pch"
CPP_PROJ=$(CPP_COMMON) $(CPP_PCH) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEsd.lib TAOsd.lib TAO_AnyTypeCodesd.lib TAO_Valuetypesd.lib TAO_PortableServersd.lib TAO_CodecFactorysd.lib TAO_PIsd.lib TAO_Messagingsd.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\inner_server.pdb" /machine:IA64 /out:"$(INSTALLDIR)\inner_server.exe"
LINK32_OBJS= \
	"$(INTDIR)\amh_ami_pch.obj" \
	"$(INTDIR)\innerS.obj" \
	"$(INTDIR)\innerC.obj" \
	"$(INTDIR)\inner_i.obj" \
	"$(INTDIR)\inner_server.obj"

"$(INSTALLDIR)\inner_server.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\inner_server.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\inner_server.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=Static_Release
INSTALLDIR=Static_Release
INTDIR=Static_Release\AMH_AMI_Inner_Server\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\inner_server.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I"..\.." -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -DUSING_PCH -f "Makefile.AMH_AMI_Inner_Server.dep" "innerS.cpp" "innerC.cpp" "inner_i.cpp" "inner_server.cpp" "amh_ami_pch.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\inner_server.exe"
	-@del /f/q "$(INSTALLDIR)\inner_server.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\AMH_AMI_Inner_Server\$(NULL)" mkdir "Static_Release\AMH_AMI_Inner_Server"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\.." /I "..\.." /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c
CPP_PCH=/D USING_PCH /Yu"amh_ami_pch.h" /Fp"$(INTDIR)\amh_ami_pch.pch"
CPP_PROJ=$(CPP_COMMON) $(CPP_PCH) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEs.lib TAOs.lib TAO_AnyTypeCodes.lib TAO_Valuetypes.lib TAO_PortableServers.lib TAO_CodecFactorys.lib TAO_PIs.lib TAO_Messagings.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\inner_server.exe"
LINK32_OBJS= \
	"$(INTDIR)\amh_ami_pch.obj" \
	"$(INTDIR)\innerS.obj" \
	"$(INTDIR)\innerC.obj" \
	"$(INTDIR)\inner_i.obj" \
	"$(INTDIR)\inner_server.obj"

"$(INSTALLDIR)\inner_server.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\inner_server.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\inner_server.exe.manifest" -outputresource:$@;1

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.AMH_AMI_Inner_Server.dep")
!INCLUDE "Makefile.AMH_AMI_Inner_Server.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="amh_ami_pch.cpp"

!IF  "$(CFG)" == "Win64 Debug"

CPP_SWITCHES=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\.." /I "..\.." /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D USING_PCH /Fp"$(INTDIR)\amh_ami_pch.pch" /Yc"amh_ami_pch.h" /FD /c

"$(INTDIR)\amh_ami_pch.obj" "$(INTDIR)\amh_ami_pch.pch" : $(SOURCE)
	$(CPP) @<<
  $(CPP_SWITCHES) /Fo"$(INTDIR)\amh_ami_pch.obj" $(SOURCE)
<<

!ELSEIF  "$(CFG)" == "Win64 Release"

CPP_SWITCHES=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\.." /I "..\.." /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D USING_PCH /Fp"$(INTDIR)\amh_ami_pch.pch" /Yc"amh_ami_pch.h" /FD /c

"$(INTDIR)\amh_ami_pch.obj" "$(INTDIR)\amh_ami_pch.pch" : $(SOURCE)
	$(CPP) @<<
  $(CPP_SWITCHES) /Fo"$(INTDIR)\amh_ami_pch.obj" $(SOURCE)
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

CPP_SWITCHES=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\.." /I "..\.." /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D USING_PCH /Fp"$(INTDIR)\amh_ami_pch.pch" /Yc"amh_ami_pch.h" /FD /c

"$(INTDIR)\amh_ami_pch.obj" "$(INTDIR)\amh_ami_pch.pch" : $(SOURCE)
	$(CPP) @<<
  $(CPP_SWITCHES) /Fo"$(INTDIR)\amh_ami_pch.obj" $(SOURCE)
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"

CPP_SWITCHES=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\.." /I "..\.." /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D USING_PCH /Fp"$(INTDIR)\amh_ami_pch.pch" /Yc"amh_ami_pch.h" /FD /c

"$(INTDIR)\amh_ami_pch.obj" "$(INTDIR)\amh_ami_pch.pch" : $(SOURCE)
	$(CPP) @<<
  $(CPP_SWITCHES) /Fo"$(INTDIR)\amh_ami_pch.obj" $(SOURCE)
<<

!ENDIF

SOURCE="innerS.cpp"

"$(INTDIR)\innerS.obj" : $(SOURCE)
	$(CPP) $(CPP_PCH) $(CPP_COMMON) /Fo"$(INTDIR)\innerS.obj" $(SOURCE)

SOURCE="innerC.cpp"

"$(INTDIR)\innerC.obj" : $(SOURCE)
	$(CPP) $(CPP_PCH) $(CPP_COMMON) /Fo"$(INTDIR)\innerC.obj" $(SOURCE)

SOURCE="inner_i.cpp"

"$(INTDIR)\inner_i.obj" : $(SOURCE)
	$(CPP) $(CPP_PCH) $(CPP_COMMON) /Fo"$(INTDIR)\inner_i.obj" $(SOURCE)

SOURCE="inner_server.cpp"

"$(INTDIR)\inner_server.obj" : $(SOURCE)
	$(CPP) $(CPP_PCH) $(CPP_COMMON) /Fo"$(INTDIR)\inner_server.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.AMH_AMI_Inner_Server.dep")
	@echo Using "Makefile.AMH_AMI_Inner_Server.dep"
!ELSE
	@echo Warning: cannot find "Makefile.AMH_AMI_Inner_Server.dep"
!ENDIF
!ENDIF

