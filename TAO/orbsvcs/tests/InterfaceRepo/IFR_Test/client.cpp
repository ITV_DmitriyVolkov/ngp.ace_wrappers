// -*- C++ -*-
// $Id: client.cpp 935 2008-12-10 21:47:27Z mitza $

#include "Admin_Client.h"

ACE_RCSID (IFR_Test,
           client,
           "$Id: client.cpp 935 2008-12-10 21:47:27Z mitza $")

int ACE_TMAIN (int argc, ACE_TCHAR *argv[])
{
  Admin_Client admin_client;

  int retval = admin_client.init (argc, argv);

  if (retval == -1)
    {
      return 1;
    }

  return admin_client.run ();
}
