// -*- C++ -*-
// $Id: test.cpp 935 2008-12-10 21:47:27Z mitza $

#include "Ptest.h"

ACE_RCSID (Persistence_Test,
           test,
           "$Id: test.cpp 935 2008-12-10 21:47:27Z mitza $")

int ACE_TMAIN (int argc, ACE_TCHAR *argv[])
{
  Ptest ptest;

  int retval = ptest.init (argc, argv);

  if (retval == -1)
    return 1;

  retval = ptest.run ();

  return retval;
}
