// -*- C++ -*-
// $Id: client.cpp 935 2008-12-10 21:47:27Z mitza $

#include "idl3_client.h"

ACE_RCSID (Application_Test,
           client,
           "$Id: client.cpp 935 2008-12-10 21:47:27Z mitza $")

int
ACE_TMAIN(int argc, ACE_TCHAR *argv[])
{
  IDL3_Client client;

  try
    {
      if (client.init (argc,
                       argv)
           == -1)
        {
          return 1;
        }
      else
        {
          int status = client.run ();

          if (status == -1)
            {
              return 1;
            }
        }
    }
  catch (const CORBA::Exception& ex)
    {
      ex._tao_print_exception ("Client Exception");
      return -1;
    }

  return 0;
}
