#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake -value_template platforms=Win64 -make_coexistence -recurse -exclude TAO/CIAO -features qos=1,mfc=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: IFR_Application_Test_Client IFR_Application_Test_Server Bug_2962_Regression-target Bug_3155_Regression_Test_Idl Bug_3174_Regression_Test_Idl Bug_3495_Regression_Client Bug_3495_Regression_Idl Bug_3495_Regression_Server IFR_IDL3_Test IFR_Inheritance_Test-target IFR_IFR_Test Latency_Test-target IFR_Persistence_Test Union_Forward_Test_Client

clean depend generated realclean $(CUSTOM_TARGETS):
	@cd Application_Test
	@echo Directory: Application_Test
	@echo Project: Makefile.IFR_Application_Test_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.IFR_Application_Test_Client.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Application_Test
	@echo Directory: Application_Test
	@echo Project: Makefile.IFR_Application_Test_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.IFR_Application_Test_Server.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Bug_2962_Regression
	@echo Directory: Bug_2962_Regression
	@echo Project: Makefile.Bug_2962_Regression.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Bug_2962_Regression.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Bug_3155_Regression
	@echo Directory: Bug_3155_Regression
	@echo Project: Makefile.Bug_3155_Regression_Test_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Bug_3155_Regression_Test_Idl.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Bug_3174_Regression
	@echo Directory: Bug_3174_Regression
	@echo Project: Makefile.Bug_3174_Regression_Test_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Bug_3174_Regression_Test_Idl.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Bug_3495_Regression
	@echo Directory: Bug_3495_Regression
	@echo Project: Makefile.Bug_3495_Regression_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Bug_3495_Regression_Client.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Bug_3495_Regression
	@echo Directory: Bug_3495_Regression
	@echo Project: Makefile.Bug_3495_Regression_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Bug_3495_Regression_Idl.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Bug_3495_Regression
	@echo Directory: Bug_3495_Regression
	@echo Project: Makefile.Bug_3495_Regression_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Bug_3495_Regression_Server.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd IDL3_Test
	@echo Directory: IDL3_Test
	@echo Project: Makefile.IFR_IDL3_Test.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.IFR_IDL3_Test.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd IFR_Inheritance_Test
	@echo Directory: IFR_Inheritance_Test
	@echo Project: Makefile.IFR_Inheritance_Test.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.IFR_Inheritance_Test.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd IFR_Test
	@echo Directory: IFR_Test
	@echo Project: Makefile.IFR_IFR_Test.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.IFR_IFR_Test.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Latency_Test
	@echo Directory: Latency_Test
	@echo Project: Makefile.Latency_Test.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Latency_Test.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Persistence_Test
	@echo Directory: Persistence_Test
	@echo Project: Makefile.IFR_Persistence_Test.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.IFR_Persistence_Test.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Union_Forward_Test
	@echo Directory: Union_Forward_Test
	@echo Project: Makefile.Union_Forward_Test_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Union_Forward_Test_Client.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)

IFR_Application_Test_Client:
	@cd Application_Test
	@echo Directory: Application_Test
	@echo Project: Makefile.IFR_Application_Test_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.IFR_Application_Test_Client.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

IFR_Application_Test_Server:
	@cd Application_Test
	@echo Directory: Application_Test
	@echo Project: Makefile.IFR_Application_Test_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.IFR_Application_Test_Server.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Bug_2962_Regression-target:
	@cd Bug_2962_Regression
	@echo Directory: Bug_2962_Regression
	@echo Project: Makefile.Bug_2962_Regression.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Bug_2962_Regression.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Bug_3155_Regression_Test_Idl:
	@cd Bug_3155_Regression
	@echo Directory: Bug_3155_Regression
	@echo Project: Makefile.Bug_3155_Regression_Test_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Bug_3155_Regression_Test_Idl.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Bug_3174_Regression_Test_Idl:
	@cd Bug_3174_Regression
	@echo Directory: Bug_3174_Regression
	@echo Project: Makefile.Bug_3174_Regression_Test_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Bug_3174_Regression_Test_Idl.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Bug_3495_Regression_Client:
	@cd Bug_3495_Regression
	@echo Directory: Bug_3495_Regression
	@echo Project: Makefile.Bug_3495_Regression_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Bug_3495_Regression_Client.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Bug_3495_Regression_Idl:
	@cd Bug_3495_Regression
	@echo Directory: Bug_3495_Regression
	@echo Project: Makefile.Bug_3495_Regression_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Bug_3495_Regression_Idl.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Bug_3495_Regression_Server: Bug_3495_Regression_Idl
	@cd Bug_3495_Regression
	@echo Directory: Bug_3495_Regression
	@echo Project: Makefile.Bug_3495_Regression_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Bug_3495_Regression_Server.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

IFR_IDL3_Test:
	@cd IDL3_Test
	@echo Directory: IDL3_Test
	@echo Project: Makefile.IFR_IDL3_Test.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.IFR_IDL3_Test.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

IFR_Inheritance_Test-target:
	@cd IFR_Inheritance_Test
	@echo Directory: IFR_Inheritance_Test
	@echo Project: Makefile.IFR_Inheritance_Test.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.IFR_Inheritance_Test.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

IFR_IFR_Test:
	@cd IFR_Test
	@echo Directory: IFR_Test
	@echo Project: Makefile.IFR_IFR_Test.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.IFR_IFR_Test.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Latency_Test-target:
	@cd Latency_Test
	@echo Directory: Latency_Test
	@echo Project: Makefile.Latency_Test.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Latency_Test.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

IFR_Persistence_Test:
	@cd Persistence_Test
	@echo Directory: Persistence_Test
	@echo Project: Makefile.IFR_Persistence_Test.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.IFR_Persistence_Test.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Union_Forward_Test_Client:
	@cd Union_Forward_Test
	@echo Directory: Union_Forward_Test
	@echo Project: Makefile.Union_Forward_Test_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Union_Forward_Test_Client.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

project_name_list:
	@echo IFR_Application_Test_Client
	@echo IFR_Application_Test_Server
	@echo Bug_2962_Regression-target
	@echo Bug_3155_Regression_Test_Idl
	@echo Bug_3174_Regression_Test_Idl
	@echo Bug_3495_Regression_Client
	@echo Bug_3495_Regression_Idl
	@echo Bug_3495_Regression_Server
	@echo IFR_IDL3_Test
	@echo IFR_Inheritance_Test-target
	@echo IFR_IFR_Test
	@echo Latency_Test-target
	@echo IFR_Persistence_Test
	@echo Union_Forward_Test_Client
