#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake -value_template platforms=Win64 -make_coexistence -recurse -exclude TAO/CIAO -features qos=1,mfc=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: CosEvent_Basic_Disconnect CosEvent_Basic_MT_Disconnect CosEvent_Basic_Pull_Push_Event CosEvent_Basic_Push_Event CosEvent_Basic_Random CosEvent_Basic_Shutdown

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.CosEvent_Basic_Disconnect.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.CosEvent_Basic_Disconnect.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.CosEvent_Basic_MT_Disconnect.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.CosEvent_Basic_MT_Disconnect.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.CosEvent_Basic_Pull_Push_Event.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.CosEvent_Basic_Pull_Push_Event.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.CosEvent_Basic_Push_Event.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.CosEvent_Basic_Push_Event.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.CosEvent_Basic_Random.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.CosEvent_Basic_Random.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.CosEvent_Basic_Shutdown.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.CosEvent_Basic_Shutdown.mak CFG="$(CFG)" $(@)

CosEvent_Basic_Disconnect:
	@echo Project: Makefile.CosEvent_Basic_Disconnect.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.CosEvent_Basic_Disconnect.mak CFG="$(CFG)" all

CosEvent_Basic_MT_Disconnect:
	@echo Project: Makefile.CosEvent_Basic_MT_Disconnect.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.CosEvent_Basic_MT_Disconnect.mak CFG="$(CFG)" all

CosEvent_Basic_Pull_Push_Event:
	@echo Project: Makefile.CosEvent_Basic_Pull_Push_Event.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.CosEvent_Basic_Pull_Push_Event.mak CFG="$(CFG)" all

CosEvent_Basic_Push_Event:
	@echo Project: Makefile.CosEvent_Basic_Push_Event.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.CosEvent_Basic_Push_Event.mak CFG="$(CFG)" all

CosEvent_Basic_Random:
	@echo Project: Makefile.CosEvent_Basic_Random.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.CosEvent_Basic_Random.mak CFG="$(CFG)" all

CosEvent_Basic_Shutdown:
	@echo Project: Makefile.CosEvent_Basic_Shutdown.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.CosEvent_Basic_Shutdown.mak CFG="$(CFG)" all

project_name_list:
	@echo CosEvent_Basic_Disconnect
	@echo CosEvent_Basic_MT_Disconnect
	@echo CosEvent_Basic_Pull_Push_Event
	@echo CosEvent_Basic_Push_Event
	@echo CosEvent_Basic_Random
	@echo CosEvent_Basic_Shutdown
