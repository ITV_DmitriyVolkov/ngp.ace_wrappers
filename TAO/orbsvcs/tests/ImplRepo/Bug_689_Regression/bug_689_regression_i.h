// $Id: bug_689_regression_i.h 979 2008-12-31 20:22:32Z mitza $
#ifndef BUG_680_REGRESSION_I_H
#define BUG_680_REGRESSION_I_H

#include "bug_689_regressionS.h"

class bug_689_regression_i: public POA_bug_689_regression
{
public:
  bug_689_regression_i (CORBA::ORB_ptr orb);
  ~bug_689_regression_i (void);

  virtual void shutdown (void);

private:
  CORBA::ORB_var orb_;
};

#endif
