/**
 * @file ORB_Task.cpp
 *
 * $Id: ORB_Task.cpp 1201 2009-06-12 20:22:45Z daiy $
 *
 * @author Carlos O'Ryan <coryan@atdesk.com>
 *
 */
#include "ORB_Task.h"
#include "tao/Environment.h"

ACE_RCSID (ReconnectServer,
           ORB_Task, "$Id: ORB_Task.cpp 1201 2009-06-12 20:22:45Z daiy $")

ORB_Task::ORB_Task (CORBA::ORB_ptr orb)
  : orb_ (CORBA::ORB::_duplicate (orb))
{
}

int
ORB_Task::svc (void)
{
  try
    {
      this->orb_->run ();
    }
  catch (const CORBA::Exception& )
    {
    }
  return 0;
}
