/**
 * @file ORB_Task.h
 *
 * $Id: ORB_Task.h 1201 2009-06-12 20:22:45Z daiy $
 *
 * @author Carlos O'Ryan <coryan@atdesk.com>
 *
 */
#ifndef IMR_REGRESSION_ORB_TASK_H
#define IMR_REGRESSION_ORB_TASK_H
#include "tao/ORB.h"
#include "ace/Task.h"

class ORB_Task : public ACE_Task_Base
{
public:
  ORB_Task (CORBA::ORB_ptr orb);

  virtual int svc (void);

private:
  CORBA::ORB_var orb_;
};

#endif /* IMR_REGRESSION_ORB_TASK_H*/
