eval '(exit $?0)' && eval 'exec perl -S $0 ${1+"$@"}'
     & eval 'exec perl -S $0 $argv:q'
     if 0;

# $Id: run_test.pl 1699 2010-07-21 14:07:39Z johnsonb $
# -*- perl -*-

# Additional synchronization patches available from OCI. Reference ticket
# #14157.

use lib "$ENV{ACE_ROOT}/bin";
use PerlACE::Run_Test;
use Sys::Hostname;

$iorfile = PerlACE::LocalFile ("server.ior");

unlink $iorfile;

$status = 0;
$host = hostname();

$ior = "file://$iorfile";
$server_port = 8088;

$server_config = PerlACE::LocalFile ("outside.conf");

$SV =
  new PerlACE::Process ("server",
                        "-o $iorfile "
			. "-ORBSvcConf outside.conf "
			. "-ORBDebuglevel 10 "
			. "-ORBVerboseLogging 1 "
			. "-ORBLogFile server.log "
                        . "-ORBEndpoint htiop://$host:$server_port ");

$CL = 
  new PerlACE::Process ("client", 
		        " -k $ior -n 3000 -c 5 -x "
			. "-ORBSvcConf inside.conf "
			. "-ORBDebuglevel 10 "
			. "-ORBVerboseLogging 1 "
			. "-ORBLogFile client.log ");

$SV->Spawn ();
print "Waiting for server to start\n";
if (PerlACE::waitforfile_timed ($iorfile, $PerlACE::wait_interval_for_process_creation) == -1) {
    print STDERR "ERROR: cannot find file <$iorfile>\n";
    $SV->Kill (); $SV->TimedWait (1);
    exit 1;
}

print "Running Client\n";
$client = $CL->SpawnWaitKill (300);

if ($client != 0) {
    print STDERR "ERROR: client returned $client\n";
    $status = 1;
    $SV->Kill(); $SV->TimedWait (1);
}
else {
    $server = $SV->WaitKill (10);

    if ($server != 0) {
        print STDERR "ERROR: server returned $server\n";
        $status = 1;
    }
}

unlink $iorfile;

exit $status;
