// $Id: server.cpp 1699 2010-07-21 14:07:39Z johnsonb $

// Additional synchronization patches available from OCI. Reference ticket
// #14157.

#include "Hello.h"

#include "ace/Get_Opt.h"
#include <ace/Task.h>
#include "tao/IORTable/IORTable.h"

int numThreads = 5;
class Worker : public ACE_Task_Base
{
  // = TITLE
  //   Run a server thread
  //
  // = DESCRIPTION
  //   Use the ACE_Task_Base class to run server threads
  //
public:
  Worker (CORBA::ORB_ptr orb);
  // ctor

  virtual int svc (void);
  // The thread entry point.

private:
  CORBA::ORB_var mOrb;
};

Worker::Worker (CORBA::ORB_ptr orb)  : 
  mOrb (CORBA::ORB::_duplicate (orb))
{
}

int Worker::svc (void)
{
  try
    {
      this->mOrb->run();
    }catch(...)
    {
      throw;
    }
  return 0;
}


ACE_RCSID (Hello,
           server,
           "$Id: server.cpp 1699 2010-07-21 14:07:39Z johnsonb $")

const ACE_TCHAR *ior_output_file = ACE_TEXT("test.ior");

int
parse_args (int argc, ACE_TCHAR *argv[])
{
  ACE_Get_Opt get_opts (argc, argv, ACE_TEXT("o:n:"));
  int c;

  while ((c = get_opts ()) != -1)
    switch (c)
      {
      case 'o':
        ior_output_file = get_opts.opt_arg ();
        break;
      case 'n':
	numThreads = atoi(ACE_TEXT_ALWAYS_CHAR(get_opts.opt_arg()));
	break;
      case '?':
      default:
        ACE_ERROR_RETURN ((LM_ERROR,
                           ACE_TEXT ("usage:  %s ")
                           ACE_TEXT ("-o <iorfile>")
                           ACE_TEXT ("-n <numthreads>")
                           ACE_TEXT ("\n"),
                           argv [0]),
                          -1);
      }
  // Indicates sucessful parsing of the command line
  return 0;
}

int
ACE_TMAIN(int argc, ACE_TCHAR *argv[])
{
  try
    {
      ACE_DEBUG ((LM_DEBUG, ACE_TEXT ("Begin of Hello_Server test\n")));

      CORBA::ORB_var orb =
        CORBA::ORB_init (argc, argv);

      //------ Get Root POA & POA Manager references

      CORBA::Object_var obj =
        orb->resolve_initial_references("RootPOA");

      PortableServer::POA_var root_poa =
        PortableServer::POA::_narrow (obj.in ());

      if (CORBA::is_nil (root_poa.in ()))
        ACE_ERROR_RETURN ((LM_ERROR,
                           ACE_TEXT (" (%P|%t) Panic: nil RootPOA\n")),
                          1);

      PortableServer::POAManager_var poa_manager =
        root_poa->the_POAManager ();

      //------- Get IOR Table reference to support CORBALOC URLs

      obj =
        orb->resolve_initial_references("IORTable");

      IORTable::Table_var ior_table =
        IORTable::Table::_narrow(obj.in());


      //-------- Prepare Servant

      if (parse_args (argc, argv) != 0)
        return 1;

      if (numThreads > 0) 
	{
	  Worker *worker = new Worker(orb);
	  if (worker->activate (THR_NEW_LWP | THR_JOINABLE, numThreads) != 0)
	    {
	      ACE_ERROR_RETURN ((LM_ERROR,
				 ACE_TEXT (" Failed to create the threads \n")),
				1);
	    }
	}

      Hello *hello_impl;
      ACE_NEW_RETURN (hello_impl,
                      Hello (orb.in ()),
                      1);
      PortableServer::ServantBase_var owner_transfer(hello_impl);

      Test::Hello_var hello =
        hello_impl->_this ();

      CORBA::String_var ior =
        orb->object_to_string (hello.in ());

      if (!CORBA::is_nil(ior_table.in()))
        ior_table->bind("HelloObj", ior.in());

      // Output the IOR to the <ior_output_file>
      FILE *output_file= ACE_OS::fopen (ior_output_file, "w");
      if (output_file == 0)
        ACE_ERROR_RETURN ((LM_ERROR,
                           ACE_TEXT ("Cannot open output file for writing IOR: %s"),
                           ior_output_file),
			  1);
      ACE_OS::fprintf (output_file, "%s", ior.in ());
      ACE_OS::fclose (output_file);

      poa_manager->activate ();

      orb->run ();

      ACE_DEBUG ((LM_DEBUG, ACE_TEXT ("(%P|%t) server - event loop finished\n")));
      
      root_poa->destroy (1, 1);
      
      ACE_DEBUG ((LM_DEBUG, ACE_TEXT ("(%P|%t) server - Root poa destroyed\n")));

      orb->destroy ();

      ACE_DEBUG ((LM_DEBUG, ACE_TEXT ("(%P|%t) server - orb destroyed\n")));
    }
  catch (const CORBA::Exception& ex)
    {
      ex._tao_print_exception (ACE_TEXT ("Exception caught:"));
      return 1;
    }

  return 0;
}
