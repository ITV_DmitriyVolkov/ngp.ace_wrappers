//
// $Id: Hello.cpp 1699 2010-07-21 14:07:39Z johnsonb $
//

// Additional synchronization patches available from OCI. Reference ticket
// #14157.

#include "Hello.h"
#include <iostream>

ACE_RCSID(Hello, Hello, "$Id: Hello.cpp 1699 2010-07-21 14:07:39Z johnsonb $")

Hello::Hello (CORBA::ORB_ptr orb)
  : orb_ (CORBA::ORB::_duplicate (orb))
{
}

char *
Hello::get_string (void)
{
  ACE_DEBUG ((LM_DEBUG, "(%P|%t) get_string called\n"));
  return CORBA::string_dup ("Hello there!");
}

void
Hello::shutdown (void)
{
  this->orb_->shutdown (0);
  ACE_DEBUG ((LM_DEBUG,"in shutdown\n"));
}

 char * Hello::get_string_n (::CORBA::Long length)
{
  char * buf = new char [length+1];
  memset(buf, 'A' + rand() % 26, length );
  buf[length] ='\0';
  ACE_DEBUG ((LM_DEBUG,"(%P|%t) get_string_n called:"
	      " filled buffer of length %d with %c\n",
	      length,  buf[0]));
  return buf;
}

void
Hello::put_string (const char * str)
{
  ACE_DEBUG ((LM_DEBUG,"(%P|%t) put_string called: length = %d\n",
	      ACE_OS::strlen(str)));
}
