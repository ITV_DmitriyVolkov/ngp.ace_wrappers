// $Id: client.cpp 1699 2010-07-21 14:07:39Z johnsonb $

// Additional synchronization patches available from OCI. Reference ticket
// #14157.

#include "TestC.h"
#include <ace/Get_Opt.h>
#include <iostream>
#include "ace/High_Res_Timer.h"

const ACE_TCHAR *ior = ACE_TEXT("file://test.ior");
long dataSize = 10000;
long loopCount = 1;
int
parse_args (int argc, ACE_TCHAR *argv[])
{
  ACE_Get_Opt get_opts (argc, argv, ACE_TEXT("k:n:c:"));
  int c;

  while ((c = get_opts ()) != -1)
    switch (c)
      {
      case 'k':
        ior = get_opts.opt_arg ();
        break;
      case 'n':
        dataSize = atoi(ACE_TEXT_ALWAYS_CHAR (get_opts.opt_arg()));
        break;
      case 'c':
        loopCount = atoi(ACE_TEXT_ALWAYS_CHAR (get_opts.opt_arg()));
        break;
      case '?':
      default:
        ACE_ERROR_RETURN ((LM_ERROR,
                           ACE_TEXT("usage:  %s ")
                           ACE_TEXT("-k <ior> ")
                           ACE_TEXT("-n <datasize> ")
                           ACE_TEXT("-c <loopcount> ")
                           ACE_TEXT("\n"),
                           argv [0]),
                          -1);
      }
  // Indicates sucessful parsing of the command line
  return 0;
}

int
ACE_TMAIN(int argc, ACE_TCHAR *argv[])
{
  try
    {

      CORBA::ORB_var orb =
        CORBA::ORB_init (argc, argv);

      if (parse_args (argc, argv) != 0)
        return 1;
#if 0
      CORBA::Object_var tmp =
        orb->string_to_object(ior);
#endif
	
      CORBA::Object_var tmp = orb->resolve_initial_references("HelloObj");
      Test::Hello_var hello =
        Test::Hello::_narrow(tmp.in ());

      if (CORBA::is_nil (hello.in ()))
        {
          ACE_ERROR_RETURN ((LM_DEBUG,
                             "Nil Test::Hello reference <%s>\n",
                             ior),
                            1);
        }

      CORBA::String_var the_string =
        hello->get_string ();
      std::cout <<"After get_string which returned: "
                << the_string.in() << std::endl;
      ACE_DEBUG ((LM_DEBUG, "(%P|%t) Client Main - string returned <%C>\n",
                  the_string.in ()));

      ACE_High_Res_Timer	timer;
      ACE_Time_Value		duration;

      timer.start();

      for (int i =0; i < loopCount ; i++){
        // std::cout <<"Requesting string of size: " << dataSize << std::endl;
        CORBA::String_var bigStr = hello->get_string_n(dataSize);
        //std::cout <<"Got the big string of length " << strlen(bigStr.in()) << " filled with " << bigStr[(CORBA::ULong)0L] << std::endl;
//         hello->put_string(bigStr.in());
        //std::cout <<"After put string" << std::endl;

      }
      timer.stop();
      timer.elapsed_time(duration);
      std::cout <<"Time take to make " << loopCount << " iteration is: " << duration.msec() << " msec" << std::endl;
      // hello->shutdown ();
      //      ACE_DEBUG ((LM_DEBUG, "(%P|%t) Client Main - shutdown returned\n"));

      orb->destroy ();
    }
  catch (const CORBA::Exception& ex)
    {
      std::cout <<"Exception occurred " << ex._name() << std::endl;
      ex._tao_print_exception ("Exception caught:");
      return 1;
    }

  return 0;
}
