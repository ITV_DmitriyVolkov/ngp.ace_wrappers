# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.Trading_Test_Lib.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "TTestC.inl" "TTestS.inl" "TTestC.h" "TTestS.h" "TTestC.cpp" "TTestS.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INTDIR=Debug\Trading_Test_Lib\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) ".\TTestd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\..\orbsvcs" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_TTEST_BUILD_DLL -f "Makefile.Trading_Test_Lib.dep" "TTestC.cpp" "TTestS.cpp" "TT_Info.cpp" "Service_Type_Exporter.cpp" "Offer_Exporter.cpp" "Offer_Importer.cpp" "Simple_Dynamic.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TTestd.pdb"
	-@del /f/q ".\TTestd.dll"
	-@del /f/q "$(OUTDIR)\TTestd.lib"
	-@del /f/q "$(OUTDIR)\TTestd.exp"
	-@del /f/q "$(OUTDIR)\TTestd.ilk"
	-@del /f/q "TTestC.inl"
	-@del /f/q "TTestS.inl"
	-@del /f/q "TTestC.h"
	-@del /f/q "TTestS.h"
	-@del /f/q "TTestC.cpp"
	-@del /f/q "TTestS.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\Trading_Test_Lib\$(NULL)" mkdir "Debug\Trading_Test_Lib"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\..\.." /I "..\..\.." /I "..\..\..\orbsvcs" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_TTEST_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_CosTradingd.lib TAO_PortableServerd.lib TAO_CosTrading_Skeld.lib TAO_Valuetyped.lib TAO_DynamicAnyd.lib TAO_Svc_Utilsd.lib TAO_CodecFactoryd.lib TAO_PId.lib TAO_Utilsd.lib TAO_IORTabled.lib TAO_CosTrading_Servd.lib TAO_CosNamingd.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:".\TTestd.pdb" /machine:IA64 /out:".\TTestd.dll" /implib:"$(OUTDIR)\TTestd.lib"
LINK32_OBJS= \
	"$(INTDIR)\TTestC.obj" \
	"$(INTDIR)\TTestS.obj" \
	"$(INTDIR)\TT_Info.obj" \
	"$(INTDIR)\Service_Type_Exporter.obj" \
	"$(INTDIR)\Offer_Exporter.obj" \
	"$(INTDIR)\Offer_Importer.obj" \
	"$(INTDIR)\Simple_Dynamic.obj"

".\TTestd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist ".\TTestd.dll.manifest" mt.exe -manifest ".\TTestd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=.
INTDIR=Release\Trading_Test_Lib\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) ".\TTest.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\..\orbsvcs" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_TTEST_BUILD_DLL -f "Makefile.Trading_Test_Lib.dep" "TTestC.cpp" "TTestS.cpp" "TT_Info.cpp" "Service_Type_Exporter.cpp" "Offer_Exporter.cpp" "Offer_Importer.cpp" "Simple_Dynamic.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q ".\TTest.dll"
	-@del /f/q "$(OUTDIR)\TTest.lib"
	-@del /f/q "$(OUTDIR)\TTest.exp"
	-@del /f/q "$(OUTDIR)\TTest.ilk"
	-@del /f/q "TTestC.inl"
	-@del /f/q "TTestS.inl"
	-@del /f/q "TTestC.h"
	-@del /f/q "TTestS.h"
	-@del /f/q "TTestC.cpp"
	-@del /f/q "TTestS.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\Trading_Test_Lib\$(NULL)" mkdir "Release\Trading_Test_Lib"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\.." /I "..\..\.." /I "..\..\..\orbsvcs" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_TTEST_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_CosTrading.lib TAO_PortableServer.lib TAO_CosTrading_Skel.lib TAO_Valuetype.lib TAO_DynamicAny.lib TAO_Svc_Utils.lib TAO_CodecFactory.lib TAO_PI.lib TAO_Utils.lib TAO_IORTable.lib TAO_CosTrading_Serv.lib TAO_CosNaming.lib /libpath:"." /libpath:"..\..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:".\TTest.dll" /implib:"$(OUTDIR)\TTest.lib"
LINK32_OBJS= \
	"$(INTDIR)\TTestC.obj" \
	"$(INTDIR)\TTestS.obj" \
	"$(INTDIR)\TT_Info.obj" \
	"$(INTDIR)\Service_Type_Exporter.obj" \
	"$(INTDIR)\Offer_Exporter.obj" \
	"$(INTDIR)\Offer_Importer.obj" \
	"$(INTDIR)\Simple_Dynamic.obj"

".\TTest.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist ".\TTest.dll.manifest" mt.exe -manifest ".\TTest.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=.
INTDIR=Static_Debug\Trading_Test_Lib\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TTestsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\..\orbsvcs" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Trading_Test_Lib.dep" "TTestC.cpp" "TTestS.cpp" "TT_Info.cpp" "Service_Type_Exporter.cpp" "Offer_Exporter.cpp" "Offer_Importer.cpp" "Simple_Dynamic.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TTestsd.lib"
	-@del /f/q "$(OUTDIR)\TTestsd.exp"
	-@del /f/q "$(OUTDIR)\TTestsd.ilk"
	-@del /f/q ".\TTestsd.pdb"
	-@del /f/q "TTestC.inl"
	-@del /f/q "TTestS.inl"
	-@del /f/q "TTestC.h"
	-@del /f/q "TTestS.h"
	-@del /f/q "TTestC.cpp"
	-@del /f/q "TTestS.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\Trading_Test_Lib\$(NULL)" mkdir "Static_Debug\Trading_Test_Lib"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd".\TTestsd.pdb" /I "..\..\..\.." /I "..\..\.." /I "..\..\..\orbsvcs" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:".\TTestsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\TTestC.obj" \
	"$(INTDIR)\TTestS.obj" \
	"$(INTDIR)\TT_Info.obj" \
	"$(INTDIR)\Service_Type_Exporter.obj" \
	"$(INTDIR)\Offer_Exporter.obj" \
	"$(INTDIR)\Offer_Importer.obj" \
	"$(INTDIR)\Simple_Dynamic.obj"

"$(OUTDIR)\TTestsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TTestsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TTestsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=.
INTDIR=Static_Release\Trading_Test_Lib\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TTests.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\.." -I"..\..\.." -I"..\..\..\orbsvcs" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Trading_Test_Lib.dep" "TTestC.cpp" "TTestS.cpp" "TT_Info.cpp" "Service_Type_Exporter.cpp" "Offer_Exporter.cpp" "Offer_Importer.cpp" "Simple_Dynamic.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TTests.lib"
	-@del /f/q "$(OUTDIR)\TTests.exp"
	-@del /f/q "$(OUTDIR)\TTests.ilk"
	-@del /f/q "TTestC.inl"
	-@del /f/q "TTestS.inl"
	-@del /f/q "TTestC.h"
	-@del /f/q "TTestS.h"
	-@del /f/q "TTestC.cpp"
	-@del /f/q "TTestS.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\Trading_Test_Lib\$(NULL)" mkdir "Static_Release\Trading_Test_Lib"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\.." /I "..\..\.." /I "..\..\..\orbsvcs" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:".\TTests.lib"
LINK32_OBJS= \
	"$(INTDIR)\TTestC.obj" \
	"$(INTDIR)\TTestS.obj" \
	"$(INTDIR)\TT_Info.obj" \
	"$(INTDIR)\Service_Type_Exporter.obj" \
	"$(INTDIR)\Offer_Exporter.obj" \
	"$(INTDIR)\Offer_Importer.obj" \
	"$(INTDIR)\Simple_Dynamic.obj"

"$(OUTDIR)\TTests.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TTests.lib.manifest" mt.exe -manifest "$(OUTDIR)\TTests.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Trading_Test_Lib.dep")
!INCLUDE "Makefile.Trading_Test_Lib.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="TTestC.cpp"

"$(INTDIR)\TTestC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TTestC.obj" $(SOURCE)

SOURCE="TTestS.cpp"

"$(INTDIR)\TTestS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TTestS.obj" $(SOURCE)

SOURCE="TT_Info.cpp"

"$(INTDIR)\TT_Info.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TT_Info.obj" $(SOURCE)

SOURCE="Service_Type_Exporter.cpp"

"$(INTDIR)\Service_Type_Exporter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Service_Type_Exporter.obj" $(SOURCE)

SOURCE="Offer_Exporter.cpp"

"$(INTDIR)\Offer_Exporter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Offer_Exporter.obj" $(SOURCE)

SOURCE="Offer_Importer.cpp"

"$(INTDIR)\Offer_Importer.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Offer_Importer.obj" $(SOURCE)

SOURCE="Simple_Dynamic.cpp"

"$(INTDIR)\Simple_Dynamic.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Simple_Dynamic.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="TTest.idl"

InputPath=TTest.idl

"TTestC.inl" "TTestS.inl" "TTestC.h" "TTestS.h" "TTestC.cpp" "TTestS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-TTest_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\..\.. -I..\..\..\orbsvcs -Wb,export_macro=TAO_TTest_Export -Wb,export_include=ttest_export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="TTest.idl"

InputPath=TTest.idl

"TTestC.inl" "TTestS.inl" "TTestC.h" "TTestS.h" "TTestC.cpp" "TTestS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-TTest_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\..\.. -I..\..\..\orbsvcs -Wb,export_macro=TAO_TTest_Export -Wb,export_include=ttest_export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="TTest.idl"

InputPath=TTest.idl

"TTestC.inl" "TTestS.inl" "TTestC.h" "TTestS.h" "TTestC.cpp" "TTestS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-TTest_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\..\.. -I..\..\..\orbsvcs -Wb,export_macro=TAO_TTest_Export -Wb,export_include=ttest_export.h "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="TTest.idl"

InputPath=TTest.idl

"TTestC.inl" "TTestS.inl" "TTestC.h" "TTestS.h" "TTestC.cpp" "TTestS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-TTest_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -I..\..\../orbsvcs -I..\..\.. -I..\..\..\orbsvcs -Wb,export_macro=TAO_TTest_Export -Wb,export_include=ttest_export.h "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Trading_Test_Lib.dep")
	@echo Using "Makefile.Trading_Test_Lib.dep"
!ELSE
	@echo Warning: cannot find "Makefile.Trading_Test_Lib.dep"
!ENDIF
!ENDIF

