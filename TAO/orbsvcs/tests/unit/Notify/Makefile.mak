#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake -value_template platforms=Win64 -make_coexistence -recurse -exclude TAO/CIAO -features qos=1,mfc=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: Control MonitorControlExt MonitorManager_Client MonitorManager_Server NotificationServiceMonitor Statistic Statistic_Registry

clean depend generated realclean $(CUSTOM_TARGETS):
	@cd MC/Control
	@echo Directory: MC/Control
	@echo Project: Makefile.Control.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Control.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd MC/MonitorControlExt
	@echo Directory: MC/MonitorControlExt
	@echo Project: Makefile.MonitorControlExt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.MonitorControlExt.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd MC/MonitorManager
	@echo Directory: MC/MonitorManager
	@echo Project: Makefile.MonitorManager_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.MonitorManager_Client.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd MC/MonitorManager
	@echo Directory: MC/MonitorManager
	@echo Project: Makefile.MonitorManager_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.MonitorManager_Server.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd MC/NotificationServiceMonitor
	@echo Directory: MC/NotificationServiceMonitor
	@echo Project: Makefile.NotificationServiceMonitor.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.NotificationServiceMonitor.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd MC/Statistic
	@echo Directory: MC/Statistic
	@echo Project: Makefile.Statistic.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Statistic.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd MC/Statistic_Registry
	@echo Directory: MC/Statistic_Registry
	@echo Project: Makefile.Statistic_Registry.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Statistic_Registry.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)

Control:
	@cd MC/Control
	@echo Directory: MC/Control
	@echo Project: Makefile.Control.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Control.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

MonitorControlExt:
	@cd MC/MonitorControlExt
	@echo Directory: MC/MonitorControlExt
	@echo Project: Makefile.MonitorControlExt.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.MonitorControlExt.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

MonitorManager_Client:
	@cd MC/MonitorManager
	@echo Directory: MC/MonitorManager
	@echo Project: Makefile.MonitorManager_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.MonitorManager_Client.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

MonitorManager_Server:
	@cd MC/MonitorManager
	@echo Directory: MC/MonitorManager
	@echo Project: Makefile.MonitorManager_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.MonitorManager_Server.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

NotificationServiceMonitor:
	@cd MC/NotificationServiceMonitor
	@echo Directory: MC/NotificationServiceMonitor
	@echo Project: Makefile.NotificationServiceMonitor.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.NotificationServiceMonitor.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Statistic:
	@cd MC/Statistic
	@echo Directory: MC/Statistic
	@echo Project: Makefile.Statistic.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Statistic.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Statistic_Registry:
	@cd MC/Statistic_Registry
	@echo Directory: MC/Statistic_Registry
	@echo Project: Makefile.Statistic_Registry.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Statistic_Registry.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

project_name_list:
	@echo Control
	@echo MonitorControlExt
	@echo MonitorManager_Client
	@echo MonitorManager_Server
	@echo NotificationServiceMonitor
	@echo Statistic
	@echo Statistic_Registry
