eval '(exit $?0)' && eval 'exec perl -S $0 ${1+"$@"}'
     & eval 'exec perl -S $0 $argv:q'
     if 0;

# $Id: run_test.pl 1815 2011-02-24 21:46:13Z hudsond $
# -*- perl -*-

use lib "$ENV{ACE_ROOT}/bin";
use PerlACE::Run_Test;

$nsiorfile = PerlACE::LocalFile("ns.ior");
unlink $nsiorfile;

my $exec_extn="";
if ($^O eq "MSWin32") {
  $exec_extn=".exe";
}

my @nslist_paths = (["../../../../bin", ""],
                    ["../../../../TAO/utils/nslist", ""]);
if (grep(($_ eq 'ARCH'), @PerlACE::ConfigList::Configs)) {
    push @nslist_paths, ["../../../../bin/" . $PerlACE::Process::ExeSubDir,
                         "../../../../bin"];
}

my $nslist_path = "";
for my $p (@nslist_paths) {
  my $use_path = ($p->[1] eq "") ? $p->[0] : $p->[1];
  if (-e $p->[0] . '/tao_nslist') {
    $nslist_path = $use_path . '/tao_nslist';
    last;
  }
  if ($exec_extn ne "" && -e $p->[0] . '/tao_nslist' . $exec_extn) {
    $nslist_path = $use_path . '/tao_nslist';
    last;
  }
}

if ($nslist_path eq "") {
    print STDERR "ERROR: tao_nslist utility not found.\n";
    exit 1;
}

$status = 0;

# Fire up the Name Service
$NS   = new PerlACE::Process ("../../../../TAO/orbsvcs/Naming_Service/Naming_Service");
$NS->Arguments("-o $nsiorfile -ORBObjRefStyle URL -ORBEndpoint shmiop:// -ORBSvcConfDirective \"dynamic SHMIOP_Factory Service_Object *TAO_Strategies:_make_TAO_SHMIOP_Protocol_Factory () ''\"");
$NS->Spawn ();

if (PerlACE::waitforfile_timed ($nsiorfile, 10) == -1)
{
   print STDERR "ERROR: cannot find Name Service IOR file <$nsiorfile>\n";
   $NS->Kill (); $NS->TimedWait (1);
   exit 1;
}

# Call nslist
$NSLIST  = new PerlACE::Process ($nslist_path);
$NSLIST->Arguments("-ORBSvcConfDirective \"dynamic SHMIOP_Factory Service_Object *TAO_Strategies:_make_TAO_SHMIOP_Protocol_Factory () ''\" -ORBInitRef NameService=file://$nsiorfile --ior --ctxior --node \"\" --tree \"\"");

$list_result = $NSLIST->SpawnWaitKill (300);

# Check return
if ($list_result != 0)
{
   print STDERR "ERROR: Regression - tao_nslist returned $listresult\n";
   $status = 1;
}

# Shutting down NameService and clean up
$server = $NS->TerminateWaitKill (5);

if ($server != 0)
{
    print STDERR "ERROR: Closing Name Service returned $server\n";
    $status = 1;
}

unlink $nsiorfile;

if ($status == 0)
{
    print "Test passed !!\n";
}

exit $status;
