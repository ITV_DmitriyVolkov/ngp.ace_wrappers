//
// $Id: NsShutdown.h 979 2008-12-31 20:22:32Z mitza $
//

#ifndef SHUTDOWN_H
#define SHUTDOWN_H
#include /**/ "ace/pre.h"

#include "TestS.h"

/// Implement the Test::Hello interface
class NsShutdown
  : public virtual POA_Test::NsShutdown
{
public:
  /// Constructor
  NsShutdown (CORBA::ORB_ptr orb);

  virtual void shutdown (void);

private:
  /// Use an ORB reference to conver strings to objects and shutdown
  /// the application.
  CORBA::ORB_var orb_;
};

#include /**/ "ace/post.h"
#endif /* HELLO_H */
