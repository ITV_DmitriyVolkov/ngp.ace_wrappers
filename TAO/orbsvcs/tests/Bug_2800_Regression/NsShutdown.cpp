//
// $Id: NsShutdown.cpp 979 2008-12-31 20:22:32Z mitza $
//
#include "NsShutdown.h"

ACE_RCSID(Hello,
          NsShutdown,
          "$Id: NsShutdown.cpp 979 2008-12-31 20:22:32Z mitza $")

NsShutdown::NsShutdown (CORBA::ORB_ptr orb)
  : orb_ (CORBA::ORB::_duplicate (orb))
{
}

void
NsShutdown::shutdown (void)
{
  this->orb_->shutdown (0);
}
