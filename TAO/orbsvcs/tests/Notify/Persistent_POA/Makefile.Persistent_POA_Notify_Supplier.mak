# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.Persistent_POA_Notify_Supplier.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Console Application")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Console Application")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = ".\goC.inl" ".\goS.inl" ".\goC.h" ".\goS.h" ".\goC.cpp" ".\goS.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INSTALLDIR=.
INTDIR=Debug\Persistent_POA_Notify_Supplier\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\Structured_Supplier.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -I"..\..\..\..\orbsvcs" -I"..\lib" -I"..\..\lib" -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_HAS_TYPED_EVENT_CHANNEL -f "Makefile.Persistent_POA_Notify_Supplier.dep" "goC.cpp" "goS.cpp" "Structured_Supplier.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\Structured_Supplier.pdb"
	-@del /f/q "$(INSTALLDIR)\Structured_Supplier.exe"
	-@del /f/q "$(INSTALLDIR)\Structured_Supplier.ilk"
	-@del /f/q ".\goC.inl"
	-@del /f/q ".\goS.inl"
	-@del /f/q ".\goC.h"
	-@del /f/q ".\goS.h"
	-@del /f/q ".\goC.cpp"
	-@del /f/q ".\goS.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\Persistent_POA_Notify_Supplier\$(NULL)" mkdir "Debug\Persistent_POA_Notify_Supplier"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\..\..\.." /I "..\..\..\.." /I "..\..\..\..\orbsvcs" /I "..\lib" /I "..\..\lib" /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_HAS_TYPED_EVENT_CHANNEL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_CosEventd.lib TAO_CosNotificationd.lib TAO_PortableServerd.lib TAO_CosEvent_Skeld.lib TAO_CosNotification_Skeld.lib TAO_Svc_Utilsd.lib ACE_ETCLd.lib TAO_ETCLd.lib ACE_ETCL_Parserd.lib TAO_Valuetyped.lib TAO_DynamicAnyd.lib TAO_CosNotification_Servd.lib TAO_CosNamingd.lib TAO_CodecFactoryd.lib TAO_PId.lib TAO_Messagingd.lib TAO_DynamicInterfaced.lib TAO_IFR_Clientd.lib TAO_CosEvent_Servd.lib TAO_NotifyTestsd.lib /libpath:"." /libpath:"..\..\..\..\..\lib" /libpath:"..\lib" /libpath:"..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\Structured_Supplier.pdb" /machine:IA64 /out:"$(INSTALLDIR)\Structured_Supplier.exe"
LINK32_OBJS= \
	"$(INTDIR)\goC.obj" \
	"$(INTDIR)\goS.obj" \
	"$(INTDIR)\Structured_Supplier.obj"

"$(INSTALLDIR)\Structured_Supplier.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\Structured_Supplier.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\Structured_Supplier.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=Release
INSTALLDIR=Release
INTDIR=Release\Persistent_POA_Notify_Supplier\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\Structured_Supplier.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -I"..\..\..\..\orbsvcs" -I"..\lib" -I"..\..\lib" -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_HAS_TYPED_EVENT_CHANNEL -f "Makefile.Persistent_POA_Notify_Supplier.dep" "goC.cpp" "goS.cpp" "Structured_Supplier.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\Structured_Supplier.exe"
	-@del /f/q "$(INSTALLDIR)\Structured_Supplier.ilk"
	-@del /f/q ".\goC.inl"
	-@del /f/q ".\goS.inl"
	-@del /f/q ".\goC.h"
	-@del /f/q ".\goS.h"
	-@del /f/q ".\goC.cpp"
	-@del /f/q ".\goS.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\Persistent_POA_Notify_Supplier\$(NULL)" mkdir "Release\Persistent_POA_Notify_Supplier"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\..\.." /I "..\..\..\.." /I "..\..\..\..\orbsvcs" /I "..\lib" /I "..\..\lib" /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_HAS_TYPED_EVENT_CHANNEL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_CosEvent.lib TAO_CosNotification.lib TAO_PortableServer.lib TAO_CosEvent_Skel.lib TAO_CosNotification_Skel.lib TAO_Svc_Utils.lib ACE_ETCL.lib TAO_ETCL.lib ACE_ETCL_Parser.lib TAO_Valuetype.lib TAO_DynamicAny.lib TAO_CosNotification_Serv.lib TAO_CosNaming.lib TAO_CodecFactory.lib TAO_PI.lib TAO_Messaging.lib TAO_DynamicInterface.lib TAO_IFR_Client.lib TAO_CosEvent_Serv.lib TAO_NotifyTests.lib /libpath:"." /libpath:"..\..\..\..\..\lib" /libpath:"..\lib" /libpath:"..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\Structured_Supplier.exe"
LINK32_OBJS= \
	"$(INTDIR)\goC.obj" \
	"$(INTDIR)\goS.obj" \
	"$(INTDIR)\Structured_Supplier.obj"

"$(INSTALLDIR)\Structured_Supplier.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\Structured_Supplier.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\Structured_Supplier.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=Static_Debug
INSTALLDIR=Static_Debug
INTDIR=Static_Debug\Persistent_POA_Notify_Supplier\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\Structured_Supplier.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -I"..\..\..\..\orbsvcs" -I"..\lib" -I"..\..\lib" -D_DEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_HAS_TYPED_EVENT_CHANNEL -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Persistent_POA_Notify_Supplier.dep" "goC.cpp" "goS.cpp" "Structured_Supplier.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\Structured_Supplier.pdb"
	-@del /f/q "$(INSTALLDIR)\Structured_Supplier.exe"
	-@del /f/q "$(INSTALLDIR)\Structured_Supplier.ilk"
	-@del /f/q ".\goC.inl"
	-@del /f/q ".\goS.inl"
	-@del /f/q ".\goC.h"
	-@del /f/q ".\goS.h"
	-@del /f/q ".\goC.cpp"
	-@del /f/q ".\goS.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\Persistent_POA_Notify_Supplier\$(NULL)" mkdir "Static_Debug\Persistent_POA_Notify_Supplier"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\..\..\.." /I "..\..\..\.." /I "..\..\..\..\orbsvcs" /I "..\lib" /I "..\..\lib" /D _DEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_HAS_TYPED_EVENT_CHANNEL /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEsd.lib TAOsd.lib TAO_AnyTypeCodesd.lib TAO_CosEventsd.lib TAO_CosNotificationsd.lib TAO_PortableServersd.lib TAO_CosEvent_Skelsd.lib TAO_CosNotification_Skelsd.lib TAO_Svc_Utilssd.lib ACE_ETCLsd.lib TAO_ETCLsd.lib ACE_ETCL_Parsersd.lib TAO_Valuetypesd.lib TAO_DynamicAnysd.lib TAO_CosNotification_Servsd.lib TAO_CosNamingsd.lib TAO_CodecFactorysd.lib TAO_PIsd.lib TAO_Messagingsd.lib TAO_DynamicInterfacesd.lib TAO_IFR_Clientsd.lib TAO_CosEvent_Servsd.lib TAO_NotifyTestssd.lib /libpath:"." /libpath:"..\..\..\..\..\lib" /libpath:"..\lib" /libpath:"..\..\lib" /nologo /subsystem:console /debug /pdb:"$(INSTALLDIR)\Structured_Supplier.pdb" /machine:IA64 /out:"$(INSTALLDIR)\Structured_Supplier.exe"
LINK32_OBJS= \
	"$(INTDIR)\goC.obj" \
	"$(INTDIR)\goS.obj" \
	"$(INTDIR)\Structured_Supplier.obj"

"$(INSTALLDIR)\Structured_Supplier.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\Structured_Supplier.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\Structured_Supplier.exe.manifest" -outputresource:$@;1

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=Static_Release
INSTALLDIR=Static_Release
INTDIR=Static_Release\Persistent_POA_Notify_Supplier\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(INSTALLDIR)\Structured_Supplier.exe"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -I"..\..\..\..\orbsvcs" -I"..\lib" -I"..\..\lib" -DNDEBUG -DWIN64 -DWIN32 -D_CONSOLE -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_HAS_TYPED_EVENT_CHANNEL -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.Persistent_POA_Notify_Supplier.dep" "goC.cpp" "goS.cpp" "Structured_Supplier.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(INSTALLDIR)\Structured_Supplier.exe"
	-@del /f/q "$(INSTALLDIR)\Structured_Supplier.ilk"
	-@del /f/q ".\goC.inl"
	-@del /f/q ".\goS.inl"
	-@del /f/q ".\goC.h"
	-@del /f/q ".\goS.h"
	-@del /f/q ".\goC.cpp"
	-@del /f/q ".\goS.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\Persistent_POA_Notify_Supplier\$(NULL)" mkdir "Static_Release\Persistent_POA_Notify_Supplier"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\..\.." /I "..\..\..\.." /I "..\..\..\..\orbsvcs" /I "..\lib" /I "..\..\lib" /D NDEBUG /D WIN64 /D WIN32 /D _CONSOLE /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_HAS_TYPED_EVENT_CHANNEL /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEs.lib TAOs.lib TAO_AnyTypeCodes.lib TAO_CosEvents.lib TAO_CosNotifications.lib TAO_PortableServers.lib TAO_CosEvent_Skels.lib TAO_CosNotification_Skels.lib TAO_Svc_Utilss.lib ACE_ETCLs.lib TAO_ETCLs.lib ACE_ETCL_Parsers.lib TAO_Valuetypes.lib TAO_DynamicAnys.lib TAO_CosNotification_Servs.lib TAO_CosNamings.lib TAO_CodecFactorys.lib TAO_PIs.lib TAO_Messagings.lib TAO_DynamicInterfaces.lib TAO_IFR_Clients.lib TAO_CosEvent_Servs.lib TAO_NotifyTestss.lib /libpath:"." /libpath:"..\..\..\..\..\lib" /libpath:"..\lib" /libpath:"..\..\lib" /nologo /subsystem:console  /machine:IA64 /out:"$(INSTALLDIR)\Structured_Supplier.exe"
LINK32_OBJS= \
	"$(INTDIR)\goC.obj" \
	"$(INTDIR)\goS.obj" \
	"$(INTDIR)\Structured_Supplier.obj"

"$(INSTALLDIR)\Structured_Supplier.exe" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(INSTALLDIR)\Structured_Supplier.exe.manifest" mt.exe -manifest "$(INSTALLDIR)\Structured_Supplier.exe.manifest" -outputresource:$@;1

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Persistent_POA_Notify_Supplier.dep")
!INCLUDE "Makefile.Persistent_POA_Notify_Supplier.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="goC.cpp"

"$(INTDIR)\goC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\goC.obj" $(SOURCE)

SOURCE="goS.cpp"

"$(INTDIR)\goS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\goS.obj" $(SOURCE)

SOURCE="Structured_Supplier.cpp"

"$(INTDIR)\Structured_Supplier.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Structured_Supplier.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="..\Blocking\go.idl"

InputPath=..\Blocking\go.idl

".\goC.inl" ".\goS.inl" ".\goC.h" ".\goS.h" ".\goC.cpp" ".\goS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-___Blocking_go_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
        if not exist . mkdir .
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. -I..\..\..\../orbsvcs -o . "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="..\Blocking\go.idl"

InputPath=..\Blocking\go.idl

".\goC.inl" ".\goS.inl" ".\goC.h" ".\goS.h" ".\goC.cpp" ".\goS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-___Blocking_go_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
        if not exist . mkdir .
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. -I..\..\..\../orbsvcs -o . "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="..\Blocking\go.idl"

InputPath=..\Blocking\go.idl

".\goC.inl" ".\goS.inl" ".\goC.h" ".\goS.h" ".\goC.cpp" ".\goS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-___Blocking_go_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
        if not exist . mkdir .
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. -I..\..\..\../orbsvcs -o . "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="..\Blocking\go.idl"

InputPath=..\Blocking\go.idl

".\goC.inl" ".\goS.inl" ".\goC.h" ".\goS.h" ".\goC.cpp" ".\goS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-___Blocking_go_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
        if not exist . mkdir .
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. -I..\..\..\../orbsvcs -o . "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Persistent_POA_Notify_Supplier.dep")
	@echo Using "Makefile.Persistent_POA_Notify_Supplier.dep"
!ELSE
	@echo Warning: cannot find "Makefile.Persistent_POA_Notify_Supplier.dep"
!ENDIF
!ENDIF

