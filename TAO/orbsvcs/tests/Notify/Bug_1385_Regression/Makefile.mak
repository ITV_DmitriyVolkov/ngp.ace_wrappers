#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake -value_template platforms=Win64 -make_coexistence -recurse -exclude TAO/CIAO -features qos=1,mfc=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: Bug_1385_Regression_Idl Bug_1385_Regression_Ntf_Struct_Cons Bug_1385_Regression_Ntf_Struct_Supp

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.Bug_1385_Regression_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Bug_1385_Regression_Idl.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Bug_1385_Regression_Ntf_Struct_Cons.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Bug_1385_Regression_Ntf_Struct_Cons.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.Bug_1385_Regression_Ntf_Struct_Supp.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Bug_1385_Regression_Ntf_Struct_Supp.mak CFG="$(CFG)" $(@)

Bug_1385_Regression_Idl:
	@echo Project: Makefile.Bug_1385_Regression_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Bug_1385_Regression_Idl.mak CFG="$(CFG)" all

Bug_1385_Regression_Ntf_Struct_Cons: Bug_1385_Regression_Idl
	@echo Project: Makefile.Bug_1385_Regression_Ntf_Struct_Cons.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Bug_1385_Regression_Ntf_Struct_Cons.mak CFG="$(CFG)" all

Bug_1385_Regression_Ntf_Struct_Supp: Bug_1385_Regression_Idl
	@echo Project: Makefile.Bug_1385_Regression_Ntf_Struct_Supp.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Bug_1385_Regression_Ntf_Struct_Supp.mak CFG="$(CFG)" all

project_name_list:
	@echo Bug_1385_Regression_Idl
	@echo Bug_1385_Regression_Ntf_Struct_Cons
	@echo Bug_1385_Regression_Ntf_Struct_Supp
