/* -*- C++ -*- */
// $Id: Notify_StructuredPushSupplier.h 14 2007-02-01 15:49:12Z mitza $
// ==========================================================================
//
// = LIBRARY
//   orbsvcs
//
// = FILENAME
//   Notify_StructuredPushSupplier
//
// = DESCRIPTION
//   This class is to be used by clients of the Notification Service
//   to implement Structured Push Suppliers.
//
// = AUTHOR
//    Pradeep Gore <pradeep@cs.wustl.edu>
//
// ==========================================================================

#ifndef TAO_NOTIFY_STRUCTUREDPUSHSUPPLIER_H
#define TAO_NOTIFY_STRUCTUREDPUSHSUPPLIER_H
#include /**/ "ace/pre.h"

#include "StructuredPushSupplier.h"

typedef TAO_Notify_Tests_StructuredPushSupplier TAO_Notify_StructuredPushSupplier;

#include /**/ "ace/post.h"
#endif /* TAO_NOTIFY_STRUCTUREDPUSHSUPPLIER_H */
