/* -*- C++ -*- */
// $Id: Notify_SequencePushConsumer.h 14 2007-02-01 15:49:12Z mitza $
// ==========================================================================
//
// = LIBRARY
//   orbsvcs
//
// = FILENAME
//   Notify_SequencePushConsumer.h
//
// = DESCRIPTION
//   This is a utility class is to be used by clients of the Notification
//   Service to implement the servant for CosNotifyComm::SequencePushConsumer
//
// = AUTHOR
//    Pradeep Gore <pradeep@cs.wustl.edu>
//
// ==========================================================================

#ifndef NOTIFY_SEQUENCEPUSHCONSUMER_H
#define NOTIFY_SEQUENCEPUSHCONSUMER_H
#include /**/ "ace/pre.h"

#include "SequencePushConsumer.h"

typedef TAO_Notify_Tests_SequencePushConsumer TAO_Notify_SequencePushConsumer;

#include /**/ "ace/post.h"

#endif /* NOTIFY_SEQUENCEPUSHCONSUMER_H */
