// $Id: Notify_PushSupplier.h 14 2007-02-01 15:49:12Z mitza $

#ifndef TAO_NOTIFY_PUSHSUPPLIER_H
#define TAO_NOTIFY_PUSHSUPPLIER_H
#include /**/ "ace/pre.h"

#include "PushSupplier.h"

typedef TAO_Notify_Tests_PushSupplier TAO_Notify_PushSupplier;

#include /**/ "ace/post.h"
#endif /* TAO_NOTIFY_PUSHSUPPLIER_H */
