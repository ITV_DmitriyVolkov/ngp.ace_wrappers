/* -*- C++ -*- */
// $Id: Notify_SequencePushSupplier.h 14 2007-02-01 15:49:12Z mitza $
// ==========================================================================
//
// = LIBRARY
//   orbsvcs
//
// = FILENAME
//   Notify_SequencePushSupplier
//
// = DESCRIPTION
//   This class is to be used by clients of the Notification Service
//   to implement Sequence Push Suppliers.
//
// = AUTHOR
//    Pradeep Gore <pradeep@cs.wustl.edu>
//
// ==========================================================================

#ifndef TAO_NOTIFY_SEQUENCEPUSHSUPPLIER_H
#define TAO_NOTIFY_SEQUENCEPUSHSUPPLIER_H
#include /**/ "ace/pre.h"

#include "SequencePushSupplier.h"

typedef TAO_Notify_Tests_SequencePushSupplier TAO_Notify_SequencePushSupplier;

#include /**/ "ace/post.h"
#endif /* TAO_NOTIFY_SequencePUSHSUPPLIER_H */
