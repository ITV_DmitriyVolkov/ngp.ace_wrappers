# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.NotifyTests_Lib.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "Activation_ManagerC.inl" "Activation_ManagerS.inl" "Activation_ManagerC.h" "Activation_ManagerS.h" "Activation_ManagerC.cpp" "Activation_ManagerS.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INTDIR=Debug\NotifyTests_Lib\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) ".\TAO_NotifyTestsd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -I"..\..\..\..\orbsvcs" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_HAS_TYPED_EVENT_CHANNEL -DTAO_NOTIFY_TEST_BUILD_DLL -f "Makefile.NotifyTests_Lib.dep" "Activation_ManagerC.cpp" "Activation_ManagerS.cpp" "Task_Stats.cpp" "ConsumerAdmin_Command.cpp" "Periodic_Consumer_Command.cpp" "Name.cpp" "LookupManager.cpp" "Direct_Consumer.cpp" "Application_Command.cpp" "Periodic_Supplier_Command.cpp" "Priority_Mapping.cpp" "Command.cpp" "StructuredPushConsumer.cpp" "Notify_Test_Client.cpp" "SupplierAdmin_Command.cpp" "PushSupplier.cpp" "Relay_Consumer.cpp" "StructuredPushSupplier.cpp" "Options_Parser.cpp" "Peer.cpp" "SequencePushConsumer.cpp" "StructuredEvent.cpp" "SequencePushSupplier.cpp" "Periodic_Supplier.cpp" "Command_Factory.cpp" "Driver.cpp" "PushConsumer.cpp" "EventChannel_Command.cpp" "Factories_Define.cpp" "common.cpp" "Activation_Manager.cpp" "Task_Callback.cpp" "Direct_Supplier.cpp" "Periodic_Consumer.cpp" "Filter_Command.cpp" "Command_Builder.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_NotifyTestsd.pdb"
	-@del /f/q ".\TAO_NotifyTestsd.dll"
	-@del /f/q "$(OUTDIR)\TAO_NotifyTestsd.lib"
	-@del /f/q "$(OUTDIR)\TAO_NotifyTestsd.exp"
	-@del /f/q "$(OUTDIR)\TAO_NotifyTestsd.ilk"
	-@del /f/q "Activation_ManagerC.inl"
	-@del /f/q "Activation_ManagerS.inl"
	-@del /f/q "Activation_ManagerC.h"
	-@del /f/q "Activation_ManagerS.h"
	-@del /f/q "Activation_ManagerC.cpp"
	-@del /f/q "Activation_ManagerS.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\NotifyTests_Lib\$(NULL)" mkdir "Debug\NotifyTests_Lib"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\..\..\.." /I "..\..\..\.." /I "..\..\..\..\orbsvcs" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_HAS_TYPED_EVENT_CHANNEL /D TAO_NOTIFY_TEST_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_CosEventd.lib TAO_CosNotificationd.lib TAO_PortableServerd.lib TAO_CosEvent_Skeld.lib TAO_CosNotification_Skeld.lib TAO_Valuetyped.lib TAO_CodecFactoryd.lib TAO_PId.lib TAO_Messagingd.lib TAO_DynamicInterfaced.lib TAO_IFR_Clientd.lib TAO_CosNamingd.lib TAO_Svc_Utilsd.lib TAO_CosEvent_Servd.lib ACE_ETCLd.lib TAO_ETCLd.lib ACE_ETCL_Parserd.lib TAO_DynamicAnyd.lib TAO_CosNotification_Servd.lib /libpath:"." /libpath:"..\..\..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:".\TAO_NotifyTestsd.pdb" /machine:IA64 /out:".\TAO_NotifyTestsd.dll" /implib:"$(OUTDIR)\TAO_NotifyTestsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Activation_ManagerC.obj" \
	"$(INTDIR)\Activation_ManagerS.obj" \
	"$(INTDIR)\Task_Stats.obj" \
	"$(INTDIR)\ConsumerAdmin_Command.obj" \
	"$(INTDIR)\Periodic_Consumer_Command.obj" \
	"$(INTDIR)\Name.obj" \
	"$(INTDIR)\LookupManager.obj" \
	"$(INTDIR)\Direct_Consumer.obj" \
	"$(INTDIR)\Application_Command.obj" \
	"$(INTDIR)\Periodic_Supplier_Command.obj" \
	"$(INTDIR)\Priority_Mapping.obj" \
	"$(INTDIR)\Command.obj" \
	"$(INTDIR)\StructuredPushConsumer.obj" \
	"$(INTDIR)\Notify_Test_Client.obj" \
	"$(INTDIR)\SupplierAdmin_Command.obj" \
	"$(INTDIR)\PushSupplier.obj" \
	"$(INTDIR)\Relay_Consumer.obj" \
	"$(INTDIR)\StructuredPushSupplier.obj" \
	"$(INTDIR)\Options_Parser.obj" \
	"$(INTDIR)\Peer.obj" \
	"$(INTDIR)\SequencePushConsumer.obj" \
	"$(INTDIR)\StructuredEvent.obj" \
	"$(INTDIR)\SequencePushSupplier.obj" \
	"$(INTDIR)\Periodic_Supplier.obj" \
	"$(INTDIR)\Command_Factory.obj" \
	"$(INTDIR)\Driver.obj" \
	"$(INTDIR)\PushConsumer.obj" \
	"$(INTDIR)\EventChannel_Command.obj" \
	"$(INTDIR)\Factories_Define.obj" \
	"$(INTDIR)\common.obj" \
	"$(INTDIR)\Activation_Manager.obj" \
	"$(INTDIR)\Task_Callback.obj" \
	"$(INTDIR)\Direct_Supplier.obj" \
	"$(INTDIR)\Periodic_Consumer.obj" \
	"$(INTDIR)\Filter_Command.obj" \
	"$(INTDIR)\Command_Builder.obj"

".\TAO_NotifyTestsd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist ".\TAO_NotifyTestsd.dll.manifest" mt.exe -manifest ".\TAO_NotifyTestsd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=.
INTDIR=Release\NotifyTests_Lib\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) ".\TAO_NotifyTests.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -I"..\..\..\..\orbsvcs" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_HAS_TYPED_EVENT_CHANNEL -DTAO_NOTIFY_TEST_BUILD_DLL -f "Makefile.NotifyTests_Lib.dep" "Activation_ManagerC.cpp" "Activation_ManagerS.cpp" "Task_Stats.cpp" "ConsumerAdmin_Command.cpp" "Periodic_Consumer_Command.cpp" "Name.cpp" "LookupManager.cpp" "Direct_Consumer.cpp" "Application_Command.cpp" "Periodic_Supplier_Command.cpp" "Priority_Mapping.cpp" "Command.cpp" "StructuredPushConsumer.cpp" "Notify_Test_Client.cpp" "SupplierAdmin_Command.cpp" "PushSupplier.cpp" "Relay_Consumer.cpp" "StructuredPushSupplier.cpp" "Options_Parser.cpp" "Peer.cpp" "SequencePushConsumer.cpp" "StructuredEvent.cpp" "SequencePushSupplier.cpp" "Periodic_Supplier.cpp" "Command_Factory.cpp" "Driver.cpp" "PushConsumer.cpp" "EventChannel_Command.cpp" "Factories_Define.cpp" "common.cpp" "Activation_Manager.cpp" "Task_Callback.cpp" "Direct_Supplier.cpp" "Periodic_Consumer.cpp" "Filter_Command.cpp" "Command_Builder.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q ".\TAO_NotifyTests.dll"
	-@del /f/q "$(OUTDIR)\TAO_NotifyTests.lib"
	-@del /f/q "$(OUTDIR)\TAO_NotifyTests.exp"
	-@del /f/q "$(OUTDIR)\TAO_NotifyTests.ilk"
	-@del /f/q "Activation_ManagerC.inl"
	-@del /f/q "Activation_ManagerS.inl"
	-@del /f/q "Activation_ManagerC.h"
	-@del /f/q "Activation_ManagerS.h"
	-@del /f/q "Activation_ManagerC.cpp"
	-@del /f/q "Activation_ManagerS.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\NotifyTests_Lib\$(NULL)" mkdir "Release\NotifyTests_Lib"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\..\.." /I "..\..\..\.." /I "..\..\..\..\orbsvcs" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_HAS_TYPED_EVENT_CHANNEL /D TAO_NOTIFY_TEST_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_CosEvent.lib TAO_CosNotification.lib TAO_PortableServer.lib TAO_CosEvent_Skel.lib TAO_CosNotification_Skel.lib TAO_Valuetype.lib TAO_CodecFactory.lib TAO_PI.lib TAO_Messaging.lib TAO_DynamicInterface.lib TAO_IFR_Client.lib TAO_CosNaming.lib TAO_Svc_Utils.lib TAO_CosEvent_Serv.lib ACE_ETCL.lib TAO_ETCL.lib ACE_ETCL_Parser.lib TAO_DynamicAny.lib TAO_CosNotification_Serv.lib /libpath:"." /libpath:"..\..\..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:".\TAO_NotifyTests.dll" /implib:"$(OUTDIR)\TAO_NotifyTests.lib"
LINK32_OBJS= \
	"$(INTDIR)\Activation_ManagerC.obj" \
	"$(INTDIR)\Activation_ManagerS.obj" \
	"$(INTDIR)\Task_Stats.obj" \
	"$(INTDIR)\ConsumerAdmin_Command.obj" \
	"$(INTDIR)\Periodic_Consumer_Command.obj" \
	"$(INTDIR)\Name.obj" \
	"$(INTDIR)\LookupManager.obj" \
	"$(INTDIR)\Direct_Consumer.obj" \
	"$(INTDIR)\Application_Command.obj" \
	"$(INTDIR)\Periodic_Supplier_Command.obj" \
	"$(INTDIR)\Priority_Mapping.obj" \
	"$(INTDIR)\Command.obj" \
	"$(INTDIR)\StructuredPushConsumer.obj" \
	"$(INTDIR)\Notify_Test_Client.obj" \
	"$(INTDIR)\SupplierAdmin_Command.obj" \
	"$(INTDIR)\PushSupplier.obj" \
	"$(INTDIR)\Relay_Consumer.obj" \
	"$(INTDIR)\StructuredPushSupplier.obj" \
	"$(INTDIR)\Options_Parser.obj" \
	"$(INTDIR)\Peer.obj" \
	"$(INTDIR)\SequencePushConsumer.obj" \
	"$(INTDIR)\StructuredEvent.obj" \
	"$(INTDIR)\SequencePushSupplier.obj" \
	"$(INTDIR)\Periodic_Supplier.obj" \
	"$(INTDIR)\Command_Factory.obj" \
	"$(INTDIR)\Driver.obj" \
	"$(INTDIR)\PushConsumer.obj" \
	"$(INTDIR)\EventChannel_Command.obj" \
	"$(INTDIR)\Factories_Define.obj" \
	"$(INTDIR)\common.obj" \
	"$(INTDIR)\Activation_Manager.obj" \
	"$(INTDIR)\Task_Callback.obj" \
	"$(INTDIR)\Direct_Supplier.obj" \
	"$(INTDIR)\Periodic_Consumer.obj" \
	"$(INTDIR)\Filter_Command.obj" \
	"$(INTDIR)\Command_Builder.obj"

".\TAO_NotifyTests.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist ".\TAO_NotifyTests.dll.manifest" mt.exe -manifest ".\TAO_NotifyTests.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=.
INTDIR=Static_Debug\NotifyTests_Lib\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_NotifyTestssd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -I"..\..\..\..\orbsvcs" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_HAS_TYPED_EVENT_CHANNEL -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.NotifyTests_Lib.dep" "Activation_ManagerC.cpp" "Activation_ManagerS.cpp" "Task_Stats.cpp" "ConsumerAdmin_Command.cpp" "Periodic_Consumer_Command.cpp" "Name.cpp" "LookupManager.cpp" "Direct_Consumer.cpp" "Application_Command.cpp" "Periodic_Supplier_Command.cpp" "Priority_Mapping.cpp" "Command.cpp" "StructuredPushConsumer.cpp" "Notify_Test_Client.cpp" "SupplierAdmin_Command.cpp" "PushSupplier.cpp" "Relay_Consumer.cpp" "StructuredPushSupplier.cpp" "Options_Parser.cpp" "Peer.cpp" "SequencePushConsumer.cpp" "StructuredEvent.cpp" "SequencePushSupplier.cpp" "Periodic_Supplier.cpp" "Command_Factory.cpp" "Driver.cpp" "PushConsumer.cpp" "EventChannel_Command.cpp" "Factories_Define.cpp" "common.cpp" "Activation_Manager.cpp" "Task_Callback.cpp" "Direct_Supplier.cpp" "Periodic_Consumer.cpp" "Filter_Command.cpp" "Command_Builder.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_NotifyTestssd.lib"
	-@del /f/q "$(OUTDIR)\TAO_NotifyTestssd.exp"
	-@del /f/q "$(OUTDIR)\TAO_NotifyTestssd.ilk"
	-@del /f/q ".\TAO_NotifyTestssd.pdb"
	-@del /f/q "Activation_ManagerC.inl"
	-@del /f/q "Activation_ManagerS.inl"
	-@del /f/q "Activation_ManagerC.h"
	-@del /f/q "Activation_ManagerS.h"
	-@del /f/q "Activation_ManagerC.cpp"
	-@del /f/q "Activation_ManagerS.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\NotifyTests_Lib\$(NULL)" mkdir "Static_Debug\NotifyTests_Lib"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd".\TAO_NotifyTestssd.pdb" /I "..\..\..\..\.." /I "..\..\..\.." /I "..\..\..\..\orbsvcs" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_HAS_TYPED_EVENT_CHANNEL /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:".\TAO_NotifyTestssd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Activation_ManagerC.obj" \
	"$(INTDIR)\Activation_ManagerS.obj" \
	"$(INTDIR)\Task_Stats.obj" \
	"$(INTDIR)\ConsumerAdmin_Command.obj" \
	"$(INTDIR)\Periodic_Consumer_Command.obj" \
	"$(INTDIR)\Name.obj" \
	"$(INTDIR)\LookupManager.obj" \
	"$(INTDIR)\Direct_Consumer.obj" \
	"$(INTDIR)\Application_Command.obj" \
	"$(INTDIR)\Periodic_Supplier_Command.obj" \
	"$(INTDIR)\Priority_Mapping.obj" \
	"$(INTDIR)\Command.obj" \
	"$(INTDIR)\StructuredPushConsumer.obj" \
	"$(INTDIR)\Notify_Test_Client.obj" \
	"$(INTDIR)\SupplierAdmin_Command.obj" \
	"$(INTDIR)\PushSupplier.obj" \
	"$(INTDIR)\Relay_Consumer.obj" \
	"$(INTDIR)\StructuredPushSupplier.obj" \
	"$(INTDIR)\Options_Parser.obj" \
	"$(INTDIR)\Peer.obj" \
	"$(INTDIR)\SequencePushConsumer.obj" \
	"$(INTDIR)\StructuredEvent.obj" \
	"$(INTDIR)\SequencePushSupplier.obj" \
	"$(INTDIR)\Periodic_Supplier.obj" \
	"$(INTDIR)\Command_Factory.obj" \
	"$(INTDIR)\Driver.obj" \
	"$(INTDIR)\PushConsumer.obj" \
	"$(INTDIR)\EventChannel_Command.obj" \
	"$(INTDIR)\Factories_Define.obj" \
	"$(INTDIR)\common.obj" \
	"$(INTDIR)\Activation_Manager.obj" \
	"$(INTDIR)\Task_Callback.obj" \
	"$(INTDIR)\Direct_Supplier.obj" \
	"$(INTDIR)\Periodic_Consumer.obj" \
	"$(INTDIR)\Filter_Command.obj" \
	"$(INTDIR)\Command_Builder.obj"

"$(OUTDIR)\TAO_NotifyTestssd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_NotifyTestssd.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_NotifyTestssd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=.
INTDIR=Static_Release\NotifyTests_Lib\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\TAO_NotifyTestss.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\.." -I"..\..\..\.." -I"..\..\..\..\orbsvcs" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_HAS_TYPED_EVENT_CHANNEL -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.NotifyTests_Lib.dep" "Activation_ManagerC.cpp" "Activation_ManagerS.cpp" "Task_Stats.cpp" "ConsumerAdmin_Command.cpp" "Periodic_Consumer_Command.cpp" "Name.cpp" "LookupManager.cpp" "Direct_Consumer.cpp" "Application_Command.cpp" "Periodic_Supplier_Command.cpp" "Priority_Mapping.cpp" "Command.cpp" "StructuredPushConsumer.cpp" "Notify_Test_Client.cpp" "SupplierAdmin_Command.cpp" "PushSupplier.cpp" "Relay_Consumer.cpp" "StructuredPushSupplier.cpp" "Options_Parser.cpp" "Peer.cpp" "SequencePushConsumer.cpp" "StructuredEvent.cpp" "SequencePushSupplier.cpp" "Periodic_Supplier.cpp" "Command_Factory.cpp" "Driver.cpp" "PushConsumer.cpp" "EventChannel_Command.cpp" "Factories_Define.cpp" "common.cpp" "Activation_Manager.cpp" "Task_Callback.cpp" "Direct_Supplier.cpp" "Periodic_Consumer.cpp" "Filter_Command.cpp" "Command_Builder.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\TAO_NotifyTestss.lib"
	-@del /f/q "$(OUTDIR)\TAO_NotifyTestss.exp"
	-@del /f/q "$(OUTDIR)\TAO_NotifyTestss.ilk"
	-@del /f/q "Activation_ManagerC.inl"
	-@del /f/q "Activation_ManagerS.inl"
	-@del /f/q "Activation_ManagerC.h"
	-@del /f/q "Activation_ManagerS.h"
	-@del /f/q "Activation_ManagerC.cpp"
	-@del /f/q "Activation_ManagerS.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\NotifyTests_Lib\$(NULL)" mkdir "Static_Release\NotifyTests_Lib"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\..\.." /I "..\..\..\.." /I "..\..\..\..\orbsvcs" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_HAS_TYPED_EVENT_CHANNEL /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:".\TAO_NotifyTestss.lib"
LINK32_OBJS= \
	"$(INTDIR)\Activation_ManagerC.obj" \
	"$(INTDIR)\Activation_ManagerS.obj" \
	"$(INTDIR)\Task_Stats.obj" \
	"$(INTDIR)\ConsumerAdmin_Command.obj" \
	"$(INTDIR)\Periodic_Consumer_Command.obj" \
	"$(INTDIR)\Name.obj" \
	"$(INTDIR)\LookupManager.obj" \
	"$(INTDIR)\Direct_Consumer.obj" \
	"$(INTDIR)\Application_Command.obj" \
	"$(INTDIR)\Periodic_Supplier_Command.obj" \
	"$(INTDIR)\Priority_Mapping.obj" \
	"$(INTDIR)\Command.obj" \
	"$(INTDIR)\StructuredPushConsumer.obj" \
	"$(INTDIR)\Notify_Test_Client.obj" \
	"$(INTDIR)\SupplierAdmin_Command.obj" \
	"$(INTDIR)\PushSupplier.obj" \
	"$(INTDIR)\Relay_Consumer.obj" \
	"$(INTDIR)\StructuredPushSupplier.obj" \
	"$(INTDIR)\Options_Parser.obj" \
	"$(INTDIR)\Peer.obj" \
	"$(INTDIR)\SequencePushConsumer.obj" \
	"$(INTDIR)\StructuredEvent.obj" \
	"$(INTDIR)\SequencePushSupplier.obj" \
	"$(INTDIR)\Periodic_Supplier.obj" \
	"$(INTDIR)\Command_Factory.obj" \
	"$(INTDIR)\Driver.obj" \
	"$(INTDIR)\PushConsumer.obj" \
	"$(INTDIR)\EventChannel_Command.obj" \
	"$(INTDIR)\Factories_Define.obj" \
	"$(INTDIR)\common.obj" \
	"$(INTDIR)\Activation_Manager.obj" \
	"$(INTDIR)\Task_Callback.obj" \
	"$(INTDIR)\Direct_Supplier.obj" \
	"$(INTDIR)\Periodic_Consumer.obj" \
	"$(INTDIR)\Filter_Command.obj" \
	"$(INTDIR)\Command_Builder.obj"

"$(OUTDIR)\TAO_NotifyTestss.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\TAO_NotifyTestss.lib.manifest" mt.exe -manifest "$(OUTDIR)\TAO_NotifyTestss.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.NotifyTests_Lib.dep")
!INCLUDE "Makefile.NotifyTests_Lib.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Activation_ManagerC.cpp"

"$(INTDIR)\Activation_ManagerC.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Activation_ManagerC.obj" $(SOURCE)

SOURCE="Activation_ManagerS.cpp"

"$(INTDIR)\Activation_ManagerS.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Activation_ManagerS.obj" $(SOURCE)

SOURCE="Task_Stats.cpp"

"$(INTDIR)\Task_Stats.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Task_Stats.obj" $(SOURCE)

SOURCE="ConsumerAdmin_Command.cpp"

"$(INTDIR)\ConsumerAdmin_Command.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ConsumerAdmin_Command.obj" $(SOURCE)

SOURCE="Periodic_Consumer_Command.cpp"

"$(INTDIR)\Periodic_Consumer_Command.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Periodic_Consumer_Command.obj" $(SOURCE)

SOURCE="Name.cpp"

"$(INTDIR)\Name.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Name.obj" $(SOURCE)

SOURCE="LookupManager.cpp"

"$(INTDIR)\LookupManager.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\LookupManager.obj" $(SOURCE)

SOURCE="Direct_Consumer.cpp"

"$(INTDIR)\Direct_Consumer.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Direct_Consumer.obj" $(SOURCE)

SOURCE="Application_Command.cpp"

"$(INTDIR)\Application_Command.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Application_Command.obj" $(SOURCE)

SOURCE="Periodic_Supplier_Command.cpp"

"$(INTDIR)\Periodic_Supplier_Command.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Periodic_Supplier_Command.obj" $(SOURCE)

SOURCE="Priority_Mapping.cpp"

"$(INTDIR)\Priority_Mapping.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Priority_Mapping.obj" $(SOURCE)

SOURCE="Command.cpp"

"$(INTDIR)\Command.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Command.obj" $(SOURCE)

SOURCE="StructuredPushConsumer.cpp"

"$(INTDIR)\StructuredPushConsumer.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\StructuredPushConsumer.obj" $(SOURCE)

SOURCE="Notify_Test_Client.cpp"

"$(INTDIR)\Notify_Test_Client.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Notify_Test_Client.obj" $(SOURCE)

SOURCE="SupplierAdmin_Command.cpp"

"$(INTDIR)\SupplierAdmin_Command.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\SupplierAdmin_Command.obj" $(SOURCE)

SOURCE="PushSupplier.cpp"

"$(INTDIR)\PushSupplier.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PushSupplier.obj" $(SOURCE)

SOURCE="Relay_Consumer.cpp"

"$(INTDIR)\Relay_Consumer.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Relay_Consumer.obj" $(SOURCE)

SOURCE="StructuredPushSupplier.cpp"

"$(INTDIR)\StructuredPushSupplier.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\StructuredPushSupplier.obj" $(SOURCE)

SOURCE="Options_Parser.cpp"

"$(INTDIR)\Options_Parser.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Options_Parser.obj" $(SOURCE)

SOURCE="Peer.cpp"

"$(INTDIR)\Peer.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Peer.obj" $(SOURCE)

SOURCE="SequencePushConsumer.cpp"

"$(INTDIR)\SequencePushConsumer.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\SequencePushConsumer.obj" $(SOURCE)

SOURCE="StructuredEvent.cpp"

"$(INTDIR)\StructuredEvent.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\StructuredEvent.obj" $(SOURCE)

SOURCE="SequencePushSupplier.cpp"

"$(INTDIR)\SequencePushSupplier.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\SequencePushSupplier.obj" $(SOURCE)

SOURCE="Periodic_Supplier.cpp"

"$(INTDIR)\Periodic_Supplier.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Periodic_Supplier.obj" $(SOURCE)

SOURCE="Command_Factory.cpp"

"$(INTDIR)\Command_Factory.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Command_Factory.obj" $(SOURCE)

SOURCE="Driver.cpp"

"$(INTDIR)\Driver.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Driver.obj" $(SOURCE)

SOURCE="PushConsumer.cpp"

"$(INTDIR)\PushConsumer.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\PushConsumer.obj" $(SOURCE)

SOURCE="EventChannel_Command.cpp"

"$(INTDIR)\EventChannel_Command.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\EventChannel_Command.obj" $(SOURCE)

SOURCE="Factories_Define.cpp"

"$(INTDIR)\Factories_Define.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Factories_Define.obj" $(SOURCE)

SOURCE="common.cpp"

"$(INTDIR)\common.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\common.obj" $(SOURCE)

SOURCE="Activation_Manager.cpp"

"$(INTDIR)\Activation_Manager.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Activation_Manager.obj" $(SOURCE)

SOURCE="Task_Callback.cpp"

"$(INTDIR)\Task_Callback.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Task_Callback.obj" $(SOURCE)

SOURCE="Direct_Supplier.cpp"

"$(INTDIR)\Direct_Supplier.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Direct_Supplier.obj" $(SOURCE)

SOURCE="Periodic_Consumer.cpp"

"$(INTDIR)\Periodic_Consumer.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Periodic_Consumer.obj" $(SOURCE)

SOURCE="Filter_Command.cpp"

"$(INTDIR)\Filter_Command.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Filter_Command.obj" $(SOURCE)

SOURCE="Command_Builder.cpp"

"$(INTDIR)\Command_Builder.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Command_Builder.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
SOURCE="Activation_Manager.idl"

InputPath=Activation_Manager.idl

"Activation_ManagerC.inl" "Activation_ManagerS.inl" "Activation_ManagerC.h" "Activation_ManagerS.h" "Activation_ManagerC.cpp" "Activation_ManagerS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Activation_Manager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. -I..\..\..\../orbsvcs "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="Activation_Manager.idl"

InputPath=Activation_Manager.idl

"Activation_ManagerC.inl" "Activation_ManagerS.inl" "Activation_ManagerC.h" "Activation_ManagerS.h" "Activation_ManagerC.cpp" "Activation_ManagerS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe" "..\..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Activation_Manager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. -I..\..\..\../orbsvcs "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="Activation_Manager.idl"

InputPath=Activation_Manager.idl

"Activation_ManagerC.inl" "Activation_ManagerS.inl" "Activation_ManagerC.h" "Activation_ManagerS.h" "Activation_ManagerC.cpp" "Activation_ManagerS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Activation_Manager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. -I..\..\..\../orbsvcs "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="Activation_Manager.idl"

InputPath=Activation_Manager.idl

"Activation_ManagerC.inl" "Activation_ManagerS.inl" "Activation_ManagerC.h" "Activation_ManagerS.h" "Activation_ManagerC.cpp" "Activation_ManagerS.cpp" : $(SOURCE)  "..\..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Activation_Manager_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\..\lib
	..\..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\..\.. -I..\..\..\../orbsvcs "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.NotifyTests_Lib.dep")
	@echo Using "Makefile.NotifyTests_Lib.dep"
!ELSE
	@echo Warning: cannot find "Makefile.NotifyTests_Lib.dep"
!ENDIF
!ENDIF

