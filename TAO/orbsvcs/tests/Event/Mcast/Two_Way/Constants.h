// $Id: Constants.h 14 2007-02-01 15:49:12Z mitza $

#include "orbsvcs/Event_Service_Constants.h"

#define HEARTBEAT ACE_ES_EVENT_UNDEFINED+1
#define SOURCE_ID ACE_ES_EVENT_SOURCE_ANY+1
#define HEARTBEATS_TO_SEND 50
