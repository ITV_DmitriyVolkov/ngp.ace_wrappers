#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake -value_template platforms=Win64 -make_coexistence -recurse -exclude TAO/CIAO -features qos=1,mfc=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: Event_Test_Lib Event_Basic_Atomic_Reconnect Event_Basic_BCast Event_Basic_Bitmask Event_Basic_Complex Event_Basic_Control Event_Basic_Disconnect Event_Basic_Gateway Event_Basic_MT_Disconnect Event_Basic_Negation Event_Basic_Observer Event_Basic_Random Event_Basic_Reconnect Event_Basic_Shutdown Event_Basic_Timeout Event_Basic_Wildcard AddrServer_Client AddrServer_Server ECMcastTests_lib Complex_Consumer Complex_Gateway_Ec Complex_Supplier RTEC_MCast_Federated_Consumer RTEC_MCast_Federated_Supplier Simple_Consumer Simple_Gateway_Ec Simple_Supplier Two_Way_Application Two_Way_Gateway_Ec Event_Performance_Connect Event_Performance_Inversion Event_Performance_Latency Event_Performance_Latency_Server Event_Performance_Throughput TFTest RtEC_UDP_Idl RtEC_UDP_Receiver RtEC_UDP_Sender

clean depend generated realclean $(CUSTOM_TARGETS):
	@cd lib
	@echo Directory: lib
	@echo Project: Makefile.Event_Test_Lib.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Test_Lib.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Atomic_Reconnect.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Atomic_Reconnect.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_BCast.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_BCast.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Bitmask.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Bitmask.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Complex.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Complex.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Control.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Control.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Disconnect.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Disconnect.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Gateway.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Gateway.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_MT_Disconnect.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_MT_Disconnect.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Negation.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Negation.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Observer.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Observer.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Random.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Random.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Reconnect.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Reconnect.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Shutdown.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Shutdown.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Timeout.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Timeout.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Wildcard.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Wildcard.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Mcast/AddrServer
	@echo Directory: Mcast/AddrServer
	@echo Project: Makefile.AddrServer_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.AddrServer_Client.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Mcast/AddrServer
	@echo Directory: Mcast/AddrServer
	@echo Project: Makefile.AddrServer_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.AddrServer_Server.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Mcast/Common
	@echo Directory: Mcast/Common
	@echo Project: Makefile.ECMcastTests_lib.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.ECMcastTests_lib.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Mcast/Complex
	@echo Directory: Mcast/Complex
	@echo Project: Makefile.Complex_Consumer.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Complex_Consumer.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Mcast/Complex
	@echo Directory: Mcast/Complex
	@echo Project: Makefile.Complex_Gateway_Ec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Complex_Gateway_Ec.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Mcast/Complex
	@echo Directory: Mcast/Complex
	@echo Project: Makefile.Complex_Supplier.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Complex_Supplier.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Mcast/RTEC_MCast_Federated
	@echo Directory: Mcast/RTEC_MCast_Federated
	@echo Project: Makefile.RTEC_MCast_Federated_Consumer.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.RTEC_MCast_Federated_Consumer.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Mcast/RTEC_MCast_Federated
	@echo Directory: Mcast/RTEC_MCast_Federated
	@echo Project: Makefile.RTEC_MCast_Federated_Supplier.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.RTEC_MCast_Federated_Supplier.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Mcast/Simple
	@echo Directory: Mcast/Simple
	@echo Project: Makefile.Simple_Consumer.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Simple_Consumer.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Mcast/Simple
	@echo Directory: Mcast/Simple
	@echo Project: Makefile.Simple_Gateway_Ec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Simple_Gateway_Ec.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Mcast/Simple
	@echo Directory: Mcast/Simple
	@echo Project: Makefile.Simple_Supplier.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Simple_Supplier.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Mcast/Two_Way
	@echo Directory: Mcast/Two_Way
	@echo Project: Makefile.Two_Way_Application.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Two_Way_Application.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Mcast/Two_Way
	@echo Directory: Mcast/Two_Way
	@echo Project: Makefile.Two_Way_Gateway_Ec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Two_Way_Gateway_Ec.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Performance
	@echo Directory: Performance
	@echo Project: Makefile.Event_Performance_Connect.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Performance_Connect.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Performance
	@echo Directory: Performance
	@echo Project: Makefile.Event_Performance_Inversion.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Performance_Inversion.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Performance
	@echo Directory: Performance
	@echo Project: Makefile.Event_Performance_Latency.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Performance_Latency.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Performance
	@echo Directory: Performance
	@echo Project: Makefile.Event_Performance_Latency_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Performance_Latency_Server.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd Performance
	@echo Directory: Performance
	@echo Project: Makefile.Event_Performance_Throughput.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Performance_Throughput.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd ThreadFlags
	@echo Directory: ThreadFlags
	@echo Project: Makefile.TFTest.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.TFTest.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd UDP
	@echo Directory: UDP
	@echo Project: Makefile.RtEC_UDP_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.RtEC_UDP_Idl.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd UDP
	@echo Directory: UDP
	@echo Project: Makefile.RtEC_UDP_Receiver.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.RtEC_UDP_Receiver.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)
	@cd UDP
	@echo Directory: UDP
	@echo Project: Makefile.RtEC_UDP_Sender.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.RtEC_UDP_Sender.mak CFG="$(CFG)" $(@)
	@cd $(MAKEDIR)

Event_Test_Lib:
	@cd lib
	@echo Directory: lib
	@echo Project: Makefile.Event_Test_Lib.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Test_Lib.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Event_Basic_Atomic_Reconnect: Event_Test_Lib
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Atomic_Reconnect.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Atomic_Reconnect.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Event_Basic_BCast: Event_Test_Lib
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_BCast.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_BCast.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Event_Basic_Bitmask: Event_Test_Lib
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Bitmask.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Bitmask.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Event_Basic_Complex: Event_Test_Lib
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Complex.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Complex.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Event_Basic_Control: Event_Test_Lib
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Control.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Control.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Event_Basic_Disconnect: Event_Test_Lib
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Disconnect.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Disconnect.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Event_Basic_Gateway: Event_Test_Lib
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Gateway.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Gateway.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Event_Basic_MT_Disconnect: Event_Test_Lib
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_MT_Disconnect.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_MT_Disconnect.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Event_Basic_Negation: Event_Test_Lib
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Negation.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Negation.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Event_Basic_Observer: Event_Test_Lib
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Observer.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Observer.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Event_Basic_Random: Event_Test_Lib
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Random.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Random.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Event_Basic_Reconnect: Event_Test_Lib
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Reconnect.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Reconnect.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Event_Basic_Shutdown: Event_Test_Lib
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Shutdown.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Shutdown.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Event_Basic_Timeout: Event_Test_Lib
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Timeout.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Timeout.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Event_Basic_Wildcard: Event_Test_Lib
	@cd Basic
	@echo Directory: Basic
	@echo Project: Makefile.Event_Basic_Wildcard.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Basic_Wildcard.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

AddrServer_Client:
	@cd Mcast/AddrServer
	@echo Directory: Mcast/AddrServer
	@echo Project: Makefile.AddrServer_Client.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.AddrServer_Client.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

AddrServer_Server:
	@cd Mcast/AddrServer
	@echo Directory: Mcast/AddrServer
	@echo Project: Makefile.AddrServer_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.AddrServer_Server.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

ECMcastTests_lib:
	@cd Mcast/Common
	@echo Directory: Mcast/Common
	@echo Project: Makefile.ECMcastTests_lib.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.ECMcastTests_lib.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Complex_Consumer: ECMcastTests_lib
	@cd Mcast/Complex
	@echo Directory: Mcast/Complex
	@echo Project: Makefile.Complex_Consumer.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Complex_Consumer.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Complex_Gateway_Ec: ECMcastTests_lib
	@cd Mcast/Complex
	@echo Directory: Mcast/Complex
	@echo Project: Makefile.Complex_Gateway_Ec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Complex_Gateway_Ec.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Complex_Supplier: ECMcastTests_lib
	@cd Mcast/Complex
	@echo Directory: Mcast/Complex
	@echo Project: Makefile.Complex_Supplier.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Complex_Supplier.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

RTEC_MCast_Federated_Consumer:
	@cd Mcast/RTEC_MCast_Federated
	@echo Directory: Mcast/RTEC_MCast_Federated
	@echo Project: Makefile.RTEC_MCast_Federated_Consumer.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.RTEC_MCast_Federated_Consumer.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

RTEC_MCast_Federated_Supplier:
	@cd Mcast/RTEC_MCast_Federated
	@echo Directory: Mcast/RTEC_MCast_Federated
	@echo Project: Makefile.RTEC_MCast_Federated_Supplier.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.RTEC_MCast_Federated_Supplier.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Simple_Consumer: ECMcastTests_lib
	@cd Mcast/Simple
	@echo Directory: Mcast/Simple
	@echo Project: Makefile.Simple_Consumer.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Simple_Consumer.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Simple_Gateway_Ec: ECMcastTests_lib
	@cd Mcast/Simple
	@echo Directory: Mcast/Simple
	@echo Project: Makefile.Simple_Gateway_Ec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Simple_Gateway_Ec.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Simple_Supplier: ECMcastTests_lib
	@cd Mcast/Simple
	@echo Directory: Mcast/Simple
	@echo Project: Makefile.Simple_Supplier.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Simple_Supplier.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Two_Way_Application: ECMcastTests_lib
	@cd Mcast/Two_Way
	@echo Directory: Mcast/Two_Way
	@echo Project: Makefile.Two_Way_Application.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Two_Way_Application.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Two_Way_Gateway_Ec: ECMcastTests_lib
	@cd Mcast/Two_Way
	@echo Directory: Mcast/Two_Way
	@echo Project: Makefile.Two_Way_Gateway_Ec.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Two_Way_Gateway_Ec.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Event_Performance_Connect: Event_Test_Lib
	@cd Performance
	@echo Directory: Performance
	@echo Project: Makefile.Event_Performance_Connect.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Performance_Connect.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Event_Performance_Inversion: Event_Test_Lib
	@cd Performance
	@echo Directory: Performance
	@echo Project: Makefile.Event_Performance_Inversion.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Performance_Inversion.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Event_Performance_Latency: Event_Test_Lib
	@cd Performance
	@echo Directory: Performance
	@echo Project: Makefile.Event_Performance_Latency.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Performance_Latency.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Event_Performance_Latency_Server: Event_Test_Lib
	@cd Performance
	@echo Directory: Performance
	@echo Project: Makefile.Event_Performance_Latency_Server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Performance_Latency_Server.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

Event_Performance_Throughput: Event_Test_Lib
	@cd Performance
	@echo Directory: Performance
	@echo Project: Makefile.Event_Performance_Throughput.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.Event_Performance_Throughput.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

TFTest:
	@cd ThreadFlags
	@echo Directory: ThreadFlags
	@echo Project: Makefile.TFTest.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.TFTest.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

RtEC_UDP_Idl:
	@cd UDP
	@echo Directory: UDP
	@echo Project: Makefile.RtEC_UDP_Idl.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.RtEC_UDP_Idl.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

RtEC_UDP_Receiver:
	@cd UDP
	@echo Directory: UDP
	@echo Project: Makefile.RtEC_UDP_Receiver.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.RtEC_UDP_Receiver.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

RtEC_UDP_Sender: RtEC_UDP_Idl
	@cd UDP
	@echo Directory: UDP
	@echo Project: Makefile.RtEC_UDP_Sender.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.RtEC_UDP_Sender.mak CFG="$(CFG)" all
	@cd $(MAKEDIR)

project_name_list:
	@echo Event_Basic_Atomic_Reconnect
	@echo Event_Basic_BCast
	@echo Event_Basic_Bitmask
	@echo Event_Basic_Complex
	@echo Event_Basic_Control
	@echo Event_Basic_Disconnect
	@echo Event_Basic_Gateway
	@echo Event_Basic_MT_Disconnect
	@echo Event_Basic_Negation
	@echo Event_Basic_Observer
	@echo Event_Basic_Random
	@echo Event_Basic_Reconnect
	@echo Event_Basic_Shutdown
	@echo Event_Basic_Timeout
	@echo Event_Basic_Wildcard
	@echo AddrServer_Client
	@echo AddrServer_Server
	@echo ECMcastTests_lib
	@echo Complex_Consumer
	@echo Complex_Gateway_Ec
	@echo Complex_Supplier
	@echo RTEC_MCast_Federated_Consumer
	@echo RTEC_MCast_Federated_Supplier
	@echo Simple_Consumer
	@echo Simple_Gateway_Ec
	@echo Simple_Supplier
	@echo Two_Way_Application
	@echo Two_Way_Gateway_Ec
	@echo Event_Performance_Connect
	@echo Event_Performance_Inversion
	@echo Event_Performance_Latency
	@echo Event_Performance_Latency_Server
	@echo Event_Performance_Throughput
	@echo TFTest
	@echo RtEC_UDP_Idl
	@echo RtEC_UDP_Receiver
	@echo RtEC_UDP_Sender
	@echo Event_Test_Lib
