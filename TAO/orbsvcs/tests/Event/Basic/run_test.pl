eval '(exit $?0)' && eval 'exec perl -S $0 ${1+"$@"}'
    & eval 'exec perl -S $0 $argv:q'
    if 0;

# $Id: run_test.pl 935 2008-12-10 21:47:27Z mitza $
# -*- perl -*-

# This is a Perl script that runs the client and all the other servers that
# are needed

use lib "$ENV{ACE_ROOT}/bin";
use PerlACE::Run_Test;

PerlACE::add_lib_path ('../lib');

$status = 0;

$svc_conf = PerlACE::LocalFile ("svc$PerlACE::svcconf_ext");
$observer_conf = PerlACE::LocalFile ("observer$PerlACE::svcconf_ext");
$svc_complex_conf = PerlACE::LocalFile ("svc.complex$PerlACE::svcconf_ext");
$mt_svc_conf = PerlACE::LocalFile ("mt.svc$PerlACE::svcconf_ext");
$svc_complex_conf = PerlACE::LocalFile ("svc.complex$PerlACE::svcconf_ext");
$control_conf = PerlACE::LocalFile ("control$PerlACE::svcconf_ext");

sub RunTest ($$$)
{
    my $message = shift;
    my $program = shift;
    my $arguments = shift;

    my $TEST = new PerlACE::Process ($program, $arguments);

    print STDERR "\n\n$message\n";

    my $test = $TEST->SpawnWaitKill (240);

    if ($test != 0) {
        print STDERR "ERROR: Test returned $test\n";
        $status = 1;
    }
}

RunTest ("Reconnect suppliers and consumers, using disconnect/connect calls",
         "Reconnect",
         "-ORBsvcconf $svc_conf -suppliers 100 -consumers 100 -d 100");

RunTest ("Reconnect suppliers and consumers, using connect calls",
         "Reconnect",
         "-ORBsvcconf $svc_conf -suppliers 100 -consumers 100 -d 100 -s -c");

RunTest ("Shutdown EC with clients still attached",
         "Shutdown",
         "-ORBsvcconf $svc_conf -suppliers 5 -consumers 5");

RunTest ("Gateway test",
         "Gateway",
         "-ORBsvcconf $observer_conf");

RunTest ("Complex event channel test, multiple ECs connected through gateways",
         "Observer",
         "-ORBsvcconf $observer_conf -consumer_tshift 0 -consumers 5 -supplier_tshift 0 -suppliers 2 -burstsize 10 -burstcount 10 -burstpause 0");

RunTest ("Timeout tests",
         "Timeout",
         "-ORBsvcconf $svc_conf");

RunTest ("Wildcard tests",
         "Wildcard",
         "-ORBsvcconf $svc_conf");

RunTest ("Negation tests",
         "Negation",
         "-ORBsvcconf $svc_conf");

RunTest ("Bitmask tests",
         "Bitmask",
         "-ORBSvcConf $svc_complex_conf");

RunTest ("Disconnect callbacks test",
         "Disconnect",
         "-ORBsvcconf $svc_conf");

RunTest ("MT Disconnects test",
         "MT_Disconnect",
         "-ORBSvcConf $mt_svc_conf");

RunTest ("Atomic Reconnection test",
         "Atomic_Reconnect",
         "-ORBSvcConf $mt_svc_conf");

RunTest ("Complex filter",
         "Complex",
         "-ORBSvcConf $svc_complex_conf");

RunTest ("Control test",
         "Control",
         "-ORBSvcConf $control_conf");

RunTest ("Random test",
         "Random",
         "-ORBSvcConf $svc_conf -suppliers 4 -consumers 4 -max_recursion 1");

exit $status;
