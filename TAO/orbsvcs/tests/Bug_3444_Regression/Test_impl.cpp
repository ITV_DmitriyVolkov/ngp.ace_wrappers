// $Id: Test_impl.cpp 979 2008-12-31 20:22:32Z mitza $

#include "Test_impl.h"

ACE_RCSID(Bug_3444_Regression, Test_impl, "$Id: Test_impl.cpp 979 2008-12-31 20:22:32Z mitza $")

Server_impl::Server_impl (CORBA::ORB_ptr orb)
  : orb_ (CORBA::ORB::_duplicate (orb))
{
}

void Server_impl::shutdown ()
{
  this->orb_->shutdown (0);
}
