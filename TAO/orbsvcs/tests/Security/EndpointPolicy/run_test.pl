eval '(exit $?0)' && eval 'exec perl -S $0 ${1+"$@"}'
     & eval 'exec perl -S $0 $argv:q'
     if 0;

# $Id: run_test.pl 1154 2009-05-15 17:17:06Z mitza $
# -*- perl -*-

use lib "$ENV{ACE_ROOT}/bin";
use PerlACE::Run_Test;

$status = 0;

$iorfile = PerlACE::LocalFile ("test.ior");
unlink $iorfile;

$port = 12345;

if (PerlACE::is_vxworks_test()) {
    $exe = new PerlACE::ProcessVX ("test", "-ORBDottedDecimalAddresses 0 -ORBUseSharedProfile 1 -o $iorfile -p $port");
}
else {
    $exe = new PerlACE::Process ("test", "-ORBDottedDecimalAddresses 0 -ORBUseSharedProfile 1 -o $iorfile -p $port");
}

print "Starting server using shared profiles\n";

$exe->Spawn ();

if (PerlACE::waitforfile_timed ($iorfile,
                        $PerlACE::wait_interval_for_process_creation) == -1) {
    print STDERR "ERROR: cannot find file <$iorfile>\n";
    $exe->Kill (); $exe->TimedWait (1);
    exit 1;
}

# The server ought to die quickly on its own.
$server = $exe->WaitKill (2);

if ($server != 0) {
    print STDERR "ERROR: server [single profile per IOR] returned $server\n";
    $status = 1;
}

unlink $iorfile;

exit $status;
