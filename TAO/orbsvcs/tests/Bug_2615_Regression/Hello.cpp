//
// $Id: Hello.cpp 935 2008-12-10 21:47:27Z mitza $
//

#include "ServerRequest_Interceptor2.h"
#include "Hello.h"

ACE_RCSID(Hello, Hello, "$Id: Hello.cpp 935 2008-12-10 21:47:27Z mitza $")

Hello::Hello (CORBA::ORB_ptr orb, Test::Hello_ptr, CORBA::ULong)
  : orb_ (CORBA::ORB::_duplicate (orb))
{
}

void
Hello::shutdown (void)
{
  this->orb_->shutdown (0);
}

void
Hello::ping (void)
{
  return;
}

CORBA::Boolean
Hello::has_ft_request_service_context (void)
{
  return ServerRequest_Interceptor2::has_ft_request_sc_;
}
