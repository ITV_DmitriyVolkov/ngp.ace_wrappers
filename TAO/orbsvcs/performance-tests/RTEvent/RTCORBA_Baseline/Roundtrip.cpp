//
// $Id: Roundtrip.cpp 935 2008-12-10 21:47:27Z mitza $
//
#include "Roundtrip.h"
#include "ace/High_Res_Timer.h"

ACE_RCSID (TAO_RTEC_Perf_RTCORBA_Baseline,
           Roundtrip,
           "$Id: Roundtrip.cpp 935 2008-12-10 21:47:27Z mitza $")

Roundtrip::Roundtrip (CORBA::ORB_ptr orb)
  : orb_ (CORBA::ORB::_duplicate (orb))
  , gsf_ (ACE_High_Res_Timer::calibrate ())
{
}

Test::Timestamp
Roundtrip::test_method (Test::Timestamp send_time,
                        CORBA::Long workload_in_usecs)
{
  ACE_hrtime_t start = ACE_OS::gethrtime ();
  CORBA::Long elapsed = 0;

  while (elapsed < workload_in_usecs)
    {
      elapsed =
        static_cast<CORBA::Long> (
            (ACE_OS::gethrtime () - start) / this->gsf_
          );
    }

  return send_time;
}

void
Roundtrip::shutdown (void)
{
  this->orb_->shutdown (0);
}
