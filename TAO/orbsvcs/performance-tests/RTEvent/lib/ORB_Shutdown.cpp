/**
 * @file ORB_Shutdown.cpp
 *
 * $Id: ORB_Shutdown.cpp 14 2007-02-01 15:49:12Z mitza $
 *
 * @author Carlos O'Ryan <coryan@uci.edu>
 */
#include "ORB_Shutdown.h"

#if !defined(__ACE_INLINE__)
#include "ORB_Shutdown.inl"
#endif /* __ACE_INLINE__ */

ACE_RCSID (TAO_PERF_RTEC, 
           ORB_Shutdown, 
           "$Id: ORB_Shutdown.cpp 14 2007-02-01 15:49:12Z mitza $")
