/**
 * @file ORB_Holder.cpp
 *
 * $Id: ORB_Holder.cpp 935 2008-12-10 21:47:27Z mitza $
 *
 * @author Carlos O'Ryan <coryan@uci.edu>
 */

#include "ORB_Holder.h"

#if !defined(__ACE_INLINE__)
#include "ORB_Holder.inl"
#endif /* __ACE_INLINE__ */

ACE_RCSID (TAO_PERF_RTEC,
           ORB_Holder,
           "$Id: ORB_Holder.cpp 935 2008-12-10 21:47:27Z mitza $")

ORB_Holder::ORB_Holder (int &argc, ACE_TCHAR *argv[],
                        const char *orb_id)
  :  orb_ (CORBA::ORB_init (argc, argv, orb_id))
{
}

ORB_Holder::~ORB_Holder (void)
{
  try{
    this->orb_->destroy ();
  } catch (const CORBA::Exception&) {
    // @@ TODO Log this event, check the Servant_var.cpp comments for
    // details.
  }
}
