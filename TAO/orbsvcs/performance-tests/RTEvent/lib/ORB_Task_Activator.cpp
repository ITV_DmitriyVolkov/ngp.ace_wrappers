/**
 * @file ORB_Task_Activator.cpp
 *
 * $Id: ORB_Task_Activator.cpp 14 2007-02-01 15:49:12Z mitza $
 *
 * @author Carlos O'Ryan <coryan@uci.edu>
 */

#include "ORB_Task_Activator.h"

#if !defined(__ACE_INLINE__)
#include "ORB_Task_Activator.inl"
#endif /* __ACE_INLINE__ */

ACE_RCSID (TAO_PERF_RTEC,
           ORB_Task_Activator,
           "$Id: ORB_Task_Activator.cpp 14 2007-02-01 15:49:12Z mitza $")

ORB_Task_Activator::~ORB_Task_Activator (void)
{
  if (this->task_ == 0)
    return;
  try{
    (*this->task_)->shutdown (0);
  } catch (const CORBA::Exception&) {
  }
}
