/* -*- C++ -*- */
//=============================================================================
/**
 *  @file    FT_FaultAnalyzer.cpp
 *
 *  $Id: FT_FaultAnalyzer.cpp 14 2007-02-01 15:49:12Z mitza $
 *
 *  This file is part of TAO's implementation of Fault Tolerant CORBA.
 *
 *  @author Steve Totten <totten_s@ociweb.com>
 */
//=============================================================================

#include "FT_FaultAnalyzer.h"

ACE_RCSID (FT_FaultAnalyzer,
           FT_FaultAnalyzer,
           "$Id: FT_FaultAnalyzer.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

/// Default constructor.
TAO::FT_FaultAnalyzer::FT_FaultAnalyzer ()
{
}

/// Destructor.
TAO::FT_FaultAnalyzer::~FT_FaultAnalyzer ()
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
