// $Id: EC_Dispatching.cpp 14 2007-02-01 15:49:12Z mitza $

#include "orbsvcs/Event/EC_Dispatching.h"
#include "orbsvcs/Event/EC_ProxySupplier.h"

ACE_RCSID(Event, EC_Dispatching, "$Id: EC_Dispatching.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_EC_Dispatching::~TAO_EC_Dispatching (void)
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
