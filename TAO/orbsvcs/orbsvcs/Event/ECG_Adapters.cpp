// $Id: ECG_Adapters.cpp 14 2007-02-01 15:49:12Z mitza $

#include "orbsvcs/Event/ECG_Adapters.h"

ACE_RCSID (Event,
           ECG_Adapters,
           "$Id: ECG_Adapters.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_ECG_Handler_Shutdown::~TAO_ECG_Handler_Shutdown (void)
{
}

TAO_ECG_Dgram_Handler::~TAO_ECG_Dgram_Handler (void)
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
