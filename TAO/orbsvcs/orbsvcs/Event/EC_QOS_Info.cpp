// $Id: EC_QOS_Info.cpp 14 2007-02-01 15:49:12Z mitza $

#include "orbsvcs/Event/EC_QOS_Info.h"

#if ! defined (__ACE_INLINE__)
#include "orbsvcs/Event/EC_QOS_Info.inl"
#endif /* __ACE_INLINE__ */

ACE_RCSID(Event, EC_QOS_Info, "$Id: EC_QOS_Info.cpp 14 2007-02-01 15:49:12Z mitza $")
