// $Id: EC_Scheduling_Strategy.cpp 14 2007-02-01 15:49:12Z mitza $

#include "orbsvcs/Event/EC_Scheduling_Strategy.h"
#include "orbsvcs/Event/EC_QOS_Info.h"

ACE_RCSID(Event, EC_Scheduling_Strategy, "$Id: EC_Scheduling_Strategy.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_EC_Scheduling_Strategy::~TAO_EC_Scheduling_Strategy (void)
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
