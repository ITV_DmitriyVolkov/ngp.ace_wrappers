// $Id: ESF_RefCount_Guard.cpp 14 2007-02-01 15:49:12Z mitza $

#ifndef TAO_ESF_REFCOUNT_GUARD_CPP
#define TAO_ESF_REFCOUNT_GUARD_CPP

#include "orbsvcs/ESF/ESF_RefCount_Guard.h"

#if ! defined (__ACE_INLINE__)
#include "orbsvcs/ESF/ESF_RefCount_Guard.inl"
#endif /* __ACE_INLINE__ */

ACE_RCSID(ESF, ESF_RefCount_Guard, "$Id: ESF_RefCount_Guard.cpp 14 2007-02-01 15:49:12Z mitza $")

#endif /* TAO_ESF_REFCOUNT_GUARD_CPP */
