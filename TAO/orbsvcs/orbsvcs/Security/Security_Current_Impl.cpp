// -*- C++ -*-

#include "orbsvcs/Security/Security_Current_Impl.h"

ACE_RCSID (Security,
           SL3_Security_Current_Impl,
           "$Id: Security_Current_Impl.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO::Security::Current_Impl::~Current_Impl (void)
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
