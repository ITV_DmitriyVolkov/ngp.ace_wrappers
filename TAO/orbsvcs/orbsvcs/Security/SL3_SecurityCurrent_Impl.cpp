// -*- C++ -*-

#include "orbsvcs/Security/SL3_SecurityCurrent_Impl.h"

ACE_RCSID (Security,
           SL3_Security_Current_Impl,
           "$Id: SL3_SecurityCurrent_Impl.cpp 14 2007-02-01 15:49:12Z mitza $")


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO::SL3::SecurityCurrent_Impl::~SecurityCurrent_Impl (void)
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
