// $Id: Replication_Strategy.cpp 14 2007-02-01 15:49:12Z mitza $

#include "orbsvcs/FtRtEvent/EventChannel/Replication_Strategy.h"
#include "orbsvcs/FtRtEvent/EventChannel/FTEC_Event_Channel.h"

ACE_RCSID (EventChannel,
           Replication_Strategy,
           "$Id: Replication_Strategy.cpp 14 2007-02-01 15:49:12Z mitza $")


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

Replication_Strategy::Replication_Strategy()
{
}


Replication_Strategy::~Replication_Strategy()
{
}


void
Replication_Strategy::check_validity(void)
{
}


Replication_Strategy*
Replication_Strategy::make_primary_strategy()
{
  return this;
}

TAO_END_VERSIONED_NAMESPACE_DECL
