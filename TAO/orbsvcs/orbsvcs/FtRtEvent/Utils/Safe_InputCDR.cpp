#include "orbsvcs/FtRtEvent/Utils/Safe_InputCDR.h"

ACE_RCSID (Utils,
           Safe_InputCDR,
           "$Id: Safe_InputCDR.cpp 14 2007-02-01 15:49:12Z mitza $")

#if !defined(__ACE_INLINE__)
#include "orbsvcs/FtRtEvent/Utils/Safe_InputCDR.inl"
#endif /* __ACE_INLINE__ */
