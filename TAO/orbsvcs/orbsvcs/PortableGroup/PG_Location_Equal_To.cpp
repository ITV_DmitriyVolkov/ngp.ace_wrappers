// -*- C++ -*-

#include "orbsvcs/PortableGroup/PG_Location_Equal_To.h"

ACE_RCSID (PortableGroup,
           PG_Location_Equal_To,
           "$Id: PG_Location_Equal_To.cpp 14 2007-02-01 15:49:12Z mitza $")

#if !defined (__ACE_INLINE__)
#include "orbsvcs/PortableGroup/PG_Location_Equal_To.inl"
#endif  /* !__ACE_INLINE__ */
