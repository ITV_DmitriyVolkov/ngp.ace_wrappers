#include "orbsvcs/PortableGroup/PG_Null_Property_Validator.h"


ACE_RCSID (PortableGroup,
           PG_Null_Property_Validator,
           "$Id: PG_Null_Property_Validator.cpp 935 2008-12-10 21:47:27Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

void
TAO_PG_Null_Property_Validator::validate_property (
    const PortableGroup::Properties &)
{
}

void
TAO_PG_Null_Property_Validator::validate_criteria (
    const PortableGroup::Properties &)
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
