// -*- C++ -*-
//
// $Id: PG_Location_Hash.cpp 14 2007-02-01 15:49:12Z mitza $

#include "orbsvcs/PortableGroup/PG_Location_Hash.h"

#if !defined (__ACE_INLINE__)
#include "orbsvcs/PortableGroup/PG_Location_Hash.inl"
#endif /* __ACE_INLINE__ */

ACE_RCSID (PortableGroup,
           PG_Location_Hash,
           "$Id: PG_Location_Hash.cpp 14 2007-02-01 15:49:12Z mitza $")
