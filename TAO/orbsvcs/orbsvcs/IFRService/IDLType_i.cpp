// $Id: IDLType_i.cpp 14 2007-02-01 15:49:12Z mitza $

#include "orbsvcs/IFRService/IDLType_i.h"

ACE_RCSID (IFRService, 
           IDLType_i, 
           "$Id: IDLType_i.cpp 14 2007-02-01 15:49:12Z mitza $")


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_IDLType_i::TAO_IDLType_i (TAO_Repository_i *repo)
  : TAO_IRObject_i (repo)
{
}

TAO_IDLType_i::~TAO_IDLType_i (void)
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
