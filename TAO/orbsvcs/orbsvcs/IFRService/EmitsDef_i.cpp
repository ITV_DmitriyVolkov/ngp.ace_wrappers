// $Id: EmitsDef_i.cpp 935 2008-12-10 21:47:27Z mitza $

#include "orbsvcs/IFRService/EmitsDef_i.h"
#include "orbsvcs/IFRService/Repository_i.h"

ACE_RCSID (IFRService,
           EmitsDef_i,
           "$Id: EmitsDef_i.cpp 935 2008-12-10 21:47:27Z mitza $")


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_EmitsDef_i::TAO_EmitsDef_i (TAO_Repository_i *repo)
  : TAO_IRObject_i (repo),
    TAO_Contained_i (repo),
    TAO_EventPortDef_i (repo)
{
}

TAO_EmitsDef_i::~TAO_EmitsDef_i (void)
{
}

CORBA::DefinitionKind
TAO_EmitsDef_i::def_kind (void)
{
  return CORBA::dk_Emits;
}


TAO_END_VERSIONED_NAMESPACE_DECL
