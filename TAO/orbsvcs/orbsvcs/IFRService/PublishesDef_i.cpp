/* -*- C++ -*- */
// $Id: PublishesDef_i.cpp 935 2008-12-10 21:47:27Z mitza $

#include "orbsvcs/IFRService/PublishesDef_i.h"
#include "orbsvcs/IFRService/Repository_i.h"

ACE_RCSID (IFRService,
           PublishesDef_i,
           "$Id: PublishesDef_i.cpp 935 2008-12-10 21:47:27Z mitza $")

TAO_PublishesDef_i::TAO_PublishesDef_i (
    TAO_Repository_i *repo
  )
  : TAO_IRObject_i (repo),
    TAO_Contained_i (repo),
    TAO_EventPortDef_i (repo)
{
}

TAO_PublishesDef_i::~TAO_PublishesDef_i (void)
{
}

CORBA::DefinitionKind
TAO_PublishesDef_i::def_kind (void)
{
  return CORBA::dk_Publishes;
}

