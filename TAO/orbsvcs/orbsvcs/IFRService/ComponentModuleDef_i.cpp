// $Id: ComponentModuleDef_i.cpp 14 2007-02-01 15:49:12Z mitza $

#include "orbsvcs/IFRService/ComponentModuleDef_i.h"

ACE_RCSID (IFRService, 
           ComponentModuleDef_i, 
           "$Id: ComponentModuleDef_i.cpp 14 2007-02-01 15:49:12Z mitza $")


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_ComponentModuleDef_i::TAO_ComponentModuleDef_i (TAO_Repository_i *repo)
  : TAO_IRObject_i (repo),
    TAO_Container_i (repo),
    TAO_ComponentContainer_i (repo)
{
}

TAO_ComponentModuleDef_i::~TAO_ComponentModuleDef_i (void)
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
