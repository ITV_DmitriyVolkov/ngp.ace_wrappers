// $Id: CosNotify_Initializer.cpp 14 2007-02-01 15:49:12Z mitza $

#include "orbsvcs/Notify/CosNotify_Initializer.h"

ACE_RCSID(Notify, TAO_Notify_CosNotify_Initializer, "$Id: CosNotify_Initializer.cpp 14 2007-02-01 15:49:12Z mitza $")

#include "orbsvcs/Notify/CosNotify_Service.h"

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Notify_CosNotify_Initializer::TAO_Notify_CosNotify_Initializer (void)
{
  ACE_Service_Config::static_svcs ()->insert (&ace_svc_desc_TAO_CosNotify_Service);
  ACE_Service_Config::static_svcs ()->insert (&ace_svc_desc_TAO_Notify_Default_EMO_Factory_OLD);
}

TAO_END_VERSIONED_NAMESPACE_DECL
