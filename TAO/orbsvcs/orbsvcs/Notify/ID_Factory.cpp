// $Id: ID_Factory.cpp 14 2007-02-01 15:49:12Z mitza $

#include "orbsvcs/Notify/ID_Factory.h"

#if ! defined (__ACE_INLINE__)
#include "orbsvcs/Notify/ID_Factory.inl"
#endif /* __ACE_INLINE__ */

ACE_RCSID(Notify, TAO_Notify_ID_Factory, "$Id: ID_Factory.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Notify_ID_Factory::TAO_Notify_ID_Factory (void)
  : seed_ (0)
{
}

TAO_Notify_ID_Factory::~TAO_Notify_ID_Factory ()
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
