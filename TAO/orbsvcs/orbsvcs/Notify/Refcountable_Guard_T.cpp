// $Id: Refcountable_Guard_T.cpp 14 2007-02-01 15:49:12Z mitza $

#ifndef TAO_Notify_REFCOUNTABLE_GUARD_T_CPP
#define TAO_Notify_REFCOUNTABLE_GUARD_T_CPP

#include "orbsvcs/Notify/Refcountable_Guard_T.h"

#if ! defined (__ACE_INLINE__)
#include "orbsvcs/Notify/Refcountable_Guard_T.inl"
#endif /* __ACE_INLINE__ */

ACE_RCSID (Notify, TAO_Notify_Refcountable_Guard_T, "$Id: Refcountable_Guard_T.cpp 14 2007-02-01 15:49:12Z mitza $")

#endif /* TAO_Notify_REFCOUNTABLE_GUARD_T_CPP */
