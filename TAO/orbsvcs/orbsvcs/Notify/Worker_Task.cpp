// $Id: Worker_Task.cpp 14 2007-02-01 15:49:12Z mitza $

#include "orbsvcs/Notify/Worker_Task.h"

ACE_RCSID(Notify, TAO_Notify_Worker_Task, "$Id: Worker_Task.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

TAO_Notify_Worker_Task::TAO_Notify_Worker_Task (void)
{
}

TAO_Notify_Worker_Task::~TAO_Notify_Worker_Task ()
{
}

void
TAO_Notify_Worker_Task::update_qos_properties (const TAO_Notify_QoSProperties& /*qos_properties*/)
{
}

TAO_END_VERSIONED_NAMESPACE_DECL
