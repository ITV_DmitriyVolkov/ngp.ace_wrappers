// $Id: Notify_Default_EMO_Factory.cpp 14 2007-02-01 15:49:12Z mitza $

#include "orbsvcs/Notify/Notify_Default_EMO_Factory.h"

ACE_RCSID(Notify, Notify_Default_EMO_Factory, "$Id: Notify_Default_EMO_Factory.cpp 14 2007-02-01 15:49:12Z mitza $")


TAO_BEGIN_VERSIONED_NAMESPACE_DECL

int
TAO_Notify_Default_EMO_Factory::init_svc (void)
{
  // NOP.
  return 0;
}

TAO_END_VERSIONED_NAMESPACE_DECL
