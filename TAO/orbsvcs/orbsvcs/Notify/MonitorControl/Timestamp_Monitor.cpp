// $Id: Timestamp_Monitor.cpp 979 2008-12-31 20:22:32Z mitza $

#include "orbsvcs/orbsvcs/Notify/MonitorControl/Timestamp_Monitor.h"

#if defined (TAO_HAS_MONITOR_FRAMEWORK) && (TAO_HAS_MONITOR_FRAMEWORK == 1)

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

Timestamp_Monitor::Timestamp_Monitor (const char *name)
  : Monitor_Base (name, Monitor_Control_Types::MC_TIME)
{
}

TAO_END_VERSIONED_NAMESPACE_DECL

#endif /* TAO_HAS_MONITOR_FRAMEWORK==1 */
