// -*- C++ -*-
//
// $Id: Constraint_Nodes.inl 979 2008-12-31 20:22:32Z mitza $

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

ACE_INLINE bool
operator!= (const TAO_Literal_Constraint& left,
            const TAO_Literal_Constraint& right)
{
  return !(left == right);
}

ACE_INLINE bool
operator<= (const TAO_Literal_Constraint& left,
            const TAO_Literal_Constraint& right)
{
  return !(left > right);
}

ACE_INLINE bool
operator>= (const TAO_Literal_Constraint& left,
            const TAO_Literal_Constraint& right)
{
  return !(left < right);
}

TAO_END_VERSIONED_NAMESPACE_DECL
