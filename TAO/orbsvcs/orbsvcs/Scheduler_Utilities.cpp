// $Id: Scheduler_Utilities.cpp 14 2007-02-01 15:49:12Z mitza $

#include "orbsvcs/Scheduler_Utilities.h"

#if ! defined (__ACE_INLINE__)
#include "orbsvcs/Scheduler_Utilities.inl"
#endif /* __ACE_INLINE__ */

ACE_RCSID (orbsvcs,
           Scheduler_Utilities,
           "$Id: Scheduler_Utilities.cpp 14 2007-02-01 15:49:12Z mitza $")

