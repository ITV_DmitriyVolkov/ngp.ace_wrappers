// $Id: Time_Utilities.cpp 935 2008-12-10 21:47:27Z mitza $

#include "orbsvcs/Time_Utilities.h"

#if !defined (__ACE_INLINE__)
# include "orbsvcs/Time_Utilities.inl"
#endif /* __ACE_INLINE__ */

ACE_RCSID(orbsvcs, Time_Utilities, "$Id: Time_Utilities.cpp 935 2008-12-10 21:47:27Z mitza $")

TAO_BEGIN_VERSIONED_NAMESPACE_DECL

// Number of nanoseconds between CORBA and POSIX epochs.
#ifdef ACE_LACKS_LONGLONG_T
const ACE_UINT64
ORBSVCS_Time::Time_Base_Offset(0x9e7d0000, 0x0A993A60F);
#else
const ACE_UINT64
ORBSVCS_Time::Time_Base_Offset(ACE_UINT64_LITERAL(12219292800000000000));
#endif

TAO_END_VERSIONED_NAMESPACE_DECL
