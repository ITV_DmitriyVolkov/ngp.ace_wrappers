MessageIdTypedef=WORD

MessageId=0x1
SymbolicName=ERROR_CATEGORY
Language=English
TAO_NT_Service errors
.


MessageId=100
SymbolicName=MSG_INVALID_NAME
Language=English
Computer name contains incorrect (non-ASCII) symbols. This is not supported by TAO.
.

MessageId=101
SymbolicName=MSG_NOT_ENOUGH_INFO
Language=English
Not enough information for TAO_NT_Service initialization.
.

MessageId=102
SymbolicName=MSG_NAME_CHANGED
Language=English
Computer name was changed. TAO_NT_Service should be reinstalled.
.

MessageId=103
SymbolicName=MSG_CONFIG_FILE_PROCESS_FAILED
Language=English
Failed to process configuration file or initialize TAO components.
.
