#ifndef TAO_NT_SERVICE_H
#define TAO_NT_SERVICE_H

#include /**/ "ace/OS.h"

#if defined (ACE_WIN32)

#include /**/ "ace/NT_Service.h"
#include /**/ "ace/Singleton.h"
#include /**/ "ace/Synch.h"
#include /**/ "tao/orbconf.h"

class TAO_NT_Service : public ACE_NT_Service
{
  // = TITLE
  //    Run the TAO Service as a Windows NT Service.
public:
  typedef TAO_SYNCH_RECURSIVE_MUTEX MUTEX;

  // = Initialization and termination hooks.
  TAO_NT_Service (void);
  virtual ~TAO_NT_Service (void);

  virtual void handle_control (DWORD control_code);
  // We override <handle_control> because it handles stop requests
  // privately.

  virtual int handle_exception (ACE_HANDLE h);
  // We override <handle_exception> so a 'stop' control code can pop
  // the reactor off of its wait.

  virtual int svc (void);
  // This is a virtual method inherited from ACE_NT_Service.

  virtual int init (int argc,
                    ACE_TCHAR *argv[]);
  // Initialize the objects argc_ and argv_ attributes values.

private:
  // = Keep track of the "command-line" arguments.
  int argc_;
  int argc_save_;
  char **argv_;
  char **argv_save_;

  friend class ACE_Singleton<TAO_NT_Service, MUTEX>;
  friend class AutoFinalizer;

  static ACE_THR_FUNC_RETURN orb_run(void* porb);

  void ReportError(DWORD errCode);

};

#endif
#endif