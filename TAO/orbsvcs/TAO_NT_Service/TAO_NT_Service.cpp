#include "TAO_NT_Service.h"

#include "tao/ORB_Constants.h"
#include "tao/ORB.h"
#include "tao/ORB_Core.h"
#include "tao/PortableServer/PortableServer.h"

#include "ace/Service_Config.h"
#include "ace/Thread_Manager.h"
#include "ace/Log_Msg.h"
#include "ace/Signal.h"
#include "ace/Time_Value.h"
#include "ace/Argv_Type_Converter.h"
#include "ace/ARGV.h"

#include "TAO_Service_Message.h"

#include <string>

namespace
{
	const wchar_t* IOR_HOSTNAME = L"hostname_in_ior=";
}

class AutoFinalizer
{
    
public:
    AutoFinalizer (TAO_NT_Service &service)
		:	service_(service)
	{
	}
    ~AutoFinalizer ()
	{
	  service_.report_status (SERVICE_STOPPED);
	  ACE_DEBUG ((LM_DEBUG, "Reported service stoped\n"));
	}
    
private:
    TAO_NT_Service &service_;
};

bool IsEnglishComputerName(wchar_t c)
{
	int code = c;
	return c < 128;
}

typedef ACE_Singleton<TAO_NT_Service, TAO_NT_Service::MUTEX> SERVICE;


ACE_NT_SERVICE_DEFINE (service,
                       TAO_NT_Service,
                       "TAO NT Services");


static BOOL __stdcall
ConsoleHandler (DWORD /* ctrlType */)
{
  SERVICE::instance ()->handle_control (SERVICE_CONTROL_STOP);
  return TRUE;
}

int main (int argc, char *argv[])
{
	ACE_Argv_Type_Converter argcon (argc, argv);
	SERVICE::instance()->init(argcon.get_argc(), argcon.get_TCHAR_argv());
	if(argc>1 && strcmp(argv[1], "--interactive")==0)
	{
	    SetConsoleCtrlHandler (&ConsoleHandler, 1);
		SERVICE::instance()->svc();
	}else
	{
      ACE_NT_SERVICE_RUN (service,
                          SERVICE::instance (),
                          ret);
      if (ret == 0)
        ACE_ERROR ((LM_ERROR,
                    "%p\n",
	               "Couldn't start service"));
	}
	return 0;
}

TAO_NT_Service::TAO_NT_Service (void)
  : argc_ (0),
    argc_save_ (0),
    argv_ (0),
    argv_save_ (0)
{
}

TAO_NT_Service::~TAO_NT_Service (void)
{
  if (argv_save_)
    {
      for (int i = 0; i < argc_save_; i++)
        ACE_OS::free (argv_save_[i]);

      ACE_OS::free (argv_save_);
    }
}

void
TAO_NT_Service::handle_control (DWORD control_code)
{
  if (control_code == SERVICE_CONTROL_SHUTDOWN
      || control_code == SERVICE_CONTROL_STOP)
    {
      // Just in case any of the following method calls
      // throws in a way we do not expect.
      // This instance's destructor will notify the OS.
      AutoFinalizer afinalizer (*this);

      report_status (SERVICE_STOP_PENDING);

      // This must be all that needs to be done since this method is executing
      // in a separate thread from the one running the reactor.
      // When the reactor is stopped it calls ORB::destroy(), which in turn
      // calls ORB::shutdown(1) *and* unbinds the ORB from the ORB table.

      try
        {
			//ACE_Service_Config::close();
			TAO_ORB_Core_instance ()->orb ()->shutdown (1);
        }
      catch (const CORBA::Exception&)
        {
          // What should we do here? Even the log messages are not
          // showing up, since the thread that runs this is not an ACE
          // thread. It is allways spawned/controlled by Windows ...
        }
    }
  else
  {
    ACE_NT_Service::handle_control (control_code);
  }
}

int
TAO_NT_Service::handle_exception (ACE_HANDLE)
{
  return 0;
}

#define REGISTRY_KEY_ROOT HKEY_LOCAL_MACHINE
#define TAO_REGISTRY_SUBKEY "SOFTWARE\\ACE\\TAO"
#define TAO_SERVICE_OPTS_NAME "TaoNTServiceOptions"
#define TAO_SERVICE_SVCCONF "TaoNTServiceSvcConf"
// #define TAO_SERVICE_PARAM_COUNT "TaoServiceParameterCount"

int
TAO_NT_Service::init (int argc,
                             ACE_TCHAR *argv[])
{
  HKEY hkey = 0;
  BYTE buf[ACE_DEFAULT_ARGV_BUFSIZ];

  *buf = '\0';
  ACE_TEXT_RegOpenKeyEx (REGISTRY_KEY_ROOT,
                         TAO_REGISTRY_SUBKEY,
                         0,
                         KEY_READ,
                         &hkey);
  DWORD type;
  DWORD bufSize = sizeof (buf);

  ACE_TEXT_RegQueryValueEx (hkey,
                            TAO_SERVICE_OPTS_NAME,
                            NULL,
                            &type,
                            buf,
                            &bufSize);

  RegCloseKey (hkey);


  if (ACE_OS::strlen ((char *) buf) > 0)
    {
      ACE_ARGV args ((const char*) buf);
      // Allocate the internal args list to be one bigger than the
      // args list passed into the function. We use a 'save' list in
      // case we use a 'destructive' args list processor - this way we
      // maintain the correct argv and argc for memory freeing
      // operations in the destructor.
      argv_save_ = (char **) ACE_OS::malloc (sizeof (char *) * (argc + args.argc ()));

      // Copy the values into the internal args buffer.
      int i;
      for (i = 0; i < argc; i++)
        argv_save_[i] = ACE_OS::strdup (argv[i]);

      int j = 0;
      for (i = argc; i < static_cast<int> ((args.argc () + argc)); i++)
        argv_save_[i] = ACE_OS::strdup (args.argv ()[j++]);

      // Set the arg counter.
      argc_save_ = argc + args.argc ();
      argc_ = argc_save_;
      argv_ = argv_save_;
    }
  else
    {
      argc_ = argc;
      argv_ = argv;
    }

  return 0;
}

ACE_THR_FUNC_RETURN
TAO_NT_Service::orb_run(void* porb)
{
	CORBA::ORB* pOrb = ACE_static_cast(CORBA::ORB*, porb);
	pOrb->run();
	return 0;
}

void TAO_NT_Service::ReportError(DWORD errCode)
{
    HANDLE hEventSource = RegisterEventSource(NULL, L"TAO_NT_Service");
    ReportEvent(
      hEventSource, EVENTLOG_ERROR_TYPE, 
      ERROR_CATEGORY, errCode, NULL, 0, 0, NULL, NULL);
    DeregisterEventSource(hEventSource);
}

int
TAO_NT_Service::svc (void)
{
  try
    {
	  // Just in case handle_control does not get the chance
      // to execute, or is never called by Windows. This instance's
      // destructor will inform the OS of our demise.
      AutoFinalizer afinalizer (*this);

	  bool initializationError = true;
	  WORD errorCode = MSG_NOT_ENOUGH_INFO;

	  DWORD nameBufferSize = MAX_COMPUTERNAME_LENGTH + 1;
	  wchar_t nameBuffer[MAX_COMPUTERNAME_LENGTH + 1] = {0};
	  GetComputerNameW(nameBuffer, &nameBufferSize);

      std::string svcConfFile("ovsoft_svc.conf.xml"); // < crap: after a while, 
      // remove this default value. 
      // It is provided to preserve compatibility with old behaviour
      // when TAO NT Service read file with such name in the absence of registry parameter

	  if (nameBufferSize != std::count_if(&nameBuffer[0], &nameBuffer[nameBufferSize], IsEnglishComputerName))
	  {
	      errorCode = MSG_INVALID_NAME;
	  }
	  else
	  {
		  HKEY hKey = NULL;
		  if (ERROR_SUCCESS == RegCreateKeyEx(HKEY_LOCAL_MACHINE, L"Software\\ACE\\TAO", 0, NULL,
			  0, KEY_QUERY_VALUE, NULL, &hKey, NULL))
		  {
			  DWORD dwType = REG_SZ;
			  DWORD dwSize = sizeof(DWORD);
			  if (ERROR_SUCCESS == RegQueryValueExW(hKey, L"TaoNTServiceOptions", NULL, &dwType, 
					NULL, &dwSize))
			  {
				  wchar_t* dataBuffer = new wchar_t[dwSize + 1];
				  if (ERROR_SUCCESS == RegQueryValueExW(hKey, L"TaoNTServiceOptions", NULL, &dwType, 
						(LPBYTE)dataBuffer, &dwSize))
				  {
					  std::basic_string<wchar_t> cName(dataBuffer, dwSize);
					  std::basic_string<wchar_t>::size_type pos1, pos2;
					  pos1 = cName.find(IOR_HOSTNAME) + wcslen(IOR_HOSTNAME);
					  if (std::basic_string<wchar_t>::npos != pos1)
					  {
						  pos2 = cName.find(L" ", pos1);
						  cName.assign(cName.substr(pos1, pos2 - pos1));

						  std::basic_string<wchar_t>::size_type size = cName.size();
						  if ((cName.size() == nameBufferSize) && (0 == wcsncmp(nameBuffer, cName.c_str(), nameBufferSize)))
						  {
							  initializationError = false;
						  }
						  else
						  {
							  errorCode = MSG_NAME_CHANGED;
						  }
					  }
				  }
				  delete[] dataBuffer;
			  }

			  dwType = REG_SZ;
			  dwSize = sizeof(DWORD);
			  if ((ERROR_SUCCESS == RegQueryValueExA(hKey, 
                  "TaoNTServiceSvcConf", NULL, &dwType, 
					NULL, &dwSize)) && (0<dwSize))
              {
                  char* buf=new char[dwSize+1];
			      if ((ERROR_SUCCESS == RegQueryValueExA(hKey, 
                      "TaoNTServiceSvcConf", NULL, &dwType, 
					    (LPBYTE)buf, &dwSize)) && (0<dwSize))
                        svcConfFile=std::string(buf, dwSize);
                  delete[] buf;
              }

			  RegCloseKey(hKey);
		  }
	  }

	  if (initializationError)
	  {
		  ReportError(errorCode);
		  return -1;
	  }

	  ACE_Argv_Type_Converter argcon (argc_, argv_);
      // ORB initialization boiler plate...
      CORBA::ORB_var orb =
        CORBA::ORB_init (argcon.get_argc (), argcon.get_ASCII_argv (),
                         "");

	  CORBA::Object_var o=orb->resolve_initial_references("RootPOA");
	  PortableServer::POA_var root=PortableServer::POA::_narrow(o);
	  PortableServer::POAManager_var mgr=root->the_POAManager();

      if (!svcConfFile.empty() && -1==ACE_Service_Config::process_file(svcConfFile.c_str()))
      {
          ReportError(MSG_CONFIG_FILE_PROCESS_FAILED);
		  return -1;
      }

      mgr->activate();

      SYSTEM_INFO sysinfo; 
      GetSystemInfo( &sysinfo );
      size_t procCount = sysinfo.dwNumberOfProcessors;
      ACE_thread_t* helperThreads = new ACE_thread_t[procCount];
      for (size_t i = 0; i < procCount; ++i)
      {
          if(ACE_Thread_Manager::instance()->spawn(TAO_NT_Service::orb_run, ACE_static_cast(void*, orb.ptr()), THR_NEW_LWP|THR_JOINABLE, &(helperThreads[i]))==-1)
	      {
		      ACE_DEBUG ((LM_ERROR, "Can't start new thread\n"));
	      }
      }

      ACE_DEBUG ((LM_INFO, "Notifying Windows of service startup\n"));
      report_status (SERVICE_RUNNING);
        
      orb->run ();

      for (size_t i = 0; i < procCount; ++i)
      {
	      ACE_Thread_Manager::instance()->join(helperThreads[i]);
      }
      delete[] helperThreads;
    }
  catch (const CORBA::Exception& ex)
    {
      ACE_DEBUG ((LM_INFO, "Exception in service - exitting\n"));
      ex._tao_print_exception ("TAO NT Services");
      return -1;
    }

  ACE_DEBUG ((LM_INFO, "Exiting gracefully\n"));
  return 0;
}
