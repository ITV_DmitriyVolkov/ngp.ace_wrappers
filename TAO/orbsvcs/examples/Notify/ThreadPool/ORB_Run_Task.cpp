// $Id: ORB_Run_Task.cpp 14 2007-02-01 15:49:12Z mitza $

#include "ORB_Run_Task.h"

ACE_RCSID (Notify, TAO_Notify_ORB_Run_Task, "$Id: ORB_Run_Task.cpp 14 2007-02-01 15:49:12Z mitza $")

TAO_Notify_ORB_Run_Task::TAO_Notify_ORB_Run_Task (TAO_Notify_ORB_Objects& orb_objects)
  : orb_objects_ (orb_objects)
{
}

TAO_Notify_ORB_Run_Task::~TAO_Notify_ORB_Run_Task ()
{
}

int
TAO_Notify_ORB_Run_Task::svc (void)
{
  try
    {
      this->orb_objects_.current_->the_priority (0);

      this->orb_objects_.orb_->run ();
    }
  catch (const CORBA::Exception&)
    {
    }

  return 0;
}
