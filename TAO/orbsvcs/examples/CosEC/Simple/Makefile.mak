#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake -value_template platforms=Win64 -make_coexistence -recurse -exclude TAO/CIAO -features qos=1,mfc=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: CosEC_Simple_Consumer CosEC_Simple_Service CosEC_Simple_Supplier

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.CosEC_Simple_Consumer.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.CosEC_Simple_Consumer.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.CosEC_Simple_Service.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.CosEC_Simple_Service.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.CosEC_Simple_Supplier.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.CosEC_Simple_Supplier.mak CFG="$(CFG)" $(@)

CosEC_Simple_Consumer:
	@echo Project: Makefile.CosEC_Simple_Consumer.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.CosEC_Simple_Consumer.mak CFG="$(CFG)" all

CosEC_Simple_Service:
	@echo Project: Makefile.CosEC_Simple_Service.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.CosEC_Simple_Service.mak CFG="$(CFG)" all

CosEC_Simple_Supplier:
	@echo Project: Makefile.CosEC_Simple_Supplier.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.CosEC_Simple_Supplier.mak CFG="$(CFG)" all

project_name_list:
	@echo CosEC_Simple_Consumer
	@echo CosEC_Simple_Service
	@echo CosEC_Simple_Supplier
