# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.CosEC_RtEC_Based_lib.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INTDIR=Debug\CosEC_RtEC_Based_lib\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) ".\CosEC_RtEC_Basedd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\..\.." -I"..\..\..\..\.." -I"..\..\..\..\..\orbsvcs" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_HAS_TYPED_EVENT_CHANNEL -DTAO_RTEC_COSEC_BUILD_DLL -f "Makefile.CosEC_RtEC_Based_lib.dep" "ProxyPushSupplier_i.cpp" "ProxyPushConsumer_i.cpp" "EventChannel_i.cpp" "SupplierAdmin_i.cpp" "CosEvent_Utilities.cpp" "ConsumerAdmin_i.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CosEC_RtEC_Basedd.pdb"
	-@del /f/q ".\CosEC_RtEC_Basedd.dll"
	-@del /f/q "$(OUTDIR)\CosEC_RtEC_Basedd.lib"
	-@del /f/q "$(OUTDIR)\CosEC_RtEC_Basedd.exp"
	-@del /f/q "$(OUTDIR)\CosEC_RtEC_Basedd.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\CosEC_RtEC_Based_lib\$(NULL)" mkdir "Debug\CosEC_RtEC_Based_lib"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd"$(INTDIR)/" /I "..\..\..\..\..\.." /I "..\..\..\..\.." /I "..\..\..\..\..\orbsvcs" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_HAS_TYPED_EVENT_CHANNEL /D TAO_RTEC_COSEC_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib TAOd.lib TAO_AnyTypeCoded.lib TAO_CosEventd.lib TAO_PortableServerd.lib TAO_CosEvent_Skeld.lib TAO_Valuetyped.lib TAO_CodecFactoryd.lib TAO_PId.lib TAO_Messagingd.lib TAO_Svc_Utilsd.lib TAO_RTEventd.lib TAO_RTEvent_Skeld.lib TAO_RTEvent_Servd.lib /libpath:"." /libpath:"..\..\..\..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:".\CosEC_RtEC_Basedd.pdb" /machine:IA64 /out:".\CosEC_RtEC_Basedd.dll" /implib:"$(OUTDIR)\CosEC_RtEC_Basedd.lib"
LINK32_OBJS= \
	"$(INTDIR)\ProxyPushSupplier_i.obj" \
	"$(INTDIR)\ProxyPushConsumer_i.obj" \
	"$(INTDIR)\EventChannel_i.obj" \
	"$(INTDIR)\SupplierAdmin_i.obj" \
	"$(INTDIR)\CosEvent_Utilities.obj" \
	"$(INTDIR)\ConsumerAdmin_i.obj"

".\CosEC_RtEC_Basedd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist ".\CosEC_RtEC_Basedd.dll.manifest" mt.exe -manifest ".\CosEC_RtEC_Basedd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=.
INTDIR=Release\CosEC_RtEC_Based_lib\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) ".\CosEC_RtEC_Based.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\..\.." -I"..\..\..\..\.." -I"..\..\..\..\..\orbsvcs" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_HAS_TYPED_EVENT_CHANNEL -DTAO_RTEC_COSEC_BUILD_DLL -f "Makefile.CosEC_RtEC_Based_lib.dep" "ProxyPushSupplier_i.cpp" "ProxyPushConsumer_i.cpp" "EventChannel_i.cpp" "SupplierAdmin_i.cpp" "CosEvent_Utilities.cpp" "ConsumerAdmin_i.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q ".\CosEC_RtEC_Based.dll"
	-@del /f/q "$(OUTDIR)\CosEC_RtEC_Based.lib"
	-@del /f/q "$(OUTDIR)\CosEC_RtEC_Based.exp"
	-@del /f/q "$(OUTDIR)\CosEC_RtEC_Based.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\CosEC_RtEC_Based_lib\$(NULL)" mkdir "Release\CosEC_RtEC_Based_lib"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\..\..\.." /I "..\..\..\..\.." /I "..\..\..\..\..\orbsvcs" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_HAS_TYPED_EVENT_CHANNEL /D TAO_RTEC_COSEC_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib TAO.lib TAO_AnyTypeCode.lib TAO_CosEvent.lib TAO_PortableServer.lib TAO_CosEvent_Skel.lib TAO_Valuetype.lib TAO_CodecFactory.lib TAO_PI.lib TAO_Messaging.lib TAO_Svc_Utils.lib TAO_RTEvent.lib TAO_RTEvent_Skel.lib TAO_RTEvent_Serv.lib /libpath:"." /libpath:"..\..\..\..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:".\CosEC_RtEC_Based.dll" /implib:"$(OUTDIR)\CosEC_RtEC_Based.lib"
LINK32_OBJS= \
	"$(INTDIR)\ProxyPushSupplier_i.obj" \
	"$(INTDIR)\ProxyPushConsumer_i.obj" \
	"$(INTDIR)\EventChannel_i.obj" \
	"$(INTDIR)\SupplierAdmin_i.obj" \
	"$(INTDIR)\CosEvent_Utilities.obj" \
	"$(INTDIR)\ConsumerAdmin_i.obj"

".\CosEC_RtEC_Based.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist ".\CosEC_RtEC_Based.dll.manifest" mt.exe -manifest ".\CosEC_RtEC_Based.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=.
INTDIR=Static_Debug\CosEC_RtEC_Based_lib\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CosEC_RtEC_Basedsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\..\.." -I"..\..\..\..\.." -I"..\..\..\..\..\orbsvcs" -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_HAS_TYPED_EVENT_CHANNEL -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CosEC_RtEC_Based_lib.dep" "ProxyPushSupplier_i.cpp" "ProxyPushConsumer_i.cpp" "EventChannel_i.cpp" "SupplierAdmin_i.cpp" "CosEvent_Utilities.cpp" "ConsumerAdmin_i.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CosEC_RtEC_Basedsd.lib"
	-@del /f/q "$(OUTDIR)\CosEC_RtEC_Basedsd.exp"
	-@del /f/q "$(OUTDIR)\CosEC_RtEC_Basedsd.ilk"
	-@del /f/q ".\CosEC_RtEC_Basedsd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\CosEC_RtEC_Based_lib\$(NULL)" mkdir "Static_Debug\CosEC_RtEC_Based_lib"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /Fd".\CosEC_RtEC_Basedsd.pdb" /I "..\..\..\..\..\.." /I "..\..\..\..\.." /I "..\..\..\..\..\orbsvcs" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_HAS_TYPED_EVENT_CHANNEL /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:".\CosEC_RtEC_Basedsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\ProxyPushSupplier_i.obj" \
	"$(INTDIR)\ProxyPushConsumer_i.obj" \
	"$(INTDIR)\EventChannel_i.obj" \
	"$(INTDIR)\SupplierAdmin_i.obj" \
	"$(INTDIR)\CosEvent_Utilities.obj" \
	"$(INTDIR)\ConsumerAdmin_i.obj"

"$(OUTDIR)\CosEC_RtEC_Basedsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CosEC_RtEC_Basedsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\CosEC_RtEC_Basedsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=.
INTDIR=Static_Release\CosEC_RtEC_Based_lib\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\CosEC_RtEC_Baseds.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\..\..\..\.." -I"..\..\..\..\.." -I"..\..\..\..\..\orbsvcs" -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DTAO_HAS_TYPED_EVENT_CHANNEL -DACE_AS_STATIC_LIBS -DTAO_AS_STATIC_LIBS -f "Makefile.CosEC_RtEC_Based_lib.dep" "ProxyPushSupplier_i.cpp" "ProxyPushConsumer_i.cpp" "EventChannel_i.cpp" "SupplierAdmin_i.cpp" "CosEvent_Utilities.cpp" "ConsumerAdmin_i.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\CosEC_RtEC_Baseds.lib"
	-@del /f/q "$(OUTDIR)\CosEC_RtEC_Baseds.exp"
	-@del /f/q "$(OUTDIR)\CosEC_RtEC_Baseds.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\CosEC_RtEC_Based_lib\$(NULL)" mkdir "Static_Release\CosEC_RtEC_Based_lib"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4250 /wd4290 /wd4355 /wd4250 /wd4290 /I "..\..\..\..\..\.." /I "..\..\..\..\.." /I "..\..\..\..\..\orbsvcs" /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D TAO_HAS_TYPED_EVENT_CHANNEL /D ACE_AS_STATIC_LIBS /D TAO_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:".\CosEC_RtEC_Baseds.lib"
LINK32_OBJS= \
	"$(INTDIR)\ProxyPushSupplier_i.obj" \
	"$(INTDIR)\ProxyPushConsumer_i.obj" \
	"$(INTDIR)\EventChannel_i.obj" \
	"$(INTDIR)\SupplierAdmin_i.obj" \
	"$(INTDIR)\CosEvent_Utilities.obj" \
	"$(INTDIR)\ConsumerAdmin_i.obj"

"$(OUTDIR)\CosEC_RtEC_Baseds.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\CosEC_RtEC_Baseds.lib.manifest" mt.exe -manifest "$(OUTDIR)\CosEC_RtEC_Baseds.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CosEC_RtEC_Based_lib.dep")
!INCLUDE "Makefile.CosEC_RtEC_Based_lib.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="ProxyPushSupplier_i.cpp"

"$(INTDIR)\ProxyPushSupplier_i.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ProxyPushSupplier_i.obj" $(SOURCE)

SOURCE="ProxyPushConsumer_i.cpp"

"$(INTDIR)\ProxyPushConsumer_i.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ProxyPushConsumer_i.obj" $(SOURCE)

SOURCE="EventChannel_i.cpp"

"$(INTDIR)\EventChannel_i.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\EventChannel_i.obj" $(SOURCE)

SOURCE="SupplierAdmin_i.cpp"

"$(INTDIR)\SupplierAdmin_i.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\SupplierAdmin_i.obj" $(SOURCE)

SOURCE="CosEvent_Utilities.cpp"

"$(INTDIR)\CosEvent_Utilities.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CosEvent_Utilities.obj" $(SOURCE)

SOURCE="ConsumerAdmin_i.cpp"

"$(INTDIR)\ConsumerAdmin_i.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ConsumerAdmin_i.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.CosEC_RtEC_Based_lib.dep")
	@echo Using "Makefile.CosEC_RtEC_Based_lib.dep"
!ELSE
	@echo Warning: cannot find "Makefile.CosEC_RtEC_Based_lib.dep"
!ENDIF
!ENDIF

