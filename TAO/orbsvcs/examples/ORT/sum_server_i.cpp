// $Id: sum_server_i.cpp 935 2008-12-10 21:47:27Z mitza $

#include "sum_server_i.h"

ACE_RCSID (ORT,
           sum_server_i,
           "$Id: sum_server_i.cpp 935 2008-12-10 21:47:27Z mitza $")

sum_server_i::sum_server_i ()
{
}

CORBA::Long
sum_server_i::add_variables (CORBA::Long a,
                             CORBA::Long b)
{
  return a+b;
}
