# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.ORT_Idl.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY = "GatewayC.inl" "GatewayS.inl" "GatewayC.h" "GatewayS.h" "GatewayC.cpp" "GatewayS.cpp" "ObjectReferenceFactoryC.inl" "ObjectReferenceFactoryS.inl" "ObjectReferenceFactoryC.h" "ObjectReferenceFactoryS.h" "ObjectReferenceFactoryC.cpp" "ObjectReferenceFactoryS.cpp" "sum_serverC.inl" "sum_serverS.inl" "sum_serverC.h" "sum_serverS.h" "sum_serverC.cpp" "sum_serverS.cpp"

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=.
INTDIR=Debug\ORT_Idl\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY)

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	-@rem
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\d.lib"
	-@del /f/q "$(OUTDIR)\d.exp"
	-@del /f/q "$(OUTDIR)\d.ilk"
	-@del /f/q "GatewayC.inl"
	-@del /f/q "GatewayS.inl"
	-@del /f/q "GatewayC.h"
	-@del /f/q "GatewayS.h"
	-@del /f/q "GatewayC.cpp"
	-@del /f/q "GatewayS.cpp"
	-@del /f/q "ObjectReferenceFactoryC.inl"
	-@del /f/q "ObjectReferenceFactoryS.inl"
	-@del /f/q "ObjectReferenceFactoryC.h"
	-@del /f/q "ObjectReferenceFactoryS.h"
	-@del /f/q "ObjectReferenceFactoryC.cpp"
	-@del /f/q "ObjectReferenceFactoryS.cpp"
	-@del /f/q "sum_serverC.inl"
	-@del /f/q "sum_serverS.inl"
	-@del /f/q "sum_serverC.h"
	-@del /f/q "sum_serverS.h"
	-@del /f/q "sum_serverC.cpp"
	-@del /f/q "sum_serverS.cpp"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\ORT_Idl\$(NULL)" mkdir "Debug\ORT_Idl"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /Fd"$(INTDIR)/" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe


!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=.
INTDIR=Release\ORT_Idl\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY)

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	-@rem
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\.lib"
	-@del /f/q "$(OUTDIR)\.exp"
	-@del /f/q "$(OUTDIR)\.ilk"
	-@del /f/q "GatewayC.inl"
	-@del /f/q "GatewayS.inl"
	-@del /f/q "GatewayC.h"
	-@del /f/q "GatewayS.h"
	-@del /f/q "GatewayC.cpp"
	-@del /f/q "GatewayS.cpp"
	-@del /f/q "ObjectReferenceFactoryC.inl"
	-@del /f/q "ObjectReferenceFactoryS.inl"
	-@del /f/q "ObjectReferenceFactoryC.h"
	-@del /f/q "ObjectReferenceFactoryS.h"
	-@del /f/q "ObjectReferenceFactoryC.cpp"
	-@del /f/q "ObjectReferenceFactoryS.cpp"
	-@del /f/q "sum_serverC.inl"
	-@del /f/q "sum_serverS.inl"
	-@del /f/q "sum_serverC.h"
	-@del /f/q "sum_serverS.h"
	-@del /f/q "sum_serverC.cpp"
	-@del /f/q "sum_serverS.cpp"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\ORT_Idl\$(NULL)" mkdir "Release\ORT_Idl"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe


!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=.
INTDIR=Static_Debug\ORT_Idl\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY)

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	-@rem
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q ".\sd.pdb"
	-@del /f/q "GatewayC.inl"
	-@del /f/q "GatewayS.inl"
	-@del /f/q "GatewayC.h"
	-@del /f/q "GatewayS.h"
	-@del /f/q "GatewayC.cpp"
	-@del /f/q "GatewayS.cpp"
	-@del /f/q "ObjectReferenceFactoryC.inl"
	-@del /f/q "ObjectReferenceFactoryS.inl"
	-@del /f/q "ObjectReferenceFactoryC.h"
	-@del /f/q "ObjectReferenceFactoryS.h"
	-@del /f/q "ObjectReferenceFactoryC.cpp"
	-@del /f/q "ObjectReferenceFactoryS.cpp"
	-@del /f/q "sum_serverC.inl"
	-@del /f/q "sum_serverS.inl"
	-@del /f/q "sum_serverC.h"
	-@del /f/q "sum_serverS.h"
	-@del /f/q "sum_serverC.cpp"
	-@del /f/q "sum_serverS.cpp"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\ORT_Idl\$(NULL)" mkdir "Static_Debug\ORT_Idl"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /Fd".\sd.pdb" /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"



!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=.
INTDIR=Static_Release\ORT_Idl\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY)

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	-@rem
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "GatewayC.inl"
	-@del /f/q "GatewayS.inl"
	-@del /f/q "GatewayC.h"
	-@del /f/q "GatewayS.h"
	-@del /f/q "GatewayC.cpp"
	-@del /f/q "GatewayS.cpp"
	-@del /f/q "ObjectReferenceFactoryC.inl"
	-@del /f/q "ObjectReferenceFactoryS.inl"
	-@del /f/q "ObjectReferenceFactoryC.h"
	-@del /f/q "ObjectReferenceFactoryS.h"
	-@del /f/q "ObjectReferenceFactoryC.cpp"
	-@del /f/q "ObjectReferenceFactoryS.cpp"
	-@del /f/q "sum_serverC.inl"
	-@del /f/q "sum_serverS.inl"
	-@del /f/q "sum_serverC.h"
	-@del /f/q "sum_serverS.h"
	-@del /f/q "sum_serverC.cpp"
	-@del /f/q "sum_serverS.cpp"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\ORT_Idl\$(NULL)" mkdir "Static_Release\ORT_Idl"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"



!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.ORT_Idl.dep")
!INCLUDE "Makefile.ORT_Idl.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
!IF  "$(CFG)" == "Win64 Debug"
SOURCE="Gateway.idl"

InputPath=Gateway.idl

"GatewayC.inl" "GatewayS.inl" "GatewayC.h" "GatewayS.h" "GatewayC.cpp" "GatewayS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-Gateway_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Sa -St "$(InputPath)"
<<

SOURCE="ObjectReferenceFactory.idl"

InputPath=ObjectReferenceFactory.idl

"ObjectReferenceFactoryC.inl" "ObjectReferenceFactoryS.inl" "ObjectReferenceFactoryC.h" "ObjectReferenceFactoryS.h" "ObjectReferenceFactoryC.cpp" "ObjectReferenceFactoryS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-ObjectReferenceFactory_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Sa -St "$(InputPath)"
<<

SOURCE="sum_server.idl"

InputPath=sum_server.idl

"sum_serverC.inl" "sum_serverS.inl" "sum_serverC.h" "sum_serverS.h" "sum_serverC.cpp" "sum_serverS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BEd.dll" "..\..\..\..\lib\TAO_IDL_FEd.dll"
	<<tempfile-Win64-Debug-idl_files-sum_server_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Sa -St "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Release"
SOURCE="Gateway.idl"

InputPath=Gateway.idl

"GatewayC.inl" "GatewayS.inl" "GatewayC.h" "GatewayS.h" "GatewayC.cpp" "GatewayS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-Gateway_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Sa -St "$(InputPath)"
<<

SOURCE="ObjectReferenceFactory.idl"

InputPath=ObjectReferenceFactory.idl

"ObjectReferenceFactoryC.inl" "ObjectReferenceFactoryS.inl" "ObjectReferenceFactoryC.h" "ObjectReferenceFactoryS.h" "ObjectReferenceFactoryC.cpp" "ObjectReferenceFactoryS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-ObjectReferenceFactory_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Sa -St "$(InputPath)"
<<

SOURCE="sum_server.idl"

InputPath=sum_server.idl

"sum_serverC.inl" "sum_serverS.inl" "sum_serverC.h" "sum_serverS.h" "sum_serverC.cpp" "sum_serverS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe" "..\..\..\..\lib\TAO_IDL_BE.dll" "..\..\..\..\lib\TAO_IDL_FE.dll"
	<<tempfile-Win64-Release-idl_files-sum_server_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Sa -St "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Debug"
SOURCE="Gateway.idl"

InputPath=Gateway.idl

"GatewayC.inl" "GatewayS.inl" "GatewayC.h" "GatewayS.h" "GatewayC.cpp" "GatewayS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-Gateway_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Sa -St "$(InputPath)"
<<

SOURCE="ObjectReferenceFactory.idl"

InputPath=ObjectReferenceFactory.idl

"ObjectReferenceFactoryC.inl" "ObjectReferenceFactoryS.inl" "ObjectReferenceFactoryC.h" "ObjectReferenceFactoryS.h" "ObjectReferenceFactoryC.cpp" "ObjectReferenceFactoryS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-ObjectReferenceFactory_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Sa -St "$(InputPath)"
<<

SOURCE="sum_server.idl"

InputPath=sum_server.idl

"sum_serverC.inl" "sum_serverS.inl" "sum_serverC.h" "sum_serverS.h" "sum_serverC.cpp" "sum_serverS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Debug-idl_files-sum_server_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Sa -St "$(InputPath)"
<<

!ELSEIF  "$(CFG)" == "Win64 Static Release"
SOURCE="Gateway.idl"

InputPath=Gateway.idl

"GatewayC.inl" "GatewayS.inl" "GatewayC.h" "GatewayS.h" "GatewayC.cpp" "GatewayS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-Gateway_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Sa -St "$(InputPath)"
<<

SOURCE="ObjectReferenceFactory.idl"

InputPath=ObjectReferenceFactory.idl

"ObjectReferenceFactoryC.inl" "ObjectReferenceFactoryS.inl" "ObjectReferenceFactoryC.h" "ObjectReferenceFactoryS.h" "ObjectReferenceFactoryC.cpp" "ObjectReferenceFactoryS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-ObjectReferenceFactory_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Sa -St "$(InputPath)"
<<

SOURCE="sum_server.idl"

InputPath=sum_server.idl

"sum_serverC.inl" "sum_serverS.inl" "sum_serverC.h" "sum_serverS.h" "sum_serverC.cpp" "sum_serverS.cpp" : $(SOURCE)  "..\..\..\..\bin\tao_idl.exe"
	<<tempfile-Win64-Static_Release-idl_files-sum_server_idl.bat
	@echo off
	PATH=%PATH%;..\..\..\..\lib
	..\..\..\..\bin\tao_idl -Wb,pre_include=ace/pre.h -Wb,post_include=ace/post.h -I..\..\.. -Sa -St "$(InputPath)"
<<

!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.ORT_Idl.dep")
	@echo Using "Makefile.ORT_Idl.dep"
!ENDIF
!ENDIF

