// file      : Test/ReferenceCounting/Inline/inline.cpp
// author    : Boris Kolpackov <boris@kolpackov.net>
// copyright : Copyright (c) 2002-2003 Boris Kolpackov
// license   : http://kolpackov.net/license.html

//
// This is a link-time test to detect any problems with inline functions
// (notably missing inline specifier).
//

#include "Utility/ReferenceCounting/ReferenceCounting.hpp"

int main ()
{
}
//$Id: inline.cpp 14 2007-02-01 15:49:12Z mitza $
