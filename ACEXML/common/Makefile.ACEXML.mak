# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.ACEXML.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\lib
INTDIR=Debug\ACEXML\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\ACEXMLd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACEXML_BUILD_DLL -f "Makefile.ACEXML.dep" "XML_Codecs.cpp" "Attributes.cpp" "CharStream.cpp" "HttpCharStream.cpp" "StrCharStream.cpp" "DTD_Manager.cpp" "Attributes_Def_Builder.cpp" "EntityResolver.cpp" "InputSource.cpp" "SAXExceptions.cpp" "Validator.cpp" "DTDHandler.cpp" "XMLFilterImpl.cpp" "LocatorImpl.cpp" "Encoding.cpp" "DefaultHandler.cpp" "AttributesImpl.cpp" "Locator.cpp" "Transcode.cpp" "Env.cpp" "XML_Util.cpp" "StreamFactory.cpp" "ZipCharStream.cpp" "Mem_Map_Stream.cpp" "FileCharStream.cpp" "Exception.cpp" "XMLReader.cpp" "Element_Def_Builder.cpp" "ContentHandler.cpp" "URL_Addr.cpp" "ErrorHandler.cpp" "NamespaceSupport.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\ACEXMLd.pdb"
	-@del /f/q "..\..\lib\ACEXMLd.dll"
	-@del /f/q "$(OUTDIR)\ACEXMLd.lib"
	-@del /f/q "$(OUTDIR)\ACEXMLd.exp"
	-@del /f/q "$(OUTDIR)\ACEXMLd.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\ACEXML\$(NULL)" mkdir "Debug\ACEXML"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4355 /Fd"$(INTDIR)/" /I "..\.." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACEXML_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\lib\ACEXMLd.pdb" /machine:IA64 /out:"..\..\lib\ACEXMLd.dll" /implib:"$(OUTDIR)\ACEXMLd.lib"
LINK32_OBJS= \
	"$(INTDIR)\XML_Codecs.obj" \
	"$(INTDIR)\Attributes.obj" \
	"$(INTDIR)\CharStream.obj" \
	"$(INTDIR)\HttpCharStream.obj" \
	"$(INTDIR)\StrCharStream.obj" \
	"$(INTDIR)\DTD_Manager.obj" \
	"$(INTDIR)\Attributes_Def_Builder.obj" \
	"$(INTDIR)\EntityResolver.obj" \
	"$(INTDIR)\InputSource.obj" \
	"$(INTDIR)\SAXExceptions.obj" \
	"$(INTDIR)\Validator.obj" \
	"$(INTDIR)\DTDHandler.obj" \
	"$(INTDIR)\XMLFilterImpl.obj" \
	"$(INTDIR)\LocatorImpl.obj" \
	"$(INTDIR)\Encoding.obj" \
	"$(INTDIR)\DefaultHandler.obj" \
	"$(INTDIR)\AttributesImpl.obj" \
	"$(INTDIR)\Locator.obj" \
	"$(INTDIR)\Transcode.obj" \
	"$(INTDIR)\Env.obj" \
	"$(INTDIR)\XML_Util.obj" \
	"$(INTDIR)\StreamFactory.obj" \
	"$(INTDIR)\ZipCharStream.obj" \
	"$(INTDIR)\Mem_Map_Stream.obj" \
	"$(INTDIR)\FileCharStream.obj" \
	"$(INTDIR)\Exception.obj" \
	"$(INTDIR)\XMLReader.obj" \
	"$(INTDIR)\Element_Def_Builder.obj" \
	"$(INTDIR)\ContentHandler.obj" \
	"$(INTDIR)\URL_Addr.obj" \
	"$(INTDIR)\ErrorHandler.obj" \
	"$(INTDIR)\NamespaceSupport.obj"

"..\..\lib\ACEXMLd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\ACEXMLd.dll.manifest" mt.exe -manifest "..\..\lib\ACEXMLd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\lib
INTDIR=Release\ACEXML\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\lib\ACEXML.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACEXML_BUILD_DLL -f "Makefile.ACEXML.dep" "XML_Codecs.cpp" "Attributes.cpp" "CharStream.cpp" "HttpCharStream.cpp" "StrCharStream.cpp" "DTD_Manager.cpp" "Attributes_Def_Builder.cpp" "EntityResolver.cpp" "InputSource.cpp" "SAXExceptions.cpp" "Validator.cpp" "DTDHandler.cpp" "XMLFilterImpl.cpp" "LocatorImpl.cpp" "Encoding.cpp" "DefaultHandler.cpp" "AttributesImpl.cpp" "Locator.cpp" "Transcode.cpp" "Env.cpp" "XML_Util.cpp" "StreamFactory.cpp" "ZipCharStream.cpp" "Mem_Map_Stream.cpp" "FileCharStream.cpp" "Exception.cpp" "XMLReader.cpp" "Element_Def_Builder.cpp" "ContentHandler.cpp" "URL_Addr.cpp" "ErrorHandler.cpp" "NamespaceSupport.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\lib\ACEXML.dll"
	-@del /f/q "$(OUTDIR)\ACEXML.lib"
	-@del /f/q "$(OUTDIR)\ACEXML.exp"
	-@del /f/q "$(OUTDIR)\ACEXML.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\ACEXML\$(NULL)" mkdir "Release\ACEXML"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\.." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACEXML_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib /libpath:"." /libpath:"..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\lib\ACEXML.dll" /implib:"$(OUTDIR)\ACEXML.lib"
LINK32_OBJS= \
	"$(INTDIR)\XML_Codecs.obj" \
	"$(INTDIR)\Attributes.obj" \
	"$(INTDIR)\CharStream.obj" \
	"$(INTDIR)\HttpCharStream.obj" \
	"$(INTDIR)\StrCharStream.obj" \
	"$(INTDIR)\DTD_Manager.obj" \
	"$(INTDIR)\Attributes_Def_Builder.obj" \
	"$(INTDIR)\EntityResolver.obj" \
	"$(INTDIR)\InputSource.obj" \
	"$(INTDIR)\SAXExceptions.obj" \
	"$(INTDIR)\Validator.obj" \
	"$(INTDIR)\DTDHandler.obj" \
	"$(INTDIR)\XMLFilterImpl.obj" \
	"$(INTDIR)\LocatorImpl.obj" \
	"$(INTDIR)\Encoding.obj" \
	"$(INTDIR)\DefaultHandler.obj" \
	"$(INTDIR)\AttributesImpl.obj" \
	"$(INTDIR)\Locator.obj" \
	"$(INTDIR)\Transcode.obj" \
	"$(INTDIR)\Env.obj" \
	"$(INTDIR)\XML_Util.obj" \
	"$(INTDIR)\StreamFactory.obj" \
	"$(INTDIR)\ZipCharStream.obj" \
	"$(INTDIR)\Mem_Map_Stream.obj" \
	"$(INTDIR)\FileCharStream.obj" \
	"$(INTDIR)\Exception.obj" \
	"$(INTDIR)\XMLReader.obj" \
	"$(INTDIR)\Element_Def_Builder.obj" \
	"$(INTDIR)\ContentHandler.obj" \
	"$(INTDIR)\URL_Addr.obj" \
	"$(INTDIR)\ErrorHandler.obj" \
	"$(INTDIR)\NamespaceSupport.obj"

"..\..\lib\ACEXML.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\lib\ACEXML.dll.manifest" mt.exe -manifest "..\..\lib\ACEXML.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\lib
INTDIR=Static_Debug\ACEXML\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\ACEXMLsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.ACEXML.dep" "XML_Codecs.cpp" "Attributes.cpp" "CharStream.cpp" "HttpCharStream.cpp" "StrCharStream.cpp" "DTD_Manager.cpp" "Attributes_Def_Builder.cpp" "EntityResolver.cpp" "InputSource.cpp" "SAXExceptions.cpp" "Validator.cpp" "DTDHandler.cpp" "XMLFilterImpl.cpp" "LocatorImpl.cpp" "Encoding.cpp" "DefaultHandler.cpp" "AttributesImpl.cpp" "Locator.cpp" "Transcode.cpp" "Env.cpp" "XML_Util.cpp" "StreamFactory.cpp" "ZipCharStream.cpp" "Mem_Map_Stream.cpp" "FileCharStream.cpp" "Exception.cpp" "XMLReader.cpp" "Element_Def_Builder.cpp" "ContentHandler.cpp" "URL_Addr.cpp" "ErrorHandler.cpp" "NamespaceSupport.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\ACEXMLsd.lib"
	-@del /f/q "$(OUTDIR)\ACEXMLsd.exp"
	-@del /f/q "$(OUTDIR)\ACEXMLsd.ilk"
	-@del /f/q "..\..\lib\ACEXMLsd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\ACEXML\$(NULL)" mkdir "Static_Debug\ACEXML"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4355 /Fd"..\..\lib\ACEXMLsd.pdb" /I "..\.." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\ACEXMLsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\XML_Codecs.obj" \
	"$(INTDIR)\Attributes.obj" \
	"$(INTDIR)\CharStream.obj" \
	"$(INTDIR)\HttpCharStream.obj" \
	"$(INTDIR)\StrCharStream.obj" \
	"$(INTDIR)\DTD_Manager.obj" \
	"$(INTDIR)\Attributes_Def_Builder.obj" \
	"$(INTDIR)\EntityResolver.obj" \
	"$(INTDIR)\InputSource.obj" \
	"$(INTDIR)\SAXExceptions.obj" \
	"$(INTDIR)\Validator.obj" \
	"$(INTDIR)\DTDHandler.obj" \
	"$(INTDIR)\XMLFilterImpl.obj" \
	"$(INTDIR)\LocatorImpl.obj" \
	"$(INTDIR)\Encoding.obj" \
	"$(INTDIR)\DefaultHandler.obj" \
	"$(INTDIR)\AttributesImpl.obj" \
	"$(INTDIR)\Locator.obj" \
	"$(INTDIR)\Transcode.obj" \
	"$(INTDIR)\Env.obj" \
	"$(INTDIR)\XML_Util.obj" \
	"$(INTDIR)\StreamFactory.obj" \
	"$(INTDIR)\ZipCharStream.obj" \
	"$(INTDIR)\Mem_Map_Stream.obj" \
	"$(INTDIR)\FileCharStream.obj" \
	"$(INTDIR)\Exception.obj" \
	"$(INTDIR)\XMLReader.obj" \
	"$(INTDIR)\Element_Def_Builder.obj" \
	"$(INTDIR)\ContentHandler.obj" \
	"$(INTDIR)\URL_Addr.obj" \
	"$(INTDIR)\ErrorHandler.obj" \
	"$(INTDIR)\NamespaceSupport.obj"

"$(OUTDIR)\ACEXMLsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\ACEXMLsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\ACEXMLsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\lib
INTDIR=Static_Release\ACEXML\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\ACEXMLs.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\.." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.ACEXML.dep" "XML_Codecs.cpp" "Attributes.cpp" "CharStream.cpp" "HttpCharStream.cpp" "StrCharStream.cpp" "DTD_Manager.cpp" "Attributes_Def_Builder.cpp" "EntityResolver.cpp" "InputSource.cpp" "SAXExceptions.cpp" "Validator.cpp" "DTDHandler.cpp" "XMLFilterImpl.cpp" "LocatorImpl.cpp" "Encoding.cpp" "DefaultHandler.cpp" "AttributesImpl.cpp" "Locator.cpp" "Transcode.cpp" "Env.cpp" "XML_Util.cpp" "StreamFactory.cpp" "ZipCharStream.cpp" "Mem_Map_Stream.cpp" "FileCharStream.cpp" "Exception.cpp" "XMLReader.cpp" "Element_Def_Builder.cpp" "ContentHandler.cpp" "URL_Addr.cpp" "ErrorHandler.cpp" "NamespaceSupport.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\ACEXMLs.lib"
	-@del /f/q "$(OUTDIR)\ACEXMLs.exp"
	-@del /f/q "$(OUTDIR)\ACEXMLs.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\ACEXML\$(NULL)" mkdir "Static_Release\ACEXML"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\.." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\lib\ACEXMLs.lib"
LINK32_OBJS= \
	"$(INTDIR)\XML_Codecs.obj" \
	"$(INTDIR)\Attributes.obj" \
	"$(INTDIR)\CharStream.obj" \
	"$(INTDIR)\HttpCharStream.obj" \
	"$(INTDIR)\StrCharStream.obj" \
	"$(INTDIR)\DTD_Manager.obj" \
	"$(INTDIR)\Attributes_Def_Builder.obj" \
	"$(INTDIR)\EntityResolver.obj" \
	"$(INTDIR)\InputSource.obj" \
	"$(INTDIR)\SAXExceptions.obj" \
	"$(INTDIR)\Validator.obj" \
	"$(INTDIR)\DTDHandler.obj" \
	"$(INTDIR)\XMLFilterImpl.obj" \
	"$(INTDIR)\LocatorImpl.obj" \
	"$(INTDIR)\Encoding.obj" \
	"$(INTDIR)\DefaultHandler.obj" \
	"$(INTDIR)\AttributesImpl.obj" \
	"$(INTDIR)\Locator.obj" \
	"$(INTDIR)\Transcode.obj" \
	"$(INTDIR)\Env.obj" \
	"$(INTDIR)\XML_Util.obj" \
	"$(INTDIR)\StreamFactory.obj" \
	"$(INTDIR)\ZipCharStream.obj" \
	"$(INTDIR)\Mem_Map_Stream.obj" \
	"$(INTDIR)\FileCharStream.obj" \
	"$(INTDIR)\Exception.obj" \
	"$(INTDIR)\XMLReader.obj" \
	"$(INTDIR)\Element_Def_Builder.obj" \
	"$(INTDIR)\ContentHandler.obj" \
	"$(INTDIR)\URL_Addr.obj" \
	"$(INTDIR)\ErrorHandler.obj" \
	"$(INTDIR)\NamespaceSupport.obj"

"$(OUTDIR)\ACEXMLs.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\ACEXMLs.lib.manifest" mt.exe -manifest "$(OUTDIR)\ACEXMLs.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.ACEXML.dep")
!INCLUDE "Makefile.ACEXML.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="XML_Codecs.cpp"

"$(INTDIR)\XML_Codecs.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\XML_Codecs.obj" $(SOURCE)

SOURCE="Attributes.cpp"

"$(INTDIR)\Attributes.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Attributes.obj" $(SOURCE)

SOURCE="CharStream.cpp"

"$(INTDIR)\CharStream.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\CharStream.obj" $(SOURCE)

SOURCE="HttpCharStream.cpp"

"$(INTDIR)\HttpCharStream.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\HttpCharStream.obj" $(SOURCE)

SOURCE="StrCharStream.cpp"

"$(INTDIR)\StrCharStream.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\StrCharStream.obj" $(SOURCE)

SOURCE="DTD_Manager.cpp"

"$(INTDIR)\DTD_Manager.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DTD_Manager.obj" $(SOURCE)

SOURCE="Attributes_Def_Builder.cpp"

"$(INTDIR)\Attributes_Def_Builder.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Attributes_Def_Builder.obj" $(SOURCE)

SOURCE="EntityResolver.cpp"

"$(INTDIR)\EntityResolver.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\EntityResolver.obj" $(SOURCE)

SOURCE="InputSource.cpp"

"$(INTDIR)\InputSource.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\InputSource.obj" $(SOURCE)

SOURCE="SAXExceptions.cpp"

"$(INTDIR)\SAXExceptions.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\SAXExceptions.obj" $(SOURCE)

SOURCE="Validator.cpp"

"$(INTDIR)\Validator.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Validator.obj" $(SOURCE)

SOURCE="DTDHandler.cpp"

"$(INTDIR)\DTDHandler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DTDHandler.obj" $(SOURCE)

SOURCE="XMLFilterImpl.cpp"

"$(INTDIR)\XMLFilterImpl.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\XMLFilterImpl.obj" $(SOURCE)

SOURCE="LocatorImpl.cpp"

"$(INTDIR)\LocatorImpl.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\LocatorImpl.obj" $(SOURCE)

SOURCE="Encoding.cpp"

"$(INTDIR)\Encoding.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Encoding.obj" $(SOURCE)

SOURCE="DefaultHandler.cpp"

"$(INTDIR)\DefaultHandler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\DefaultHandler.obj" $(SOURCE)

SOURCE="AttributesImpl.cpp"

"$(INTDIR)\AttributesImpl.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\AttributesImpl.obj" $(SOURCE)

SOURCE="Locator.cpp"

"$(INTDIR)\Locator.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Locator.obj" $(SOURCE)

SOURCE="Transcode.cpp"

"$(INTDIR)\Transcode.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Transcode.obj" $(SOURCE)

SOURCE="Env.cpp"

"$(INTDIR)\Env.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Env.obj" $(SOURCE)

SOURCE="XML_Util.cpp"

"$(INTDIR)\XML_Util.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\XML_Util.obj" $(SOURCE)

SOURCE="StreamFactory.cpp"

"$(INTDIR)\StreamFactory.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\StreamFactory.obj" $(SOURCE)

SOURCE="ZipCharStream.cpp"

"$(INTDIR)\ZipCharStream.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ZipCharStream.obj" $(SOURCE)

SOURCE="Mem_Map_Stream.cpp"

"$(INTDIR)\Mem_Map_Stream.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Mem_Map_Stream.obj" $(SOURCE)

SOURCE="FileCharStream.cpp"

"$(INTDIR)\FileCharStream.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\FileCharStream.obj" $(SOURCE)

SOURCE="Exception.cpp"

"$(INTDIR)\Exception.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Exception.obj" $(SOURCE)

SOURCE="XMLReader.cpp"

"$(INTDIR)\XMLReader.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\XMLReader.obj" $(SOURCE)

SOURCE="Element_Def_Builder.cpp"

"$(INTDIR)\Element_Def_Builder.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Element_Def_Builder.obj" $(SOURCE)

SOURCE="ContentHandler.cpp"

"$(INTDIR)\ContentHandler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ContentHandler.obj" $(SOURCE)

SOURCE="URL_Addr.cpp"

"$(INTDIR)\URL_Addr.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\URL_Addr.obj" $(SOURCE)

SOURCE="ErrorHandler.cpp"

"$(INTDIR)\ErrorHandler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\ErrorHandler.obj" $(SOURCE)

SOURCE="NamespaceSupport.cpp"

"$(INTDIR)\NamespaceSupport.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\NamespaceSupport.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.ACEXML.dep")
	@echo Using "Makefile.ACEXML.dep"
!ELSE
	@echo Warning: cannot find "Makefile.ACEXML.dep"
!ENDIF
!ENDIF

