#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake -value_template platforms=Win64 -make_coexistence -recurse -exclude TAO/CIAO -features qos=1,mfc=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: adainset cinset cppinset iinset iinset2 m3inset pinset preinset taoinset tinset

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.adainset.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.adainset.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.cinset.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.cinset.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.cppinset.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.cppinset.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.iinset.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.iinset.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.iinset2.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.iinset2.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.m3inset.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.m3inset.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.pinset.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.pinset.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.preinset.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.preinset.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.taoinset.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.taoinset.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.tinset.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.tinset.mak CFG="$(CFG)" $(@)

adainset:
	@echo Project: Makefile.adainset.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.adainset.mak CFG="$(CFG)" all

cinset:
	@echo Project: Makefile.cinset.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.cinset.mak CFG="$(CFG)" all

cppinset:
	@echo Project: Makefile.cppinset.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.cppinset.mak CFG="$(CFG)" all

iinset:
	@echo Project: Makefile.iinset.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.iinset.mak CFG="$(CFG)" all

iinset2: iinset
	@echo Project: Makefile.iinset2.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.iinset2.mak CFG="$(CFG)" all

m3inset:
	@echo Project: Makefile.m3inset.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.m3inset.mak CFG="$(CFG)" all

pinset:
	@echo Project: Makefile.pinset.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.pinset.mak CFG="$(CFG)" all

preinset:
	@echo Project: Makefile.preinset.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.preinset.mak CFG="$(CFG)" all

taoinset:
	@echo Project: Makefile.taoinset.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.taoinset.mak CFG="$(CFG)" all

tinset:
	@echo Project: Makefile.tinset.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.tinset.mak CFG="$(CFG)" all

project_name_list:
	@echo adainset
	@echo cinset
	@echo cppinset
	@echo iinset
	@echo iinset2
	@echo m3inset
	@echo pinset
	@echo preinset
	@echo taoinset
	@echo tinset
