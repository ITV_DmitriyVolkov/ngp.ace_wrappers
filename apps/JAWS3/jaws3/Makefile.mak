#----------------------------------------------------------------------------
#       NMAKE Workspace
#
# $Id: NMakeWorkspaceCreator.pm 1934 2010-11-12 12:40:22Z elliott_c $
#
# This file was generated by MPC.  Any changes made directly to
# this file will be lost the next time it is generated.
#
# MPC Command:
# bin/mwc.pl -type nmake -value_template platforms=Win64 -make_coexistence -recurse -exclude TAO/CIAO -features qos=1,mfc=1
#
#----------------------------------------------------------------------------

!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CUSTOM_TARGETS)" == ""
CUSTOM_TARGETS=_EMPTY_TARGET_
!ENDIF

all: JAWS3 JAWS3_server

clean depend generated realclean $(CUSTOM_TARGETS):
	@echo Project: Makefile.JAWS3.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.JAWS3.mak CFG="$(CFG)" $(@)
	@echo Project: Makefile.JAWS3_server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.JAWS3_server.mak CFG="$(CFG)" $(@)

JAWS3:
	@echo Project: Makefile.JAWS3.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.JAWS3.mak CFG="$(CFG)" all

JAWS3_server: JAWS3
	@echo Project: Makefile.JAWS3_server.mak
	$(MAKE) /$(MAKEFLAGS) /f Makefile.JAWS3_server.mak CFG="$(CFG)" all

project_name_list:
	@echo JAWS3
	@echo JAWS3_server
