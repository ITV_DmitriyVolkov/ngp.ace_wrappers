# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.JAWS3.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\..\lib
INTDIR=Debug\JAWS3\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\lib\JAWS3d.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DJAWS_BUILD_DLL -f "Makefile.JAWS3.dep" "Asynch_IO.cpp" "Concurrency.cpp" "Config_File.cpp" "Datagram.cpp" "Event_Completer.cpp" "Event_Dispatcher.cpp" "FILE.cpp" "IO.cpp" "Options.cpp" "Protocol_Handler.cpp" "Reactive_IO.cpp" "Signal_Task.cpp" "Symbol_Table.cpp" "Synch_IO.cpp" "THYBRID_Concurrency.cpp" "TPOOL_Concurrency.cpp" "TPR_Concurrency.cpp" "Task_Timer.cpp" "Templates.cpp" "Timer.cpp" "Timer_Helpers.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\JAWS3d.pdb"
	-@del /f/q "..\..\..\lib\JAWS3d.dll"
	-@del /f/q "$(OUTDIR)\JAWS3d.lib"
	-@del /f/q "$(OUTDIR)\JAWS3d.exp"
	-@del /f/q "$(OUTDIR)\JAWS3d.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\JAWS3\$(NULL)" mkdir "Debug\JAWS3"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4355 /Fd"$(INTDIR)/" /I "..\..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D JAWS_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\..\lib\JAWS3d.pdb" /machine:IA64 /out:"..\..\..\lib\JAWS3d.dll" /implib:"$(OUTDIR)\JAWS3d.lib"
LINK32_OBJS= \
	"$(INTDIR)\Asynch_IO.obj" \
	"$(INTDIR)\Concurrency.obj" \
	"$(INTDIR)\Config_File.obj" \
	"$(INTDIR)\Datagram.obj" \
	"$(INTDIR)\Event_Completer.obj" \
	"$(INTDIR)\Event_Dispatcher.obj" \
	"$(INTDIR)\FILE.obj" \
	"$(INTDIR)\IO.obj" \
	"$(INTDIR)\Options.obj" \
	"$(INTDIR)\Protocol_Handler.obj" \
	"$(INTDIR)\Reactive_IO.obj" \
	"$(INTDIR)\Signal_Task.obj" \
	"$(INTDIR)\Symbol_Table.obj" \
	"$(INTDIR)\Synch_IO.obj" \
	"$(INTDIR)\THYBRID_Concurrency.obj" \
	"$(INTDIR)\TPOOL_Concurrency.obj" \
	"$(INTDIR)\TPR_Concurrency.obj" \
	"$(INTDIR)\Task_Timer.obj" \
	"$(INTDIR)\Templates.obj" \
	"$(INTDIR)\Timer.obj" \
	"$(INTDIR)\Timer_Helpers.obj"

"..\..\..\lib\JAWS3d.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\lib\JAWS3d.dll.manifest" mt.exe -manifest "..\..\..\lib\JAWS3d.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\..\lib
INTDIR=Release\JAWS3\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\lib\JAWS3.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DJAWS_BUILD_DLL -f "Makefile.JAWS3.dep" "Asynch_IO.cpp" "Concurrency.cpp" "Config_File.cpp" "Datagram.cpp" "Event_Completer.cpp" "Event_Dispatcher.cpp" "FILE.cpp" "IO.cpp" "Options.cpp" "Protocol_Handler.cpp" "Reactive_IO.cpp" "Signal_Task.cpp" "Symbol_Table.cpp" "Synch_IO.cpp" "THYBRID_Concurrency.cpp" "TPOOL_Concurrency.cpp" "TPR_Concurrency.cpp" "Task_Timer.cpp" "Templates.cpp" "Timer.cpp" "Timer_Helpers.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\..\lib\JAWS3.dll"
	-@del /f/q "$(OUTDIR)\JAWS3.lib"
	-@del /f/q "$(OUTDIR)\JAWS3.exp"
	-@del /f/q "$(OUTDIR)\JAWS3.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\JAWS3\$(NULL)" mkdir "Release\JAWS3"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D JAWS_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\..\lib\JAWS3.dll" /implib:"$(OUTDIR)\JAWS3.lib"
LINK32_OBJS= \
	"$(INTDIR)\Asynch_IO.obj" \
	"$(INTDIR)\Concurrency.obj" \
	"$(INTDIR)\Config_File.obj" \
	"$(INTDIR)\Datagram.obj" \
	"$(INTDIR)\Event_Completer.obj" \
	"$(INTDIR)\Event_Dispatcher.obj" \
	"$(INTDIR)\FILE.obj" \
	"$(INTDIR)\IO.obj" \
	"$(INTDIR)\Options.obj" \
	"$(INTDIR)\Protocol_Handler.obj" \
	"$(INTDIR)\Reactive_IO.obj" \
	"$(INTDIR)\Signal_Task.obj" \
	"$(INTDIR)\Symbol_Table.obj" \
	"$(INTDIR)\Synch_IO.obj" \
	"$(INTDIR)\THYBRID_Concurrency.obj" \
	"$(INTDIR)\TPOOL_Concurrency.obj" \
	"$(INTDIR)\TPR_Concurrency.obj" \
	"$(INTDIR)\Task_Timer.obj" \
	"$(INTDIR)\Templates.obj" \
	"$(INTDIR)\Timer.obj" \
	"$(INTDIR)\Timer_Helpers.obj"

"..\..\..\lib\JAWS3.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\lib\JAWS3.dll.manifest" mt.exe -manifest "..\..\..\lib\JAWS3.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\..\lib
INTDIR=Static_Debug\JAWS3\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\JAWS3sd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.JAWS3.dep" "Asynch_IO.cpp" "Concurrency.cpp" "Config_File.cpp" "Datagram.cpp" "Event_Completer.cpp" "Event_Dispatcher.cpp" "FILE.cpp" "IO.cpp" "Options.cpp" "Protocol_Handler.cpp" "Reactive_IO.cpp" "Signal_Task.cpp" "Symbol_Table.cpp" "Synch_IO.cpp" "THYBRID_Concurrency.cpp" "TPOOL_Concurrency.cpp" "TPR_Concurrency.cpp" "Task_Timer.cpp" "Templates.cpp" "Timer.cpp" "Timer_Helpers.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\JAWS3sd.lib"
	-@del /f/q "$(OUTDIR)\JAWS3sd.exp"
	-@del /f/q "$(OUTDIR)\JAWS3sd.ilk"
	-@del /f/q "..\..\..\lib\JAWS3sd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\JAWS3\$(NULL)" mkdir "Static_Debug\JAWS3"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4355 /Fd"..\..\..\lib\JAWS3sd.pdb" /I "..\..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\lib\JAWS3sd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Asynch_IO.obj" \
	"$(INTDIR)\Concurrency.obj" \
	"$(INTDIR)\Config_File.obj" \
	"$(INTDIR)\Datagram.obj" \
	"$(INTDIR)\Event_Completer.obj" \
	"$(INTDIR)\Event_Dispatcher.obj" \
	"$(INTDIR)\FILE.obj" \
	"$(INTDIR)\IO.obj" \
	"$(INTDIR)\Options.obj" \
	"$(INTDIR)\Protocol_Handler.obj" \
	"$(INTDIR)\Reactive_IO.obj" \
	"$(INTDIR)\Signal_Task.obj" \
	"$(INTDIR)\Symbol_Table.obj" \
	"$(INTDIR)\Synch_IO.obj" \
	"$(INTDIR)\THYBRID_Concurrency.obj" \
	"$(INTDIR)\TPOOL_Concurrency.obj" \
	"$(INTDIR)\TPR_Concurrency.obj" \
	"$(INTDIR)\Task_Timer.obj" \
	"$(INTDIR)\Templates.obj" \
	"$(INTDIR)\Timer.obj" \
	"$(INTDIR)\Timer_Helpers.obj"

"$(OUTDIR)\JAWS3sd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\JAWS3sd.lib.manifest" mt.exe -manifest "$(OUTDIR)\JAWS3sd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\..\lib
INTDIR=Static_Release\JAWS3\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\JAWS3s.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.JAWS3.dep" "Asynch_IO.cpp" "Concurrency.cpp" "Config_File.cpp" "Datagram.cpp" "Event_Completer.cpp" "Event_Dispatcher.cpp" "FILE.cpp" "IO.cpp" "Options.cpp" "Protocol_Handler.cpp" "Reactive_IO.cpp" "Signal_Task.cpp" "Symbol_Table.cpp" "Synch_IO.cpp" "THYBRID_Concurrency.cpp" "TPOOL_Concurrency.cpp" "TPR_Concurrency.cpp" "Task_Timer.cpp" "Templates.cpp" "Timer.cpp" "Timer_Helpers.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\JAWS3s.lib"
	-@del /f/q "$(OUTDIR)\JAWS3s.exp"
	-@del /f/q "$(OUTDIR)\JAWS3s.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\JAWS3\$(NULL)" mkdir "Static_Release\JAWS3"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\lib\JAWS3s.lib"
LINK32_OBJS= \
	"$(INTDIR)\Asynch_IO.obj" \
	"$(INTDIR)\Concurrency.obj" \
	"$(INTDIR)\Config_File.obj" \
	"$(INTDIR)\Datagram.obj" \
	"$(INTDIR)\Event_Completer.obj" \
	"$(INTDIR)\Event_Dispatcher.obj" \
	"$(INTDIR)\FILE.obj" \
	"$(INTDIR)\IO.obj" \
	"$(INTDIR)\Options.obj" \
	"$(INTDIR)\Protocol_Handler.obj" \
	"$(INTDIR)\Reactive_IO.obj" \
	"$(INTDIR)\Signal_Task.obj" \
	"$(INTDIR)\Symbol_Table.obj" \
	"$(INTDIR)\Synch_IO.obj" \
	"$(INTDIR)\THYBRID_Concurrency.obj" \
	"$(INTDIR)\TPOOL_Concurrency.obj" \
	"$(INTDIR)\TPR_Concurrency.obj" \
	"$(INTDIR)\Task_Timer.obj" \
	"$(INTDIR)\Templates.obj" \
	"$(INTDIR)\Timer.obj" \
	"$(INTDIR)\Timer_Helpers.obj"

"$(OUTDIR)\JAWS3s.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\JAWS3s.lib.manifest" mt.exe -manifest "$(OUTDIR)\JAWS3s.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.JAWS3.dep")
!INCLUDE "Makefile.JAWS3.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Asynch_IO.cpp"

"$(INTDIR)\Asynch_IO.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Asynch_IO.obj" $(SOURCE)

SOURCE="Concurrency.cpp"

"$(INTDIR)\Concurrency.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Concurrency.obj" $(SOURCE)

SOURCE="Config_File.cpp"

"$(INTDIR)\Config_File.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Config_File.obj" $(SOURCE)

SOURCE="Datagram.cpp"

"$(INTDIR)\Datagram.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Datagram.obj" $(SOURCE)

SOURCE="Event_Completer.cpp"

"$(INTDIR)\Event_Completer.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Event_Completer.obj" $(SOURCE)

SOURCE="Event_Dispatcher.cpp"

"$(INTDIR)\Event_Dispatcher.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Event_Dispatcher.obj" $(SOURCE)

SOURCE="FILE.cpp"

"$(INTDIR)\FILE.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\FILE.obj" $(SOURCE)

SOURCE="IO.cpp"

"$(INTDIR)\IO.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IO.obj" $(SOURCE)

SOURCE="Options.cpp"

"$(INTDIR)\Options.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Options.obj" $(SOURCE)

SOURCE="Protocol_Handler.cpp"

"$(INTDIR)\Protocol_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Protocol_Handler.obj" $(SOURCE)

SOURCE="Reactive_IO.cpp"

"$(INTDIR)\Reactive_IO.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Reactive_IO.obj" $(SOURCE)

SOURCE="Signal_Task.cpp"

"$(INTDIR)\Signal_Task.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Signal_Task.obj" $(SOURCE)

SOURCE="Symbol_Table.cpp"

"$(INTDIR)\Symbol_Table.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Symbol_Table.obj" $(SOURCE)

SOURCE="Synch_IO.cpp"

"$(INTDIR)\Synch_IO.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Synch_IO.obj" $(SOURCE)

SOURCE="THYBRID_Concurrency.cpp"

"$(INTDIR)\THYBRID_Concurrency.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\THYBRID_Concurrency.obj" $(SOURCE)

SOURCE="TPOOL_Concurrency.cpp"

"$(INTDIR)\TPOOL_Concurrency.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TPOOL_Concurrency.obj" $(SOURCE)

SOURCE="TPR_Concurrency.cpp"

"$(INTDIR)\TPR_Concurrency.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\TPR_Concurrency.obj" $(SOURCE)

SOURCE="Task_Timer.cpp"

"$(INTDIR)\Task_Timer.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Task_Timer.obj" $(SOURCE)

SOURCE="Templates.cpp"

"$(INTDIR)\Templates.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Templates.obj" $(SOURCE)

SOURCE="Timer.cpp"

"$(INTDIR)\Timer.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Timer.obj" $(SOURCE)

SOURCE="Timer_Helpers.cpp"

"$(INTDIR)\Timer_Helpers.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Timer_Helpers.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.JAWS3.dep")
	@echo Using "Makefile.JAWS3.dep"
!ELSE
	@echo Warning: cannot find "Makefile.JAWS3.dep"
!ENDIF
!ENDIF

