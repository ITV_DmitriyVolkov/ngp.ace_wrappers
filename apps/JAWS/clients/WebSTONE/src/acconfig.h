/* $Id: acconfig.h 14 2007-02-01 15:49:12Z mitza $ */
/* Special definitions for autoheader
        Copyright (C) 1995 Silicon Graphics, Inc.
*/

/* Define to the name of the distribution.  */
#undef PRODUCT

/* Define to the version of the distribution.  */
#undef VERSION

/* Should we use timezone in gettimeofday? */
#undef USE_TIMEZONE

/* end */
