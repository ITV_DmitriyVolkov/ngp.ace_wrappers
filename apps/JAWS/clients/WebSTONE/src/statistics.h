#ifndef _STATISTICS_H_
#define _STATISTICS_H_
/* $Id: statistics.h 14 2007-02-01 15:49:12Z mitza $ */

extern double   mean(const double, const int);
extern double   variance(const double, const double, const int);
extern double   stddev(const double, const double, const int);

#endif /* ! _STATISTICS_H_ */
