# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.JAWS.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\..\lib
INTDIR=Debug\JAWS\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\lib\JAWSd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_BUILD_SVC_DLL -f "Makefile.JAWS.dep" "HTTP_Server.cpp" "HTTP_Config.cpp" "HTTP_Handler.cpp" "HTTP_Helpers.cpp" "JAWS_Pipeline.cpp" "JAWS_Concurrency.cpp" "HTTP_Request.cpp" "HTTP_Response.cpp" "Parse_Headers.cpp" "IO.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\JAWSd.pdb"
	-@del /f/q "..\..\..\lib\JAWSd.dll"
	-@del /f/q "$(OUTDIR)\JAWSd.lib"
	-@del /f/q "$(OUTDIR)\JAWSd.exp"
	-@del /f/q "$(OUTDIR)\JAWSd.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\JAWS\$(NULL)" mkdir "Debug\JAWS"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4355 /Fd"$(INTDIR)/" /I "..\..\.." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_BUILD_SVC_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\..\lib\JAWSd.pdb" /machine:IA64 /out:"..\..\..\lib\JAWSd.dll" /implib:"$(OUTDIR)\JAWSd.lib"
LINK32_OBJS= \
	"$(INTDIR)\HTTP_Server.obj" \
	"$(INTDIR)\HTTP_Config.obj" \
	"$(INTDIR)\HTTP_Handler.obj" \
	"$(INTDIR)\HTTP_Helpers.obj" \
	"$(INTDIR)\JAWS_Pipeline.obj" \
	"$(INTDIR)\JAWS_Concurrency.obj" \
	"$(INTDIR)\HTTP_Request.obj" \
	"$(INTDIR)\HTTP_Response.obj" \
	"$(INTDIR)\Parse_Headers.obj" \
	"$(INTDIR)\IO.obj"

"..\..\..\lib\JAWSd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\lib\JAWSd.dll.manifest" mt.exe -manifest "..\..\..\lib\JAWSd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\..\lib
INTDIR=Release\JAWS\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\lib\JAWS.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_BUILD_SVC_DLL -f "Makefile.JAWS.dep" "HTTP_Server.cpp" "HTTP_Config.cpp" "HTTP_Handler.cpp" "HTTP_Helpers.cpp" "JAWS_Pipeline.cpp" "JAWS_Concurrency.cpp" "HTTP_Request.cpp" "HTTP_Response.cpp" "Parse_Headers.cpp" "IO.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\..\lib\JAWS.dll"
	-@del /f/q "$(OUTDIR)\JAWS.lib"
	-@del /f/q "$(OUTDIR)\JAWS.exp"
	-@del /f/q "$(OUTDIR)\JAWS.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\JAWS\$(NULL)" mkdir "Release\JAWS"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\..\.." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_BUILD_SVC_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\..\lib\JAWS.dll" /implib:"$(OUTDIR)\JAWS.lib"
LINK32_OBJS= \
	"$(INTDIR)\HTTP_Server.obj" \
	"$(INTDIR)\HTTP_Config.obj" \
	"$(INTDIR)\HTTP_Handler.obj" \
	"$(INTDIR)\HTTP_Helpers.obj" \
	"$(INTDIR)\JAWS_Pipeline.obj" \
	"$(INTDIR)\JAWS_Concurrency.obj" \
	"$(INTDIR)\HTTP_Request.obj" \
	"$(INTDIR)\HTTP_Response.obj" \
	"$(INTDIR)\Parse_Headers.obj" \
	"$(INTDIR)\IO.obj"

"..\..\..\lib\JAWS.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\lib\JAWS.dll.manifest" mt.exe -manifest "..\..\..\lib\JAWS.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\..\lib
INTDIR=Static_Debug\JAWS\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\JAWSsd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.JAWS.dep" "HTTP_Server.cpp" "HTTP_Config.cpp" "HTTP_Handler.cpp" "HTTP_Helpers.cpp" "JAWS_Pipeline.cpp" "JAWS_Concurrency.cpp" "HTTP_Request.cpp" "HTTP_Response.cpp" "Parse_Headers.cpp" "IO.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\JAWSsd.lib"
	-@del /f/q "$(OUTDIR)\JAWSsd.exp"
	-@del /f/q "$(OUTDIR)\JAWSsd.ilk"
	-@del /f/q "..\..\..\lib\JAWSsd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\JAWS\$(NULL)" mkdir "Static_Debug\JAWS"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4355 /Fd"..\..\..\lib\JAWSsd.pdb" /I "..\..\.." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\lib\JAWSsd.lib"
LINK32_OBJS= \
	"$(INTDIR)\HTTP_Server.obj" \
	"$(INTDIR)\HTTP_Config.obj" \
	"$(INTDIR)\HTTP_Handler.obj" \
	"$(INTDIR)\HTTP_Helpers.obj" \
	"$(INTDIR)\JAWS_Pipeline.obj" \
	"$(INTDIR)\JAWS_Concurrency.obj" \
	"$(INTDIR)\HTTP_Request.obj" \
	"$(INTDIR)\HTTP_Response.obj" \
	"$(INTDIR)\Parse_Headers.obj" \
	"$(INTDIR)\IO.obj"

"$(OUTDIR)\JAWSsd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\JAWSsd.lib.manifest" mt.exe -manifest "$(OUTDIR)\JAWSsd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\..\lib
INTDIR=Static_Release\JAWS\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\JAWSs.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.JAWS.dep" "HTTP_Server.cpp" "HTTP_Config.cpp" "HTTP_Handler.cpp" "HTTP_Helpers.cpp" "JAWS_Pipeline.cpp" "JAWS_Concurrency.cpp" "HTTP_Request.cpp" "HTTP_Response.cpp" "Parse_Headers.cpp" "IO.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\JAWSs.lib"
	-@del /f/q "$(OUTDIR)\JAWSs.exp"
	-@del /f/q "$(OUTDIR)\JAWSs.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\JAWS\$(NULL)" mkdir "Static_Release\JAWS"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\..\.." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\lib\JAWSs.lib"
LINK32_OBJS= \
	"$(INTDIR)\HTTP_Server.obj" \
	"$(INTDIR)\HTTP_Config.obj" \
	"$(INTDIR)\HTTP_Handler.obj" \
	"$(INTDIR)\HTTP_Helpers.obj" \
	"$(INTDIR)\JAWS_Pipeline.obj" \
	"$(INTDIR)\JAWS_Concurrency.obj" \
	"$(INTDIR)\HTTP_Request.obj" \
	"$(INTDIR)\HTTP_Response.obj" \
	"$(INTDIR)\Parse_Headers.obj" \
	"$(INTDIR)\IO.obj"

"$(OUTDIR)\JAWSs.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\JAWSs.lib.manifest" mt.exe -manifest "$(OUTDIR)\JAWSs.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.JAWS.dep")
!INCLUDE "Makefile.JAWS.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="HTTP_Server.cpp"

"$(INTDIR)\HTTP_Server.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\HTTP_Server.obj" $(SOURCE)

SOURCE="HTTP_Config.cpp"

"$(INTDIR)\HTTP_Config.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\HTTP_Config.obj" $(SOURCE)

SOURCE="HTTP_Handler.cpp"

"$(INTDIR)\HTTP_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\HTTP_Handler.obj" $(SOURCE)

SOURCE="HTTP_Helpers.cpp"

"$(INTDIR)\HTTP_Helpers.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\HTTP_Helpers.obj" $(SOURCE)

SOURCE="JAWS_Pipeline.cpp"

"$(INTDIR)\JAWS_Pipeline.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\JAWS_Pipeline.obj" $(SOURCE)

SOURCE="JAWS_Concurrency.cpp"

"$(INTDIR)\JAWS_Concurrency.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\JAWS_Concurrency.obj" $(SOURCE)

SOURCE="HTTP_Request.cpp"

"$(INTDIR)\HTTP_Request.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\HTTP_Request.obj" $(SOURCE)

SOURCE="HTTP_Response.cpp"

"$(INTDIR)\HTTP_Response.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\HTTP_Response.obj" $(SOURCE)

SOURCE="Parse_Headers.cpp"

"$(INTDIR)\Parse_Headers.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Parse_Headers.obj" $(SOURCE)

SOURCE="IO.cpp"

"$(INTDIR)\IO.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IO.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.JAWS.dep")
	@echo Using "Makefile.JAWS.dep"
!ELSE
	@echo Warning: cannot find "Makefile.JAWS.dep"
!ENDIF
!ENDIF

