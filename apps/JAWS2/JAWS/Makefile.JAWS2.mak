# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.JAWS2.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\..\lib
INTDIR=Debug\JAWS2\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\lib\JAWS2d.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DJAWS_BUILD_DLL -f "Makefile.JAWS2.dep" "Assoc_Array.cpp" "Cache_Manager.cpp" "Cache_Object.cpp" "Concurrency.cpp" "Data_Block.cpp" "FILE.cpp" "Filecache.cpp" "Headers.cpp" "IO.cpp" "IO_Acceptor.cpp" "IO_Handler.cpp" "Parse_Headers.cpp" "Pipeline.cpp" "Pipeline_Tasks.cpp" "Policy.cpp" "Reaper.cpp" "Server.cpp" "Waiter.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\JAWS2d.pdb"
	-@del /f/q "..\..\..\lib\JAWS2d.dll"
	-@del /f/q "$(OUTDIR)\JAWS2d.lib"
	-@del /f/q "$(OUTDIR)\JAWS2d.exp"
	-@del /f/q "$(OUTDIR)\JAWS2d.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\JAWS2\$(NULL)" mkdir "Debug\JAWS2"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4355 /Fd"$(INTDIR)/" /I "..\..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D JAWS_BUILD_DLL /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\..\lib\JAWS2d.pdb" /machine:IA64 /out:"..\..\..\lib\JAWS2d.dll" /implib:"$(OUTDIR)\JAWS2d.lib"
LINK32_OBJS= \
	"$(INTDIR)\Assoc_Array.obj" \
	"$(INTDIR)\Cache_Manager.obj" \
	"$(INTDIR)\Cache_Object.obj" \
	"$(INTDIR)\Concurrency.obj" \
	"$(INTDIR)\Data_Block.obj" \
	"$(INTDIR)\FILE.obj" \
	"$(INTDIR)\Filecache.obj" \
	"$(INTDIR)\Headers.obj" \
	"$(INTDIR)\IO.obj" \
	"$(INTDIR)\IO_Acceptor.obj" \
	"$(INTDIR)\IO_Handler.obj" \
	"$(INTDIR)\Parse_Headers.obj" \
	"$(INTDIR)\Pipeline.obj" \
	"$(INTDIR)\Pipeline_Tasks.obj" \
	"$(INTDIR)\Policy.obj" \
	"$(INTDIR)\Reaper.obj" \
	"$(INTDIR)\Server.obj" \
	"$(INTDIR)\Waiter.obj"

"..\..\..\lib\JAWS2d.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\lib\JAWS2d.dll.manifest" mt.exe -manifest "..\..\..\lib\JAWS2d.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\..\lib
INTDIR=Release\JAWS2\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\lib\JAWS2.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DJAWS_BUILD_DLL -f "Makefile.JAWS2.dep" "Assoc_Array.cpp" "Cache_Manager.cpp" "Cache_Object.cpp" "Concurrency.cpp" "Data_Block.cpp" "FILE.cpp" "Filecache.cpp" "Headers.cpp" "IO.cpp" "IO_Acceptor.cpp" "IO_Handler.cpp" "Parse_Headers.cpp" "Pipeline.cpp" "Pipeline_Tasks.cpp" "Policy.cpp" "Reaper.cpp" "Server.cpp" "Waiter.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\..\lib\JAWS2.dll"
	-@del /f/q "$(OUTDIR)\JAWS2.lib"
	-@del /f/q "$(OUTDIR)\JAWS2.exp"
	-@del /f/q "$(OUTDIR)\JAWS2.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\JAWS2\$(NULL)" mkdir "Release\JAWS2"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D JAWS_BUILD_DLL  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\..\lib\JAWS2.dll" /implib:"$(OUTDIR)\JAWS2.lib"
LINK32_OBJS= \
	"$(INTDIR)\Assoc_Array.obj" \
	"$(INTDIR)\Cache_Manager.obj" \
	"$(INTDIR)\Cache_Object.obj" \
	"$(INTDIR)\Concurrency.obj" \
	"$(INTDIR)\Data_Block.obj" \
	"$(INTDIR)\FILE.obj" \
	"$(INTDIR)\Filecache.obj" \
	"$(INTDIR)\Headers.obj" \
	"$(INTDIR)\IO.obj" \
	"$(INTDIR)\IO_Acceptor.obj" \
	"$(INTDIR)\IO_Handler.obj" \
	"$(INTDIR)\Parse_Headers.obj" \
	"$(INTDIR)\Pipeline.obj" \
	"$(INTDIR)\Pipeline_Tasks.obj" \
	"$(INTDIR)\Policy.obj" \
	"$(INTDIR)\Reaper.obj" \
	"$(INTDIR)\Server.obj" \
	"$(INTDIR)\Waiter.obj"

"..\..\..\lib\JAWS2.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\lib\JAWS2.dll.manifest" mt.exe -manifest "..\..\..\lib\JAWS2.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\..\lib
INTDIR=Static_Debug\JAWS2\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\JAWS2sd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I".." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.JAWS2.dep" "Assoc_Array.cpp" "Cache_Manager.cpp" "Cache_Object.cpp" "Concurrency.cpp" "Data_Block.cpp" "FILE.cpp" "Filecache.cpp" "Headers.cpp" "IO.cpp" "IO_Acceptor.cpp" "IO_Handler.cpp" "Parse_Headers.cpp" "Pipeline.cpp" "Pipeline_Tasks.cpp" "Policy.cpp" "Reaper.cpp" "Server.cpp" "Waiter.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\JAWS2sd.lib"
	-@del /f/q "$(OUTDIR)\JAWS2sd.exp"
	-@del /f/q "$(OUTDIR)\JAWS2sd.ilk"
	-@del /f/q "..\..\..\lib\JAWS2sd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\JAWS2\$(NULL)" mkdir "Static_Debug\JAWS2"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4355 /Fd"..\..\..\lib\JAWS2sd.pdb" /I "..\..\.." /I ".." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\lib\JAWS2sd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Assoc_Array.obj" \
	"$(INTDIR)\Cache_Manager.obj" \
	"$(INTDIR)\Cache_Object.obj" \
	"$(INTDIR)\Concurrency.obj" \
	"$(INTDIR)\Data_Block.obj" \
	"$(INTDIR)\FILE.obj" \
	"$(INTDIR)\Filecache.obj" \
	"$(INTDIR)\Headers.obj" \
	"$(INTDIR)\IO.obj" \
	"$(INTDIR)\IO_Acceptor.obj" \
	"$(INTDIR)\IO_Handler.obj" \
	"$(INTDIR)\Parse_Headers.obj" \
	"$(INTDIR)\Pipeline.obj" \
	"$(INTDIR)\Pipeline_Tasks.obj" \
	"$(INTDIR)\Policy.obj" \
	"$(INTDIR)\Reaper.obj" \
	"$(INTDIR)\Server.obj" \
	"$(INTDIR)\Waiter.obj"

"$(OUTDIR)\JAWS2sd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\JAWS2sd.lib.manifest" mt.exe -manifest "$(OUTDIR)\JAWS2sd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\..\lib
INTDIR=Static_Release\JAWS2\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\JAWS2s.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -I".." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.JAWS2.dep" "Assoc_Array.cpp" "Cache_Manager.cpp" "Cache_Object.cpp" "Concurrency.cpp" "Data_Block.cpp" "FILE.cpp" "Filecache.cpp" "Headers.cpp" "IO.cpp" "IO_Acceptor.cpp" "IO_Handler.cpp" "Parse_Headers.cpp" "Pipeline.cpp" "Pipeline_Tasks.cpp" "Policy.cpp" "Reaper.cpp" "Server.cpp" "Waiter.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\JAWS2s.lib"
	-@del /f/q "$(OUTDIR)\JAWS2s.exp"
	-@del /f/q "$(OUTDIR)\JAWS2s.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\JAWS2\$(NULL)" mkdir "Static_Release\JAWS2"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\..\.." /I ".." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\lib\JAWS2s.lib"
LINK32_OBJS= \
	"$(INTDIR)\Assoc_Array.obj" \
	"$(INTDIR)\Cache_Manager.obj" \
	"$(INTDIR)\Cache_Object.obj" \
	"$(INTDIR)\Concurrency.obj" \
	"$(INTDIR)\Data_Block.obj" \
	"$(INTDIR)\FILE.obj" \
	"$(INTDIR)\Filecache.obj" \
	"$(INTDIR)\Headers.obj" \
	"$(INTDIR)\IO.obj" \
	"$(INTDIR)\IO_Acceptor.obj" \
	"$(INTDIR)\IO_Handler.obj" \
	"$(INTDIR)\Parse_Headers.obj" \
	"$(INTDIR)\Pipeline.obj" \
	"$(INTDIR)\Pipeline_Tasks.obj" \
	"$(INTDIR)\Policy.obj" \
	"$(INTDIR)\Reaper.obj" \
	"$(INTDIR)\Server.obj" \
	"$(INTDIR)\Waiter.obj"

"$(OUTDIR)\JAWS2s.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\JAWS2s.lib.manifest" mt.exe -manifest "$(OUTDIR)\JAWS2s.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.JAWS2.dep")
!INCLUDE "Makefile.JAWS2.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Assoc_Array.cpp"

"$(INTDIR)\Assoc_Array.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Assoc_Array.obj" $(SOURCE)

SOURCE="Cache_Manager.cpp"

"$(INTDIR)\Cache_Manager.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Cache_Manager.obj" $(SOURCE)

SOURCE="Cache_Object.cpp"

"$(INTDIR)\Cache_Object.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Cache_Object.obj" $(SOURCE)

SOURCE="Concurrency.cpp"

"$(INTDIR)\Concurrency.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Concurrency.obj" $(SOURCE)

SOURCE="Data_Block.cpp"

"$(INTDIR)\Data_Block.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Data_Block.obj" $(SOURCE)

SOURCE="FILE.cpp"

"$(INTDIR)\FILE.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\FILE.obj" $(SOURCE)

SOURCE="Filecache.cpp"

"$(INTDIR)\Filecache.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Filecache.obj" $(SOURCE)

SOURCE="Headers.cpp"

"$(INTDIR)\Headers.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Headers.obj" $(SOURCE)

SOURCE="IO.cpp"

"$(INTDIR)\IO.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IO.obj" $(SOURCE)

SOURCE="IO_Acceptor.cpp"

"$(INTDIR)\IO_Acceptor.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IO_Acceptor.obj" $(SOURCE)

SOURCE="IO_Handler.cpp"

"$(INTDIR)\IO_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\IO_Handler.obj" $(SOURCE)

SOURCE="Parse_Headers.cpp"

"$(INTDIR)\Parse_Headers.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Parse_Headers.obj" $(SOURCE)

SOURCE="Pipeline.cpp"

"$(INTDIR)\Pipeline.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Pipeline.obj" $(SOURCE)

SOURCE="Pipeline_Tasks.cpp"

"$(INTDIR)\Pipeline_Tasks.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Pipeline_Tasks.obj" $(SOURCE)

SOURCE="Policy.cpp"

"$(INTDIR)\Policy.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Policy.obj" $(SOURCE)

SOURCE="Reaper.cpp"

"$(INTDIR)\Reaper.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Reaper.obj" $(SOURCE)

SOURCE="Server.cpp"

"$(INTDIR)\Server.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Server.obj" $(SOURCE)

SOURCE="Waiter.cpp"

"$(INTDIR)\Waiter.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Waiter.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.JAWS2.dep")
	@echo Using "Makefile.JAWS2.dep"
!ELSE
	@echo Warning: cannot find "Makefile.JAWS2.dep"
!ENDIF
!ENDIF

