# Microsoft Developer Studio Generated NMAKE File
!IF "$(CFG)" == ""
CFG=Win64 Debug
!MESSAGE No configuration specified. Defaulting to Win64 Debug.
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release"
!ELSE
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE
!MESSAGE NMAKE /f "Makefile.Gateway.mak" CFG="Win64 Debug"
!MESSAGE
!MESSAGE Possible choices for configuration are:
!MESSAGE
!MESSAGE "Win64 Debug" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Release" (based on "Win64 (IA64) Dynamic-Link Library")
!MESSAGE "Win64 Static Debug" (based on "Win64 (IA64) Static Library")
!MESSAGE "Win64 Static Release" (based on "Win64 (IA64) Static Library")
!MESSAGE
!ERROR An invalid configuration was specified.
!ENDIF

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE
NULL=nul
!ENDIF

!IF "$(DEPGEN)" == ""
!IF EXISTS("$(MPC_ROOT)/depgen.pl")
DEPGEN=perl $(MPC_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(DEPGEN_ROOT)/depgen.pl")
DEPGEN=perl $(DEPGEN_ROOT)/depgen.pl -i -t nmake
!ELSEIF EXISTS("$(ACE_ROOT)/bin/depgen.pl")
DEPGEN=perl $(ACE_ROOT)/bin/depgen.pl -i -t nmake
!ENDIF
!ENDIF

GENERATED_DIRTY =

!IF  "$(CFG)" == "Win64 Debug"

OUTDIR=..\..\..\lib
INTDIR=Debug\Gateway\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\lib\Gatewayd.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -f "Makefile.Gateway.dep" "Concrete_Connection_Handlers.cpp" "Config_Files.cpp" "File_Parser.cpp" "Gateway.cpp" "Event_Channel.cpp" "Event_Forwarding_Discriminator.cpp" "Options.cpp" "Connection_Handler.cpp" "Connection_Handler_Acceptor.cpp" "Connection_Handler_Connector.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\Gatewayd.pdb"
	-@del /f/q "..\..\..\lib\Gatewayd.dll"
	-@del /f/q "$(OUTDIR)\Gatewayd.lib"
	-@del /f/q "$(OUTDIR)\Gatewayd.exp"
	-@del /f/q "$(OUTDIR)\Gatewayd.ilk"

"$(INTDIR)" :
	if not exist "Debug\$(NULL)" mkdir "Debug"
	if not exist "Debug\Gateway\$(NULL)" mkdir "Debug\Gateway"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /MDd /GR /Gy /wd4355 /wd4355 /Fd"$(INTDIR)/" /I "..\..\.." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D MPC_LIB_MODIFIER=\"d\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACEd.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /dll /debug /pdb:"..\..\..\lib\Gatewayd.pdb" /machine:IA64 /out:"..\..\..\lib\Gatewayd.dll" /implib:"$(OUTDIR)\Gatewayd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Concrete_Connection_Handlers.obj" \
	"$(INTDIR)\Config_Files.obj" \
	"$(INTDIR)\File_Parser.obj" \
	"$(INTDIR)\Gateway.obj" \
	"$(INTDIR)\Event_Channel.obj" \
	"$(INTDIR)\Event_Forwarding_Discriminator.obj" \
	"$(INTDIR)\Options.obj" \
	"$(INTDIR)\Connection_Handler.obj" \
	"$(INTDIR)\Connection_Handler_Acceptor.obj" \
	"$(INTDIR)\Connection_Handler_Connector.obj"

"..\..\..\lib\Gatewayd.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\lib\Gatewayd.dll.manifest" mt.exe -manifest "..\..\..\lib\Gatewayd.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Release"

OUTDIR=..\..\..\lib
INTDIR=Release\Gateway\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "..\..\..\lib\Gateway.dll"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -f "Makefile.Gateway.dep" "Concrete_Connection_Handlers.cpp" "Config_Files.cpp" "File_Parser.cpp" "Gateway.cpp" "Event_Channel.cpp" "Event_Forwarding_Discriminator.cpp" "Options.cpp" "Connection_Handler.cpp" "Connection_Handler_Acceptor.cpp" "Connection_Handler_Connector.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "..\..\..\lib\Gateway.dll"
	-@del /f/q "$(OUTDIR)\Gateway.lib"
	-@del /f/q "$(OUTDIR)\Gateway.exp"
	-@del /f/q "$(OUTDIR)\Gateway.ilk"

"$(INTDIR)" :
	if not exist "Release\$(NULL)" mkdir "Release"
	if not exist "Release\Gateway\$(NULL)" mkdir "Release\Gateway"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\..\.." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE  /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"

RSC=rc.exe

LINK32=link.exe
LINK32_FLAGS=advapi32.lib user32.lib /INCREMENTAL:NO ACE.lib /libpath:"." /libpath:"..\..\..\lib" /nologo /subsystem:windows /dll  /machine:IA64 /out:"..\..\..\lib\Gateway.dll" /implib:"$(OUTDIR)\Gateway.lib"
LINK32_OBJS= \
	"$(INTDIR)\Concrete_Connection_Handlers.obj" \
	"$(INTDIR)\Config_Files.obj" \
	"$(INTDIR)\File_Parser.obj" \
	"$(INTDIR)\Gateway.obj" \
	"$(INTDIR)\Event_Channel.obj" \
	"$(INTDIR)\Event_Forwarding_Discriminator.obj" \
	"$(INTDIR)\Options.obj" \
	"$(INTDIR)\Connection_Handler.obj" \
	"$(INTDIR)\Connection_Handler_Acceptor.obj" \
	"$(INTDIR)\Connection_Handler_Connector.obj"

"..\..\..\lib\Gateway.dll" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "..\..\..\lib\Gateway.dll.manifest" mt.exe -manifest "..\..\..\lib\Gateway.dll.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Debug"

OUTDIR=..\..\..\lib
INTDIR=Static_Debug\Gateway\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\Gatewaysd.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -D_DEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.Gateway.dep" "Concrete_Connection_Handlers.cpp" "Config_Files.cpp" "File_Parser.cpp" "Gateway.cpp" "Event_Channel.cpp" "Event_Forwarding_Discriminator.cpp" "Options.cpp" "Connection_Handler.cpp" "Connection_Handler_Acceptor.cpp" "Connection_Handler_Connector.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\Gatewaysd.lib"
	-@del /f/q "$(OUTDIR)\Gatewaysd.exp"
	-@del /f/q "$(OUTDIR)\Gatewaysd.ilk"
	-@del /f/q "..\..\..\lib\Gatewaysd.pdb"

"$(INTDIR)" :
	if not exist "Static_Debug\$(NULL)" mkdir "Static_Debug"
	if not exist "Static_Debug\Gateway\$(NULL)" mkdir "Static_Debug\Gateway"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /Ob0 /W3 /Gm /EHsc /Zi /GR /Gy /MDd /wd4355 /wd4355 /Fd"..\..\..\lib\Gatewaysd.pdb" /I "..\..\.." /D _DEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"sd\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\lib\Gatewaysd.lib"
LINK32_OBJS= \
	"$(INTDIR)\Concrete_Connection_Handlers.obj" \
	"$(INTDIR)\Config_Files.obj" \
	"$(INTDIR)\File_Parser.obj" \
	"$(INTDIR)\Gateway.obj" \
	"$(INTDIR)\Event_Channel.obj" \
	"$(INTDIR)\Event_Forwarding_Discriminator.obj" \
	"$(INTDIR)\Options.obj" \
	"$(INTDIR)\Connection_Handler.obj" \
	"$(INTDIR)\Connection_Handler_Acceptor.obj" \
	"$(INTDIR)\Connection_Handler_Connector.obj"

"$(OUTDIR)\Gatewaysd.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\Gatewaysd.lib.manifest" mt.exe -manifest "$(OUTDIR)\Gatewaysd.lib.manifest" -outputresource:$@;2

!ELSEIF  "$(CFG)" == "Win64 Static Release"

OUTDIR=..\..\..\lib
INTDIR=Static_Release\Gateway\IA64

ALL : "$(INTDIR)" "$(OUTDIR)" DEPENDCHECK $(GENERATED_DIRTY) "$(OUTDIR)\Gateways.lib"

DEPEND :
!IF "$(DEPGEN)" == ""
	@echo No suitable dependency generator could be found.
	@echo One comes with MPC, just set the MPC_ROOT environment variable
	@echo to the full path of MPC.  You can download MPC from
	@echo http://www.ociweb.com/products/mpc/down.html
!ELSE
	$(DEPGEN) -I"..\..\.." -DNDEBUG -DWIN64 -DWIN32 -D_WINDOWS -D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE -DACE_AS_STATIC_LIBS -f "Makefile.Gateway.dep" "Concrete_Connection_Handlers.cpp" "Config_Files.cpp" "File_Parser.cpp" "Gateway.cpp" "Event_Channel.cpp" "Event_Forwarding_Discriminator.cpp" "Options.cpp" "Connection_Handler.cpp" "Connection_Handler_Acceptor.cpp" "Connection_Handler_Connector.cpp"
!ENDIF

REALCLEAN : CLEAN
	-@del /f/q "$(OUTDIR)\Gateways.lib"
	-@del /f/q "$(OUTDIR)\Gateways.exp"
	-@del /f/q "$(OUTDIR)\Gateways.ilk"

"$(INTDIR)" :
	if not exist "Static_Release\$(NULL)" mkdir "Static_Release"
	if not exist "Static_Release\Gateway\$(NULL)" mkdir "Static_Release\Gateway"
	if not exist "$(INTDIR)\$(NULL)" mkdir "$(INTDIR)"

CPP=cl.exe
CPP_COMMON=/Zc:wchar_t /nologo /Wp64 /O2 /W3 /EHsc /MD /GR /wd4355 /wd4355 /I "..\..\.." /D NDEBUG /D WIN64 /D WIN32 /D _WINDOWS /D _CRT_SECURE_NO_WARNINGS /D _CRT_SECURE_NO_DEPRECATE /D _CRT_NONSTDC_NO_DEPRECATE /D ACE_AS_STATIC_LIBS /D MPC_LIB_MODIFIER=\"s\" /FD /c

CPP_PROJ=$(CPP_COMMON) /Fo"$(INTDIR)\\"


LINK32=link.exe -lib
LINK32_FLAGS=/nologo /machine:IA64 /out:"..\..\..\lib\Gateways.lib"
LINK32_OBJS= \
	"$(INTDIR)\Concrete_Connection_Handlers.obj" \
	"$(INTDIR)\Config_Files.obj" \
	"$(INTDIR)\File_Parser.obj" \
	"$(INTDIR)\Gateway.obj" \
	"$(INTDIR)\Event_Channel.obj" \
	"$(INTDIR)\Event_Forwarding_Discriminator.obj" \
	"$(INTDIR)\Options.obj" \
	"$(INTDIR)\Connection_Handler.obj" \
	"$(INTDIR)\Connection_Handler_Acceptor.obj" \
	"$(INTDIR)\Connection_Handler_Connector.obj"

"$(OUTDIR)\Gateways.lib" : $(DEF_FILE) $(LINK32_OBJS)
	$(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<
	if exist "$(OUTDIR)\Gateways.lib.manifest" mt.exe -manifest "$(OUTDIR)\Gateways.lib.manifest" -outputresource:$@;2

!ENDIF

CLEAN :
	-@del /f/s/q "$(INTDIR)"

"$(OUTDIR)" :
	if not exist "$(OUTDIR)\$(NULL)" mkdir "$(OUTDIR)"

.c{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.obj::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.c{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cpp{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

.cxx{$(INTDIR)}.sbr::
	$(CPP) @<<
   $(CPP_PROJ) $<
<<

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Gateway.dep")
!INCLUDE "Makefile.Gateway.dep"
!ENDIF
!ENDIF

!IF "$(CFG)" == "Win64 Debug" || "$(CFG)" == "Win64 Release" || "$(CFG)" == "Win64 Static Debug" || "$(CFG)" == "Win64 Static Release" 
SOURCE="Concrete_Connection_Handlers.cpp"

"$(INTDIR)\Concrete_Connection_Handlers.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Concrete_Connection_Handlers.obj" $(SOURCE)

SOURCE="Config_Files.cpp"

"$(INTDIR)\Config_Files.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Config_Files.obj" $(SOURCE)

SOURCE="File_Parser.cpp"

"$(INTDIR)\File_Parser.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\File_Parser.obj" $(SOURCE)

SOURCE="Gateway.cpp"

"$(INTDIR)\Gateway.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Gateway.obj" $(SOURCE)

SOURCE="Event_Channel.cpp"

"$(INTDIR)\Event_Channel.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Event_Channel.obj" $(SOURCE)

SOURCE="Event_Forwarding_Discriminator.cpp"

"$(INTDIR)\Event_Forwarding_Discriminator.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Event_Forwarding_Discriminator.obj" $(SOURCE)

SOURCE="Options.cpp"

"$(INTDIR)\Options.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Options.obj" $(SOURCE)

SOURCE="Connection_Handler.cpp"

"$(INTDIR)\Connection_Handler.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Connection_Handler.obj" $(SOURCE)

SOURCE="Connection_Handler_Acceptor.cpp"

"$(INTDIR)\Connection_Handler_Acceptor.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Connection_Handler_Acceptor.obj" $(SOURCE)

SOURCE="Connection_Handler_Connector.cpp"

"$(INTDIR)\Connection_Handler_Connector.obj" : $(SOURCE)
	$(CPP) $(CPP_COMMON) /Fo"$(INTDIR)\Connection_Handler_Connector.obj" $(SOURCE)

!IF  "$(CFG)" == "Win64 Debug"
!ELSEIF  "$(CFG)" == "Win64 Release"
!ELSEIF  "$(CFG)" == "Win64 Static Debug"
!ELSEIF  "$(CFG)" == "Win64 Static Release"
!ENDIF


!ENDIF

GENERATED : "$(INTDIR)" "$(OUTDIR)" $(GENERATED_DIRTY)
	-@rem

DEPENDCHECK :
!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Makefile.Gateway.dep")
	@echo Using "Makefile.Gateway.dep"
!ELSE
	@echo Warning: cannot find "Makefile.Gateway.dep"
!ENDIF
!ENDIF

