// $Id: Default_Dispatcher_Impl.inl 14 2007-02-01 15:49:12Z mitza $

namespace Kokyu
{
ACE_INLINE
Shutdown_Task_Command::Shutdown_Task_Command (ACE_Allocator *mb_allocator)
   :Dispatch_Command(0,mb_allocator)
{
}

}
